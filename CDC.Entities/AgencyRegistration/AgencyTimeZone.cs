﻿using CDC.Entities.AgencyRegistration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.AgencyRegistration
{
    /// <summary>
    ///     The Pricing Class
    /// </summary>
    /// <seealso cref="IEntityBase" />
    public class AgencyTimeZone : IEntityBase
    {
        public string TimeZone { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long? AgencyRegistrationId { get; set; }
        [ForeignKey("AgencyRegistrationId")]
        public virtual AgencyRegistrationInfo AgencyRegistrationInfo { get; set; }
    }
}