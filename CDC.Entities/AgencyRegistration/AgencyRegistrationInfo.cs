﻿using System.Collections.Generic;
using CDC.Entities.Family;
using CDC.Entities.Pricing;
using CDC.Entities.TimeClockUsers;
using System;

namespace CDC.Entities.AgencyRegistration
{
    public class AgencyRegistrationInfo : IEntityBase
    {
        public string AgencyName { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsExistingAccount { get; set; }
        public bool IsLoggedFirstTime { get; set; }
        public long UserId { get; set; }
        public long? PricingPlanId { get; set; }

        public string StripeUserId { get; set; }

        public string StripeSubscriptionId { get; set; }
        public long? TimeClockUsersPlanId { get; set; }
        public virtual ICollection<AgencyContactInfo> AgencyContactInfos { get; set; }
        public virtual ICollection<AgencyBillingInfo> AgencyBillingInfos { get; set; }
        public virtual ICollection<AgencyPaymentInfo> AgencyPaymentInfos { get; set; }
        public virtual ICollection<FamilyInfo> Family { get; set; }
        public virtual PricingPlan PricingPlan { get; set; }
        public virtual TimeClockUsersPlan TimeClockUsersPlan { get; set; }
        public virtual ICollection<AgencyTimeZone> AgencyTimeZones { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public int TotalTimeClockUsers { get; set; }
        public int TotalRequireEnrollParticipants{ get; set; }
        public string StripeTimeClockSubscriptionId { get; set; }
        public bool? IsTrial { get; set; }
        public DateTime? TrialStart { get; set; }
        public DateTime? TrialEnd { get; set; }
        public bool? IsTrialMailSent { get; set; }

    }
}