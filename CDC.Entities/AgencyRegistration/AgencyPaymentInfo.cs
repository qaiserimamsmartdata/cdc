﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.AgencyRegistration
{
    public class AgencyPaymentInfo : IEntityBase
    {
        [Column(Order = 2)]
        public long AgencyId { get; set; }

        public bool IsCreditCash { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public int CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        public string CVV { get; set; }
        public string CardBillingAddress { get; set; }
        public long? CardCountryId { get; set; }
        public long? CardStateId { get; set; }
        public long? CardCityId { get; set; }
        public string CardCityName { get; set; }
        public string CardPostalCode { get; set; }
        public bool Acceptance { get; set; }
        public long? AgencyRegistrationId { get; set; }

        [ForeignKey("AgencyRegistrationId")]
        public virtual AgencyRegistrationInfo AgencyRegistrationInfo { get; set; }

        [Column(Order = 1)]
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
    }
}