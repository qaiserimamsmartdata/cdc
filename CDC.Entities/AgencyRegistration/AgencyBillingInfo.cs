﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.AgencyRegistration
{
    public class AgencyBillingInfo : IEntityBase
    {
        [Column(Order = 2)]
        public long AgencyId { get; set; }

        public string AuthorizedBillingAccountname { get; set; }
        public string BillingEmail { get; set; }
        public long? AgencyRegistrationId { get; set; }

        [ForeignKey("AgencyRegistrationId")]
        public virtual AgencyRegistrationInfo AgencyRegistrationInfo { get; set; }

        [Column(Order = 1)]
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
    }
}