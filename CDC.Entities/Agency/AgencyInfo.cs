﻿using CDC.Entities.Class;
using CDC.Entities.Schedule;
using CDC.Entities.Students;
using System;
using System.Collections.Generic;

namespace CDC.Entities.Agency
{
   public class AgencyInfo : IEntityBase
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public string AgencyName { get; set; }
        public string ContactPerson { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        
        public Nullable<long> CountryId { get; set; }
        
        public Nullable<long> StateId { get; set; }
        //[ForeignKey("City")]
        public Nullable<long> CityId { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public bool IsLoggedFirstTime { get; set; }
        //public virtual Country Country { get; set; }
        //public virtual State State { get; set; }

        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<ClassInfo> ClassInfos { get; set; }
        public virtual ICollection<LessonPlan> LassonPlans { get; set; }
        //public virtual ICollection<Category> Categories { get; set; }
        //public virtual ICollection<Room> Rooms { get; set; }
        //public virtual ICollection<StudentSchedule> StudentSchedules { get; set; }
        public virtual City City { get; set; }

    }
}
