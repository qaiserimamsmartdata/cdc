﻿using System;
using System.Collections.Generic;

namespace CDC.Entities
{
    /// <summary>
    ///     The State Class
    /// </summary>
    /// <seealso cref="IEntityBase" />
    public class State : IEntityBase
    {
        public State()
        {
            Cities = new HashSet<City>();
        }

        /// <summary>
        ///     Gets or sets the country identifier.
        /// </summary>
        /// <value>
        ///     The country identifier.
        /// </value>
        public long CountryId { get; set; }

        /// <summary>
        ///     Gets or sets the name of the state.
        /// </summary>
        /// <value>
        ///     The name of the state.
        /// </value>
        public string StateName { get; set; }

        /// <summary>
        ///     Gets or sets the state code.
        /// </summary>
        /// <value>
        ///     The state code.
        /// </value>
        public string StateCode { get; set; }

        /// <summary>
        ///     Gets or sets the created by.
        /// </summary>
        /// <value>
        ///     The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        ///     Gets or sets the created on.
        /// </summary>
        /// <value>
        ///     The created on.
        /// </value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        ///     Gets or sets the modified by.
        /// </summary>
        /// <value>
        ///     The modified by.
        /// </value>
        public string ModifiedBy { get; set; }

        /// <summary>
        ///     Gets or sets the modified on.
        /// </summary>
        /// <value>
        ///     The modified on.
        /// </value>
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        ///     Gets or sets the deleted by.
        /// </summary>
        /// <value>
        ///     The deleted by.
        /// </value>
        public string DeletedBy { get; set; }

        /// <summary>
        ///     Gets or sets the deleted on.
        /// </summary>
        /// <value>
        ///     The deleted on.
        /// </value>
        public DateTime? DeletedOn { get; set; }

        public virtual Country Country { get; set; }

        protected virtual ICollection<City> Cities { get; set; }

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public long ID { get; set; }

        /// <summary>
        ///     Gets or sets the is deleted.
        /// </summary>
        /// <value>
        ///     The is deleted.
        /// </value>
        public bool? IsDeleted { get; set; }
    }
}