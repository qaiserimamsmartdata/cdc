﻿using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Tools.Category;

namespace CDC.Entities.Skills
{
    public class Skills : IEntityBase
    {
        public string Skill { get; set; }

        public string SubSkill { get; set; }

        public string Details { get; set; }

        public long DaysRequired { get; set; }
        public long ClassesRequired { get; set; }


        [ForeignKey("Category")]
        public long CategoryId { get; set; }

        public virtual Category Category { get; set; }

        [Column(Order = 2)]
        public long AgencyId { get; set; }

        [ForeignKey("AgencyId")]
        public virtual AgencyRegistrationInfo AgencyInfo { get; set; }

        [Column(Order = 1)]
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
    }
}