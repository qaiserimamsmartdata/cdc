﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Skills
{
    public class StaffSkill : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }

        [Column(Order = 2)]
        public long AgencyId { get; set; }

        public long StaffId { get; set; }
        public long SkillMasterId { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
        public virtual SkillMaster SkillMaster { get; set; }
    }
}
