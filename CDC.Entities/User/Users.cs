﻿using CDC.Entities.AgencyRegistration;
using CDC.Entities.Staff;
using System;
using System.Collections.Generic;

namespace CDC.Entities.User
{
    /// <summary>
    /// User's class
    /// </summary>
    public class Users : IEntityBase
    {
        public long ID { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public long RoleId { get; set; }
        public bool? IsDeleted { get; set; }
        public string Token { get; set; }
        public DateTime? TokenExpiryDate { get; set; }
        public virtual Roles Role { get; set; }
        public virtual ICollection<StaffInfo> Staffinfo { get; set; }
        public virtual ICollection<AgencyRegistrationInfo> Agencyinfo { get; set; }
        public ICollection<ForgotPasswordLog> ForgotPasswordLogInfo{ get; set; }
    }
}
