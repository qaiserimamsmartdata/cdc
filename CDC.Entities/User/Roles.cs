﻿using System.Collections.Generic;

namespace CDC.Entities.User
{
    public class Roles : IEntityBase
    {
        public long ID { get; set; }
        public string RoleName{ get; set; }
        public bool? IsDeleted { get; set; }
        public virtual ICollection<Users> User { get; set; }

    }
}
