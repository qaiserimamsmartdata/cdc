﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.User
{
   public class ForgotPasswordLog : IEntityBase
    {
        public long ID { get; set; }
        public long UserId { get; set; }
        public Guid Guid { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        //public Nullable<bool> IsReset { get; set; }

        public virtual Users User { get; set; }
    }
}
