﻿using System;
using CDC.Entities.Staff;

namespace CDC.Entities.ExceptionLogs
{
    public class ExceptionLogInfo : IEntityBase
    {
        public long? AgencyId { get; set; }
        public DateTime? CreatedDate { get; set; }        
        public string MethodName { get; set; }
        public string ExMessage { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
      
    }
}