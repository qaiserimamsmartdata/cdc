﻿using CDC.Entities.AgencyRegistration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Association
{
   public class NewParticipantAssociationMaster:IEntityBase
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public virtual AgencyRegistrationInfo Agency { get; set; }
        public long? AgencyId { get; set; }
        public string AssociationTypeName { get; set; }
    }
}