﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.Class;
using CDC.Entities.Family;
using System.Collections;
using System.Collections.Generic;
using CDC.Entities.ParticipantEnrollmentPayment;
using CDC.Entities.NewParticipant;

namespace CDC.Entities.Schedule
{
    public class StudentSchedule : IEntityBase
    {
        public string Title { get; set; }
        public long CategoryId { get; set; }
        public long ClassId { get; set; }
        public long RoomId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? MondayIn { get; set; }
        public TimeSpan? MondayOut { get; set; }
        public TimeSpan? TuesdayIn { get; set; }
        public TimeSpan? TuesdayOut { get; set; }
        public TimeSpan? WednesdayIn { get; set; }
        public TimeSpan? WednesdayOut { get; set; }
        public TimeSpan? ThursdayIn { get; set; }
        public TimeSpan? ThursdayOut { get; set; }
        public TimeSpan? FridayIn { get; set; }
        public TimeSpan? FridayOut { get; set; }
        public TimeSpan? SaturdayIn { get; set; }
        public TimeSpan? SaturdayOut { get; set; }
        public decimal? TutionFees { get; set; }
        public decimal? PaidFees { get; set; }
        public long? StudentId { get; set; }
        [ForeignKey("StudentId")]
        public virtual tblParticipantInfo StudentInfo { get; set; }

        [ForeignKey("ClassId")]
        public virtual ClassInfo ClassInfo { get; set; }

        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public bool IsEnrolled { get; set; }

        public virtual ICollection<ParticipantEnrollPayment> PaymentStatusList { get; set; }
        //public virtual Category { get; set; }
        //public virtual Room { get; set; }
    }
}