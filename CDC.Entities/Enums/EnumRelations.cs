﻿namespace CDC.Entities.Enums
{
    public enum EnumRelations
    {
        Father = 0,
        Mother = 1,
        Uncle = 2,
        GrandMother = 3,
        GrandFather = 4
    }
}