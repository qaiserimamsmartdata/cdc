﻿namespace CDC.Entities.Enums
{
    public enum EnumGender
    {
        Male = 1,
        Female = 2,
        Other = 3
    }
}