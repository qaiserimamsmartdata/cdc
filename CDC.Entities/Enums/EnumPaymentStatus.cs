﻿namespace CDC.Entities.Enums
{
    public enum EnumPaymentStatus
    {
        Unpaid = 0,
        Paid = 1,
        Other = 2
    }
}