﻿namespace CDC.Entities.Enums
{
    public enum EnumClassStatus
    {
        Active = 0,
        Inactive = 1
    }
}