﻿namespace CDC.Entities.Enums
{
    public enum EnumLeaveStatus
    {
        Pending = 1,
        OnLeave = 2,
        Declined = 3,
        Approved = 4
    }
}