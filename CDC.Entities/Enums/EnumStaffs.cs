﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Enums
{
    public enum InOutPurpose
    {
        Day = 0,
        Lunch = 1,
        Break = 2
    }

    public enum TimeCheckInOrOut
    {
        In = 1,
        Out = 0
    }
}
