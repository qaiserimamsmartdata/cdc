﻿namespace CDC.Entities.Enums
{
    public enum EnumClasses
    {
        Pre_School = 0,
        Infant = 1,
        Toddler = 2,
        After_School = 3
    }
}