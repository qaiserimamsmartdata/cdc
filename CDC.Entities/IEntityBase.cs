﻿namespace CDC.Entities
{
    /// <summary>
    ///     The Base Entity Class
    /// </summary>
    public interface IEntityBase
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        long ID { get; set; }

        /// <summary>
        ///     Gets or sets the is deleted.
        /// </summary>
        /// <value>
        ///     The is deleted.
        /// </value>
        bool? IsDeleted { get; set; }
    }
}