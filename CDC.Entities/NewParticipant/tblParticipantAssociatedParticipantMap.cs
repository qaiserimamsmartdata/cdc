﻿using CDC.Entities.Association;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.NewParticipant
{
    public class tblParticipantAssociatedParticipantMap : IEntityBase
    {
        public long ID { get; set; }
        public virtual tblParticipantInfo NewParticipantInfo { get; set; }
        public virtual tblParticipantInfo AssociatedParticipantInfo { get; set; }
        public bool? IsDeleted { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? NewParticipantInfoId { get; set; }
        public long? AssociatedParticipantInfoId { get; set; }
        public long? AssociationTypeMasterID { get; set; }
        public virtual NewParticipantAssociationMaster AssociationTypeMaster { get; set; }

    }
}
