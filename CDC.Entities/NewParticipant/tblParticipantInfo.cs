﻿using CDC.Entities.AgencyRegistration;
using CDC.Entities.Association;
using CDC.Entities.NewParent;
using CDC.Entities.Tools.Family;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.NewParticipant
{
    public class tblParticipantInfo:IEntityBase
    {
        public virtual AgencyRegistrationInfo Agency { get; set; }
        public int Gender { get; set; }
        public string ImagePath { get; set; }
        public long? AgencyId { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Radio { get; set; }
        public string RadioParent { get; set; }
        public string FullName { get; set; }
        public string SchoolName { get; set; }
        public string Transportation { get; set; }
        public string Description { get; set; }
        public string Disabilities { get; set; }
        public string Allergies { get; set; }
        public string Medications { get; set; }
        public string PrimaryDoctor { get; set; }
        public long? GradeLevelId { get; set; }
        public virtual GradeLevel GradeLevel { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public virtual MembershipType MembershipType { get; set; }
        public long? MembershipTypeId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string InsuranceCarrier { get; set; }
        public string InsurancePolicyNumber { get; set; }
        public string PreferredHospitalName { get; set; }
        public string PreferredHospitalAddress { get; set; }
        public string FamilyDoctorName { get; set; }
        public string FamilyDoctorPhoneNumber { get; set; }

    }
}
