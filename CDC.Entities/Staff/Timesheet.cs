﻿using CDC.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Staff
{
    public class Timesheet : IEntityBase
    {
        public long ID { get; set; }
        public int StaffID { get; set; }
        public DateTime? TimeIN { get; set; }
        public DateTime? TimeINStamp { get; set; }
        public string TimeINComments { get; set; }
        public DateTime? TimeOUT { get; set; }
        public DateTime? TimeOUTStamp { get; set; }
        public string TimeOUTComments { get; set; }
        public double? TotalHours { get; set; }
        public DateTime? LastModified { get; set; }
        public int? EditedBy { get; set; }
        public string INIPAddress { get; set; }
        public string OUTIPAddress { get; set; }
        public InOutPurpose CheckInOutPurpose { get; set; }
        public string Timezone { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
