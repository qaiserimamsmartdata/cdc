﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Staff
{
    public class Tasks : IEntityBase
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public long StaffId { get; set; }
        public long FamilyId { get; set; }

        public long ClassId { get; set; }
        public long RoomId { get; set; }

        public long LeaveId { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public string StartTimezone { get; set; }
        public DateTime End { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }

        public bool IsAllDay { get; set; }
        public int? OwnerID { get; set; }

        public bool? IsDeleted { get; set; }
        public bool? IsSendEmailChecked { get; set; }

        public bool? IsRecurring { get; set; }

        public string EndsOn { get; set; }
    }
}
