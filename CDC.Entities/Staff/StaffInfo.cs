﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Enums;
using CDC.Entities.Tools.Staff;
using CDC.Entities.User;

namespace CDC.Entities.Staff
{
    public class StaffInfo : IEntityBase
    {
        [Column(Order = 2)]
        public long AgencyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EnumGender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ImagePath { get; set; }
        public long? PositionId { get; set; }
        public long? StatusId { get; set; }
        public DateTime? DateHired { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }


        public string Certification { get; set; }

        public long? CityId { get; set; }
        public string PostalCode { get; set; }
        //Mobile
        public string PhoneNumber { get; set; }
        public string HomePhone { get; set; }
        public long? AgencyInfoId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CityName { get; set; }
        public virtual City City { get; set; }
        public long? AgencyRegistrationId { get; set; }

        [ForeignKey("AgencyRegistrationId")]
        public virtual AgencyRegistrationInfo AgencyRegistrationInfo { get; set; }

        public virtual Position Position { get; set; }
        public virtual Status Status { get; set; }
        //public Nullable<long> StaffLeaveId { get; set; }
        //public virtual LeaveInfo StaffLeave { get; set; }
        public long TotalCnt { get; set; }
        public long UserId { get; set; }
        //public virtual ICollection<StaffScheduleInfo> StaffScheduleList { get; set; }

        [Column(Order = 1)]
        public long ID { get; set; }
        public virtual Users User { get; set; }

        public bool? IsDeleted { get; set; }

        public string Apartment { get; set; }
        //public long? AgencyLocationInfoId { get; set; }
        public bool? IsTimeClockUser { get; set; }

        public long? SMSCarrierId { get; set; }
        public virtual SMSCarrier.SMSCarrier SMSCarrier { get; set; }
        //public virtual AgencyLocationInfo AgencyLocationInfo { get; set; }
        public virtual ICollection<StaffLocationInfo> StaffLocationInfoList { get; set; }
    }
}