﻿using System;
using System.Collections.Generic;
using CDC.Entities.Attendance;
using CDC.Entities.Class;
using CDC.Entities.Tools.Class;

namespace CDC.Entities.Staff
{
    public class StaffScheduleInfo : IEntityBase
    {
        public string Title { get; set; }
        public long? StaffId { get; set; }
        public long? ClassId { get; set; }
        public long? RoomId { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? Time { get; set; }

        public DateTime? EndDate { get; set; }
        public TimeSpan? EndTime { get; set; }

        public long? LessonPlanId { get; set; }
        public virtual StaffInfo Staff { get; set; }
        public virtual ClassInfo Class { get; set; }
        public virtual Room Room { get; set; }
        public virtual LessonPlan LessonPlan { get; set; }
        public long TotalCnt { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        //public virtual ICollection<AttendanceInfo> AttendanceInfo { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }

        public int RecurrenceTypeId { get; set; }
        public int RecurrenceTypeValue { get; set; }

    }
}