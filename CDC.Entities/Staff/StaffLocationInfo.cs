﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Enums;
using CDC.Entities.Tools.Staff;
using CDC.Entities.User;

namespace CDC.Entities.Staff
{
    public class StaffLocationInfo : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }
        [Column(Order = 2)]
        public long AgencyId { get; set; }
        public bool? IsDeleted { get; set; }
        public long? StaffId { get; set; }
        [ForeignKey("StaffId")]
        public virtual StaffInfo StaffInfo { get; set; }
        public long? AgencyLocationInfoId { get; set; }
        public virtual AgencyLocationInfo AgencyLocationInfo { get; set; }
        
    }
}