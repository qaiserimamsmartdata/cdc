﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Staff
{
 public   class TasksClassMapping:IEntityBase
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long TaskId { get; set; }
        public long ClassId { get; set; }
    }
}
