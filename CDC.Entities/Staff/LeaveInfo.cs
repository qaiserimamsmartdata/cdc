﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.Tools.Staff;

namespace CDC.Entities.Staff
{
    public class LeaveInfo : IEntityBase
    {
        [Column(Order = 2)]
        public long AgencyId { get; set; }

        public long? StaffId { get; set; }
        public long? StatusId { get; set; }
        public DateTime? LeaveFromDate { get; set; }
        public DateTime? LeaveToDate { get; set; }
        public virtual StaffInfo Staff { get; set; }
        public virtual Status Status { get; set; }
        public long TotalCnt { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? LeaveTypeId { get; set; }
        public virtual LeaveType LeaveType { get; set; }

        /// <summary>
        ///     Gets or sets the remark.
        ///     Use when approval or cancel the leave.
        /// </summary>
        /// <value>
        ///     The remark.
        /// </value>
        public string Remarks { get; set; }

        /// <summary>
        ///     Gets or sets the action taken by.
        ///     Use when Cancel or Approve the leave
        /// </summary>
        /// <value>
        ///     The action taken by.
        /// </value>
        public long? ActionTakenBy { get; set; }

        /// <summary>
        ///     Gets or sets the action date.
        ///     Use when Cancel or Approve the leave
        /// </summary>
        /// <value>
        ///     The action date.
        /// </value>
        public DateTime? ActionDate { get; set; }

        [Column(Order = 1)]
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
    }
}