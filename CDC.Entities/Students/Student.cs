﻿using System;
using System.Collections.Generic;
using CDC.Entities.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.Agency;
using CDC.Entities.Schedule;
using CDC.Entities.Family;

namespace CDC.Entities.Students
{
    /// <summary>
    ///     CDC Customer Info
    /// </summary>
    public class Student : IEntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EnumGender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ImagePath { get; set; }
        public virtual ICollection<ParentInfo> ParentInfos { get; set; }
        public virtual ICollection<ContactInfo> ContactInfos { get; set; }
        public virtual ICollection<GuardianInfo> GuardianInfos { get; set; }
        public virtual ICollection<EnrollmentInfo> EnrollmentInfos { get; set; }
        public virtual ICollection<Skills.Skills> Skills { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public Nullable<long> AgencyId { get; set; }
        [ForeignKey("AgencyId")]
        public virtual AgencyInfo AgencyInfo { get; set; }

        public virtual ICollection<StudentSchedule> StudentSchedules { get; set; }
    }
}