﻿using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.Enums;

namespace CDC.Entities.Students
{
    /// <summary>
    ///     CDC Customer Info
    /// </summary>
    public class GuardianInfo : IEntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string EmailId { get; set; }


        //[ForeignKey("Relation")]
        public EnumRelations RelationId { get; set; }

        [ForeignKey("Student")]
        public long StudentId { get; set; }

        public virtual Student Student { get; set; }

        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        //public virtual Relation Relation { get; set; }
    }
}