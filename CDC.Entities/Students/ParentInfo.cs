﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.Students
{
    /// <summary>
    ///     CDC Customer Info
    /// </summary>
    public class ParentInfo : IEntityBase
    {
        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public string Mobile { get; set; }

        public string EmailId { get; set; }

        [ForeignKey("Student")]
        public long StudentId { get; set; }

        public virtual Student Student { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}