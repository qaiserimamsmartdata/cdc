﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.Enums;
using CDC.Entities.Family;

namespace CDC.Entities.Students
{
    /// <summary>
    ///     CDC Customer Info
    /// </summary>
    public class EnrollmentInfo : IEntityBase
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AdditionalInfo { get; set; }
        //[ForeignKey("Room")]
        public EnumRooms RoomId { get; set; }

        //[ForeignKey("Class")]
        public EnumClasses ClassId { get; set; }

        [ForeignKey("Student")]
        public long StudentId { get; set; }

        public virtual StudentInfo Student { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        //public virtual Class { get; set; }
        //public virtual Room { get; set; }
    }
}