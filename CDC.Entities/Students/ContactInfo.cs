﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.Students
{
    /// <summary>
    ///     CDC Customer Info
    /// </summary>
    public class ContactInfo : IEntityBase
    {
        public string Address { get; set; }

        //[ForeignKey("Country")]
        public long CountryId { get; set; }

        //[ForeignKey("State")]
        public long StateId { get; set; }

        //[ForeignKey("City")]
        public string City { get; set; }
        public string PostalCode { get; set; }

        [ForeignKey("Student")]
        public long StudentId { get; set; }

        public virtual Student Student { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}