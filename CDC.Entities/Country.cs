﻿using System;
using System.Collections.Generic;

namespace CDC.Entities
{
    /// <summary>
    ///     The Country Class
    /// </summary>
    /// <seealso cref="IEntityBase" />
    public class Country : IEntityBase
    {
        public Country()
        {
            States = new HashSet<State>();
        }

        ///// <summary>
        ///// Gets or sets the country identifier.
        ///// </summary>
        ///// <value>
        ///// The country identifier.
        ///// </value>
        //public long CountryId { get; set; }
        /// <summary>
        ///     Gets or sets the name of the country.
        /// </summary>
        /// <value>
        ///     The name of the country.
        /// </value>
        public string CountryName { get; set; }

        /// <summary>
        ///     Gets or sets the country code.
        /// </summary>
        /// <value>
        ///     The country code.
        /// </value>
        public string CountryCode { get; set; }

        /// <summary>
        ///     Gets or sets the number code.
        /// </summary>
        /// <value>
        ///     The number code.
        /// </value>
        public float NumCode { get; set; }

        /// <summary>
        ///     Gets or sets the phone code.
        /// </summary>
        /// <value>
        ///     The phone code.
        /// </value>
        public float PhoneCode { get; set; }

        /// <summary>
        ///     Gets or sets the created by.
        /// </summary>
        /// <value>
        ///     The created by.
        /// </value>
        public string CreatedBy { get; set; }

        /// <summary>
        ///     Gets or sets the created on.
        /// </summary>
        /// <value>
        ///     The created on.
        /// </value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        ///     Gets or sets the modified by.
        /// </summary>
        /// <value>
        ///     The modified by.
        /// </value>
        public string ModifiedBy { get; set; }

        /// <summary>
        ///     Gets or sets the modified on.
        /// </summary>
        /// <value>
        ///     The modified on.
        /// </value>
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        ///     Gets or sets the deleted by.
        /// </summary>
        /// <value>
        ///     The deleted by.
        /// </value>
        public string DeletedBy { get; set; }

        /// <summary>
        ///     Gets or sets the deleted on.
        /// </summary>
        /// <value>
        ///     The deleted on.
        /// </value>
        public DateTime? DeletedOn { get; set; }

        /// <summary>
        ///     Gets or sets the active.
        /// </summary>
        /// <value>
        ///     The active.
        /// </value>
        public string Active { get; set; }

        /// <summary>
        ///     Gets or sets the states.
        /// </summary>
        /// <value>
        ///     The states.
        /// </value>
        protected virtual ICollection<State> States { get; set; }

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public long ID { get; set; }

        /// <summary>
        ///     Gets or sets the is deleted.
        /// </summary>
        /// <value>
        ///     The is deleted.
        /// </value>
        public bool? IsDeleted { get; set; }
    }
}