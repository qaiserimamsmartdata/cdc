﻿using System;
using CDC.Entities.Tools.Family;

namespace CDC.Entities.Family
{
    public class ContactInfo : IEntityBase
    {
        public string Address { get; set; }
        public string CityName { get; set; }
        public long StateId { get; set; }
        public long CountryId { get; set; }
        public string PostalCode { get; set; }
        public string EmergencyFirstName { get; set; }
        public string EmergencyLastName { get; set; }
        public long RelationshipToParticipantId { get; set; }
        public string EmergencyPhoneNumber { get; set; }
        public string InsuranceCarrier { get; set; }
        public string InsurancePolicyNumber { get; set; }
        public string PreferredHospitalName { get; set; }
        public string PreferredHospitalAddress { get; set; }
        public string FamilyDoctorName { get; set; }
        public string FamilyDoctorPhoneNumber { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual State State { get; set; }
        public virtual Country Country { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}