﻿using System;
using CDC.Entities.Tools.Family;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.Family
{
    public class PaymentInfo : IEntityBase
    {
        [Column(Order = 2)]
       public long FamilyId { get; set; }
        public string CardName { get; set; }
        public int CardType { get; set; }
      
        public long CardNumber { get; set; }
        public int CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }    
        public virtual FamilyInfo Family { get; set; }
        [Column(Order = 1)]
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CVV { get; set; }
    }
}