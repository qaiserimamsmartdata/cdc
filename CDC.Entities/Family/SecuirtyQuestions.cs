﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Family
{
   public class SecurityQuestions: IEntityBase
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public string SecurityQuestion { get; set; }
    }
}
