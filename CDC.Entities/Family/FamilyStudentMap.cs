﻿using System;

namespace CDC.Entities.Family
{
    public class FamilyStudentMap : IEntityBase
    {
        public long FamilyId { get; set; }
        public long StudentId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual FamilyInfo Family { get; set; }
        public virtual StudentInfo Student { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }       
    }
}