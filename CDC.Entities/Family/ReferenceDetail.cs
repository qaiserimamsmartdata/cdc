﻿using System;
using CDC.Entities.Tools.Family;

namespace CDC.Entities.Family
{
    public class ReferenceDetail : IEntityBase
    {
        public long? FamilyId { get; set; }
        public long InfoSourceId { get; set; }
        public string ReferedBy { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual FamilyInfo Family { get; set; }
        public virtual InfoSource InfoSource { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}