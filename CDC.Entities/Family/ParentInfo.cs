﻿using System;
using CDC.Entities.Tools.Family;

namespace CDC.Entities.Family
{
    public class ParentInfo : IEntityBase
    {
        public long FamilyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string HomePhone { get; set; }
        public string EmailId { get; set; }
        public long RelationTypeId { get; set; }
        public string SecurityKey { get; set; }
        public bool? IsPrimary { get; set; } 
        public string ImagePath { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual FamilyInfo Family { get; set; }
        public virtual RelationType RelationType { get; set; }
        public long SecurityQuestionId { get; set; }
        public string SecurityQuestionAnswer { get; set; }
        public virtual SecurityQuestions SecurityQuestion { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long SMSCarrierId { get; set; }
        public virtual SMSCarrier.SMSCarrier SMSCarrier { get; set; }
        public bool? IsEventMailReceived { get; set; }
        public bool IsLoggedFirstTime { get; set; }
        public bool? IsParticipantAttendanceMailReceived { get; set; }
    }
}