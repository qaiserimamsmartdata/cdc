﻿using System;
using CDC.Entities.Tools.Family;

namespace CDC.Entities.Family
{
    public class StudentDetail : IEntityBase
    {
        public long studentId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string SchoolName { get; set; }
        public string Transportation { get; set; }
        public string Disabilities { get; set; }
        public string Allergies { get; set; }
        public string Medications { get; set; }
        public string PrimaryDoctor { get; set; }
        public long? GradeLevelId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Description { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual GradeLevel GradeLevel { get; set; }
        public virtual StudentInfo Student { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}