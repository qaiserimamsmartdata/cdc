﻿using CDC.Entities.ParticipantIncident;
using CDC.Entities.Schedule;
using CDC.Entities.Tools.Family;
using System;
using System.Collections.Generic;

namespace CDC.Entities.Family
{
    public class StudentInfo : IEntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ImagePath { get; set; }
        public long? MembershipTypeId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public virtual MembershipType MembershipType { get; set; }
        public ICollection<StudentSchedule> StudentSchedule { get; set; }
        
        //public ICollection<Incident> Incident { get; set; }

        //public string Description { get; set; }
    }
}