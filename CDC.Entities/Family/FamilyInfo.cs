﻿using System;
using System.Collections.Generic;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Common;

namespace CDC.Entities.Family
{
    public class FamilyInfo : IEntityBase
    {
        public long AgencyId { get; set; }
        public string FamilyName { get; set; }
        public string SecurityKey { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual AgencyRegistrationInfo Agency { get; set; }
        public virtual ICollection<ParentInfo> ParentInfo { get; set; }
        public virtual ICollection<ContactInfoMap> ContactInfoMap { get; set; }
        public virtual ICollection<FamilyStudentMap> FamilyStudentMap { get; set; }
        public virtual ICollection<ReferenceDetail> ReferenceDetail { get; set; }
        public virtual ICollection<PaymentInfo> PaymentInfo { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}