﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Messages
{
    public class MessageTo : IEntityBase
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string image { get; set; }
        public long messageId { get; set; }
        public virtual Messages message { get; set; }
    }
}
