﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Messages
{
    public class Messages : IEntityBase
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }

        public bool? IsInboxDeleted { get; set; }
        public bool? IsSentDeleted { get; set; }
        public string subject { get; set; }
        public bool unread { get; set; }
        public DateTime date { get; set; }
        public string content { get; set; }
        public virtual ICollection<MessageTo> to { get; set; }
        public virtual ICollection<MessageCC> cc { get; set; }
        public virtual ICollection<MessageFrom> from1 { get; set; }

       
       
    }
}
