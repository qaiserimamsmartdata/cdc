﻿using CDC.Entities.AgencyRegistration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.PaymentHistory
{
    public class PaymentHistoryInfo : IEntityBase
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }      
        public long OrganizationID { get; set; }
        public long PlanId { get; set; }
        public decimal PlanAmount { get; set; }
        public string PlanName { get; set; }
        public string EmailAddress { get; set; }
        public string OrganizationName { get; set; }
        public string PhoneNumber { get; set; }
        public string StripeSubscriptionId { get; set; }
        public bool IsFinal { get; set; }
        public long CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime PlanExpiryDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> DeletedBy { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public string EventType { get; set; }
        public bool LiveMode { get; set; }
        public string JsonObject { get; set; }
        public string EventId { get; set; }
        public string balance_transaction { get; set; }
        public string CustomerId { get; set; }
    }
}