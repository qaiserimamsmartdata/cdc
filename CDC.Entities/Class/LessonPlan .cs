﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;

namespace CDC.Entities.Class
{
    public class LessonPlan : IEntityBase
    {
        public string Name { get; set; }

        public DateTime? Date { get; set; }

        public string Theme { get; set; }

        public long Order { get; set; }
        public string SpecialInstructions { get; set; }
        public string Description { get; set; }

        public string UploadFile { get; set; }
        public long? AgencyId { get; set; }

        [ForeignKey("AgencyId")]
        public virtual AgencyRegistrationInfo AgencyInfo { get; set; }

        public long? ClassId { get; set; }
        public virtual ClassInfo Class { get; set; }
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
    }
}