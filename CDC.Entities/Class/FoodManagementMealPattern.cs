﻿using CDC.Entities.Class;
using CDC.Entities.Tools.FoodManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Class
{
    public class FoodManagementMealPattern : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }
        [Column(Order = 2)]
        public long AgencyId { get; set; }
        public long ClassInfoId { get; set; }
        public virtual ClassInfo ClassInfo { get; set; }
        public string AgeGroup { get; set; }
        public long AgeGroupId { get; set; }

        //public virtual MealType MealType { get; set; }
        //public long MealTypeId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }        
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }       
        public virtual ICollection<FoodManagementMealPatternItems> FoodManagementMealPatternItemsInfos { get; set; }
}
}
