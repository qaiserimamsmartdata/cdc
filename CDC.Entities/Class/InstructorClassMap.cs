﻿using System;
using CDC.Entities.Staff;

namespace CDC.Entities.Class
{
    public class InstructorClassMap : IEntityBase
    {
        public long? StaffId { get; set; }
        public long? ClassId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual StaffInfo Staff { get; set; }
        public virtual ClassInfo Class { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}