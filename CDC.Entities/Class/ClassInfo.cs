﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Tools.Category;
using CDC.Entities.Tools.Class;
using CDC.Entities.Tools.Student;

namespace CDC.Entities.Class
{
    /// <summary>
    ///     The class info class
    /// </summary>
    /// <seealso cref="IEntityBase" />
    public class ClassInfo : IEntityBase
    {
        [Column(Order = 2)]
        public long AgencyId { get; set; }

        /// <summary>
        ///     Gets or sets the name of the class.
        /// </summary>
        /// <value>
        ///     The name of the class.
        /// </value>
        public string ClassName { get; set; }

        /// <summary>
        ///     Gets or sets the category identifier.
        /// </summary>
        /// <value>
        ///     The category identifier.
        /// </value>
        [ForeignKey("Category")]
        public long CategoryId { get; set; }

      
        /// <summary>
        ///     Gets or sets the status identifier.
        /// </summary>
        /// <value>
        ///     The status identifier.
        /// </value>
        /// 
        public Nullable<long> ClassStatusId { get; set; }

        public virtual ClassStatus ClassStatus { get; set; }

        /// <summary>
        ///     Gets or sets the enroll capacity.
        /// </summary>
        /// <value>
        ///     The enroll capacity.
        /// </value>
        public int EnrollCapacity { get; set; }

        [ForeignKey("Session")]
        public Nullable<long> SessionId { get; set; }

        [ForeignKey("Room")]
        public Nullable<long> RoomId { get; set; }

        public int MinAgeFrom { get; set; }
        public int MinAgeTo { get; set; }
        public int MaxAgeFrom { get; set; }
        public int MaxAgeTo { get; set; }
        public DateTime? AgeCutOffDate { get; set; }
        public DateTime? RegistrationStartDate { get; set; }
        public DateTime? ClassStartDate { get; set; }
        public DateTime? ClassEndDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string Description { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public bool Sun { get; set; }

        [ForeignKey("AgencyId")]
        public virtual AgencyRegistrationInfo AgencyInfo { get; set; }

        public virtual Session Session { get; set; }

        public virtual Room Room { get; set; }
        public virtual Category Category { get; set; }

        [Column(Order = 1)]
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
        public bool? OnGoing { get; set; }

        public decimal? Fees { get; set; }
        [ForeignKey("FeeType")]
        public long? FeeTypeId { get; set; }
        public virtual FeeType FeeType { get; set; }

        public long? AgencyLocationInfoId { get; set; }
        public virtual AgencyLocationInfo AgencyLocationInfo { get; set; }
       
    }
}