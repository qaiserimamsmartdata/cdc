﻿using CDC.Entities.Class;
using CDC.Entities.Tools.FoodManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Class
{
    public class FoodManagementMealPatternItems : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }
      
        public long FoodManagementMealPatternId { get; set; }
        //public virtual FoodManagementMealPattern FoodManagementMealPattern { get; set; }
        public long FoodManagementMasterId { get; set; }
        public virtual FoodManagementMaster FoodManagementMaster { get; set; }
        public bool? IsDeleted { get; set; }       
    }
}
