﻿using System;
using CDC.Entities.Family;

namespace CDC.Entities.Class
{
    public class WaitListMap : IEntityBase
    {
        public long ClassId { get; set; }
        public long StudentId { get; set; }
        public string Notes { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual ClassInfo Class { get; set; }
        public virtual StudentInfo Student { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}