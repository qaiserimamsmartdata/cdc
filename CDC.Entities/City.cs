﻿namespace CDC.Entities
{
    /// <summary>
    ///     The City Class
    /// </summary>
    /// <seealso cref="IEntityBase" />
    public class City : IEntityBase
    {
        public string name { get; set; }

        /// <summary>
        ///     Gets or sets the state identifier.
        /// </summary>
        /// <value>
        ///     The state identifier.
        /// </value>
        public long StateId { get; set; }

        //[ForeignKey("StateId")]
        public virtual State State { get; set; }

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public long ID { get; set; }

        /// <summary>
        ///     Gets or sets the is deleted.
        /// </summary>
        /// <value>
        ///     The is deleted.
        /// </value>
        public bool? IsDeleted { get; set; }

        //public ICollection<AgencyInfo> AgencyInfos { get; set; }
    }
}