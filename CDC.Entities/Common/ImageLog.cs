﻿using System;

namespace CDC.Entities.Common
{
    public class ImageLog : IEntityBase
    {
        public long ID { get; set; }
        public string ImageName { get; set; }
        public string ImageUrl { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
