﻿using System;
using CDC.Entities.Family;
using CDC.Entities.Staff;

namespace CDC.Entities.Common
{
    public class ContactInfoMap : IEntityBase
    {
        public long? FamilyId { get; set; }
        public long? StaffId { get; set; }
        public long? ContactId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual FamilyInfo Family { get; set; }
        public virtual StaffInfo Staff { get; set; }
        public virtual ContactInfo Contact { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}