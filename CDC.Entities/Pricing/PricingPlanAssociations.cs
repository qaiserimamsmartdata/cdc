﻿using CDC.Entities.AgencyRegistration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.Pricing
{
    /// <summary>
    ///     The Pricing Class
    /// </summary>
    /// <seealso cref="IEntityBase" />
    public class PricingPlanAssociations : IEntityBase
    {
        public long ID { get; set; }
        public long PricingPlanId { get; set; }
        public long AssociatedPlanId { get; set; }
        public bool? IsDeleted { get; set; }
    }
}