﻿using CDC.Entities.AgencyRegistration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.Pricing
{
    /// <summary>
    ///     The Pricing Class
    /// </summary>
    /// <seealso cref="IEntityBase" />
    public class PricingPlan : IEntityBase
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int MinNumberOfParticipants { get; set; }
        public int MaxNumberOfParticipants { get; set; }
        public int TimeClockUsers { get; set; }
        public long ID { get; set; }
        public string StripePlanId { get; set; }
        public bool? IsDeleted { get; set; }
        public  ICollection<AgencyRegistrationInfo> AgencyRegistration { get; set; }

        public bool? IsTrial { get; set; }
    }
}