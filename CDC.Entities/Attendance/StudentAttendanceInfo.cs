﻿using System;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.Entities.Tools.Student;
using CDC.Entities.Class;
using CDC.Entities.NewParticipant;
using CDC.Entities.NewParent;

namespace CDC.Entities.Attendance
{
    public class StudentAttendanceInfo : IEntityBase
    {
        public long ID { get; set; }
        public long? StudentId { get; set; }
        public DateTime AttendanceDate { get; set; }
        public TimeSpan? InTime { get; set; }
        public TimeSpan? OutTime { get; set; }
        public long? DropedById { get; set; }
        public long? DropedByOtherId { get; set; }
        public long? PickupById { get; set; }
        public string PickupByOtherName { get; set; }
        public bool? OnLeave { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public string OnLeaveComment { get; set; }
        public bool? DisableOnLeave { get; set; }
        public virtual tblParticipantInfo Student { get; set; }
        public virtual tblParentInfo DropedBy { get; set; }
        public virtual GeneralDroppedBy DropedByOther { get; set; }
        public virtual tblParentInfo PickupBy { get; set; }
        public virtual GeneralDroppedBy PickupByOther { get; set; }
        public long? AgencyId { get; set; }
        public long? StudentScheduleId { get; set; }
        public virtual StudentSchedule StudentSchedule { get; set; }
    }
}