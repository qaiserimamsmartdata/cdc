﻿using System;
using CDC.Entities.Staff;

namespace CDC.Entities.Attendance
{
    public class AttendanceInfo : IEntityBase
    {
        public long? StaffId { get; set; }
        public TimeSpan? InTime { get; set; }
        public TimeSpan? OutTime { get; set; }
        public bool? OnLeave { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual StaffInfo Staff { get; set; }
        public DateTime? AttendanceDate { get; set; }
        public long? AttendanceMarkedBy { get; set; }
        public string Comment { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}