﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Enums;
using CDC.Entities.Family;
using CDC.Entities.DailyStatus;
using CDC.Entities.NewParticipant;

namespace CDC.Entities.DailyStatus
{
    public class DailyStatus : IEntityBase
    {
        
        public DateTime? Date { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string Comments { get; set; }
        public long? DailyStatusCategoryId { get; set; }
        public virtual DailyStatusCategory DailyStatusCategory { get; set; }
        public long? StudentInfoId { get; set; }
        public virtual tblParticipantInfo StudentInfo { get; set; }
        public long? AgencyInfoId { get; set; }
        public virtual AgencyRegistrationInfo AgencyInfo { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
    }
}