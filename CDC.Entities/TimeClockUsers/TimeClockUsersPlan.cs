﻿using CDC.Entities.AgencyRegistration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.TimeClockUsers
{
    /// <summary>
    ///     The Pricing Class
    /// </summary>
    /// <seealso cref="IEntityBase" />
    public class TimeClockUsersPlan : IEntityBase
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int MaxNumberOfTimeClockUsers { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public  ICollection<AgencyRegistrationInfo> AgencyRegistration { get; set; }
        public string StripePlanId { get; set; }
    }
}