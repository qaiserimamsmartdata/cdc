﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.Class;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.Entities.Tools.Student;

namespace CDC.Entities.ParticipantEnrollmentPayment
{
    public class ParticipantEnrollPayment : IEntityBase
    {
        public long? ScheduleId { get; set; }
        [ForeignKey("ScheduleId")]
        public virtual StudentSchedule StudentSchedule { get; set; }
        public DateTime? scheduleDate { get; set; }
        public int PaymentStatus{ get; set; }
        public decimal? Fees { get; set; }
        public string Description { get; set; }
        public decimal? Discount { get; set; }

        [ForeignKey("FeeType")]
        public long? FeeTypeId { get; set; }
        public virtual FeeType FeeType { get; set; }

        public string PaymentMode { get; set; }
        [ForeignKey("ParentInfo")]
        public long? ParentInfoId { get; set; }
        public virtual ParentInfo ParentInfo { get; set; }
        public long AgencyId { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}