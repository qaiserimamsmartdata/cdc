﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.ToDoTask
{
    public class ToDo : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }

        [Column(Order = 2)]
        public long AgencyId { get; set; }

        public string Name { get; set; }

        public DateTime? DueDate { get; set; }
        public string Description { get; set; }      
        public long? PriorityId { get; set;}

        public bool? IsDeleted { get; set; }

        public virtual Priority Priority { get; set; }
    }
}
