﻿using CDC.Entities.Tools.MealServingType;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Tools.FoodManagement
{
    public class FoodManagementMaster : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }
        [Column(Order = 2)]
        public long AgencyId { get; set; }
        public long MealItemId { get; set; }
        public virtual MealItemMaster MealItem { get; set; }
        public int MinAgeFrom { get; set; }
        public int MinAgeTo { get; set; }
        public int MaxAgeFrom { get; set; }
        public int MaxAgeTo { get; set; }
        public virtual MealType MealType { get; set; }
        public long MealTypeId { get; set; }
        public long ServingSizeId { get; set; }
        public long ServingSizeProportionId { get; set; }
        public long ServingSizeTitleId { get; set; }     
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }        
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }       
    }
}
