﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Tools.FoodManagement
{
    public class MealType : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }
        [Column(Order = 2)]
        public string Name { get; set; }
        public long AgencyId { get; set; }
        public bool? IsDeleted { get; set; }       
    }
}
