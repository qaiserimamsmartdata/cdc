﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Tools.Enhancements
{
    public class Enhancement : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }
        [Column(Order = 2)]
        public long AgencyId { get; set; }
        public string Title { get; set; }
        public DateTime? EnhancementDate { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
