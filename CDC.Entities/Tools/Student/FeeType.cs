﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.Tools.Student
{
    public class FeeType : IEntityBase
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public long ID { get; set; }

        public long AgencyId { get; set; }

        public int Days { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the is deleted.
        /// </summary>
        /// <value>
        ///     The is deleted.
        /// </value>
        public bool? IsDeleted { get; set; }
    }
}
