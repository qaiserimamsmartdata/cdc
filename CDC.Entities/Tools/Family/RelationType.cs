﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.Tools.Family
{
    public class RelationType : IEntityBase
    {
        [Column(Order = 2)]
        public long AgencyId { get; set; }

        public string Name { get; set; }

        [Column(Order = 1)]
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
    }
}