﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.Tools.Staff
{
    /// <summary>
    ///     Leave Type (HalfDay,FullDay,Emergency,Medical)
    /// </summary>
    /// <seealso cref="CDC.Entities.IEntityBase" />
    public class LeaveType : IEntityBase
    {
        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; set; }

        [Column(Order = 2)]
        public long AgencyId { get; set; }

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        [Column(Order = 1)]
        public long ID { get; set; }

        /// <summary>
        ///     Gets or sets the is deleted.
        /// </summary>
        /// <value>
        ///     The is deleted.
        /// </value>
        public bool? IsDeleted { get; set; }
    }
}