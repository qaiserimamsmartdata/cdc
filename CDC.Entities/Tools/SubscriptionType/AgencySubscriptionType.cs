﻿using CDC.Entities.AgencyRegistration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.AgencyRegistration
{
    public class AgencySubscriptionType : IEntityBase
    {
        public string TypeName { get; set; }
        public string Description { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}