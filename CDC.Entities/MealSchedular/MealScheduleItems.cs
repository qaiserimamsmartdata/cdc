﻿using CDC.Entities.Tools.FoodManagement;
using CDC.Entities.Tools.MealServingType;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.MealSchedular
{
    public class MealScheduleItemsInfo : IEntityBase
    {
        [Column(Order = 1)]
        public long ID { get; set; }
        public long MealScheduleId { get; set; }
        //public virtual FoodManagementMealPattern FoodManagementMealPattern { get; set; }
        public long FoodManagementMasterId { get; set; }
        public virtual FoodManagementMaster FoodManagementMaster { get; set; }
        public bool? IsDeleted { get; set; }
        public long SizeId { get; set; }
        public virtual MealServeSize Size { get; set; }
        public long QuantityId { get; set; }
        public virtual MealServeQuantity Quantity { get; set; }
        public long TypeId { get; set; }
        public virtual MealServeType Type { get; set; }
    }
}
