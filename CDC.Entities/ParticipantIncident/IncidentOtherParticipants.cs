﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Family;

namespace CDC.Entities.ParticipantIncident
{
    public class IncidentOtherParticipants : IEntityBase
    {
        public long ID { get; set; }
        public long? OtherStudentInfoId { get; set; }
        public virtual StudentInfo OtherStudentInfo { get; set; }
        public long? IncidentId { get; set; }
        //public virtual Incident Incident { get; set; }
        public bool? IsDeleted { get; set; }
    }
}