﻿using CDC.Entities.NewParticipant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.ParticipantIncident
{
   public class IncidentParticipantMapping : IEntityBase
    {
        public long ID { get; set; }
        public long? StudentInfoId { get; set; }
        public virtual tblParticipantInfo StudentInfo { get; set; }
        public long? IncidentId { get; set; }
        //public virtual Incident Incident { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
