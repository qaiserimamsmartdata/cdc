﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Family;
using System.Collections.Generic;
using CDC.Entities.Staff;

namespace CDC.Entities.ParticipantIncident
{
    public class Incident : IEntityBase
    {
        public long ID { get; set; }
        public string ReporterName { get; set; }
        public long? StaffInfoId { get; set; }
     
        public string PhoneNumber { get; set; }
        public DateTime? DateOfReport { get; set; }
        public string ParticipantName { get; set; }
        public DateTime? DateOfAccident { get; set; }
        public TimeSpan? TimeOfAccident { get; set; }
        public DateTime? DateOfNotification { get; set; }
        public long? AgencyRegistrationId { get; set; }
        public string PlaceOfAccident { get; set; }
        public bool? ParentInformed { get; set; }
        public string ParentDescription { get; set; }
        public bool? FirstAidAdministered { get; set; }
        public string OtherTreatment { get; set; }
        public bool? DoctorRequired { get; set; }
        public string DescriptionOfInjury { get; set; }
        public string ActionTaken { get; set; }
        public bool? Status { get; set; }
        public DateTime? DateOfStatus { get; set; }
        public long? ParentId { get; set; }
        public long? FamilyId { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsRead { get; set; }
        public string NatureOfInjury { get; set; }
        public virtual StaffInfo StaffInfo { get; set; }
        public virtual ICollection<IncidentParticipantMapping> IncidentParticipant { get; set; }
        public virtual ICollection<IncidentOtherParticipantMapping> IncidentOtherParticipant { get; set; }
        public virtual ICollection<IncidentParentParticipantMapping> IncidentParentParticipant { get; set; }
    }
}