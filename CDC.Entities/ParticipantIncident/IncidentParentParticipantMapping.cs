﻿using CDC.Entities.NewParent;
using CDC.Entities.NewParticipant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.ParticipantIncident
{
   public class IncidentParentParticipantMapping : IEntityBase
    {
        public long ID { get; set; }
        public long? IncidentId { get; set; }
        public long? ParentInfoId { get; set; }
        public virtual tblParentInfo ParentInfo { get; set; }
        public bool? IsDeleted { get; set; }

    }
}
