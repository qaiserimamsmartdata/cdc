﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Family;
using CDC.Entities.NewParticipant;

namespace CDC.Entities.ParticipantIncident
{
    public class IncidentParticipants : IEntityBase
    {
        public long ID { get; set; }
        public long? StudentInfoId { get; set; }
        public virtual tblParticipantInfo StudentInfo{ get; set; }
        public long? IncidentId { get; set; }
        //public virtual Incident Incident { get; set; }
        public bool? IsDeleted { get; set; }
    }
}