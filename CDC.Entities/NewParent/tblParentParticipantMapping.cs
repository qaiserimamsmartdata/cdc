﻿using CDC.Entities.NewParticipant;
using CDC.Entities.Tools.Family;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.NewParent
{
   public class tblParentParticipantMapping:IEntityBase
    {
        public long ID { get; set; }
        
        public bool? IsDeleted { get; set; }
        //public virtual tblParticipantInfo NewParticipantInfo { get; set; }
        public long? NewParticipantInfoID { get; set; }
        public virtual tblParticipantInfo NewParticipantInfo { get; set; }
        public long? NewParentInfoID { get; set; }        
        public virtual tblParentInfo NewParentInfo { get; set; }
        public long? RelationTypeId { get; set; }
        public virtual RelationType RelationType { get; set; }
        
    }
}
