﻿using CDC.Entities.AgencyRegistration;
using CDC.Entities.Family;
using CDC.Entities.NewParticipant;
using CDC.Entities.Tools.Family;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Entities.NewParent
{
    public class tblParentInfo:IEntityBase
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long CreatedBy { get; set; }
        public virtual AgencyRegistrationInfo Agency { get; set; }
        public virtual tblParticipantInfo Participant { get; set; }
        public string FullName { get; set; }
        public long? AgencyId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual RelationType RelationType { get; set; }
        public long SecurityQuestionId { get; set; }
        public string SecurityQuestionAnswer { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public virtual SecurityQuestions SecurityQuestion { get; set; }
        public long? RelationTypeId { get; set; }
        public string Address { get; set; }
        public string FirstName { get; set; }
        public string  LastName { get; set; }
        public string EmailId { get; set; }
        public string ImagePath { get; set; }
        public string SecurityKey { get; set; }
        public string Mobile { get; set; }
        public long SMSCarrierId { get; set; }
        public virtual SMSCarrier.SMSCarrier SMSCarrier { get; set; }
        public bool? IsEventMailReceived { get; set; }
        public bool IsLoggedFirstTime { get; set; }
        public bool? IsParticipantAttendanceMailReceived { get; set; }
        public string TimeZone { get; set; }
    }

}
