﻿using CDC.Entities.ParticipantEnrollmentPayment;
using CDC.Entities.Schedule;
using CDC.ViewModel.Class;
using CDC.ViewModel.Family;
using CDC.ViewModel.NewParticipant;
using CDC.ViewModel.ParticipantEnrollmentPayment;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Schedule
{
    public class StudentScheduleViewModel
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public long CategoryId { get; set; }

        public long ClassId { get; set; }
        public long RoomId { get; set; }
        public string ImagePath { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? MondayIn { get; set; }
        public TimeSpan? MondayOut { get; set; }
        public TimeSpan? TuesdayIn { get; set; }
        public TimeSpan? TuesdayOut { get; set; }
        public TimeSpan? WednesdayIn { get; set; }
        public TimeSpan? WednesdayOut { get; set; }
        public TimeSpan? ThursdayIn { get; set; }
        public TimeSpan? ThursdayOut { get; set; }
        public TimeSpan? FridayIn { get; set; }
        public TimeSpan? FridayOut { get; set; }
        public TimeSpan? SaturdayIn { get; set; }
        public TimeSpan? SaturdayOut { get; set; }
        public bool? IsDeleted { get; set; }
        public long? StudentId { get; set; }
        public decimal? TutionFees { get; set; }
        public decimal? PaidFees { get; set; }
        public String ClassName { get; set; }
        public tblParticipantInfoViewModel StudentInfo { get; set; }
        public ClassViewModel ClassInfo { get; set; }
        public ICollection<ParticipantEnrollPaymentViewModel> PaymentStatusList { get; set; }
        public long AgencyId { get; set; }
        public int PaymentStatus { get; set; }
        public string ParticipantAge { get; set; }
        public int Count { get; set; }
    }
    public class StudentScheduleViewModelNew
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public long studentId { get; set; }

    }
}