﻿using System;
using CDC.Entities.Enums;

namespace CDC.ViewModel.Student
{
    /// <summary>
    ///     CDC Customer Info
    /// </summary>
    public class StudentViewModel
    {
        public long ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public EnumGender Gender { get; set; }
        public string  GenderName { get; set; }
        public DateTime ? EnrolledDate { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? Dateofbirth { get; set; }
        public string Fathername { get; set; }
        public string Mothername { get; set; }
        public string Email { get; set; }
        public string ParentsMobile { get; set; }
        public string Guardianfirstname { get; set; }
        public string Guardianlastname { get; set; }
        public EnumRelations Relation { get; set; }
        public string Guardianemail { get; set; }
        public string Guardianmobile { get; set; }
        public string Address { get; set; }
        public long Country { get; set; }
        public long State { get; set; }
        public long City { get; set; }
        public long? StudentId { get; set; }
        public string Postalcode { get; set; }
        public EnumClasses ClassID { get; set; }
        public EnumRooms Room { get; set; }
        public DateTime Startdate { get; set; }
        public DateTime Enddate { get; set; }
        public string AdditionalInformation { get; set; }
        public int TotalCount { get; set; }
    }
}