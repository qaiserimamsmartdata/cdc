﻿using CDC.Entities.SMSCarrier;
using System.Collections;
using System.Collections.Generic;

namespace CDC.ViewModel.Common
{
    public class EmailDataViewModel
    {
        public string Subject { get; set; }
        public string Messages { get; set; }
        public string AgencyName { get; set; }
        public List<GuardianDataViewModel> GuardianInfo { get; set; }
        public List<ParticipantDataViewModel> Regards { get; set; }
    }
    public class GuardianDataViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public SMSCarrierViewModel SMSCarrier { get; set; }
    }
    public class ParticipantDataViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        
    }

}
