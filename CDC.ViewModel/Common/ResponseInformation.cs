﻿using System.Collections;
using System.Collections.Generic;

namespace CDC.ViewModel.Common
{
    public class ResponseInformation
    {
        public bool ReturnStatus { get; set; }
        public List<string> ReturnMessage { get; set; }
        public Hashtable ValidationErrors;
        public int TotalPages;
        public long TotalRows;
        public int TotalTimeClockUsers;
        public int TotalEnrolledParticipants;
        public int TotalRequireEnrollParticipants;
        public int MaximumParticipants;
        public int PageSize;
        public long ID;
        public bool IsExist;
        public bool IsEligible;
        public object Content { get; set; }
        public ResponseInformation()
        {
            ID = 0;
            ReturnMessage = new List<string>();
            ReturnStatus = true;
            ValidationErrors = new Hashtable();
            TotalPages = 0;
            TotalPages = 0;
            PageSize = 0;
            IsExist = false;
        }
    }
}
