﻿using System;
using System.Collections.Generic;
using CDC.Entities.Enums;
using CDC.ViewModel.Tools.FoodManagementMaster;
using CDC.ViewModel.Tools.Staff;

namespace CDC.ViewModel.Common
{
    /// <summary>
    ///     CDC Customer Info
    /// </summary>
    public class SearchStudentViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public int status { get; set; }
        public EnumClasses classid { get; set; }
        public string name { get; set; }
    }
    public class SearchClassViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string ClassName { get; set; }
        public int SessionId { get; set; }
        public int StatusId { get; set; }
        public int CategoryId { get; set; }
        public int AgencyId { get; set; }
        public DateTime? ClassStartDate { get; set; }
        public DateTime? ClassEndDate { get; set; }
        public int GenderId { get; set; }
        public string TimeZone { get; set; }
    }
    public class SearchFieldsViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public int status { get; set; }
        public EnumClasses classid { get; set; }
        public string name { get; set; }
        public long AgencyId { get; set; }
        public bool IsTimeClockUser { get; set; }
        public bool? IsFromMealSchedule { get; set; }
        public long AssociatedParticipantInfoId { get; set; }
        

    }
    public class SearchFieldsViewModelForParticipants

    {
        public long? StudentId { get; set; }
        public List<long?> OtherStudentIds { get; set; }
        public DateTime? DateOfAccident { get; set; }
    }
        public class SearchSkillViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string name { get; set; }
    }
    public class SearchLessonPlanViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string name { get; set; }
        public long? ClassId { get; set; }
    }
    public class SearchStaffViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public long AgencyId { get; set; }
        public string StaffName { get; set; }
        public long PositionId { get; set; }
        public int StatusId { get; set; }

    }
    public class SearchParticipantViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public long AgencyId { get; set; }
        public string ParticipantName { get; set; }
        public long PositionId { get; set; }
        public int StatusId { get; set; }
        public long? FamilyId { get; set; }
    }
    public class SearchDailyStatusViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public long AgencyId { get; set; }
        public string ParticipantName { get; set; }
        public long PurposeId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long ParticipantId { get; set; }
        public long FamilyId{ get; set; }
        public string TimeZone { get; set; }
    }
    public class SearchIncidentViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public long AgencyId { get; set; }
        public long FamilyId { get; set; }
        public long StaffInfoId { get; set; }
        public string ParticipantName { get; set; }
        public string TimeZone { get; set; }
    }
    public class SearchMessageViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }
        public long AgencyId { get; set; }

        public string TimeZone { get; set; }



    }
    public class SearchUserViewModel
    {
        public int FamilyId { get; set; }
    }


    public class SearchAgencyViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        //public long AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        //public long PositionId { get; set; }
        //public int StatusId { get; set; }

    }
    public class SearchPaymentHistoryViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        //public long AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string PlanName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        //public long PositionId { get; set; }
        public int AgencyId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string PaymentStatus { get; set; }
        public string TimeZone { get; set; }
    }
    public class SearchParticipantPlanViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        //public long AgencyId { get; set; }
        public string Name { get; set; }

        public long Price { get; set; }
        public long MinNumberOfParticipants { get; set; }
        public long MaxNumberOfParticipants { get; set; }
        public long TimeClockUsers { get; set; }
        //public long PositionId { get; set; }
        //public int StatusId { get; set; }

    }

    public class SearchTimeClockUserPlanViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        //public long AgencyId { get; set; }
        public string Name { get; set; }
        public long Price { get; set; }
        public long MaxNumberOfTimeClockUsers { get; set; }


    }

    public class SearchToDoViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Description { get; set; }
        public long AgencyId { get; set; }
        public string Name { get; set; }
        public long PriorityId { get; set; }
        public DateTime DueDate { get; set; }

    }

    public class SearchStaffSkillViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Description { get; set; }
        public long AgencyId { get; set; }
        public long SkillMasterId { get; set; }
        public long StaffId { get; set; }

    }
    public class SearchHolidayViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Description { get; set; }
        public long AgencyId { get; set; }
        public string Title { get; set; }
        public DateTime HolidayDate { get; set; }

    }
    public class SearchFoodMasterViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Description { get; set; }
        public long AgencyId { get; set; }
        public string ItemName { get; set; }
        public int MinAgeFrom { get; set; }
        public int MinAgeTo { get; set; }
        public int MaxAgeFrom { get; set; }
        public int MaxAgeTo { get; set; }
        public MealTypeViewModel MealType { get; set; }
        public long MealTypeId { get; set; }

        public DateTime HolidayDate { get; set; }

    }
    public class SearchFoodMealPatternViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Description { get; set; }
        public long AgencyId { get; set; }
        public string ItemName { get; set; }
        public string AgeGroup { get; set; }
        public long MealTypeId { get; set; }
    }
    public class SearchEnhancementViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Description { get; set; }
        public long AgencyId { get; set; }
        public string Title { get; set; }
        public DateTime EnhancementDate { get; set; }

    }

    public class SearchPositionViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchModeOfConvenienceViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchGeneralDroppedByViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchGradeLevelListViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchInfoSourceListViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchMembershipTypeListViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchRelationTypeListViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }

    public class SearchCategoryListViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchClassStatusListViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchRoomListViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }

    public class SearchSessionListViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchPriorityViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Name { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchSkillMasterViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public string Skill { get; set; }
        public long? AgencyId { get; set; }
    }
    public class SearchStudentScheduleViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public string filter { get; set; }
        public List<int> SearchFields { get; set; }
        public long? StudentId { get; set; }
        public long? ClassId { get; set; }
        public long? SessionId { get; set; }
        public long? StatusId { get; set; }
        public long? CategoryId { get; set; }
        public long? FamilyId { get; set; }
        public long? AgencyId { get; set; }
        public string ParticipantName { get; set; }
        public bool EnrollStatus { get; set; }
        public bool IsEnrolled { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? PaymentStatus { get; set; }
        //public DateTime? ClassEndDate { get; set; }
    }
    public class SearchLeaveViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public long? AgencyId { get; set; }
        public long? StaffId { get; set; }
        public string StaffName { get; set; }
        public StatusViewModel LeaveStatus { get; set; }
        public long LeaveStatusId { get; set; }
        public long? StatusId { get; set; }
        public DateTime? LeaveStartDate { get; set; }
        public DateTime? leaveEndDate { get; set; }
    }
    public class SearchStaffScheduleViewModel
    {
        public Nullable<long> StaffId { get; set; }
        public string Title { get; set; }
        public string StaffName { get; set; }
        public Nullable<long> DesignationId { get; set; }
        public Nullable<long> ClassId { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public long AgencyId { get; set; }
    }

    public class SearchEventViewModel
    {
        public string Title { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public long AgencyId { get; set; }
    }

    public class SearchTasksViewModel
    {
        public string Title { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public long AgencyId { get; set; }
        public long StaffId { get; set; }
        public long FamilyId { get; set; }
        public string TimeZone { get; set; }
        public string TaskType { get; set; }
        public long ClassId { get; set; }
    }

    public class SearchStaffAttendanceViewModel
    {
        public DateTime? AttendanceDate { get; set; }
        public string StaffName { get; set; }
        public Nullable<long> ClassId { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public long AgencyId { get; set; }
        public string TimeZoneName { get; set; }
        public long StaffId { get; set; }
    }
    public class SearchStudentAttendanceViewModel
    {
        public DateTime? Date { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public string Name { get; set; }
        public Nullable<long> ClassId { get; set; }
        public Nullable<long> AgencyId { get; set; }
        public string PhoneNumber { get; set; }
        public string PinNumber { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }

        public string TimeZoneName { get; set; }
    }
    public class SearchInstructorViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public string InstructorName { get; set; }
        public string DesignationName { get; set; }
        public string Email { get; set; }
        public long ClassId { get; set; }

    }

    public class SearchFamilyViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public List<int> SearchFields { get; set; }
        public long AgencyId { get; set; }
        public string FamilyName { get; set; }
        public long? FamilyId { get; set; }
        public int? StatusId { get; set; }
        public string StudentName { get; set; }
        public string ParentName { get; set; }
        public long StudentId { get; set; }
        public DateTime Date { get; set; }

    }

    public class SearchGradeLevelViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
    }

    public class SearchInfoSourceViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
    }
    public class IDViewModel
    {
        public long ID { get; set; }
        public long StaffId { get; set; }
        public long AgencyId { get; set; }
        public string TimeZone { get; set; }

        public long ClassId { get; set; }
        public long FamilyId { get; set; }
        public long StudentId { get; set; }
        public long PositionId { get; set; }
        public DateTime? Date { get; set; }
        public long DailyStatusCategoryId { get; set; }

        public long StudentInfoId { get; set; }
        public long AgencyInfoId { get; set; }
        public long IncidentId { get; set; }

        public List<long?> participantIds { get; set; }
        public List<long> classIds { get; set; }
    }
    public class SearchAttendanceHistoryViewModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string StudentName { get; set; }
        public Nullable<long> ClassId { get; set; }
        public Nullable<long> AgencyId { get; set; }

        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public string TimeZoneName { get; set; }
        public long FamilyId { get; set; }
    }
    public class SearchStudentAttendanceHistoryViewModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string StudentName { get; set; }
        //public DateTime? AttendanceDate { get; set; }
        public Nullable<long> ClassId { get; set; }
        public Nullable<long> AgencyId { get; set; }
        public Nullable<long> StudentId { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public string order { get; set; }
        public string TimeZoneName { get; set; }
    }

    public class SecurityKeyCheckViewModel
    {
        public long AgencyId { get; set; }
        public long FamilyId { get; set; }
        public string SecurityKey { get; set; }
    }
    public class SearchTimesheetViewModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string TimeZone { get; set; }
        public Nullable<long> StaffId { get; set; }
        public Nullable<long> AgencyId { get; set; }
        public int? limit { get; set; }
        public int? page { get; set; }
        public string order { get; set; }
    }
    public class SearchLocationViewModel
    {
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
        public string LocationName { get; set; }
        public long AgencyId { get; set; }
    }

    public class DropDownViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}