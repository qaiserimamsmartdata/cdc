﻿using CDC.Entities.User;
using System;
using System.Security.Principal;

namespace CDC.ViewModel.Common
{
    /// <summary>
    ///     The Response View Model
    /// </summary>
    public class ResponseViewModel: ResponseInformation
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public long Id { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess { get; set; }

        /// <summary>
        ///     Gets or sets the message.
        /// </summary>
        /// <value>
        ///     The message.
        /// </value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public object Content { get; set; }

        /// <summary>
        /// Gets or sets the is trial.
        /// </summary>
        /// <value>
        /// This is for trial data.
        /// </value>
        /// 
        public bool? IsTrial { get; set; }
        public string EmailIdParent { get; set; }
        public DateTime? TrialStart { get; set; }
        public DateTime? TrialEnd { get; set; }
        public string StripeToken { get; set; }
        public string PlanName { get; set; }
        public decimal? PlanAmount { get; set; }
        public long? PlanId { get; set; }
        public string StripePlanId { get; set; }
        public bool? IsAttendee { get; set; }
        public int? trialDays { get; set; }
        public GenericPrincipal principal { get; set; }
        public Users user { get; set; }
        public string Token { get; set; }

    }
}