﻿using CDC.Entities.AgencyRegistration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.ViewModel.Tools.MealServingType
{
    public class MealServeTypeViewModel
    {
        public string Name { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}