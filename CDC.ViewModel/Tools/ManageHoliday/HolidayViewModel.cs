﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Tools.ManageHoliday
{
    public class HolidayViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public string Title { get; set; }
        public DateTime? HolidayDate { get; set; }
        public bool? Status { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
      
    }
}
