﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Tools.Student
{
    public class FeeTypeViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public int Days { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public string filter { get; set; }
        public string order { get; set; }
    }
}
