﻿namespace CDC.ViewModel.Tools.Staff
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="CDC.Entities.IEntityBase" />
    public class LeaveTypeViewModel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public long ID { get; set; }
        /// <summary>
        /// Gets or sets the is deleted.
        /// </summary>
        /// <value>
        /// The is deleted.
        /// </value>
        public bool? IsDeleted { get; set; }
    }
}
