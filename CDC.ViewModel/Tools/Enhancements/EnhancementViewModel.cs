﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Tools.Enhancements
{
    public class EnhancementViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public string Title { get; set; }
        public DateTime? EnhancementDate { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
