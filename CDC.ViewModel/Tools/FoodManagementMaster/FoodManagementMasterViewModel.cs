﻿using CDC.ViewModel.Tools.MealServingType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Tools.FoodManagementMaster
{
    public class FoodManagementMasterViewModel
    {
        public long ID { get; set; }
        public string ItemName { get; set; }
        public int MinAgeFrom { get; set; }
        public int MinAgeTo { get; set; }
        public int MaxAgeFrom { get; set; }
        public int MaxAgeTo { get; set; }
        public MealTypeViewModel MealType { get; set; }
        public long MealTypeId { get; set; }
        public long ServingSizeId { get; set; }
        public long ServingSizeProportionId { get; set; }
        public long ServingSizeTitleId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
        public long MealItemId { get; set; }
        public MealItemViewModel MealItem { get; set; }
        public long AgencyId { get; set; }
    }
}
