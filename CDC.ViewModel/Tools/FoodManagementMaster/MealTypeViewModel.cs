﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Tools.FoodManagementMaster
{
    public class MealTypeViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public long AgencyId { get; set; }
        public bool? IsDeleted { get; set; }

    }
}
