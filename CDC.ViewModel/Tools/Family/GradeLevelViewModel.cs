﻿namespace CDC.ViewModel.Tools.Family
{
    public class GradeLevelViewModel
    {
        //private static  int SRNo = 0;
        //private int myId = 0;
        //public int MyId
        //{
        //    get { return myId; }
        //}

        //public GradeLevelViewModel()

        //{
        //    SRNo++;
        //    this.myId = SRNo;
        //}
        public long ID { get; set; }

        public long AgencyId { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
