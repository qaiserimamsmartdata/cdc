﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Tools.Family
{
    public class SecurityQuestionsViewModel
    {
        public long ID { get; set; }
        public bool? IsDeleled { get; set; }
        public string SecurityQuestion { get; set; }
    }
}
