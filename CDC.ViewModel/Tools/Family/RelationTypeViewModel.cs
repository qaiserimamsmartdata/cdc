﻿namespace CDC.ViewModel.Tools.Family
{
    public class RelationTypeViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
