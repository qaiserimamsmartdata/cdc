﻿namespace CDC.ViewModel.Tools.Category
{
    public class CategoryViewModel
    {
        public string Name { get; set; }
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
        public long AgencyId { get; set; }
       
    }
}