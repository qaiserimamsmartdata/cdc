﻿using CDC.ViewModel.Class;
using CDC.ViewModel.DailyStatus;
using CDC.ViewModel.Family;
using CDC.ViewModel.NewParent;
using CDC.ViewModel.NewParticipant;
using CDC.ViewModel.Schedule;
using CDC.ViewModel.Tools.Student;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Attendance
{
    public class StudentAttendanceViewModel
    {
        public StudentAttendanceViewModel()
        {
            DSR_eat = new DSR_eat();
        }
        public long ID { get; set; }
        public Nullable<long> StudentScheduleId { get; set; }
        public System.DateTime AttendanceDate { get; set; }
        public long familyId { get; set; }
        public string familyEmail { get; set; }
        public  DSR_eat DSR_eat { get; set; }
        public DSR_sleep DSR_sleep { get; set; }
        public DSR_toilet DSR_toilet { get; set; }
        public Nullable<TimeSpan> InTime { get; set; }
        public Nullable<TimeSpan> OutTime { get; set; }
        public long? DropedById { get; set; }
        public string DropedByName { get; set; }
        public bool SignInChecked { get; set; }
        public bool SignOutChecked { get; set; }
        public long? DropedByOtherId { get; set; }
        public string DropedByOtherName { get; set; }
        public long? PickupById { get; set; }
        public string PickupByName { get; set; }
        public long? PickupByOtherId { get; set; }
        public string PickupByOtherName { get; set; }
        public bool? OnLeave { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        //public virtual StudentScheduleViewModel StudentSchedule { get; set; }
        public DateTime? Date { get; set; }
        public Nullable<long> AttendanceMarkedId { get; set; }
        public string AttendanceMarkedBy { get; set; }
        public Nullable<long> StudentId { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public string RoomName { get; set; }
        public string GenderName { get; set; }
        public string ParentName { get; set; }
        public string SecurityKey { get; set; }
        public string DOB { get; set; }
        public string ImagePath { get; set; }
        public string GuradianEmail { get; set; }
        public string GuradianSMSCarrier { get; set; }
        public string GuradianPhone { get; set; }
        public long? AgencyId { get; set; }
        public string OnLeaveComment { get; set; }
        public string CheckSecurityKey { get; set; }
        public bool? DisableOnLeave { get; set; } = false;
        public StudentScheduleViewModel StudentSchedule { get; set; }
        public tblParticipantInfoViewModel Student { get; set; }
        public tblParentInfoViewModel DropedBy { get; set; }
        public GeneralDroppedByViewModel DropedByOther { get; set; }
        public tblParentInfoViewModel PickupBy { get; set; }
        public GeneralDroppedByViewModel PickupByOther { get; set; }

        public ClassViewModel ClassInfo { get; set; }

        public bool IsOthers { get; set; }
        public bool IsParents { get; set; }

        public string TimeZone { get; set; }

        public int TodaysIncidenceCount { get; set; }

        public string DropByRelationName { get; set; }
        public string PickedByRelationName { get; set; }


        //for Primary Parent
        public string PrimaryDropByRelationName { get; set; }
        public string PrimaryPickedByRelationName { get; set; }
        public string PrimaryGuradianEmail { get; set; }
        public string PrimaryGuradianSMSCarrier { get; set; }
        public string PrimaryGuradianPhone { get; set; }
        public string PrimaryParentName { get; set; }
        public bool? IsParticipantAttendanceMailReceived { get; set; }
        

        //public ParentInfoViewModel DropedBy { get; set; }
        //public ParentInfoViewModel PickupBy { get; set; }
        ///added for new attendance screen
        public List<tblParentInfoViewModel> ParentList { get; set; }
    }
}
