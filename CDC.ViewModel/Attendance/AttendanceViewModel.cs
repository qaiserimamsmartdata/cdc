﻿using CDC.Entities;
using CDC.Entities.Enums;
using CDC.Entities.Staff;
using CDC.Entities.Tools.Staff;
using CDC.ViewModel.Staff;
using System;

namespace CDC.ViewModel.Attendance
{
    public class AttendanceViewModel
    {
        public long ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<long> StaffId { get; set; }
        public Nullable<long> StaffScheduleId { get; set; }
        public Nullable<TimeSpan> InTime { get; set; }
        public Nullable<TimeSpan> OutTime { get; set; }
        public string PhoneNumber { get; set; }


       // public EnumGender Gender { get; set; }
        public virtual Position Position { get; set; }
      //  public DateTime? DateOfBirth { get; set; }
      //  public string ImagePath { get; set; }
        public long? PositionId { get; set; }
        public long? StatusId { get; set; }
      //  public DateTime? DateHired { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long TotalCnt { get; set; }
        public long UserId { get; set; }
        public long? CityId { get; set; }

        public string CityName { get; set; }
        public virtual City City { get; set; }
        public bool? IsDeleted { get; set; }

        public string Apartment { get; set; }
        public long? AgencyLocationInfoId { get; set; }
        public bool? IsTimeClockUser { get; set; }

        public  string FullName { get; set; }

        public string PositionName { get; set; }
        public string ImagePath { get; set; }
        public bool? OnLeave { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; } = DateTime.UtcNow;
        public Nullable<long> ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
      //  public bool IsDeleted { get; set; }
        public DateTime? AttendanceDate { get; set; }
        public Nullable<long> AttendanceMarkedId { get; set; }

        public string PostalCode { get; set; }

        public string DateOfBirthString { get; set; }
        public string DateHiredString { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string AttendanceMarkedBy { get; set; }
        public string StaffName { get; set; }
        public string ClassName { get; set; }
        public string RoomName { get; set; }
        public string GenderName { get; set; }
       public string DOB { get; set; }
        public string StringCreatedDate { get; set; }
        public string Createdday { get; set; }
        public string Comment { get; set; }
        public string TimeZone { get; set; }
        public StaffViewModel Staff { get; set; }

    }
}
