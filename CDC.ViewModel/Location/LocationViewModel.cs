﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Location
{
    public class LocationViewModel
    {
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string LocationName { get; set; }
        public string Description { get; set; }
        public string LocationEmailId { get; set; }
        public string LocationPhoneNumber { get; set; }
        public string Address { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public long? AgencyRegistrationId { get; set; }
        public long ID { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
