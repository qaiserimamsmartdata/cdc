﻿namespace CDC.ViewModel
{
    public class ProfilePicViewModel
    {
        public string AttachmentPath { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string ThumbnailFileName { get; set; }
    }
}