﻿using CDC.Entities.Class;
using CDC.ViewModel.Tools.FoodManagementMaster;

using CDC.ViewModel.Tools.MealServingType;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.MealSchedular
{
    public class MealScheduleItemsInfoViewModel
    {
        public long ID { get; set; }
        public long MealScheduleId { get; set; }      
        public long FoodManagementMasterId { get; set; }
        public FoodManagementMasterViewModel FoodManagementMaster { get; set; }
        public bool? IsDeleted { get; set; }
        public bool IsChecked { get; set; }
        public long SizeId { get; set; }
        public long QuantityId { get; set; }
        public long TypeId { get; set; }

        public string ItemName { get; set; }
        public  MealServeSizeViewModel Size { get; set; }
        public MealServeQuantityViewModel Quantity { get; set; }
        public MealServeTypeViewModel Type { get; set; }


    }
}
