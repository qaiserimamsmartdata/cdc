﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Newtonsoft.Json.Linq;
using CDC.ViewModel.Class;

namespace CDC.ViewModel.MealSchedular
{
    public class MealSchedularViewModel
    {
        public long MealSheduleID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        private DateTime start;
        public DateTime Start
        {
            get
            {
                return start;
            }
            set
            {
                start = value.ToUniversalTime();
            }
        }

        public string StartTimezone { get; set; }

        private DateTime end;
        public DateTime End
        {
            get
            {
                return end;
            }
            set
            {
                end = value.ToUniversalTime();
            }
        }

        public string EndTimezone { get; set; }

        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }
        public int? OwnerID { get; set; }
        public long AgencyId { get; set; }
        public int? StaffId { get; set; }
        public int? FamilyId { get; set; }
        public long ClassInfoId { get; set; }
        public int? RoomId { get; set; }
        public int? LeaveId { get; set; }
        public string TimeZone { get; set; }
        public ClassViewModel ClassInfo { get; set; }
        public ICollection<MealScheduleItemsInfoViewModel> MealScheduleItemsInfos { get; set; }
        public List<MealScheduleItemsInfoViewModel> ItemList { get; set; }
        //public int? RoomID { get; set; }
    }
    public class TasksViewModelCopy 
    {
        public long MealSheduleID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        private DateTime start;
        public DateTime Start
        {
            get
            {
                return start;
            }
            set
            {
                //start = value.ToUniversalTime();
                start = value;
            }
        }

        public string StartTimezone { get; set; }

        private DateTime end;
        public DateTime End
        {
            get
            {
                return end;
            }
            set
            {
                //end = value.ToUniversalTime();
                end = value;
            }
        }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }
        public int? OwnerID { get; set; }
        public long AgencyId { get; set; }
        public int? StaffId { get; set; }
        public int? FamilyId { get; set; }
        public long ClassInfoId { get; set; }
        public int? RoomId { get; set; }
        public int? LeaveId { get; set; }

        public string  StaffName { get; set; }
        public string ClassName { get; set; }
        public string Size { get; set; }
        public string Quantity { get; set; }
        public string Type { get; set; }
        public ICollection<MealScheduleItemsInfoViewModel> MealScheduleItemsInfos { get; set; }
        //public int? RoomID { get; set; }
    }

    public class RootObject
    {
        public string models { get; set; }
        public string data { get; set; }
    }

    public class NewRootObject
    {
        public string agency { get; set; }
    }

}
