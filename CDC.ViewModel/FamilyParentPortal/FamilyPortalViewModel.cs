﻿using CDC.Entities.Family;
using CDC.Entities.SMSCarrier;
using CDC.ViewModel.Staff;
using CDC.ViewModel.Tools.Family;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.FamilyPortal
{
    public class FamilyPortalViewModel
    {
        public string PortalEmail { get; set; }
        public long RoleId { get; set; }
        public string PortalPassword { get; set; }
    }

}
