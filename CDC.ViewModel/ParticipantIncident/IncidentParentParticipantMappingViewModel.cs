﻿using CDC.Entities.NewParent;
using CDC.Entities.NewParticipant;
using CDC.ViewModel.NewParent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.ParticipantIncident
{
   public class IncidentParentParticipantMappingViewModel
    {
        public long ID { get; set; }
        public long? IncidentId { get; set; }
        public long? ParentInfoId { get; set; }
        public virtual tblParentInfoViewModel ParentInfo { get; set; }
        public bool? IsDeleted { get; set; }

    }
}
