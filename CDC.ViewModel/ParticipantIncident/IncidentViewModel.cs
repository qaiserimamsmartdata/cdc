﻿using System;
using CDC.Entities.Class;
using CDC.Entities.Family;
using CDC.ViewModel.Schedule;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Family;
using CDC.ViewModel.Staff;
using System.Collections.Generic;

namespace CDC.ViewModel.ParticipantIncident
{
    public class IncidentViewModel
    {
        public long ID { get; set; }
        public string ReporterName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfReport { get; set; }
        public DateTime? DateOfAccident { get; set; }
        public TimeSpan? TimeOfAccident { get; set; }
        public DateTime? DateOfNotification { get; set; }
        public string EmailId { get; set; }
        public bool? ParentInformed { get; set; }

        public long? AgencyRegistrationId { get; set; }
        public long? ParentId { get; set; }
        public long? FamilyId { get; set; }
        public AgencyRegistrationViewModel AgencyRegistration { get; set; }

        public ICollection<IncidentParticipantViewModel> IncidentParticipant { get; set; }
        public ICollection<IncidentOtherParticipantViewModel> IncidentOtherParticipant { get; set; }
        public ICollection<IncidentParentParticipantMappingViewModel> IncidentParentParticipant { get; set; }
        

        public long? StaffInfoId { get; set; }
        public StaffViewModel StaffInfo { get; set; }
        public string PlaceOfAccident { get; set; }

        public bool? FirstAidAdministered { get; set; }
        public string OtherTreatment { get; set; }
        public bool? DoctorRequired { get; set; }
        public string DescriptionOfInjury { get; set; }
        public string ActionTaken { get; set; }
        public bool? Status { get; set; }
        public DateTime? DateOfStatus { get; set; }
        public bool? IsDeleted { get; set; }
        //For Parent Portal View Purpose
        public string StaffName { get; set; }
        public string ParticipantName { get; set; }
        public string AgeOfChild { get; set; }
        public string ParentName { get; set; }
        public string NatureOfInjury { get; set; }
        public string ParentDescription { get; set; }
        public string OtherParticipants { get; set; }
        public bool? IsRead { get; set; }
        public List<IncidentParticipantViewModel> ListStudentInfoIds { get; set; }
        public List<IncidentOtherParticipantViewModel> ListOtherStudentInfoIds { get; set; }
        public List<IncidentParentParticipantMappingViewModel> ListParentInfoIds { get; set; }
        public string TimeZone { get; set; }

    }
}