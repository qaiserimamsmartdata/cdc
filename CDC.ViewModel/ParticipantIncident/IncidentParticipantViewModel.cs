﻿using System;
using CDC.Entities.Class;
using CDC.Entities.Family;
using CDC.ViewModel.Schedule;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Family;
using CDC.ViewModel.Staff;
using CDC.ViewModel.NewParticipant;

namespace CDC.ViewModel.ParticipantIncident
{
    public class IncidentParticipantViewModel
    {
        public long ID { get; set; }
        public long? studentId { get; set; }
        public tblParticipantInfoViewModel StudentInfo { get; set; }
        public long? IncidentId { get; set; }
        //public IncidentViewModel Incident { get; set; }
        public bool? IsDeleted { get; set; }
    }
}