﻿using System;
using CDC.Entities.Class;
using CDC.Entities.Family;
using CDC.ViewModel.Schedule;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Family;
using CDC.ViewModel.Staff;
using System.Collections.Generic;

namespace CDC.ViewModel.ParticipantIncident
{
    public class IncidentIdViewModel
    {
        public long IncidentId { get; set; }
        public bool IsRead { get; set; }

    }
}