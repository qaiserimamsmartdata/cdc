﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.SuperAdmin
{
    public class AgencyPaymentViewModel
    {
        public long AgencyRegistrationId { get; set; }
        public long ID { get; set; }
        public int AgencyId { get; set; }
        public bool IsCreditCash { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public int CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        public string CardBillingAddress { get; set; }
        public Nullable<long> CardCountryId { get; set; } = 0;
        public Nullable<long> CardStateId { get; set; } = 0;
        public Nullable<long> CardCityId { get; set; } = 0;
        public string CardCityName { get; set; }
        public string CardPostalCode { get; set; }
        public bool Acceptance { get; set; }
        public long RoleId { get; set; }
        public long UserId { get; set; }
        public string CVV { get; set; }
        public string RoleName { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public string StripeUserId { get; set; }
    }
}
