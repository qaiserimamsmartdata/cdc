﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.SuperAdmin
{
    class TimeClockUsersPlanViewModel
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int MinNumberOfParticipants { get; set; }
        public int MaxNumberOfParticipants { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }

    }
}
