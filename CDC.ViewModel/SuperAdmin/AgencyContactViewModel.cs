﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.SuperAdmin
{
    public class AgencyContactViewModel
    {
        public string Address { get; set; }
        public Nullable<long> CountryId { get; set; } = 0;
        public Nullable<long> StateId { get; set; } = 0;
        public Nullable<long> CityId { get; set; } = 0;
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string CityName { get; set; }
    }
}
