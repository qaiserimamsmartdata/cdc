﻿
using CDC.Entities.Pricing;
using CDC.Entities.TimeClockUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.SuperAdmin
{
    public class AgencyListViewModel
    {
        public string AgencyName { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsExistingAccount { get; set; }
        public bool IsLoggedFirstTime { get; set; }
        public long UserId { get; set; }
        public long PricingPlanId { get; set; }
        public long? TimeClockUsersPlanId { get; set; }
        public List<AgencyContactViewModel> AgencyContactInfos { get; set; }
        public List<AgencyBillingViewModel> AgencyBillingInfos { get; set; }
        public List<AgencyPaymentViewModel> AgencyPaymentInfos { get; set; }
        public List<FamilyViewModel> Family { get; set; }
        public PricingPlan PricingPlan { get; set; }
        public TimeClockUsersPlan TimeClockUsersPlan { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public int TotalTimeClockUsers { get; set; }
        public int TotalRequireEnrollParticipants { get; set; }
    }
}
