﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.SuperAdmin
{
    public class AgencyBillingViewModel
    {
        public string AuthorizedBillingAccountname { get; set; }
        public string BillingEmail { get; set; }
    }
}
