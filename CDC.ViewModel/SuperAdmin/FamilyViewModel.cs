﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.SuperAdmin
{
    public class FamilyViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public string FamilyName { get; set; }
        public string SecurityKey { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
