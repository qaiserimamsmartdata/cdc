﻿namespace CDC.ViewModel
{
    public class StateViewModel
    {
        public long ID { get; set; }
        public long CountryId { get; set; }
        public string StateName { get; set; }
        public bool? IsDeleted { get; set; }

        public string CountryName { get; set; }
    }
}