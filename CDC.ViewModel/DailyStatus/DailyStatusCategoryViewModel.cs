﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Enums;
using CDC.Entities.Family;

namespace CDC.ViewModel.DailyStatus
{
    public class DailyStatusCategoryViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
    }
}