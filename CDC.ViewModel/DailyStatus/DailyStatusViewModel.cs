﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Enums;
using CDC.Entities.Family;
using CDC.ViewModel.Family;
using CDC.ViewModel.AgencyRegistration;
using System.Collections.Generic;
using CDC.ViewModel.NewParticipant;

namespace CDC.ViewModel.DailyStatus
{
    public class DailyStatusViewModel
    {
        
        public DateTime? Date { get; set; }
        public List<DSR_eat> DSR_eat { get; set; }
        public List<DSR_sleep> DSR_sleep { get; set; }
        public List<DSR_toilet> DSR_toilet { get; set; }
        public List<DSR_Comment> DSR_Comment { get; set; }
        public TimeSpan? StartTime { get; set; }
        public string  ParticipantName { get; set; }
        public long StudentId { get; set; }
        public string familyEmail { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string Comments { get; set; }
        public long familyId { get; set; }
        public long? DailyStatusCategoryId { get; set; }
        public DailyStatusCategoryViewModel DailyStatusCategory { get; set; }
        public long? StudentInfoId { get; set; }
        public tblParticipantInfoViewModel StudentInfo { get; set; }
        public long? AgencyInfoId { get; set; }
        public AgencyRegistrationViewModel AgencyInfo { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public string ParticipantAge { get; set; }
        public string TimeZone { get; set; }
    }
}