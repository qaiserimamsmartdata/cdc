﻿using System;
using CDC.Entities.Class;
using CDC.Entities.Family;
using CDC.ViewModel.Schedule;

namespace CDC.ViewModel.ParticipantEnrollmentPayment
{
    public class ParticipantEnrollPaymentViewModel 
    {
        public long? ScheduleId { get; set; }
        public int PaymentStatus{ get; set; }
        public long AgencyId { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? scheduleDate { get; set; }

        public decimal? Fees { get; set; }
        public string Description { get; set; }
        public decimal? Discount { get; set; }

        public long? FeeTypeId { get; set; }
        public string PaymentMode { get; set; }

        public long? ParentInfoId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}