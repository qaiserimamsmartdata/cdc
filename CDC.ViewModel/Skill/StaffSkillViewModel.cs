﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Skill
{
    public class StaffSkillViewModel
    {

        public long ID { get; set; }
        public long AgencyId { get; set; }
        public long StaffId { get; set; }
        public long SkillMasterId { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
        public SkillMasterViewModel SkillMaster { get; set; }
    }
}
