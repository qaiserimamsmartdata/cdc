﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Skill
{
    public class SkillMasterViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public string Skill { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
