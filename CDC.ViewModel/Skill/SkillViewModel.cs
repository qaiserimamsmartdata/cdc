﻿namespace CDC.ViewModel.Skill
{
    public class SkillViewModel
    {
        public string Skill { get; set; }

        public string SubSkill { get; set; }

        public string Details { get; set; }

        public long DaysRequired { get; set; }

        public long ClassesRequired { get; set; }
        public long AgencyId { get; set; }


        public long CategoryId { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
    }
}