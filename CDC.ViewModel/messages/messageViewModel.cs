﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CDC.ViewModel.messages
{
    public class messageViewModel
    {
        public string id { get; set; }

        public string subject { get; set; }
        public bool unread { get; set; }
        public string Host { get; set; }
        public DateTime date { get; set; }
        public string content { get; set; }


        //public string[] content1
        //{
        //    get
        //    {
                
        //        var list = new List<string>();
        //        var pattern = "(<p>|</p>)";
        //        var isintag = false;
        //        var intagvalue = string.Empty;

        //        foreach (var substr in Regex.Split(content, pattern))
        //        {
        //            if (substr.Equals("<p>"))
        //            {
        //                isintag = true;
        //                continue;
        //            }
        //            else if (substr.Equals("</p>"))
        //            {
        //                isintag = false;
        //                list.Add(string.Format("<p>{0}</p>", intagvalue));
        //                continue;
        //            }

        //            if (isintag)
        //            {
        //                intagvalue = substr;
        //                continue;
        //            }

        //            list.Add(substr);

        //        }
        //        return list.ToArray();
        //    }
        //}


        public ICollection<toViewModel> to { get; set; }
        public ICollection<ccViewModel> cc { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }

        public List<fromViewModel> from1 { get; set; }
        public fromViewModel from
        {
            get
            {
                fromViewModel model = new fromViewModel();
                if (from1 != null && from1.ToList().Count > 0)
                {
                    model = from1.ToList().Select(x => x).FirstOrDefault();
                }
                return model;
            }
            set
            {

            }
        }

    }


    //public class newmessageViewModel
    //{
    //    public string id { get; set; }
    //    public string subject { get; set; }
    //    public bool unread { get; set; }
    //    public DateTime date { get; set; }
    //    public string content { get; set; }

    //}
}

