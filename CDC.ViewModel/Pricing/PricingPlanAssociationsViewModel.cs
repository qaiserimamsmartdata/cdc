﻿using CDC.Entities.AgencyRegistration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDC.Entities.Pricing
{
    
    public class PricingPlanAssociationsViewModel
    {
        public long ID { get; set; }
        public long PricingPlanId { get; set; }
        public long AssociatedPlanId { get; set; }
        public bool? IsDeleted { get; set; }
    }
}