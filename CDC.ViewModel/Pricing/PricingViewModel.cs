﻿namespace CDC.Entities.Pricing
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="CDC.Entities.IEntityBase" />
    public class PricingPlanViewModel : IEntityBase
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int MinNumberOfParticipants { get; set; }
        public int MaxNumberOfParticipants { get; set; }
        public int TimeClockUsers { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }

        public string StripePlanId { get; set; }
        public bool? IsTrial { get; set; }
        public bool? IsFromUpdate { get; set; }
        //public long AssociationPlanId { get; set; }

    }
}