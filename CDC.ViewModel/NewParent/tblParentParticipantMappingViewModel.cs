﻿using CDC.ViewModel.NewParticipant;
using CDC.ViewModel.Tools.Family;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.NewParent
{
   public class tblParentParticipantMappingViewModel
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public tblParticipantInfoViewModel NewParticipantInfo { get; set; }
        public long? NewParticipantInfoID { get; set; }
        public tblParentInfoViewModel NewParentInfo { get; set; }   
        public long? NewParentInfoID { get; set; }
        public RelationTypeViewModel RelationType { get; set; }
        public long? RelationTypeId { get; set; }
    }
}
