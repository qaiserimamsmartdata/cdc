﻿using CDC.Entities.AgencyRegistration;
using CDC.Entities.Family;
using CDC.Entities.SMSCarrier;
using CDC.Entities.Tools.Family;
using CDC.ViewModel.AgencyRegistration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.NewParent
{
   public class tblParentInfoViewModel
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long CreatedBy { get; set; }
        public AgencyRegistrationViewModel Agency { get; set; }
        public string FullName { get; set; }
        public Nullable<long> CountryId { get; set; } = 0;
        public Nullable<long> StateId { get; set; } = 0;
        public string PostalCode { get; set; }
        public string CityName { get; set; }
        public long? AgencyId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual RelationType RelationType { get; set; }
        public long? RelationTypeId { get; set; }
        public string Address { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public SMSCarrierViewModel SMSCarrier { get; set; }
        public string SecurityQuestionAnswer { get; set; }
        public virtual SecurityQuestions SecurityQuestion { get; set; }
        public long SecurityQuestionId { get; set; }
        public long SMSCarrierId { get; set; }
        public string SMSCarrierEmailAddress { get; set; }
        public string EmailId { get; set; }
        public string ImagePath { get; set; }
        public string SecurityKey { get; set; }
        public string Mobile { get; set; }
        public bool? IsEventMailReceived { get; set; }
        public bool IsLoggedFirstTime { get; set; }
        public bool? IsParticipantAttendanceMailReceived { get; set; }
        public string RelationName { get; set; }
        public long? RoleId { get; set; }
        public long? UserId { get; set; }
        public string RoleName { get; set; }
        public string TimeZone { get; set; }
    }
}
