﻿namespace CDC.Entities.TimeClockUsers
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="CDC.Entities.IEntityBase" />
    public class TimeClockUsersViewModel : IEntityBase
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int MaxNumberOfTimeClockUsers { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public string StripePlanId { get; set; }
    }
}