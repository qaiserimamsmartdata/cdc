﻿namespace CDC.ViewModel
{
    public class CityViewModel
    {
        public long ID { get; set; }
        public long StateId { get; set; }
        public string CityName { get; set; }
        public bool? IsDeleted { get; set; }

        public string StateName { get; set; }
        public string CountryName { get; set; }
    }
}