﻿using System.Collections.Generic;

namespace CDC.ViewModel
{
    public class CustomerGridViewModel
    {
        public List<CustomerViewModel> customerList { get; set; }
        public int totalCount { get; set; }
    }
}
