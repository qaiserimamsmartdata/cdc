﻿using CDC.ViewModel.NewAssociation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.NewParticipant
{
   public class tblParticipantAssociatedParticipantViewModel
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? NewParticipantInfoId { get; set; }
        public tblParticipantInfoViewModel NewParticipantInfo { get; set; }
        public long? AssociatedParticipantInfoId { get; set; }
        public tblParticipantInfoViewModel AssociatedParticipantInfo { get; set; }
        public long? AssociationTypeMasterID { get; set; }
        public tblParticipantAssociationMasterViewModel AssociationTypeMaster { get; set; }
    }
}
