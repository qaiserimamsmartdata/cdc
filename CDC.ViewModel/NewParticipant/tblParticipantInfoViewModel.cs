﻿using CDC.Entities.AgencyRegistration;
using CDC.Entities.Tools.Family;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.FamilyPortal;
using CDC.ViewModel.NewParent;
using CDC.ViewModel.Tools.Family;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.NewParticipant
{
    public class tblParticipantInfoViewModel
    {
        public AgencyRegistrationViewModel AgencyRegistrationInfo { get; set; }
        public int Gender { get; set; }
        public string ImagePath { get; set; }
        public string Radio { get; set; }
        public string RadioParent { get; set; }
        public string FullName { get; set; }
        public long AgencyId { get; set; }
        public string SchoolName { get; set; }
        public string Transportation { get; set; }
        public string Disabilities { get; set; }
        public string Allergies { get; set; }
        public string Medications { get; set; }
        public string PrimaryDoctor { get; set; }
        public string Description { get; set; }
        public long? GradeLevelId { get; set; }
        public string InsuranceCarrier { get; set; }
        public string InsurancePolicyNumber { get; set; }
        public string PreferredHospitalName { get; set; }
        public string PreferredHospitalAddress { get; set; }
        public string FamilyDoctorName { get; set; }
        public string FamilyDoctorPhoneNumber { get; set; }
        public GradeLevelViewModel GradeLevel { get; set; }
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public Nullable<long> CountryId { get; set; } = 0;
        public Nullable<long> StateId { get; set; } = 0;
        public string PostalCode { get; set; }
        public string CityName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long ParticipantID { get; set; }
        public long ParentID { get; set; }
        public string Address { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public MembershipTypeViewModel MembershipType { get; set; }
        public long? MembershipTypeId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public FamilyPortalViewModel portal { get; set; }
        public List<tblParentInfoViewModel> ParentInfo { get; set; }
        public List<tblParentInfoViewModel> ParentInfoExisting { get; set; }
        public List<tblParentParticipantMappingViewModel> ParentParticipantMapping { get; set; }
        public List<tblParticipantAssociatedParticipantViewModel> ParticipantAssociatedParticipantMapping { get; set; }
        public string MappedClass { get; set; }

        public string Age { get; set; }

    }
}
