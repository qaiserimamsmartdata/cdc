﻿namespace CDC.ViewModel.Agency
{
    /// <summary>
    ///     The Agency View Model
    /// </summary>
    public class AgencyViewModel
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public string AgencyName { get; set; }
        public string ContactPerson { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public long CountryId { get; set; }
        public string CountryName { get; set; }
        public long StateId { get; set; }
        public string StateName { get; set; }

        public long CityId { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }

        public string Password { get; set; }
        public bool IsLoggedFirstTime { get; set; }
        public long TotalCnt{ get; set; }       

    }
}