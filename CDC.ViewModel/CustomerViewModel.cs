﻿using System;

namespace CDC.ViewModel
{
    public class CustomerViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime? DOB { get; set; }
        public long? CityId { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public string ProfileImageURL { get; set; }
        public bool? IsDeleted { get; set; }

        public string CityName { get; set; }
    }
}