﻿using CDC.Entities.Family;
using CDC.Entities.SMSCarrier;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.FamilyPortal;
using CDC.ViewModel.ParticipantIncident;
using CDC.ViewModel.Staff;
using CDC.ViewModel.Tools.Family;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Family
{
    public class FamilyInfoViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public string FamilyName { get; set; }
        public string SecurityKey { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }

        

        public AgencyRegistrationViewModel Agency { get; set; }
        public ICollection<ParentInfoViewModel> ParentInfo { get; set; }
        public ICollection<ContactInfoMapViewModel> ContactInfoMap { get; set; }
        public ICollection<FamilyStudentMapViewModel> FamilyStudentMap { get; set; }
        public ICollection<ReferenceDetailViewModel> ReferenceDetail { get; set; }
        public ICollection<PaymentInfoViewModel> PaymentInfo { get; set; }
    }

    public class ParentInfoViewModel
    {
        public long ID { get; set; }
        public long FamilyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string HomePhone { get; set; }
        public string EmailId { get; set; }
        public long RelationTypeId { get; set; }
        public long SecurityQuestionId { get; set; }
        public string SecurityQuestionAnswer { get; set; }
        public virtual SecurityQuestions SecurityQuestion { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsPrimary { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public string RelationName { get; set; }
        public string SecurityKey { get; set; }
        public string FullName { get; set; }
        public long SMSCarrierId { get; set; }
        public string SMSCarrierEmailAddress { get; set; }
        public string SecurityCode { get; set; }
        public string ImagePath { get; set; }


        public long? RoleId { get; set; }

        public long? UserId { get; set; }



        public string RoleName { get; set; }
        public SMSCarrierViewModel SMSCarrier { get; set; }
        // public FamilyInfoViewModel Family { get; set; }
        //public RelationTypeViewModel RelationType { get; set; }

        public long? AgencyId { get; set; }
        public string TimeZone { get; set; }
        public bool? IsEventMailReceived { get; set; }
        public bool? IsParticipantAttendanceMailReceived { get; set; }
        public bool? IsFromRegistration { get; set; }
        public bool IsLoggedFirstTime { get; set; }
    }

    public class ReferenceDetailViewModel
    {
        public long ID { get; set; }
        public long FamilyId { get; set; }
        public long InfoSourceId { get; set; }
        public string ReferedBy { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        //public FamilyInfoViewModel Family { get; set; }
        //public InfoSourceViewModel InfoSource { get; set; }
    }
    public class FamilyStudentMapViewModel
    {
        public long ID { get; set; }
        public long FamilyId { get; set; }
        public long StudentId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        //public FamilyInfoViewModel Family { get; set; }
        public StudentInfoViewModel Student { get; set; }
    }

    public class ContactInfoViewModel
    {
        public long ID { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public long StateId { get; set; }
        public long CountryId { get; set; }
        public string PostalCode { get; set; }
        public string PhoneCode { get; set; }
        public string EmergencyFirstName { get; set; }
        public string EmergencyLastName { get; set; }
        public long RelationshipToParticipantId { get; set; }
        public string EmergencyPhoneNumber { get; set; }
        public string InsuranceCarrier { get; set; }
        public string InsurancePolicyNumber { get; set; }
        public string PreferredHospitalName { get; set; }
        public string PreferredHospitalAddress { get; set; }
        public string FamilyDoctorName { get; set; }
        public string FamilyDoctorPhoneNumber { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public Nullable<long> FamilyId { get; set; }
        public Nullable<long> StaffId { get; set; }

        public StateViewModel State { get; set; }
        public CountryViewModel Country { get; set; }
        public RelationTypeViewModel RelationshipToParticipant { get; set; }
    }

    public class ContactInfoMapViewModel
    {
        public long ID { get; set; }
        public Nullable<long> FamilyId { get; set; }
        public Nullable<long> StaffId { get; set; }
        public Nullable<long> ContactId { get; set; }
        public string PhoneCode { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        //public FamilyInfoViewModel Family { get; set; }
        public StaffViewModel Staff { get; set; }
        public ContactInfoViewModel Contact { get; set; }
    }

    public class StudentInfoViewModel
    {
        public long ID { get; set; }
        public Nullable<long> FamilyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Age { get; set; }
        public string ImagePath { get; set; }
        public Nullable<long> MembershipTypeId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public string StringDateOfBirth { get; set; }
        public MembershipTypeViewModel MembershipType { get; set; }
        //public string Description { get; set; }
        //public ICollection<IncidentViewModel> Incident { get; set; }

    }

    public class StudentDetailViewModel
    {
        public long ID { get; set; }
        public long studentId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string MappedClass { get; set; }
        public string SchoolName { get; set; }
        public string Transportation { get; set; }
        public string Disabilities { get; set; }
        public string Allergies { get; set; }
        public string Medications { get; set; }
        public string PrimaryDoctor { get; set; }
        public long? GradeLevelId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public GradeLevelViewModel GradeLevel { get; set; }
        public StudentInfoViewModel Student { get; set; }
    }
    public class FamilyRegisterViewModel
    {
        public long FamilyId { get; set; }
        public long StudentId { get; set; }
        public DateTime? StartDate { get; set; }
        public long ClassId { get; set; }
        public long AgencyId { get; set; }
        public FamilyInfoViewModel FamilyInfo { get; set; }
        public ReferenceDetailViewModel ReferenceDetail { get; set; }
        public List<ParentInfoViewModel> ParentInfo { get; set; }
        public ContactInfoViewModel ContactInfo { get; set; }
        public List<StudentDetailViewModel> StudentDetail { get; set; }
        public PaymentInfoViewModel PaymentInfo { get; set; }
        public FamilyPortalViewModel PortalInfo { get; set; }
        
    }
    public class FamilyDetailViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public string FamilyName { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public long StateId { get; set; }
        public long CountryId { get; set; }
        public string PostalCode { get; set; }
        public string EmergencyContactInfo { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }

        public FamilyInfoViewModel Family { get; set; }
    }
    public class PaymentInfoViewModel
    {
        public long ID { get; set; }    
        public Nullable<long> FamilyId { get; set; }
        public bool IsCreditCash { get; set; }
        public string CardName { get; set; }
        public int CardType { get; set; }
        public string CVV { get; set; }
        public string CardNumber { get; set; }
        public int CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        public string CardBillingAddress { get; set; }
        public long? CardCountryId { get; set; }
        public long? CardStateId { get; set; }
        public long? CardCityId { get; set; }
        public string CardCityName { get; set; }
        public string CardPostalCode { get; set; }
        public bool Acceptance { get; set; }
        public bool? IsDeleted { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
