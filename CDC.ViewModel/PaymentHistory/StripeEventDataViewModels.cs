﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.PaymentHistory
{
    public class Metadata
    {
    }

    public class Plan
    {
        public string id { get; set; }
        public string @object { get; set; }
        public int amount { get; set; }
        public int created { get; set; }
        public string currency { get; set; }
        public string interval { get; set; }
        public int interval_count { get; set; }
        public bool livemode { get; set; }
        public Metadata metadata { get; set; }
        public string name { get; set; }
        public object statement_descriptor { get; set; }
        public object trial_period_days { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public string @object { get; set; }
        public int created { get; set; }
        public Plan plan { get; set; }
        public int quantity { get; set; }
    }

    public class Items
    {
        public string @object { get; set; }
        public List<Datum> data { get; set; }
        public bool has_more { get; set; }
        public int total_count { get; set; }
        public string url { get; set; }
    }

    public class Metadata2
    {
    }

    public class Metadata3
    {
    }

    public class Plan2
    {
        public string id { get; set; }
        public string @object { get; set; }
        public int amount { get; set; }
        public int created { get; set; }
        public string currency { get; set; }
        public string interval { get; set; }
        public int interval_count { get; set; }
        public bool livemode { get; set; }
        public Metadata3 metadata { get; set; }
        public string name { get; set; }
        public object statement_descriptor { get; set; }
        public object trial_period_days { get; set; }
    }

    public class Object
    {
        public string id { get; set; }
        public string @object { get; set; }
        public object application_fee_percent { get; set; }
        public bool cancel_at_period_end { get; set; }
        public object canceled_at { get; set; }
        public int created { get; set; }
        public int current_period_end { get; set; }
        public int current_period_start { get; set; }
        public string customer { get; set; }
        public object discount { get; set; }
        public object ended_at { get; set; }
        public Items items { get; set; }
        public bool livemode { get; set; }
        public Metadata2 metadata { get; set; }
        public Plan2 plan { get; set; }
        public int quantity { get; set; }
        public int start { get; set; }
        public string status { get; set; }
        public object tax_percent { get; set; }
        public object trial_end { get; set; }
        public object trial_start { get; set; }
        public string balance_transaction { get; set; }
    }

    public class StripeEventDataViewModel
    {
        public Object @object { get; set; }
        public object previous_attributes { get; set; }
    }
}
