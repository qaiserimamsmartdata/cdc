﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.ToDoTask
{
    public class ToDoViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public DateTime DueDate { get; set; }
        public long AgencyId { get; set; }
        public bool? IsDeleted { get; set; }
        public long? PriorityId { get; set; }
        public string Description { get; set; }
        public  PriorityViewModel Priority { get; set; }
    }
}
