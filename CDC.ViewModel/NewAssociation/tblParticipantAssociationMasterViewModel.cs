﻿using CDC.Entities.AgencyRegistration;
using CDC.ViewModel.AgencyRegistration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.NewAssociation
{
   public class tblParticipantAssociationMasterViewModel
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public AgencyRegistrationViewModel Agency { get; set; }
        public long? AgencyId { get; set; }
        public string AssociationTypeName { get; set; }
    }
}
