﻿using System;
using System.Collections.Generic;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Enums;
using CDC.Entities.Tools.Staff;
using CDC.Entities.User;

namespace CDC.Entities.SMSCarrier
{
    public class SMSCarrierViewModel
    {
        public long? ID { get; set; }
        public bool? IsDeleted { get; set; }
        public string Carrier { get; set; }
        public string CarrierAddress { get; set; }
    }
}