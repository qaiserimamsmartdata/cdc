﻿using CDC.Entities.Pricing;
using CDC.Entities.TimeClockUsers;
using System;

namespace CDC.ViewModel.AgencyRegistration
{
    /// <summary>
    ///     The Agency View Model
    /// </summary>
    public class AgencyRegistrationViewModel
    {
        //Basic Information//
        public long ID { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public string AgencyName { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string Email { get; set; }
        public bool IsExistingAccount { get; set; }
        public string Password { get; set; }
        //
        //Billing Information//
        public string AuthorizedBillingAccountname { get; set; }
        public string BillingEmail { get; set; }

        public long? TimeClockUsersPlanId { get; set; }

        //
        //Contact Information//
        public string Address { get; set; }
        public Nullable<long> CountryId { get; set; } = 0;
        public Nullable<long> StateId { get; set; } = 0;
        public Nullable<long> CityId { get; set; } = 0;
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string CityName { get; set; }

        //
        //Payment Information//
        public bool IsCreditCash { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string CVV { get; set; }
        public int CardExpiryMonth { get; set; }
        public int CardExpiryYear { get; set; }
        public string CardBillingAddress { get; set; }
        public Nullable<long> CardCountryId { get; set; } = 0;
        public Nullable<long> CardStateId { get; set; } = 0;
        public Nullable<long> CardCityId { get; set; } = 0;
        public string CardCityName { get; set; }
        public string CardPostalCode { get; set; }

        public string StripeUserId { get; set; }

        public String StripeToken { get; set; }

        public String StripeSubscriptionId { get; set; }
        public bool Acceptance { get; set; }
        public long RoleId { get; set; }
        public long UserId { get; set; }
        public string RoleName { get; set; }
        //Agency Location Info
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string LocationName { get; set; }
        public string Description { get; set; }
        public string LocationEmailId { get; set; }
        public string LocationPhoneNumber { get; set; }
        
        public bool IsLoggedFirstTime { get; set; }
        public long TotalCnt{ get; set; }
        public long PricingPlanId { get; set; }
        public int TotalTimeClockUsers { get; set; }
        public int TotalRequireEnrollParticipants { get; set; }
        public PricingPlanViewModel PricingPlan { get; set; }
        public TimeClockUsersViewModel TimeClockUsersPlan { get; set; }
        public string TimeZone{ get; set; }
        public string StripeTimeClockSubscriptionId { get; set; }
        public string StripePlanId { get; set; }
        public bool? IsTrial { get; set; }
        public DateTime? TrialStart { get; set; }
        public DateTime? TrialEnd { get; set; }


        /// <summary>
        /// Gets or sets the agency time clock users count.
        /// </summary>
        /// <value>
        /// The agency time clock users with staff and Timeclockusersplan users count.
        /// </value>
        public int? AgencyTimeClockUsers { get; set; }
        public int? TimeClockPlanUsers { get; set; }
        public int? TimeClockStaffCount { get; set; }
        //public ICollection<AgencyContactInfo> AgencyContactInfos { get; set; }
        //public ICollection<AgencyBillingInfo> AgencyBillingInfos { get; set; }
        //public ICollection<AgencyPaymentInfo> AgencyPaymentInfos { get; set; }

    }
}

