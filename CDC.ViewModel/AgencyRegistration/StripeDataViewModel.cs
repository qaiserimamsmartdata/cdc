﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.AgencyRegistration
{
   public class StripeDataViewModel
    {
        public string StripeSubscriptionId { get; set; }
        public string StripeUserId { get; set; }
    }
}
