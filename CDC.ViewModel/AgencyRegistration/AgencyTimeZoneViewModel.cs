﻿using CDC.Entities.Pricing;
using CDC.Entities.TimeClockUsers;
using System;

namespace CDC.ViewModel.AgencyRegistration
{
    /// <summary>
    ///     The Agency View Model
    /// </summary>
    public class AgencyTimeZoneViewModel
    {
        //Basic Information//
        public long ID { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public string TimeZone { get; set; }

    }
}

