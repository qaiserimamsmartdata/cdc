﻿using CDC.Entities.Enums;
using CDC.Entities.SMSCarrier;
using CDC.ViewModel.Attendance;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Staff
{
    public class StaffViewModel
    {
        public long ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public EnumGender Gender { get; set; }
        public string GenderName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string DateOfBirthString { get; set; }
        public string ImagePath { get; set; }
        public string PositionName { get; set; }
        public Nullable<long> StatusId { get; set; }
        public string StatusName { get; set; }
        public DateTime? DateHired { get; set; }
        public string DateHiredString { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public Nullable<long> CountryId { get; set; }
        public string CountryName { get; set; }
        public Nullable<long> StateId { get; set; }
        public string StateName { get; set; }
        public string Certification { get; set; }
        public Nullable<long> CityId { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        //Mobile
        public string PhoneNumber { get; set; }
        public string HomePhone { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long RoleId { get; set; }
        public long UserId { get; set; }
        public string RoleName { get; set; }
        public Nullable<long> AgencyRegistrationId { get; set; }

        public string AgencyName { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public long TotalCnt { get; set; }
        public long? PositionId { get; set; }
        public string Apartment { get; set; }
        //public long? AgencyLocationInfoId { get; set; }
        public bool? IsTimeClockUser { get; set; }
        public string TimeZone { get; set; }

        public long MyProperty { get; set; }
        public long? SMSCarrierId { get; set; }
        public string SMSCarrierEmailAddress { get; set; }
        public SMSCarrierViewModel SMSCarrier { get; set; }

        public List<StaffLocationInfoViewModel> StaffLocationList{ get; set; }
        public ICollection<StaffScheduleViewModel> StaffScheduleList { get; set; }
        public ICollection<StaffLocationInfoViewModel> StaffLocationInfoList { get; set; }
        public ICollection<AttendanceViewModel> StaffAttendanceList { get; set; }
    }
}
