﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Staff
{
    public  class TaskClassMappingViewModel
    {
        public bool? IsDeleted { get; set; }
        public long TaskId { get; set; }
        public long ClassId { get; set; }

        public long Id { get; set; }
        public string Name { get; set; }
    }
}
