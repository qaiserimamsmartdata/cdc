﻿using System;
using System.Collections.Generic;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Enums;
using CDC.Entities.Tools.Staff;
using CDC.Entities.User;

namespace CDC.ViewModel.Staff
{
    public class StaffLocationInfoViewModel
    {
        public long ID { get; set; }
        public long? AgencyId { get; set; }
        public bool? IsDeleted { get; set; }
        public long? AgencyLocationInfoId { get; set; }
        
        public long StaffId { get; set; }
        public AgencyLocationInfoViewModel AgencyLocationInfo { get; set; }
        
    }
}