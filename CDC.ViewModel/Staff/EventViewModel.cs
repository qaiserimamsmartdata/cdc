﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Staff
{
    public class EventViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }

        public string Title { get; set; }
        public DateTime? StartDate { get; set; }
        public TimeSpan? StartTime { get; set; }

        public DateTime? EndDate { get; set; }
        public TimeSpan? EndTime { get; set; }

        public string Location { get; set; }
        public string Description { get; set; }

        public bool? IsAllDay { get; set; }

        //public int RecurrenceTypeId { get; set; }
        //public int RecurrenceTypeValue { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
