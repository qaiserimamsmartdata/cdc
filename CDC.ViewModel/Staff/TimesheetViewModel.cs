﻿using CDC.Entities.Enums;
using CDC.Entities.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Staff
{
    public class TimesheetViewModel
    {
        public List<TimesheetListModel> TimeSheets { get; set; }
        public double TotalHours
        {
            get
            {
                if (this.TimeSheets != null)
                {
                    return this.TimeSheets.Sum(x => x.TotalHours);
                }
                return 0;
            }
        }
        public int? TotalRecords { get; set; }
    }
    public class TimesheetListModel
    {
        /// <summary>
        /// Gets or sets the staff id.
        /// </summary>
        public int StaffID { get; set; }
        /// <summary>
        /// Gets of set the Day name.
        /// </summary>
        public string Day { get; set; }
        /// <summary>
        /// Gets or sets the day check in date time.
        /// </summary>
        public DateTime? DayCheckIn { get; set; }
        /// <summary>
        /// Gets or sets the day check out date time.
        /// </summary>
        public DateTime? DayCheckOut { get; set; }
        /// <summary>
        /// Gets or sets the lunch check in date time.
        /// </summary>
        public DateTime? LunchCheckIn { get; set; }
        /// <summary>
        /// Gets or sets the lunch check out date time.
        /// </summary>
        public DateTime? LunchCheckOut { get; set; }

        public List<GeneralBreak> BreakChecks { get; set; }

        /// <summary>
        /// Gets or sets the total working hours.
        /// </summary>
        public double TotalHours { get; set; }

        private bool? _CheckOutAtNextDay = false;
        public bool? CheckOutAtNextDay
        {
            get
            {

                if (this.DayCheckIn.HasValue && this.DayCheckOut.HasValue && this.DayCheckIn.Value.Date != this.DayCheckOut.Value.Date)
                {
                    _CheckOutAtNextDay = true;
                }

                return _CheckOutAtNextDay;
            }
        }
    }
    public class GeneralBreak
    {
        public DateTime? CheckOut { get; set; }
        public DateTime? CheckIn { get; set; }
    }

    public class TimeCheckModel
    {
        public long TimesheetID { get; set; }
        /// <summary>
        /// Gets or sets property to hold the company staff id.
        /// </summary>
        public int StaffID { get; set; }
        /// <summary>
        /// Gets or sets property to hold the time check is IN or OUT.
        /// </summary>
        public TimeCheckInOrOut TimeChecksForInOrOut { get; set; }
        public string TimeChecksForInOrOutToString
        {
            get
            {
                return this.TimeChecksForInOrOut.ToString();
            }
        }
        /// <summary>
        /// Gets or sets to check in/out date time.
        /// </summary>
        public DateTime CheckOnDateTime { get; set; }
        /// <summary>
        /// Gets or sets property to hold the check in/out operations for a day, lunch or break.
        /// </summary>
        public InOutPurpose PurposeToCheckInOrOut { get; set; }
        public string PurposeToCheckInOrOutToString
        {
            get
            {
                return this.PurposeToCheckInOrOut.ToString();
            }
        }
        /// <summary>
        /// Gets or sets property to hold the check in/out comment(optional).
        /// </summary>
        public string Comment { get; set; }

        private object _OpenChecks = null;
        /// <summary>
        /// Gets or sets property to hold as on current check what will be open checks or operation.
        /// </summary>
        /// <example>Only Day-In : OpenChecks for Lunch, general break and Day check out. </example>
        public object OpenChecks
        {
            get
            {
                if (this.PurposeToCheckInOrOut == InOutPurpose.Day && this.TimeChecksForInOrOut == TimeCheckInOrOut.In)
                {
                    List<CheckOption> Options = null;
                    Options = new List<CheckOption>();
                    Options.Add(new CheckOption() { ID = 0, TimeCheckFor = "Day" });
                    Options.Add(new CheckOption() { ID = 1, TimeCheckFor = "Lunch" });
                    Options.Add(new CheckOption() { ID = 2, TimeCheckFor = "Break" });

                    _OpenChecks = Options;
                    //_OpenChecks = new
                    //{
                    //checkOptions = "[{\"ID\":0, \"TimeCheckFor\":\"Day\"}, {\"ID\":1, \"TimeCheckFor\":\"Lunch\"}, {\"ID\":2,\"TimeCheckFor\":\"Break\"}]"
                    //    Options.Add( )
                    //};
                }
                else if ((this.PurposeToCheckInOrOut == InOutPurpose.Lunch
                    || this.PurposeToCheckInOrOut == InOutPurpose.Break)
                    && this.TimeChecksForInOrOut == TimeCheckInOrOut.Out)
                {
                    //_OpenChecks = new { checkOptions = this.PurposeToCheckInOrOut == InOutPurpose.Lunch ? "[{\"ID\":1, \"TimeCheckFor\":\"Lunch\"}]" : "[{\"ID\":2, \"TimeCheckFor\":\"Break\"}]" };
                    List<CheckOption> Options = null;
                    Options = new List<CheckOption>();
                    if (this.PurposeToCheckInOrOut == InOutPurpose.Lunch)
                    {
                        Options.Add(new CheckOption() { ID = 1, TimeCheckFor = "Lunch" });
                    }
                    else if (this.PurposeToCheckInOrOut == InOutPurpose.Break)
                    {
                        Options.Add(new CheckOption() { ID = 2, TimeCheckFor = "Break" });
                    }

                    _OpenChecks = Options;
                }

                return _OpenChecks;
            }
        }
        ///// <summary>
        ///// Gets or sets property to hold the check in/out IP address
        ///// </summary>
        //public string IPAddress { get; set; }
        ///// <summary>
        ///// Gets or sets the check in/out timezone.
        ///// </summary>
        //public string Timezone { get; set; }
    }

    public class CheckOption
    {
        public int ID { get; set; }
        public string TimeCheckFor { get; set; }
    }
    public class TimesheetModel : Timesheet
    {

    }
}
