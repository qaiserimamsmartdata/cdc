﻿using CDC.ViewModel.Attendance;
using CDC.ViewModel.Class;
using CDC.ViewModel.Tools.Class;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Staff
{
    public class StaffScheduleViewModel
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public Nullable<long> StaffId { get; set; }
        public Nullable<long> ClassId { get; set; }
        public Nullable<long> RoomId { get; set; }
        public DateTime? Date { get; set; }
        public Nullable<TimeSpan> Time { get; set; }

        public DateTime? EndDate { get; set; }
        public Nullable<TimeSpan> EndTime { get; set; }

        public Nullable<long> LessonPlanId { get; set; }
        public StaffViewModel Staff { get; set; }
        public ClassViewModel Class { get; set; }
        public RoomViewModel Room { get; set; }
        public  LessonPlanViewModel LessonPlan { get; set; }
        public long TotalCnt { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

        public int RecurrenceTypeId { get; set; }
        public int RecurrenceTypeValue { get; set; }

        //public ICollection<AttendanceViewModel> AttendanceInfo { get; set; }
    }
}
