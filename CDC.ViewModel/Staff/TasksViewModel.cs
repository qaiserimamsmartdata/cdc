﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Newtonsoft.Json.Linq;

namespace CDC.ViewModel.Staff
{
    public class TasksViewModel 
    {
        public int TaskID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        private DateTime start;
        public DateTime Start
        {
            get
            {
                return start;
            }
            set
            {
                start = value.ToUniversalTime();
            }
        }

        public string StartTimezone { get; set; }

        private DateTime end;
        public DateTime End
        {
            get
            {
                return end;
            }
            set
            {
                end = value.ToUniversalTime();
            }
        }

        public string EndTimezone { get; set; }

        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }
        public int? OwnerID { get; set; }
        public int? AgencyId { get; set; }
        public int? StaffId { get; set; }
        public int? FamilyId { get; set; }
        public int? ClassId { get; set; }
        public List<TaskClassMappingViewModel> ClassIdJson { get; set; }
        public int? RoomId { get; set; }
        public int? LeaveId { get; set; }
        public string TimeZone { get; set; }
        public bool? IsSendEmailChecked { get; set; }
        //public int? RoomID { get; set; }
    }
    public class TasksViewModelCopy 
    {
        public int TaskID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        private DateTime start;
        public DateTime Start
        {
            get
            {
                return start;
            }
            set
            {
                //start = value.ToUniversalTime();
                start = value;
            }
        }

        public string StartTimezone { get; set; }

        private DateTime end;
        public DateTime End
        {
            get
            {
                return end;
            }
            set
            {
                //end = value.ToUniversalTime();
                end = value;
            }
        }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }
        public int? OwnerID { get; set; }
        public int? AgencyId { get; set; }
        public int? StaffId { get; set; }
        public int? FamilyId { get; set; }
        public int? ClassId { get; set; }
        public List<TaskClassMappingViewModel> ClassIdJson { get; set; }
        public int? RoomId { get; set; }
        public int? LeaveId { get; set; }

        public string  StaffName { get; set; }
        public string ClassName { get; set; }
        public string TimeZone { get; set; }
        public bool? IsSendEmailChecked { get; set; }
        //public int? RoomID { get; set; }
    }

    public class RootObject
    {
        public string models { get; set; } 
    }

    public class NewRootObject
    {
        public string agency { get; set; }
    }

}
