﻿using CDC.ViewModel.Tools.Staff;
using System;

namespace CDC.ViewModel.Staff
{
    public class LeaveViewModel
    {
        public long ID { get; set; }
        public Nullable<long> StaffId { get; set; }
        public Nullable<long> StatusId { get; set; }
        public DateTime? LeaveFromDate { get; set; }
        public DateTime? LeaveToDate { get; set; }
        public virtual StaffViewModel Staff { get; set; }
        public virtual StatusViewModel Status { get; set; }
        public long TotalCnt { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> LeaveTypeId { get; set; }
        public virtual LeaveTypeViewModel LeaveType { get; set; }
        /// <summary>
        /// Gets or sets the remark.
        /// Use when approval or cancel the leave.
        /// </summary>
        /// <value>
        /// The remark.
        /// </value>
        public string Remarks { get; set; }
        /// <summary>
        /// Gets or sets the action taken by.
        /// Use when Cancel or Approve the leave
        /// </summary>
        /// <value>
        /// The action taken by.
        /// </value>
        public Nullable<long> ActionTakenBy { get; set; }
        /// <summary>
        /// Gets or sets the action date.
        /// Use when Cancel or Approve the leave
        /// </summary>
        /// <value>
        /// The action date.
        /// </value>
        public DateTime? ActionDate { get; set; }


        public int AgencyId { get; set; }
        public string TimeZone { get; set; }
        public string ProfileName { get; set; }
    }
}
