﻿namespace CDC.ViewModel
{
    public static class AppConstants
    {
        public const string Agency = "Agency";
        public const string Organization = "Organization Admin";
        public const string Staff = "Staff";
        public const string UserDoesNotExist = "User does not exist.";
        public const string SomethingWentWrong = "Something went wrong.";
        public const string NewEventCreated = "New event created.";
        public const string InvalidRoleName = "Invalid role name.";
        public const string StaffAddedSuccesfully = "Staff added successfully.";
        public const string RequestForCDCStaffLogin = "Request for PinWheel Care Staff Login";
        public const string RequestForCDCAgencyLogin = "Request for PinWheel Care Organization Login";
        public const string RequestForCDCParentLogin = "Request for PinWheel Care Parent Login";
        public const string RequestForCDCFamilyLogin = "Request for PinWheel Care Family Login";
        public const string OrganisationAddedSuccesfully = "Organization added successfully.";
        public const string InvalidEmailOrPassword = "Invalid EmailId or Password.";
        public const string ForgotPassword = "Request to reset the Password.";
        public const string AgencyRegForAdmin = "New Organization has been added.";
        public const string LeaveRequestStatus = "Leave request status.";
        public const string StaffSuperAdmin = "New Staff has been logged in.";
        public const string Family = "Family";
        public const string ParentIncident = "Incident report of your ward.";
        public const string ParticipantDailyStatus = "Daily Status report.";
        public const string PrimaryParentRegistered = "Parent added successfully.";
        public const string ResetPassword = "Your password has been reset successfully. ";
        public const string TrialPlanExpiry = "Trial plan expiry information. ";
        public const string AssociatedParent = "New Participant has been added.";
    }
}