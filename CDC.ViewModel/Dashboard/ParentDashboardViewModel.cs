﻿using CDC.Entities.Class;
using CDC.Entities.Enums;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Class;
using CDC.ViewModel.ToDoTask;
using CDC.ViewModel.Tools.Enhancements;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Dashboard
{
    public class ParentDashboardViewModel
    {
        public long ID { get; set; }
        public int ContactCount { get; set; }
        public int  ParticipantCount { get; set; }
        public int AgencyCount { get; set; }
        public int  AttendanceCount { get; set; }
        public int EventCount { get; set; }
        public  int TotalEnrolledParticipants { get; set; }
        public int TimeClockStaffCount { get; set; }
        public int UnAssignedEnrollParticipantCount { get; set; }
        public int FutureEnrollParticipantCount { get; set; }
        public int CurrentEnrollParticipantCount { get; set; }
        public int SignInParticipantCount { get; set; }
        public List<StudentAttendanceViewModel> attendanceList{ get; set; }
        public int DailyStatusCount { get; set; }
}
}
