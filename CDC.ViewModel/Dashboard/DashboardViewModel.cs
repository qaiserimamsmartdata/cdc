﻿using CDC.Entities.Class;
using CDC.Entities.Enums;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Class;
using CDC.ViewModel.ToDoTask;
using CDC.ViewModel.Tools.Enhancements;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Dashboard
{
    public class DashboardViewModel
    {
        public long ID { get; set; }
        public int FamilyCount { get; set; }
        public int  StaffCount { get; set; }
        public int  ParticipantCount { get; set; }
        public int  AttendanceCount { get; set; }
        public int EnhancementCount { get; set; }
        public List<ClassViewModel> ClassList { get; set; }
        public List<AttendanceViewModel> AttendanceList { get; set; }
        public List<ToDoViewModel> ToDoList { get; set; }
        public List<EnhancementViewModel> EnhancementList { get; set; }
        public  int TotalEnrolledParticipants { get; set; }
        public int TimeClockStaffCount { get; set; }
        public int UnAssignedEnrollParticipantCount { get; set; }
        public int FutureEnrollParticipantCount { get; set; }
        public int CurrentEnrollParticipantCount { get; set; }
        public int SignInParticipantCount { get; set; }

        ///Newly added for timeclockusers count
        public int TimeClockUserPlanMaxCount { get; set; }
        public int PricingPlanTimeClockMaxCount { get; set; }

        public int EventCount { get; set; }
    }
}
