﻿using CDC.Entities.Class;
using CDC.Entities.Enums;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Class;
using CDC.ViewModel.Staff;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Dashboard
{
    public class SuperAdminDashboardViewModel
    {
        public long ID { get; set; }
        public long TotalAgencyCount { get; set; }
        public int  PricingPlanCount { get; set; }
        public int TimeClockUserPlanCount { get; set; }
        public double PricingPlanAmountSum { get; set; }
        public double TimeClockUsersPlanAmountSum { get; set; }
        public List<AgencyRegistrationViewModel> AgencyList { get; set; }
        public long AgencyPricingPlansSum { get; set; }
        public long AgencyTimeClockUsersPlansSum { get; set; }
    }
}
