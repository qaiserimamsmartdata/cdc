﻿using CDC.Entities.Class;
using CDC.Entities.Enums;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Class;
using CDC.ViewModel.Staff;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Dashboard
{
    public class StaffDashboardViewModel
    {
        public long ID { get; set; }
        public double? TotalHours { get; set; }
        public int  ParticipantCount { get; set; }
        public int  AttendanceCount { get; set; }
        
        public int LeaveCount {get; set;}
        public  List<StaffDashboardTimeSheetViewModel> TimesheetViewModelData { get; set;}

        public string StartTime { get; set; }
        public double? TotalWorkingHours { get; set; }
        public int TotalLunch { get; set; }
        public int TotalBreaks { get; set; }

        public string InTime { get; set; }
        public string OutTime { get; set; }
        public int EventCount { get; set; }
    }
}
