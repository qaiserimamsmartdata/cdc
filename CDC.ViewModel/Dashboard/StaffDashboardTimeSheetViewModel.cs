﻿using CDC.Entities.Class;
using CDC.Entities.Enums;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Class;
using CDC.ViewModel.Staff;
using System;
using System.Collections.Generic;

namespace CDC.ViewModel.Dashboard
{
    public class StaffDashboardTimeSheetViewModel
    {
        public long ID { get; set; }
        public double? TotalHours { get; set; }
        public string Name { get; set; }
    }
}
