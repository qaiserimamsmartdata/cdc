﻿using System;

namespace CDC.ViewModel.Class
{
    public class LessonPlanViewModel
    {
        public string Name { get; set; }

        public DateTime? Date { get; set; }

        public string Theme { get; set; }

        public long Order { get; set; }
        public string SpecialInstructions { get; set; }
        public string Description { get; set; }

        public string UploadFile { get; set; }
        public long ID { get; set; }
        public long AgencyId { get; set; }

        public bool? IsDeleted { get; set; }
        public Nullable<long> ClassId { get; set; }
    }
}