﻿using CDC.Entities.Class;
using CDC.ViewModel.Tools.FoodManagementMaster;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Class
{
    public class FoodManagementMealPatternItemsViewModel 
    {
        public long ID { get; set; }
        public long FoodManagementMealPatternId { get; set; }
        public FoodManagementMealPatternViewModel FoodManagementMealPattern { get; set; }
        public long FoodManagementMasterId { get; set; }
        public FoodManagementMasterViewModel FoodManagementMaster { get; set; }
        public bool? IsDeleted { get; set; }
        public bool IsChecked { get; set; }
    }
}
