﻿using CDC.ViewModel.Student;
using System;

namespace CDC.ViewModel.Class
{
    public class WaitListMapViewModel
    {
        public long ID { get; set; }
        public long ClassId { get; set; }
        public long StudentId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public ClassViewModel Class { get; set; }
        public StudentViewModel Student { get; set; }
    }
}
