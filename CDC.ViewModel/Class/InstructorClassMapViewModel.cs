﻿using System;

namespace CDC.ViewModel.Class
{
    public class InstructorClassMapViewModel
    {
        public long ID { get; set; }
        public Nullable<long> StaffId { get; set; }
        public Nullable<long> ClassId { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public string InstructorName { get; set; }
        public string DesignationName { get; set; }
        public string ClassName { get; set; }
        public string Email { get; set; }
    }
}
