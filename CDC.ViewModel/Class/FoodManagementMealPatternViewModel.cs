﻿using CDC.Entities.Class;
using CDC.ViewModel.Tools.FoodManagementMaster;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.Class
{
    public class FoodManagementMealPatternViewModel
    {
        public long ID { get; set; }
        public long AgencyId { get; set; }
        public long ClassInfoId { get; set; }
        public ClassViewModel ClassInfo { get; set; }
        public string AgeGroup { get; set; }
        public long AgeGroupId { get; set; }

        public MealTypeViewModel MealType { get; set; }
        public long MealTypeId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }        
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
        public List<FoodManagementMealPatternItemsViewModel> ItemList { get; set; }

        public ICollection<FoodManagementMealPatternItemsViewModel> FoodManagementMealPatternItemsInfos { get; set; }
    }
}
