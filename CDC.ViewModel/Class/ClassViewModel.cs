﻿using CDC.ViewModel.Tools.Class;
using CDC.ViewModel.Tools.Student;
using System;

namespace CDC.ViewModel.Class
{
    public class ClassViewModel
    {
        public long ID { get; set; }
        public bool? IsDeleted { get; set; }
        public string ClassName { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public long ClassStatusId { get; set; }
        public int EnrollCapacity { get; set; }
        public long? SessionId { get; set; }
        public string SessionName { get; set; }
        public long? RoomId { get; set; }
        public string RoomName { get; set; }
        public int MinAgeFrom { get; set; }
        public int MinAgeTo { get; set; }
        public int MaxAgeFrom { get; set; }
        public int MaxAgeTo { get; set; }
        public DateTime? AgeCutOffDate { get; set; }
        public DateTime? RegistrationStartDate { get; set; }
        public DateTime? ClassStartDate { get; set; }
        public DateTime? ClassEndDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string Description { get; set; }
        public string AgeCutOffDateStr { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }

        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public bool Sun { get; set; }
        public long TotalCnt { get; set; }
        public Nullable<long> AgencyId { get; set; }
        public int EnrolledParticipants { get; set; } = 0;
        public bool? OnGoing { get; set; }
        public decimal? Fees { get; set; }
        public long? FeeTypeId { get; set; }
        public FeeTypeViewModel FeeType { get; set; }
        public  ClassStatusViewModel ClassStatus { get; set; }
        public long? AgencyLocationInfoId { get; set; }
        public string TimeZone{ get; set; }

        public string AgeCutOffDateString { get; set; }
        public string RegistrationStartDateString { get; set; }
        public string ClassStartDateString { get; set; }
        public string ClassEndDateString { get; set; }
    }
}