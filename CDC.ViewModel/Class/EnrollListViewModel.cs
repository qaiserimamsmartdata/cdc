﻿using CDC.Entities.Enums;
using System;

namespace CDC.ViewModel.Class
{
    public class EnrollListViewModel
    {
        public long ID { get; set; }
        public long StudentId { get; set; }
        public string StudentFullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EnumGender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Age { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public Nullable<TimeSpan> MondayIn { get; set; }
        public Nullable<TimeSpan> MondayOut { get; set; }
        public Nullable<TimeSpan> TuesdayIn { get; set; }
        public Nullable<TimeSpan> TuesdayOut { get; set; }
        public Nullable<TimeSpan> WednesdayIn { get; set; }
        public Nullable<TimeSpan> WednesdayOut { get; set; }
        public Nullable<TimeSpan> ThursdayIn { get; set; }
        public Nullable<TimeSpan> ThursdayOut { get; set; }
        public Nullable<TimeSpan> FridayIn { get; set; }
        public Nullable<TimeSpan> FridayOut { get; set; }
        public Nullable<TimeSpan> SaturdayIn { get; set; }
        public Nullable<TimeSpan> SaturdayOut { get; set; }
        public string Status { get; set; }
        public long ClassId { get; set; }
        public string ClassName { get; set; }
        public string RoomName { get; set; }
        public string CategoryName { get; set; }
        public string SessionName { get; set; }
        public string ImagePath { get; set; }
        public long RoomId { get; set; }
        public string EnrollStatus { get; set; }
        public long IsEnrolled { get; set; }
    }
}
