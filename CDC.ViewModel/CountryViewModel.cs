﻿namespace CDC.ViewModel
{
    public class CountryViewModel
    {
        public long ID { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }

        public bool? IsDeleted { get; set; }
    }
}