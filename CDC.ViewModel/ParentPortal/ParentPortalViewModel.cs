﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.ParentPortal
{
   public class ParentPortalViewModel
    {
        public string PortalEmail { get; set; }
        public long RoleId { get; set; }
        public string PortalPassword { get; set; }
    }
}
