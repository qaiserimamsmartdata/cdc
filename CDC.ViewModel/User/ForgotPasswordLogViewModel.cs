﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.ViewModel.User
{
    public class ForgotPasswordLogViewModel
    {
        public long ID { get; set; }
        public long UserId { get; set; }

        public string  EmailId { get; set; }
        public Guid Guid { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

        //public Nullable<bool> IsReset { get; set; }

       public UserViewModel User { get; set; }
    }
}
