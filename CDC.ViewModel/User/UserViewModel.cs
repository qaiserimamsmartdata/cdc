﻿using System;

namespace CDC.ViewModel.User
{
    public class UserViewModel
    { 
        public long ID { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string OldPassword { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid Guid { get; set; }
        public string Token { get; set; }
        public DateTime? TokenExpiryDate { get; set; }
        public RolesViewModel Role { get; set; }
    }
}
