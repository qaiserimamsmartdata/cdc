﻿namespace CDC.ViewModel.User
{
    public class RolesViewModel
    {
        public long ID { get; set; }
        public string RoleName { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
