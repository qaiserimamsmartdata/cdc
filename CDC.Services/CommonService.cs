﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using AutoMapper;
using CDC.Data;
using CDC.Data.Masters;
using CDC.Data.Repositories;
using CDC.Entities;
using CDC.Entities.Class;
using CDC.Entities.Staff;
using CDC.Entities.Tools.Class;
using CDC.Entities.Tools.Staff;
using CDC.Entities.Tools.Student;
using CDC.Services.Abstract;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;
using CDC.ViewModel.Tools.Student;
using CDC.ViewModel.Staff;
using CDC.Entities.Pricing;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CDC.Entities.AgencyRegistration;
using CDC.ViewModel.Location;
using CDC.Entities.SMSCarrier;
using System.Configuration;
using System.Security.Cryptography;
using CDC.Entities.TimeClockUsers;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.ViewModel.Class;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Family;
using CDC.Entities.User;
using CDC.Entities.ToDoTask;
using CDC.Entities.Tools.ManageHoliday;
using CDC.Entities.DailyStatus;
using CDC.ViewModel.DailyStatus;
using CDC.Entities.Attendance;
using CDC.ViewModel.Attendance;
using CDC.Entities.Tools.FoodManagement;
using CDC.ViewModel.Tools.FoodManagementMaster;
using CDC.Entities.Tools.MealServingType;
using CDC.ViewModel.Tools.MealServingType;
using CDC.ViewModel.Schedule;
using CDC.Entities.Tools.Attendance;
using CDC.Entities.ExceptionLogs;
using System.Text;
using AT.Net.Service;
using CDC.Entities.NewParticipant;
using CDC.Entities.NewParent;
using CDC.ViewModel.NewParent;
//using CDC.Entities.Tools.Category;
//using CDC.ViewModel.Tools.Category;

namespace CDC.Services
{
    /// <summary>
    /// </summary>
    /// <seealso cref="CDC.Services.Abstract.ICommonService" />
    public class CommonService : ICommonService
    {
        private readonly IEntityBaseRepository<City> _cityRepository;
        private readonly IEntityBaseRepository<FamilyInfo> _familyRepository;
        private readonly IEntityBaseRepository<ClassInfo> _classInfoRepository;
        private readonly IEntityBaseRepository<Country> _countryRepository;
        private readonly IEntityBaseRepository<Position> _designationInfoRepository;
        private readonly IEntityBaseRepository<FeeType> _feesTypeRepository;
        private readonly IEntityBaseRepository<LessonPlan> _lessonPlanInfoRepository;
        private readonly IEntityBaseRepository<Room> _roomInfoRepository;
        private readonly IEntityBaseRepository<StaffInfo> _staffInfoRepository;
        private readonly IEntityBaseRepository<StaffScheduleInfo> _staffScheduleInfoRepository;
        private readonly IEntityBaseRepository<State> _stateRepository;
        private readonly IEntityBaseRepository<PricingPlan> _pricingPlanRepository;
        private readonly IEntityBaseRepository<AgencyLocationInfo> _agencyLocationInfoRepository;
        private readonly IEntityBaseRepository<SMSCarrier> _smsCarrierInfoRepository;
        private readonly IEntityBaseRepository<TimeClockUsersPlan> _timeClockUsersPlanInfoRepository;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familystudentMapRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IEntityBaseRepository<AgencyRegistrationInfo> _agencyRegistrationInfoRepository;
        private readonly IEntityBaseRepository<ParentInfo> _parentInfoRepository;
        private readonly IEntityBaseRepository<Users> _usersInfoRepository;
        private readonly IEntityBaseRepository<Entities.ToDoTask.Priority> _priorityRepository;
        private IEntityBaseRepository<Holiday> _holidayRepository;
       private IEntityBaseRepository<DailyStatusCategory> _DailyStatusCategoryRepository;
        private IEntityBaseRepository<StudentAttendanceInfo> _StudentAttendanceRepository;
        private IEntityBaseRepository<Position> _positionRepository;
        private IEntityBaseRepository<StudentDetail> _studentDetailsRepository;
        private IEntityBaseRepository<MealType> _mealTypeRepository;
        private IEntityBaseRepository<FoodManagementMaster> _foodManagementMasterRepository;
        private IEntityBaseRepository<MealItemMaster> _mealItemMasterRepository;
        private IEntityBaseRepository<Status> _statusLeaveRepository;
        private IEntityBaseRepository<MealServeSize> _mealServeSizeRepository;
        private IEntityBaseRepository<MealServeQuantity> _mealServeQuantityRepository;
        
        private IEntityBaseRepository<MealServeType> _mealServeTypeRepository;
        private IEntityBaseRepository<tblParentInfo> _parentRepository;
        private IEntityBaseRepository<ModeOfConvenience> _modeOfConvenienceTypeRepository;

        private IEntityBaseRepository<tblParticipantInfo> _participantRepository;
        private IEntityBaseRepository<tblParentParticipantMapping> _parentParticipantMappingRepository;
        private readonly IEntityBaseRepository<tblParentInfo> _tblParentInfoRepository;
        public CommonService(
            IEntityBaseRepository<Country> countryRepository,
            IEntityBaseRepository<FamilyInfo> familyRepository,
            IEntityBaseRepository<State> stateRepository,
            IEntityBaseRepository<Status> statusleaveRepository,
            IEntityBaseRepository<City> cityRepository,
            IEntityBaseRepository<ClassInfo> classInfoRepository,
            IEntityBaseRepository<Position> designationInfoRepository,
            IEntityBaseRepository<Room> roomInfoRepository,
            IEntityBaseRepository<LessonPlan> lessonPlanInfoRepository,
            IEntityBaseRepository<StaffInfo> staffInfoRepository,
            IEntityBaseRepository<tblParentInfo> parentRepository,
            IEntityBaseRepository<StaffScheduleInfo> staffScheduleInfoRepository,
             IEntityBaseRepository<FeeType> feesTypeRepository,
             IEntityBaseRepository<PricingPlan> pricingPlanRepository,
             IEntityBaseRepository<AgencyLocationInfo> agencyLocationInfoRepository,
             IEntityBaseRepository<SMSCarrier>  smsCarrierInfoRepository,
             IEntityBaseRepository<TimeClockUsersPlan> timeClockUsersPlanInfoRepository,
            IEntityBaseRepository<FamilyStudentMap> familystudentMapRepository,
            IEntityBaseRepository<StudentSchedule>  studentScheduleRepository,
            IEntityBaseRepository<AgencyRegistrationInfo> agencyRegistrationInfoRepository,
            IEntityBaseRepository<ParentInfo> parentInfoRepository,
            IEntityBaseRepository<Users> usersInfoRepository,
             IEntityBaseRepository<Entities.ToDoTask.Priority> priorityRepository,
             IEntityBaseRepository<Holiday> holidayRepository,
             IEntityBaseRepository<DailyStatusCategory> DailyStatusCategoryRepository,
            IEntityBaseRepository<StudentAttendanceInfo> StudentAttendanceRepository,
            IEntityBaseRepository<Position> positionRepository,
            IEntityBaseRepository<StudentDetail>  studentDetailsRepository,
            IEntityBaseRepository<MealType> mealTypeRepository,
            IEntityBaseRepository<FoodManagementMaster> foodManagementMasterRepository,
            IEntityBaseRepository<MealItemMaster> mealItemMasterRepository,
               IEntityBaseRepository<MealServeSize> mealServeSizeRepository,
        IEntityBaseRepository<MealServeQuantity> mealServeQuantityRepository,
        IEntityBaseRepository<MealServeType> mealServeTypeRepository,
        IEntityBaseRepository<ModeOfConvenience> modeOfConvenienceRepository,
        IEntityBaseRepository<tblParticipantInfo> participantRepository,
        IEntityBaseRepository<tblParentParticipantMapping> parentParticipantMappingRepository,
        IEntityBaseRepository<tblParentInfo> tblParentInfoRepository
            )
        {
            _countryRepository = countryRepository;
            _familyRepository = familyRepository;
            _stateRepository = stateRepository;
            _cityRepository = cityRepository;
            _classInfoRepository = classInfoRepository;
            _parentRepository = parentRepository;
            _designationInfoRepository = designationInfoRepository;
            _roomInfoRepository = roomInfoRepository;
            _lessonPlanInfoRepository = lessonPlanInfoRepository;
            _staffInfoRepository = staffInfoRepository;
            _staffScheduleInfoRepository = staffScheduleInfoRepository;
            _feesTypeRepository = feesTypeRepository;
            _pricingPlanRepository = pricingPlanRepository;
            _agencyLocationInfoRepository = agencyLocationInfoRepository;
            _smsCarrierInfoRepository = smsCarrierInfoRepository;
            _holidayRepository = holidayRepository;
            _timeClockUsersPlanInfoRepository = timeClockUsersPlanInfoRepository;
            _familystudentMapRepository = familystudentMapRepository;
            _studentScheduleRepository = studentScheduleRepository;
            _agencyRegistrationInfoRepository = agencyRegistrationInfoRepository;
            _parentInfoRepository = parentInfoRepository;
            _usersInfoRepository = usersInfoRepository;
            _DailyStatusCategoryRepository = DailyStatusCategoryRepository;
            _StudentAttendanceRepository = StudentAttendanceRepository;
            _positionRepository = positionRepository;
            _studentDetailsRepository = studentDetailsRepository;
            _mealTypeRepository = mealTypeRepository;
            _statusLeaveRepository = statusleaveRepository;
            _foodManagementMasterRepository = foodManagementMasterRepository;
            _mealItemMasterRepository = mealItemMasterRepository;
            _mealServeSizeRepository = mealServeSizeRepository;
            _mealServeQuantityRepository = mealServeQuantityRepository;
            _mealServeTypeRepository = mealServeTypeRepository;
            _modeOfConvenienceTypeRepository = modeOfConvenienceRepository;
            _participantRepository = participantRepository;
            _parentParticipantMappingRepository = parentParticipantMappingRepository;
            _tblParentInfoRepository = tblParentInfoRepository;
        }

        /// <summary>
        ///     Gets the country list.
        /// </summary>
        /// <returns></returns>
        public List<CountryViewModel> GetCountryList()
        {
            List<CountryViewModel> countryViewModel;
            try
            {
                var tempData = _countryRepository.GetAll().ToList();
                countryViewModel = (from c in tempData
                    select new CountryViewModel
                    {
                        ID = c.ID,
                        CountryName = c.CountryName,
                        CountryCode = c.CountryCode,
                        IsDeleted = c.IsDeleted
                    }).ToList();
            }
            catch (Exception)
            {
                return new List<CountryViewModel>();
            }
            return countryViewModel;
        }

        /// <summary>
        ///     Gets the state by country identifier.
        /// </summary>
        /// <param name="countryId">The country identifier.</param>
        /// <returns></returns>
        public List<StateViewModel> GetStateByCountryId(long countryId)
        {
            List<StateViewModel> stateViewModel;
            try
            {
                List<State> stateList;
                if (countryId > 0)
                    stateList = _stateRepository.FindBy(x => x.CountryId == countryId).ToList();
                else
                    stateList = _stateRepository.GetAll().ToList();
                stateViewModel = (from c in stateList
                    select new StateViewModel
                    {
                        ID = c.ID,
                        CountryId = c.CountryId,
                        StateName = c.StateName
                    }).ToList();
            }
            catch (Exception)
            {
                return new List<StateViewModel>();
            }
            return stateViewModel;
        }

        /// <summary>
        ///     Gets the city by country identifier.
        /// </summary>
        /// <param name="stateId">The state identifier.</param>
        /// <returns></returns>
        public List<CityViewModel> GetCityByCountryId(long stateId)
        {
            List<CityViewModel> cityViewModel;
            try
            {
                var tempData = _cityRepository.FindBy(x => x.StateId == stateId).ToList();
                cityViewModel = (from c in tempData
                    select new CityViewModel
                    {
                        ID = c.ID,
                        CityName = c.name,
                        StateId = c.StateId
                    }).ToList();
            }
            catch (Exception)
            {
                return new List<CityViewModel>();
            }
            return cityViewModel;
        }

        public bool IsExistClassName(string name,long classId,long agencyId)
        {
            var isExist = false;
            try
            {
                if (classId != 0)
                {
                    //Update 
                    var tempData = _classInfoRepository.FindBy(x => x.ClassName.ToLower() == name.ToLower() && x.ID !=classId && x.AgencyId == agencyId).FirstOrDefault();
                    if (tempData != null && tempData.ID > 0)
                    {
                            return isExist = true;
                    }
                    else
                    {
                        return isExist = false;
                    }
                }
                else
                {
                    //Add
                    var tempData = _classInfoRepository.FindBy(x => x.ClassName.ToLower() == name.ToLower() && x.AgencyId == agencyId).FirstOrDefault();
                    if (tempData != null && tempData.ID > 0)
                    {
                        return isExist = true;
                    }
                    else
                    {
                        return isExist = false;
                    }

                }

            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;
        }

        public bool IsExistSecurityCode(string name)
        {
            var isExist = false;
            try
            {
                var tempData = _parentRepository.FindBy(x =>x.SecurityKey  == name).FirstOrDefault();
                if (tempData != null && tempData.ID > 0)
                    isExist = true;
            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;
        }
        public bool IsExistMobileNumber(string name, long agencyId)
        {
            var isExist = false;
            try
            {
                var FamilyData = _familyRepository.FindBy(x => x.AgencyId == agencyId).Select(x => x.ID).ToList();
                
                foreach (var item in FamilyData.ToList())
                {
                    var tempData = _parentInfoRepository.FindBy(x => x.Mobile == name && x.FamilyId == item).FirstOrDefault();
                    if (tempData != null && tempData.ID > 0)
                    { isExist = true;
                        return isExist;
                    }
                }
                
            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;
        }
        public bool IsExistTitle(string name,long agencyId )
        {
            var isExist = false;
            try
            {
                var tempData = _holidayRepository.FindBy(x => x.Title == name && x.AgencyId == agencyId).FirstOrDefault();
                if (tempData != null && tempData.ID > 0)
                    isExist = true;
            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;
        }

        public bool FileSize(long file)
        {
            
            var isExist = false;
            try
            {
               if(file > 2097152)
                {
                    return isExist = true;
                }
                else
                {
                    return isExist = false;
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;

        }
        public bool IsExistStaffUsername(string name,long agencyId,long staffId)
        {
            var isExist = false;
            try
            {
              
                var tempData = _staffInfoRepository.FindBy(x => x.Email == name && x.AgencyRegistrationId == agencyId).FirstOrDefault();
                if (tempData != null && tempData.ID > 0)
                {
                    if(staffId == tempData.ID)
                    {
                        return isExist = false;
                    }
                    else
                    {
                        return isExist = true;

                    }
                   
                }
                else
                {
                    return isExist = false;
                }
                
                    
                
            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;
        }

        public bool IsExistParentUsername(string name)
        {
            var isExist = false;
            try
            {

                var tempData = _usersInfoRepository.FindBy(x => x.EmailId == name).FirstOrDefault();
                if (tempData != null && tempData.ID > 0)
                {
                        return isExist = true;
                  
                }
                else
                {
                    return isExist = false;
                }



            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;
        }

        
        /// <summary>
        ///     Calculate Age from DateofBirth.
        /// </summary>
        /// <param name="dob">Date of Birth.</param>
        /// <returns>Age.</returns>
        public string CalculateAge(DateTime dob)
        {
            var today = DateTime.Today;

            var months = today.Month - dob.Month;
            var years = today.Year - dob.Year;

            if (today.Day < dob.Day)
            {
                months--;
            }

            if (months < 0)
            {
                years--;
                months += 12;
            }


            var days = (today - dob.AddMonths((years*12) + months)).Days;

            return
                $"{years} year{((years == 1) ? "" : "s")}, {months} month{((months == 1) ? "" : "s")} and {days} day{((days == 1) ? "" : "s")}";
        }

        /// <summary>
        ///     Gets the designations.
        /// </summary>
        /// <returns></returns>
        public List<PositionViewModel> GetPositions(long agencyId)
        {
            var designationData = _designationInfoRepository.GetAll().Where(t => t.AgencyId == agencyId);
            var designationViewModel = (from c in designationData
                select new PositionViewModel
                {
                    ID = c.ID,
                    Name = c.Name
                }).ToList();
            //var MapViewMdel = Mapper.Map<Designation, DesignationViewModel>(designationData);
            return designationViewModel;
        }

        public ResponseViewModel GetFeesTypes(FeeTypeViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var feesTypeData = _feesTypeRepository.GetAll().Where(t => t.AgencyId == vmModel.AgencyId).ToList();
                var resultData = Mapper.Map<List<FeeType>, List<FeeTypeViewModel>>(feesTypeData);
                response.Content = resultData;
                response.IsSuccess = true;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
            }

            return response;
        }

        public ResponseViewModel GetClass_Room_Lesson(SearchFieldsViewModel SearchVM)
        {
            var resultData = new ResponseViewModel();
            try
            {

                var staffList =
                    _staffInfoRepository.GetAll()
                        .Where(m => m.IsTimeClockUser== SearchVM.IsTimeClockUser && m.AgencyRegistrationId==SearchVM.AgencyId)
                        .Select(m => new {Name = m.FirstName + " " + m.LastName, m.ID});

                var roomList =
                    _roomInfoRepository.GetAll().Where(m => m.AgencyId== SearchVM.AgencyId).Select(m => new {m.Name, m.ID});
                var classList =
                    _classInfoRepository.GetAll().Where(m => m.AgencyId == SearchVM.AgencyId).Select(m => new {m.ID, m.ClassName});
                var lessonPlanList =
                    _lessonPlanInfoRepository.GetAll().Where(m => m.AgencyId == SearchVM.AgencyId).Select(m => new {m.ID, m.Name});
                resultData.Content = new {roomList, classList, lessonPlanList, staffList};
                resultData.IsSuccess = true;
                return resultData;
            }
            catch (Exception ex)
            {
                resultData.Message = ex.Message;
                return resultData;
            }
        }

        public ResponseViewModel GetStaffSchedule()
        {
            var resultData = new ResponseViewModel();
            try
            {
                var staffScheduleList =
                    _staffScheduleInfoRepository.GetAll()
                        .Where(m => m.IsDeleted == false)
                        .Select(m => new {Name = m.Staff.FirstName + " " + m.Staff.LastName, m.ID})
                        .ToList();
                resultData.Content = new {staffScheduleList};
                //resultData.Content = staffScheduleList.ToList();
                resultData.IsSuccess = true;
                return resultData;
            }
            catch (Exception ex)
            {
                resultData.Message = ex.Message;
                return resultData;
            }
        }

        public ResponseViewModel GetClass_Room_LessonByAgency(long agencyId = 0)
        {
            var resultData = new ResponseViewModel();
            try
            {
                if (agencyId > 0)
                {
                    var classList =
                        _classInfoRepository.GetAll()
                            .Where(m => m.IsDeleted == false && m.AgencyId == agencyId)
                            .Select(m => new {m.ID, m.ClassName, m.Fees, m.FeeType.Name});
                    resultData.Content = new {classList};
                }
                else
                {
                    var classList =
                        _classInfoRepository.GetAll()
                            .Where(m => m.IsDeleted == false)
                            .Select(m => new {m.ID, m.ClassName});
                    resultData.Content = new {classList};
                }
                resultData.IsSuccess = true;
                return resultData;
            }
            catch (Exception ex)
            {
                resultData.Message = ex.Message;
                return resultData;
            }
        }


        public void SendEmail(string mailTo, string emailSubject, string emailMessage)
        {
            try
            {
                List<string> toMails = new List<string>();
                toMails.Clear();
                toMails.Add(mailTo);
                Email smtp1 = new Email();
                smtp1.Body = emailMessage;
                smtp1.Subject = emailSubject;
                smtp1.To = toMails;
                smtp1.Format = BodyFormat.HTML;
                smtp1.Send(true);
            }
            catch (Exception)
            {
            }
        }

      
        public bool InsertDefaultValue(long agencyId,long agencyLocationId)
        {
            var context = new CDCContext();

            try
            {
                ////Family Masters
                RelationshipTypeData.GetRelationshipType(context, agencyId);
                InfoSourceData.GetInfoSource(context, agencyId);
                MembershipTypeData.GetMembershipType(context, agencyId);
                GradeLevelData.GetGradeLevel(context, agencyId);

                //class Masters
                CategoryData.GetCategory(context, agencyId);
                RoomData.GetRoom(context, agencyId, agencyLocationId);
                SessionData.GetSession(context, agencyId);

                //staff Masters
                DesignationMasterData.GetDesignationMasterData(context, agencyId);
                GeneralDroppedByData.GetGeneralDroppedByData(context, agencyId);
                //_unitOfWork.Commit();
                //Student Masters
                FeesTypeData.GetFeesTypeData(context, agencyId);

                PriorityMasterData.GetPriorityMasterData(context, agencyId);
                SkillMasterData.GetSkillMaster(context, agencyId);
                ClassStatusMasterData.GetClassStatus(context, agencyId);

                ///'For Food Meal Item Master
                MealItemMasterData.GetMealItemMasterData(context, agencyId);
                ParticipantAssociationMasterData.GetParticipantAssociationMasterData(context, agencyId);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public ResponseViewModel GetTypes()
        {
            var resultData = new ResponseViewModel();
            try
            {
                var staffList =
                    _staffInfoRepository.GetAll()
                        .Where(m => m.IsDeleted == false)
                        .Select(m => new {Name = m.FirstName + " " + m.LastName, m.ID});
                var roomList =
                    _roomInfoRepository.GetAll().Where(m => m.IsDeleted == false).Select(m => new {m.Name, m.ID});
                var classList =
                    _classInfoRepository.GetAll().Where(m => m.IsDeleted == false).Select(m => new {m.ID, m.ClassName});
                var lessonPlanList =
                    _lessonPlanInfoRepository.GetAll().Where(m => m.IsDeleted == false).Select(m => new {m.ID, m.Name});
                resultData.Content = new {roomList, classList, lessonPlanList, staffList};
                resultData.IsSuccess = true;
                return resultData;
            }
            catch (Exception ex)
            {
                resultData.Message = ex.Message;
                return resultData;
            }
        }
        public ResponseViewModel GetPricingPlans(PricingPlanViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var pricingPlanData = _pricingPlanRepository.GetAll().ToList();
                var ResultData = Mapper.Map<List<PricingPlan>, List<PricingPlanViewModel>>(pricingPlanData);
                if (vmModel != null && vmModel.IsFromUpdate == true)
                    ResultData = ResultData.Where(m => m.IsTrial != true).ToList();
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }


        //public ResponseViewModel GetStaffList(long agencyId = 0)
        public ResponseViewModel GetStaffList(IDViewModel model)
        {
            var resultData = new ResponseViewModel();
            try
            {
                if (model.ID > 0)
                {
                    var staffList = _staffInfoRepository.FindBy(f => f.AgencyRegistrationId == model.ID && f.ID==model.StaffId).Select(m => new { m.ID, Name = m.FirstName + " " + m.LastName }).ToList();
                    resultData.Content = staffList;
                    resultData.IsSuccess = true;
                    resultData.Message = "Staff Details Retrieved";
                }
                else
                {
                    resultData.IsSuccess = false;
                    resultData.Message = "Please try again";
                }
            }
            catch (Exception ex)
            {
                resultData.IsSuccess = false;
                resultData.Message = "Please try again";
            }
            return resultData;
        }
        public List<ParentInfo> GetStudentPrimaryParentDetail(int? CLassId, int? agencyId)
        {
            var resultData = new ResponseViewModel();
            List<ParentInfo> EmailId = new List<ParentInfo>();
            try
            {
                if (CLassId > 0)
                {
                    List<long> FamilyList = new List<long>();

                    var studentList = _studentScheduleRepository.FindBy(f => f.ClassId == CLassId&&f.IsEnrolled==true).Select(m => m.StudentId).ToList();
                    foreach (var item in studentList)
                    {
                        FamilyList.Add(_familystudentMapRepository.FindBy(m => m.StudentId == item.Value).Select(m => m.FamilyId).FirstOrDefault());
                    }
                    FamilyList=FamilyList.Distinct().ToList();
                    foreach (var item in FamilyList)
                    {
                        EmailId.Add( _parentInfoRepository.FindBy(m => m.FamilyId == item && m.IsPrimary == true).FirstOrDefault());
                    }
                    //EmailId.AddRange(_agencyRegistrationInfoRepository.FindBy(m => m.ID == agencyId).Select(f => f.Email).ToList());
                }
                EmailId=EmailId.GroupBy(x => x.EmailId).Select(g => g.First()).ToList();
                return EmailId;

            }
            catch (Exception ex)
            {
               

                throw ex;
            }
        }

        #region TimeSheet
        public DateTime? ConvertToUTC(DateTime value, string timeZone = null)
        {
            if (string.IsNullOrEmpty(timeZone))
            {
                return DateTime.FromFileTimeUtc(value.ToFileTime());
            }

            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            //DateTimeOffset localServerTime = DateTimeOffset.Now;

            //DateTimeOffset usersTime = TimeZoneInfo.ConvertTime(localServerTime, tz);

            //DateTimeOffset utc = localServerTime.ToUniversalTime();
            return TimeZoneInfo.ConvertTimeToUtc(value, tz);
        }
        
        public DateTime? ConvertFromUTC(DateTime value, string timeZone = null)
        {
            TimeZoneInfo tz;
            if (string.IsNullOrEmpty(timeZone))
            {
                return DateTime.FromFileTime(value.ToFileTimeUtc());
            }
            if (timeZone == "Eastern Daylight Time")
            {
                string displayName = "(GMT-05:00) Eastern Standard Time";
                string standardName = "Eastern Standard Time";
                TimeSpan offset = new TimeSpan(04, 00, 00);
                tz = TimeZoneInfo.CreateCustomTimeZone(standardName, offset, displayName, standardName);
            }
            else
            {
                tz = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            }
            return TimeZoneInfo.ConvertTimeFromUtc(value, tz);
        }
        public TimeSpan? ConvertTimeFromUTC(DateTime dt,TimeSpan ts, string timeZone)
        {
            TimeSpan newts = new TimeSpan();
            TimeZoneInfo mawson;
            try
            {               
                if (timeZone == "Eastern Daylight Time")
                {
                    string displayName = "(GMT-05:00) Eastern Standard Time";
                    string standardName = "Eastern Standard Time";
                    TimeSpan offset = new TimeSpan(04, 00, 00);
                    mawson = TimeZoneInfo.CreateCustomTimeZone(standardName, offset, displayName, standardName);

                }
                else
                {
                    mawson = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                }
                dt = Convert.ToDateTime(ts.ToString());
                DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(dt, mawson);
                newts = cstTime.TimeOfDay;
            }
            catch (Exception ex)
            {

            }
            return newts;
        }

        public ReadOnlyCollection<TimeZoneInfo> GetTimezones()
        {
            ReadOnlyCollection<TimeZoneInfo> tz;
            tz = TimeZoneInfo.GetSystemTimeZones();

            return tz;
        }
        #endregion
        public ResponseViewModel GetLocations(long agencyId)
        {
            var response = new ResponseViewModel();
            try
            {
                var agencyLocations = _agencyLocationInfoRepository.GetAll().Where(m=>m.AgencyRegistrationId==agencyId).ToList();
                var ResultData = Mapper.Map<List<AgencyLocationInfo>, List<LocationViewModel>>(agencyLocations);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;

                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetSMSCarrier()
        {
            var response = new ResponseViewModel();
            try
            {
                var smsCarriers = _smsCarrierInfoRepository.GetAll().Where(m => m.IsDeleted== false).ToList();
                var ResultData = Mapper.Map<List<SMSCarrier>, List<SMSCarrierViewModel>>(smsCarriers);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetTimeClockUsersPricingPlans(TimeClockUsersViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var pricingPlanData = _timeClockUsersPlanInfoRepository.GetAll().ToList();
                var ResultData = Mapper.Map<List<TimeClockUsersPlan>, List<TimeClockUsersViewModel>>(pricingPlanData);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }


        public ResponseViewModel GetAllEnrollList(SearchStudentScheduleViewModel model)
        {
            var response = new ResponseViewModel();
            List<long> studentIds;
            //var predicate = PredicateBuilder.True<StudentSchedule>();
            try
            {
                List<StudentSchedule> resultData;
                studentIds =
                    _familystudentMapRepository.FindBy(f => f.Family.AgencyId == model.AgencyId)
                        .Select(s => s.StudentId)
                        .ToList();
                if (model.IsEnrolled == true)
                    resultData = _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value) && f.IsEnrolled == true).ToList();
                else
                    resultData = _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value)).ToList();
                //resultData = resultData.Skip((model.page - 1) * model.limit).Take(model.limit).ToList();

                studentIds =
                        _familystudentMapRepository.GetAll()
                            .Where(w => w.Family.AgencyId == model.AgencyId)
                            .Select(m => m.StudentId)
                            .ToList();
                //resultData =
                //    _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value))
                //        .Where(x => x.StartDate <= DateTime.Today)
                //        .ToList();

                //resultData = resultData.Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var responseData = Mapper.Map<List<StudentSchedule>, List<EnrollListViewModel>>(resultData);
                response.TotalEnrolledParticipants = resultData.Count;
                response.IsSuccess = true;
                response.Message = "Data Retrieved Successfully";
                //response.TotalRows = responseData.Count;
                response.Content = responseData;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Data Retrieved Successfully";
                return response;
            }
        }
        public ResponseViewModel GetPricingPlanCount(SearchFieldsViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                var resultAgencyRegisterInfo = _agencyRegistrationInfoRepository.GetSingle(model.AgencyId);
                var returnAgencyViewModelRegisterInfo =
                    Mapper.Map<AgencyRegistrationInfo, AgencyRegistrationViewModel>(resultAgencyRegisterInfo);

                response.MaximumParticipants = returnAgencyViewModelRegisterInfo.PricingPlan.MaxNumberOfParticipants;
                response.Content = returnAgencyViewModelRegisterInfo;
                response.IsSuccess = true;
                return response;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                return response;
            }
        }
        public ResponseViewModel GetAllParentList(IDViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var parentData = _parentInfoRepository.GetAll().Where(m=>m.Family.AgencyId==vmModel.AgencyId).ToList();
                var ResultData = Mapper.Map<List<ParentInfo>, List<ParentInfoViewModel>>(parentData);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }

        
        public bool IsExistEmailId(string emailId)
        {
            var isEmailExist = false;
            try
            {
                var tempData = _usersInfoRepository. FindBy(x => x.EmailId == emailId ).FirstOrDefault();
                if (tempData != null)
                    isEmailExist = true;
            }
            catch (Exception)
            {
            }
            return isEmailExist;
        }


        public ResponseViewModel CheckDeleteClass(IDViewModel model)
        {
            var response = new ResponseViewModel();

            try
            {
                var tempData = _studentScheduleRepository.FindBy(x => x.ClassId == model.ClassId);
                if (tempData.Count() > 0)
                {
                    response.IsSuccess = true;
                    response.Message = "Class is  enrolled";
                }
                else
                {
                    response.Message = "Class is not enrolled";
                    response.IsSuccess = false;
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return response;
        }

        public ResponseViewModel CheckParticipantDeleteClass(SearchStudentScheduleViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                var tempData = _studentScheduleRepository.FindBy(x => x.StudentId == model.StudentId);
                if (tempData.Count() > 0)
                {
                    response.IsSuccess = true;
                    response.Message = "Class is enrolled";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Class is not enrolled";

                }
            }
            catch (Exception)
            {

            }
            return response;
        }
    
        public ResponseViewModel GetAllParticipants(IDViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var studentIds =
                       _participantRepository.GetAll()
                           .Where(w => w.AgencyId == vmModel.AgencyId)
                           .Select(m => m.ID)
                           .ToList();
                var resultData = _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value))                        
                       .ToList();
                var responseData = Mapper.Map<List<StudentSchedule>, List<StudentScheduleViewModel>>(resultData);

                List<string> classes = new List<string>();
                responseData.ForEach(x =>
                {
                    classes.Add(x.ClassName);
                });
                responseData=responseData.GroupBy(k=>k.StudentId).Select(g=>g.First()).ToList();
                var data = responseData.Select(m => new { name = m.StudentInfo.FirstName+" "+ m.StudentInfo.LastName, image = m.StudentInfo.ImagePath, email = "", studentId = m.StudentId,Age=CalculateAge(m.StudentInfo.DateOfBirth.Value), _lowername=(m.StudentInfo.FirstName+" "+m.StudentInfo.LastName).ToLower(),ClassName=m.ClassName,classList= classes }).Distinct();
                response.Content = data;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetAllOtherParticipants(IDViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var studentIds =
                       _familystudentMapRepository.GetAll()
                           .Where(w => w.Family.AgencyId == vmModel.AgencyId && w.StudentId !=vmModel.StudentId)
                           .Select(m => m.StudentId)
                           .ToList();
                var resultData =
                    _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value))
                        .Where(x => x.StartDate <= DateTime.Today && x.IsEnrolled == true)
                        .ToList();



                var responseData = Mapper.Map<List<StudentSchedule>, List<EnrollListViewModel>>(resultData);
                var data = responseData.Select(m => new { name = m.StudentFullName, image = m.ImagePath, email = "", studentId = m.StudentId, Age = CalculateAge(m.DateOfBirth.Value), _lowername = m.StudentFullName.ToLower() }).Distinct();
                response.Content = data;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        
        public ResponseViewModel GetAllStaff(IDViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var staffList =
                       _staffInfoRepository.All.Where(m => m.AgencyRegistrationId == vmModel.AgencyId)
                           .ToList();
                var responseData = Mapper.Map<List<StaffInfo>, List<StaffViewModel>>(staffList);
                response.Content = responseData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetParentInformation(IDViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var parentIds =_parentParticipantMappingRepository.All.Where(m => m.NewParticipantInfoID == vmModel.StudentId).Select(m => m.NewParentInfoID).ToList();
                var parentInfoList = _tblParentInfoRepository.All.Where(m => parentIds.Contains( m.ID)).ToList();       
                var responseData = Mapper.Map<List<tblParentInfo>,List<tblParentInfoViewModel>>(parentInfoList);
                response.Content = responseData.Select(m => new {Email=m.EmailId, Name = m.FirstName + " " + m.LastName, ParentId = m.ID, PhoneNumber = m.Mobile});
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetDailyStatusCategory(IDViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var dailyStatusData =
                       _DailyStatusCategoryRepository.All.ToList();

                var responseData = Mapper.Map<List<DailyStatusCategory>, List<DailyStatusCategoryViewModel>>(dailyStatusData);
                response.Content = responseData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetPositionNameById(IDViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                var positionData =_positionRepository.GetAll().Where(x=>x.AgencyId==model.AgencyId && x.ID==model.PositionId).FirstOrDefault();
                var responseData = Mapper.Map<Position,PositionViewModel>(positionData);
                response.Content = responseData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }

        public ResponseViewModel GetParticipantInformation(IDViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                var studentData = _studentDetailsRepository.FindBy(m=>m.studentId==model.StudentId).FirstOrDefault();
                var responseData = Mapper.Map<StudentDetail, StudentDetailViewModel>(studentData);
                response.Content = responseData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }

        //public ResponseViewModel GetCategories()
        //{
        //    var response = new ResponseViewModel();
        //    try
        //    {
        //        var tempData = _CategoryRepository.GetAll().ToList();
        //        var modellst = Mapper.Map<List<Category>, List<CategoryViewModel>>(tempData);
        //        //if (modellst.Count() > 0)
        //        //{
        //        //    response.IsSuccess = true;
        //        //    response.Message = "No Categories Defined";
        //        //}
        //        //else
        //        //{
        //        //    response.IsSuccess = false;
        //        //    response.Message = "";

        //        //}
        //        response.Content = modellst;
        //        response.IsSuccess = true;
        //    }
        //    catch (Exception)
        //    {
        //        response.IsSuccess = false;
        //    }
        //    return response;
        //}
        public ResponseViewModel GetClassCombo(SearchFieldsViewModel SearchVM)
        {
            var resultData = new ResponseViewModel();
            try
            {

               
                var classList =
                    _classInfoRepository.GetAll().Where(m => m.AgencyId == SearchVM.AgencyId).Select(m => new { m.ID, m.ClassName });
           
                resultData.Content = new { classList };
                resultData.IsSuccess = true;
                return resultData;
            }
            catch (Exception ex)
            {
                resultData.Message = ex.Message;
                return resultData;
            }
        }
        public ResponseViewModel GetMealType(SearchFieldsViewModel obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var mealTypeData = _mealTypeRepository.GetAll().Where(m => m.IsDeleted == false).ToList();
                var ResultData = Mapper.Map<List<MealType>, List<MealTypeViewModel>>(mealTypeData);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetFoodMaster(SearchFieldsViewModel obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var Data = _foodManagementMasterRepository.GetAll().Where(m => m.IsDeleted == false && m.AgencyId==obj.AgencyId).ToList();
                //Data = _foodManagementMasterRepository.GetAll().ToList();
                var ResultData = Mapper.Map<List<FoodManagementMaster>, List<FoodManagementMasterViewModel>>(Data);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }

        public ResponseViewModel GetFoodItems(long AgencyId, long MealTypeId)
        {
            var response = new ResponseViewModel();
            try
            {
                var Data = _foodManagementMasterRepository.GetAll().Where(m => m.IsDeleted == false && m.AgencyId == AgencyId && m.MealTypeId==MealTypeId).ToList();
                //Data = _foodManagementMasterRepository.GetAll().ToList();
                var ResultData = Mapper.Map<List<FoodManagementMaster>, List<FoodManagementMasterViewModel>>(Data);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetMealItemMaster(SearchFieldsViewModel obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var mealItemData = _mealItemMasterRepository.GetAll().Where(m => m.IsDeleted == false && m.AgencyId==obj.AgencyId).ToList();
                var ResultData = Mapper.Map<List<MealItemMaster>, List<MealItemViewModel>>(mealItemData);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel getLeaveStatusData(SearchFieldsViewModel obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var statusData = _statusLeaveRepository.GetAll().Where(m => m.IsDeleted == false).ToList();
                var ResultData = Mapper.Map<List<Status>, List<StatusViewModel>>(statusData);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetMealServeSize(SearchFieldsViewModel obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var mealItemData = _mealServeSizeRepository.GetAll().Where(m => m.IsDeleted == false).ToList();
                var ResultData = Mapper.Map<List<MealServeSize>, List<MealServeSizeViewModel>>(mealItemData);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetMealServeQuantity(SearchFieldsViewModel obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var mealItemData = _mealServeQuantityRepository.GetAll().Where(m => m.IsDeleted == false).ToList();
                var ResultData = Mapper.Map<List<MealServeQuantity>, List<MealServeQuantityViewModel>>(mealItemData);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetMealServeTypes(SearchFieldsViewModel obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var mealItemData = _mealServeTypeRepository.GetAll().Where(m => m.IsDeleted == false).ToList();
                var ResultData = Mapper.Map<List<MealServeType>, List<MealServeTypeViewModel>>(mealItemData);
                response.Content = ResultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel CheckParticipantAttendance(SearchFieldsViewModelForParticipants obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var attendanceData = _StudentAttendanceRepository.FindBy(m => m.StudentId==obj.StudentId 
                && m.AttendanceDate.Day==obj.DateOfAccident.Value.Day 
                && m.AttendanceDate.Month == obj.DateOfAccident.Value.Month
                && m.AttendanceDate.Year == obj.DateOfAccident.Value.Year).ToList();
                if (attendanceData.Count > 0)
                    response.IsAttendee = true;
                else
                    response.IsAttendee = false;
                response.IsSuccess = true;

        }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public ResponseViewModel GetModeOfConvinience(SearchFieldsViewModel obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var resultData = _modeOfConvenienceTypeRepository.FindBy(m => m.AgencyId == obj.AgencyId).ToList();
                response.Content = resultData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
        public string RandomString(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
        public string GetMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (byte t in hash)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }
        public ResponseViewModel GetParentList(IDViewModel vmModel)
        {
            var response = new ResponseViewModel();
            try
            {
                List<long?> parentIds;
                if (vmModel.classIds!=null&& vmModel.classIds.Count>0)
                {
                    var studentIds = _studentScheduleRepository.FindBy(f => vmModel.classIds.Contains(f.ClassId) && f.IsEnrolled == true).Select(m => m.StudentId).ToList().Distinct();

                    parentIds = _parentParticipantMappingRepository.All.Where(m => studentIds.Contains(m.NewParticipantInfoID.Value)).Select(m => m.NewParentInfoID).Distinct().ToList();
                }
                else
                parentIds = _parentParticipantMappingRepository.All.Where(m => vmModel.participantIds.Contains(m.NewParticipantInfoID.Value)).Select(m => m.NewParentInfoID).Distinct().ToList();
                var parentInfoLst = _tblParentInfoRepository.All.Where(m => parentIds.Contains(m.ID)).ToList();
                var responseData = Mapper.Map<List<tblParentInfo>, List<tblParentInfoViewModel>>(parentInfoLst);
                response.Content = responseData;
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                throw ex;
            }

            return response;
        }
    }
}