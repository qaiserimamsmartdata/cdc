﻿using System.Collections.Generic;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.Student;

namespace CDC.Services.Abstract.Student
{
    /// <summary>
    ///     The Student Service
    /// </summary>
    public interface IStudentService
    {
        /// <summary>
        ///     Adds the student.
        /// </summary>
        /// <param name="studentViewModel">The student view model.</param>
        /// <returns></returns>
        int AddStudent(AddStudentViewModel studentViewModel);

        /// <summary>
        ///     Updates the student detail.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        int UpdateStudentDetail(AddStudentViewModel model);

        /// <summary>
        ///     Gets the student by identifier.
        /// </summary>
        /// <param name="studentId">The student identifier.</param>
        /// <returns></returns>
        dynamic GetStudentById(long studentId);

        /// <summary>
        ///     Gets all students.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        List<StudentViewModel> GetAllStudents(SearchStudentViewModel model);

        /// <summary>
        ///     Deletes the student by identifier.
        /// </summary>
        /// <param name="studentId">The student identifier.</param>
        /// <returns></returns>
        int DeleteStudentById(long studentId);
    }
}