﻿using CDC.ViewModel.Attendance;
using CDC.ViewModel.Common;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Attendance
{
    public interface IStudentAttendanceService
    {
        void GetStudentScheduleList(SearchStudentAttendanceViewModel model, out ResponseViewModel responseResult);
        void GetStudentAttendanceList(SearchStudentAttendanceViewModel model, out ResponseViewModel responseResult);
        void CheckParentSignIn(SearchStudentAttendanceViewModel model, out ResponseViewModel responseResult);
        void GetStudentAttendanceListTest(SearchStudentAttendanceViewModel model, out ResponseViewModel responseResult);
        void AddStudentAttendance(StudentAttendanceViewModel model, out ResponseInformation transaction);
        void UpdateStudentAttendance(StudentAttendanceViewModel model, out ResponseInformation responseInfo);
        void DeleteStudentAttendanceById(long studentAttendanceId, out ResponseInformation responseInfo);
        ResponseViewModel GetParentListByStudentId(IDViewModel model);
        ResponseViewModel MarkDropByAttendance(StudentAttendanceViewModel model);
        ResponseViewModel MarkPickUpByAttendance(StudentAttendanceViewModel model);
        ResponseViewModel GetAttendanceHistory(SearchAttendanceHistoryViewModel model);

        ResponseViewModel GetStudentAttendanceHistory(SearchStudentAttendanceHistoryViewModel model);
        void GetStudentFailedSignOutList(SearchStudentAttendanceViewModel model, out ResponseViewModel responseResult);
        ResponseViewModel CheckAttendanceForClass(StudentAttendanceViewModel model);
        //added for kiosk
        ResponseViewModel CheckAttendanceForClassKiosk(List<StudentAttendanceViewModel> model);
        void GetStudentAttendanceListForKiosk(SearchStudentAttendanceViewModel model, out ResponseViewModel responseResult);
        ResponseViewModel MarkDropByAttendanceKiosk(List<StudentAttendanceViewModel> model);
        ResponseViewModel MarkPickUpByAttendanceKiosk(List<StudentAttendanceViewModel> model);
        


        //ResponseViewModel GetParticipantFailedSignout(SearchStaffAttendanceViewModel model);
    }
}
