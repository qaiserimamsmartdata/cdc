﻿using CDC.ViewModel.Attendance;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.Attendance
{
    public interface IAttendanceService
    {
        //void GetStaffScheduleList(SearchStaffAttendanceViewModel model, out ResponseViewModel responseInfo);
        void GetStaffAttendanceList(SearchStaffAttendanceViewModel model, out ResponseViewModel responseInfo);
        //void GetStaffAttendanceListNew(SearchStaffAttendanceViewModel model, out ResponseViewModel responseInfo);
        ResponseViewModel AddAttendance(AttendanceViewModel model);
        void UpdateStaffAttendance(AttendanceViewModel model, out ResponseInformation responseInfo);
        void DeleteStaffAttendanceById(long staffAttendanceId, out ResponseInformation responseInfo);
        void GetStaffFailedSignOutList(SearchStaffAttendanceViewModel model, out ResponseViewModel responseResult);
    }
}
