﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using System;

namespace CDC.Services.Abstract.RazorEngine
{
    public interface IRazorService
    {
        string GetRazorTemplate(string template,string key, object item);
    }
}
