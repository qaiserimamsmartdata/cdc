﻿using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Common;
using CDC.ViewModel.SuperAdmin;

namespace CDC.Services.Abstract.AgencyRegistration
{
    /// <summary>
    ///     The Agency Registration Interface
    /// </summary>
    public interface IAgencyRegistrationService
    {
        AgencyRegistrationViewModel GetAgencyById(long agencyId);
        AgencyPaymentViewModel GetPaymentById(long agencyId);
        void UpdateAgency(AgencyRegistrationViewModel model, out ResponseInformation transaction);
        void UpdatePayment(AgencyPaymentViewModel model, out ResponseInformation transaction);
        ResponseViewModel FindAsync(string userName, string password);
        ResponseViewModel SetIsLoggedFirstTime(long agencyId);
        void AddAgency(AgencyRegistrationViewModel model, out ResponseInformation transaction);
        void GetDesignation(long agencyId);
        void UpdateAgencyByTimeClockUsers(AgencyRegistrationViewModel model, out ResponseInformation transaction);
        void UpdateAgencyPricingPlan(AgencyRegistrationViewModel model, out ResponseInformation transaction);
        void getSubscriptionPlan(long AgencyId, out ResponseViewModel responseInfo);

    }
}
