﻿using CDC.ViewModel.Common;
using CDC.ViewModel.ParticipantIncident;
using CDC.ViewModel.Staff;

namespace CDC.Services.Abstract.ParticipantIncident
{
    public interface IIncidentService
    {

        void GetAllIncident(SearchIncidentViewModel model,out ResponseViewModel responseInfo);
        void AddIncident(IncidentViewModel model, out ResponseInformation transaction);
        //void UpdateIncident(IncidentViewModel model, out ResponseInformation transaction);
        IncidentViewModel GetIncidentById(IDViewModel model);

        void UpdateIncidentById(IncidentIdViewModel model, out ResponseInformation responseInfo);
        void DeleteIncidentById(long incidentId, out ResponseInformation transaction);
    }
}
