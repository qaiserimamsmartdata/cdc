﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;

namespace CDC.Services.Abstract.Staff
{
    public interface IStaffService
    {

        /// <summary>
        /// Gets the staff by identifier.
        /// </summary>
        /// <param name="staffId">The staff identifier.</param>
        /// <returns></returns>
        StaffViewModel GetStaffById(long staffId);

        /// <summary>
        /// Gets all staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        void GetAllStaff(SearchStaffViewModel model,out ResponseViewModel responseInfo);

        /// <summary>
        /// Adds the staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        void AddStaff(StaffViewModel model, out ResponseInformation transaction);

        /// <summary>
        /// Updates the staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        void UpdateStaff(StaffViewModel model, out ResponseInformation transaction);

        /// <summary>
        /// Deletes the staff by identifier.
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="transaction">The transaction.</param>
        void DeleteStaffById(long agencyId, out ResponseInformation transaction);
    }
}
