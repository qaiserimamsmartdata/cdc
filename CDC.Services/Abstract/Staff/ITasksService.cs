﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Staff
{
    public interface ITasksService
    {
        void GetAllStaffs(SearchTasksViewModel model, out ResponseViewModel responseInfo);
        void GetAllClass(SearchTasksViewModel model, out ResponseViewModel responseInfo);

        void GetAllTasks(SearchTasksViewModel model, out ResponseViewModel responseInfo);
        void AddTasks(TasksViewModel model, out ResponseInformation transaction);
        void UpdateTasks(TasksViewModel model, out ResponseInformation transaction);
        void DeleteTasksById(TasksViewModel model, out ResponseInformation transaction);
        void GetAll(TasksViewModel model, out ResponseViewModel transaction);
    }
}
