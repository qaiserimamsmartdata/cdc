﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Staff
{
    public interface IEventService
    {
        void GetAllEvent(SearchEventViewModel model, out ResponseViewModel responseInfo);
        void AddEvent(EventViewModel model, out ResponseInformation transaction);
        void UpdateEvent(EventViewModel model, out ResponseInformation transaction);
        void DeleteEventById(long eventId, out ResponseInformation transaction);
    }
}
