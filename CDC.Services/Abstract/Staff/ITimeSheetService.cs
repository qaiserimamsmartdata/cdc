﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Staff
{
    public interface ITimeSheetService
    {
        #region [ Timesheet ]
        int DefaultPageSize { set; }
        /// <summary>
        /// This method is use to get all the time sheets.
        /// </summary>
        /// <param name="errorMessage">Capture the error message if occurs.</param>
        /// <returns></returns>
        TimesheetViewModel GetTimesheetsByStaffId(SearchTimesheetViewModel model);

        TimesheetViewModel GetTimesheetsByStaffIdAndTimeZone(SearchTimesheetViewModel model);

        /// <summary>
        /// This method is use to get the specific time sheets pagewise.
        /// </summary>
        /// <param name="staffID">Provide the staff ID to get the particular timesheet.</param>
        /// <param name="pageNo">Page number from where the time sheet to be fetched.</param>
        /// <param name="pageSize">Number of records in a page.</param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        TimesheetViewModel GetTimesheetsWithoutTimeZone(SearchTimesheetViewModel model);

        /// <summary>
        /// This method is use to get the specific time sheets pagewise.
        /// </summary>
        /// <param name="staffID">Provide the staff ID to get the particular timesheet.</param>
        /// <param name="timeZone">Provide the specific timezone.</param>
        /// <param name="pageNo">Page number from where the time sheet to be fetched.</param>
        /// <param name="pageSize">Number of records in a page.</param>
        /// <param name="fromDate">Date from which the timesheet requires.</param>
        /// <param name="toDate">Date to that the timesheet requires.</param>
        TimesheetViewModel GetTimesheetWithTimeZone(SearchTimesheetViewModel model);

        /// <summary>
        /// This method is use to save the employee check in/out data for day, lunch and tea.
        /// </summary>
        /// <param name="data"></param>
        TimesheetModel SaveCheckTime(TimeCheckModel data, string IPAddress, string timezone);

        /// <summary>
        /// This method is use to get the current status of staff checks.
        /// </summary>
        /// <param name="staffID"></param>
        /// <returns></returns>
        TimeCheckModel GetCurrentTimeCheck(int staffID);

        //Task<List<TimeCheckInOutOption>> GetTimeCheckInOutOptions();
        ReadOnlyCollection<TimeZoneInfo> GetTimezones();
        TimesheetModel SaveCheckFirstTimeFromStaffAttendance(TimeCheckModel data,DateTime AttDate,TimeSpan inTime, TimeSpan outTime, string IPAddress, string timezone);
        #endregion
    }
}
