﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;

namespace CDC.Services.Abstract.Staff
{
    public interface ILeaveServices
    {
        void GetAllLeave(SearchLeaveViewModel model, out ResponseViewModel responseInfo);
        //int AddLeave(LeaveViewModel leaveVm);
        //int UpdateLeaveDetail(LeaveViewModel model, long leaveId);
        //int DeleteLeaveById(long leaveId);

        void AddLeave(LeaveViewModel leaveVm, out ResponseInformation transaction);
        void UpdateLeaveDetail(LeaveViewModel model, out ResponseInformation transaction);
        void DeleteLeaveById(long leaveId, out ResponseInformation transaction);
    }
}
