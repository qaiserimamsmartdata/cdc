﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;

namespace CDC.Services.Abstract.Staff
{
    public interface IStaffScheduleService
    {
        void GetAllSchedule(SearchStaffScheduleViewModel model,out ResponseViewModel responseInfo);
        void GetAllStaffSchedule(SearchStaffScheduleViewModel model, out ResponseViewModel responseInfo);
        void GetAllScheduleStaffWise(SearchStaffScheduleViewModel model, out ResponseViewModel responseInfo);
        void AddScheduleStaff(StaffScheduleViewModel model, out ResponseInformation transaction);
        void UpdateScheduleStaff(StaffScheduleViewModel model, out ResponseInformation transaction);
        void DeleteStaffScheduleById(long staffScheduledId, out ResponseInformation transaction);
    }
}
