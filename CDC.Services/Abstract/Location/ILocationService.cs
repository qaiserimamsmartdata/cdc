﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Location;

namespace CDC.Services.Abstract.Location
{
    public interface ILocationService
    {

        /// <summary>
        /// Gets the location by identifier.
        /// </summary>
        /// <param name="locationId">The location identifier.</param>
        /// <returns></returns>
        LocationViewModel GetLocationById(long locationId);

        /// <summary>
        /// Gets all location.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        void GetAllLocation(SearchLocationViewModel model,out ResponseViewModel responseInfo);

        /// <summary>
        /// Adds the location.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        void AddLocation(LocationViewModel model, out ResponseInformation transaction);

        /// <summary>
        /// Updates the location.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        void UpdateLocation(LocationViewModel model, out ResponseInformation transaction);

        /// <summary>
        /// Deletes the location by identifier.
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="locationId">The transaction.</param>
        void DeleteLocationById(long locationId, out ResponseInformation transaction);
       


        /// <summary>
        ///Add/Updates the  Agency Locations.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        int AddUpdateLocation(LocationViewModel model);
    }
}
