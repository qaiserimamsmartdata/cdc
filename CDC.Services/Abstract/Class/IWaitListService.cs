﻿using CDC.Entities.Class;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.Class
{
    public interface IWaitListService
    {
        ResponseViewModel MapWaitList(WaitListMap obj);
        ResponseViewModel GetWaitListStudentList(WaitListMap obj);
        ResponseViewModel DeleteMappedWaitList(WaitListMap obj);
        ResponseViewModel SaveWaitListNotes(WaitListMap obj);
        bool IsStudentAllreadyinWaitList(WaitListMap obj);
    }
}
