﻿using System.Collections.Generic;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.Class
{
    /// <summary>
    ///     The Class Service Interface
    /// </summary>
    public interface IClassService
    {
        /// <summary>
        ///     Gets the class by identifier.
        /// </summary>
        /// <param name="classId">The class identifier.</param>
        /// <returns></returns>
        ClassViewModel GetById(long id,string timezone);

        /// <summary>
        ///     Gets all classes.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        void All(SearchClassViewModel model, out ResponseInformation resp);

        /// <summary>
        /// Adds the class.
        /// </summary>
        /// <param name="addClassViewModel">The add class view model.</param>
        /// <returns></returns>
        void Add(ClassViewModel model,out ResponseInformation resp);

        /// <summary>
        ///     Updates the class detail.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="classId">The class identifier.</param>
        /// <returns></returns>
        void Update(ClassViewModel model, out ResponseInformation resp);

        /// <summary>
        ///     Deletes the class by identifier.
        /// </summary>
        /// <param name="classId">The class identifier.</param>
        /// <returns></returns>
        void Delete(long id,out ResponseInformation resp);
    }
}