﻿using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.FoodManagementMaster;
using CDC.ViewModel.Tools.ManageHoliday;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Class
{
    public interface IFoodManagementMealPatternService
    {
        void GetAllFoodManagementMealPattern(SearchFoodMealPatternViewModel model, out ResponseViewModel responseInfo);
        FoodManagementMealPatternViewModel GetFoodMealPatternById(long FoodMealPatternId);
        void AddFoodMealPattern(FoodManagementMealPatternViewModel model, out ResponseInformation transaction);
        void UpdateFoodMealPattern(FoodManagementMealPatternViewModel model, out ResponseInformation responseInfo);
        void DeleteFoodMealPatternById(long FoodMealPatternId, out ResponseInformation responseInfo);
    }
}
