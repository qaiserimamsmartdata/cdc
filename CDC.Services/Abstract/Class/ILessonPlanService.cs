﻿using System.Collections.Generic;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.Class
{
    /// <summary>
    ///     The Lesson Plan Service interface
    /// </summary>
    public interface ILessonPlanService
    {
        /// <summary>
        ///     Gets the lesson plan.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        List<LessonPlanViewModel> GetLessonPlan(SearchLessonPlanViewModel model, out int totalCount);

        /// <summary>
        ///     Adds the update lesson plan.
        /// </summary>
        /// <param name="addLessonPlanViewModel">The add lesson plan view model.</param>
        /// <returns></returns>
        int AddUpdateLessonPlan(LessonPlanViewModel addLessonPlanViewModel);

        /// <summary>
        ///     Deletes the lesson plan by identifier.
        /// </summary>
        /// <param name="lessonPlanId">The lesson plan identifier.</param>
        /// <returns></returns>
        int DeleteLessonPlanById(long lessonPlanId);
    }
}