﻿using CDC.ViewModel.Class;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.Class
{
    public interface IEnrollService
    {
        /// <summary>
        /// Get Current Enroll List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>
        /// List
        /// .
        /// </returns>
        ResponseViewModel GetEnrollList(SearchStudentScheduleViewModel model);

        /// <summary>
        /// Gets the future enroll list.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        ResponseViewModel GetFutureEnrollList(SearchStudentScheduleViewModel model);

        /// <summary>
        /// Gets the participant waiting list by class id.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        ResponseViewModel GetParticipantWaitingListByClassId(SearchStudentScheduleViewModel model);
        /// <summary>
        /// Gets the un enrolled student list.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        ResponseViewModel GetUnEnrolledStudentList(SearchStudentScheduleViewModel model);
        /// <summary>
        /// Update future enrolled student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ResponseViewModel UpdateFutureEnrollList(EnrollListViewModel model);
        ResponseViewModel DeleteFutureEnrolledStudent(EnrollListViewModel model);
        ResponseViewModel CancelEnrollMent(long EnrollMentId);
        ResponseViewModel GetEnrollHistory(SearchStudentScheduleViewModel model);
    }
}
