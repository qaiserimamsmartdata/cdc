﻿using CDC.ViewModel.Class;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.Class
{
    public interface IInstructorsMappingService
    {
        /// <summary>
        /// Gets the instructor class map by identifier.
        /// </summary>
        /// <param name="instructorId">The instructor identifier.</param>
        /// <returns></returns>
        InstructorClassMapViewModel GetInstructorClassMapById(long instructorId);
        /// <summary>
        /// Adds the instructor class map.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        void AddInstructorClassMap(InstructorClassMapViewModel model, out ResponseInformation transaction);
        /// <summary>
        /// Updates the instructor class map.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo">The response information.</param>
        void UpdateInstructorClassMap(InstructorClassMapViewModel model, out ResponseInformation responseInfo);

        /// <summary>
        /// Deletes the instructor class map by identifier.
        /// </summary>
        /// <param name="instructorId">The instructor identifier.</param>
        /// <param name="responseInfo">The response information.</param>
        /// <param name="model"></param>
        ResponseViewModel DeleteInstructorClassMapById(InstructorClassMapViewModel model);
        /// <summary>
        /// Gets all instructor class map.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo">The response information.</param>
        void GetAllInstructorClassMap(SearchInstructorViewModel model, out ResponseViewModel responseInfo);
    }
}
