﻿using CDC.Entities.Common;
using CDC.ViewModel.Common;
using CDC.ViewModel.Family;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Family
{
    public interface IFamilyService
    {
        ResponseViewModel RegisterFamily(FamilyRegisterViewModel model);        
        ResponseViewModel AddFamilyInfo(FamilyInfoViewModel familyVm);
        ResponseViewModel SetIsLoggedFirstTime(long familyId);
        ResponseViewModel AddReferenceDetail(ReferenceDetailViewModel referenceVm);
        ResponseViewModel SaveParentInfo(List<ParentInfoViewModel> parentVm);
        ResponseViewModel SaveContactInfo(ContactInfoViewModel contactInfoVm);
        ResponseViewModel MapContactInfo(ContactInfoMap model);
        ResponseViewModel SaveStudentInfo(StudentInfoViewModel studentInfoVm);
        ResponseViewModel SaveStudentDetail(List<StudentDetailViewModel> studentDetailVm);
        ResponseViewModel DeleteParentInfo(ParentInfoViewModel model);
        ResponseViewModel GetFamilyInfo(SearchFamilyViewModel model);
        ResponseViewModel GetFamilyContactInfo(SearchFamilyViewModel model);
        ResponseViewModel GetFamilyStudentList(SearchFamilyViewModel model);
        ResponseViewModel AddParentAndContactInfo(FamilyRegisterViewModel model);
        ResponseViewModel DeleteStudentInfo(long studentInfoId);
        ResponseViewModel GetFamilyParentList(SearchFamilyViewModel model);
        ResponseViewModel GetFamilyStudentDetails(SearchFamilyViewModel model);
        ResponseViewModel SavePaymentInfo(PaymentInfoViewModel contactInfoVm);
    }
}
