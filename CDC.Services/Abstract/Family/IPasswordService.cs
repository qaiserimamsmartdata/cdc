﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Family
{
    public interface IPasswordService
    {
        string Generate();
        string Generate(int length);
        string Generate(int minLength, int maxLength);
    }
}
