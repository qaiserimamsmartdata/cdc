﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.SuperAdmin
{
    public interface ISuperAdminService
    {
        /// <summary>
        /// Gets all Agency List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        void GetAllAgency(SearchAgencyViewModel model, out ResponseViewModel responseInfo);

        /// <summary>
        /// Deletes Agency by identifier.
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="transaction">The transaction.</param>
        void DeleteAgencyById(long agencyId, out ResponseInformation transaction);
    }
}
