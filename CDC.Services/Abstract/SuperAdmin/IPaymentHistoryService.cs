﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.SuperAdmin
{
    public interface IPaymentHistoryService
    {
        /// <summary>
        /// Gets all Agency payment History List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        void GetAllAgencyPaymentHistory(SearchPaymentHistoryViewModel model, out ResponseViewModel responseInfo);

        
    }
}
