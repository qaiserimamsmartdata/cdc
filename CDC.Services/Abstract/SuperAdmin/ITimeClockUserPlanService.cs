﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDC.ViewModel.Common;
using CDC.Entities.TimeClockUsers;

namespace CDC.Services.Abstract.SuperAdmin
{
    public interface ITimeClockUserPlanService
    {
        /// <summary>
        /// Gets all TimeClockUserPlan List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        void GetAllTimeClockUserPlan(SearchTimeClockUserPlanViewModel model, out ResponseViewModel responseInfo);




        /// <summary>
        /// Deletes TimeClockUserPlan by identifier.
        /// </summary>
        /// <param name="timeclockuserplanId">The TimeClockUserPlan identifier.</param>
        /// <param name="transaction">The transaction.</param>
        void DeleteTimeClockUserById(long timeclockuserplanId, out ResponseInformation transaction);



        /// <summary>
        ///Add/Updates the  TimeClockUserPlan List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        int AddUpdateTimeClockUserPlan(TimeClockUsersViewModel addTimeClockUsersPlanViewModel);
        void getTimeClockPlan(long PlanId, out ResponseViewModel responseInfo);
    }
}
