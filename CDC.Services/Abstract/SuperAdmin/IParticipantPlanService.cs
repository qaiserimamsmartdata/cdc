﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDC.ViewModel.Common;
using CDC.Entities.Pricing;

namespace CDC.Services.Abstract.SuperAdmin
{
    public interface IParticipantPlanService
    {

        /// <summary>
        /// Gets all ParticipantPlan List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        void GetAllParticipantPlan(SearchParticipantPlanViewModel model, out ResponseViewModel responseInfo);




        /// <summary>
        /// Deletes ParticipantPlan by identifier.
        /// </summary>
        /// <param name="participantplanId">The ParticipantPlan identifier.</param>
        /// <param name="transaction">The transaction.</param>
        void DeleteParticipantPlanById(long participantplanId, out ResponseInformation transaction);



        /// <summary>
        ///Add/Updates the  ParticipantPlan List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        int AddUpdateParticipantPlan(PricingPlanViewModel addPricingPlanViewModel);

        void getPricingPlan(long PlanId, out ResponseViewModel responseInfo);
    }
}
