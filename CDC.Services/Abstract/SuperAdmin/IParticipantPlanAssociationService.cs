﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDC.ViewModel.Common;
using CDC.Entities.Pricing;

namespace CDC.Services.Abstract.SuperAdmin
{
    public interface IParticipantPlanAssociationService
    {

        void GetAllParticipantAssociationPlan(SearchParticipantPlanViewModel model, out ResponseViewModel responseInfo);
        void DeleteParticipantAssociationPlanById(long pricingplanId, out ResponseInformation transaction);
        int AddUpdateParticipantAssociationPlan(PricingPlanAssociationsViewModel addPricingPlanAssociationViewModel);

        void getPricingPlanAssociation(long pricingPlanId, out ResponseViewModel responseInfo);
    }
}
