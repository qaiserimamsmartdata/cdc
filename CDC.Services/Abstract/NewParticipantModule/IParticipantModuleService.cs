﻿using CDC.ViewModel.Common;
using CDC.ViewModel.NewParent;
using CDC.ViewModel.NewParticipant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.NewParticipantModule
{
   public interface IParticipantModuleService
    {
        ResponseViewModel RegisterParticipant(tblParticipantInfoViewModel model);
        ResponseViewModel ParticipantParentDetails(tblParticipantInfoViewModel model);
        ResponseViewModel DeleteParticipant(tblParticipantInfoViewModel model);
        ResponseViewModel getAssociationType(SearchFieldsViewModel associationTypeViewModel);
        ResponseViewModel getAssociationParticipant(SearchFieldsViewModel associationParticipantTypeViewModel);
        ResponseViewModel getAssociationParents(List<tblParentParticipantMappingViewModel> associationParentTypeViewModel);
        void GetAllParticipant(SearchParticipantViewModel model, out ResponseViewModel responseInfo);
        


    }
}
