﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Dashboard;
using CDC.ViewModel.Staff;

namespace CDC.Services.Abstract.Dashboard
{
    public interface ISuperAdminDashboardService
    {
        void GetAllCountsById(IDViewModel  Model,out ResponseViewModel responseInfo);
        
    }
}
