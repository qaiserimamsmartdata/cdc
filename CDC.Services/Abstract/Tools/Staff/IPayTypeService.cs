﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;

namespace CDC.Services.Abstract.Tools.Staff
{
    public interface IPayTypeService
    {
        void AddUpdatePayType(PayTypeViewModel addPayTypeViewModel, out ResponseInformation responseInfo);
        void GetPayType(out ResponseViewModel responseInfo);
        void DeletePayTypeById(long payTypeId, out ResponseInformation responseInfo);
    }
}
