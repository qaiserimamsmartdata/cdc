﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Staff
{
    public interface IStatusServices
    {
        int AddUpdateStatus(StatusViewModel addStatusViewModel);
        List<StatusViewModel> GetStatus();
        int DeleteStatusById(long StatusId);
    }
}
