﻿
﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;
using System;
using System.Collections.Generic;
namespace CDC.Services.Abstract.Tools.Staff
{
    public interface IDesignationService
    {
        
        int AddUpdateDesignation(PositionViewModel addDesignationViewModel);
        List<PositionViewModel> GetDesignation(SearchFieldsViewModel model);
        int DeleteDesignationById(PositionViewModel addDesignationViewModel);

        void GetAllDesignation(SearchPositionViewModel model, out ResponseViewModel responseInfo);


    }
}
