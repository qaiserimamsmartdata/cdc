﻿using CDC.ViewModel.Tools.Staff;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Staff
{
    public interface ILeaveTypeService
    {
        int AddUpdateLeaveType(LeaveTypeViewModel addLeaveTypeViewModel);
        List<LeaveTypeViewModel> GetLeaveType();
        int DeleteLeaveTypeById(long leaveTypeId);
    }
}
