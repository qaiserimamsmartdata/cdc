﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.ManageHoliday;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Tools.ManageHoliday
{
    public interface IHolidayService
    {
        void GetAllHoliday(SearchHolidayViewModel model, out ResponseViewModel responseInfo);
        void DeleteHolidayById(long id, out ResponseInformation transaction);
        int AddUpdateHoliday(HolidayViewModel holidayViewModel);
    }
}
