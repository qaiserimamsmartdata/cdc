﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.FoodManagementMaster;
using CDC.ViewModel.Tools.ManageHoliday;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Tools.FoodManagementMaster
{
    public interface IFoodManagementMasterService
    {
        void GetAllFoodManagementMaster(SearchFoodMasterViewModel model, out ResponseViewModel responseInfo);
        FoodManagementMasterViewModel GetFoodMasterById(long foodMasterId);
        void AddFoodMaster(FoodManagementMasterViewModel model, out ResponseInformation transaction);
        void UpdateFoodMaster(FoodManagementMasterViewModel model, out ResponseInformation responseInfo);
        void DeleteFoodMasterById(long foodMasterId, out ResponseInformation responseInfo);
    }
}
