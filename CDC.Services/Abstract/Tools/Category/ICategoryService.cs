﻿using System.Collections.Generic;
using CDC.ViewModel.Tools.Category;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.Tools.Category
{
    public interface ICategoryService
    {
        int AddCategory(CategoryViewModel addCategoryViewModel);
        IEnumerable<CategoryViewModel> GetCategory(SearchFieldsViewModel model);
        int UpdateCategory(CategoryViewModel model, long categoryId);
        int DeleteCategoryById(long categoryId);
        void SaveCategory();

        void GetAllCategory(SearchCategoryListViewModel model, out ResponseViewModel responseInfo);
    }
}