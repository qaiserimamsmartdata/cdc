﻿
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Attendance;
using CDC.ViewModel.Tools.Staff;
using System;
using System.Collections.Generic;
namespace CDC.Services.Abstract.Tools.Staff
{
    public interface IModeOfConvenienceService
    {
        
        int AddUpdateModeOfConvenience(ModeOfConvenienceViewModel model);
        List<ModeOfConvenienceViewModel> GetModeOfConvenience(SearchFieldsViewModel model);
        int DeleteModeOfConvenienceById(ModeOfConvenienceViewModel model);

        void GetAllModeOfConvenience(SearchModeOfConvenienceViewModel model, out ResponseViewModel responseInfo);


    }
}
