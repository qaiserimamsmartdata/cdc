﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Student
{
    public interface IDropReasonService
    {
        int AddUpdateDropReason(DropReasonViewModel addDropReasonViewModel);
        List<DropReasonViewModel> GetDropReason(SearchFieldsViewModel addDropReasonViewModel);
        int DeleteDropReasonById(DropReasonViewModel addDropReasonViewModel);
    }
}
