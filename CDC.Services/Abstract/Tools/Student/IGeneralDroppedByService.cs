﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Tools.Student
{
    public interface IGeneralDroppedByService
    {
        ResponseViewModel GetGeneralDroppedBy(long agencyid);
        ResponseViewModel AddUpdateGeneralDroppedBy(GeneralDroppedByViewModel model);
        ResponseViewModel DeleteGeneralDroppedBy(GeneralDroppedByViewModel model);
        void GetAll(SearchGeneralDroppedByViewModel model, out ResponseViewModel responseInfo);
    }

}
