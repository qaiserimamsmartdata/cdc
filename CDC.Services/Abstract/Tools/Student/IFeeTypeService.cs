﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Tools.Student
{
    public interface IFeeTypeService
    {
        ResponseViewModel GetFeeTypeById(long Id);

        ResponseViewModel GetAllFeeTypes(FeeTypeViewModel feetypeVM);

        ResponseViewModel AddUpdateFeeTypes(FeeTypeViewModel feetypeVM);

        void DeleteFeeTypes(long Id, out ResponseInformation responseInfo);
    }
}
