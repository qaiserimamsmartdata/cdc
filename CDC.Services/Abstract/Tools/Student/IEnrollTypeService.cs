﻿using CDC.ViewModel.Tools.Student;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Student
{
    public interface IEnrollTypeService
    {
        int AddUpdateEnrollType(EnrollTypeViewModel addEnrollTypeViewModel);
        List<EnrollTypeViewModel> GetEnrollType(EnrollTypeViewModel addEnrollTypeViewModel);
        int DeleteEnrollTypeById(EnrollTypeViewModel addEnrollTypeViewModel);
    }
}
