﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Enhancements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Tools.Enhancements
{
    public interface IEnhancementService
    {
        void GetAllEnhancement(SearchEnhancementViewModel model, out ResponseViewModel responseInfo);
        void DeleteEnhancementById(long id, out ResponseInformation transaction);
        int AddUpdateEnhancement(EnhancementViewModel enhancementViewModel);
    }
}
