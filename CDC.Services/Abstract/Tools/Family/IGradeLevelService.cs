﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Family
{
    public interface IGradeLevelService
    {
        int AddGradeLevel(GradeLevelViewModel addGradeLevelViewModel);
        IEnumerable<GradeLevelViewModel> GetGradeLevel();
        int UpdateGradeLevel(GradeLevelViewModel model, long gradeLevelId);
        int DeleteGradeLevelById(long gradeLevelId);
        void SaveGradeLevel();

        ResponseViewModel GetGradeLevelList(SearchFieldsViewModel gradeLevelViewModel);
        int AddUpdateGradeLevel(GradeLevelViewModel gradeLevelViewModel);
        ResponseViewModel DeleteGradeLevel(GradeLevelViewModel gradeLevelViewModel);
        void GetAllGradeLevel(SearchGradeLevelListViewModel model, out ResponseViewModel responseInfo);
    }
}
