﻿using CDC.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Tools.Family
{
    public interface ISecurityQuestionsService
    {
        ResponseViewModel GetSecurityQuestionsList(SearchFieldsViewModel questionTypeViewModel);
    }
}
