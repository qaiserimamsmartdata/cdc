﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Family
{
    public interface IInfoSourceService
    {
        int AddInfoSource(InfoSourceViewModel addInfoSourceViewModel);
        IEnumerable<InfoSourceViewModel> GetInfoSource();
        int UpdateInfoSource(InfoSourceViewModel model, long infoSourceId);
        int DeleteInfoSourceById(long infoSourceId);
        void SaveInfoSource();


        ResponseViewModel GetInfoSourceList(SearchFieldsViewModel infoSourceViewModel);
        int AddUpdateInfoSource(InfoSourceViewModel infoSourceViewModel);
        ResponseViewModel DeleteInfoSource(InfoSourceViewModel infoSourceViewModel);

        void GetAllInfoSource(SearchInfoSourceListViewModel model, out ResponseViewModel responseInfo);
    }
}
