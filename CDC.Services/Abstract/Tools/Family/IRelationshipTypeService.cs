﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;

namespace CDC.Services.Abstract.Tools.Family
{
    public interface IRelationshipTypeService
    {
        ResponseViewModel GetRelationshipTypeList(SearchFieldsViewModel relationshipTypeViewModel);
        int AddUpdateRelationshipType(RelationTypeViewModel relationshipTypeViewModel);
        ResponseViewModel DeleteRelationshipType(RelationTypeViewModel relationshipTypeViewModel);

        void GetAllRelationshipType(SearchRelationTypeListViewModel model, out ResponseViewModel responseInfo);
    }
}
