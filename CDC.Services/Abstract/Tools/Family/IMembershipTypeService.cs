﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;

namespace CDC.Services.Abstract.Tools.Family
{
    public interface IMembershipTypeService
    {
        ResponseViewModel GetMembershipTypeList(SearchFieldsViewModel membershipTypeViewModel);
        int AddUpdateMembershipType(MembershipTypeViewModel membershipTypeViewModel);
        ResponseViewModel DeleteMembershipType(MembershipTypeViewModel membershipTypeViewModel);

        void GetAllMembershipType(SearchMembershipTypeListViewModel model, out ResponseViewModel responseInfo);
    }
}
