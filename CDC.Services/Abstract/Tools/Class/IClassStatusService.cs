﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Class;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Class
{
    public interface IClassStatusService
    {
        int AddUpdateClassStatus(ClassStatusViewModel addClassStatusViewModel);
        List<ClassStatusViewModel> GetClassStatus(SearchFieldsViewModel addClassStatusViewModel);
        int DeleteClassStatusById(ClassStatusViewModel addClassStatusViewModel);

        void GetAllClassStatus(SearchClassStatusListViewModel model, out ResponseViewModel responseInfo);
    }
}
