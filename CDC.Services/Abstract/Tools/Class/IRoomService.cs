﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Class;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Class
{
    public interface IRoomService
    {
        int AddUpdateRoom(RoomViewModel addRoomViewModel);
        List<RoomViewModel> GetRoom(RoomViewModel addRoomViewModel);
        int DeleteRoomById(RoomViewModel addRoomViewModel);
        void GetAllRoom(SearchRoomListViewModel model, out ResponseViewModel responseInfo);
    }
}
