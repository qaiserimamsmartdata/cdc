﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Class;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Tools.Class
{
    public interface ISessionService
    {
        int AddUpdateSession(SessionViewModel addSessionViewModel);
        List<SessionViewModel> GetSession(SearchFieldsViewModel addSessionViewModel);
        int DeleteSessionById(SessionViewModel addSessionViewModel);

        void GetAllSession(SearchSessionListViewModel model, out ResponseViewModel responseInfo);
    }
}
