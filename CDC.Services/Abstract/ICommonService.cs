﻿using System.Collections.Generic;
using CDC.ViewModel;
using System;
using CDC.ViewModel.Tools.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using CDC.Entities.Pricing;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CDC.Entities.TimeClockUsers;
using CDC.Entities.Family;

namespace CDC.Services.Abstract
{
    /// <summary>
    ///     The Common Service
    /// </summary>
    public interface ICommonService
    {
        /// <summary>
        ///     Gets the country list.
        /// </summary>
        /// <returns></returns>
        List<CountryViewModel> GetCountryList();

        /// <summary>
        ///     Gets the state by country identifier.
        /// </summary>
        /// <param name="countryId">The country identifier.</param>
        /// <returns></returns>
        List<StateViewModel> GetStateByCountryId(long countryId);

        /// <summary>
        ///     Gets the city by country identifier.
        /// </summary>
        /// <param name="stateId">The state identifier.</param>
        /// <returns></returns>
        List<CityViewModel> GetCityByCountryId(long stateId);

        /// <summary>
        ///     Determines whether [is exist class name] [the specified name].
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>
        ///     <c>true</c> if [is exist class name] [the specified name]; otherwise, <c>false</c>.
        /// </returns>
        bool IsExistClassName(string name,long classId, long agencyId);

        bool IsExistStaffUsername(string name,long agencyId,long staffId);
        bool IsExistMobileNumber(string name, long agencyId);
        bool IsExistParentUsername(string name);

        bool IsExistTitle(string name,long agencyId);

        bool IsExistSecurityCode(string name);

        bool IsExistEmailId(string emailId);

     
        /// <summary>
        /// Calculate Age from DateofBirth.
        /// </summary>
        /// <param name="dob">Date of Birth.</param>
        /// <returns>Age.</returns>
        string CalculateAge(DateTime dob);

        bool FileSize(long file);
        /// <summary>
        /// Gets the designations.
        /// </summary>
        /// <returns></returns>
        
        List<PositionViewModel> GetPositions(long agencyId);

        ResponseViewModel GetClass_Room_Lesson(SearchFieldsViewModel SearchVM);
        /// <summary>
        /// Gets the staff schedule.
        /// </summary>
        /// <returns></returns>
        ResponseViewModel GetStaffSchedule();

        ResponseViewModel GetClass_Room_LessonByAgency(long agencyId = 0);

        void SendEmail(string mailTo, string emailSubject, string emailMessage);
        
        
        bool InsertDefaultValue(long agencyId,long locationId);
        ResponseViewModel GetFeesTypes(FeeTypeViewModel vmModel);
        ResponseViewModel GetStaffList(IDViewModel model);
        ResponseViewModel GetPricingPlans(PricingPlanViewModel vmModel);
        ResponseViewModel GetTimeClockUsersPricingPlans(TimeClockUsersViewModel vmModel);
        #region TimeSheet
        DateTime? ConvertToUTC(DateTime value, string timeZone = null);

        DateTime? ConvertFromUTC(DateTime value, string timeZone = null);
        TimeSpan? ConvertTimeFromUTC(DateTime value,TimeSpan ts, string timeZone = null);

        ReadOnlyCollection<TimeZoneInfo> GetTimezones();
        #endregion
        ResponseViewModel GetLocations(long agencyId);
        ResponseViewModel GetSMSCarrier();
        ResponseViewModel GetAllEnrollList(SearchStudentScheduleViewModel model);
        ResponseViewModel GetPricingPlanCount(SearchFieldsViewModel model);
        ResponseViewModel GetAllParentList(IDViewModel model);
        ResponseViewModel CheckDeleteClass(IDViewModel model);
        ResponseViewModel CheckParticipantDeleteClass(SearchStudentScheduleViewModel model);


        ResponseViewModel GetAllParticipants(IDViewModel model);
        ResponseViewModel GetAllOtherParticipants(IDViewModel model);
        ResponseViewModel GetAllStaff(IDViewModel model);
        ResponseViewModel GetParentInformation(IDViewModel vmModel);
        ResponseViewModel GetDailyStatusCategory(IDViewModel vmModel);
        ResponseViewModel GetPositionNameById(IDViewModel model);
        ResponseViewModel GetParticipantInformation(IDViewModel model);
        ResponseViewModel GetClassCombo(SearchFieldsViewModel SearchVM);
        List<ParentInfo> GetStudentPrimaryParentDetail(int? CLassId, int? agencyId);

        ResponseViewModel GetMealType(SearchFieldsViewModel obj);
        ResponseViewModel GetFoodMaster(SearchFieldsViewModel obj);
        ResponseViewModel GetFoodItems(long AgencyId, long MealTypeId);
        ResponseViewModel GetMealItemMaster(SearchFieldsViewModel obj);
        ResponseViewModel getLeaveStatusData(SearchFieldsViewModel obj);
        ResponseViewModel GetMealServeSize(SearchFieldsViewModel obj);
        ResponseViewModel GetMealServeQuantity(SearchFieldsViewModel obj);
        ResponseViewModel GetMealServeTypes(SearchFieldsViewModel obj);
        ResponseViewModel CheckParticipantAttendance(SearchFieldsViewModelForParticipants obj);
        ResponseViewModel GetModeOfConvinience(SearchFieldsViewModel obj);
        ResponseViewModel GetParentList(IDViewModel vmModel);
        string RandomString(int length);
        string GetMd5Hash(string input);
    }
}