﻿using System.Collections.Generic;
using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;

namespace CDC.Services.Abstract.Skills
{
    /// <summary>
    ///     The Skill Service
    /// </summary>
    public interface ISkillService
    {
        /// <summary>
        ///     Adds the skill.
        /// </summary>
        /// <param name="addSkillViewModel">The add skill view model.</param>
        /// <returns></returns>
        int AddSkill(SkillViewModel addSkillViewModel);

        /// <summary>
        ///     Gets the skills.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        List<SkillViewModel> GetSkills(SearchSkillViewModel model, out int totalCount);

        /// <summary>
        ///     Updates the skill.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="skillId">The skill identifier.</param>
        /// <returns></returns>
        int UpdateSkill(SkillViewModel model, long skillId);

        /// <summary>
        ///     Deletes the skill by identifier.
        /// </summary>
        /// <param name="skillId">The skill identifier.</param>
        /// <returns></returns>
        int DeleteSkillById(long skillId);

        /// <summary>
        ///     Gets the skill by identifier.
        /// </summary>
        /// <param name="skillId">The skill identifier.</param>
        /// <returns></returns>
        SkillViewModel GetSkillById(long skillId);
    }
}