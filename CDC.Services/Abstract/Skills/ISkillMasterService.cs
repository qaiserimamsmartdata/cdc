﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Skills
{
    public interface ISkillMasterService
    {
        int AddUpdateSkillMaster(SkillMasterViewModel skillmasterViewModel);
        int DeleteSkillMaster(SkillMasterViewModel skillmasterViewModel);
        void GetAllSkillMaster(SearchSkillMasterViewModel searchModel, out ResponseViewModel responseInfo);
    }
}
