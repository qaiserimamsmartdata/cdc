﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Skills
{
    public interface IStaffSkillService
    {
        void GetAllStaffSkill(SearchStaffSkillViewModel model, out ResponseViewModel responseInfo);
        void DeleteStaffSkillById(long staffskillId, out ResponseInformation transaction);
        int AddUpdateStaffSkill(StaffSkillViewModel staffskillViewModel);
    }
}
