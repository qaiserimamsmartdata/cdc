﻿using CDC.ViewModel.Common;
using CDC.ViewModel.User;

namespace CDC.Services.Abstract.User
{
    public interface IUserService
    {
        ResponseViewModel FindUser(string userName, string password);
        ResponseViewModel GetUserRoleId(string roleName);
        ResponseViewModel GetUserRoleName(long roleId);
        ResponseViewModel ForgotPassword(UserViewModel model);
        ResponseViewModel ResetPassword(UserViewModel userViewModel);
        ResponseViewModel ResetParentPassword(UserViewModel userViewModel);
        void AddForgotPasswordLog(ForgotPasswordLogViewModel model, out ResponseInformation transaction);
        bool IsExistUserName(string name);
       bool EnrolledCapacity(IDViewModel model);

    }
}
