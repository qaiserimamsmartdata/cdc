using CDC.ViewModel.Common;
namespace CDC.Services.Abstract.User
{
    /// <summary>
    /// Interface IMembershipService
    /// </summary>
    public interface IMembershipService
    {
        ResponseViewModel ValidateToken(string Token);
    }
}