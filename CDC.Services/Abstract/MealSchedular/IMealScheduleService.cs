﻿using CDC.ViewModel.Common;
using CDC.ViewModel.MealSchedular;
using CDC.ViewModel.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.MealSchedular
{
    public interface IMealScheduleService
    {
        //void GetAllStaffs(SearchTasksViewModel model, out ResponseViewModel responseInfo);
        //void GetAllClass(SearchTasksViewModel model, out ResponseViewModel responseInfo);

        void GetAllTasks(SearchTasksViewModel model, out ResponseViewModel responseInfo);
        void AddTasks(MealSchedularViewModel model, out ResponseInformation transaction);
        void UpdateTasks(MealSchedularViewModel model, out ResponseInformation transaction);
        //void UpdateTasks(TasksViewModel model, out ResponseInformation transaction);
        void DeleteMealScheduleById(MealSchedularViewModel model, out ResponseInformation transaction);
        //  void GetAll(MealSchedularViewModel model, out ResponseViewModel transaction);
    }
}
