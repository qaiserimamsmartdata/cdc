﻿using CDC.ViewModel.Common;
using CDC.ViewModel.messages;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Abstract.Email
{
    public interface IEmailService
    {

        void GetAgencyByFamilyId(out ResponseViewModel responseInfo, SearchUserViewModel model);
        void GetAllMail(out ResponseViewModel responseInfo, SearchMessageViewModel model);
        void GetAllSentMail(out ResponseViewModel responseInfo, SearchMessageViewModel model);
        void GetAllContacts(out ResponseViewModel responseInfo, SearchMessageViewModel model);
        void ComposeMessage(messageViewModel model, out ResponseViewModel transaction);
        void DeleteInboxMessage(messageViewModel model, out ResponseInformation transaction);
        void DeleteSentMessage(messageViewModel model, out ResponseInformation transaction);

        void updateUnreadCount(messageViewModel model, out ResponseInformation transaction);
        void GetUnreadCount(out ResponseViewModel responseInfo, SearchMessageViewModel model);
      
    }
}
