﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDC.ViewModel.Common;
using CDC.Entities.ToDoTask;
using CDC.ViewModel.ToDoTask;

namespace CDC.Services.Abstract.ToDoTask
{
    public interface IToDoService
    {
        /// <summary>
        /// Gets all ToDo List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        void GetAllToDo(SearchToDoViewModel model, out ResponseViewModel responseInfo);




        /// <summary>
        /// Deletes ToDo by identifier.
        /// </summary>
        /// <param name="toDoId">The toDoId identifier.</param>
        /// <param name="transaction">The transaction.</param>
        void DeleteToDoById(long toDoId, out ResponseInformation transaction);



        /// <summary>
        ///Add/Updates the  toDoId List.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        int AddUpdateToDo(ToDoViewModel toDoViewModel);
    }
}
