﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDC.ViewModel.Common;
using CDC.ViewModel.ToDoTask;

namespace CDC.Services.Abstract.ToDoTask
{
    public interface IPriorityService
    {
        int AddUpdatePriority(PriorityViewModel priorityViewModel);
        int DeletePriority(PriorityViewModel priorityViewModel);
        void GetAllPriority(SearchPriorityViewModel searchModel,out ResponseViewModel responseInfo);

    }
}
