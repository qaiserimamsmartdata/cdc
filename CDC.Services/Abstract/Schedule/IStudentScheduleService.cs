﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Schedule;
using System.Collections.Generic;

namespace CDC.Services.Abstract.Schedule
{
    /// <summary>
    ///     The Schedule Service Interface
    /// </summary>
    public interface IStudentScheduleService
    {
        //List<StudentScheduleViewModel> GetAllStudentSchedule(SearchStudentScheduleViewModel model, out ResponseInformation transaction);
        List<StudentScheduleViewModel> GetAllStudentSchedule(StudentScheduleViewModelNew model, out ResponseInformation transaction);
        void AddStudentSchedule(StudentScheduleViewModel model, out ResponseInformation transaction);
        void UpdateStudentSchedule(StudentScheduleViewModel profile, out ResponseInformation transaction);
        void DeleteStudentScheduleById(long scheduleId, out ResponseInformation transaction);
        List<StudentScheduleViewModel> GetUnPaidHistory(SearchStudentScheduleViewModel model, out ResponseInformation transaction);
    }
}