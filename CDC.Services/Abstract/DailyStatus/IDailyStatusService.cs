﻿using CDC.ViewModel.Common;
using CDC.ViewModel.DailyStatus;
using System;

namespace CDC.Services.Abstract.DailyStatus
{
    public interface IDailyStatusService
    {

        DailyStatusViewModel GetDailyStatusById(long DailyStatusId);
        void GetAllDailyStatusParticipants(SearchDailyStatusViewModel model, out ResponseViewModel responseInfo);
        void AddDailyStatus(DailyStatusViewModel model, out ResponseInformation transaction);
        DailyStatusViewModel DailyStatusEmail(long StudentId , DateTime Date, string TimeZone );
        void UpdateDailyStatus(DailyStatusViewModel model, out ResponseInformation transaction);

        void DeleteDailyStatusById(long agencyId, out ResponseInformation transaction);
        void GetAllDailyStatus(SearchDailyStatusViewModel model, out ResponseViewModel responseInfo);
    }
}
