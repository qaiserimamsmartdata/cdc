﻿using CDC.ViewModel.Common;
using CDC.ViewModel.Location;
using CDC.ViewModel.PaymentHistory;
using CDC.ViewModel.PaymentHistoryForPlans;

namespace CDC.Services.Abstract.PaymentHistoryForPlans
{
    public interface IPaymentHistoryInfoForPlanService
    {
        void AddPaymentHistoryInfoForPlans(PaymentHistoryInfoForPlansViewModel model, out ResponseInformation transaction);
     
    }
}
