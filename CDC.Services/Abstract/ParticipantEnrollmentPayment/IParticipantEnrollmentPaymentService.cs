﻿using CDC.Entities.ParticipantEnrollmentPayment;
using CDC.ViewModel.Common;
using CDC.ViewModel.ParticipantEnrollmentPayment;
using CDC.ViewModel.Staff;

namespace CDC.Services.Abstract.ParticipantEnrollmentPayment
{
    public interface IParticipantEnrollmentPaymentService
    {
        void AddPayInfo(ParticipantEnrollPaymentViewModel model, out ResponseInformation transaction);
        void UpdatePayInfo(ParticipantEnrollPaymentViewModel model, out ResponseInformation transaction);

    }
}
