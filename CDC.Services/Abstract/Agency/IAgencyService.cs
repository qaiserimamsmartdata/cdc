using CDC.ViewModel;
using CDC.ViewModel.Agency;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;

namespace CDC.Services.Abstract.Agency
{
    /// <summary>
    ///     The Agency Interface
    /// </summary>
    public interface IAgencyService
    {
        /// <summary>
        /// Gets the agency by identifier.
        /// </summary>
        /// <param name="AgencyId">The agency identifier.</param>
        /// <returns></returns>
        AgencyViewModel GetAgencyById(long AgencyId);
        /// <summary>
        ///     Adds the agency.
        /// </summary>
        /// <param name="addAgencyViewModel">The add agency view model.</param>
        /// <returns></returns>
        int AddAgency(AgencyViewModel addAgencyViewModel);

        /// <summary>
        ///     Updates the agency detail.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <returns></returns>
        int UpdateAgencyDetail(AgencyViewModel model, long agencyId);
        /// <summary>
        /// Deletes the agency by identifier.
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <returns></returns>
        int DeleteAgencyById(long agencyId);
        /// <summary>
        /// Finds the asynchronous.
        /// </summary>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Password">The password.</param>
        /// <returns></returns>
        ResponseViewModel FindAsync(string UserName, string Password);

        ResponseViewModel SetIsLoggedFirstTime(long agencyId);

    }
}