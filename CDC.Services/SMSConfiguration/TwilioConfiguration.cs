﻿namespace CDC.Services.SMSConfiguration
{
 public  class TwilioConfiguration
    {
        /// <summary>
        /// The twilio sid
        /// </summary>
        public const string TwilioSid = "AC0683117bab1f8fa9ea68081fe8116e51";
        /// <summary>
        /// The twilio authentication token
        /// </summary>
        public const string TwilioAuthToken = "87eff52e7ed304419a86303591225ae7";
        /// <summary>
        /// The twilio phone number
        /// </summary>
        public const string TwilioPhoneNumber = "+13036257566";


        /// <summary>
        /// The SMS booking confirmed format
        /// </summary>
        public const string SMSBookingConfirmedFormat =
            "Hello ##username##, Thank You for using CDC agency. Your Booking ID is ##bookingid##.";
    }
}
