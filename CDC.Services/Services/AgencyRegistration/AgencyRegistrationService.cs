﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Tools.Staff;
using CDC.Entities.User;
using CDC.Services.Abstract;
using CDC.Services.Abstract.AgencyRegistration;
using CDC.ViewModel;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Common;
using CDC.ViewModel.SuperAdmin;

namespace CDC.Services.Services.AgencyRegistration
{
    public class AgencyRegistrationService : IAgencyRegistrationService
    {
        private readonly IEntityBaseRepository<AgencyBillingInfo> _agencyBillingInfoRepository;
        private readonly IEntityBaseRepository<AgencyContactInfo> _agencyContactInfoRepository;
        private readonly IEntityBaseRepository<AgencyPaymentInfo> _agencyPaymentInfoRepository;
        private readonly IEntityBaseRepository<AgencyRegistrationInfo> _agencyRegistrationInfoRepository;
        private readonly IEntityBaseRepository<AgencyLocationInfo> _agencyLocationInfoRepository;
        private readonly IEntityBaseRepository<AgencyTimeZone> _agencyTimeZoneInfoRepository;
        private readonly ICommonService _commonServices;
        private readonly IEntityBaseRepository<Position> _designationRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEntityBaseRepository<Users> _userRepository;

        public AgencyRegistrationService(IEntityBaseRepository<AgencyRegistrationInfo> agencyRegistrationInfoRepository,
            IEntityBaseRepository<AgencyContactInfo> agencyContactInfoRepository,
            IEntityBaseRepository<AgencyBillingInfo> agencyBillingInfoRepository,
            IEntityBaseRepository<AgencyPaymentInfo> agencyPaymentInfoRepository,
            IEntityBaseRepository<Users> userRepository, IUnitOfWork unitOfWork,
            IEntityBaseRepository<AgencyLocationInfo> agencyLocationInfoRepository,
            IEntityBaseRepository<AgencyTimeZone> agencyTimeZoneInfoRepository, 
            IEntityBaseRepository<Position> designationRepository, ICommonService commonServices)
        {
            _agencyRegistrationInfoRepository = agencyRegistrationInfoRepository;
            _agencyContactInfoRepository = agencyContactInfoRepository;
            _agencyBillingInfoRepository = agencyBillingInfoRepository;
            _agencyPaymentInfoRepository = agencyPaymentInfoRepository;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _designationRepository = designationRepository;
            _commonServices = commonServices;
            _agencyLocationInfoRepository = agencyLocationInfoRepository;
            _agencyTimeZoneInfoRepository = agencyTimeZoneInfoRepository;
        }

        public AgencyRegistrationViewModel GetAgencyById(long agencyId)
        {
            var resultAgencyRegisterInfo = _agencyRegistrationInfoRepository.GetSingle(agencyId);
            var returnAgencyViewModelRegisterInfo =
                Mapper.Map<AgencyRegistrationInfo, AgencyRegistrationViewModel>(resultAgencyRegisterInfo);
            if (returnAgencyViewModelRegisterInfo.ID > 0)
            {
                returnAgencyViewModelRegisterInfo.CountryId =
                    resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.CountryId;
                returnAgencyViewModelRegisterInfo.StateId =
                    resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.StateId;
                //returnAgencyViewModelRegisterInfo.CityId = resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault().CityId;
                returnAgencyViewModelRegisterInfo.PostalCode =
                    resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.PostalCode;
                returnAgencyViewModelRegisterInfo.CityName =
                    resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.CityName;
                returnAgencyViewModelRegisterInfo.PhoneNumber =
                    resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.PhoneNumber;
                returnAgencyViewModelRegisterInfo.Address =
                    resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.Address;
                returnAgencyViewModelRegisterInfo.TimeZone =
                    resultAgencyRegisterInfo.AgencyTimeZones.FirstOrDefault()?.TimeZone;
            }
            return returnAgencyViewModelRegisterInfo;
        }

        public AgencyPaymentViewModel GetPaymentById(long ID)
        {
            var resultAgencyPaymentInfo = _agencyPaymentInfoRepository.GetAll().FirstOrDefault(x => x.AgencyRegistrationId == ID);
            var returnAgencyViewModelPayementInfo =
                Mapper.Map<AgencyPaymentInfo, AgencyPaymentViewModel>(resultAgencyPaymentInfo);
            if (ID > 0)
            {
                if (resultAgencyPaymentInfo != null)
                {
                    returnAgencyViewModelPayementInfo.CardName =
                        resultAgencyPaymentInfo.CardName;
                    returnAgencyViewModelPayementInfo.ID =
                        resultAgencyPaymentInfo.ID;
                    returnAgencyViewModelPayementInfo.CardType = resultAgencyPaymentInfo.CardType;
                    returnAgencyViewModelPayementInfo.CardNumber = resultAgencyPaymentInfo.CardNumber;
                    returnAgencyViewModelPayementInfo.CardExpiryMonth = resultAgencyPaymentInfo.CardExpiryMonth;
                    returnAgencyViewModelPayementInfo.CardExpiryYear = resultAgencyPaymentInfo.CardExpiryYear;
                    returnAgencyViewModelPayementInfo.CVV = resultAgencyPaymentInfo.CVV;
                }
            }
            return returnAgencyViewModelPayementInfo;
        }

        public ResponseViewModel SetIsLoggedFirstTime(long agencyId)
        {
            var responseViewModelInfo = new ResponseViewModel();
            try
            {
                var agencyDetailOld =
                    _agencyRegistrationInfoRepository.FindBy(m => m.ID == agencyId && m.IsDeleted == false)
                        .FirstOrDefault();
                if (agencyDetailOld != null)
                {
                    agencyDetailOld.IsLoggedFirstTime = true;
                    _agencyRegistrationInfoRepository.Edit(agencyDetailOld, agencyDetailOld);
                    _unitOfWork.Commit();
                    responseViewModelInfo.IsSuccess = true;
                }
                else
                    responseViewModelInfo.IsSuccess = false;
            }
            catch (Exception)
            {
                responseViewModelInfo.IsSuccess = false;
            }
            return responseViewModelInfo;
        }

        /// <summary>
        ///     Adds the agency
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddAgency(AgencyRegistrationViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                //User Login information
                var userLoginInfo = new Users

                {
                    RoleId = 1,
                    EmailId = model.Email,
                    Password = model.Password,
                    IsDeleted = false
                };
                _userRepository.Add(userLoginInfo);
                _unitOfWork.Commit();
                model.UserId = userLoginInfo.ID;
                
                var agencyLocationInfo = AddAgencyInfo(model);
                //Dropdown default value
               // _commonServices.InsertDefaultValue(model.ID, agencyLocationInfo.ID);

                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add(AppConstants.OrganisationAddedSuccesfully);
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        private AgencyLocationInfo AddAgencyInfo(AgencyRegistrationViewModel model)
        {
            var agencyRegistrationInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyRegistrationInfo>(model);
            _agencyRegistrationInfoRepository.Add(agencyRegistrationInfo);
            _unitOfWork.Commit();

            model.ID = agencyRegistrationInfo.ID;
            var agencyContactInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyContactInfo>(model);
            agencyContactInfo.AgencyRegistrationId = model.ID;
            _agencyContactInfoRepository.Add(agencyContactInfo);


            var agencyBillingInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyBillingInfo>(model);
            agencyBillingInfo.AgencyRegistrationId = model.ID;
            _agencyBillingInfoRepository.Add(agencyBillingInfo);


            var agencyPaymentInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyPaymentInfo>(model);
            agencyPaymentInfo.AgencyRegistrationId = model.ID;
            _agencyPaymentInfoRepository.Add(agencyPaymentInfo);
            _unitOfWork.Commit();

            var agencyLocationInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyLocationInfo>(model);
            agencyLocationInfo.AgencyRegistrationId = model.ID;
            agencyLocationInfo.ContactFirstName = model.ContactPersonFirstName;
            agencyLocationInfo.ContactLastName = model.ContactPersonLastName;
            agencyLocationInfo.LocationPhoneNumber = model.PhoneNumber;
            _agencyLocationInfoRepository.Add(agencyLocationInfo);
            _unitOfWork.Commit();

            //var agencyTimeZoneInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyTimeZone>(model);
            //agencyTimeZoneInfo.TimeZone = model.TimeZone;
            //agencyTimeZoneInfo.AgencyRegistrationId = model.ID;
            //_agencyTimeZoneInfoRepository.Add(agencyTimeZoneInfo);
            //_unitOfWork.Commit();
            return agencyLocationInfo;
        }

        /// <summary>
        ///     Updates the agency.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void UpdateAgency(AgencyRegistrationViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var agencyRegistrationDetail =
                    _agencyRegistrationInfoRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (agencyRegistrationDetail != null)
                {
                    model.StripeUserId = agencyRegistrationDetail.StripeUserId;
                    model.StripeSubscriptionId = agencyRegistrationDetail.StripeSubscriptionId;
                    model.StripeTimeClockSubscriptionId = agencyRegistrationDetail.StripeTimeClockSubscriptionId;
                    model.IsTrial = agencyRegistrationDetail.IsTrial;
                    model.TrialStart = agencyRegistrationDetail.TrialStart;
                    model.TrialEnd = agencyRegistrationDetail.TrialEnd;
                    model.TotalTimeClockUsers = agencyRegistrationDetail.TotalTimeClockUsers;

                    var mapAgencyRegistrationInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyRegistrationInfo>(model);
                    _agencyRegistrationInfoRepository.Edit(agencyRegistrationDetail, mapAgencyRegistrationInfo);

                    var agencyContactDetail =
                        _agencyContactInfoRepository.FindBy(m => m.AgencyRegistrationId == model.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    var mapAgencyContactInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyContactInfo>(model);
                
                    if (agencyContactDetail != null)
                    {
                        mapAgencyContactInfo.ID = agencyContactDetail.ID;
                        mapAgencyContactInfo.AgencyRegistrationId = agencyRegistrationDetail.ID;
                        _agencyContactInfoRepository.Edit(agencyContactDetail, mapAgencyContactInfo);
                    }
                }
                var agencyTimeZoneDetail =
                   _agencyTimeZoneInfoRepository.FindBy(m => m.AgencyRegistrationId == model.ID && m.IsDeleted == false)
                       .FirstOrDefault();
                if (agencyTimeZoneDetail != null)
                {
                    agencyTimeZoneDetail.AgencyRegistrationId = model.ID;
                    agencyTimeZoneDetail.TimeZone = model.TimeZone;
                    _agencyTimeZoneInfoRepository.Edit(agencyTimeZoneDetail, agencyTimeZoneDetail);
                }
                else
                {
                    var agencyTimeZoneInfo = Mapper.Map<AgencyRegistrationViewModel, AgencyTimeZone>(model);
                    agencyTimeZoneInfo.AgencyRegistrationId = model.ID;
                    _agencyTimeZoneInfoRepository.Add(agencyTimeZoneInfo);
                }
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Organisation successfully updated at " + DateTime.UtcNow);
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
        public void UpdatePayment(AgencyPaymentViewModel model, out ResponseInformation transaction)
        {
            //model.PricingPlanId = 1;
            transaction = new ResponseInformation();
            try
            {
                var paymentDetail =
                    _agencyPaymentInfoRepository.FindBy(m => m.AgencyRegistrationId == model.AgencyRegistrationId && m.IsDeleted == false).FirstOrDefault();
                var mappaymentViewModelInfo = Mapper.Map<AgencyPaymentViewModel, AgencyPaymentInfo>(model);
                _agencyPaymentInfoRepository.Edit(paymentDetail, mappaymentViewModelInfo);
                
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
        public void GetDesignation(long agencyId)
        {
            var designData = new Position
            {
                Name = "Teachers",
                IsDeleted = false,
                AgencyId = agencyId
            };
            _designationRepository.Add(designData);
            _unitOfWork.Commit();
        }

        public ResponseViewModel FindAsync(string userName, string password)
        {
            var responseViewModelInfo = new ResponseViewModel();
            try
            {
                var agencyinfo =
                    _agencyRegistrationInfoRepository.FindBy(m => m.Email == userName && m.Password == password)
                        .FirstOrDefault();
                var agencyViewModelinfo = Mapper.Map<AgencyRegistrationInfo, AgencyRegistrationViewModel>(agencyinfo);
                if (agencyViewModelinfo != null)
                {
                    responseViewModelInfo.Content = agencyViewModelinfo;
                    responseViewModelInfo.IsSuccess = true;
                }
                else
                {
                    responseViewModelInfo.Content = null;
                    responseViewModelInfo.IsSuccess = false;
                }
            }
            catch (Exception)
            {
                responseViewModelInfo.IsSuccess = false;
            }
            return responseViewModelInfo;
        }

        /// <summary>
        ///     Deletes the agency by identifier.
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="transaction">The transaction.</param>
        public void DeleteAgencyById(long agencyId, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var studentSheduleDetails =
                    _agencyRegistrationInfoRepository.FindBy(m => m.ID == agencyId && m.IsDeleted == false)
                        .FirstOrDefault();
                if (studentSheduleDetails != null && studentSheduleDetails.ID > 0)
                {
                    studentSheduleDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("Organisation updated successfully");
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
        public void UpdateAgencyByTimeClockUsers(AgencyRegistrationViewModel model, out ResponseInformation transaction)
        {
            //model.PricingPlanId = 1;
            transaction = new ResponseInformation();
            try
            {
                var agencyRegistrationDetail =
                    _agencyRegistrationInfoRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (agencyRegistrationDetail != null)
                {
                    agencyRegistrationDetail.TimeClockUsersPlanId = model.TimeClockUsersPlanId;

                    #region for timeclock users count

                    if (model.TimeClockStaffCount != null)
                        if (model.AgencyTimeClockUsers != null)
                            if (model.TimeClockPlanUsers != null)
                                agencyRegistrationDetail.TotalTimeClockUsers = model.TimeClockStaffCount.Value + model.AgencyTimeClockUsers.Value + model.TimeClockPlanUsers.Value;

                    #endregion
                    ////////////////////////
                    //////Subscription id for timeclock for same userid
                    agencyRegistrationDetail.StripeTimeClockSubscriptionId = model.StripeTimeClockSubscriptionId;
                    /////////////////////////////////////////////////////
                    _agencyRegistrationInfoRepository.Edit(agencyRegistrationDetail, agencyRegistrationDetail);
                    _unitOfWork.Commit();
                    transaction.TotalTimeClockUsers = agencyRegistrationDetail.TotalTimeClockUsers;
                }
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("TimeClockUsers updated successfully");
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
        public void UpdateAgencyPricingPlan(AgencyRegistrationViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var agencyRegistrationDetail =
                    _agencyRegistrationInfoRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (agencyRegistrationDetail != null)
                {
                    agencyRegistrationDetail.PricingPlanId = model.PricingPlanId;
                    agencyRegistrationDetail.StripeSubscriptionId = model.StripeSubscriptionId;
                    agencyRegistrationDetail.IsTrial = false;
                    /////for totaltimeclockuser count including added staff count with pricing timeclock count   

                    if (model.TimeClockStaffCount != null)
                        if (model.AgencyTimeClockUsers != null)
                            if (model.TimeClockPlanUsers != null)
                                agencyRegistrationDetail.TotalTimeClockUsers = model.TimeClockStaffCount.Value + model.AgencyTimeClockUsers.Value + model.TimeClockPlanUsers.Value;
                    ////////////////////////////////////////////////
                    _agencyRegistrationInfoRepository.Edit(agencyRegistrationDetail, agencyRegistrationDetail);
                    _unitOfWork.Commit();
                    var mappedData = Mapper.Map<AgencyRegistrationInfo, AgencyRegistrationViewModel>(agencyRegistrationDetail);
                    model.PricingPlan = mappedData.PricingPlan;
                    transaction.TotalTimeClockUsers = agencyRegistrationDetail.TotalTimeClockUsers;
                }
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Pricing plan updated successfully");
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        public void getSubscriptionPlan(long AgencyId, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var stripeSubscriptionId =
                    _agencyRegistrationInfoRepository.FindBy(m => m.ID == AgencyId && m.IsDeleted == false).Select(x =>new { x.StripeSubscriptionId,x.StripeUserId}).FirstOrDefault();
                if (stripeSubscriptionId != null)
                {
                    responseInfo.ReturnStatus = true;
                    responseInfo.Content = stripeSubscriptionId;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}