﻿using System;
using System.Collections.Generic;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.ViewModel.Common;
using CDC.Services.Abstract.PaymentHistoryForPlans;
using CDC.Entities.PaymentHistoryForPlans;
using CDC.ViewModel.PaymentHistoryForPlans;

namespace CDC.Services.Services.PaymentHistoryForPlans
{
    public class PaymentHistoryForPlansService : IPaymentHistoryInfoForPlanService
    {
        private readonly IEntityBaseRepository<PaymentHistoryInfoForPlans> _paymentHistoryInfoForPlansRepository;
        private readonly IUnitOfWork _unitOfWork;        

        public PaymentHistoryForPlansService(
            IEntityBaseRepository<PaymentHistoryInfoForPlans> paymentHistoryInfoForPlansRepository,
            IUnitOfWork unitOfWork)
        {
            _paymentHistoryInfoForPlansRepository = paymentHistoryInfoForPlansRepository;
            _unitOfWork = unitOfWork;
        }
        public void AddPaymentHistoryInfoForPlans(PaymentHistoryInfoForPlansViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                
                var locationInfo = Mapper.Map<PaymentHistoryInfoForPlansViewModel, PaymentHistoryInfoForPlans>(model);
                _paymentHistoryInfoForPlansRepository.Add(locationInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Information added successfully");
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }        

    }
}