﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Services.Abstract.SuperAdmin;
using AutoMapper;
using CDC.Data.Repositories;
using CDC.ViewModel.Common;
using LinqKit;
using CDC.Entities.PaymentHistory;
using CDC.ViewModel.PaymentHistory;
using CDC.Services.Abstract;
using System.Data.Entity;

namespace CDC.Services.Services.SuperAdmin
{
    public class PaymentHistoryService : IPaymentHistoryService
    {
        private readonly IEntityBaseRepository<PaymentHistoryInfo> _paymentHistoryInfoRepository;
        private readonly ICommonService _icommonService;

        public PaymentHistoryService(IEntityBaseRepository<PaymentHistoryInfo> paymentHistoryInfoRepository,ICommonService icommnService
          )
        {
            _paymentHistoryInfoRepository = paymentHistoryInfoRepository;
            _icommonService = icommnService;
        }


   

        /// <summary>
        ///     Gets all agency Payment History.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>

        public void GetAllAgencyPaymentHistory(SearchPaymentHistoryViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<PaymentHistoryInfo>();

                if (!string.IsNullOrEmpty(model.AgencyName))
                    predicate = predicate.And(p => p.OrganizationName.ToLower().Contains(model.AgencyName.ToLower()));
                if (!string.IsNullOrEmpty(model.PlanName))
                    predicate = predicate.And(p => p.PlanName.ToLower().Contains(model.PlanName.ToLower()));
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.OrganizationID == model.AgencyId);
                if(!string.IsNullOrEmpty(model.Email))
                    predicate = predicate.And(p => p.EmailAddress.ToLower().Contains(model.Email.ToLower()));
                if (model.FromDate != null)
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.CreatedDate.Value) >= DbFunctions.TruncateTime(model.FromDate.Value));
                if (model.ToDate != null)
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.CreatedDate.Value) <= DbFunctions.TruncateTime(model.ToDate.Value));
                
                if (!string.IsNullOrEmpty(model.PaymentStatus))
                    predicate = predicate.And(p => p.EventType==model.PaymentStatus);

                predicate = predicate.And(p => p.EventType == "invoice.payment_succeeded" || p.EventType == "invoice.payment_failed");
                var resultData = _paymentHistoryInfoRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "AgencyName":
                        resultData = resultData.OrderBy(m => m.OrganizationName);
                        break;
                    case "-AgencyName":
                        resultData = resultData.OrderByDescending(m => m.OrganizationName);
                        break;
                    case "Email":
                        resultData = resultData.OrderBy(m => m.EmailAddress);
                        break;
                    case "-Email":
                        resultData = resultData.OrderByDescending(m => m.EmailAddress);
                        break;
                    case "PhoneNumber":
                        resultData = resultData.OrderBy(m => m.PhoneNumber);
                        break;
                    case "-PhoneNumber":
                        resultData = resultData.OrderByDescending(m => m.PhoneNumber);
                        break;
                    case "PlanName":
                        resultData = resultData.OrderBy(m => m.PlanName);
                        break;
                    case "-PlanName":
                        resultData = resultData.OrderByDescending(m => m.PlanName);
                        break;
                    case "PlanAmount":
                        resultData = resultData.OrderBy(m => m.PlanAmount);
                        break;
                    case "-PlanAmount":
                        resultData = resultData.OrderByDescending(m => m.PlanAmount);
                        break;
                    case "CreatedDate":
                        resultData = resultData.OrderBy(m => m.CreatedDate);
                        break;
                    case "-CreatedDate":
                        resultData = resultData.OrderByDescending(m => m.CreatedDate);
                        break;
                    case "StartDate":
                        resultData = resultData.OrderBy(m => m.StartDate);
                        break;
                    case "-StartDate":
                        resultData = resultData.OrderByDescending(m => m.StartDate);
                        break;
                    case "ExpiryDate":
                        resultData = resultData.OrderBy(m => m.PlanExpiryDate);
                        break;
                    case "-ExpiryDate":
                        resultData = resultData.OrderByDescending(m => m.PlanExpiryDate);
                        break;
                    case "EventType":
                        resultData = resultData.OrderBy(m => m.EventType);
                        break;
                    case "-EventType":
                        resultData = resultData.OrderByDescending(m => m.EventType);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.OrganizationName);
                var returnData = resultData.AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
             
                var agencyPaymentList = Mapper.Map<List<PaymentHistoryInfo>, List<PaymentHistoryInfoViewModel>>(returnData);
                foreach (var item in agencyPaymentList)
                {
                    item.CreatedDate = _icommonService.ConvertFromUTC(item.CreatedDate.Value, model.TimeZone).Value;
                }     
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = agencyPaymentList;

            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
