﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Services.Abstract.SuperAdmin;
using AutoMapper;
using CDC.Data.Repositories;
using CDC.Data.Infrastructure;
using CDC.ViewModel.Common;
using LinqKit;
using CDC.Entities.Pricing;

namespace CDC.Services.Services.SuperAdmin
{
    
    public class ParticipantPlanService : IParticipantPlanService
    {
        private readonly IEntityBaseRepository<PricingPlan> _pricingPlanRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ParticipantPlanService(IEntityBaseRepository<PricingPlan> pricingPlanRepository,
                                    IUnitOfWork unitOfWork)
        {
            _pricingPlanRepository = pricingPlanRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Gets all participant plan.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>

        public void GetAllParticipantPlan(SearchParticipantPlanViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<PricingPlan>();
                if (model == null)
                    return;
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));
                if (model.Price > 0)
                    predicate = predicate.And(p => p.Price == model.Price);
                if (model.MinNumberOfParticipants > 0)
                    predicate = predicate.And(p => p.Price == model.MinNumberOfParticipants);
                if (model.MaxNumberOfParticipants > 0)
                    predicate = predicate.And(p => p.Price == model.MaxNumberOfParticipants);
                if (model.TimeClockUsers > 0)
                    predicate = predicate.And(p => p.Price == model.TimeClockUsers);
                var resultData = _pricingPlanRepository.GetAll();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;
                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                    case "Price":
                        resultData = resultData.OrderBy(m => m.Price);
                        break;
                    case "-Price":
                        resultData = resultData.OrderByDescending(m => m.Price);
                        break;
                    case "MinNumberOfParticipants":
                        resultData = resultData.OrderBy(m => m.MinNumberOfParticipants);
                        break;
                    case "-MinNumberOfParticipants":
                        resultData = resultData.OrderByDescending(m => m.MinNumberOfParticipants);
                        break;
                    case "MaxNumberOfParticipants":
                        resultData = resultData.OrderBy(m => m.MaxNumberOfParticipants);
                        break;
                    case "-MaxNumberOfParticipants":
                        resultData = resultData.OrderByDescending(m => m.MaxNumberOfParticipants);
                        break;
                    case "TimeClockUsers":
                        resultData = resultData.OrderBy(m => m.MaxNumberOfParticipants);
                        break;
                    case "-TimeClockUsers":
                        resultData = resultData.OrderByDescending(m => m.TimeClockUsers);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var participantplanList = Mapper.Map<List<PricingPlan>, List<PricingPlanViewModel>>(returnData);
                responseInfo.IsSuccess = true;
                responseInfo.Content = participantplanList;
                long totalCount = resultData.Count();
                responseInfo.TotalRows = totalCount;

            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }


     

        /// <summary>
        ///     Delete Participant Plan by identifier.
        /// </summary>
        /// <param name="participantplanId">The Participant Plan identifier.</param>
        /// <param name="responseInfo">The response information.</param>
        public void DeleteParticipantPlanById(long participantplanId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var participantplanDetails =
                    _pricingPlanRepository.FindBy(m => m.ID == participantplanId && m.IsDeleted == false).FirstOrDefault();
                if (participantplanDetails != null && participantplanDetails.ID > 0)
                {
                    participantplanDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Participant Plan successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }


        /// <summary>
        ///     Add or update PricingPlan.
        /// </summary>
        /// <param name="addPricingPlanViewModel">The add room view model.</param>
        /// <returns></returns>
        public int AddUpdateParticipantPlan(PricingPlanViewModel addPricingPlanViewModel)
        {
            try
            {
                PricingPlan pricingplan;
                if (addPricingPlanViewModel.ID == 0)
                {
                
                    pricingplan = Mapper.Map<PricingPlanViewModel, PricingPlan>(addPricingPlanViewModel);
                    _pricingPlanRepository.Add(pricingplan);                   

                }
                else
                {
                    var pricingplanDetail =
                        _pricingPlanRepository.FindBy(m => m.ID == addPricingPlanViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (pricingplanDetail != null)
                    {
                        addPricingPlanViewModel.IsDeleted = false;
                        pricingplan = Mapper.Map<PricingPlanViewModel, PricingPlan>(addPricingPlanViewModel);
                        _pricingPlanRepository.Edit(pricingplanDetail, pricingplan);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void getPricingPlan(long PlanId, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                PricingPlan participantplanStripeData =
                    _pricingPlanRepository.FindBy(m => m.ID == PlanId && m.IsDeleted == false).ToList().FirstOrDefault();
                if (participantplanStripeData != null)
                {
                    responseInfo.ReturnStatus = true;
                    responseInfo.Content = participantplanStripeData;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

    }
}
