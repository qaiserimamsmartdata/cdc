﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Services.Abstract.SuperAdmin;
using AutoMapper;
using CDC.Data.Repositories;
using CDC.Data.Infrastructure;
using CDC.ViewModel.Common;
using CDC.Entities.Pricing;

namespace CDC.Services.Services.SuperAdmin
{
    
    public class ParticipantPlanAssociationService : IParticipantPlanAssociationService
    {
        private readonly IEntityBaseRepository<PricingPlanAssociations> _pricingPlanAssociationRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ParticipantPlanAssociationService(IEntityBaseRepository<PricingPlanAssociations> pricingPlanAssociationRepository,
                                    IUnitOfWork unitOfWork)
        {
            _pricingPlanAssociationRepository = pricingPlanAssociationRepository;
            _unitOfWork = unitOfWork;
        }

        public void GetAllParticipantAssociationPlan(SearchParticipantPlanViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                

                var resultData = _pricingPlanAssociationRepository.GetAll();
                long totalCount = resultData.Count();
                //resultData=resultData.OrderBy(m => m.ID);
                //switch (model.order)
                //{
                //    case "id":
                //        resultData = resultData.OrderBy(m => m.ID);
                //        break;
                //    case "-id":
                //        resultData = resultData.OrderByDescending(m => m.ID);
                //        break;
                //    case "Name":
                //        resultData = resultData.OrderBy(m => m.Name);
                //        break;
                //    case "-Name":
                //        resultData = resultData.OrderByDescending(m => m.Name);
                //        break;
                //    case "Price":
                //        resultData = resultData.OrderBy(m => m.Price);
                //        break;
                //    case "-Price":
                //        resultData = resultData.OrderByDescending(m => m.Price);
                //        break;
                //    case "MinNumberOfParticipants":
                //        resultData = resultData.OrderBy(m => m.MinNumberOfParticipants);
                //        break;
                //    case "-MinNumberOfParticipants":
                //        resultData = resultData.OrderByDescending(m => m.MinNumberOfParticipants);
                //        break;
                //    case "MaxNumberOfParticipants":
                //        resultData = resultData.OrderBy(m => m.MaxNumberOfParticipants);
                //        break;
                //    case "-MaxNumberOfParticipants":
                //        resultData = resultData.OrderByDescending(m => m.MaxNumberOfParticipants);
                //        break;
                //    case "TimeClockUsers":
                //        resultData = resultData.OrderBy(m => m.MaxNumberOfParticipants);
                //        break;
                //    case "-TimeClockUsers":
                //        resultData = resultData.OrderByDescending(m => m.TimeClockUsers);
                //        break;
                //}
                //if (string.IsNullOrEmpty(model.order))
                //    resultData = resultData.OrderByDescending(m => m.Name);
                //var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                //    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                //foreach (var item in returnData)
                //{
                //    if (item.AgencyScheduleList.Count > 0)
                //    {
                //        item.StaffScheduleList = item.StaffScheduleList.Where(s => s.IsDeleted == false).ToList();
                //    }
                //}
                var participantplanList = Mapper.Map<List<PricingPlanAssociations>, List<PricingPlanAssociationsViewModel>>(resultData.ToList());
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = participantplanList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    
        public void DeleteParticipantAssociationPlanById(long participantplanId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var participantplanDetails =
                    _pricingPlanAssociationRepository.FindBy(m => m.ID == participantplanId && m.IsDeleted == false).FirstOrDefault();
                if (participantplanDetails != null && participantplanDetails.ID > 0)
                {
                    participantplanDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Participant Plan successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        public int AddUpdateParticipantAssociationPlan(PricingPlanAssociationsViewModel addPricingPlanViewModel)
        {
            try
            {
                if (addPricingPlanViewModel.ID == 0)
                {
                    var pricingplan = Mapper.Map<PricingPlanAssociationsViewModel, PricingPlanAssociations>(addPricingPlanViewModel);
                    _pricingPlanAssociationRepository.Add(pricingplan);
                }
                else
                {
                    var pricingplanDetail =
                        _pricingPlanAssociationRepository.FindBy(m => m.ID == addPricingPlanViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (pricingplanDetail != null)
                    {
                        addPricingPlanViewModel.IsDeleted = false;
                        var pricingplan = Mapper.Map<PricingPlanAssociationsViewModel, PricingPlanAssociations>(addPricingPlanViewModel);
                        _pricingPlanAssociationRepository.Edit(pricingplanDetail, pricingplan);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }
      
        
        public void getPricingPlanAssociation(long PlanId, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var data = _pricingPlanAssociationRepository.FindBy(m => m.PricingPlanId == PlanId && m.IsDeleted == false).ToList();
                responseInfo.ReturnStatus = true;
                responseInfo.Content = data;
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

    }
}
