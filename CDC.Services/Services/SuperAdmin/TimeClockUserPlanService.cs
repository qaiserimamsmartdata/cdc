﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Services.Abstract.SuperAdmin;
using AutoMapper;
using CDC.Data.Repositories;
using CDC.Data.Infrastructure;
using CDC.ViewModel.Common;
using LinqKit;
using CDC.Entities.TimeClockUsers;


namespace CDC.Services.Services.SuperAdmin
{
    public class TimeClockUserPlanService : ITimeClockUserPlanService
    {

        private readonly IEntityBaseRepository<TimeClockUsersPlan> _timeclockuserPlanRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TimeClockUserPlanService(IEntityBaseRepository<TimeClockUsersPlan> timeclockuserPlanRepository,
                                IUnitOfWork unitOfWork)
        {
            _timeclockuserPlanRepository = timeclockuserPlanRepository;
            _unitOfWork = unitOfWork;
        }
      
        public int AddUpdateTimeClockUserPlan(TimeClockUsersViewModel addTimeClockUsersPlanViewModel)
        {
            try
            {
                

                if (addTimeClockUsersPlanViewModel.ID == 0)
                {
                    var timeclockuserplan = Mapper.Map<TimeClockUsersViewModel, TimeClockUsersPlan>(addTimeClockUsersPlanViewModel);
                    _timeclockuserPlanRepository.Add(timeclockuserplan);
                }
                else
                {
                    var timeclockuserplanDetail =
                        _timeclockuserPlanRepository.FindBy(m => m.ID == addTimeClockUsersPlanViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (timeclockuserplanDetail != null)
                    {
                        addTimeClockUsersPlanViewModel.IsDeleted = false;
                        var timeclockuserplan = Mapper.Map<TimeClockUsersViewModel, TimeClockUsersPlan>(addTimeClockUsersPlanViewModel);
                        _timeclockuserPlanRepository.Edit(timeclockuserplanDetail, timeclockuserplan);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void DeleteTimeClockUserById(long timeclockuserplanId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var timeclockuserplanDetails =
                    _timeclockuserPlanRepository.FindBy(m => m.ID == timeclockuserplanId && m.IsDeleted == false).FirstOrDefault();
                if (timeclockuserplanDetails != null && timeclockuserplanDetails.ID > 0)
                {
                    timeclockuserplanDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Time clock User Plan successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        public void GetAllTimeClockUserPlan(SearchTimeClockUserPlanViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<TimeClockUsersPlan>();

                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));
                if (model.Price > 0)
                    predicate = predicate.And(p => p.Price == model.Price);
                if (model.MaxNumberOfTimeClockUsers > 0)
                    predicate = predicate.And(p => p.Price == model.MaxNumberOfTimeClockUsers);
                var resultData = _timeclockuserPlanRepository.GetAll();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;
                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                    case "Price":
                        resultData = resultData.OrderBy(m => m.Price);
                        break;
                    case "-Price":
                        resultData = resultData.OrderByDescending(m => m.Price);
                        break;
                    case "MaxNumberOfTimeClockUsers":
                        resultData = resultData.OrderBy(m => m.MaxNumberOfTimeClockUsers);
                        break;
                    case "-MaxNumberOfTimeClockUsers":
                        resultData = resultData.OrderByDescending(m => m.MaxNumberOfTimeClockUsers);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var timeclockuserplanList = Mapper.Map<List<TimeClockUsersPlan>, List<TimeClockUsersViewModel>>(returnData);
                responseInfo.IsSuccess = true;
                responseInfo.Content = timeclockuserplanList;
                long totalCount = resultData.Count();
                responseInfo.TotalRows = totalCount;
                
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
        public void getTimeClockPlan(long PlanId, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                TimeClockUsersPlan timeclockplanStripeData =
                    _timeclockuserPlanRepository.FindBy(m => m.ID == PlanId && m.IsDeleted == false).ToList().FirstOrDefault();
                if (timeclockplanStripeData != null)
                {
                    responseInfo.ReturnStatus = true;
                    responseInfo.Content = timeclockplanStripeData;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
