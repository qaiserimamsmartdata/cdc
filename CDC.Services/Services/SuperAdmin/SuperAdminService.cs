﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Services.Abstract.SuperAdmin;
using CDC.ViewModel.AgencyRegistration;
using AutoMapper;
using CDC.Entities.AgencyRegistration;
using CDC.Data.Repositories;
using CDC.Data.Infrastructure;
using CDC.ViewModel.Common;
using LinqKit;

namespace CDC.Services.Services.SuperAdmin
{
    public class SuperAdminService : ISuperAdminService
    {
        private readonly IEntityBaseRepository<AgencyRegistrationInfo> _agencyRegistrationInfoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SuperAdminService(IEntityBaseRepository<AgencyRegistrationInfo> agencyRegistrationInfoRepository, IUnitOfWork unitOfWork)
        {
            _agencyRegistrationInfoRepository = agencyRegistrationInfoRepository;
            _unitOfWork = unitOfWork;
        }


   

        /// <summary>
        ///     Gets all agency.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>

        public void GetAllAgency(SearchAgencyViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<AgencyRegistrationInfo>();

                if (!string.IsNullOrEmpty(model.AgencyName))
                    predicate = predicate.And(p => p.AgencyName.ToLower().Contains(model.AgencyName.ToLower()));
                if (!string.IsNullOrEmpty(model.PhoneNumber))
                    predicate = predicate.And(p => p.Email.Contains(model.PhoneNumber));
                if (!string.IsNullOrEmpty(model.Email))
                    predicate = predicate.And(p => p.Email.Contains(model.Email));
                if (!string.IsNullOrEmpty(model.Address))
                    predicate = predicate.And(p => p.Email.Contains(model.Address));
                var resultData = _agencyRegistrationInfoRepository.GetAll();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "AgencyName":
                        resultData = resultData.OrderBy(m => m.AgencyName);
                        break;
                    case "-AgencyName":
                        resultData = resultData.OrderByDescending(m => m.AgencyName);
                        break;
                    case "PhoneNumber":
                        //resultData = resultData.OrderBy(m => m.PhoneNumber);
                        break;
                    case "-PhoneNumber":
                        //resultData = resultData.OrderByDescending(m => m.PhoneNumber);
                        break;
                    case "Email":
                        resultData = resultData.OrderBy(m => m.Email);
                        break;
                    case "-Email":
                        resultData = resultData.OrderByDescending(m => m.Email);
                        break;
                    case "Address":
                        //resultData = resultData.OrderBy(m => m.Address);
                        break;
                    case "-Address":
                        //resultData = resultData.OrderByDescending(m => m.Address);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.AgencyName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var agencyList = Mapper.Map<List<AgencyRegistrationInfo>, List<AgencyRegistrationViewModel>>(returnData);      
                foreach (var agency in agencyList)
                {
                    var resultAgencyRegisterInfo = _agencyRegistrationInfoRepository.GetSingle(agency.ID);
                    if (agency.ID > 0)
                    {
                        agency.CityName =
                            resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.CityName;
                        agency.PhoneNumber =
                            resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.PhoneNumber;
                        agency.Address =
                            resultAgencyRegisterInfo.AgencyContactInfos.FirstOrDefault()?.Address;
                    }
                }
                
                responseInfo.IsSuccess = true;
                responseInfo.Content = agencyList;
                long totalCount = resultData.Count();
                responseInfo.TotalRows = totalCount;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }


        /// <summary>
        ///     Deletes Agency by identifier.
        /// </summary>
        /// <param name="agencyId">The Agency identifier.</param>
        /// <param name="responseInfo">The response information.</param>
        public void DeleteAgencyById(long agencyId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var agencyDetails =
                    _agencyRegistrationInfoRepository.FindBy(m => m.ID == agencyId && m.IsDeleted == false).FirstOrDefault();
                if (agencyDetails != null && agencyDetails.ID > 0)
                {
                    agencyDetails.IsDeleted = true;
                    //agencyDetails.Email = null;
                     _agencyRegistrationInfoRepository.FindBy(m => m.ID == agencyId).FirstOrDefault().Email = null;
                   // _userRepository.FindBy(m => m.ID == agencyId).FirstOrDefault().EmailId = null;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Agency successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
