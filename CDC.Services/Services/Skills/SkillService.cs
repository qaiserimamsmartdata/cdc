﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Services.Abstract.Skills;
using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;
using LinqKit;

namespace CDC.Services.Services.Skills
{
    public class SkillService : ISkillService
    {
        private readonly IEntityBaseRepository<Entities.Skills.Skills> _skillRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SkillService(
            IEntityBaseRepository<Entities.Skills.Skills> skillRepository, IUnitOfWork unitOfWork
            )
        {
            _skillRepository = skillRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Get all the skill list
        /// </summary>
        /// <param name="model"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<SkillViewModel> GetSkills(SearchSkillViewModel model, out int totalCount)
        {
            //Int32 TotalCount = _skillRepository.GetAll().Count();
            //var resultData = _skillRepository.GetAll();
            var predicate = PredicateBuilder.True<Entities.Skills.Skills>();
            if (!string.IsNullOrEmpty(model.name))
                predicate = predicate.And(p => p.Skill.Contains(model.name));
            var resultData = _skillRepository.GetAll().AsExpandable().Where(predicate).OrderBy(m => m.ID);
            switch (model.order)
            {
                case "Skill":
                    resultData = resultData.OrderBy(m => m.Skill);
                    break;
                case "-Skill":
                    resultData = resultData.OrderByDescending(m => m.Skill);
                    break;
                case "Subskill":
                    resultData = resultData.OrderBy(m => m.SubSkill);
                    break;
                case "-Subskill":
                    resultData = resultData.OrderByDescending(m => m.SubSkill);
                    break;
                case "Detail":
                    resultData = resultData.OrderBy(m => m.Details);
                    break;
                case "-Detail":
                    resultData = resultData.OrderByDescending(m => m.Details);
                    break;
                case "DaysReq":
                    resultData = resultData.OrderBy(m => m.DaysRequired);
                    break;
                case "-DaysReq":
                    resultData = resultData.OrderByDescending(m => m.DaysRequired);
                    break;
                case "ClassesReq":
                    resultData = resultData.OrderBy(m => m.ClassesRequired);
                    break;
                case "-ClassesReq":
                    resultData = resultData.OrderByDescending(m => m.ClassesRequired);
                    break;
            }
            totalCount = resultData.Count();
            var returnData = resultData.Select(m =>
                new SkillViewModel
                {
                    ID = m.ID,
                    Skill = m.Skill,
                    SubSkill = string.IsNullOrEmpty(m.SubSkill) == false ? m.SubSkill : "N/A",
                    ClassesRequired = m.ClassesRequired,
                    DaysRequired = m.DaysRequired,
                    Details = m.Details,
                    CategoryId = m.CategoryId
                }).Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
            return returnData;
        }

        /// <summary>
        /// Get skill by SkillId
        /// </summary>
        /// <param name="skillId"></param>
        /// <returns></returns>
        public SkillViewModel GetSkillById(long skillId)
        {
            var resultData = _skillRepository.GetSingle(skillId);
            if (resultData != null)
            {
                var returnData =
                    new SkillViewModel
                    {
                        ID = resultData.ID,
                        Skill = resultData.Skill,
                        SubSkill = resultData.SubSkill,
                        ClassesRequired = resultData.ClassesRequired,
                        DaysRequired = resultData.DaysRequired,
                        Details = resultData.Details,
                        CategoryId = resultData.CategoryId
                    };
                return returnData;
            }
            return null;
        }

        /// <summary>
        /// Add Skills
        /// </summary>
        /// <param name="addSkillViewModel"></param>
        /// <returns></returns>
        public int AddSkill(SkillViewModel addSkillViewModel)
        {
            try
            {
                var skill = new Entities.Skills.Skills
                {
                    Skill = addSkillViewModel.Skill,
                    SubSkill = addSkillViewModel.SubSkill,
                    Details = addSkillViewModel.Details,
                    DaysRequired = addSkillViewModel.DaysRequired,
                    ClassesRequired = addSkillViewModel.ClassesRequired,
                    CategoryId = addSkillViewModel.CategoryId,
                    AgencyId = addSkillViewModel.AgencyId,
                    IsDeleted = false
                };
                _skillRepository.Add(skill);

                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Update skills
        /// </summary>
        /// <param name="model"></param>
        /// <param name="skillId"></param>
        /// <returns></returns>
        public int UpdateSkill(SkillViewModel model, long skillId)
        {
            var resultData = 0;
            try
            {
                var skillDetail = _skillRepository.FindBy(m => m.ID == skillId && m.IsDeleted == false).FirstOrDefault();
                if (skillDetail != null)
                {
                    skillDetail.CategoryId = model.CategoryId;
                    skillDetail.ClassesRequired = model.ClassesRequired;
                    skillDetail.DaysRequired = model.DaysRequired;
                    skillDetail.Details = model.Details;
                    skillDetail.Skill = model.Skill;
                    skillDetail.SubSkill = model.SubSkill;
                    _unitOfWork.Commit();
                    resultData = 1;
                    return resultData;
                }
                return resultData;
            }
            catch (Exception)
            {
                return resultData;
            }
        }

        /// <summary>
        /// Delete skills
        /// </summary>
        /// <param name="skillId"></param>
        /// <returns></returns>
        public int DeleteSkillById(long skillId)
        {
            var returnState = 0;
            try
            {
                long count = _skillRepository.FindBy(d => d.ID == skillId && d.IsDeleted == false).Count();
                if (count > 0)
                {
                    var skillDetail =
                        _skillRepository.FindBy(m => m.ID == skillId && m.IsDeleted == false).FirstOrDefault();
                    if (skillDetail != null) skillDetail.IsDeleted = true;
                    _unitOfWork.Commit();
                    returnState = 2;
                    return returnState;
                }
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }
    }
}