﻿using CDC.Services.Abstract.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;
using CDC.Data.Infrastructure;
using CDC.Entities.Skills;
using CDC.Data.Repositories;
using LinqKit;
using AutoMapper;

namespace CDC.Services.Services.Skills
{
    public class SkillMasterService : ISkillMasterService
    {
        private readonly IEntityBaseRepository<SkillMaster> _skillmasterRepository;
        private readonly IUnitOfWork _unitOfWork;
        public SkillMasterService(IEntityBaseRepository<SkillMaster> skillmasterRepository, IUnitOfWork unitOfWork)
        {
            _skillmasterRepository = skillmasterRepository;
            _unitOfWork = unitOfWork;
        }
        public int AddUpdateSkillMaster(SkillMasterViewModel skillmasterViewModel)
        {
            try
            {
                if (skillmasterViewModel.ID == 0)
                {
                    var skillmaster = Mapper.Map<SkillMasterViewModel, SkillMaster>(skillmasterViewModel);
                    _skillmasterRepository.Add(skillmaster);
                }
                else
                {
                    var skillmasterDetail =
                        _skillmasterRepository.FindBy(m => m.ID == skillmasterViewModel.ID)
                            .FirstOrDefault();
                    if (skillmasterDetail != null)
                    {
                        skillmasterViewModel.IsDeleted = false;
                        var skillmaster = Mapper.Map<SkillMasterViewModel, SkillMaster>(skillmasterViewModel);
                        _skillmasterRepository.Edit(skillmasterDetail, skillmaster);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {

                return 0;
            }
        }

        public int DeleteSkillMaster(SkillMasterViewModel skillmasterViewModel)
        {
            var returnState = 0;
            try
            {
                long count =
                    _skillmasterRepository.FindBy(d => d.ID == skillmasterViewModel.ID && d.IsDeleted == false)
                        .Count();
                if (count <= 0) return returnState;
                var skillmasterDetails =
                    _skillmasterRepository.FindBy(m => m.ID == skillmasterViewModel.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (skillmasterDetails != null) skillmasterDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void GetAllSkillMaster(SearchSkillMasterViewModel searchModel, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<SkillMaster>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (searchModel.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == searchModel.AgencyId);
                if (!string.IsNullOrEmpty(searchModel.Skill))
                    predicate = predicate.And(p => p.Skill.ToLower().Contains(searchModel.Skill.ToLower()));
                var resultData = _skillmasterRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (searchModel.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Skill":
                        resultData = resultData.OrderBy(m => m.Skill);
                        break;

                    case "-Skill":
                        resultData = resultData.OrderByDescending(m => m.Skill);
                        break;
                }
                if (string.IsNullOrEmpty(searchModel.order))
                    resultData = resultData.OrderByDescending(m => m.Skill);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((searchModel.page - 1) * searchModel.limit).Take(searchModel.limit).ToList();
                var skillmasterList = Mapper.Map<List<SkillMaster>, List<SkillMasterViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = skillmasterList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
