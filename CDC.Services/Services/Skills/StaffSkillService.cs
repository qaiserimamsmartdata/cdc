﻿using CDC.Services.Abstract.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;
using CDC.Data.Repositories;
using CDC.Data.Infrastructure;
using LinqKit;
using CDC.Entities.Skills;
using AutoMapper;

namespace CDC.Services.Services.Skills
{
    public class StaffSkillService : IStaffSkillService
    {
        private readonly IEntityBaseRepository<StaffSkill> _staffskillRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StaffSkillService(IEntityBaseRepository<StaffSkill> staffskillRepository,
                             IUnitOfWork unitOfWork)
        {
            _staffskillRepository = staffskillRepository;
            _unitOfWork = unitOfWork;
        }
        public int AddUpdateStaffSkill(StaffSkillViewModel staffskillViewModel)
        {
            try
            {
                if (staffskillViewModel.ID == 0)
                {
                    var staffskill = Mapper.Map<StaffSkillViewModel, StaffSkill>(staffskillViewModel);
                    _staffskillRepository.Add(staffskill);
                }
                else
                {
                    var staffskillDetail =
                        _staffskillRepository.FindBy(m => m.ID == staffskillViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (staffskillDetail != null)
                    {
                        staffskillDetail.IsDeleted = false;
                        var staffskill = Mapper.Map<StaffSkillViewModel, StaffSkill>(staffskillViewModel);
                        _staffskillRepository.Edit(staffskillDetail, staffskill);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void DeleteStaffSkillById(long staffskillId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var staffskillDetails =
                    _staffskillRepository.FindBy(m => m.ID == staffskillId && m.IsDeleted == false).FirstOrDefault();
                if (staffskillDetails != null && staffskillDetails.ID > 0)
                {
                    staffskillDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("StaffSkill successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        public void GetAllStaffSkill(SearchStaffSkillViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<StaffSkill>();

                if (!string.IsNullOrEmpty(model.Description))
                    predicate = predicate.And(p => p.Description.ToLower().Contains(model.Description.ToLower()));
                if (model.SkillMasterId > 0)
                    predicate = predicate.And(p => p.SkillMasterId == model.SkillMasterId);
                if (model.StaffId > 0)
                    predicate = predicate.And(p => p.StaffId == model.StaffId);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                var resultData = _staffskillRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Description":
                        resultData = resultData.OrderBy(m => m.Description);
                        break;
                    case "-Description":
                        resultData = resultData.OrderByDescending(m => m.Description);
                        break;
                    case "SkillMasterId":
                        resultData = resultData.OrderBy(m => m.SkillMasterId);
                        break;
                    case "-SkillMasterId":
                        resultData = resultData.OrderByDescending(m => m.SkillMasterId);
                        break;

                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Description);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
          
                var staffskillList = Mapper.Map<List<StaffSkill>, List<StaffSkillViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffskillList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
