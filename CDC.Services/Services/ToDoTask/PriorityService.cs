﻿using CDC.Services.Abstract.ToDoTask;
using System;
using System.Collections.Generic;
using System.Linq;
using CDC.ViewModel.Common;
using CDC.ViewModel.ToDoTask;
using CDC.Data.Repositories;
using CDC.Entities.ToDoTask;
using CDC.Data.Infrastructure;
using AutoMapper;
using LinqKit;

namespace CDC.Services.Services.ToDoTask
{
    public class PriorityService : IPriorityService
    {
        private readonly IEntityBaseRepository<Priority> _priorityRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PriorityService(IEntityBaseRepository<Priority> priorityRepository, IUnitOfWork unitOfWork)
        {
            _priorityRepository = priorityRepository;
            _unitOfWork = unitOfWork;
        }

        public int AddUpdatePriority(PriorityViewModel priorityViewModel)
        {
            try
            {
                if (priorityViewModel.ID == 0)
                {
                    var priority = Mapper.Map<PriorityViewModel,Priority>(priorityViewModel);
                    _priorityRepository.Add(priority);
                }
                else
                {
                    var priorityDetail =
                        _priorityRepository.FindBy(m => m.ID == priorityViewModel.ID)
                            .FirstOrDefault();
                    if (priorityDetail != null)
                    {
                        priorityViewModel.IsDeleted = false;
                        var priority = Mapper.Map<PriorityViewModel, Priority>(priorityViewModel);
                        _priorityRepository.Edit(priorityDetail, priority);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                
                return 0;
            }
        }

        public int DeletePriority(PriorityViewModel priorityViewModel)
        {
            var returnState = 0;
            try
            {
                long count =
                    _priorityRepository.FindBy(d => d.ID == priorityViewModel.ID && d.IsDeleted == false)
                        .Count();
                if (count <= 0) return returnState;
                var priorityDetails =
                    _priorityRepository.FindBy(m => m.ID == priorityViewModel.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (priorityDetails != null) priorityDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void GetAllPriority(SearchPriorityViewModel searchModel, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Priority>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (searchModel.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == searchModel.AgencyId);
                if (!string.IsNullOrEmpty(searchModel.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(searchModel.Name.ToLower()));

                var resultData = _priorityRepository.GetAll();
                resultData = resultData.Where(m => m.AgencyId == searchModel.AgencyId);
                long totalCount = resultData.Count();

                switch (searchModel.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(searchModel.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((searchModel.page - 1) * searchModel.limit).Take(searchModel.limit).ToList();
                var priorityList = Mapper.Map<List<Priority>, List<PriorityViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = priorityList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
