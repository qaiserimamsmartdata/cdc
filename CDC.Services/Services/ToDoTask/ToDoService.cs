﻿using CDC.Services.Abstract.ToDoTask;
using System;
using System.Collections.Generic;
using System.Linq;
using CDC.ViewModel.Common;
using CDC.ViewModel.ToDoTask;
using CDC.Entities.ToDoTask;
using CDC.Data.Repositories;
using CDC.Data.Infrastructure;
using AutoMapper;
using LinqKit;
using System.Data.Entity;

namespace CDC.Services.Services.ToDoTask
{
    public class ToDoService : IToDoService
    {
        private readonly IEntityBaseRepository<ToDo> _toDoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ToDoService(IEntityBaseRepository<ToDo> toDoRepository,
                                IUnitOfWork unitOfWork)
        {
            _toDoRepository = toDoRepository;
            _unitOfWork = unitOfWork;
        }

        public int AddUpdateToDo(ToDoViewModel toDoViewModel)
        {
            try
            {
                if (toDoViewModel.ID == 0)
                {
                    var toDo = Mapper.Map<ToDoViewModel, ToDo>(toDoViewModel);
                    _toDoRepository.Add(toDo);
                }
                else
                {
                    var toDoDetail =
                        _toDoRepository.FindBy(m => m.ID == toDoViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (toDoDetail != null)
                    {
                        toDoDetail.IsDeleted = false;
                        var toDo = Mapper.Map<ToDoViewModel, ToDo>(toDoViewModel);
                        _toDoRepository.Edit(toDoDetail, toDo);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public void DeleteToDoById(long toDoId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var toDoDetails =
                    _toDoRepository.FindBy(m => m.ID == toDoId && m.IsDeleted == false).FirstOrDefault();
                if (toDoDetails != null && toDoDetails.ID > 0)
                {
                    toDoDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("To do successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        public void GetAllToDo(SearchToDoViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<ToDo>();

                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));
                if (model.PriorityId > 0)
                    predicate = predicate.And(p => p.PriorityId == model.PriorityId);
                if (model.DueDate != new DateTime())
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.DueDate) == DbFunctions.TruncateTime(model.DueDate));
                //if (model.Price > 0)
                //    predicate = predicate.And(p => p.Price == model.Price);
                //if (model.MaxNumberOfTimeClockUsers > 0)
                //    predicate = predicate.And(p => p.Price == model.MaxNumberOfTimeClockUsers);
                //if (model.StatusId > 0)
                //    predicate = predicate.And(p => p.StatusId == model.StatusId);
                //if (model.PositionId > 0)
                //    predicate = predicate.And(p => p.PositionId == model.PositionId);

                var resultData = _toDoRepository.GetAll().Where(w => w.AgencyId == model.AgencyId).AsExpandable().Where(predicate).AsExpandable();
                responseInfo.TotalRows = resultData.Count();
                //resultData=resultData.OrderBy(m => m.ID);
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;
                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                    case "Priority":
                        resultData = resultData.OrderBy(m => m.PriorityId);
                        break;
                    case "-Priority":
                        resultData = resultData.OrderByDescending(m => m.PriorityId);
                        break;
                    case "DueDate":
                        resultData = resultData.OrderBy(m => m.DueDate);
                        break;
                    case "-DueDate":
                        resultData = resultData.OrderByDescending(m => m.DueDate);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                //foreach (var item in returnData)
                //{
                //    if (item.AgencyScheduleList.Count > 0)
                //    {
                //        item.StaffScheduleList = item.StaffScheduleList.Where(s => s.IsDeleted == false).ToList();
                //    }
                //}
                
                var toDoList = Mapper.Map<List<ToDo>, List<ToDoViewModel>>(returnData);
                //var newagencyList = Mapper.Map<List<AgencyRegistrationInfo>, List<AgencyListViewModel>>(returnData);
                responseInfo.IsSuccess = true;
                responseInfo.Content = toDoList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }

        }
    }
}
