﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Staff;
using CDC.Entities.User;
using CDC.ViewModel.Common;
using LinqKit;
using CDC.Services.Abstract.ParticipantIncident;
using CDC.Entities.ParticipantIncident;
using CDC.ViewModel.ParticipantIncident;
using CDC.Services.Abstract;
using CDC.Entities.Family;
using CDC.Entities.NewParticipant;
using CDC.Entities.NewParent;

namespace CDC.Services.Services.ParticipantIncident
{
    public class IncidentService : IIncidentService
    {
        private readonly IEntityBaseRepository<Incident> _incidentRepository;
        private readonly IEntityBaseRepository<IncidentParticipantMapping> _incidentParticipantRepository;
        private readonly IEntityBaseRepository<IncidentOtherParticipantMapping> _incidentOtherParticipantRepository;
        private readonly IEntityBaseRepository<ParentInfo> _parentInfoRepository;
        private readonly IEntityBaseRepository<StudentInfo> _studentRepository;
        private readonly ICommonService _icommonService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEntityBaseRepository<StaffInfo> _staffRepository;
        private readonly IEntityBaseRepository<tblParticipantInfo> _tblParticipantInfoRepository;
        private readonly  IEntityBaseRepository<tblParentInfo> _tblParentInfoRepository;
        private readonly IEntityBaseRepository<IncidentParentParticipantMapping> _incidentParentParticipantMappingRepository;
        

        public IncidentService(
            IEntityBaseRepository<Incident> incidentRepository,
            IEntityBaseRepository<StaffInfo> staffRepository,
            IEntityBaseRepository<Users> userRepository,
            IEntityBaseRepository<IncidentParticipantMapping> incidentParticipantRepository,
            IEntityBaseRepository<IncidentOtherParticipantMapping> incidentOtherParticipantRepository,
            IEntityBaseRepository<ParentInfo> parentInfoRepository,
               IEntityBaseRepository<StudentInfo> studentRepository,
            ICommonService icommonService,
            IEntityBaseRepository<tblParticipantInfo> tblParticipantInfoRepository,
            IEntityBaseRepository<tblParentInfo> tblParentInfoRepository,
            IEntityBaseRepository<IncidentParentParticipantMapping> incidentParentParticipantMappingRepository,
        IUnitOfWork unitOfWork)
        {
            _incidentRepository = incidentRepository;
            _staffRepository = staffRepository;
            _unitOfWork = unitOfWork;
            _incidentParticipantRepository = incidentParticipantRepository;
            _incidentOtherParticipantRepository = incidentOtherParticipantRepository;
            _parentInfoRepository = parentInfoRepository;
            _studentRepository = studentRepository;
                _icommonService = icommonService;
            _tblParticipantInfoRepository = tblParticipantInfoRepository;
            _tblParentInfoRepository = tblParentInfoRepository;
            _incidentParentParticipantMappingRepository = incidentParentParticipantMappingRepository;
        }
        public IncidentViewModel GetIncidentById(IDViewModel model)
        {
            var incidentObj = _incidentRepository.GetSingle(model.IncidentId);
            var incidentMappedObj = Mapper.Map<Incident, IncidentViewModel>(incidentObj);
            if(incidentMappedObj!=null)
            {
                incidentMappedObj.AgeOfChild = _icommonService.CalculateAge(incidentMappedObj.IncidentParticipant.FirstOrDefault().StudentInfo.DateOfBirth.Value);
                incidentMappedObj.StaffName = incidentMappedObj.StaffInfo.FullName;
                incidentMappedObj.TimeOfAccident = _icommonService.ConvertTimeFromUTC(incidentMappedObj.DateOfAccident.Value,incidentMappedObj.TimeOfAccident.Value, model.TimeZone);
                //incidentMappedObj.ListStudentInfoIds.add
                var ParentInfo= _tblParentInfoRepository.All.Where(m => m.ID == incidentMappedObj.ParentId).ToList().FirstOrDefault();
                incidentMappedObj.ParentName = ParentInfo.FirstName + " " + ParentInfo.LastName;
                incidentMappedObj.PhoneNumber = ParentInfo.Mobile;
                incidentMappedObj.ParticipantName= incidentMappedObj.IncidentParticipant.FirstOrDefault().StudentInfo.FirstName +" "+incidentMappedObj.IncidentParticipant.FirstOrDefault().StudentInfo.LastName;
                string otherParticipants = incidentMappedObj.IncidentOtherParticipant.Aggregate("", (current, item) => current + (item.OtherStudentInfo.FirstName + " " + item.OtherStudentInfo.LastName + ","));
                if (otherParticipants.Length > 0)
                    incidentMappedObj.OtherParticipants =otherParticipants.Remove(otherParticipants.Length - 1);

            }
            return incidentMappedObj;
        }
       
        public void GetAllIncident(SearchIncidentViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Incident>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyRegistrationId == model.AgencyId);
                if (model.FamilyId > 0)
                    predicate = predicate.And(p => p.IncidentParentParticipant.FirstOrDefault().ParentInfoId == model.FamilyId);
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate =
                        predicate.And(p => p.ParticipantName.Contains(model.ParticipantName));
                if (model.StaffInfoId > 0)
                    predicate = predicate.And(p => p.StaffInfoId == model.StaffInfoId);
                var resultData = _incidentRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Name":
                        
                        resultData = resultData.OrderBy(m => m.ParticipantName);
                        break;
                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.ParticipantName);
                        break;
                        case "ReporterName":
                        resultData = resultData.OrderBy(m => m.ReporterName);
                        break;
                    case "-ReporterName":
                        resultData = resultData.OrderByDescending(m => m.ReporterName);
                        break;
                    case "PlaceOfAccident":
                        resultData = resultData.OrderBy(m => m.PlaceOfAccident);
                        break;
                    case "-PlaceOfAccident":
                        resultData = resultData.OrderByDescending(m => m.PlaceOfAccident);
                        break;
                    case "DateOfAccident":
                        resultData = resultData.OrderBy(m => m.DateOfAccident);
                        break;
                    case "-DateOfAccident":
                        resultData = resultData.OrderByDescending(m => m.DateOfAccident);
                        break;
                    case "DateOfReport":
                        resultData = resultData.OrderBy(m => m.DateOfReport);
                        break;
                    case "-DateOfReport":
                        resultData = resultData.OrderByDescending(m => m.DateOfReport);
                        break;
                }
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
           
                var incidentLst = Mapper.Map<List<Incident>, List<IncidentViewModel>>(returnData);
                foreach (var item in incidentLst)
                {
                    item.AgeOfChild = _icommonService.CalculateAge(item.IncidentParticipant.FirstOrDefault().StudentInfo.DateOfBirth.Value);
                    item.DateOfReport = _icommonService.ConvertFromUTC(item.DateOfReport.Value, model.TimeZone);
                    item.TimeOfAccident= _icommonService.ConvertTimeFromUTC(item.DateOfAccident.Value, item.TimeOfAccident.Value, model.TimeZone);
                }
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = incidentLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }


        /// <summary>
        ///     Adds the incident.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddIncident(IncidentViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var reporterName = _staffRepository.FindBy(m => m.ID == model.StaffInfoId).Select(m => new { m.FirstName, m.LastName }).FirstOrDefault();
                model.ReporterName = reporterName.FirstName + " " + reporterName.LastName;
                model.DateOfReport = DateTime.UtcNow;
                //model.DateOfAccident= _icommonService.ConvertToUTC(new DateTime((model.DateOfAccident.Value.Date).Ticks, DateTimeKind.Unspecified), model.TimeZone);
                model.TimeOfAccident= _icommonService.ConvertToUTC(new DateTime((model.DateOfAccident.Value.Date + model.TimeOfAccident.Value).Ticks, DateTimeKind.Unspecified), model.TimeZone).Value.TimeOfDay;

                foreach (var item in model.ListStudentInfoIds)
                {
                    var name = _tblParticipantInfoRepository.GetAll().Where(x => x.ID == item.studentId).Select(m => new { m.FirstName, m.LastName }).FirstOrDefault();
                    model.ParticipantName = name.FirstName + " " + name.LastName;
                }
                var incidentInfoRespose = AddIncidentInfo(model);
                if (incidentInfoRespose.IsSuccess)
                {
                    model.ListStudentInfoIds.ForEach(e => e.IncidentId = incidentInfoRespose.Id);
                    model.ListOtherStudentInfoIds.ForEach(e => e.IncidentId = incidentInfoRespose.Id);
                    model.ListParentInfoIds.ForEach(e => e.IncidentId = incidentInfoRespose.Id);
                    #region Adding participant and other involved participants Mapping with incident
                    SaveIncidentParticipantsInfo(model.ListStudentInfoIds);
                    SaveIncidentOtherParticipantsInfo(model.ListOtherStudentInfoIds);
                    SaveIncidentParentsInfo(model.ListParentInfoIds);
                    #endregion
                    foreach (var item in model.ListStudentInfoIds)
                    {
                        var name= _tblParticipantInfoRepository.GetAll().Where(x=>x.ID== item.studentId).Select(m=>new { m.FirstName,m.LastName}).FirstOrDefault();
                        model.ParticipantName = name.FirstName + " " + name.LastName; 
                    }
                    foreach (var item in model.ListOtherStudentInfoIds)
                    {
                        var othername = _tblParticipantInfoRepository.GetAll().Where(x => x.ID == item.studentId).Select(m => new { m.FirstName, m.LastName }).FirstOrDefault();
                        if (othername != null) model.OtherParticipants = othername.FirstName + " " + othername.LastName;
                    }

                    model.DescriptionOfInjury = model.DescriptionOfInjury?.Replace("<p>", "") ?? "";

                    model.ActionTaken = model.ActionTaken?.Replace("<p>", "") ?? "";
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("Incident Report Saved Successfully");
                }
                else
                {
                    transaction.ReturnStatus = false;
                    transaction.ReturnMessage.Add("Please Try Again");
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        private ResponseViewModel SaveIncidentParentsInfo(List<IncidentParentParticipantMappingViewModel> listParentInfoIds)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<List<IncidentParentParticipantMappingViewModel>, List<IncidentParentParticipantMapping>>(listParentInfoIds);
            long incidentId = model.FirstOrDefault().IncidentId.Value;
            var existingData = _incidentParentParticipantMappingRepository.All.Where(m => m.IncidentId == incidentId).ToList();
            existingData.ForEach(m => _incidentParentParticipantMappingRepository.Delete(m));
            foreach (var item in model)
            {
                item.IsDeleted = false;
                _incidentParentParticipantMappingRepository.Add(item);
                _unitOfWork.Commit();
                respose.Message = "Incident Parent Participant Information Saved Successfully";
            }

            respose.IsSuccess = true;
            return respose;
        }

        public ResponseViewModel SaveIncidentParticipantsInfo(List<IncidentParticipantViewModel> StudentInfoIds)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<List<IncidentParticipantViewModel>, List<IncidentParticipantMapping>>(StudentInfoIds);
            long incidentId = model.FirstOrDefault().IncidentId.Value;
            var existingData=_incidentParticipantRepository.All.Where(m => m.IncidentId == incidentId).ToList();
            existingData.ForEach(m => _incidentParticipantRepository.Delete(m));
            foreach (var item in model)
            {
                item.IsDeleted = false;                      
                _incidentParticipantRepository.Add(item);
                _unitOfWork.Commit();
                respose.Message = "Incident Participant Information Saved Successfully";
            }

            respose.IsSuccess = true;
            return respose;
        }
        public ResponseViewModel SaveIncidentOtherParticipantsInfo(List<IncidentOtherParticipantViewModel> OtherStudentInfoIds)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<List<IncidentOtherParticipantViewModel>, List<IncidentOtherParticipantMapping>>(OtherStudentInfoIds);
            long IncidentId = 0;
            if (model.Count>0)
                IncidentId= model.FirstOrDefault().IncidentId.Value;
            var existingData = _incidentOtherParticipantRepository.All.Where(m => m.IncidentId == IncidentId).ToList();
            existingData.ForEach(m => _incidentOtherParticipantRepository.Delete(m));
            foreach (var item in model)
            {
                item.IsDeleted = false;
                _incidentOtherParticipantRepository.Add(item);
                _unitOfWork.Commit();
                respose.Message = "Incident Other Participant Information Saved Successfully";
            }
            var responseData = Mapper.Map<List<IncidentOtherParticipantMapping>, List<IncidentOtherParticipantViewModel>>(model);
            respose.IsSuccess = true;
            respose.Content = responseData;
            return respose;
        }
        public ResponseViewModel AddIncidentInfo(IncidentViewModel incidentVm)
        {
            var respose = new ResponseViewModel();
            try
            {
                if (incidentVm.ID > 0)
                {
                    var oldObj = _incidentRepository.GetSingle(incidentVm.ID);
                    var model = Mapper.Map<IncidentViewModel, Incident>(incidentVm);
                    _incidentRepository.Edit(oldObj, model);
                    _unitOfWork.Commit();
                    respose.IsSuccess = true;
                    respose.Message = "Incident Info Updated Successfully";
                    respose.Id = model.ID;
                }
                else
                {
                    var model = Mapper.Map<IncidentViewModel, Incident>(incidentVm);
                    model.DateOfAccident= _icommonService.ConvertToUTC(model.DateOfAccident.Value);
                    model.DateOfReport = DateTime.UtcNow;
                    model.IsDeleted = false;
                    _incidentRepository.Add(model);
                    _unitOfWork.Commit();
                    respose.IsSuccess = true;
                    respose.Message = "Incident Report Saved Successfully";
                    respose.Id = model.ID;
                }
            }
            catch (Exception)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }

        public void UpdateIncidentById(IncidentIdViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {

                var incidentDetails =
                    _incidentRepository.FindBy(m => m.ID == model.IncidentId && m.IsDeleted == false).FirstOrDefault();
                     incidentDetails.IsRead = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Report has been read");
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
        public void DeleteIncidentById(long incidentId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var incidentDetails =
                    _incidentRepository.FindBy(m => m.ID == incidentId && m.IsDeleted == false).FirstOrDefault();
                if (incidentDetails != null && incidentDetails.ID > 0)
                {
                    incidentDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Incident Successfully Deleted");
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}