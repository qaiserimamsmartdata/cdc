﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Data.Repositories;
using CDC.Entities.Staff;
using CDC.ViewModel.Common;
using CDC.Entities.Family;
using CDC.Entities.Attendance;
using CDC.Services.Abstract.Dashboard;
using CDC.ViewModel.Dashboard;
using CDC.Entities.Enums;
using CDC.Services.Abstract;

namespace CDC.Services.Services.Dashboard
{
    public class StaffDashboardService : IStaffDashboardService
    {
        private readonly IEntityBaseRepository<Timesheet> _timesheetRepository;
        private readonly IEntityBaseRepository<LeaveInfo> _leaveInfoRepository;
        private readonly IEntityBaseRepository<Tasks> _tasksRepository;
        private readonly ICommonService _commonService;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familyStudentMapRepository;
        private readonly IEntityBaseRepository<StudentAttendanceInfo> _studentAttendanceInfoRepository;

        public StaffDashboardService(
            IEntityBaseRepository<Timesheet> timesheetRepository,
            IEntityBaseRepository<StudentAttendanceInfo> studentAttendanceInfoRepository,
                     IEntityBaseRepository<LeaveInfo> leaveInfoRepository,
                       ICommonService commonService,
                       IEntityBaseRepository<FamilyStudentMap> familyStudentMapRepository,
                        IEntityBaseRepository<Tasks> tasksRepository)
        {
            _timesheetRepository = timesheetRepository;
            _studentAttendanceInfoRepository = studentAttendanceInfoRepository;
            _familyStudentMapRepository = familyStudentMapRepository;
            _leaveInfoRepository = leaveInfoRepository;
            _commonService = commonService;
            _tasksRepository = tasksRepository;
        }
        public void GetAllCountsById(IDViewModel Model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            StaffDashboardViewModel resultData = new StaffDashboardViewModel();
            try
            {
                var today = DateTime.UtcNow;
                List<Timesheet> timeSheetData = _timesheetRepository.All.Where(m => m.StaffID == Model.StaffId && m.TimeINStamp.Value.Day == today.Day && m.TimeINStamp.Value.Month == today.Month && m.TimeINStamp.Value.Year == today.Year).ToList();
                var leaveData = _leaveInfoRepository.All.Where(m => m.StaffId == Model.StaffId && m.StatusId == 4).ToList();
                resultData.LeaveCount = leaveData.Count();

                resultData.ParticipantCount = _familyStudentMapRepository.All.Where(m => m.Family.AgencyId == Model.AgencyId).Count();

                var attendanceData =
                  _studentAttendanceInfoRepository.FindBy(
                      f =>
                          f.AttendanceDate.Day == today.Date.Day &&
                          f.AttendanceDate.Month == today.Date.Month &&
                          f.AttendanceDate.Year == today.Date.Year && f.AgencyId == Model.AgencyId && f.OnLeave == false);


                resultData.AttendanceCount = attendanceData.Count();
                List<StaffDashboardTimeSheetViewModel> objData = new List<StaffDashboardTimeSheetViewModel>();
                var timeSheetDataFirst = timeSheetData.Where(w => w.CheckInOutPurpose == 0).OrderBy(w => w.TimeINStamp).FirstOrDefault();
                var timeSheetDataLast = timeSheetData.Where(w => w.CheckInOutPurpose == 0).OrderByDescending(w => w.TimeINStamp).FirstOrDefault();
                var daytotalhours = (double)timeSheetData.Where(m=>m.CheckInOutPurpose==0).Sum(m => m.TotalHours);
                if (timeSheetDataFirst != null)
                {
                    DateTime dtTempTimeInStamp = timeSheetDataFirst.TimeINStamp ?? DateTime.UtcNow;
                    resultData.InTime =_commonService.ConvertFromUTC(dtTempTimeInStamp, Model.TimeZone).Value.ToShortTimeString();
                    resultData.StartTime = resultData.InTime;
                }
                else
                {
                    resultData.InTime = null;
                }
                if (timeSheetDataLast != null)
                {
                    resultData.OutTime = timeSheetDataLast.TimeOUTStamp != null?_commonService.ConvertFromUTC(timeSheetDataLast.TimeOUTStamp.Value, Model.TimeZone).Value.ToShortTimeString():null;
                }
                else
                {
                    resultData.OutTime = null;
                }
                if (timeSheetData.Count > 0)
                {
                    var lunchTotalHours = timeSheetData.Where(m => m.CheckInOutPurpose == InOutPurpose.Lunch).Sum(m => m.TotalHours);
                    var breakTotalHours = timeSheetData.Where(m => m.CheckInOutPurpose == InOutPurpose.Break).Sum(m => m.TotalHours);
                    ////Newly added for piecharts
                    resultData.TotalLunch = timeSheetData.Where(m => m.CheckInOutPurpose == InOutPurpose.Lunch).Count();
                    resultData.TotalBreaks = timeSheetData.Where(m => m.CheckInOutPurpose == InOutPurpose.Break).Count();
                    //////////////////////////////
                    if (lunchTotalHours > 0)
                    {

                        objData.Add(new StaffDashboardTimeSheetViewModel()
                    {
                        Name = "Lunch",
                        TotalHours = lunchTotalHours
                    });
                    }
                    if (breakTotalHours > 0)
                    {
                        objData.Add(new StaffDashboardTimeSheetViewModel()
                        {
                            Name = "Break",
                            TotalHours = breakTotalHours
                        });
                    }
                }
                daytotalhours=double.Parse(String.Format("{0:N2}", daytotalhours));
                var eventCount = _tasksRepository.GetAll().Where(m=>m.AgencyId==Model.AgencyId&& m.ClassId>0).ToList().Count();
                resultData.TotalWorkingHours = daytotalhours;
                resultData.TimesheetViewModelData = objData;
                resultData.EventCount = eventCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = resultData;
            }
            catch (Exception)
            {
                responseInfo.IsSuccess = false;
            }

        }

    }
}