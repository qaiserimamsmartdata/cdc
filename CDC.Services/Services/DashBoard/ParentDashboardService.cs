﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Repositories;
using CDC.Entities.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using CDC.Entities.Family;
using CDC.Entities.Attendance;
using CDC.Services.Abstract.Dashboard;
using CDC.ViewModel.Dashboard;
using CDC.Entities.Schedule;
using CDC.ViewModel.Attendance;
using CDC.Services.Abstract;
using CDC.Entities.NewParent;

namespace CDC.Services.Services.Dashboard
{
    public class ParentDashboardService : IParentDashboardService
    {
        private readonly IEntityBaseRepository<ParentInfo> _parentRepository;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familyStudentMapRepository;
        private readonly IEntityBaseRepository<Tasks> _tasksRepository;
        private readonly IEntityBaseRepository<StudentAttendanceInfo> _studentAttendanceInfoRepository;
        private readonly IEntityBaseRepository<Entities.DailyStatus.DailyStatus> _dailyStatusRepository;
        private readonly ICommonService _commonService;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IEntityBaseRepository<tblParentParticipantMapping> _tblParentParticipantMappingRepository;
        public ParentDashboardService(IEntityBaseRepository<Tasks> tasksRepository,
            IEntityBaseRepository<ParentInfo> parentRepository,
            IEntityBaseRepository<FamilyStudentMap> familyStudentMapRepository,
            IEntityBaseRepository<StudentAttendanceInfo> studentAttendanceInfoRepository,
            IEntityBaseRepository<Entities.DailyStatus.DailyStatus> dailyStatusRepository,
            IEntityBaseRepository<tblParentParticipantMapping> tblParentParticipantMappingRepository,
               ICommonService commonService,
             IEntityBaseRepository<StudentSchedule>  studentScheduleRepository)
        {
            _parentRepository = parentRepository;
            _tasksRepository=tasksRepository;
            _studentAttendanceInfoRepository = studentAttendanceInfoRepository;
            _familyStudentMapRepository = familyStudentMapRepository;
            _commonService = commonService;
            _dailyStatusRepository = dailyStatusRepository;
            _studentScheduleRepository = studentScheduleRepository;
            _tblParentParticipantMappingRepository = tblParentParticipantMappingRepository;

        }
        public void GetAllCountsById(IDViewModel Model,out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            ParentDashboardViewModel resultData = new ParentDashboardViewModel();
            try
            {
                var today = DateTime.UtcNow;
               var participantIds  = _tblParentParticipantMappingRepository.FindBy(m => m.NewParentInfoID == Model.FamilyId).Select(k=>k.NewParticipantInfoID);
                resultData.ParticipantCount = participantIds.Count();
                var returnData = _tasksRepository.GetAll().Where(m => m.AgencyId == Model.AgencyId && m.StaffId == 0).ToList();
                if (returnData.Any())
                {
                    var lst = Mapper.Map<List<Tasks>, List<TasksViewModelCopy>>(returnData);
                    List<StudentSchedule> studentinfolist = new List<StudentSchedule>();
                    studentinfolist.AddRange(_studentScheduleRepository.FindBy(m => participantIds.Contains(m.StudentId)));
                    var results = studentinfolist.GroupBy(n => new { n.ClassId })
                 .Select(g => new
                 {
                     g.Key.ClassId
                 }).ToList();


                    //List<TasksViewModelCopy> listcopy = new List<TasksViewModelCopy>();
                    //results.ForEach(
                    //    x => listcopy.AddRange(lst.Where(m => m.ClassId == x.ClassId).ToList())
                    //    );
                    //lst = listcopy;


                    resultData.EventCount = results.Count;
                }
               
                var result = _dailyStatusRepository.GetAll().Where(x => participantIds.Contains(x.StudentInfoId.Value) && (x.Date.Value.Day >= today.Day) );
                 resultData.DailyStatusCount = result.GroupBy(m => m.StudentInfoId).ToList().Count();
               
                var studentAttendanceData = _studentAttendanceInfoRepository.All.Where(m => participantIds.Contains(m.StudentId.Value) && m.AttendanceDate.Day == today.Day && m.AttendanceDate.Month == today.Month && m.AttendanceDate.Year == today.Year);
                resultData.AttendanceCount = studentAttendanceData.GroupBy(m => m.StudentId).Count();
                var attendancelistObj = Mapper.Map<List<StudentAttendanceInfo>, List<StudentAttendanceViewModel>>(studentAttendanceData.ToList());

                foreach (var item in attendancelistObj)
                {
                    if (item.AttendanceDate != null)
                    {
                        item.AttendanceDate = (DateTime)_commonService.ConvertFromUTC(Convert.ToDateTime(item.AttendanceDate), Model.TimeZone);
                    }
                    if (item.InTime != null)
                    {
                        item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.InTime.ToString()), item.InTime.Value, Model.TimeZone);
                    }
                    if (item.OutTime != null)
                    {
                        item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.OutTime.ToString()), item.OutTime.Value, Model.TimeZone);
                    }
                    item.DropByRelationName = item.DropedBy?.RelationName;
                    item.PickedByRelationName = item.PickupBy?.RelationName;
                }
                resultData.attendanceList = attendancelistObj;
                responseInfo.IsSuccess = true;
                responseInfo.Content = resultData;
            }
            catch (Exception)
            {
                responseInfo.IsSuccess = false;
            }
            
        }
    }
}