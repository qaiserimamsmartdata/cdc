﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Repositories;
using CDC.ViewModel.Common;
using CDC.Services.Abstract.Dashboard;
using CDC.ViewModel.Dashboard;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Pricing;
using CDC.Entities.TimeClockUsers;
using CDC.ViewModel.AgencyRegistration;

namespace CDC.Services.Services.Dashboard
{
    public class SuperAdminDashboardService : ISuperAdminDashboardService
    {
        private readonly IEntityBaseRepository<AgencyRegistrationInfo> _agencyRegistrationRepository;
        private readonly IEntityBaseRepository<PricingPlan> _pricingPlanRepository;
        private readonly IEntityBaseRepository<TimeClockUsersPlan> _timeClockUsersPlanRepository;

        public SuperAdminDashboardService(
                     IEntityBaseRepository<AgencyRegistrationInfo> agencyRegistrationRepository,
                     IEntityBaseRepository<PricingPlan> pricingPlanRepository,
                     IEntityBaseRepository<TimeClockUsersPlan> timeClockUsersPlanRepository)
        {
            _agencyRegistrationRepository = agencyRegistrationRepository;
            _pricingPlanRepository = pricingPlanRepository;
            _timeClockUsersPlanRepository = timeClockUsersPlanRepository;
        }
        /// <summary>
        /// Gets all counts by identifier.
        /// </summary>
        /// <param name="Model">The model.</param>
        /// <param name="responseInfo">The response information.</param>
        public void GetAllCountsById(IDViewModel Model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            SuperAdminDashboardViewModel resultData = new SuperAdminDashboardViewModel();
            try
            {
                var agencyRegistrationData = _agencyRegistrationRepository.All.ToList();
                var agencyRegistrationVmData = Mapper.Map<List<AgencyRegistrationInfo>, List<AgencyRegistrationViewModel>>(agencyRegistrationData);
                resultData.PricingPlanAmountSum=Convert.ToDouble(agencyRegistrationVmData.Sum(m => m.PricingPlan.Price));
                
                resultData.TimeClockUsersPlanAmountSum= Convert.ToDouble(agencyRegistrationVmData.Sum(m => m.TimeClockUsersPlan==null?0: m.TimeClockUsersPlan.Price));
                resultData.PricingPlanCount = _pricingPlanRepository.All.ToList().Count();
                resultData.TimeClockUserPlanCount = _timeClockUsersPlanRepository.All.ToList().Count();
                resultData.TotalAgencyCount = agencyRegistrationVmData.Count();

                resultData.AgencyPricingPlansSum= agencyRegistrationVmData.Where(m=>m.PricingPlanId>0).ToList().Count();
                resultData.AgencyTimeClockUsersPlansSum = agencyRegistrationVmData.Where(m => m.TimeClockUsersPlanId > 0).ToList().Count();


                resultData.AgencyList = agencyRegistrationVmData;
                responseInfo.IsSuccess = true;
                responseInfo.Content = resultData;
            }
            catch (Exception)
            {
                responseInfo.IsSuccess = false;
            }

        }
    }
}