﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Staff;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.Entities.Family;
using CDC.Entities.Attendance;
using CDC.Services.Abstract.Dashboard;
using CDC.ViewModel.Dashboard;
using CDC.Entities.Class;
using CDC.ViewModel.Class;
using CDC.Entities.Schedule;
using CDC.ViewModel.Attendance;
using CDC.Entities.ToDoTask;
using CDC.ViewModel.ToDoTask;
using CDC.Entities.Tools.Enhancements;
using CDC.ViewModel.Tools.Enhancements;
using CDC.Entities.AgencyRegistration;
using System.Web;
using System.Configuration;
using CDC.Services.Abstract;
using CDC.ViewModel.AgencyRegistration;
using CDC.Services.Abstract.RazorEngine;
using CDC.Entities.NewParticipant;

namespace CDC.Services.Services.Dashboard
{
    public class DashboardService : IDashboardService
    {
        private readonly IEntityBaseRepository<FamilyInfo> _familyRepository;
        
        private readonly IEntityBaseRepository<StaffInfo> _staffRepository;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familyStudentMapRepository;

        private readonly IEntityBaseRepository<StudentAttendanceInfo> _studentAttendanceInfoRepository;
        private readonly IEntityBaseRepository<ClassInfo> _classRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IEntityBaseRepository<AttendanceInfo> _staffAttendanceRepository;
        private readonly IEntityBaseRepository<StudentDetail> _studentDetailRepository;
        private readonly IEntityBaseRepository<Enhancement> _enhancementRepository;
        private readonly IEntityBaseRepository<ToDo> _toDoRepository;
        private readonly IEntityBaseRepository<AgencyRegistrationInfo> _agencyRegistrationRepository;
        private readonly IEntityBaseRepository<tblParticipantInfo> _tblParticipantInfoRepository;
        private readonly IEntityBaseRepository<Tasks> _tasksRepository;


        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommonService _icommonservice;
        private readonly IRazorService _iRazorService;
        public DashboardService(
            IEntityBaseRepository<FamilyInfo> familyRepository,
            IEntityBaseRepository<StaffInfo> staffRepository,
            IEntityBaseRepository<StudentAttendanceInfo> studentAttendanceInfoRepository,
            IEntityBaseRepository<FamilyStudentMap> familyStudentMapRepository,
            IEntityBaseRepository<ClassInfo>  classRepository,
            IEntityBaseRepository<StudentSchedule>  studentScheduleRepository,
            IEntityBaseRepository<AttendanceInfo>  staffAttendanceRepository,
                        IEntityBaseRepository<StudentDetail> studentDetailRepository,
                        IEntityBaseRepository<Enhancement> enhancementRepository,
                        IEntityBaseRepository<ToDo> toDoRepository,
                        IEntityBaseRepository<AgencyRegistrationInfo> agencyRegistrationRepository,
                        IEntityBaseRepository<tblParticipantInfo> tblParticipantInfoRepository,
                        ICommonService icommonservice,
                        IRazorService iRazorService,
                        IEntityBaseRepository<Tasks> tasksRepository,

            IUnitOfWork unitOfWork)
        {
            _staffRepository = staffRepository;
            _familyRepository = familyRepository;
            _studentAttendanceInfoRepository = studentAttendanceInfoRepository;
            _familyStudentMapRepository = familyStudentMapRepository;
            _classRepository = classRepository;
            _unitOfWork = unitOfWork;
            _studentScheduleRepository = studentScheduleRepository;
            _staffAttendanceRepository = staffAttendanceRepository;
            _studentDetailRepository = studentDetailRepository;
            _enhancementRepository = enhancementRepository;
            _toDoRepository = toDoRepository;
            _agencyRegistrationRepository = agencyRegistrationRepository;
            _icommonservice = icommonservice;
            _iRazorService = iRazorService;
            _tblParticipantInfoRepository = tblParticipantInfoRepository;
            _tasksRepository = tasksRepository;


        }
        public void GetAllCountsById(IDViewModel Model,out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            Uri currentUrl = HttpContext.Current.Request.Url;
            var Url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            DashboardViewModel ResultData = new DashboardViewModel();
            try
            {
                var today = DateTime.UtcNow;
                #region Attendancedata
                var attendanceData =
                  _studentAttendanceInfoRepository.FindBy(
                      f =>
                          f.AttendanceDate.Day == today.Date.Day &&
                          f.AttendanceDate.Month == today.Date.Month &&
                          f.AttendanceDate.Year == today.Date.Year && f.AgencyId == Model.AgencyId && f.OnLeave == false);
                ResultData.AttendanceCount = attendanceData.Count();
                #endregion
                #region Staffdata
                var staffData = _staffRepository.All.Where(m => m.AgencyRegistrationId == Model.AgencyId).ToList();
                ResultData.ParticipantCount = _tblParticipantInfoRepository.FindBy(m => m.AgencyId == Model.AgencyId).Count();
                ResultData.StaffCount = staffData.Count();
                ResultData.TimeClockStaffCount = staffData.Where(m => m.IsTimeClockUser == true).ToList().Count();
                #endregion
                #region Classdata
                var classList = _classRepository.All.Where(m => m.AgencyId == Model.AgencyId).ToList();
                var mappedClassLst = Mapper.Map<List<ClassInfo>, List<ClassViewModel>>(classList);
                foreach (var item in mappedClassLst)
                {
                    item.EnrolledParticipants = _studentScheduleRepository.FindBy(f => f.ClassId == item.ID && f.IsEnrolled).Distinct().ToList().Count;
                }
                ResultData.ClassList = mappedClassLst;
                #endregion

                var studentIds = _tblParticipantInfoRepository.FindBy(w => w.AgencyId == Model.AgencyId)
                    .Select(m => m.ID)
                    .ToList();
                var resultData = _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value) && f.IsEnrolled).ToList();
                ResultData.TotalEnrolledParticipants = resultData.Count;

                #region StaffAttedanceData
                var staffAttendanceData = _staffAttendanceRepository.All.Where(w => w.Staff.AgencyRegistrationId == Model.AgencyId && w.AttendanceDate.Value.Day == DateTime.UtcNow.Day && w.AttendanceDate.Value.Month == DateTime.UtcNow.Month && w.AttendanceDate.Value.Year == DateTime.UtcNow.Year).ToList();
                var stafflistObj = Mapper.Map<List<AttendanceInfo>, List<AttendanceViewModel>>(staffAttendanceData);
                ResultData.AttendanceList = stafflistObj;
                #endregion
                #region EnrollUnenrollData
                var mappedstudent = _studentScheduleRepository.FindBy(f => f.ClassInfo.AgencyId == Model.AgencyId && f.IsEnrolled).Select(s => s.StudentId).ToList();
                var resultData1 = _tblParticipantInfoRepository.FindBy(w => w.AgencyId == Model.AgencyId && !mappedstudent.Contains(w.ID)).Select(s => s.ID).ToList();
                ResultData.UnAssignedEnrollParticipantCount = resultData1.Count;
                ResultData.FutureEnrollParticipantCount = resultData.GroupBy(x => x.StudentId).Where(x => x.Any(z => z.StartDate > today)).Count();
                ResultData.CurrentEnrollParticipantCount = resultData.GroupBy(x => x.StudentId).Where(x => x.Any(z => z.StartDate <= today)).Count();
                #endregion
                #region Tododata
                var toDoData = _toDoRepository.GetAll().Where(w => w.AgencyId == Model.AgencyId && w.IsDeleted == false).ToList();
                var toDoObj = Mapper.Map<List<ToDo>, List<ToDoViewModel>>(toDoData);
                ResultData.ToDoList = toDoObj;
                #endregion
                #region Enhancementdata
                var enhancementData = _enhancementRepository.GetAll().Where(w => w.IsDeleted == false).OrderByDescending(w => w.EnhancementDate).ToList();
                var enhancementObj = Mapper.Map<List<Enhancement>, List<EnhancementViewModel>>(enhancementData);
                ResultData.EnhancementList = enhancementObj;
                ResultData.EnhancementCount = enhancementObj.Count();
                #endregion




                //For Trial Check
                var trialdata= _agencyRegistrationRepository.FindBy(m => m.ID == Model.AgencyId).FirstOrDefault();
                string MailSubject1 = AppConstants.TrialPlanExpiry;
                var trialMapObj = Mapper.Map<AgencyRegistrationInfo, AgencyRegistrationViewModel>(trialdata);

                if (trialMapObj != null)
                {
                    ResultData.TimeClockUserPlanMaxCount = trialMapObj.TimeClockUsersPlan != null ? trialMapObj.TimeClockUsersPlan.MaxNumberOfTimeClockUsers : 0;
                    ResultData.PricingPlanTimeClockMaxCount = trialMapObj.PricingPlan != null ? trialMapObj.PricingPlan.TimeClockUsers : 0;
                }
                #region Eventdata
                var eventCount = _tasksRepository.FindBy(m => m.AgencyId == Model.AgencyId && m.StaffId == 0).ToList().Count;
                ResultData.EventCount = eventCount;
                #endregion
                int trialDays;
                responseInfo.IsTrial = trialMapObj.IsTrial;
                responseInfo.TrialStart = trialMapObj.TrialStart;
                responseInfo.TrialEnd = trialMapObj.TrialEnd;
                
                responseInfo.IsSuccess = true;
                responseInfo.Content = ResultData;

                if (trialdata.IsTrial == null || !trialdata.IsTrial.Value)
                {
                    responseInfo.trialDays = 0;
                    return;
                }
                trialDays =(int)Math.Ceiling((trialdata.TrialEnd - DateTime.UtcNow).Value.TotalDays);
                responseInfo.trialDays = trialDays;
                if (trialDays == 1)
                {
                    if (trialdata.IsTrialMailSent == null || !trialdata.IsTrialMailSent.Value)
                    {
                        var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.TrialPlanExpires, _icommonservice.GetMd5Hash(CDCEmailTemplates.TrialPlanExpires), new
                        {
                            Url = Url,
                            name = trialdata.AgencyName.ToString(),
                            Data = "Your Trial plan will be expired tomorrow, Please update your plan for uninterrupted service else your account will be locked.",
                            TopLogo = Url + ConfigurationManager.AppSettings["TopLogo"],
                            Logo = Url + ConfigurationManager.AppSettings["Logo"]
                        });
                        var mailId = trialdata.AgencyBillingInfos.FirstOrDefault().BillingEmail.ToString();
                        _icommonservice.SendEmail(mailId, MailSubject1, result);
                        trialdata.IsTrialMailSent = true;
                        _agencyRegistrationRepository.Edit(trialdata, trialdata);
                        _unitOfWork.Commit();
                    }
                }
                

            }
            catch (Exception)
            {
                responseInfo.IsSuccess = false;
            }
            
        }
    }
}