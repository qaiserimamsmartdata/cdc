﻿using CDC.Services.Abstract.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using CDC.ViewModel.Common;
using CDC.Data.Repositories;
using CDC.Entities.Staff;
using CDC.Data.Infrastructure;
using AutoMapper;
using CDC.ViewModel.Staff;
using System.Globalization;
using LinqKit;

namespace CDC.Services.Services.Staff
{
    public class EventService : IEventService
    {

        private readonly IEntityBaseRepository<EventInfo> _eventRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EventService(
            IEntityBaseRepository<EventInfo> eventRepository,
            IUnitOfWork unitOfWork)
        {
            _eventRepository = eventRepository;
            _unitOfWork = unitOfWork;
        }


        public void GetAllEvent(SearchEventViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<EventInfo>();
                if (model.AgencyId > 0)
                    predicate.And(p => p.AgencyId == model.AgencyId);
                var resultData = _eventRepository.GetAll().ToList();
                long totalCount = resultData.Count();

                var lst = Mapper.Map<List<EventInfo>, List<EventViewModel>>(resultData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = lst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        public void AddEvent(EventViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var eventInfo = Mapper.Map<EventViewModel, EventInfo>(model);

                eventInfo.StartDate = new DateTime(eventInfo.StartDate.Value.Year, eventInfo.StartDate.Value.Month, eventInfo.StartDate.Value.Day,
                eventInfo.StartTime.Value.Hours, eventInfo.StartTime.Value.Minutes, eventInfo.StartTime.Value.Seconds);
                eventInfo.EndDate = new DateTime(eventInfo.EndDate.Value.Year, eventInfo.EndDate.Value.Month, eventInfo.EndDate.Value.Day,
                   eventInfo.EndTime.Value.Hours, eventInfo.EndTime.Value.Minutes, eventInfo.EndTime.Value.Seconds);

                _eventRepository.Add(eventInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Schedule added successfully");
                transaction.ID = eventInfo.ID;
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        public void DeleteEventById(long eventId, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var eventDetails =
                    _eventRepository.FindBy(m => m.ID == eventId && m.IsDeleted == false)
                        .FirstOrDefault();
                if (eventDetails != null && eventDetails.ID > 0)
                {
                    eventDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("Schedule successfully deleted at " +
                                                  DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

      

        public void UpdateEvent(EventViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var staffScheduleLst =
                    _eventRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapInfo = Mapper.Map<EventViewModel, EventInfo>(model);

                mapInfo.StartDate = new DateTime(mapInfo.StartDate.Value.Year, mapInfo.StartDate.Value.Month, mapInfo.StartDate.Value.Day,
                 mapInfo.StartTime.Value.Hours, mapInfo.StartTime.Value.Minutes, mapInfo.StartTime.Value.Seconds);
                mapInfo.EndDate = new DateTime(mapInfo.EndDate.Value.Year, mapInfo.EndDate.Value.Month, mapInfo.EndDate.Value.Day,
                   mapInfo.EndTime.Value.Hours, mapInfo.EndTime.Value.Minutes, mapInfo.EndTime.Value.Seconds);

                _eventRepository.Edit(staffScheduleLst, mapInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Schedule successfully updated at " +
                                              DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(ex.Message);
            }
        }
    }
}
