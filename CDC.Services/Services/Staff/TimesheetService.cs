﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Attendance;
using CDC.Entities.Enums;
using CDC.Entities.Staff;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CDC.Services.Services.Staff
{
    public class TimesheetService : ITimeSheetService
    {
        private readonly IEntityBaseRepository<Timesheet> _timesheetRepository;
        private readonly IEntityBaseRepository<AttendanceInfo> _attendanceRepository;
        private readonly ICommonService _commonService;
        private readonly IUnitOfWork _unitOfWork;


        public TimesheetService(
           IEntityBaseRepository<Timesheet> timesheetRepository,
           ICommonService commonService,
           IEntityBaseRepository<AttendanceInfo> attendanceRepository,
           IUnitOfWork unitOfWork)
        {
            _timesheetRepository = timesheetRepository;
            _attendanceRepository = attendanceRepository;
            _commonService = commonService;
            _unitOfWork = unitOfWork;
        }

        private int _defaultPageSize = 10;
        public int DefaultPageSize { set { _defaultPageSize = value; } }

        public TimesheetViewModel GetTimesheetsByStaffId(SearchTimesheetViewModel model)
        {
            try
            {
                //  Return object.

                //  Collect all timesheet check in/oy data.
                List<Timesheet> dbDataCollection = _timesheetRepository.GetAll()
                .Where(x => (x.StaffID == model.StaffId))
                .OrderBy(x => x.StaffID).ToList().OrderByDescending(x => x.ID).ToList();
                //var abc = _TimesheetRepository.GetAll().ToList();
                if (!dbDataCollection.Any())
                {
                    return null;
                }

                //  Collect the check day in/out data.
                List<Timesheet> dayDataCollection = dbDataCollection.Where(x => x.CheckInOutPurpose == InOutPurpose.Day).ToList();

                var dataCollection = GetTimesheetDataWithoutTimeZone(dbDataCollection, dayDataCollection);

                var returnObject = new TimesheetViewModel()
                {
                    TimeSheets = dataCollection,
                    TotalRecords = dayDataCollection.Count()
                };

                //  Return final data.
                return returnObject;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public TimesheetViewModel GetTimesheetsByStaffIdAndTimeZone(SearchTimesheetViewModel model)
        {
            try
            {
                //  Return object.

                //  Collect all timesheet check in/oy data.
                List<Timesheet> dbDataCollection = _timesheetRepository.GetAll()
                    .Where(x => (model.StaffId == null || x.StaffID == model.StaffId))
                    .OrderBy(x => x.StaffID).ToList().OrderByDescending(x => x.ID).ToList();

                if (!dbDataCollection.Any())
                {
                    return null;
                }

                //  Collect the check day in/out data.
                List<Timesheet> dayDataCollection = dbDataCollection.Where(x => x.CheckInOutPurpose == InOutPurpose.Day).ToList();

                var dataCollection = GetTimesheetDataWithTimeZone(dbDataCollection, dayDataCollection, model.TimeZone);

                var returnObject = new TimesheetViewModel()
                {
                    TimeSheets = dataCollection,
                    TotalRecords = dayDataCollection.Count()
                };

                //  Return final data.
                return returnObject;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public TimesheetViewModel GetTimesheetsWithoutTimeZone(SearchTimesheetViewModel model)
        {
            model.page = model.page ?? 1;
            model.limit = model.limit ?? _defaultPageSize;

            //  Collect all timesheet check in/oy data.
            List<Timesheet> dbDataCollection = _timesheetRepository.GetAll()
                    .Where(x => (model.StaffId == null || x.StaffID == model.StaffId))
                    .OrderBy(x => x.StaffID).ToList().OrderByDescending(x => x.ID).ToList();

            if (dbDataCollection.Count == 0)
            {
                return null;
            }

            try
            {
                //  Return object
                List<Timesheet> filterDbCollection;
                //  Collect the day check in/out data.
                var dayCheckInOutDataCollection = dbDataCollection.Where(x => x.CheckInOutPurpose == InOutPurpose.Day);

                //  Check condition : If From date provided by to date is null.
                if (model.FromDate.HasValue && !model.ToDate.HasValue)
                {
                    filterDbCollection = dayCheckInOutDataCollection.Where(x => x.TimeIN.Value.Date >= model.FromDate.Value.Date).ToList();
                }
                else if (!model.FromDate.HasValue && model.ToDate.HasValue)
                {
                    //  Check condition : If From date is NULL and to date is provided.
                    filterDbCollection = dayCheckInOutDataCollection.Where(x => x.TimeIN.Value.Date <= model.ToDate.Value.Date).ToList();
                }
                else if (!model.FromDate.HasValue && !model.ToDate.HasValue)
                {
                    filterDbCollection = dayCheckInOutDataCollection.ToList();
                }
                else   //   Both dates are provided.
                {
                    filterDbCollection = dayCheckInOutDataCollection.Where(x => (x.TimeIN.Value.Date >= model.FromDate.Value.Date && x.TimeIN.Value.Date <= model.ToDate.Value.Date)).ToList();
                }

                //  Collect the paged data.
                var pagedDbDataCollection = filterDbCollection
                    .Skip(((int)model.page - 1) * (int)model.limit).Take((int)model.limit).ToList();


                var dataCollection = GetTimesheetDataWithoutTimeZone(dbDataCollection, pagedDbDataCollection);


                var returnObject = new TimesheetViewModel

                {
                    TimeSheets = dataCollection,
                    TotalRecords = dayCheckInOutDataCollection.Count()
                };

                //  Return the data object.
                return returnObject;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public TimesheetViewModel GetTimesheetWithTimeZone(SearchTimesheetViewModel model)
        {
           //  model.StaffId = 1;

            model.page = model.page ?? 1;
            model.limit = model.limit ?? _defaultPageSize;

            //  Collect all timesheet check in/oy data.
            List<Timesheet> dbDataCollection = _timesheetRepository.GetAll()
                    .Where(x => (model.StaffId == null || x.StaffID == model.StaffId))
                    .OrderBy(x => x.StaffID).ToList().OrderByDescending(x => x.ID).ToList();

            if (dbDataCollection.Count == 0)
            {
                return null;
            }

            try
            {

                List<Timesheet> filterDbCollection;
                //  Collect the day check in/out data.
                var dayCheckInOutDataCollection = dbDataCollection.Where(x => x.CheckInOutPurpose == InOutPurpose.Day);

                //  Check condition : If From date provided by to date is null.
                if (model.FromDate.HasValue && !model.ToDate.HasValue)
                {
                    filterDbCollection = dayCheckInOutDataCollection.Where(x => _commonService.ConvertFromUTC(x.TimeINStamp.Value, model.TimeZone).Value.Date >= model.FromDate.Value.Date).ToList();
                }
                else if (!model.FromDate.HasValue && model.ToDate.HasValue)
                {
                    //  Check condition : If From date is NULL and to date is provided.
                    filterDbCollection = dayCheckInOutDataCollection.Where(x => _commonService.ConvertFromUTC(x.TimeINStamp.Value, model.TimeZone).Value.Date <= model.ToDate.Value.Date).ToList();
                }
                else if (!model.FromDate.HasValue && !model.ToDate.HasValue)
                {
                    filterDbCollection = dayCheckInOutDataCollection.ToList();
                }
                else   //   Both dates are provided.
                {
                    filterDbCollection = dayCheckInOutDataCollection.Where(x => x.TimeINStamp.Value.Date >= model.FromDate.Value.Date
                        && x.TimeINStamp.Value.Date <= model.ToDate.Value.Date).ToList();
                }

                //  Collect the paged data.
                var pagedDbDataCollection = filterDbCollection
                    .Skip(((int)model.page - 1) * (int)model.limit).Take((int)model.limit).ToList();

                var dataCollection = GetTimesheetDataWithTimeZone(dbDataCollection, pagedDbDataCollection, model.TimeZone);

                var returnObject = new TimesheetViewModel
                {
                    TimeSheets = dataCollection,
                    TotalRecords = dayCheckInOutDataCollection.Count()
                };

                //  Return the data object.
                return returnObject;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal List<TimesheetListModel> GetTimesheetDataWithoutTimeZone(List<Timesheet> consolidatedData, List<Timesheet> manipulationData)
        {
            List<TimesheetListModel> dataCollection = null;

            foreach (var dbData in manipulationData)
            {
                if (dataCollection == null)
                {
                    dataCollection = new List<TimesheetListModel>();
                }

                TimesheetListModel data = new TimesheetListModel()
                {
                    StaffID = dbData.StaffID,
                    Day = dbData.TimeIN.Value.DayOfWeek.ToString(),
                    DayCheckIn = dbData.TimeIN.Value,
                    DayCheckOut = dbData.TimeOUT,
                    TotalHours = dbData.TotalHours.Value
                };

                //  Collect the lunch check in/out data.
                var lunchCheckInOutData = consolidatedData.Where(x => (x.CheckInOutPurpose == InOutPurpose.Lunch
                    && x.StaffID == dbData.StaffID
                    && (x.TimeOUT.Value >= dbData.TimeIN.Value && x.TimeOUT.Value <= (dbData.TimeOUT == null ? DateTime.Now : dbData.TimeOUT.Value))
                )).FirstOrDefault();

                if (lunchCheckInOutData != null)
                {
                    data.LunchCheckIn = lunchCheckInOutData.TimeIN.Value;
                    data.LunchCheckOut = lunchCheckInOutData.TimeOUT.Value;
                }

                //  Collect the Break check in/out data.
                var breakCheckInOutData = consolidatedData.Where(x => (x.CheckInOutPurpose == InOutPurpose.Break
                    && x.StaffID == dbData.StaffID
                    && (x.TimeOUT.Value >= dbData.TimeIN.Value && x.TimeOUT.Value <= (dbData.TimeOUT == null ? DateTime.Now : dbData.TimeOUT.Value))
                ));

                if (breakCheckInOutData.Count() != 0)
                {
                    List<GeneralBreak> breakInfo = new List<GeneralBreak>();

                    foreach (var breakData in breakCheckInOutData
                        .Select((x, n) => new { ExpressionData = x, Index = n + 1 }))
                    {
                        breakInfo.Add(new GeneralBreak()
                        {
                            CheckOut = breakData.ExpressionData.TimeOUT,
                            CheckIn = breakData.ExpressionData.TimeIN
                        });
                        data.BreakChecks = breakInfo;
                    }
                    dataCollection.Add(data);
                }
                else
                {
                    dataCollection.Add(data);
                }
            }

            return dataCollection;
        }

        internal List<TimesheetListModel> GetTimesheetDataWithTimeZone(List<Timesheet> consolidatedData, List<Timesheet> manipulationData, string timeZone)
        {
            List<TimesheetListModel> dataCollection = null;

            foreach (var dbData in manipulationData)
            {
                if (dataCollection == null)
                {
                    dataCollection = new List<TimesheetListModel>();
                }

                TimesheetListModel data = new TimesheetListModel()
                {
                    StaffID = dbData.StaffID,
                    Day = _commonService.ConvertFromUTC(dbData.TimeINStamp.Value, timeZone).Value.DayOfWeek.ToString(),
                    DayCheckIn = _commonService.ConvertFromUTC(dbData.TimeINStamp.Value, timeZone),
                    DayCheckOut = dbData.TimeOUTStamp != null ? _commonService.ConvertFromUTC(dbData.TimeOUTStamp.Value, timeZone) : null,
                    TotalHours = dbData.TotalHours.Value
                };

                //  Collect the lunch check in/out data.
                var lunchCheckInOutData = consolidatedData.Where(x => (x.CheckInOutPurpose == InOutPurpose.Lunch
                    && x.StaffID == dbData.StaffID
                    && (_commonService.ConvertFromUTC(x.TimeOUTStamp.Value, timeZone) >= _commonService.ConvertFromUTC(dbData.TimeINStamp.Value, timeZone)
                        && _commonService.ConvertFromUTC(x.TimeOUTStamp.Value, timeZone) <= (dbData.TimeOUTStamp == null ? _commonService.ConvertFromUTC(DateTime.UtcNow, timeZone) : _commonService.ConvertFromUTC(dbData.TimeOUTStamp.Value, timeZone)))
                )).FirstOrDefault();

                if (lunchCheckInOutData != null)
                {
                    data.LunchCheckIn = lunchCheckInOutData.TimeINStamp != null ? _commonService.ConvertFromUTC(lunchCheckInOutData.TimeINStamp.Value, timeZone) : null;
                    data.LunchCheckOut = lunchCheckInOutData.TimeOUTStamp != null ? _commonService.ConvertFromUTC(lunchCheckInOutData.TimeOUTStamp.Value, timeZone) : null;
                }

                //  Collect the Break check in/out data.
                var breakCheckInOutData = consolidatedData.Where(x => (x.CheckInOutPurpose == InOutPurpose.Break
                    && x.StaffID == dbData.StaffID
                    && (_commonService.ConvertFromUTC(x.TimeOUTStamp.Value, timeZone) >= _commonService.ConvertFromUTC(dbData.TimeINStamp.Value, timeZone)
                        && _commonService.ConvertFromUTC(x.TimeOUTStamp.Value, timeZone) <= (dbData.TimeOUTStamp == null ? _commonService.ConvertFromUTC(DateTime.UtcNow, timeZone) : _commonService.ConvertFromUTC(dbData.TimeOUTStamp.Value, timeZone)))
                ));

                if (breakCheckInOutData.Count() != 0)
                {
                    List<GeneralBreak> breakInfo = new List<GeneralBreak>();

                    foreach (var breakData in breakCheckInOutData
                        .Select((x, n) => new { ExpressionData = x, Index = n + 1 }))
                    {
                        breakInfo.Add(new GeneralBreak()
                        {
                            CheckOut = breakData.ExpressionData.TimeOUTStamp != null ? _commonService.ConvertFromUTC(breakData.ExpressionData.TimeOUTStamp.Value, timeZone) : null,
                            CheckIn = breakData.ExpressionData.TimeINStamp != null ? _commonService.ConvertFromUTC(breakData.ExpressionData.TimeINStamp.Value, timeZone) : null
                        });
                        data.BreakChecks = breakInfo;
                    }
                    dataCollection.Add(data);
                }
                else
                {
                    dataCollection.Add(data);
                }
            }

            return dataCollection;
        }

        public TimesheetModel SaveCheckTime(TimeCheckModel data, string IPAddress, string timezone)
        {
            try
            {
                //  Collect the Check In time.
                DateTime? inTime = data.TimeChecksForInOrOut == TimeCheckInOrOut.In ? (DateTime?)data.CheckOnDateTime : null;
                //  Collect the Check out time.
                DateTime? outTime = data.TimeChecksForInOrOut == TimeCheckInOrOut.Out ? (DateTime?)data.CheckOnDateTime : null;
                //  Collect Check In IP Address.
                string inIpAddress = data.TimeChecksForInOrOut == TimeCheckInOrOut.In ? IPAddress : string.Empty;
                //  Collect the Check out IP Address.
                string outIpAddress = data.TimeChecksForInOrOut == TimeCheckInOrOut.Out ? IPAddress : string.Empty;

                Timesheet oldEntity = null;
                 
                //  Timesheet data is call for update.
                if (data.TimesheetID != 0)
                {
                    oldEntity = _timesheetRepository.GetSingle((long)data.TimesheetID);
                    inTime = oldEntity.TimeINStamp.HasValue ? oldEntity.TimeINStamp.Value : inTime;
                    outTime = oldEntity.TimeOUT.HasValue ? oldEntity.TimeOUT.Value : outTime;
                    timezone = oldEntity.Timezone.Equals(timezone) ? timezone : oldEntity.Timezone.Equals(timezone) + " : " + timezone;
                }
                //  *** Timesheet data is call for update.
                DateTime? timeInStampUtc;
                if (data.TimesheetID != 0)
                    timeInStampUtc=inTime;
                else
                timeInStampUtc = inTime != null ? _commonService.ConvertToUTC(inTime.Value) : null;

                DateTime? timeOutStampUtc = outTime;

                double totalHours = outTime.HasValue && inTime.HasValue ?
                    (data.PurposeToCheckInOrOut == InOutPurpose.Day ? (outTime.Value - inTime.Value).TotalHours :
                    (inTime.Value - outTime.Value).TotalHours) : 0;

                totalHours = double.Parse(String.Format("{0:N2}", totalHours));
                //  Collect the data to Save/Update
                var timeCheckData = new Timesheet
                {
                    ID = data.TimesheetID,
                    StaffID = data.StaffID,
                    CheckInOutPurpose = data.PurposeToCheckInOrOut,
                    TimeIN = inTime,
                    TimeINStamp = timeInStampUtc,
                    TimeINComments = inTime != null ? data.Comment : string.Empty,
                    TimeOUT = outTime,
                    TimeOUTStamp = timeOutStampUtc,
                    TimeOUTComments = outTime != null ? data.Comment : string.Empty,
                    INIPAddress = inIpAddress,
                    OUTIPAddress = outIpAddress,
                    TotalHours = totalHours,
                    Timezone = timezone,
                    IsDeleted = false
                };
                //  *** Collect the data to Save/Update

                if (data.TimesheetID == 0)  //  Save as New timesheet.
                {
                    _timesheetRepository.Add(timeCheckData);
                }
                else    //  Update the existing timesheet data.
                {
                    _timesheetRepository.Edit(oldEntity, timeCheckData);
                }
                _unitOfWork.Commit();


                if (data.PurposeToCheckInOrOut == InOutPurpose.Day)
                {
                    TimeSpan? timeIn = null;
                    TimeSpan? timeOut = null;
                    var todaysTimeSheetData = _timesheetRepository.All.Where(m => m.StaffID == data.StaffID && m.CheckInOutPurpose == InOutPurpose.Day);
                    var result = (from d in todaysTimeSheetData
                                  where (d.TimeINStamp.Value.Day == DateTime.UtcNow.Day
                                  && d.TimeINStamp.Value.Month == DateTime.UtcNow.Month
                                  && d.TimeINStamp.Value.Year == DateTime.UtcNow.Year)
                                  select d).ToList();

                    if (result != null)
                    {
                        timeIn = result.OrderBy(m => m.TimeINStamp).FirstOrDefault().TimeINStamp.Value.TimeOfDay;
                        var timeOutData = result.OrderByDescending(m => m.TimeINStamp).FirstOrDefault();
                        timeOut = timeOutData.TimeOUTStamp?.TimeOfDay;
                    }
                    var attendanceInfoOld = _attendanceRepository.FindBy(m => m.StaffId == data.StaffID
                                     && m.AttendanceDate.Value.Day == data.CheckOnDateTime.Day
                                     && m.AttendanceDate.Value.Month == data.CheckOnDateTime.Month
                                     && m.AttendanceDate.Value.Year == data.CheckOnDateTime.Year).FirstOrDefault();
                    if (attendanceInfoOld != null)
                    {
                        attendanceInfoOld.InTime = timeIn;
                        attendanceInfoOld.OutTime = timeOut;
                        attendanceInfoOld.Comment = "";
                        attendanceInfoOld.OnLeave = false;
                        _attendanceRepository.Edit(attendanceInfoOld, attendanceInfoOld);
                        _unitOfWork.Commit();
                    }
                    else
                    {
                        AttendanceViewModel attendanceVm = new AttendanceViewModel
                        {
                            InTime = timeIn,
                            OutTime = timeOut,
                            OnLeave = false,
                            AttendanceDate = DateTime.UtcNow,
                            CreatedDate = DateTime.UtcNow,
                            StaffId = data.StaffID
                        };
                        var attendancData = Mapper.Map<AttendanceViewModel, AttendanceInfo>(attendanceVm);
                        _attendanceRepository.Add(attendancData);
                        _unitOfWork.Commit();
                    }
                }

                var response = Mapper.Map<Timesheet, TimesheetModel>(timeCheckData);
                return response;
            }
            catch (Exception)
            {
                // ignored
            }

            return null;
        }

        public TimeCheckModel GetCurrentTimeCheck(int staffID)
        {
            //staffID = 1;
            TimeCheckModel returnModel;

            var staffCheckDataCollection = _timesheetRepository.GetAll().Where(x => (x.StaffID == staffID)).ToList();
            if (!staffCheckDataCollection.Any())
            {
                return null;
            }
            var staffCheckData = staffCheckDataCollection.LastOrDefault();
            DateTime? inTime = staffCheckData.TimeINStamp.HasValue ? staffCheckData.TimeINStamp : null;
            DateTime? outTime = staffCheckData.TimeOUT.HasValue ? staffCheckData.TimeOUT : null;

            if (staffCheckData.CheckInOutPurpose == InOutPurpose.Day && inTime == null && outTime == null)
            {
                returnModel = new TimeCheckModel
                {
                    TimesheetID = staffCheckData.ID,
                    StaffID = staffCheckData.StaffID,
                    PurposeToCheckInOrOut = staffCheckData.CheckInOutPurpose,
                    CheckOnDateTime = inTime.Value,
                    TimeChecksForInOrOut = TimeCheckInOrOut.In
                };
                return returnModel;
            }
            //DateTime? DayCheckInTime = null;
            if (inTime.HasValue && outTime.HasValue)
            {
                var dayCheckInData = staffCheckDataCollection.Where(x => (x.CheckInOutPurpose == InOutPurpose.Day && (x.TimeIN.Value.Date <= inTime.Value.Date || x.TimeIN.Value <= DateTime.Now) && !x.TimeOUT.HasValue)).FirstOrDefault();
                if (dayCheckInData == null)
                {
                    return null;
                }
                returnModel = new TimeCheckModel()
                {
                    TimesheetID = dayCheckInData.ID,
                    StaffID = dayCheckInData.StaffID,
                    PurposeToCheckInOrOut = dayCheckInData.CheckInOutPurpose,
                    CheckOnDateTime = (outTime != null && dayCheckInData.TimeOUT.HasValue) ? dayCheckInData.TimeOUT.Value : dayCheckInData.TimeIN.Value,
                    TimeChecksForInOrOut = dayCheckInData.TimeOUT.HasValue ? TimeCheckInOrOut.Out : TimeCheckInOrOut.In,
                    Comment = inTime != null ? dayCheckInData.TimeINComments : dayCheckInData.TimeOUTComments
                };
                return returnModel;
            }

            returnModel = new TimeCheckModel()
            {
                TimesheetID = staffCheckData.ID,
                StaffID = staffCheckData.StaffID,
                PurposeToCheckInOrOut = staffCheckData.CheckInOutPurpose,
                CheckOnDateTime = outTime ?? inTime.Value,
                TimeChecksForInOrOut = outTime != null ? TimeCheckInOrOut.Out : TimeCheckInOrOut.In,
                Comment = outTime != null ? staffCheckData.TimeOUTComments : staffCheckData.TimeINComments
            };

            return returnModel;
        }
        public TimesheetModel SaveCheckFirstTimeFromStaffAttendance(TimeCheckModel data, DateTime AttDate, TimeSpan inTime, TimeSpan outTime, string IPAddress, string timezone)
        {
            string outComments = string.Empty;
            string inIpAddress = IPAddress;
            string outIpAddress = IPAddress;
            try
            {
                DateTime dtIn = new DateTime((AttDate.Date + inTime).Ticks, DateTimeKind.Unspecified);
                var inUtcDate = _commonService.ConvertToUTC(dtIn, timezone);
                var inDate = _commonService.ConvertFromUTC(inUtcDate.Value, "India Standard Time");
                var inComments = "Add By Agency";

                DateTime? outUtcDate;
                DateTime? outDate;
                if (outTime.ToString() == "00:00:00")
                {
                    outUtcDate = null;
                    outDate = null;
                }
                else
                {
                    DateTime dtOut = new DateTime((AttDate.Date + outTime).Ticks, DateTimeKind.Unspecified);
                    outUtcDate = _commonService.ConvertToUTC(dtOut, timezone);
                    outDate = _commonService.ConvertFromUTC(outUtcDate.Value, "India Standard Time");
                    outComments = "Add By Agency";
                }

                double totalHours = outUtcDate.HasValue && inUtcDate.HasValue ? (outUtcDate.Value - inUtcDate.Value).TotalHours : 0;
                totalHours = double.Parse(String.Format("{0:N2}", totalHours));

                var timeCheckData = new Timesheet
                {
                    ID = data.TimesheetID,
                    StaffID = data.StaffID,
                    CheckInOutPurpose = data.PurposeToCheckInOrOut,
                    TimeIN = inDate,
                    TimeINStamp = inUtcDate,
                    TimeINComments = inComments,
                    TimeOUT = outDate,
                    TimeOUTStamp = outUtcDate,
                    TimeOUTComments = outComments,
                    INIPAddress = inIpAddress,
                    OUTIPAddress = outIpAddress,
                    TotalHours = totalHours,
                    Timezone = "India Standard Time",
                    IsDeleted = false
                };
                _timesheetRepository.Add(timeCheckData);
                _unitOfWork.Commit();
                var response = Mapper.Map<Timesheet, TimesheetModel>(timeCheckData);
                return response;
            }
            catch (Exception)
            {
                // ignored
            }

            return null;
        }
        public ReadOnlyCollection<TimeZoneInfo> GetTimezones()
        {
            ReadOnlyCollection<TimeZoneInfo> tz;
            tz = TimeZoneInfo.GetSystemTimeZones();
            return tz;
        }
    }
}
