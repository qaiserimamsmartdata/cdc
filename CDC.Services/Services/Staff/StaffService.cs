﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Staff;
using CDC.Entities.User;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using LinqKit;
using CDC.Services.Abstract;

namespace CDC.Services.Services.Staff
{
    public class StaffService : IStaffService
    {
        private readonly IEntityBaseRepository<StaffInfo> _staffRepository;
        private readonly IEntityBaseRepository<StaffLocationInfo> _staffLocationInfoRepository;
        
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEntityBaseRepository<Users> _userRepository;
        private readonly ICommonService _iCommonService;

        public StaffService(
            IEntityBaseRepository<StaffInfo> staffRepository,
            IEntityBaseRepository<Users> userRepository,
            IEntityBaseRepository<StaffLocationInfo>  staffLocationInfoRepository,
            IUnitOfWork unitOfWork,ICommonService iCommonService)
        {
            _staffRepository = staffRepository;

            _userRepository = userRepository;
            _staffLocationInfoRepository = staffLocationInfoRepository;
            _unitOfWork = unitOfWork;
            _iCommonService = iCommonService;
        }

        public StaffViewModel GetStaffById(long staffId)
        {

            var obj = _staffRepository.GetSingle(staffId);
            var loginDetail =
                  _userRepository.FindBy(m => m.ID == obj.UserId && m.IsDeleted == false).FirstOrDefault();
            var staffObj = Mapper.Map<StaffInfo, StaffViewModel>(obj);
            staffObj.Password = loginDetail.Password;
           
            return staffObj;
        }


        /// <summary>
        ///     Gets all staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        public void GetAllStaff(SearchStaffViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<StaffInfo>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyRegistrationId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.StaffName))
                    predicate =
                        predicate.And(p => p.FirstName.Contains(model.StaffName) || p.LastName.Contains(model.StaffName));
                if (model.StatusId > 0)
                    predicate = predicate.And(p => p.StatusId == model.StatusId);
                if (model.PositionId > 0)
                    predicate = predicate.And(p => p.PositionId == model.PositionId);

                var resultData = _staffRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.FirstName);
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.FirstName);
                        break;
                    case "PhoneNumber":
                        resultData = resultData.OrderBy(m => m.PhoneNumber);
                        break;
                    case "-PhoneNumber":
                        resultData = resultData.OrderByDescending(m => m.PhoneNumber);
                        break;
                    case "Email":
                        resultData = resultData.OrderBy(m => m.Email);
                        break;
                    case "-Email":
                        resultData = resultData.OrderByDescending(m => m.Email);
                        break;
                    case "Gender":
                        resultData = resultData.OrderBy(m => m.Gender);
                        break;
                    case "-Gender":
                        resultData = resultData.OrderByDescending(m => m.Gender);
                        break;
                    case "DateHired":
                        resultData = resultData.OrderBy(m => m.DateHired);
                        break;
                    case "-DateHired":
                        resultData = resultData.OrderByDescending(m => m.DateHired);
                        break;
                    case "Designation":
                        resultData = resultData.OrderBy(m => m.Position.Name);
                        break;
                    case "-Designation":
                        resultData = resultData.OrderByDescending(m 
                            => m.Position.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.FirstName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1)*model.limit).Take(model.limit).ToList();     
                var staffLst = Mapper.Map<List<StaffInfo>, List<StaffViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }


        /// <summary>
        ///     Adds the staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddStaff(StaffViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {

                var staffInfo = Mapper.Map<StaffViewModel, StaffInfo>(model);
                   

                //User Login information
                var userLoginInfo = new Users
                {
                    RoleId = model.RoleId,
                    EmailId = model.Email,
                    Password = model.Password,
                    IsDeleted = false,
                    Token =_iCommonService.RandomString(150),
                    TokenExpiryDate = DateTime.UtcNow.AddMonths(1)
            };
                _userRepository.Add(userLoginInfo);
                _unitOfWork.Commit();

                staffInfo.UserId = userLoginInfo.ID;
                _staffRepository.Add(staffInfo);
                _unitOfWork.Commit();

                if(staffInfo.ID>0)
                {
                    model.StaffLocationList.ForEach(e => e.StaffId = staffInfo.ID);
                   ResponseViewModel response= SaveStaffLocationInfo(model.StaffLocationList);
                    if(response.IsSuccess)
                    {
                        transaction.ReturnStatus = response.IsSuccess;
                        transaction.ReturnMessage.Add("Staff added successfully");
                    }
                    else
                    {
                        transaction.ReturnStatus = response.IsSuccess;
                        transaction.ReturnMessage.Add("Unable to add staff successfully");
                    }

                }

                
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        private ResponseViewModel SaveStaffLocationInfo(List<StaffLocationInfoViewModel> staffLocationList)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<List<StaffLocationInfoViewModel>, List<StaffLocationInfo>>(staffLocationList.ToList());
            foreach (var item in model)
            {
                item.IsDeleted = false;
                _staffLocationInfoRepository.Add(item);
                _unitOfWork.Commit();
                respose.Message = "Staff location added successfully";

            }
            var responseData = Mapper.Map<List<StaffLocationInfo>, List<StaffLocationInfoViewModel>>(model);

            respose.IsSuccess = true;
            respose.Content = responseData;
            return respose;
        }
        private ResponseViewModel UpdateStaffLocationInfo(List<StaffLocationInfoViewModel> staffLocationList)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<List<StaffLocationInfoViewModel>, List<StaffLocationInfo>>(staffLocationList.ToList());
            long staffId = model.First().StaffId.Value;
            long agencyId = model.First().AgencyId;
            var newObj = _staffLocationInfoRepository.All.Where(m => m.StaffId == staffId && m.AgencyId == agencyId);
            newObj.ForEach(x => _staffLocationInfoRepository.Delete(x));
            foreach (var item in model)
            {
                    
                item.IsDeleted = false;
                _staffLocationInfoRepository.Add(item);
                _unitOfWork.Commit();
                respose.Message = "Staff location updated successfully";

            }
            var responseData = Mapper.Map<List<StaffLocationInfo>, List<StaffLocationInfoViewModel>>(model);

            respose.IsSuccess = true;
            respose.Content = responseData;
            return respose;
        }
        /// <summary>
        ///     Updates the staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo">The response information.</param>
        public void UpdateStaff(StaffViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var staffDetail =
                    _staffRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapStaffViewModelInfo = Mapper.Map<StaffViewModel, StaffInfo>(model);
                _staffRepository.Edit(staffDetail, mapStaffViewModelInfo);

                //Update user login details
                var loginDetail =
                    _userRepository.FindBy(m => m.ID == model.UserId && m.IsDeleted == false).FirstOrDefault();
                var userLoginInfo = new Users();
                if (loginDetail != null)
                {
                    if ((!string.IsNullOrEmpty(loginDetail.Token) && loginDetail.TokenExpiryDate == DateTime.UtcNow) || string.IsNullOrEmpty(loginDetail.Token))
                    {
                        userLoginInfo.Token = _iCommonService.RandomString(150);
                        userLoginInfo.TokenExpiryDate = DateTime.UtcNow.AddMonths(1);
                    }
                    else
                    {
                        userLoginInfo.Token = loginDetail.Token;
                        userLoginInfo.TokenExpiryDate = loginDetail.TokenExpiryDate;
                    }
                    userLoginInfo.RoleId = loginDetail.RoleId;
                    userLoginInfo.EmailId = model.Email;
                    userLoginInfo.Password = model.Password;
                    userLoginInfo.ID = loginDetail.ID;
                    userLoginInfo.IsDeleted = false;
                    _userRepository.Edit(loginDetail, userLoginInfo);
                }
                _unitOfWork.Commit();

                if (model.ID > 0)
                {
                    model.StaffLocationList.ForEach(e => e.StaffId = model.ID);
                    ResponseViewModel response = UpdateStaffLocationInfo(model.StaffLocationList);
                    if (response.IsSuccess)
                    {
                        responseInfo.ReturnStatus = response.IsSuccess;
                        responseInfo.ReturnMessage.Add("Staff updated successfully");
                    }
                    else
                    {
                        responseInfo.ReturnStatus = response.IsSuccess;
                        responseInfo.ReturnMessage.Add("Unable to update staff successfully");
                    }

                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }


        /// <summary>
        ///     Deletes the staff by identifier.
        /// </summary>
        /// <param name="staffId">The staff identifier.</param>
        /// <param name="responseInfo">The response information.</param>
        public void DeleteStaffById(long staffId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var staffDetails =
                    _staffRepository.FindBy(m => m.ID == staffId && m.IsDeleted == false).FirstOrDefault();
                if (staffDetails != null && staffDetails.ID > 0)
                {
                    staffDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Staff successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}