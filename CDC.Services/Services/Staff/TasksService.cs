﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Class;
using CDC.Entities.Family;
using CDC.Entities.NewParent;
using CDC.Entities.Schedule;
using CDC.Entities.Staff;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using LinqKit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CDC.Services.Services.Staff
{
    public class TasksService : ITasksService
    {

        private readonly IEntityBaseRepository<Tasks> _tasksRepository;
        private readonly IEntityBaseRepository<StaffInfo> _staffsRepository;
        private readonly IEntityBaseRepository<ClassInfo> _classRepository;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familyStudentMapRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IEntityBaseRepository<TasksClassMapping> _tasksClassMappinRepository;
        private readonly IEntityBaseRepository<tblParentParticipantMapping> _tblParentParticipantMappingRepository;
        private readonly ICommonService _commonService;
        private readonly IUnitOfWork _unitOfWork;
        
        public TasksService(
            IEntityBaseRepository<Tasks> tasksRepository,
            IEntityBaseRepository<StaffInfo> staffsRepository,
            IEntityBaseRepository<ClassInfo> classRepository,
             IEntityBaseRepository<FamilyStudentMap> familyStudentMapRepository,
             IEntityBaseRepository<StudentSchedule> studentScheduleRepository,
             ICommonService commonService,
             IEntityBaseRepository<TasksClassMapping> tasksClassMappinRepository,
             IEntityBaseRepository<tblParentParticipantMapping> tblParentParticipantMappingRepository,
            IUnitOfWork unitOfWork)
        {
            _tasksRepository = tasksRepository;
            _staffsRepository = staffsRepository;
            _classRepository = classRepository;
            _familyStudentMapRepository = familyStudentMapRepository;
            _studentScheduleRepository = studentScheduleRepository;
            _tasksClassMappinRepository = tasksClassMappinRepository;
            _tblParentParticipantMappingRepository = tblParentParticipantMappingRepository;
            _commonService = commonService;
            _unitOfWork = unitOfWork;
        }

        public void GetAllStaffs(SearchTasksViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<StaffInfo>();
                predicate = predicate.And(p => p.IsTimeClockUser == true);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyRegistrationId == model.AgencyId);

                var resultData = _staffsRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.FirstName);
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.FirstName);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.FirstName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                   .ToList();

                var staffLst = Mapper.Map<List<StaffInfo>, List<StaffViewModel>>(returnData);
                var staffLstnew = staffLst.Select(i => new { Id = i.ID, Name = i.FirstName + " " + i.LastName }).ToList();

                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffLstnew;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }


        public void GetAllClass(SearchTasksViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<ClassInfo>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                var resultData = _classRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "ClassName":
                        resultData = resultData.OrderBy(m => m.ClassName);
                        break;
                    case "-ClassName":
                        resultData = resultData.OrderByDescending(m => m.ClassName);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.ClassName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .ToList();

                var classLst = Mapper.Map<List<ClassInfo>, List<ClassViewModel>>(returnData);
                var classLstnew = classLst.Select(i => new { Id = i.ID, Name = i.ClassName }).ToList();
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = classLstnew;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }


        public void GetAllTasks(SearchTasksViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Tasks>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (model.StaffId > 0)
                    predicate = predicate.And(p => p.StaffId == model.StaffId);

                if (model.TaskType == "s")
                {
                    //for staff schedule(Agency)
                    predicate = predicate.And(p => p.StaffId > 0);
                }
                if (model.TaskType == "e")
                {
                    //for event(Agency and Staff)
                    //predicate = predicate.And(p =>  p.ClassId == 0);
                    predicate = predicate.And(p => p.StaffId == 0);

                }
                if (model.TaskType == "f")
                {
                    //predicate = predicate.And(p =>  p.ClassId > 0);
                    predicate = predicate.And(p => p.ClassId == 0);
                }
                var result = _tasksRepository.GetAll();
                var resultData = _tasksRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        result = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        result = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "AgencyId":
                        result = resultData.OrderBy(m => m.AgencyId);
                        break;
                    case "-AgencyId":
                        result = resultData.OrderByDescending(m => m.AgencyId);
                        break;
                }
                var returnData = result.AsExpandable().Where(predicate).AsExpandable()
             .ToList();



                var lst = Mapper.Map<List<Tasks>, List<TasksViewModelCopy>>(returnData);
                List<long> classIds = null;
                if (model.TaskType == "f")
                {
                    var maplist = _tblParentParticipantMappingRepository.FindBy(x => x.NewParentInfoID == model.FamilyId).Select(m=>m.NewParticipantInfoID).ToList();
                    List<StudentSchedule> studentinfolist = new List<StudentSchedule>();
                    maplist.ForEach(
                        x => studentinfolist.AddRange(_studentScheduleRepository.FindBy(m => m.StudentId == x.Value).ToList())
                        );

                    classIds = studentinfolist.GroupBy(n => new { n.ClassId })
                 .Select(g => new
                 {
                     g.Key.ClassId
                 }).Select(i => i.ClassId).ToList();


                    //List<TasksViewModelCopy> listcopy = new List<TasksViewModelCopy>();
                    //    results.ForEach(
                    //        x => listcopy.AddRange(lst.Where(m => m.ClassId == x.ClassId).ToList())
                    //        );
                    //    lst = listcopy;
                }
                List<TasksViewModelCopy> listcopy = new List<TasksViewModelCopy>();
                string className = "";
                foreach (var item in lst)
                {
                    className = "";
                    var staffName = _staffsRepository.FindBy(m => m.ID == item.StaffId).Select(m => m.FirstName + " " + m.LastName).FirstOrDefault();
                    item.StaffName = Convert.ToString(staffName);



                    item.ClassName = Convert.ToString(className);

                    if (item.Start != null)
                    {
                        item.Start = (DateTime)_commonService.ConvertFromUTC(item.Start, model.TimeZone);
                    }
                    if (item.End != null)
                    {
                        item.End = (DateTime)_commonService.ConvertFromUTC(item.End, model.TimeZone);
                    }
                    var tasksClassesMapped = _tasksClassMappinRepository.GetAll().Where(m => m.TaskId == item.TaskID).ToList();
                    List<TaskClassMappingViewModel> test = new List<TaskClassMappingViewModel>();
                    string tempClassName = "";

                    ///for family enrolled classes
                    if (classIds != null)
                    {
                        var enrolledClassedForFamily = tasksClassesMapped.Where(m => classIds.Contains(m.ClassId)).ToList();
                        if (enrolledClassedForFamily == null || enrolledClassedForFamily.Count == 0)
                            continue;
                    }
                    //////////////////////////////////

                    foreach (var data in tasksClassesMapped)
                    {
                        tempClassName = _classRepository.FindBy(l => l.ID == data.ClassId).Select(l => l.ClassName).FirstOrDefault().ToString();
                        className += tempClassName + ",";

                        test.Add(new TaskClassMappingViewModel()
                        {
                            Id = data.ID,
                            ClassId = data.ClassId,
                            TaskId = item.TaskID,
                            IsDeleted = false,
                            Name = tempClassName
                        });

                    }
                    item.ClassIdJson = test;
                    if (!string.IsNullOrEmpty(className))
                        item.ClassName = className.Substring(0, className.Length - 1);


                    listcopy.Add(item);
                    //    tasksClassesMapped.ForEach(m=> item.ClassIdJson.Add(new TaskClassMappingViewModel()
                    //    {
                    //       Id=m.ID,
                    //       ClassId=m.ClassId,
                    //       TaskId=item.TaskID,
                    //       IsDeleted=false,
                    //       Name= (_classRepository.FindBy(l => l.ID == m.ClassId).Select(l => l.ClassName).FirstOrDefault()).ToString()
                    //}));


                }
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = listcopy;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }
        public void GetAll(TasksViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Tasks>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                var resultData = _tasksRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();


                IQueryable<Tasks> result = resultData.OrderBy(m => m.AgencyId);

                var returnData = result.AsExpandable().Where(predicate).AsExpandable()
             .ToList();



                var lst = Mapper.Map<List<Tasks>, List<TasksViewModelCopy>>(returnData);

                foreach (var item in lst)
                {
                    if (item.Start != null)
                    {
                        item.Start = (DateTime)_commonService.ConvertFromUTC(item.Start, model.TimeZone);
                    }
                    if (item.End != null)
                    {
                        item.End = (DateTime)_commonService.ConvertFromUTC(item.End, model.TimeZone);
                    }
                }
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = lst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }
        public void AddTasks(TasksViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var tasksInfo = Mapper.Map<TasksViewModel, Tasks>(model);

                _tasksRepository.Add(tasksInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                if (model.ClassIdJson!=null&& model.ClassIdJson.Count > 0)
                {
                    foreach (var item in model.ClassIdJson)
                    {
                        TasksClassMapping obj = new TasksClassMapping()
                        {
                           IsDeleted=false,
                           TaskId=tasksInfo.ID,
                           ClassId=item.Id
                        };
                        _tasksClassMappinRepository.FindBy(m => m.TaskId == item.TaskId).ToList().ForEach(k => _tasksClassMappinRepository.Delete(k));
                        _tasksClassMappinRepository.Add(obj);
                    }
                }
                _unitOfWork.Commit();
                transaction.ReturnMessage.Add("Tasks added successfully");
                transaction.ID = tasksInfo.ID;
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        public void DeleteTasksById(TasksViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var tasksDetails =
                    _tasksRepository.FindBy(m => m.ID == model.TaskID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (tasksDetails != null && tasksDetails.ID > 0)
                {
                    tasksDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("Tasks successfully deleted at " +
                                                  DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }



        public void UpdateTasks(TasksViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var staffScheduleLst =
                    _tasksRepository.FindBy(m => m.ID == model.TaskID && m.IsDeleted == false).FirstOrDefault();
                var mapInfo = Mapper.Map<TasksViewModel, Tasks>(model);
                mapInfo.IsDeleted = false;
                _tasksRepository.Edit(staffScheduleLst, mapInfo);
                _unitOfWork.Commit();

                if (model.ClassIdJson.Count > 0)
                {
                    foreach (var item in model.ClassIdJson)
                    {
                        TasksClassMapping obj = new TasksClassMapping()
                        {
                            IsDeleted = false,
                            TaskId = mapInfo.ID,
                            ClassId = item.Id
                        };
                        _tasksClassMappinRepository.FindBy(m => m.TaskId == model.TaskID).ToList().ForEach(k => _tasksClassMappinRepository.Delete(k));
                        _tasksClassMappinRepository.Add(obj);
                    }
                }
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Tasks successfully updated at " +
                                              DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(ex.Message);
            }
        }
    }
}
