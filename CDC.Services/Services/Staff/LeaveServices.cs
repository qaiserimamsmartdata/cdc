﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Enums;
using CDC.Entities.Staff;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using LinqKit;
using System.Data.Entity;

namespace CDC.Services.Services.Staff
{
    public class LeaveServices : ILeaveServices
    {
        private readonly IEntityBaseRepository<StaffInfo> _staffRepository;
        private readonly IEntityBaseRepository<LeaveInfo> _leaveRepository;
        private readonly IEntityBaseRepository<Tasks> _tasksRepository;
        private readonly IUnitOfWork _unitOfWork;

        public LeaveServices(
            IEntityBaseRepository<LeaveInfo> leaveRepository,
             IEntityBaseRepository<StaffInfo> staffRepository,
              IEntityBaseRepository<Tasks> tasksRepository,
            IUnitOfWork unitOfWork)
        {
            _leaveRepository = leaveRepository;
            _staffRepository = staffRepository;
            _tasksRepository = tasksRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Gets all leave.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        public void GetAllLeave(SearchLeaveViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<LeaveInfo>();
                if (model.AgencyId > 0 && model.StaffId == null)
                    predicate = predicate.And(p => p.Staff.AgencyRegistrationId == model.AgencyId);
                //predicate = predicate.And(p => p.Staff.AgencyRegistrationId == model.AgencyId && p.StatusId!=3);
                else
                    predicate = predicate.And(p => p.StaffId == model.StaffId);
                if (model.LeaveStatusId > 0)
                    predicate = predicate.And(p => p.StatusId == model.LeaveStatusId);
                if (model.StatusId > 0)
                    predicate = predicate.And(p => p.StatusId == model.StatusId);
                if (model.LeaveStartDate!= null)
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.LeaveFromDate.Value) >= DbFunctions.TruncateTime(model.LeaveStartDate.Value));
                if (model.leaveEndDate != null)
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.LeaveToDate.Value) <= DbFunctions.TruncateTime(model.leaveEndDate.Value));
                var resultData = _leaveRepository.GetAll().AsExpandable().Where(predicate).AsExpandable();
               
                //if (model.LeaveStartDate != null && model.leaveEndDate != null)
                //{
                //    resultData = resultData.Where(m => m.LeaveFromDate >= model.LeaveStartDate && m.LeaveToDate <= model.leaveEndDate);
                //}
                //else if (model.LeaveStartDate != null && model.leaveEndDate == null)
                //{
                //    resultData = resultData.Where(m => m.LeaveFromDate >= model.LeaveStartDate);
                //}
             
                responseInfo.TotalRows = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Name":
                        resultData = resultData.OrderBy(m => m.Staff.FirstName).ThenBy(m => m.Staff.LastName);
                        break;
                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Staff.FirstName).ThenBy(m => m.Staff.LastName);
                        break;
                    case "Status":
                        resultData = resultData.OrderBy(m => m.Status.Name);
                        break;
                    case "-Status":
                        resultData = resultData.OrderByDescending(m => m.Status.Name);
                        break;
                    case "FromDate":
                        resultData = resultData.OrderBy(m => m.LeaveFromDate);
                        break;
                    case "-FromDate":
                        resultData = resultData.OrderByDescending(m => m.LeaveFromDate);
                        break;
                    case "ToDate":
                        resultData = resultData.OrderBy(m => m.LeaveToDate);
                        break;
                    case "-ToDate":
                        resultData = resultData.OrderByDescending(m => m.LeaveToDate);
                        break;
                }
                if (model.order == null)
                {
                    resultData = resultData.OrderByDescending(m => m.LeaveFromDate);
                }
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var leaveLst = Mapper.Map<List<LeaveInfo>, List<LeaveViewModel>>(returnData);
                responseInfo.IsSuccess = true;
                responseInfo.Content = leaveLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Adds the leave.
        /// </summary>
        /// <param name="leaveVm">The leave vm.</param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void AddLeave(LeaveViewModel leaveVm, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                leaveVm.StatusId = (long)EnumLeaveStatus.Pending;
                var leaveInfo = Mapper.Map<LeaveViewModel, LeaveInfo>(leaveVm);

                DateTime utcDate = DateTime.UtcNow;

                leaveInfo.LeaveFromDate = new DateTime(leaveInfo.LeaveFromDate.Value.Year, leaveInfo.LeaveFromDate.Value.Month, leaveInfo.LeaveFromDate.Value.Day, utcDate.Hour, utcDate.Minute, utcDate.Second);
                leaveInfo.LeaveToDate = new DateTime(leaveInfo.LeaveToDate.Value.Year, leaveInfo.LeaveToDate.Value.Month, leaveInfo.LeaveToDate.Value.Day, utcDate.Hour, utcDate.Minute, utcDate.Second);



                _leaveRepository.Add(leaveInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Schedule added successfully");
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Updates the leave detail.
        ///     Ask for Status(Pending,OnLeave,Cancel),Remarks from in-charge while cancel or approve the leave.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void UpdateLeaveDetail(LeaveViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var leaveInfoNew = Mapper.Map<LeaveViewModel, LeaveInfo>(model);
                var leaveInfoOld =
                    _leaveRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                _leaveRepository.Edit(leaveInfoOld, leaveInfoNew);



                var taskInfo = _tasksRepository.FindByAll(m => m.LeaveId == leaveInfoNew.ID).FirstOrDefault();
                if(taskInfo == null)
                {
                    //Leave Not Present in Task
                    if (model.StatusId == 4)
                    {
                        //Leave Approved
                        TasksViewModel tashvm = new TasksViewModel
                        {
                            TaskID = 0,
                            Title = "is on leave",
                            Description = leaveInfoNew.Remarks,
                            Start = leaveInfoNew.LeaveFromDate.Value,
                            StartTimezone = "",
                            End = leaveInfoNew.LeaveToDate.Value,
                            EndTimezone = "",
                            RecurrenceRule = "",
                            RecurrenceID = 0,
                            RecurrenceException = "",
                            IsAllDay = false,
                            OwnerID = 1,
                            AgencyId = model.AgencyId,
                            StaffId = Convert.ToInt32(model.StaffId),
                            LeaveId = Convert.ToInt32(leaveInfoNew.ID),
                            TimeZone = ""
                        };

                        var tasksInfo = Mapper.Map<TasksViewModel, Tasks>(tashvm);
                        _tasksRepository.Add(tasksInfo);

                    }
                }
                else
                {
                    if (taskInfo.LeaveId == leaveInfoNew.ID)
                    {
                        //Leave Present in Task
                        if (model.StatusId == 4)
                        {
                            //For Approved
                            taskInfo.IsDeleted = false;
                            _tasksRepository.Edit(taskInfo, taskInfo);

                        }
                        if (model.StatusId == 3)
                        {
                            //For Declined
                            taskInfo.IsDeleted = true;
                            _tasksRepository.Edit(taskInfo, taskInfo);
                        }
                    }
                    else
                    {
                        //Leave Not Present in Task
                        if (model.StatusId == 4)
                        {
                            //Leave Approved
                            TasksViewModel tashvm = new TasksViewModel
                            {
                                TaskID = 0,
                                Title = "is on leave",
                                Description = leaveInfoNew.Remarks,
                                Start = leaveInfoNew.LeaveFromDate.Value,
                                StartTimezone = "",
                                End = leaveInfoNew.LeaveToDate.Value,
                                EndTimezone = "",
                                RecurrenceRule = "",
                                RecurrenceID = 0,
                                RecurrenceException = "",
                                IsAllDay = false,
                                OwnerID = 1,
                                AgencyId = model.AgencyId,
                                StaffId = Convert.ToInt32(model.StaffId),
                                LeaveId = Convert.ToInt32(leaveInfoNew.ID),
                                TimeZone = ""
                            };

                            var tasksInfo = Mapper.Map<TasksViewModel, Tasks>(tashvm);
                            _tasksRepository.Add(tasksInfo);

                        }
                    }
                }
               

                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Schedule successfully updated at " +
                                              DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Deletes the leave by identifier.
        ///     Delete Only when in-charge decline or cancel leave
        /// </summary>
        /// <param name="leaveId">The leave identifier.</param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void DeleteLeaveById(long leaveId, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                long count = _leaveRepository.FindBy(d => d.ID == leaveId && d.IsDeleted == false).Count();
                if (count > 0)
                {
                    var leaveDetails =
                        _leaveRepository.FindBy(m => m.ID == leaveId && m.IsDeleted == false).FirstOrDefault();
                    if (leaveDetails != null) leaveDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("Schedule successfully deleted at " +
                                                  DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                }
            }
            catch
            {
                transaction.ReturnMessage = new List<string>();
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add("Unable to delete at the moment. Please try again after some time.");
            }
        }

        /// <summary>
        ///     Gets the leave by identifier.
        /// </summary>
        /// <param name="staffId">The staff identifier.</param>
        /// <returns></returns>
        public LeaveViewModel GetLeaveById(long staffId)
        {
            var obj = _leaveRepository.GetSingle(staffId);
            var leaveObj = Mapper.Map<LeaveInfo, LeaveViewModel>(obj);
            return leaveObj;
        }
    }
}