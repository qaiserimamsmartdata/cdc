﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Staff;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using LinqKit;

namespace CDC.Services.Services.Staff
{
    public class StaffScheduleService : IStaffScheduleService
    {
        private readonly IEntityBaseRepository<StaffScheduleInfo> _staffScheduleRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StaffScheduleService(
            IEntityBaseRepository<StaffScheduleInfo> staffScheduleRepository,
            IUnitOfWork unitOfWork)
        {
            _staffScheduleRepository = staffScheduleRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Gets all schedule.
        /// </summary>
        /// 
        /// <param name="responseInfo">The response information.</param>
        public void GetAllSchedule(SearchStaffScheduleViewModel model,out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<StaffScheduleInfo>();
                if (model.AgencyId > 0)
                    predicate.And(p => p.Staff.AgencyRegistrationId == model.AgencyId);
                var resultData = _staffScheduleRepository.GetAll().ToList();
                long totalCount = resultData.Count();
                var staffSchdulLst = Mapper.Map<List<StaffScheduleInfo>,List<StaffScheduleViewModel>>(resultData);
                staffSchdulLst = staffSchdulLst.Where(m => m.Staff.AgencyRegistrationId == model.AgencyId).ToList();
                if (model.StaffId > 0)
                    staffSchdulLst = staffSchdulLst.Where(m => m.StaffId == model.StaffId).ToList();
                List<StaffScheduleViewModel> dailyList = new List<StaffScheduleViewModel>();
                foreach (var item in staffSchdulLst)
                {
                    var recurrencevalue = item.RecurrenceTypeValue;
                    var start = (DateTime)item.Date;
                    var end = (DateTime)item.EndDate;

                    switch (item.RecurrenceTypeId)
                    {
                        case 0:
                            dailyList.Add(item);
                            break;
                        case 1:
                            for (var day = start.Date; day.Date <= end.Date; day = day.AddDays(recurrencevalue))
                            {
                                StaffScheduleViewModel svm = new StaffScheduleViewModel
                                {
                                    Class = item.Class,
                                    ClassId = item.ClassId,
                                    Date = (DateTime) ToDateTime((DateTime) day, (TimeSpan) item.Time),
                                    EndDate = (DateTime) ToDateTime((DateTime) day, (TimeSpan) item.EndTime),
                                    EndTime = (TimeSpan) item.EndTime,
                                    Time = (TimeSpan) item.Time,
                                    ID = item.ID,
                                    LessonPlan = item.LessonPlan,
                                    LessonPlanId = item.LessonPlanId,
                                    RecurrenceTypeId = item.RecurrenceTypeId,
                                    RecurrenceTypeValue = item.RecurrenceTypeValue,
                                    Room = item.Room,
                                    RoomId = item.RoomId,
                                    Staff = item.Staff,
                                    StaffId = item.StaffId,
                                    Title = item.Title,
                                    TotalCnt = item.TotalCnt,
                                    IsDeleted = item.IsDeleted
                                };

                                dailyList.Add(svm);
                            }
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                  
                }
                staffSchdulLst = dailyList;
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffSchdulLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        public DateTime ToDateTime(DateTime date,TimeSpan timespan)
        {
            TimeSpan ts = (TimeSpan)timespan;
            DateTime dt = (DateTime)date.Date;
            var abc = new DateTime(dt.Year, dt.Month, dt.Day, ts.Hours, ts.Minutes, ts.Seconds);
            return (DateTime)abc;
        }

        /// <summary>
        ///     Gets all staff schedule.
        ///     Not Used this api, reference purpose
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        public void GetAllStaffSchedule(SearchStaffScheduleViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();

            try
            {
                var predicate = PredicateBuilder.True<StaffScheduleInfo>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.Staff.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.StaffName))
                    predicate =
                        predicate.And(
                            p =>
                                p.Staff.FirstName.Contains(model.StaffName) ||
                                p.Staff.LastName.Contains(model.StaffName));
                else if (model.StaffId > 0)
                    predicate = predicate.And(p => p.StaffId == model.StaffId);
                if (model.DesignationId > 0)
                    predicate = predicate.And(p => p.Staff.PositionId == model.DesignationId);
                if (model.ClassId > 0)
                    predicate = predicate.And(p => p.ClassId == model.ClassId);

                var resultData = _staffScheduleRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                if (!string.IsNullOrEmpty(model.order))
                {
                    switch (model.order)
                    {
                        case "id":
                            resultData = resultData.OrderBy(m => m.ID);
                            break;
                        case "-id":
                            resultData = resultData.OrderByDescending(m => m.ID);
                            break;
                        case "StaffName":
                            resultData = resultData.OrderBy(m => m.Staff.FirstName).ThenBy(m => m.Staff.LastName);
                            break;
                        case "-StaffName":
                            resultData =
                                resultData.OrderByDescending(m => m.Staff.FirstName).ThenBy(m => m.Staff.LastName);
                            break;
                    }
                }
                var returnData = resultData.AsExpandable().Skip((model.page - 1)*model.limit).Take(model.limit).ToList();
                var staffSchdulLst = Mapper.Map<List<StaffScheduleInfo>, List<StaffScheduleViewModel>>(returnData);

                #region Fetch Here according to business requirement , it will show on Web

                var staffResultList = new
                {
                    staffSchdulLst = staffSchdulLst.Select(m => new
                    {
                        m.ID,
                        m.Title,
                        m.Time,
                        m.Date
                       
                    })
                };

                #endregion

                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffResultList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }


        public void GetAllScheduleStaffWise(SearchStaffScheduleViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
   
                var predicate = PredicateBuilder.True<StaffScheduleInfo>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.Staff.AgencyRegistrationId == model.AgencyId);
                if (model.StaffId > 0)
                    predicate = predicate.And(p => p.StaffId == model.StaffId);

                var resultData = _staffScheduleRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                if (!string.IsNullOrEmpty(model.order))
                {
                    switch (model.order)
                    {
                        case "id":
                            resultData = resultData.OrderBy(m => m.ID);
                            break;
                        case "-id":
                            resultData = resultData.OrderByDescending(m => m.ID);
                            break;
                        case "Title":
                            resultData = resultData.OrderBy(m => m.Title).ThenBy(m => m.Title);
                            break;
                        case "-Title":
                            resultData =resultData.OrderByDescending(m => m.Title).ThenBy(m => m.Title);
                            break;
                        case "Date":
                            resultData = resultData.OrderBy(m => m.Date).ThenBy(m => m.Date);
                            break;
                        case "-Date":
                            resultData = resultData.OrderByDescending(m => m.Date).ThenBy(m => m.Date);
                            break;
                    }
                }

                var returnData = resultData.AsExpandable().Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var staffSchdulLst = Mapper.Map<List<StaffScheduleInfo>, List<StaffScheduleViewModel>>(returnData);

              

                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffSchdulLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Adds the schedule staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddScheduleStaff(StaffScheduleViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var staffScheduleInfo = Mapper.Map<StaffScheduleViewModel, StaffScheduleInfo>(model);

                staffScheduleInfo.Date = new DateTime(staffScheduleInfo.Date.Value.Year, staffScheduleInfo.Date.Value.Month, staffScheduleInfo.Date.Value.Day,
                staffScheduleInfo.Time.Value.Hours, staffScheduleInfo.Time.Value.Minutes, staffScheduleInfo.Time.Value.Seconds);
                staffScheduleInfo.EndDate = new DateTime(staffScheduleInfo.EndDate.Value.Year, staffScheduleInfo.EndDate.Value.Month, staffScheduleInfo.EndDate.Value.Day,
                   staffScheduleInfo.EndTime.Value.Hours, staffScheduleInfo.EndTime.Value.Minutes, staffScheduleInfo.EndTime.Value.Seconds);

                _staffScheduleRepository.Add(staffScheduleInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Schedule added successfully");
                transaction.ID = staffScheduleInfo.ID;
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        /// <summary>
        ///     Updates the schedule staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void UpdateScheduleStaff(StaffScheduleViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var staffScheduleLst =
                    _staffScheduleRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapStaffInfo = Mapper.Map<StaffScheduleViewModel, StaffScheduleInfo>(model);

                mapStaffInfo.Date = new DateTime(mapStaffInfo.Date.Value.Year, mapStaffInfo.Date.Value.Month, mapStaffInfo.Date.Value.Day,
                 mapStaffInfo.Time.Value.Hours, mapStaffInfo.Time.Value.Minutes, mapStaffInfo.Time.Value.Seconds);
                mapStaffInfo.EndDate = new DateTime(mapStaffInfo.EndDate.Value.Year, mapStaffInfo.EndDate.Value.Month, mapStaffInfo.EndDate.Value.Day,
                   mapStaffInfo.EndTime.Value.Hours, mapStaffInfo.EndTime.Value.Minutes, mapStaffInfo.EndTime.Value.Seconds);

                _staffScheduleRepository.Edit(staffScheduleLst, mapStaffInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Schedule successfully updated at " +
                                              DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Deletes the staff schedule by identifier.
        /// </summary>
        /// <param name="staffScheduledId">The staff scheduled identifier.</param>
        /// <param name="transaction">The transaction.</param>
        public void DeleteStaffScheduleById(long staffScheduledId, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var studentSheduleDetails =
                    _staffScheduleRepository.FindBy(m => m.ID == staffScheduledId && m.IsDeleted == false)
                        .FirstOrDefault();
                if (studentSheduleDetails != null && studentSheduleDetails.ID > 0)
                {
                    studentSheduleDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("Schedule successfully deleted at " +
                                                  DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
    }
}