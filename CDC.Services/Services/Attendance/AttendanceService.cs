﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Attendance;
using CDC.Entities.Staff;
using CDC.Services.Abstract.Attendance;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Common;
using LinqKit;
using System.Data.Entity;
using CDC.Services.Abstract;
using CDC.ViewModel.Staff;
using CDC.Services.Abstract.Staff;

namespace CDC.Services.Services.Attendance
{
    public class AttendanceService : IAttendanceService
    {
        private readonly IEntityBaseRepository<AttendanceInfo> _attendanceRepository;
        private readonly IEntityBaseRepository<StaffInfo> _staffInfoRepository;
        private readonly ICommonService _commonService;
        private readonly ITimeSheetService _timeSheetService;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IEntityBaseRepository<Timesheet> _timesheetRepository;
        public AttendanceService(
            IEntityBaseRepository<AttendanceInfo> attendanceRepository,
            IEntityBaseRepository<StaffInfo> staffInfoRepository,
             ICommonService commonService,
             ITimeSheetService timesheetService,

IEntityBaseRepository<Timesheet> timesheetRepository,
            IUnitOfWork unitOfWork)
        {
            _attendanceRepository = attendanceRepository;
            _staffInfoRepository = staffInfoRepository;
            _commonService = commonService;
            _timeSheetService = timesheetService;
            _unitOfWork = unitOfWork;
            _timesheetRepository = timesheetRepository;

        }
        /// <summary>
        ///     Gets the staff schedule list.
        /// </summary>
        /// <param name="model">
        ///     The model.
        ///     The model.
        /// </param>
        /// <param name="responseResult">
        ///     The response result.
        ///     The response result.
        /// </param>
        /// <summary>
        ///     Gets the staff attendance list.
        /// </summary>
        public void GetStaffAttendanceList(SearchStaffAttendanceViewModel model, out ResponseViewModel responseResult)
        {
            responseResult = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<AttendanceInfo>();
                var stafflist = _staffInfoRepository.FindBy(f => f.AgencyRegistrationId == model.AgencyId);
                var resultData =
                    _attendanceRepository.GetAll().Where(w => w.Staff.AgencyRegistrationId == model.AgencyId);
                var jj = resultData.ToList();
                if (!string.IsNullOrEmpty(model.StaffName))
                {
                    stafflist =
                        stafflist.Where(
                            w => w.FirstName.Contains(model.StaffName) || w.LastName.Contains(model.StaffName));
                }
                long totalCount = stafflist.Count();
                switch (model.order)
                {
                    case "id":
                        stafflist = stafflist.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        stafflist = stafflist.OrderByDescending(m => m.ID);
                        break;
                    case "StaffName":
                        stafflist = stafflist.OrderBy(m => m.FirstName);
                        break;
                    case "-StaffName":
                        stafflist = stafflist.OrderByDescending(m => m.FirstName);
                        break;
                }

                if (string.IsNullOrEmpty(model.order))
                    stafflist = stafflist.OrderByDescending(m => m.FirstName);
                var returnData = stafflist
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var stafflistObj = Mapper.Map<List<StaffInfo>, List<AttendanceViewModel>>(returnData);
                foreach (var item in stafflistObj)
                {
                    if (model.AttendanceDate != null)
                    {
                        var atteUtcDate = _commonService.ConvertFromUTC(model.AttendanceDate.Value, model.TimeZoneName);
                        var staffattendancedata = resultData.FirstOrDefault(w => w.StaffId == item.StaffId
                                                                                 && w.AttendanceDate.Value.Day == atteUtcDate.Value.Day
                                                                                 && w.AttendanceDate.Value.Month == atteUtcDate.Value.Month
                                                                                 && w.AttendanceDate.Value.Year == atteUtcDate.Value.Year
                            );
                        if (staffattendancedata != null)
                        {
                            item.AttendanceMarkedId = staffattendancedata.AttendanceMarkedBy;
                            item.CreatedBy = staffattendancedata.CreatedBy;
                            item.CreatedDate = staffattendancedata.CreatedDate;
                            item.Comment = staffattendancedata.Comment;
                            item.ID = staffattendancedata.ID;
                            if (staffattendancedata.InTime != null)
                            {
                                item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(staffattendancedata.InTime.ToString()), staffattendancedata.InTime.Value, model.TimeZoneName);
                                string[] words = item.InTime.ToString().Split(':');
                                string h = words[0];
                                string m = words[1];
                                item.InTime = new TimeSpan(Convert.ToInt32(h), Convert.ToInt32(m), 0);
                            }
                            if (staffattendancedata.OutTime != null)
                            {
                                item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(staffattendancedata.OutTime.ToString()), staffattendancedata.OutTime.Value, model.TimeZoneName);
                                string[] words = item.OutTime.ToString().Split(':');
                                string h = words[0];
                                string m = words[1];
                                item.OutTime = new TimeSpan(Convert.ToInt32(h), Convert.ToInt32(m), 0);
                            }
                            item.ModifiedBy = staffattendancedata.ModifiedBy;
                            item.ModifiedDate = staffattendancedata.ModifiedDate;
                            item.OnLeave = staffattendancedata.OnLeave;
                            item.IsDeleted = Convert.ToBoolean(staffattendancedata.IsDeleted);
                        }
                        else
                        {
                            item.ID = 0;
                        }
                    }
                    item.AttendanceDate = model.AttendanceDate;
                }
                responseResult.TotalRows = totalCount;
                responseResult.IsSuccess = true;
                responseResult.Content = stafflistObj;
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Adds the attendance.
        /// </summary>
        /// <param name="model">The model.</param>
        public ResponseViewModel AddAttendance(AttendanceViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                if (model.AttendanceDate != null)
                {
                    DateTime? attDate = _commonService.ConvertFromUTC(model.AttendanceDate.Value, model.TimeZone);
                    var attendanceInfo = Mapper.Map<AttendanceViewModel, AttendanceInfo>(model);

                    DateTime dtnew = new DateTime((model.AttendanceDate.Value.Date + model.InTime ?? DateTime.UtcNow).Ticks, DateTimeKind.Unspecified);
                    var inUtcDate = _commonService.ConvertToUTC(dtnew, model.TimeZone);
                    model.AttendanceDate = inUtcDate;
                    if (model.OnLeave == false)
                        if (inUtcDate != null) attendanceInfo.InTime = inUtcDate.Value.TimeOfDay;
                    if (inUtcDate != null)
                    {
                        var inDate = _commonService.ConvertFromUTC(inUtcDate.Value, "India Standard Time");

                        attendanceInfo.AttendanceDate = inUtcDate;

                        DateTime? outUtcDate = null;
                        DateTime? outDate = null;
                        if (model.OutTime == null)
                        {
                            outUtcDate = null;
                            attendanceInfo.OutTime = null;
                            model.AttendanceDate = null;
                            outDate = null;
                        }
                        else
                        {
                            DateTime dtOutnew = new DateTime((attDate.Value.Date + model.OutTime ?? DateTime.UtcNow).Ticks, DateTimeKind.Unspecified);
                            outUtcDate = _commonService.ConvertToUTC(dtOutnew, model.TimeZone);
                            attendanceInfo.OutTime = outUtcDate.Value.TimeOfDay;
                            model.AttendanceDate = inUtcDate;
                            outDate = _commonService.ConvertFromUTC(outUtcDate.Value, "India Standard Time");
                        }


                        if (model.OutTime == null)
                        {
                            attendanceInfo.OutTime = null;
                        }

                        if (model.ID > 0)

                        {
                            var attendanceInfoOld = _attendanceRepository.FindBy(m => m.ID == model.ID).FirstOrDefault();
                            _attendanceRepository.Edit(attendanceInfoOld, attendanceInfo);

                            //----Update Timesheet On Update Attendance
                            //var timeSheetData = _TimesheetRepository.FindBy(m => m.StaffID == model.StaffId && m.TimeINStamp.Value.Day == model.AttendanceDate.Value.Day && m.TimeINStamp.Value.Month == model.AttendanceDate.Value.Month && m.TimeINStamp.Value.Year == model.AttendanceDate.Value.Year).OrderBy(m => m.TimeINStamp).ToList();
                            var timeSheetData = _timesheetRepository.FindBy(m => m.StaffID == model.StaffId && m.CheckInOutPurpose == 0).ToList();
                            var result = (from d in timeSheetData
                                where (d.TimeIN.Value.Day == attendanceInfo.AttendanceDate.Value.Day
                                       && d.TimeIN.Value.Month == attendanceInfo.AttendanceDate.Value.Month
                                       && d.TimeIN.Value.Year == attendanceInfo.AttendanceDate.Value.Year)
                                select d).ToList();

                            //-------------when OnLeave is true earlier  entered data will be deleted--------------
                            if (result != null && result.Count > 0 && model.OnLeave == true)
                            {
                                var timeSheetData1 = _timesheetRepository.FindBy(m => m.StaffID == model.StaffId && m.CheckInOutPurpose == 0).ToList();
                                var result1 = (from d in timeSheetData1
                                    where (d.TimeIN.Value.Day == attendanceInfo.AttendanceDate.Value.Day
                                           && d.TimeIN.Value.Month == attendanceInfo.AttendanceDate.Value.Month
                                           && d.TimeIN.Value.Year == attendanceInfo.AttendanceDate.Value.Year)
                                    select d).ToList();

                                foreach (var item in result1)
                                {
                                    item.IsDeleted = true;
                                    _timesheetRepository.Edit(item, item);
                                }




                            }
                            //------------------
                            if (result.Count > 0)
                            {
                                var oldTimeSheetTimeInData = result.ToList().OrderBy(m => m.ID).FirstOrDefault();
                                if (oldTimeSheetTimeInData != null)
                                {
                                    oldTimeSheetTimeInData.TimeINStamp = inUtcDate;
                                    oldTimeSheetTimeInData.TimeIN = inDate;
                                    oldTimeSheetTimeInData.TimeINComments = "Update By Agency";

                                    double totalHoursI1 = oldTimeSheetTimeInData.TimeOUTStamp.HasValue ? (oldTimeSheetTimeInData.TimeOUTStamp.Value.TimeOfDay - inUtcDate.Value.TimeOfDay).TotalHours : 0;
                                    totalHoursI1 = double.Parse(String.Format("{0:N2}", totalHoursI1));
                                    oldTimeSheetTimeInData.TotalHours = totalHoursI1;

                                    _timesheetRepository.Edit(oldTimeSheetTimeInData, oldTimeSheetTimeInData);
                                }

                                //-----------------------=-----------------------------
                                var oldTimeSheetOutData = result.ToList().OrderByDescending(m => m.ID).FirstOrDefault();
                                if (oldTimeSheetOutData != null)
                                {
                                    oldTimeSheetOutData.TimeOUTStamp = outUtcDate;
                                    oldTimeSheetOutData.TimeOUT = outDate;
                                    oldTimeSheetOutData.TimeOUTComments = "Update By Agency";

                                    double totalHours2 = outUtcDate.HasValue && oldTimeSheetOutData.TimeINStamp.HasValue ? (outUtcDate.Value.TimeOfDay - oldTimeSheetOutData.TimeINStamp.Value.TimeOfDay).TotalHours : 0;
                                    totalHours2 = double.Parse(String.Format("{0:N2}", totalHours2));
                                    oldTimeSheetOutData.TotalHours = totalHours2;

                                    _timesheetRepository.Edit(oldTimeSheetOutData, oldTimeSheetOutData);
                                }
                            }
                            //-------insert data into timesheet when onleave is false----------

                            else if (model.OnLeave == false)
                            {
                                TimeCheckModel value = new TimeCheckModel();
                                string IPAddress = "";
                                if (model.StaffId != null) value.StaffID = (int)model.StaffId;
                                value.TimesheetID = 0;
                                value.PurposeToCheckInOrOut = Entities.Enums.InOutPurpose.Day;
                                value.CheckOnDateTime = DateTime.UtcNow;
                                if (model.OutTime == null)
                                {
                                    TimeSpan span = new TimeSpan(0, 0, 0, 0, 0);
                                    model.OutTime = span;
                                }
                                if (model.InTime != null)
                                    _timeSheetService.SaveCheckFirstTimeFromStaffAttendance(value, inUtcDate.Value, model.InTime.Value, model.OutTime.Value, IPAddress, model.TimeZone);
                            }
                            _unitOfWork.Commit();
                            response.IsSuccess = true;
                            response.Message = "Attendance updated successfully";
                        }
                        else
                        {

                            _attendanceRepository.Add(attendanceInfo);
                            _unitOfWork.Commit();
                            response.IsSuccess = true;
                            response.Message = "Attendance added successfully";
                        }
                    }
                }
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }

        /// <summary>
        ///     Updates the staff attendance.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo">The response information.</param>
        public void UpdateStaffAttendance(AttendanceViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var staffDetail =
                    _attendanceRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapStaffViewModelInfo = Mapper.Map<AttendanceViewModel, AttendanceInfo>(model);
                _attendanceRepository.Edit(staffDetail, mapStaffViewModelInfo);
                _unitOfWork.Commit();
                responseInfo.ReturnStatus = true;
                responseInfo.ReturnMessage.Add("Staff attendance successfully updated at " + DateTime.UtcNow);
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Deletes the staff attendance by identifier.
        /// </summary>
        /// <param name="staffAttendanceId"></param>
        /// <param name="responseInfo">The response information.</param>
        public void DeleteStaffAttendanceById(long staffAttendanceId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var staffDetails =
                    _attendanceRepository.FindBy(m => m.ID == staffAttendanceId && m.IsDeleted == false)
                        .FirstOrDefault();
                if (staffDetails != null && staffDetails.ID > 0)
                {
                    staffDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Staff attendance deleted successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        public void GetStaffFailedSignOutList(SearchStaffAttendanceViewModel model,out ResponseViewModel responseResult)
        {
            responseResult = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<AttendanceInfo>();
                if (model.StaffId > 0)
                    predicate = predicate.And(x => x.StaffId == model.StaffId);
                if (model.AgencyId>0)
                    predicate = predicate.And(x => x.Staff.AgencyRegistrationId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.StaffName))
                    predicate = predicate.And(x => x.Staff.FirstName.Contains(model.StaffName) || x.Staff.LastName.Contains(model.StaffName));
                if (model.AttendanceDate != null)
                {
                    //predicate = predicate.And(x => x.AttendanceDate == model.AttendanceDate);
                    predicate = predicate.And(
                       p =>
                           (p.AttendanceDate.Value.Day == model.AttendanceDate.Value.Day &&
                           p.AttendanceDate.Value.Month == model.AttendanceDate.Value.Month &&
                           p.AttendanceDate.Value.Year == model.AttendanceDate.Value.Year)&& (p.InTime!=null&&p.OutTime==null));
                }
                //// predicate.And(p => p.Staff.IsTimeClockUser==true);
                // predicate.And(p => p.InTime !=null);
                // predicate.And(p => p.OutTime == null);
                //if (!string.IsNullOrEmpty(model.StaffName))
                //{
                //    predicate =
                //        predicate.And(
                //            p =>
                //                p.Staff.FirstName.Contains(model.) ||
                //                p.Staff.LastName.Contains(model.Name));
                //}
                //if (model.ClassId > 0)
                //{
                //    predicate = predicate.And(p => p.Staff. == model.ClassId);
                //}

                //var stafflist1 = _staffInfoRepository.FindBy(f => f.AgencyRegistrationId == model.AgencyId);
                //var resultData1 =
                //    _attendanceRepository.GetAll().Where(w => w.Staff.AgencyRegistrationId == model.AgencyId);

                //var scheduleData = _attendanceRepository.GetAll();
                var scheduleData = _attendanceRepository.GetAll().AsExpandable().Where(predicate);
                var totalCount = scheduleData.Count();



                switch (model.order)
                {
                    case "id":
                        scheduleData = scheduleData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        scheduleData = scheduleData.OrderByDescending(m => m.ID);
                        break;
                    case "StudentName":
                        scheduleData = scheduleData.OrderBy(m => m.Staff.FirstName);
                        break;
                    case "-StudentName":
                        scheduleData = scheduleData.OrderByDescending(m => m.Staff.FirstName);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    scheduleData = scheduleData.OrderByDescending(m => m.Staff.FirstName);

                var resultData = scheduleData.Include("Staff");
                var result = resultData                      
                        .Skip((model.page - 1) * model.limit)
                        .Take(model.limit)
                        .ToList();
                var NewData = Mapper.Map<List<AttendanceInfo>, List<AttendanceViewModel>>(result);


                foreach (var item in NewData)
                {
                    if (item.AttendanceDate != null)
                    {
                        item.AttendanceDate = (DateTime)_commonService.ConvertFromUTC(Convert.ToDateTime(item.AttendanceDate), model.TimeZoneName);
                    }
                    if (item.InTime != null)
                    {
                        item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.InTime.ToString()), item.InTime.Value, model.TimeZoneName);
                    }
                    if (item.OutTime != null)
                    {
                        item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.OutTime.ToString()), item.OutTime.Value, model.TimeZoneName);
                    }
                }

            

                responseResult.TotalRows = totalCount;
                responseResult.IsSuccess = true;
                responseResult.Content = NewData;
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }
        }
    }
}