﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Attendance;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.Services.Abstract.Attendance;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Common;
using CDC.ViewModel.Family;
using LinqKit;
using CDC.ViewModel.Schedule;
using CDC.Services.Abstract;
using CDC.Entities.ParticipantIncident;
using CDC.Entities.NewParent;
using CDC.ViewModel.NewParent;

namespace CDC.Services.Services.Attendance
{
    public class StudentAttendanceService : IStudentAttendanceService
    {
        private readonly IEntityBaseRepository<FamilyInfo> _familyRepository;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familyStudentMapRepository;
        private readonly IEntityBaseRepository<tblParentInfo> _parentRepository;
        private readonly IEntityBaseRepository<StudentAttendanceInfo> _studentAttendanceRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IEntityBaseRepository<Incident> _incidentRepository;
        private readonly ICommonService _commonService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEntityBaseRepository<tblParentParticipantMapping> _parentParticipantMappingRepository;
        public StudentAttendanceService(
            IEntityBaseRepository<StudentAttendanceInfo> studentAttendanceRepository,
            IEntityBaseRepository<StudentSchedule> studentScheduleRepository,
            IEntityBaseRepository<tblParentInfo> parentRepository,
            IEntityBaseRepository<FamilyInfo> familyRepository,
            IEntityBaseRepository<FamilyStudentMap> familyStudentMapRepository,
            IEntityBaseRepository<Incident> incidentRepository,
            ICommonService commonService,
            ICommonService participantRepository,
            IEntityBaseRepository<tblParentParticipantMapping> parentParticipantMappingRepository,
            IUnitOfWork unitOfWork)
        {
            _studentAttendanceRepository = studentAttendanceRepository;
            _studentScheduleRepository = studentScheduleRepository;
            _parentRepository = parentRepository;
            _familyRepository = familyRepository;
            _familyStudentMapRepository = familyStudentMapRepository;
            _commonService = commonService;
            _unitOfWork = unitOfWork;
            _incidentRepository = incidentRepository;
            _parentParticipantMappingRepository = parentParticipantMappingRepository;
        }

        /// <summary>
        ///     Gets the student schedule list.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseResult">The response result.</param>
        public void GetStudentScheduleList(SearchStudentAttendanceViewModel model, out ResponseViewModel responseResult)
        {
            responseResult = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<StudentSchedule>();
                if (!string.IsNullOrEmpty(model.Name))
                {
                    predicate =
                        predicate.And(
                            p =>
                                p.StudentInfo.FirstName.ToLower().Contains(model.Name.ToLower()) ||
                                p.StudentInfo.LastName.ToLower().Contains(model.Name.ToLower()));
                }
                else if (model.ClassId > 0)
                {
                    predicate = predicate.And(p => p.ClassId == model.ClassId);
                }
                var resultData = _studentScheduleRepository.GetAll();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "StudentName":
                        resultData = resultData.OrderBy(m => m.StudentInfo.FirstName);
                        break;
                    case "-StudentName":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.FirstName);
                        break;
                    case "AttendanceDate":
                        resultData = resultData.OrderBy(m => m.StartDate);
                        break;
                    case "-AttendanceDate":
                        resultData = resultData.OrderByDescending(m => m.StartDate);
                        break;

                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderBy(m => m.StudentInfo.FirstName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();

                var studentLst = Mapper.Map<List<StudentSchedule>, List<StudentAttendanceViewModel>>(returnData);

                responseResult.TotalRows = totalCount;
                responseResult.IsSuccess = true;
                responseResult.Content = studentLst;
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }
        }

        public void GetStudentAttendanceList(SearchStudentAttendanceViewModel model,
            out ResponseViewModel responseResult)
        {
            responseResult = new ResponseViewModel();
            try
            {
                string str = "";
                var predicate = PredicateBuilder.True<StudentSchedule>();
                predicate = predicate.And(p => p.ClassInfo.AgencyId == model.AgencyId);
                predicate = predicate.And(p => p.IsEnrolled == true);
                if (!string.IsNullOrEmpty(model.Name))
                {
                    predicate =
                        predicate.And(
                            p =>
                                p.StudentInfo.FirstName.ToLower().Contains(model.Name.ToLower()) ||
                                p.StudentInfo.LastName.ToLower().Contains(model.Name.ToLower()));
                }
                if (model.ClassId > 0)
                {
                    predicate = predicate.And(p => p.ClassId == model.ClassId);
                }

                //--------Neha Chopra----------
                if (model.Date != null)
                {
                    DateTime? attendanceDate = model.Date;
                    DayOfWeek dw = attendanceDate.Value.DayOfWeek;
                    str = dw.ToString();
                    predicate = predicate.And(p => p.StartDate <= attendanceDate);
                }
                List<StudentAttendanceViewModel> responseData;

                var scheduleData = _studentScheduleRepository.GetAll().Where(predicate).AsExpandable();
                switch (model.order)
                {
                    case "id":
                        scheduleData = scheduleData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        scheduleData = scheduleData.OrderByDescending(m => m.ID);
                        break;
                    case "StudentName":
                        scheduleData = scheduleData.OrderBy(m => m.StudentInfo.FirstName);
                        break;
                    case "-StudentName":
                        scheduleData = scheduleData.OrderByDescending(m => m.StudentInfo.FirstName);
                        break;
                    case "AttendanceDate":
                        scheduleData = scheduleData.OrderBy(m => m.StartDate);
                        break;
                    case "-AttendanceDate":
                        scheduleData = scheduleData.OrderByDescending(m => m.StartDate);
                        break;
                }

                //------------Neha Chopra----------------
                switch (str)
                {
                    case "Monday":
                        predicate.And(x => x.ClassInfo.Mon == true);
                        break;
                    case "Tuesday":
                        predicate.And(x => x.ClassInfo.Tue == true);
                        break;
                    case "Wednesday":
                        predicate.And(x => x.ClassInfo.Wed == true);
                        break;
                    case "Thursday":
                        predicate.And(x => x.ClassInfo.Thu == true);
                        break;
                    case "Friday":
                        predicate.And(x => x.ClassInfo.Fri == true);
                        break;
                    case "Saturday":
                        predicate.And(x => x.ClassInfo.Sat == true);
                        break;
                    case "Sunday":
                        predicate.And(x => x.ClassInfo.Sun == true);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    scheduleData = scheduleData.OrderByDescending(m => m.StudentInfo.FirstName);
                var scheduleResult = Mapper.Map<List<StudentSchedule>, List<StudentScheduleViewModel>>(scheduleData.ToList());
                var attendanceData =
                    _studentAttendanceRepository.FindBy(
                        f =>
                            f.AttendanceDate.Day == model.Date.Value.Day &&
                            f.AttendanceDate.Month == model.Date.Value.Month &&
                            f.AttendanceDate.Year == model.Date.Value.Year).ToList();
                var attendanceResult = Mapper.Map<List<StudentAttendanceInfo>, List<StudentAttendanceViewModel>>(attendanceData);
                var CurrentTime = (TimeSpan)_commonService.ConvertTimeFromUTC(DateTime.UtcNow, DateTime.UtcNow.TimeOfDay, model.TimeZoneName);
                if (attendanceResult.Count > 0)
                {


                    var query = (from schedule in scheduleResult
                                 join attendance in attendanceResult
                                 on new { schedule.StudentId, schedule = schedule.ID } equals
                                 new { attendance.StudentId, schedule = attendance.StudentScheduleId.Value } into gj
                                 from subpet in gj.DefaultIfEmpty()

                                 select new StudentAttendanceViewModel
                                 {
                                     StudentName = schedule.StudentInfo.FirstName + " " + schedule.StudentInfo.LastName,
                                     ClassName = schedule.ClassInfo.ClassName,
                                     AttendanceDate = subpet?.AttendanceDate ?? DateTime.UtcNow,
                                     DropedByName =
                                        subpet == null ? null : subpet.DropedBy == null ? "" : subpet.DropedBy.FirstName + " " + subpet.DropedBy.LastName,
                                     DropedById = subpet?.DropedById,
                                     CreatedBy = subpet?.CreatedBy,
                                     CreatedDate = subpet == null ? DateTime.UtcNow : (subpet
                                     .CreatedDate ?? DateTime.UtcNow),
                                     ModifiedBy = subpet == null ? 0 : subpet.ModifiedBy,
                                     ModifiedDate = subpet == null ? DateTime.UtcNow : subpet.ModifiedDate ?? DateTime.UtcNow,
                                     IsDeleted = subpet?.IsDeleted,
                                     DropedByOtherName = subpet == null ? null : subpet.DropedByOther == null ? "" : subpet.DropedByOther.Name,
                                     ID = subpet?.ID ?? 0,
                                     ImagePath = schedule.StudentInfo.ImagePath,
                                     PickupById = subpet?.PickupById,
                                     PickupByName =
                                     subpet == null ? null : subpet.PickupBy == null ? "" : subpet.PickupBy.FirstName + " " + subpet.PickupBy.LastName,
                                     PickupByOtherName = subpet == null ? null : subpet.PickupByOtherName ?? "",
                                     PickupByOtherId = subpet?.PickupByOtherId,
                                     InTime = subpet?.InTime,
                                     OutTime = subpet?.OutTime,
                                     OnLeave = subpet?.OnLeave,
                                     StudentId = schedule.StudentInfo.ID,
                                     ClassInfo = schedule.ClassInfo,
                                     IsOthers = false,
                                     IsParents = false,
                                     OnLeaveComment = subpet == null ? "" : subpet.OnLeaveComment,
                                     CheckSecurityKey = "",
                                     DisableOnLeave = subpet == null ? false : subpet.DisableOnLeave,
                                     StudentScheduleId = schedule.ID,
                                     DropByRelationName = subpet?.DropedBy?.RelationName,
                                     PickedByRelationName = subpet?.PickupBy?.RelationName,
                                 }).Where(m => m.OutTime == null ? (_commonService.ConvertTimeFromUTC(DateTime.UtcNow, m.ClassInfo.EndTime.Value, model.TimeZoneName) >= CurrentTime && m.InTime == null) || m.InTime != null : _commonService.ConvertTimeFromUTC(DateTime.UtcNow, m.ClassInfo.EndTime.Value, model.TimeZoneName) >= CurrentTime).ToList();
                    responseData = query.ToList();
                    foreach (var item in responseData)
                    {
                        if (item.InTime != null)
                        {
                            item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.InTime.ToString()), item.InTime.Value, model.TimeZoneName);
                        }
                        if (item.OutTime != null)
                        {
                            item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.OutTime.ToString()), item.OutTime.Value, model.TimeZoneName);
                        }


                        item.SecurityKey =
                            _familyStudentMapRepository.FindBy(f => f.StudentId == item.StudentId)
                                .Select(s => s.Family.SecurityKey.ToString())
                                .FirstOrDefault();
                    }
                    responseData = responseData.Where(m => m.ClassInfo.AgencyId == model.AgencyId).ToList();
                }
                else
                {
                    responseData = Mapper.Map<List<StudentSchedule>, List<StudentAttendanceViewModel>>(scheduleData.ToList());
                    responseData.ForEach(
                        e => e.AttendanceDate = model.Date == null ? DateTime.UtcNow : Convert.ToDateTime(model.Date));
                    responseData = responseData.Where(m => m.ClassInfo.AgencyId == model.AgencyId && (_commonService.ConvertTimeFromUTC(DateTime.UtcNow, m.ClassInfo.EndTime.Value, model.TimeZoneName) >= CurrentTime)).ToList();
                }
                responseResult.TotalRows = responseData.Count;
                var ResultData = responseData
                    .Skip((model.page - 1) * model.limit)
                    .Take(model.limit)
                    .ToList();
                responseResult.IsSuccess = true;
                responseResult.Content = ResultData;

                foreach (var item in responseData)
                {
                    //_incidentRepository.All.Where(m => m.IncidentParticipant.FirstOrDefault().StudentInfo.ID == item.StudentId && m.DateOfReport.Value.Day == model.Date.Value.Day && m.DateOfReport.Value.Month == model.Date.Value.Month && m.DateOfReport.Value.Year == model.Date.Value.Year).Count();
                    item.TodaysIncidenceCount = _incidentRepository.All.Count(m => m.IncidentParticipant.FirstOrDefault().StudentInfo.ID == item.StudentId && m.DateOfReport.Value.Day == model.Date.Value.Day && m.DateOfReport.Value.Month == model.Date.Value.Month && m.DateOfReport.Value.Year == model.Date.Value.Year);
                }
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }
        }
        public void CheckParentSignIn(SearchStudentAttendanceViewModel model,
             out ResponseViewModel responseResult)
        {
            responseResult = new ResponseViewModel();
            try
            {
                var parentInfo = _parentRepository.FindBy(x => x.Mobile == model.PhoneNumber && x.SecurityKey == model.PinNumber).FirstOrDefault();
                if (parentInfo == null)
                {
                    responseResult.ReturnMessage = new List<string>();
                    responseResult.ReturnStatus = false;
                    responseResult.ReturnMessage.Add("Not found");
                    return;
                }
                else
                {
                    responseResult.ReturnMessage = new List<string>();
                    responseResult.ReturnStatus = true;

                }
            }
            catch
            {

            }
        }

        public void GetStudentAttendanceListForKiosk(SearchStudentAttendanceViewModel model,
            out ResponseViewModel responseResult)
        {

            responseResult = new ResponseViewModel();

            try
            {
                string str = "";
                var predicate = PredicateBuilder.True<StudentSchedule>();
                predicate = predicate.And(p => p.ClassInfo.AgencyId == model.AgencyId);
                predicate = predicate.And(p => p.IsEnrolled == true);

                //new code added 
                List<long> studentIds = new List<long>();
                List<tblParentInfoViewModel> parentInfoList = new List<tblParentInfoViewModel>();

                var data = (tblParentInfo)_parentRepository.GetAll().Where(m => m.SecurityKey.Contains(model.PinNumber) && m.Mobile == model.PhoneNumber).FirstOrDefault();
                var parentParticipantMappingResult = _parentParticipantMappingRepository.GetAll().Where(k => k.NewParentInfoID == data.ID).ToList();
                studentIds = parentParticipantMappingResult.Select(k => k.NewParticipantInfoID.Value).ToList();

                parentInfoList = Mapper.Map<List<tblParentInfo>, List<tblParentInfoViewModel>>(parentParticipantMappingResult.GroupBy(m=>
                m.NewParentInfoID).Select(x=>
                x.FirstOrDefault().NewParentInfo).
                ToList());

                //var testInfoList = Mapper.Map<List<tblParentParticipantMapping>, List<tblParentParticipantMappingViewModel>>(result);
                if (studentIds.Count > 0)
                    predicate = predicate.And(p => studentIds.Contains(p.StudentId.Value));
                //////////////////////////////////////////////////

                if (!string.IsNullOrEmpty(model.Name))
                {
                    predicate =
                        predicate.And(
                            p =>
                                p.StudentInfo.FirstName.ToLower().Contains(model.Name.ToLower()) ||
                                p.StudentInfo.LastName.ToLower().Contains(model.Name.ToLower()));
                }
                if (model.ClassId > 0)
                {
                    predicate = predicate.And(p => p.ClassId == model.ClassId);
                }


                //--------Neha Chopra----------
                if (model.Date != null)
                {
                    DateTime? attendanceDate = model.Date;
                    DayOfWeek dw = attendanceDate.Value.DayOfWeek;
                    str = dw.ToString();
                    predicate = predicate.And(p => p.StartDate <= attendanceDate);
                }
                List<StudentAttendanceViewModel> responseData;

                var scheduleData = _studentScheduleRepository.GetAll().Where(predicate).AsExpandable();
                switch (model.order)
                {
                    case "id":
                        scheduleData = scheduleData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        scheduleData = scheduleData.OrderByDescending(m => m.ID);
                        break;
                    case "StudentName":
                        scheduleData = scheduleData.OrderBy(m => m.StudentInfo.FirstName);
                        break;
                    case "-StudentName":
                        scheduleData = scheduleData.OrderByDescending(m => m.StudentInfo.FirstName);
                        break;
                    case "AttendanceDate":
                        scheduleData = scheduleData.OrderBy(m => m.StartDate);
                        break;
                    case "-AttendanceDate":
                        scheduleData = scheduleData.OrderByDescending(m => m.StartDate);
                        break;
                }

                //------------Neha Chopra----------------
                switch (str)
                {
                    case "Monday":
                        predicate.And(x => x.ClassInfo.Mon == true);
                        break;
                    case "Tuesday":
                        predicate.And(x => x.ClassInfo.Tue == true);
                        break;
                    case "Wednesday":
                        predicate.And(x => x.ClassInfo.Wed == true);
                        break;
                    case "Thursday":
                        predicate.And(x => x.ClassInfo.Thu == true);
                        break;
                    case "Friday":
                        predicate.And(x => x.ClassInfo.Fri == true);
                        break;
                    case "Saturday":
                        predicate.And(x => x.ClassInfo.Sat == true);
                        break;
                    case "Sunday":
                        predicate.And(x => x.ClassInfo.Sun == true);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    scheduleData = scheduleData.OrderByDescending(m => m.StudentInfo.FirstName);
                var scheduleResult = Mapper.Map<List<StudentSchedule>, List<StudentScheduleViewModel>>(scheduleData.ToList());
                var attendanceData =
                    _studentAttendanceRepository.FindBy(
                        f =>
                            f.AttendanceDate.Day == model.Date.Value.Day &&
                            f.AttendanceDate.Month == model.Date.Value.Month &&
                            f.AttendanceDate.Year == model.Date.Value.Year).ToList();
                var attendanceResult = Mapper.Map<List<StudentAttendanceInfo>, List<StudentAttendanceViewModel>>(attendanceData);
                var CurrentTime = (TimeSpan)_commonService.ConvertTimeFromUTC(DateTime.UtcNow, DateTime.UtcNow.TimeOfDay, model.TimeZoneName);
                if (attendanceResult.Count > 0)
                {


                    var query = (from schedule in scheduleResult
                                 join attendance in attendanceResult
                                 on new { schedule.StudentId, schedule = schedule.ID } equals
                                 new { attendance.StudentId, schedule = attendance.StudentScheduleId.Value } into gj
                                 from subpet in gj.DefaultIfEmpty()

                                 select new StudentAttendanceViewModel
                                 {
                                     StudentName = schedule.StudentInfo.FirstName + " " + schedule.StudentInfo.LastName,
                                     ClassName = schedule.ClassInfo.ClassName,
                                     AttendanceDate = subpet?.AttendanceDate ?? DateTime.UtcNow,
                                     DropedByName =
                                        subpet == null ? null : subpet.DropedBy == null ? "" : subpet.DropedBy.FirstName + " " + subpet.DropedBy.LastName,
                                     DropedById = subpet?.DropedById,
                                     CreatedBy = subpet?.CreatedBy,
                                     CreatedDate = subpet == null ? DateTime.UtcNow : (subpet
                                     .CreatedDate ?? DateTime.UtcNow),
                                     ModifiedBy = subpet == null ? 0 : subpet.ModifiedBy,
                                     ModifiedDate = subpet == null ? DateTime.UtcNow : subpet.ModifiedDate ?? DateTime.UtcNow,
                                     IsDeleted = subpet?.IsDeleted,
                                     DropedByOtherName = subpet == null ? null : subpet.DropedByOther == null ? "" : subpet.DropedByOther.Name,
                                     ID = subpet?.ID ?? 0,
                                     ImagePath = schedule.StudentInfo.ImagePath,
                                     PickupById = subpet?.PickupById,
                                     PickupByName =
                                     subpet == null ? null : subpet.PickupBy == null ? "" : subpet.PickupBy.FirstName + " " + subpet.PickupBy.LastName,
                                     PickupByOtherName = subpet == null ? null : subpet.PickupByOtherName ?? "",
                                     PickupByOtherId = subpet?.PickupByOtherId,
                                     InTime = subpet?.InTime,
                                     OutTime = subpet?.OutTime,
                                     OnLeave = subpet?.OnLeave,
                                     StudentId = schedule.StudentInfo.ID,
                                     ClassInfo = schedule.ClassInfo,
                                     IsOthers = false,
                                     IsParents = false,
                                     OnLeaveComment = subpet == null ? "" : subpet.OnLeaveComment,
                                     CheckSecurityKey = "",
                                     DisableOnLeave = subpet == null ? false : subpet.DisableOnLeave,
                                     StudentScheduleId = schedule.ID,
                                     DropByRelationName = subpet?.DropedBy?.RelationName,
                                     PickedByRelationName = subpet?.PickupBy?.RelationName,
                                     ParentList = parentInfoList
                                 });
                    responseData = query.ToList();
                    foreach (var item in responseData)
                    {
                        if (item.InTime != null)
                        {
                            item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.InTime.ToString()), item.InTime.Value, model.TimeZoneName);
                        }
                        if (item.OutTime != null)
                        {
                            item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.OutTime.ToString()), item.OutTime.Value, model.TimeZoneName);
                        }


                        item.SecurityKey =
                            _familyStudentMapRepository.FindBy(f => f.StudentId == item.StudentId)
                                .Select(s => s.Family.SecurityKey.ToString())
                                .FirstOrDefault();
                    }
                    responseData = responseData.Where(m => m.ClassInfo.AgencyId == model.AgencyId).ToList();
                }
                else
                {
                    responseData = Mapper.Map<List<StudentSchedule>, List<StudentAttendanceViewModel>>(scheduleData.ToList());
                    responseData.ForEach(e =>
                    {
                        e.AttendanceDate = model.Date == null ? DateTime.UtcNow : Convert.ToDateTime(model.Date);
                            //added new code
                            e.ParentList = parentInfoList;
                            ///////////
                        });

                    //  responseData = responseData.Where(m => m.ClassInfo.AgencyId == model.AgencyId && (_commonService.ConvertTimeFromUTC(DateTime.UtcNow, m.ClassInfo.EndTime.Value, model.TimeZoneName) >= CurrentTime)).ToList();
                }
                responseResult.TotalRows = responseData.Count;
                var ResultData = responseData
                    .Skip((model.page - 1) * model.limit)
                    .Take(model.limit)
                    .ToList();
                ResultData.ForEach(
                    x =>
                    {
                        if (x.InTime == null)
                            x.SignInChecked = false;
                        else
                            x.SignInChecked = true;

                        if (x.OutTime == null)
                            x.SignOutChecked = false;
                        else
                            x.SignOutChecked = true;
                    });
                responseResult.IsSuccess = true;
                responseResult.Content = ResultData;

                
                foreach (var item in responseData)
                {
                    //_incidentRepository.All.Where(m => m.IncidentParticipant.FirstOrDefault().StudentInfo.ID == item.StudentId && m.DateOfReport.Value.Day == model.Date.Value.Day && m.DateOfReport.Value.Month == model.Date.Value.Month && m.DateOfReport.Value.Year == model.Date.Value.Year).Count();
                    item.TodaysIncidenceCount = _incidentRepository.All.Count(m => m.IncidentParticipant.FirstOrDefault().StudentInfo.ID == item.StudentId && m.DateOfReport.Value.Day == model.Date.Value.Day && m.DateOfReport.Value.Month == model.Date.Value.Month && m.DateOfReport.Value.Year == model.Date.Value.Year);
                }
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }

        }


        public void GetStudentAttendanceListTest(SearchStudentAttendanceViewModel model,
            out ResponseViewModel responseResult)
        {
            responseResult = new ResponseViewModel();
            try
            {
                string str = "";
                var predicate = PredicateBuilder.True<StudentSchedule>();
                predicate = predicate.And(p => p.ClassInfo.AgencyId == model.AgencyId);
                predicate = predicate.And(p => p.IsEnrolled == true);
                if (!string.IsNullOrEmpty(model.Name))
                {
                    predicate =
                        predicate.And(
                            p =>
                                p.StudentInfo.FirstName.ToLower().Contains(model.Name.ToLower()) ||
                                p.StudentInfo.LastName.ToLower().Contains(model.Name.ToLower()));
                }
                if (model.ClassId > 0)
                {
                    predicate = predicate.And(p => p.ClassId == model.ClassId);
                }

                //--------Neha Chopra----------
                if (model.Date != null)
                {
                    DateTime? AttendanceDate = model.Date;
                    DayOfWeek dw = AttendanceDate.Value.DayOfWeek;
                    str = dw.ToString();
                    predicate = predicate.And(p => p.StartDate <= AttendanceDate);
                }
                List<StudentAttendanceViewModel> responseData;
                var scheduleData = _studentScheduleRepository.GetAll().Where(predicate).AsExpandable();
                switch (model.order)
                {
                    case "id":
                        scheduleData = scheduleData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        scheduleData = scheduleData.OrderByDescending(m => m.ID);
                        break;
                    case "StudentName":
                        scheduleData = scheduleData.OrderBy(m => m.StudentInfo.FirstName);
                        break;
                    case "-StudentName":
                        scheduleData = scheduleData.OrderByDescending(m => m.StudentInfo.FirstName);
                        break;
                    case "AttendanceDate":
                        scheduleData = scheduleData.OrderBy(m => m.StartDate);
                        break;
                    case "-AttendanceDate":
                        scheduleData = scheduleData.OrderByDescending(m => m.StartDate);
                        break;
                }

                //------------Neha Chopra----------------
                switch (str)
                {
                    case "Monday":
                        predicate.And(x => x.ClassInfo.Mon == true);
                        break;
                    case "Tuesday":
                        predicate.And(x => x.ClassInfo.Tue == true);
                        break;
                    case "Wednesday":
                        predicate.And(x => x.ClassInfo.Wed == true);
                        break;
                    case "Thursday":
                        predicate.And(x => x.ClassInfo.Thu == true);
                        break;
                    case "Friday":
                        predicate.And(x => x.ClassInfo.Fri == true);
                        break;
                    case "Saturday":
                        predicate.And(x => x.ClassInfo.Sat == true);
                        break;
                    case "Sunday":
                        predicate.And(x => x.ClassInfo.Sun == true);
                        break;
                }


                if (string.IsNullOrEmpty(model.order))
                    scheduleData = scheduleData.OrderByDescending(m => m.StudentInfo.FirstName);
                var scheduleResult = Mapper.Map<List<StudentSchedule>, List<StudentScheduleViewModel>>(scheduleData.ToList());
                var attendanceData =
                    _studentAttendanceRepository.FindBy(
                        f =>
                            f.AttendanceDate.Day == model.Date.Value.Day &&
                            f.AttendanceDate.Month == model.Date.Value.Month &&
                            f.AttendanceDate.Year == model.Date.Value.Year).ToList();
                //long totalCount = scheduleData.Count();

                //var attendanceDataNew = attendanceData.ToList();
                var attendanceResult = Mapper.Map<List<StudentAttendanceInfo>, List<StudentAttendanceViewModel>>(attendanceData);
                var CurrentTime = (TimeSpan)_commonService.ConvertTimeFromUTC(DateTime.UtcNow, DateTime.UtcNow.TimeOfDay, model.TimeZoneName);
                if (attendanceResult.Count > 0)
                {


                    var query = (from schedule in scheduleResult
                                 join attendance in attendanceResult
                                 on new { schedule.StudentId, schedule = schedule.ID } equals
                                 new { attendance.StudentId, schedule = attendance.StudentScheduleId.Value } into gj
                                 from subpet in gj.DefaultIfEmpty()

                                 select new StudentAttendanceViewModel
                                 {
                                     StudentName = schedule.StudentInfo.FirstName + " " + schedule.StudentInfo.LastName,
                                     ClassName = schedule.ClassInfo.ClassName,
                                     AttendanceDate = subpet?.AttendanceDate ?? DateTime.UtcNow,
                                     DropedByName =
                                        subpet == null ? null : subpet.DropedBy == null ? "" : subpet.DropedBy.FirstName + " " + subpet.DropedBy.LastName,
                                     DropedById = subpet?.DropedById,
                                     CreatedBy = subpet?.CreatedBy,
                                     CreatedDate = subpet == null ? DateTime.UtcNow : (subpet
                                     .CreatedDate ?? DateTime.UtcNow),
                                     ModifiedBy = subpet == null ? 0 : subpet.ModifiedBy,
                                     ModifiedDate = subpet == null ? DateTime.UtcNow : subpet.ModifiedDate ?? DateTime.UtcNow,
                                     IsDeleted = subpet?.IsDeleted,
                                     DropedByOtherName = subpet == null ? null : subpet.DropedByOther == null ? "" : subpet.DropedByOther.Name,
                                     ID = subpet?.ID ?? 0,
                                     ImagePath = schedule.StudentInfo.ImagePath,
                                     PickupById = subpet?.PickupById,
                                     PickupByName =
                                         subpet == null ? null : subpet.PickupBy == null ? "" : subpet.PickupBy.FirstName + " " + subpet.PickupBy.LastName,
                                     PickupByOtherName = subpet == null ? null : subpet.PickupByOtherName ?? "",
                                     PickupByOtherId = subpet?.PickupByOtherId,
                                     InTime = subpet?.InTime,
                                     OutTime = subpet?.OutTime,
                                     OnLeave = subpet?.OnLeave,
                                     StudentId = schedule.StudentInfo.ID,
                                     ClassInfo = schedule.ClassInfo,
                                     IsOthers = false,
                                     IsParents = false,
                                     OnLeaveComment = subpet == null ? "" : subpet.OnLeaveComment,
                                     CheckSecurityKey = "",
                                     DisableOnLeave = subpet == null ? false : subpet.DisableOnLeave,
                                     StudentScheduleId = schedule.ID,
                                     DropByRelationName = subpet?.DropedBy?.RelationName,
                                     PickedByRelationName = subpet?.PickupBy?.RelationName,
                                 }).Where(m => m.OutTime == null ? (_commonService.ConvertTimeFromUTC(DateTime.UtcNow, m.ClassInfo.EndTime.Value, model.TimeZoneName) >= CurrentTime && m.InTime == null) || m.InTime != null : _commonService.ConvertTimeFromUTC(DateTime.UtcNow, m.ClassInfo.EndTime.Value, model.TimeZoneName) >= CurrentTime).ToList();
                    responseData = query.ToList();
                    //long totalCount = responseData.Count();
                    foreach (var item in responseData)
                    {
                        if (item.InTime != null)
                        {
                            item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.InTime.ToString()), item.InTime.Value, model.TimeZoneName);
                        }
                        if (item.OutTime != null)
                        {
                            item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.OutTime.ToString()), item.OutTime.Value, model.TimeZoneName);
                        }


                        item.SecurityKey =
                            _familyStudentMapRepository.FindBy(f => f.StudentId == item.StudentId)
                                .Select(s => s.Family.SecurityKey.ToString())
                                .FirstOrDefault();
                    }
                    responseData = responseData.Where(m => m.ClassInfo.AgencyId == model.AgencyId).ToList();
                }
                else
                {
                    responseData = Mapper.Map<List<StudentSchedule>, List<StudentAttendanceViewModel>>(scheduleData.ToList());
                    responseData.ForEach(
                        e => e.AttendanceDate = model.Date == null ? DateTime.UtcNow : Convert.ToDateTime(model.Date));
                    responseData = responseData.Where(m => m.ClassInfo.AgencyId == model.AgencyId && (_commonService.ConvertTimeFromUTC(DateTime.UtcNow, m.ClassInfo.EndTime.Value, model.TimeZoneName) >= CurrentTime)).ToList();
                }
                responseResult.TotalRows = responseData.Count;
                var resultData = responseData
                    .Skip((model.page - 1) * model.limit)
                    .Take(model.limit)
                    .ToList();
                responseResult.IsSuccess = true;
                responseResult.Content = resultData;

                foreach (var item in responseData)
                {
                    item.TodaysIncidenceCount = _incidentRepository.All.Count(m => m.IncidentParticipant.FirstOrDefault().StudentInfo.ID == item.StudentId && m.DateOfReport.Value.Day == model.Date.Value.Day && m.DateOfReport.Value.Month == model.Date.Value.Month && m.DateOfReport.Value.Year == model.Date.Value.Year);
                }
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }
        }
        /// <summary>
        ///     Adds the student attendance.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddStudentAttendance(StudentAttendanceViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var attendanceInfo = Mapper.Map<StudentAttendanceViewModel, StudentAttendanceInfo>(model);
                _studentAttendanceRepository.Add(attendanceInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Attendance added successfully");
                transaction.ID = attendanceInfo.ID;
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        /// <summary>
        ///     Updates the student attendance.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo">The response information.</param>
        public void UpdateStudentAttendance(StudentAttendanceViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var studentDetail =
                    _studentAttendanceRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapStudentViewModelInfo = Mapper.Map<StudentAttendanceViewModel, StudentAttendanceInfo>(model);
                _studentAttendanceRepository.Edit(studentDetail, mapStudentViewModelInfo);
                _unitOfWork.Commit();
                responseInfo.ReturnStatus = true;
                responseInfo.ReturnMessage.Add("Staff attendance successfully updated at " +
                                               DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Deletes the student attendance by identifier.
        /// </summary>
        /// <param name="studentAttendanceId">The student attendance identifier.</param>
        /// <param name="responseInfo">The response information.</param>
        public void DeleteStudentAttendanceById(long studentAttendanceId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var staffDetails =
                    _studentAttendanceRepository.FindBy(m => m.ID == studentAttendanceId && m.IsDeleted == false)
                        .FirstOrDefault();
                if (staffDetails != null && staffDetails.ID > 0)
                {
                    staffDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Staff attendance deleted successfully deleted at " +
                                                   DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Gets the parent list by student identifier.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public ResponseViewModel GetParentListByStudentId(IDViewModel model)
        {
            var responseDate = new ResponseViewModel();
            try
            {
                var parentDetail =
                    _familyRepository.FindBy(m => m.FamilyStudentMap.Any(w => w.StudentId == model.ID)).FirstOrDefault();
                var abc = parentDetail?.ParentInfo.ToList();
                var mapFamilyList = Mapper.Map<List<ParentInfo>, List<ParentInfoViewModel>>(abc);
                responseDate.IsSuccess = true;
                responseDate.Content = mapFamilyList;
                responseDate.Message = "Data Retrieved Successfully";
            }
            catch (Exception)
            {
                responseDate.IsSuccess = false;
                responseDate.Message = "Please try again";
            }
            return responseDate;
        }

        public ResponseViewModel MarkDropByAttendance(StudentAttendanceViewModel model)
        {
            var responseDate = new ResponseViewModel();
            try
            {

                var attendanceObj = Mapper.Map<StudentAttendanceViewModel, StudentAttendanceInfo>(model);
                attendanceObj.AttendanceDate = model.AttendanceDate;
                attendanceObj.CreatedBy = 1;
                attendanceObj.CreatedDate = DateTime.UtcNow;
                attendanceObj.IsDeleted = false;
                if (model.OnLeave != true)
                    attendanceObj.InTime = DateTime.UtcNow.TimeOfDay;
                _studentAttendanceRepository.Add(attendanceObj);
                _unitOfWork.Commit();
                responseDate.IsSuccess = true;
                responseDate.Message = "Attendance marked Successfully";
            }
            catch (Exception)
            {
                responseDate.IsSuccess = false;
                responseDate.Message = "Please try again";

            }
            return responseDate;
        }
        public ResponseViewModel MarkDropByAttendanceKiosk(List<StudentAttendanceViewModel> model)
        {
            var responseDate = new ResponseViewModel();
            try
            {
                foreach (var item in model)
                {
                    if (item.SignInChecked == true && item.SignOutChecked == false&&item.InTime==null)
                    {
                        
                        var attendanceObj = Mapper.Map<StudentAttendanceViewModel, StudentAttendanceInfo>(item);
                        attendanceObj.AttendanceDate = item.AttendanceDate;
                        attendanceObj.CreatedBy = 1;
                        attendanceObj.CreatedDate = DateTime.UtcNow;
                        attendanceObj.IsDeleted = false;
                        if (item.OnLeave != true)
                            attendanceObj.InTime = DateTime.UtcNow.TimeOfDay;
                        _studentAttendanceRepository.Add(attendanceObj);
                        _unitOfWork.Commit();
                        responseDate.IsSuccess = true;
                        responseDate.Message = "Attendance marked Successfully";

                    }
                    else if (item.SignOutChecked == false && item.SignInChecked == false && item.OutTime==null&& item.InTime != null)
                    {
                        var oldObj = _studentAttendanceRepository.GetSingle(item.ID);
                        var newObj = Mapper.Map<StudentAttendanceViewModel, StudentAttendanceInfo>(item);
                        newObj.OutTime = DateTime.UtcNow.TimeOfDay;
                        newObj.PickupById = item.PickupById;
                        newObj.ModifiedBy = 1;
                        newObj.ModifiedDate = DateTime.UtcNow;
                        newObj.PickupByOtherName = item.PickupByOtherName;
                        _studentAttendanceRepository.Edit(oldObj, newObj);
                        _unitOfWork.Commit();

                    }
                    responseDate.IsSuccess = true;
                    responseDate.Message = "Attendance marked Successfully";


                }


            }
            catch (Exception ex)
            {
                responseDate.IsSuccess = false;
                responseDate.Message = "Please try again";

            }
            return responseDate;
        }
        public ResponseViewModel MarkPickUpByAttendance(StudentAttendanceViewModel model)
        {
            var responseDate = new ResponseViewModel();
            try
            {
                var oldObj = _studentAttendanceRepository.GetSingle(model.ID);
                Mapper.Map<StudentAttendanceViewModel, StudentAttendanceInfo>(model);
                oldObj.OutTime = DateTime.UtcNow.TimeOfDay;
                oldObj.PickupById = model.PickupById;
                oldObj.ModifiedBy = 1;
                oldObj.ModifiedDate = DateTime.UtcNow;
                oldObj.PickupByOtherName = model.PickupByOtherName;
                _studentAttendanceRepository.Edit(oldObj, oldObj);
                _unitOfWork.Commit();
                responseDate.IsSuccess = true;
                responseDate.Message = "Attendance marked Successfully";

            }
            catch (Exception)
            {
                responseDate.IsSuccess = false;
                responseDate.Message = "Please try again";
            }
            return responseDate;
        }
        public ResponseViewModel MarkPickUpByAttendanceKiosk(List<StudentAttendanceViewModel> model)
        {
            var responseDate = new ResponseViewModel();
            try
            {
                foreach (var item in model)
                {
                    var oldObj = _studentAttendanceRepository.GetSingle(item.ID);
                    Mapper.Map<List<StudentAttendanceViewModel>, List<StudentAttendanceInfo>>(model);
                    oldObj.OutTime = DateTime.UtcNow.TimeOfDay;
                    oldObj.PickupById = item.PickupById;
                    oldObj.ModifiedBy = 1;
                    oldObj.ModifiedDate = DateTime.UtcNow;
                    oldObj.PickupByOtherName = item.PickupByOtherName;
                    _studentAttendanceRepository.Edit(oldObj, oldObj);
                    _unitOfWork.Commit();

                }
                responseDate.IsSuccess = true;
                responseDate.Message = "Attendance marked Successfully";

            }
            catch (Exception)
            {
                responseDate.IsSuccess = false;
                responseDate.Message = "Please try again";
            }
            return responseDate;
        }

        public ResponseViewModel GetAttendanceHistory(SearchAttendanceHistoryViewModel model)
        {
            var responseResult = new ResponseViewModel();
            try
            {
                if (model.FromDate != null)
                {
                    model.FromDate = new DateTime(model.FromDate.Value.Year, model.FromDate.Value.Month,
                        model.FromDate.Value.Day, 0, 0, 0);
                    if (model.ToDate != null)
                    {
                        model.ToDate = new DateTime(model.ToDate.Value.Year, model.ToDate.Value.Month,
                            model.ToDate.Value.Day,
                            23, 59, 59);
                        var predicate = PredicateBuilder.True<StudentAttendanceInfo>();
                        if (model.AgencyId != null || model.AgencyId > 0)
                            predicate = predicate.And(p => p.AgencyId == model.AgencyId); //changed
                        if (model.FamilyId > 0)
                        {
                            var studentIds = _familyStudentMapRepository.All.Where(m => m.FamilyId == model.FamilyId).Select(m => m.StudentId).ToList();
                            predicate = predicate.And(p => studentIds.Contains(p.StudentId.Value)); //changed
                        }
                        if (!string.IsNullOrEmpty(model.StudentName))
                        {
                            predicate =
                                predicate.And(
                                    p =>
                                        p.Student.FirstName.Contains(model.StudentName) ||
                                        p.Student.LastName.Contains(model.StudentName));
                        }
                        if (model.ClassId != null && model.ClassId > 0)
                            predicate = predicate.And(p => p.StudentSchedule.ClassId == model.ClassId); //changed
                        var resultData = _studentAttendanceRepository.All.AsExpandable().Where(predicate);

                        if (model.FromDate != null && model.ToDate != null)
                        {
                            resultData =
                                resultData.Where(
                                    p => p.AttendanceDate >= model.FromDate && p.AttendanceDate <= model.ToDate);
                        }
                        long totalCount = resultData.Count();
                        switch (model.order)
                        {
                            case "id":
                                resultData = resultData.OrderBy(m => m.ID);
                                break;
                            case "-id":
                                resultData = resultData.OrderByDescending(m => m.ID);
                                break;
                            case "StudentName":
                                resultData = resultData.OrderBy(m => m.Student.FirstName);
                                break;
                            case "-StudentName":
                                resultData = resultData.OrderByDescending(m => m.Student.FirstName);
                                break;
                            case "ClassName":
                                resultData = resultData.OrderBy(m => m.StudentSchedule.ClassInfo.ClassName);
                                break;
                            case "-ClassName":
                                resultData = resultData.OrderByDescending(m => m.StudentSchedule.ClassInfo.ClassName);
                                break;
                            case "AttendanceDate":
                                resultData = resultData.OrderBy(m => m.AttendanceDate);
                                break;
                            case "-AttendanceDate":
                                resultData = resultData.OrderByDescending(m => m.AttendanceDate);
                                break;
                        }

                        if (string.IsNullOrEmpty(model.order))
                            resultData = resultData.OrderBy(m => m.Student.FirstName);
                        var returnData = resultData.AsExpandable()
                            .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();

                        var attendanceList =
                            Mapper.Map<List<StudentAttendanceInfo>, List<StudentAttendanceViewModel>>(returnData);

                        foreach (var item in attendanceList)
                        {
                            if (item.AttendanceDate != null)
                            {
                                item.AttendanceDate = (DateTime)_commonService.ConvertFromUTC(Convert.ToDateTime(item.AttendanceDate), model.TimeZoneName);
                            }
                            if (item.InTime != null)
                            {
                                item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.InTime.ToString()), item.InTime.Value, model.TimeZoneName);
                            }
                            if (item.OutTime != null)
                            {
                                item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.OutTime.ToString()), item.OutTime.Value, model.TimeZoneName);
                            }
                            var obj = returnData.FirstOrDefault(w => w.ID == item.ID);
                            if (obj != null)
                            {
                                item.DropedByName = obj.DropedById == null
                                    ? ""
                                    : obj.DropedBy.FirstName + ' ' + obj.DropedBy.LastName;
                                item.PickupByName = obj.PickupById == null
                                    ? ""
                                    : _parentRepository.FindBy(f => f.ID == obj.PickupById)
                                        .Select(s => s.FirstName.ToString() + " " + s.LastName.ToString())
                                        .FirstOrDefault();

                            }
                            item.DropByRelationName = item.DropedBy?.RelationName;
                            item.PickedByRelationName = item.PickupBy?.RelationName;
                        }
                        responseResult.TotalRows = totalCount;
                        responseResult.IsSuccess = true;
                        responseResult.Content = attendanceList;
                    }
                }
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }
            return responseResult;
        }
        public ResponseViewModel GetStudentAttendanceHistory(SearchStudentAttendanceHistoryViewModel model)
        {
            var responseResult = new ResponseViewModel();
            try
            {
                if (model.FromDate != null)
                {

                    model.FromDate = new DateTime(model.FromDate.Value.Year, model.FromDate.Value.Month,
                        model.FromDate.Value.Day, 0, 0, 0);
                    if (model.ToDate != null)
                    {
                        model.ToDate = new DateTime(model.ToDate.Value.Year, model.ToDate.Value.Month,
                            model.ToDate.Value.Day,
                            23, 59, 59);
                        var predicate = PredicateBuilder.True<StudentAttendanceInfo>();
                        predicate = predicate.And(p => p.AgencyId == model.AgencyId); //changed
                        if (!string.IsNullOrEmpty(model.StudentName))
                        {
                            predicate =
                                predicate.And(
                                    p =>
                                        p.Student.FirstName.Contains(model.StudentName) ||
                                        p.Student.LastName.Contains(model.StudentName));
                        }
                        if (model.ClassId > 0)
                        {
                            predicate =
                                predicate.And(p => p.StudentSchedule.ClassId == model.ClassId);
                        }
                        var resultData = _studentAttendanceRepository.GetAll().AsExpandable().Where(predicate);
                        if (model.StudentId != null && model.StudentId > 0)
                        {
                            resultData =
                              resultData.Where(
                                  p => p.StudentId == model.StudentId);
                        }
                        if (model.FromDate != null && model.ToDate != null)
                        {
                            resultData =
                                resultData.Where(
                                    p => p.AttendanceDate >= model.FromDate && p.AttendanceDate <= model.ToDate);
                        }

                        long totalCount = resultData.Count();
                        switch (model.order)
                        {
                            case "id":
                                resultData = resultData.OrderBy(m => m.ID);
                                break;
                            case "-id":
                                resultData = resultData.OrderByDescending(m => m.ID);
                                break;
                            case "StudentName":
                                resultData = resultData.OrderBy(m => m.Student.FirstName);
                                break;
                            case "-StudentName":
                                resultData = resultData.OrderByDescending(m => m.Student.FirstName);
                                break;
                            case "ClassName":
                                resultData = resultData.OrderBy(m => m.StudentSchedule.ClassInfo.ClassName);
                                break;
                            case "-ClassName":
                                resultData = resultData.OrderByDescending(m => m.StudentSchedule.ClassInfo.ClassName);
                                break;
                            case "AttendanceDate":
                                resultData = resultData.OrderBy(m => m.AttendanceDate);
                                break;
                            case "-AttendanceDate":
                                resultData = resultData.OrderByDescending(m => m.AttendanceDate);
                                break;
                        }
                        if (string.IsNullOrEmpty(model.order))
                            resultData = resultData.OrderBy(m => m.Student.FirstName);
                        var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                            .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();

                        var attendanceList =
                            Mapper.Map<List<StudentAttendanceInfo>, List<StudentAttendanceViewModel>>(returnData);
                        foreach (var item in attendanceList)
                        {
                            if (item.AttendanceDate != null)
                            {
                                item.AttendanceDate = (DateTime)_commonService.ConvertFromUTC(Convert.ToDateTime(item.AttendanceDate), model.TimeZoneName);
                            }
                            if (item.InTime != null)
                            {
                                item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.InTime.ToString()), item.InTime.Value, model.TimeZoneName);
                            }
                            if (item.OutTime != null)
                            {
                                item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.OutTime.ToString()), item.OutTime.Value, model.TimeZoneName);
                            }
                            var obj = returnData.FirstOrDefault(w => w.ID == item.ID);
                            if (obj != null)
                            {
                                item.DropedByName = obj.DropedById == null
                                    ? ""
                                    : obj.DropedBy.FirstName + ' ' + obj.DropedBy.LastName;
                                item.PickupByName = obj.PickupById == null
                                    ? ""
                                    : _parentRepository.FindBy(f => f.ID == obj.PickupById)
                                        .Select(s => s.FirstName.ToString() + " " + s.LastName.ToString())
                                        .FirstOrDefault();
                                item.DropByRelationName = item.DropedBy?.RelationName;
                                item.PickedByRelationName = item.PickupBy?.RelationName;
                            }
                        }
                        responseResult.TotalRows = totalCount;
                        responseResult.IsSuccess = true;
                        responseResult.Content = attendanceList;
                    }
                }
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }
            return responseResult;
        }
        public void GetStudentFailedSignOutList(SearchStudentAttendanceViewModel model,
          out ResponseViewModel responseResult)
        {
            responseResult = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<StudentAttendanceInfo>();
                if (model.AgencyId > 0)
                {
                    predicate = predicate.And(f => f.AgencyId == model.AgencyId && (f.AttendanceDate.Day == model.Date.Value.Day &&
                          f.AttendanceDate.Month == model.Date.Value.Month &&
                          f.AttendanceDate.Year == model.Date.Value.Year) && (f.OutTime == null) && f.OnLeave == false);

                }
                if (!string.IsNullOrEmpty(model.Name))
                {
                    predicate =
                        predicate.And(
                            p =>
                                p.Student.FirstName.ToLower().Contains(model.Name.ToLower()) ||
                                p.Student.LastName.ToLower().Contains(model.Name.ToLower()));
                }
                if (model.ClassId > 0)
                {
                    predicate = predicate.And(p => p.StudentSchedule.ClassId == model.ClassId);
                }
                List<StudentAttendanceViewModel> responseData;

                var scheduleData = _studentAttendanceRepository.GetAll().AsExpandable().Where(predicate);
                switch (model.order)
                {
                    case "id":
                        scheduleData = scheduleData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        scheduleData = scheduleData.OrderByDescending(m => m.ID);
                        break;
                    case "StudentName":
                        scheduleData = scheduleData.OrderBy(m => m.Student.FirstName);
                        break;
                    case "-StudentName":
                        scheduleData = scheduleData.OrderByDescending(m => m.Student.FirstName);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    scheduleData = scheduleData.OrderByDescending(m => m.Student.FirstName);

                long totalCount = scheduleData.ToList().Count();
                if (scheduleData.Count() > 0)
                {

                    var query = (from schedule in scheduleData
                                 select new StudentAttendanceViewModel
                                 {
                                     StudentName = schedule.Student.FirstName + " " + schedule.Student.LastName,
                                     ClassName = schedule.StudentSchedule.ClassInfo.ClassName,
                                     AttendanceDate = schedule.AttendanceDate == null ? DateTime.UtcNow : schedule.AttendanceDate,
                                     DropedByName = schedule.DropedBy == null ? null : schedule.DropedBy == null ? "" : schedule.DropedBy.FirstName + " " + schedule.DropedBy.LastName,
                                     DropedById = schedule.DropedById == null ? null : schedule.DropedById,
                                     CreatedBy = schedule.CreatedBy == null ? null : schedule.CreatedBy,
                                     CreatedDate = schedule.CreatedDate == null ? DateTime.UtcNow : (schedule.CreatedDate ?? DateTime.UtcNow),
                                     ModifiedBy = schedule.ModifiedBy == null ? 0 : schedule.ModifiedBy,
                                     ModifiedDate = schedule.ModifiedDate == null ? DateTime.UtcNow : schedule.ModifiedDate ?? DateTime.UtcNow,
                                     IsDeleted = schedule.IsDeleted == null ? null : schedule.IsDeleted,
                                     DropedByOtherId = schedule.DropedByOtherId == null ? 0 : (schedule.DropedByOtherId ?? 0),
                                     DropedByOtherName = schedule.DropedByOther == null ? null : schedule.DropedByOther == null ? "" : schedule.DropedByOther.Name,
                                     ID = schedule.ID,
                                     ImagePath = schedule.Student.ImagePath,
                                     PickupById = schedule.PickupById == null ? 0 : schedule.PickupById ?? 0,
                                     PickupByName = schedule.PickupBy == null ? null : schedule.PickupBy == null ? "" : schedule.PickupBy.FirstName + " " + schedule.PickupBy.LastName,
                                     PickupByOtherName = schedule.PickupByOtherName == null ? null : schedule.PickupByOtherName ?? "",
                                     //PickupByOtherId = subpet.PickupByOtherId == null ? 0 : subpet.PickupByOtherId,
                                     //PickupByOtherName = subpet.PickupByOtherName !=null? subpet.PickupByOtherName.Name:"",
                                     InTime = schedule.InTime == null ? null : schedule.InTime,
                                     OutTime = schedule.OutTime == null ? null : schedule.OutTime,
                                     OnLeave = schedule.OnLeave == null ? null : schedule.OnLeave,
                                     StudentId = schedule.StudentId,
                                     DropByRelationName = schedule.DropedBy == null ? null : schedule.DropedBy.RelationType == null ? null : schedule.DropedBy.RelationType.Name,
                                     PickedByRelationName = schedule.PickupBy == null ? null : schedule.PickupBy.RelationType == null ? null : schedule.PickupBy.RelationType.Name
                                 }).ToList();
                    responseData = query.Where(x => x.OutTime == null).ToList();
                    responseData = responseData.Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                    foreach (var item in responseData)
                    {
                        if (item.AttendanceDate != null)
                        {
                            item.AttendanceDate = (DateTime)_commonService.ConvertFromUTC(Convert.ToDateTime(item.AttendanceDate), model.TimeZoneName);
                        }
                        if (item.InTime != null)
                        {
                            item.InTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.InTime.ToString()), item.InTime.Value, model.TimeZoneName);
                        }
                        if (item.OutTime != null)
                        {
                            item.OutTime = _commonService.ConvertTimeFromUTC(Convert.ToDateTime(item.OutTime.ToString()), item.OutTime.Value, model.TimeZoneName);
                        }
                        item.SecurityKey =
                            _familyStudentMapRepository.FindBy(f => f.StudentId == item.StudentId)
                                .Select(s => s.Family.SecurityKey.ToString())
                                .FirstOrDefault();
                    }

                }
                else
                {
                    responseData = new List<StudentAttendanceViewModel>();
                }
                responseResult.TotalRows = totalCount;
                responseResult.IsSuccess = true;
                responseResult.Content = responseData;
            }
            catch (Exception ex)
            {
                responseResult.ReturnMessage = new List<string>();
                responseResult.ReturnStatus = false;
                responseResult.ReturnMessage.Add(ex.Message);
            }
        }
        public ResponseViewModel CheckAttendanceForClass(StudentAttendanceViewModel model)
        {
            var responseResult = new ResponseViewModel();
            try
            {
                var date = new DateTime(model.AttendanceDate.Date.Ticks);
                var result = _studentAttendanceRepository.FindBy(m => m.StudentId == model.StudentId &&
                m.AttendanceDate.Day == model.AttendanceDate.Day &&
                m.AttendanceDate.Month == model.AttendanceDate.Month &&
                m.AttendanceDate.Year == model.AttendanceDate.Year
                && m.InTime != null && m.OutTime == null).ToList();
                if (result.Any())
                {
                    responseResult.IsSuccess = true;
                    responseResult.Message = "You have already marked attendance for another class";
                }
                else
                {
                    responseResult.IsSuccess = false;
                }
            }
            catch (Exception)
            {
                responseResult.IsSuccess = false;
                responseResult.Message = "Please try again";

            }
            return responseResult;
        }
        public ResponseViewModel CheckAttendanceForClassKiosk(List<StudentAttendanceViewModel> model)
        {
            var responseResult = new ResponseViewModel();
            try
            {
                foreach (var item in model)
                {
                    var date = new DateTime(item.AttendanceDate.Date.Ticks);
                    var result = _studentAttendanceRepository.FindBy(m => m.StudentId == item.StudentId &&
                    m.AttendanceDate.Day == item.AttendanceDate.Day &&
                    m.AttendanceDate.Month == item.AttendanceDate.Month &&
                    m.AttendanceDate.Year == item.AttendanceDate.Year
                    && m.InTime != null && m.OutTime == null).ToList();
                    if (result.Any())
                    {
                        responseResult.IsSuccess = true;
                        responseResult.Message = "You have already marked attendance for another class";
                    }
                    else
                    {
                        responseResult.IsSuccess = false;
                    }
                }

            }
            catch (Exception)
            {
                responseResult.IsSuccess = false;
                responseResult.Message = "Please try again";

            }
            return responseResult;
        }
    }
}