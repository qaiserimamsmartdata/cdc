﻿using System;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Agency;
using CDC.Services.Abstract.Agency;
using CDC.ViewModel.Agency;
using CDC.ViewModel.Common;

namespace CDC.Services.Services.Agency
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="IAgencyService" />
    public class AgencyService : IAgencyService
    {
        private readonly IEntityBaseRepository<AgencyInfo> _agencyRepository;
        
        private readonly IUnitOfWork _unitOfWork;
        
        public AgencyService(
          IEntityBaseRepository<AgencyInfo> agencyRepository, IUnitOfWork unitOfWork
          )
        {
            _agencyRepository = agencyRepository;
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Gets the agency by identifier.
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <returns></returns>
        public AgencyViewModel GetAgencyById(long agencyId)
        {
            var resultData = _agencyRepository.GetSingle(agencyId);
            var returnData = Mapper.Map<AgencyInfo, AgencyViewModel>(resultData);
            return returnData;
        }
        public ResponseViewModel SetIsLoggedFirstTime(long agencyId)
        {
            ResponseViewModel responseViewModelInfo = new ResponseViewModel();
            try
            {
                var agencyDetailOld = _agencyRepository.FindBy(m => m.ID == agencyId && m.IsDeleted == false).FirstOrDefault();
                if (agencyDetailOld != null)
                {
                    agencyDetailOld.IsLoggedFirstTime = true;
                    _agencyRepository.Edit(agencyDetailOld, agencyDetailOld);
                    _unitOfWork.Commit();
                    responseViewModelInfo.IsSuccess = true;
                }
                else
                    responseViewModelInfo.IsSuccess = false;

            }
            catch (Exception)
            {
                responseViewModelInfo.IsSuccess = false;
            }
            return responseViewModelInfo;
        }

        //public List<AgencyViewModel> GetAllAgencies(SearchClassViewModel model)
        //{

        //    var predicate = PredicateBuilder.True<AgencyInfo>();

        //    //if(!string.IsNullOrEmpty(model.ClassName))
        //    //predicate = predicate.And(p => p.AgencyName.Contains(model.ClassName));
        //    //if (model.SessionId > 0)
        //    //    predicate = predicate.And(p => p.SessionId == model.SessionId);
        //    //if (model.StatusId > 0)
        //    //    predicate = predicate.And(p => p.StatusId == model.StatusId);
        //    //if (model.CategoryId > 0)
        //    //    predicate = predicate.And(p => p.CategoryId == model.CategoryId);
        //    ////if (model.ClassStartDate != null)
        //    ////    predicate = predicate.And(p => p.ClassStartDate == model.ClassStartDate);
        //    ////if (model.ClassEndDate != null)
        //    ////    predicate = predicate.And(p => p.ClassEndDate == model.ClassEndDate);
        //    ////if (model.GenderId > 0)
        //    ////    predicate = predicate.Or(p => p.GenderId == model.GenderId);
        //    //var resultData = _classRepository.GetAll();
        //    //Int64 TotalCount = resultData.Count();
        //    //if (model.ClassStartDate != null && model.ClassEndDate != null)
        //    //{
        //    //    resultData = resultData.Where(m => m.ClassStartDate >= model.ClassStartDate && m.ClassEndDate <= model.ClassEndDate);
        //    //}
        //    //else if (model.ClassStartDate != null && model.ClassEndDate == null)
        //    //{

        //    //    resultData = resultData.Where(m => m.ClassStartDate >= model.ClassStartDate);
        //    //}
        //    ////resultData=resultData.OrderBy(m => m.ID);
        //    //switch (model.order)
        //    //{
        //    //    case "id":
        //    //        resultData = resultData.OrderBy(m => m.ID);
        //    //        break;
        //    //    case "-id":
        //    //        resultData = resultData.OrderByDescending(m => m.ID);
        //    //        break;
        //    //    case "ClassName":
        //    //        resultData = resultData.OrderBy(m => m.ClassName);
        //    //        break;
        //    //    case "-ClassName":
        //    //        resultData = resultData.OrderByDescending(m => m.ClassName);
        //    //        break;
        //    //    case "RegistrationDate":
        //    //        resultData = resultData.OrderBy(m => m.RegistrationStartDate);
        //    //        break;
        //    //    case "-RegistrationDate":
        //    //        resultData = resultData.OrderByDescending(m => m.RegistrationStartDate);
        //    //        break;
        //    //    case "ClassStartDate":
        //    //        resultData = resultData.OrderBy(m => m.ClassStartDate);
        //    //        break;
        //    //    case "-ClassStartDate":
        //    //        resultData = resultData.OrderByDescending(m => m.ClassStartDate);
        //    //        break;
        //    //    case "ClassEndDate":
        //    //        resultData = resultData.OrderBy(m => m.ClassEndDate);
        //    //        break;
        //    //    case "-ClassEndDate":
        //    //        resultData = resultData.OrderByDescending(m => m.ClassEndDate);
        //    //        break;
        //    //    case "StartTime":
        //    //        resultData = resultData.OrderBy(m => m.StartTime);
        //    //        break;
        //    //    case "-StartTime":
        //    //        resultData = resultData.OrderByDescending(m => m.StartTime);
        //    //        break;
        //    //    case "EndTime":
        //    //        resultData = resultData.OrderBy(m => m.EndTime);
        //    //        break;
        //    //    case "-EndTime":
        //    //        resultData = resultData.OrderByDescending(m => m.EndTime);
        //    //        break;
        //    //}
        //    //var returnData1 = resultData.AsExpandable().Where(predicate).AsExpandable().Skip((model.page - 1) * model.limit).Take(model.limit).ToList();

        //    //var abc = Mapper.Map<List<ClassInfo>, List<ClassViewModel>>(returnData1);
        //    //abc.FirstOrDefault().TotalCnt = TotalCount;


        //    //return abc;
        //}
        /// <summary>
        /// Finds the asynchronous.
        /// </summary>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Password">The password.</param>
        /// <returns></returns>
        public ResponseViewModel FindAsync(string UserName, string Password)
        {
            ResponseViewModel responseViewModelInfo = new ResponseViewModel();
            try
            {
                var agencyinfo = _agencyRepository.FindBy(m => m.Email== UserName && m.Password == Password).FirstOrDefault();
                var agencyViewModelinfo = Mapper.Map<AgencyInfo,AgencyViewModel>(agencyinfo);
                if (agencyViewModelinfo != null)
                {
                    responseViewModelInfo.Content = agencyViewModelinfo;
                    responseViewModelInfo.IsSuccess = true;
                }
                else
                {
                    responseViewModelInfo.Content = agencyViewModelinfo;
                    responseViewModelInfo.IsSuccess = false;
                }
                
            }
            catch (Exception)
            {
                responseViewModelInfo.IsSuccess = false;
            }
            return responseViewModelInfo;
        }
        /// <summary>
        /// Adds the agency.
        /// </summary>
        /// <param name="addAgencyViewModel">The add agency view model.</param>
        /// <returns></returns>
        public int AddAgency(AgencyViewModel addAgencyViewModel)
        {
            try
            {
                //addAgencyViewModel.CityId = 1;
                //addAgencyViewModel.CountryId = 1;
                //addAgencyViewModel.StateId = 1;
                var agencyinfo = Mapper.Map<AgencyViewModel, AgencyInfo>(addAgencyViewModel);
                _agencyRepository.Add(agencyinfo);
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {

                return 0;
            }
        }
    
        /// <summary>
        /// Updates the agency detail.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <returns></returns>
        public int UpdateAgencyDetail(AgencyViewModel model, long agencyId)
        {
            int resultData = 0;
            try
            {
                var agencyDetailOld = _agencyRepository.FindBy(m => m.ID == agencyId && m.IsDeleted == false).FirstOrDefault();
                var agencyDetailNew= Mapper.Map<AgencyViewModel,AgencyInfo>(model);
                _agencyRepository.Edit(agencyDetailOld, agencyDetailNew);
                _unitOfWork.Commit();
                if (agencyDetailNew != null)
                {
                    resultData = 1;
                    return resultData;
                }
                else
                {
                    return resultData;
                }

            }
            catch (Exception)
            {
                return resultData;
            }
        }
        /// <summary>
        /// Deletes the agency by identifier.
        /// </summary>
        /// <param name="agencyId">The agency identifier.</param>
        /// <returns></returns>
        public int DeleteAgencyById(long agencyId)
        {
            int returnState = 0;
            try
            {
                long count = _agencyRepository.FindBy(d => d.ID == agencyId && d.IsDeleted == false).Count();
                if (count > 0)
                {
                    var agencyDetails = _agencyRepository.FindBy(m => m.ID == agencyId && m.IsDeleted == false).FirstOrDefault();
                    if (agencyDetails != null) agencyDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    returnState = 1;
                    return returnState;
                }
                else
                {
                    returnState = 2;
                    return returnState;
                }
            }
            catch
            {
                return returnState;
            }
        }
   
    }
}
