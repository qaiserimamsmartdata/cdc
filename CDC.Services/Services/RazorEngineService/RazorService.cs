﻿using CDC.Services.Abstract.RazorEngine;
using RazorEngine;
using RazorEngine.Templating;

namespace CDC.Services.Services.RazorEngineService
{
    public class RazorService : IRazorService
    {
        public string GetRazorTemplate(string template, string key, object item)
        {
            var result = Engine.Razor.RunCompile(template, key, null,item);
            return result;
        }
        
    }
}