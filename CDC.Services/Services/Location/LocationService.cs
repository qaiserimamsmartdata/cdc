﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Services.Abstract.Location;
using CDC.ViewModel.Common;
using CDC.ViewModel.Location;
using LinqKit;
using CDC.Entities.AgencyRegistration;

namespace CDC.Services.Services.Location
{
    public class LocationService : ILocationService
    {
        private readonly IEntityBaseRepository<AgencyLocationInfo> _locationRepository;
        private readonly IUnitOfWork _unitOfWork;

        public LocationService(
            IEntityBaseRepository<AgencyLocationInfo> locationRepository,
            IUnitOfWork unitOfWork)
        {
            _locationRepository = locationRepository;
            _unitOfWork = unitOfWork;
        }

        public LocationViewModel GetLocationById(long LocationId)
        {
            var obj = _locationRepository.GetSingle(LocationId);
            var locationObj = Mapper.Map<AgencyLocationInfo, LocationViewModel>(obj);
            return locationObj;
        }

        /// <summary>
        ///     Gets all Location.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        public void GetAllLocation(SearchLocationViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<AgencyLocationInfo>();
                if (!string.IsNullOrEmpty(model.LocationName))
                    predicate =predicate.And(p => p.LocationName.Contains(model.LocationName));

                var resultData = _locationRepository.GetAll().Where(m=>m.AgencyRegistrationId==model.AgencyId);
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "LocationName":
                        resultData = resultData.OrderBy(m => m.LocationName);
                        break;
                    case "-LocationName":
                        resultData = resultData.OrderByDescending(m => m.LocationName);
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.ContactFirstName);
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.ContactFirstName);
                        break;
                    case "LastName":
                        resultData = resultData.OrderBy(m => m.ContactLastName);
                        break;
                    case "-LastName":
                        resultData = resultData.OrderByDescending(m => m.ContactLastName);
                        break;
                    case "EmailId":
                        resultData = resultData.OrderBy(m => m.LocationEmailId);
                        break;
                    case "-EmailId":
                        resultData = resultData.OrderByDescending(m => m.LocationEmailId);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.LocationName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1)*model.limit).Take(model.limit).ToList();
                
                var locationLst = Mapper.Map<List<AgencyLocationInfo>, List<LocationViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = locationLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }


        /// <summary>
        ///     Adds the Location.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddLocation(LocationViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                
                var locationInfo = Mapper.Map<LocationViewModel, AgencyLocationInfo>(model);
                _locationRepository.Add(locationInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Location added successfully");
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        /// <summary>
        ///     Updates the Location.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo">The response information.</param>
        public void UpdateLocation(LocationViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var locationDetail =
                    _locationRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapLocationViewModelInfo = Mapper.Map<LocationViewModel, AgencyLocationInfo>(model);
                _locationRepository.Edit(locationDetail, mapLocationViewModelInfo);
                
                _unitOfWork.Commit();
                responseInfo.ReturnStatus = true;
                responseInfo.ReturnMessage.Add("Location successfully updated at " +
                                               DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        /// <summary>
        ///    Add/Updates the Location.
        /// </summary>
        /// <param name="model">The model.</param>
        public int AddUpdateLocation(LocationViewModel model)
        {
            try
            {
                if (model.ID == 0)
                {
                    var locationInfo = Mapper.Map<LocationViewModel, AgencyLocationInfo>(model);
                    _locationRepository.Add(locationInfo);
                }
                else
                {
                    var locationDetail =
                    _locationRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                    if (locationDetail != null)
                    {
                        model.IsDeleted = false;
                        var mapLocationViewModelInfo = Mapper.Map<LocationViewModel, AgencyLocationInfo>(model);
                        _locationRepository.Edit(locationDetail, mapLocationViewModelInfo);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }


        /// <summary>
        ///     Deletes the Location by identifier.
        /// </summary>
        /// <param name="LocationId">The Location identifier.</param>
        /// <param name="responseInfo">The response information.</param>
        public void DeleteLocationById(long LocationId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var locationDetails =
                    _locationRepository.FindBy(m => m.ID == LocationId && m.IsDeleted == false).FirstOrDefault();
                if (locationDetails != null && locationDetails.ID > 0)
                {
                    locationDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Location successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }



    }
}