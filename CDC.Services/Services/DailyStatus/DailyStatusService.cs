﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Attendance;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.Services.Abstract;
using CDC.Services.Abstract.DailyStatus;
using CDC.ViewModel.Common;
using CDC.ViewModel.DailyStatus;
using CDC.ViewModel.Schedule;
using LinqKit;
using CDC.Entities.NewParent;

namespace CDC.Services.Services.DailyStatus
{
    public class DailyStatusService : IDailyStatusService
    {
        private readonly IEntityBaseRepository<Entities.DailyStatus.DailyStatus> _dailyStatusRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IEntityBaseRepository<ParentInfo> _parentInfoRepository;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familyStudentMapRepository;
        private readonly IEntityBaseRepository<StudentAttendanceInfo> _studentAttendanceRepository;
        private readonly IEntityBaseRepository<StudentInfo> _studentInfoRepository;
        private readonly IEntityBaseRepository<tblParentParticipantMapping> _tblParentParticipantMappingRepository;




        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommonService _icommonService;
        public DailyStatusService(
            IEntityBaseRepository<Entities.DailyStatus.DailyStatus> dailyStatusRepository,
            IEntityBaseRepository<ParentInfo> parentInfoRepository,
            IEntityBaseRepository<StudentSchedule> studentScheduleRepository,
            IEntityBaseRepository<FamilyStudentMap> familyStudentMapRepository,
            IEntityBaseRepository<StudentInfo> studentInfoRepository,
            IEntityBaseRepository<StudentAttendanceInfo> studentAttendanceRepository,
            IEntityBaseRepository<tblParentParticipantMapping> tblParentParticipantMappingRepository,
        ICommonService icommonService,
            IUnitOfWork unitOfWork)
        {
            _dailyStatusRepository = dailyStatusRepository;
            _studentScheduleRepository = studentScheduleRepository;
            _studentAttendanceRepository = studentAttendanceRepository;
            _studentInfoRepository = studentInfoRepository;
            _parentInfoRepository = parentInfoRepository;
            _familyStudentMapRepository = familyStudentMapRepository;
            _tblParentParticipantMappingRepository = tblParentParticipantMappingRepository;
            _icommonService = icommonService;
            _unitOfWork = unitOfWork;
        }

        public DailyStatusViewModel GetDailyStatusById(long staffId)
        {

            var obj = _dailyStatusRepository.GetSingle(staffId);
            var dailyStatusObj = Mapper.Map<Entities.DailyStatus.DailyStatus, DailyStatusViewModel>(obj);
            return dailyStatusObj;
        }
        public DailyStatusViewModel DailyStatusEmail(long StudentId,DateTime Date1,string TimeZone)
        {
            DailyStatusViewModel model = new DailyStatusViewModel();
            List<DSR_eat> dsrEatModel = new List<DSR_eat>();
            List<DSR_sleep> dsrSleepModel = new List<DSR_sleep>();
            List<DSR_toilet> dsrToiletModel = new List<DSR_toilet>();
            List<DSR_Comment> dsrComment = new List<DSR_Comment>();
            
            long familyId = _familyStudentMapRepository.GetAll().Where(x => x.StudentId == StudentId).Select(x => x.FamilyId).FirstOrDefault();
            model.familyEmail = _parentInfoRepository.GetAll().Where(x => x.FamilyId == familyId && x.IsPrimary == true).Select(x => x.EmailId).FirstOrDefault();
            var participantName = _studentInfoRepository.FindBy(x => x.ID == StudentId).Select(x => new { First = x.FirstName, Last = x.LastName }).FirstOrDefault();
            model.ParticipantName = participantName.First+" "+participantName.Last;

            var eat = _dailyStatusRepository.GetAll().Where(x => x.StudentInfoId == StudentId && x.DailyStatusCategoryId == 1 && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(Date1)).Select(x => new { StartTime = x.StartTime, EndTime = x.EndTime, Date = x.Date, Comments = x.Comments }).ToList();
            if (eat != null)
            {
                foreach (var item in eat)
                {
                    /////Palak check it, it works_icommonService.ConvertTimeFromUTC(item.Date.Value, item.StartTime.Value, model.TimeZone);
                    DSR_eat Obj = new DSR_eat();
                    Obj.StartTime = new DateTime(_icommonService.ConvertTimeFromUTC(item.Date.Value, item.StartTime.Value, TimeZone).Value.Ticks).ToShortTimeString();
                    Obj.EndTime = new DateTime(_icommonService.ConvertTimeFromUTC(item.Date.Value, item.EndTime.Value,TimeZone).Value.Ticks).ToShortTimeString();
                    Obj.Date = item.Date;
                    Obj.Comments = item.Comments == null ? "" : item.Comments;
                    dsrEatModel.Add(Obj);
                }
                model.DSR_eat = dsrEatModel;
            }
            var sleep = _dailyStatusRepository.GetAll().Where(x => x.StudentInfoId == StudentId && x.DailyStatusCategoryId == 2 && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(Date1)).Select(x => new { StartTime = x.StartTime, EndTime = x.EndTime, Date = x.Date, Comments = x.Comments }).ToList();
            if(sleep != null)
            {
                foreach (var item in sleep)
                {
                    DSR_sleep Obj = new DSR_sleep();
                    Obj.StartTime = new DateTime(_icommonService.ConvertTimeFromUTC(item.Date.Value, item.StartTime.Value, TimeZone).Value.Ticks).ToShortTimeString();
                    Obj.EndTime = new DateTime(_icommonService.ConvertTimeFromUTC(item.Date.Value, item.EndTime.Value, TimeZone).Value.Ticks).ToShortTimeString();
                    Obj.Date = item.Date;
                    Obj.Comments = item.Comments == null ? "" : item.Comments;
                    dsrSleepModel.Add(Obj);
                }
                model.DSR_sleep = dsrSleepModel;
            }
          
            var toilet = _dailyStatusRepository.GetAll().Where(x => x.StudentInfoId == StudentId && x.DailyStatusCategoryId == 3 && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(Date1)).Select(x => new { StartTime = x.StartTime, EndTime = x.EndTime, Date = x.Date, Comments = x.Comments }).ToList();
            if (toilet != null)
            {
                foreach (var item in toilet)
                {
                    DSR_toilet Obj = new DSR_toilet();
                    Obj.StartTime = new DateTime(_icommonService.ConvertTimeFromUTC(item.Date.Value, item.StartTime.Value,TimeZone).Value.Ticks).ToShortTimeString();
                    Obj.EndTime = new DateTime(_icommonService.ConvertTimeFromUTC(item.Date.Value, item.EndTime.Value, TimeZone).Value.Ticks).ToShortTimeString();
                    Obj.Date = item.Date;
                    Obj.Comments = item.Comments == null ? "" : item.Comments;
                    dsrToiletModel.Add(Obj);
                }
                model.DSR_toilet = dsrToiletModel;
            }
            var comment = _dailyStatusRepository.GetAll().Where(x => x.StudentInfoId == StudentId && x.DailyStatusCategoryId == 4 && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(Date1)).Select(x => new { Comments = x.Comments }).ToList();
            if (comment != null)
            {
                foreach (var item in comment)
                {
                    DSR_Comment Obj = new DSR_Comment();
                    Obj.Comment = item.Comments;

                    dsrComment.Add(Obj);
                }
                model.DSR_Comment = dsrComment;
            }

            return model;
        }
        public void GetAllDailyStatusParticipants(SearchDailyStatusViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Entities.DailyStatus.DailyStatus>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyInfoId == model.AgencyId);
                if (model.PurposeId > 0)
                    predicate = predicate.And(p => p.DailyStatusCategoryId == model.PurposeId);
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate = predicate.And(p => p.StudentInfo.FirstName.Contains(model.ParticipantName) || p.StudentInfo.LastName.Contains(model.ParticipantName));

                if (model.FamilyId > 0)
                {
                    var studenIds = _tblParentParticipantMappingRepository.FindBy(m => m.NewParentInfoID == model.FamilyId).Select(m => m.NewParticipantInfoID).ToList();
                    predicate = predicate.And(p => studenIds.Contains(p.StudentInfoId.Value));
                }
                if (model.FromDate!=null)
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.Date.Value) >= DbFunctions.TruncateTime(model.FromDate.Value));
                if (model.ToDate != null)
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.Date.Value) <= DbFunctions.TruncateTime(model.ToDate.Value));

                var resultData = _dailyStatusRepository.GetAll().AsExpandable().Where(predicate).ToList();
                var ids = resultData                
                .GroupBy(item => item.StudentInfoId)
                 .Select(grouping => grouping)
                 .OrderByDescending(item => item.FirstOrDefault().StartTime)
                 .Select(m => new { StudentInfoId = m.Key, Count = m.Count() })
                 .ToList();
                var idss = ids.Select(k => k.StudentInfoId).ToList();
                var studentScheduleIds = _studentAttendanceRepository.All
                  .Where(m => DbFunctions.TruncateTime(m.AttendanceDate) >= DbFunctions.TruncateTime(model.FromDate.Value)
                  && DbFunctions.TruncateTime(m.AttendanceDate) <= DbFunctions.TruncateTime(model.ToDate.Value)
                  && idss.Contains(m.StudentId))
                  .GroupBy(m => m.StudentScheduleId)
                  .Select(grouping => grouping.FirstOrDefault())
                  .Select(m => m.StudentScheduleId)
                  .ToList();
                var sheduleData = _studentScheduleRepository.FindBy(m => studentScheduleIds.Contains(m.ID));


              

                var scheduleLst = Mapper.Map<List<StudentSchedule>, List<StudentScheduleViewModel>>(sheduleData.ToList());
                scheduleLst.ForEach(m => { m.ParticipantAge = _icommonService.CalculateAge(m.StudentInfo.DateOfBirth.Value); m.Count = ids.Where(w => w.StudentInfoId == m.StudentId).Select(k => k.Count).FirstOrDefault(); });
                var NewList = scheduleLst.GroupBy(c => c.StudentId)
                .Select(g => new { Qty = g.Count(), First = g.OrderBy(c => c.StudentInfo.FirstName).First() }).ToList();
                
                switch (model.order)
                {
                    case "id":
                        NewList = NewList.OrderBy(m => m.First.ID).AsQueryable().ToList();
                        break;
                    case "-id":
                        NewList = NewList.OrderByDescending(m => m.First.ID).AsQueryable().ToList();
                        break;
                    case "FirstName":
                        NewList = NewList.OrderBy(m => m.First.StudentInfo.FirstName).AsQueryable().ToList();
                        break;
                    case "-FirstName":
                        NewList = NewList.OrderByDescending(m => m.First.StudentInfo.FirstName).AsQueryable().ToList();
                        break;
                    case "ClassName":
                        NewList = NewList.OrderBy(m => m.First.ClassName).AsQueryable().ToList();
                        break;
                    case "-ClassName":
                        NewList = NewList.OrderByDescending(m => m.First.ClassName).AsQueryable().ToList();
                        break;
                    case "Age":
                        NewList = NewList.OrderBy(m => m.First.StudentInfo.DateOfBirth).AsQueryable().ToList();
                        break;
                    case "-Age":
                        NewList = NewList.OrderByDescending(m => m.First.StudentInfo.DateOfBirth).AsQueryable().ToList();
                        break;
                    case "Count":
                        NewList = NewList.OrderBy(m => m.First.Count).AsQueryable().ToList();
                        break;
                    case "-Count":
                        NewList = NewList.OrderByDescending(m => m.First.Count).AsQueryable().ToList();
                        break;
                }

                long totalCount = NewList.Count;
                string ClassNames;
                foreach (var item in NewList)
                {
                    var data = scheduleLst.Where(m => m.StudentId == item.First.StudentId);
                    ClassNames = data.Aggregate("", (current, item2) => current + (item2.ClassName + ","));
                    if (ClassNames.Length > 0)
                        ClassNames = ClassNames.Substring(0, ClassNames.Length - 1);
                    item.First.ClassName = ClassNames;

                }

                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = NewList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();

                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
        public void GetAllDailyStatus(SearchDailyStatusViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Entities.DailyStatus.DailyStatus>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyInfoId == model.AgencyId);
                if (model.PurposeId > 0)
                    predicate = predicate.And(p => p.DailyStatusCategoryId == model.PurposeId);
                if (model.ParticipantId > 0)
                    predicate = predicate.And(p => p.StudentInfo.ID == model.ParticipantId);
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate = predicate.And(p => p.StudentInfo.FirstName.Contains(model.ParticipantName) || p.StudentInfo.LastName.Contains(model.ParticipantName));
                if (model.FromDate!=null&&model.ToDate!=null)
                    predicate =
                        predicate.And(p => DbFunctions.TruncateTime(p.Date.Value)>= DbFunctions.TruncateTime(model.FromDate.Value) && DbFunctions.TruncateTime(p.Date.Value) <= DbFunctions.TruncateTime(model.ToDate.Value));
                var resultData = _dailyStatusRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Purpose":
                        resultData = resultData.OrderBy(m => m.DailyStatusCategoryId);
                        break;
                    case "-Purpose":
                        resultData = resultData.OrderByDescending(m => m.DailyStatusCategoryId);
                        break;
                    case "StartTime":
                        resultData = resultData.OrderBy(m => m.StartTime);
                        break;
                    case "-StartTime":
                        resultData = resultData.OrderByDescending(m => m.EndTime);
                        break;
                    case "EndTime":
                        resultData = resultData.OrderBy(m => m.EndTime);
                        break;
                    case "-EndTime":
                        resultData = resultData.OrderByDescending(m => m.EndTime);
                        break;
                    case "Comments":
                        resultData = resultData.OrderBy(m => m.Comments);
                        break;
                    case "-Comments":
                        resultData = resultData.OrderByDescending(m => m.Comments);
                        break;
                    case "Date":
                        resultData = resultData.OrderBy(m => m.Date);
                        break;
                    case "-Date":
                        resultData = resultData.OrderByDescending(m => m.Date);
                        break;

                }
                
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();

                
                var dailyStatusLst = Mapper.Map<List<Entities.DailyStatus.DailyStatus>, List<DailyStatusViewModel>>(returnData);
                
                foreach (var item in dailyStatusLst)
                {
                    item.ParticipantAge = _icommonService.CalculateAge(item.StudentInfo.DateOfBirth.Value);
                    if (item.StartTime != null)
                        item.StartTime = _icommonService.ConvertTimeFromUTC(item.Date.Value, item.StartTime.Value, model.TimeZone);
                    if (item.EndTime != null)
                        item.EndTime = _icommonService.ConvertTimeFromUTC(item.Date.Value, item.EndTime.Value, model.TimeZone);
                }
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = dailyStatusLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
        public void AddDailyStatus(DailyStatusViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                if (model.StartTime != null && model.EndTime!=null)
                {
                    var date = model.Date;
                    model.Date = _icommonService.ConvertToUTC(new DateTime(date.Value.Ticks, DateTimeKind.Unspecified), model.TimeZone);
                    var startTime = _icommonService.ConvertToUTC(new DateTime((date.Value.Date + model.StartTime.Value).Ticks, DateTimeKind.Unspecified), model.TimeZone).Value.TimeOfDay;
                    var endTime = _icommonService.ConvertToUTC(new DateTime((date.Value.Date + model.EndTime.Value).Ticks, DateTimeKind.Unspecified), model.TimeZone).Value.TimeOfDay;
                    model.StartTime = startTime;
                    model.EndTime = endTime;
                }
                var DailyStatus = Mapper.Map<DailyStatusViewModel, Entities.DailyStatus.DailyStatus>(model);
                _dailyStatusRepository.Add(DailyStatus);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Daily status added successfully");
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        public void UpdateDailyStatus(DailyStatusViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                if (model.StartTime != null && model.EndTime != null)
                {
                    var date = model.Date;
                    //model.Date = _icommonService.ConvertToUTC(date.Value, model.TimeZone);
                    var startTime = _icommonService.ConvertToUTC(new DateTime((date.Value.Date + model.StartTime.Value).Ticks, DateTimeKind.Unspecified), model.TimeZone).Value.TimeOfDay;
                    var endTime = _icommonService.ConvertToUTC(new DateTime((date.Value.Date + model.EndTime.Value).Ticks, DateTimeKind.Unspecified), model.TimeZone).Value.TimeOfDay;
                    model.StartTime = startTime;
                    model.EndTime = endTime;
                }

                var staffDetail =
                    _dailyStatusRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapDailyStatusViewModelInfo = Mapper.Map<DailyStatusViewModel, Entities.DailyStatus.DailyStatus>(model);
                _dailyStatusRepository.Edit(staffDetail, mapDailyStatusViewModelInfo);

                _unitOfWork.Commit();

                responseInfo.ReturnStatus = true;
                responseInfo.ReturnMessage.Add("Daily status updated successfully");

            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        public void DeleteDailyStatusById(long dailyStatusId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var dailyStatusDetails =
                    _dailyStatusRepository.FindBy(m => m.ID == dailyStatusId && m.IsDeleted == false).FirstOrDefault();
                if (dailyStatusDetails != null && dailyStatusDetails.ID > 0)
                {
                    dailyStatusDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Staff successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

    }
}