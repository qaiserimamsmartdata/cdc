﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Class;
using CDC.Entities.MealSchedular;
using CDC.Entities.Staff;
using CDC.Services.Abstract;
using CDC.Services.Abstract.MealSchedular;
using CDC.ViewModel.Common;
using CDC.ViewModel.MealSchedular;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CDC.Services.Services.MealSchedular
{
    public class MealSchedularService : IMealScheduleService
    {

        private readonly IEntityBaseRepository<MealSchedule> _mealScheduleRepository;
        private readonly IEntityBaseRepository<MealScheduleItemsInfo> _mealItemsScheduleRepository;
        private readonly IEntityBaseRepository<ClassInfo> _classRepository;
        private readonly ICommonService _commonService;
        private readonly IUnitOfWork _unitOfWork;

        public MealSchedularService(
            IEntityBaseRepository<MealSchedule> mealScheduleRepository,
            IEntityBaseRepository<MealScheduleItemsInfo>  mealItemsScheduleRepository,
             IEntityBaseRepository<ClassInfo> classRepository,
             ICommonService commonService,
            IUnitOfWork unitOfWork)
        {
            _mealScheduleRepository = mealScheduleRepository;
            _mealItemsScheduleRepository = mealItemsScheduleRepository;
            _classRepository = classRepository;
            _commonService = commonService;
            _unitOfWork = unitOfWork;
        }
        public void GetAllTasks(SearchTasksViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<MealSchedule>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (model.ClassId > 0)
                    predicate = predicate.And(p => p.ClassInfoId == model.ClassId);
                var result = _mealScheduleRepository.GetAll();
                var resultData = _mealScheduleRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = 0;
                totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        result = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        result = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "AgencyId":
                        result = resultData.OrderBy(m => m.AgencyId);
                        break;
                    case "-AgencyId":
                        result = resultData.OrderByDescending(m => m.AgencyId);
                        break;
                }
                var returnData = result.AsExpandable().Where(predicate).AsExpandable().ToList();



                var lst = Mapper.Map<List<MealSchedule>, List<TasksViewModelCopy>>(returnData);

                foreach (var item in lst)
                {

                    var className = _classRepository.FindBy(m => m.ID == item.ClassInfoId).Select(m => m.ClassName).FirstOrDefault();
                    item.ClassName = Convert.ToString(className);

                    

                    if (item.Start != null)
                    {
                        item.Start = (DateTime)_commonService.ConvertFromUTC(item.Start, model.TimeZone);
                    }
                    if (item.End != null)
                    {
                        item.End = (DateTime)_commonService.ConvertFromUTC(item.End, model.TimeZone);
                    }
                }
                
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = lst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }
        public void AddTasks(MealSchedularViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var tasksInfo = Mapper.Map<MealSchedularViewModel, MealSchedule>(model);
                _mealScheduleRepository.Add(tasksInfo);
                _unitOfWork.Commit();
                if (tasksInfo.ID>0)
                {
                    foreach (var item in model.ItemList)
                    {
                        _mealItemsScheduleRepository.Add(new MealScheduleItemsInfo() { FoodManagementMasterId = item.FoodManagementMasterId, MealScheduleId = tasksInfo.ID,SizeId=item.SizeId,QuantityId=item.QuantityId,TypeId=item.TypeId });
                    }
                }
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Meal Schedule added successfully");
                transaction.ID = tasksInfo.ID;

            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
        public void UpdateTasks(MealSchedularViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var tasksInfo = _mealScheduleRepository.FindBy(m => m.ID == model.MealSheduleID && m.IsDeleted == false).FirstOrDefault();
                var mapInfo = Mapper.Map<MealSchedularViewModel, MealSchedule>(model);
                mapInfo.IsDeleted = false;
                _mealScheduleRepository.Edit(tasksInfo, mapInfo);
                _unitOfWork.Commit();
                _mealItemsScheduleRepository.FindBy(m => m.MealScheduleId == model.MealSheduleID).ToList().ForEach(m=>_mealItemsScheduleRepository.Delete(m));
                if (tasksInfo != null && tasksInfo.ID > 0)
                {
                    foreach (var item in model.ItemList)
                    {
                        _mealItemsScheduleRepository.Add(new MealScheduleItemsInfo() { FoodManagementMasterId = item.FoodManagementMasterId, MealScheduleId = tasksInfo.ID, SizeId = item.SizeId, QuantityId = item.QuantityId, TypeId = item.TypeId });
                    }
                }
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Meal Schedule added successfully");
                if (tasksInfo != null) transaction.ID = tasksInfo.ID;
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        public void DeleteMealScheduleById(MealSchedularViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var scheduleDetails =
                    _mealScheduleRepository.FindBy(m => m.ID == model.MealSheduleID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (scheduleDetails != null && scheduleDetails.ID > 0)
                {
                    scheduleDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("Tasks successfully deleted at " +
                                                  DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
