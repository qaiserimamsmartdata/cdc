﻿using CDC.Data.Repositories;
using CDC.Services.Abstract.Email;
using CDC.Entities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using CDC.ViewModel.Common;
using CDC.ViewModel.messages;
using AutoMapper;
using CDC.Entities.Family;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.User;
using CDC.Data.Infrastructure;
using CDC.Services.Abstract;
using CDC.Entities.NewParent;

namespace CDC.Services.Services.Email
{
    public class EmailService : IEmailService
    {
        private readonly IEntityBaseRepository<Messages> _emailRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommonService _commonService;
        private readonly IEntityBaseRepository<ParentInfo> _parentInfoRepository;
        private readonly IEntityBaseRepository<AgencyRegistrationInfo> _agencyRegistrationRepository;
        private readonly IEntityBaseRepository<FamilyInfo> _familyInfoRepository;
        private readonly IEntityBaseRepository<MessageTo> _toRepository;
        private readonly IEntityBaseRepository<MessageFrom> _fromRepository;
        private readonly IEntityBaseRepository<Users> _userRepository;
        private readonly IEntityBaseRepository<Roles> _rolesRepository;
        private readonly IEntityBaseRepository<tblParentInfo> _tblParentInfoRepository;

        public EmailService(
           IEntityBaseRepository<Messages> EmailRepository, ICommonService commonService, IEntityBaseRepository<tblParentInfo> tblParentInfoRepository, IEntityBaseRepository<Roles> roleRepository ,IEntityBaseRepository<Users> userRepository, IUnitOfWork unitOfWork, IEntityBaseRepository<MessageTo> toRepository, IEntityBaseRepository<MessageFrom> fromRepository, IEntityBaseRepository<MessageCC> ccRepository, IEntityBaseRepository<MessageContacts> contactsRepository, IEntityBaseRepository<FamilyInfo> familyInfoRepository, IEntityBaseRepository<ParentInfo> parentInfoRepository, IEntityBaseRepository<AgencyRegistrationInfo> agencyRegistrationRepository)

        {
            _emailRepository = EmailRepository;
            _rolesRepository = roleRepository;
            _commonService = commonService;
            _userRepository = userRepository;
            _tblParentInfoRepository = tblParentInfoRepository;
            _toRepository = toRepository;
            _fromRepository = fromRepository;
            _parentInfoRepository = parentInfoRepository;
            _agencyRegistrationRepository = agencyRegistrationRepository;
            _familyInfoRepository = familyInfoRepository;
            _unitOfWork = unitOfWork;

        }
        public void GetAllMail(out ResponseViewModel responseViewModel, SearchMessageViewModel model)
        {
            responseViewModel = new ResponseViewModel();
            var userinfo = _userRepository.FindBy(m => m.ID == model.UserId && m.RoleId == model.RoleId).Select(m => m.EmailId).FirstOrDefault();
            var messageID = _toRepository.FindBy(m => m.email == userinfo && (m.IsDeleted == false || m.IsDeleted == null)).ToList();
            List<Messages> lst = new List<Messages>();
            foreach (var item in messageID)
            {
                lst.AddRange(_emailRepository.FindBy(x => x.ID == item.messageId && x.IsInboxDeleted==false).ToList());
            }            
            var messages= Mapper.Map<List<Messages>, List<messageViewModel>>(lst);

            foreach (var item in messages)
            {
                if (item.date != null)
                {
                    item.date = (DateTime)_commonService.ConvertFromUTC(item.date, model.TimeZone);
                }
            }
            responseViewModel.Content = messages;
            responseViewModel.IsSuccess = true;

        }

        public void GetAllSentMail(out ResponseViewModel responseViewModel, SearchMessageViewModel model)
        {
            responseViewModel = new ResponseViewModel();
            var userinfo = _userRepository.FindBy(m => m.ID == model.UserId && m.RoleId == model.RoleId).Select(m => m.EmailId).FirstOrDefault();
            var messageID = _fromRepository.FindBy(m => m.email == userinfo && (m.IsDeleted == false || m.IsDeleted == null)).ToList();
            List<Messages> lst = new List<Messages>();
            foreach (var item in messageID)
            {
                lst.AddRange(_emailRepository.FindBy(x => x.ID == item.messageId && x.IsSentDeleted == false).ToList());
            }
            var messages = Mapper.Map<List<Messages>, List<messageViewModel>>(lst);

            foreach (var item in messages)
            {
                if (item.date != null)
                {
                    item.date = (DateTime)_commonService.ConvertFromUTC(item.date, model.TimeZone);
                }
            }
            responseViewModel.Content = messages;
            responseViewModel.IsSuccess = true;
        }

        public void GetAllContacts(out ResponseViewModel responseViewModel, SearchMessageViewModel model)
        {
            responseViewModel = new ResponseViewModel();
           
            if(model.RoleId==4)
            {
                var abc = _agencyRegistrationRepository.GetAll().ToList();
                var data = (from a in abc
                            select new { email = a.Email, name = a.AgencyName, image = "assets/images/avatars/avatar-5.png" }).ToList();

                responseViewModel.Content = data;
                responseViewModel.IsSuccess = true;
            }
           else if (model.RoleId == 1)
            {
                var data = (from ari in _agencyRegistrationRepository.FindBy(m => m.ID == model.AgencyId)
                           join fi in _tblParentInfoRepository.GetAll() on ari.ID equals fi.AgencyId
                           
                           
                           select new {email = fi.EmailId, image=fi.ImagePath, name=fi.FullName}).ToList();
                responseViewModel.Content = data;
                responseViewModel.IsSuccess = true;
            }


        }
        public void GetAgencyByFamilyId(out ResponseViewModel responseViewModel, SearchUserViewModel model)
        {
            responseViewModel = new ResponseViewModel();
            var familyData = _familyInfoRepository.FindBy(m=> m.ID==model.FamilyId).FirstOrDefault();
            var agencyData = _agencyRegistrationRepository.FindBy(m => m.ID == familyData.AgencyId).FirstOrDefault();
            responseViewModel.Content = agencyData;
            responseViewModel.IsSuccess = true;
        }

        public void ComposeMessage(messageViewModel model, out ResponseViewModel transaction)
        {
            transaction = new ResponseViewModel();
            try
            {
                if(model != null)
                {
                    var messagesInfo = Mapper.Map<messageViewModel, Messages>(model);
                    messagesInfo.IsDeleted = false;
                    messagesInfo.IsInboxDeleted = false;
                    messagesInfo.IsSentDeleted = false;
                    foreach (var item in messagesInfo.to)
                    {
                        item.IsDeleted = false;
                    }
                    foreach (var item in messagesInfo.cc)
                    {
                        item.IsDeleted = false;
                    }

                 
                


                    messagesInfo.unread = true;
                    messagesInfo.date = DateTime.UtcNow;
                    _emailRepository.Add(messagesInfo);
                    var messageId = messagesInfo.ID;
                 
                    var email = _userRepository.FindBy(m => m.ID == model.UserId && m.RoleId == model.RoleId).Select(m => m.EmailId).ToList();
                    var roleName = _rolesRepository.FindBy(m => m.ID == model.RoleId).Select(m => m.RoleName).ToList();
                   
                    var emailId = Convert.ToString( email[0]);

                    if (roleName[0]=="Family")
                    {
                        string host;
                        var parentData = _tblParentInfoRepository.FindBy(m => m.EmailId == emailId).SingleOrDefault();
                        if(parentData.ImagePath== "assets/images/avatars/avatar-5.png")
                        {
                            host = "";
                        }
                        else
                        {
                            host = model.Host;
                        }
                        fromViewModel fromvm = new fromViewModel();
                        fromvm.name = parentData.FirstName + " " + parentData.LastName;
                        fromvm.email = parentData.EmailId;
                        fromvm.image = host + parentData.ImagePath;
                        model.from1 = new List<fromViewModel>();
                        model.from1.Add(fromvm);
                        model.from = fromvm;

                        var fromInfo = Mapper.Map<fromViewModel, MessageFrom>(fromvm);
                        fromInfo.messageId = messageId;
                        fromInfo.IsDeleted = false;
                        _fromRepository.Add(fromInfo);

                    }
                    else if (roleName[0] == "Agency")
                    {
                        
                        var agencyData = _agencyRegistrationRepository.FindBy(m => m.Email == emailId).SingleOrDefault();
                        
                        fromViewModel fromvm = new fromViewModel();
                        fromvm.name = agencyData.AgencyName;
                        fromvm.email = agencyData.Email;
                        fromvm.image = "assets/images/avatars/avatar-5.png";
                        model.from1 = new List<fromViewModel>();
                        model.from1.Add(fromvm);
                        model.from = fromvm;
                        var fromInfo = Mapper.Map<fromViewModel, MessageFrom>(fromvm);
                       
                        fromInfo.messageId = messageId;
                        fromInfo.IsDeleted = false;
                  
                        _fromRepository.Add(fromInfo);

                    }

                    _unitOfWork.Commit();
                    transaction.Content = model;
                    
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
            
        }

        public void DeleteInboxMessage(messageViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                int id = Convert.ToInt32(model.id);
                var message = _emailRepository.FindBy(m => m.ID == id).FirstOrDefault();
                message.IsInboxDeleted = true;
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        public void DeleteSentMessage(messageViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                int id = Convert.ToInt32(model.id);
                var message = _emailRepository.FindBy(m => m.ID == id).FirstOrDefault();
                message.IsSentDeleted = true;
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        public void updateUnreadCount(messageViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
           long id= Convert.ToInt32(model.id);
            var tasksDetails =
                   _emailRepository.FindBy(m => m.ID == id && m.IsDeleted == false)
                       .FirstOrDefault();
            if (tasksDetails != null && tasksDetails.ID > 0)
            {
                tasksDetails.unread = false;
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
            }
        }


        public void GetUnreadCount(out ResponseViewModel responseViewModel, SearchMessageViewModel model)
        {
            responseViewModel = new ResponseViewModel();
            var userinfo = _userRepository.FindBy(m => m.ID == model.UserId && m.RoleId == model.RoleId).Select(m => m.EmailId).FirstOrDefault();
            var messageID = _toRepository.FindBy(m => m.email == userinfo && (m.IsDeleted == false || m.IsDeleted == null)).ToList();
            List<Messages> lst = new List<Messages>();
            foreach (var item in messageID)
            {
                lst.AddRange(_emailRepository.FindBy(x => x.ID == item.messageId && x.IsDeleted == false && x.unread==true ).ToList());
            }
            var messages = Mapper.Map<List<Messages>, List<messageViewModel>>(lst);
            responseViewModel.Content = messages.Count;
            responseViewModel.IsSuccess = true;

        }

        
    }
}
