﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.Services.Abstract.ParticipantEnrollmentPayment;
using CDC.Entities.ParticipantEnrollmentPayment;
using CDC.ViewModel.ParticipantEnrollmentPayment;

namespace CDC.Services.Services.ParticipantEnrollmentPayment
{
    public class ParticipantEnrollmentPaymentService : IParticipantEnrollmentPaymentService
    {
        private readonly IEntityBaseRepository<ParticipantEnrollPayment> _enrollPaymentRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ParticipantEnrollmentPaymentService(
            IEntityBaseRepository<ParticipantEnrollPayment> enrollPaymentRepository,
            IUnitOfWork unitOfWork)
        {
            _enrollPaymentRepository = enrollPaymentRepository;
            _unitOfWork = unitOfWork;
        }
        public void AddPayInfo(ParticipantEnrollPaymentViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {

                var payInfo = Mapper.Map<ParticipantEnrollPaymentViewModel, ParticipantEnrollPayment>(model);
                payInfo.CreatedDate = DateTime.Now;
                _enrollPaymentRepository.Add(payInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add(AppConstants.StaffAddedSuccesfully);
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
        public void UpdatePayInfo(ParticipantEnrollPaymentViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var enrollPaymentDetail =
                      _enrollPaymentRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapEnrollPaymentViewModelInfo = Mapper.Map<ParticipantEnrollPaymentViewModel, ParticipantEnrollPayment>(model);
                mapEnrollPaymentViewModelInfo.ModifiedDate = DateTime.Now;
                _enrollPaymentRepository.Edit(enrollPaymentDetail, mapEnrollPaymentViewModelInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add(AppConstants.StaffAddedSuccesfully);
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

    }
}