﻿using CDC.Services.Abstract.Tools.Enhancements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Enhancements;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Enhancements;
using CDC.Services.Abstract;
using CDC.Data.Infrastructure;
using AutoMapper;
using LinqKit;
using System.Data.Entity;

namespace CDC.Services.Services.Tools.Enhancements
{
    public class EnhancementService : IEnhancementService
    {
        private readonly IEntityBaseRepository<Enhancement> _enhancementRepository;
        private readonly ICommonService _commonServices;
        private readonly IUnitOfWork _unitOfWork;

        public EnhancementService(IEntityBaseRepository<Enhancement> enhancementRepository,
                          IUnitOfWork unitOfWork,
                          ICommonService commonServices)
        {
            _enhancementRepository = enhancementRepository;
            _unitOfWork = unitOfWork;
            _commonServices = commonServices;
        }

        public int AddUpdateEnhancement(EnhancementViewModel enhancementViewModel)
        {
            try
            {
                if (enhancementViewModel.ID == 0)
                {
                    enhancementViewModel.IsDeleted = false;
                    var holiday = Mapper.Map<EnhancementViewModel, Enhancement>(enhancementViewModel);
                    _enhancementRepository.Add(holiday);
                }
                else
                {
                    var holidayDetail =
                        _enhancementRepository.FindBy(m => m.ID == enhancementViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (holidayDetail != null)
                    {
                        holidayDetail.IsDeleted = false;
                        var holiday = Mapper.Map<EnhancementViewModel, Enhancement>(enhancementViewModel);
                        _enhancementRepository.Edit(holidayDetail, holiday);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public void DeleteEnhancementById(long id, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var enhancementDetails =
                    _enhancementRepository.FindBy(m => m.ID == id && m.IsDeleted == false).FirstOrDefault();
                if (enhancementDetails != null && enhancementDetails.ID > 0)
                {
                    enhancementDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Enhancement successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        public void GetAllEnhancement(SearchEnhancementViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Enhancement>();

                if (!string.IsNullOrEmpty(model.Title))
                    predicate = predicate.And(p => p.Title.ToLower().Contains(model.Title.ToLower()));
                if (model.EnhancementDate != new DateTime())
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.EnhancementDate) == DbFunctions.TruncateTime(model.EnhancementDate));


                var resultData = _enhancementRepository.GetAll().Where(w => w.AgencyId == model.AgencyId);
                long totalCount = resultData.Count();
                //resultData=resultData.OrderBy(m => m.ID);
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Title":
                        resultData = resultData.OrderBy(m => m.Title);
                        break;
                    case "-Title":
                        resultData = resultData.OrderByDescending(m => m.Title);
                        break;
                    case "EnhancementDate":
                        resultData = resultData.OrderBy(m => m.EnhancementDate);
                        break;
                    case "-EnhancementDate":
                        resultData = resultData.OrderByDescending(m => m.EnhancementDate);
                        break;
                    case "Description":
                        resultData = resultData.OrderBy(m => m.Description);
                        break;
                    case "-Description":
                        resultData = resultData.OrderByDescending(m => m.Description);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Title);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                //foreach (var item in returnData)
                //{
                //    if (item.AgencyScheduleList.Count > 0)
                //    {
                //        item.StaffScheduleList = item.StaffScheduleList.Where(s => s.IsDeleted == false).ToList();
                //    }
                //}
                var enhancementList = Mapper.Map<List<Enhancement>, List<EnhancementViewModel>>(returnData);
                //var newagencyList = Mapper.Map<List<AgencyRegistrationInfo>, List<AgencyListViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = enhancementList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
