﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Staff;
using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Tools.Staff;
using CDC.ViewModel.Common;
using LinqKit;
using CDC.Entities.Tools.Attendance;
using CDC.ViewModel.Tools.Attendance;

namespace CDC.Services.Services.Tools.Attendance
{
    /// <summary>
    /// </summary>
    /// <seealso>
    ///     <cref>CDC.Services.Services.Tools.Attendance</cref>
    /// </seealso>
    public class ModeOfConvenienceService : IModeOfConvenienceService
    {
        private readonly IEntityBaseRepository<ModeOfConvenience> _modeOfConvenienceRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ModeOfConvenienceService(
            IEntityBaseRepository<ModeOfConvenience> modeOfConvenienceRepository, IUnitOfWork unitOfWork
            )
        {
            _modeOfConvenienceRepository = modeOfConvenienceRepository;
            _unitOfWork = unitOfWork;
        }
        public int AddUpdateModeOfConvenience(ModeOfConvenienceViewModel model)
        {
            try
            {
                if (model.ID == 0)
                {
                    //var count = _modeOfConvenienceRepository.FindBy(m => m.Name.ToUpper() == addDesignationViewModel.Name.ToUpper() 
                    //                            && m.AgencyId == addDesignationViewModel.AgencyId && m.IsDeleted == false).Count();
                    //if (count > 0)
                    //{
                    //    return 3;
                    //}
                    var modeOfConv = Mapper
                        .Map<ModeOfConvenienceViewModel, ModeOfConvenience>(model);
                    _modeOfConvenienceRepository.Add(modeOfConv);
                }
                else
                {
                    //var count = _modeOfConvenienceRepository.FindBy(m => m.Name.ToUpper() == addDesignationViewModel.Name.ToUpper()
                    //                            && m.AgencyId == addDesignationViewModel.AgencyId && m.IsDeleted == false).Count();
                    //if (count > 0)
                    //{
                    //    return 3;
                    //}
                    var modeOfConvDetail =
                        _modeOfConvenienceRepository.FindBy(m => m.ID == model.ID)
                            .FirstOrDefault();
                    if (modeOfConvDetail != null)
                    {
                        model.IsDeleted = false;
                        var designation = Mapper.Map<ModeOfConvenienceViewModel, ModeOfConvenience>(model);
                        _modeOfConvenienceRepository.Edit(modeOfConvDetail, designation);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public List<ModeOfConvenienceViewModel> GetModeOfConvenience(SearchFieldsViewModel model)
        {
            var resultData = _modeOfConvenienceRepository.GetAll().Where(m => m.AgencyId == model.AgencyId && m.IsDeleted == false).ToList();
            if (!string.IsNullOrEmpty(model.name))
            {
                resultData = resultData.Where(x => x.Name.Contains(model.name)).Select(x => x).ToList();
            }
            var modeOfConvResult = Mapper.Map<List<ModeOfConvenience>, List<ModeOfConvenienceViewModel>>(resultData);
            return modeOfConvResult;
        }
        public int DeleteModeOfConvenienceById(ModeOfConvenienceViewModel model)
        {
            var returnState = 0;
            try
            {
                long count =
                    _modeOfConvenienceRepository.FindBy(d => d.ID == model.ID && d.IsDeleted == false)
                        .Count();
                if (count <= 0) return returnState;
                var modeOfConvDetails =
                    _modeOfConvenienceRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (modeOfConvDetails != null)
                    modeOfConvDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void GetAllModeOfConvenience(SearchModeOfConvenienceViewModel model,out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<ModeOfConvenience>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));
      
                //var resultData = _modeOfConvenienceRepository.GetAll();
                //resultData = resultData.Where(m => m.AgencyId == model.AgencyId);
                //long totalCount = resultData.Count();

                var resultData = _modeOfConvenienceRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if(string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var modeOfConvList = Mapper.Map<List<ModeOfConvenience>, List<ModeOfConvenienceViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = modeOfConvList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

    }
}