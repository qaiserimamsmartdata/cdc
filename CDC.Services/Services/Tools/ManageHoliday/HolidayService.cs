﻿using CDC.Services.Abstract.Tools.ManageHoliday;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.ManageHoliday;
using CDC.Data.Repositories;
using CDC.Entities.Tools.ManageHoliday;
using CDC.Services.Abstract;
using CDC.Data.Infrastructure;
using AutoMapper;
using LinqKit;
using System.Data.Entity;

namespace CDC.Services.Services.Tools.ManageHoliday
{
    public class HolidayService : IHolidayService
    {
        private readonly IEntityBaseRepository<Holiday> _holidayRepository;
        private readonly ICommonService _commonServices;
        private readonly IUnitOfWork _unitOfWork;

        public HolidayService(IEntityBaseRepository<Holiday> holidayRepository,
                            IUnitOfWork unitOfWork,
                            ICommonService commonServices)
        {
            _holidayRepository = holidayRepository;
            _unitOfWork = unitOfWork;
            _commonServices = commonServices;
        }

        public int AddUpdateHoliday(HolidayViewModel holidayViewModel)
        {
            try
            {
                if (holidayViewModel.ID == 0)
                {
                    holidayViewModel.IsDeleted = false;
                    holidayViewModel.Status = true;
                    var holiday = Mapper.Map<HolidayViewModel, Holiday>(holidayViewModel);
                    _holidayRepository.Add(holiday);
                }
                else
                {
                    var holidayDetail =
                        _holidayRepository.FindBy(m => m.ID == holidayViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (holidayDetail != null)
                    {
                        holidayDetail.IsDeleted = false;
                        holidayDetail.Status = true;
                        var holiday = Mapper.Map<HolidayViewModel, Holiday>(holidayViewModel);
                        _holidayRepository.Edit(holidayDetail, holiday);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public void DeleteHolidayById(long id, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var holidayDetails =
                    _holidayRepository.FindBy(m => m.ID == id && m.IsDeleted == false).FirstOrDefault();
                if (holidayDetails != null && holidayDetails.ID > 0)
                {
                    holidayDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Holiday successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        public void GetAllHoliday(SearchHolidayViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Holiday>();

                if (!string.IsNullOrEmpty(model.Title))
                    predicate = predicate.And(p => p.Title.ToLower().Contains(model.Title.ToLower()));
                if (model.HolidayDate != new DateTime())
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.HolidayDate) == DbFunctions.TruncateTime(model.HolidayDate));


                var resultData = _holidayRepository.GetAll().Where(w => w.AgencyId == model.AgencyId);
                long totalCount = resultData.Count();
                //resultData=resultData.OrderBy(m => m.ID);
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Title":
                        resultData = resultData.OrderBy(m => m.Title);
                        break;
                    case "-Title":
                        resultData = resultData.OrderByDescending(m => m.Title);
                        break;
                    case "HolidayDate":
                        resultData = resultData.OrderBy(m => m.HolidayDate);
                        break;
                    case "-HolidayDate":
                        resultData = resultData.OrderByDescending(m => m.HolidayDate);
                        break;
                    case "Description":
                        resultData = resultData.OrderBy(m => m.Description);
                        break;
                    case "-Description":
                        resultData = resultData.OrderByDescending(m => m.Description);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Title);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                //foreach (var item in returnData)
                //{
                //    if (item.AgencyScheduleList.Count > 0)
                //    {
                //        item.StaffScheduleList = item.StaffScheduleList.Where(s => s.IsDeleted == false).ToList();
                //    }
                //}
                var holidayList = Mapper.Map<List<Holiday>, List<HolidayViewModel>>(returnData);
                //var newagencyList = Mapper.Map<List<AgencyRegistrationInfo>, List<AgencyListViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = holidayList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }

        }
    }
}
