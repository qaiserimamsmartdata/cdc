﻿using AutoMapper;
using CDC.Data.AsyncRepositories;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Student;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Tools.Student;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Services.Tools.Student
{
    public class GeneralDroppedByService : IGeneralDroppedByService
    {
        private readonly IEntityBaseRepository<GeneralDroppedBy> _generalDroppedByRepository;
        private readonly ICommonService _commonServices;
        private readonly IUnitOfWork _unitOfWork;
        public GeneralDroppedByService(IEntityBaseRepository<GeneralDroppedBy> generalDroppedByRepository,
           IUnitOfWork unitOfWork,
           ICommonService commonServices)
        {
            _generalDroppedByRepository = generalDroppedByRepository;
            _unitOfWork = unitOfWork;
            _commonServices = commonServices;
        }
        public ResponseViewModel GetGeneralDroppedBy(long agencyid)
        {
            var response = new ResponseViewModel();
            try
            {
                var obj = _generalDroppedByRepository.FindBy(g => g.AgencyId == agencyid).ToList();
                var responsedata = Mapper.Map<List<GeneralDroppedBy>, List<GeneralDroppedByViewModel>>(obj);
                response.IsSuccess = true;
                response.Message = "General Dropped By retrieved successfully";
                response.Content = responsedata;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }

        public ResponseViewModel AddUpdateGeneralDroppedBy(GeneralDroppedByViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                var obj = Mapper.Map<GeneralDroppedByViewModel, GeneralDroppedBy>(model);
                if (obj.ID > 0)
                {
                    var oldObj = _generalDroppedByRepository.GetSingle(obj.ID);
                    obj.IsDeleted = false;
                    _generalDroppedByRepository.Edit(oldObj, obj);
                    _unitOfWork.Commit();
                    response.IsSuccess = true;
                    response.Message = "General Dropped By added successfully";
                }
                else
                {
                    obj.IsDeleted = false;
                    _generalDroppedByRepository.Add(obj);
                    _unitOfWork.Commit();
                    response.IsSuccess = true;
                    response.Message = "General Dropped By updated successfully";
                }
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
                response.Content = model;
            }
            return response;
        }

        public ResponseViewModel DeleteGeneralDroppedBy(GeneralDroppedByViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                var obj = Mapper.Map<GeneralDroppedByViewModel, GeneralDroppedBy>(model);
                _generalDroppedByRepository.SoftDelete(obj);
                _unitOfWork.Commit();
                response.IsSuccess = true;
                response.Message = "General Dropped By deleted successfully";
                response.Content = model;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
                response.Content = model;
            }
            return response;
        }
        public void GetAll(SearchGeneralDroppedByViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<GeneralDroppedBy>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

                //var resultData = _modeOfConvenienceRepository.GetAll();
                //resultData = resultData.Where(m => m.AgencyId == model.AgencyId);
                //long totalCount = resultData.Count();

                var resultData = _generalDroppedByRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var modeOfConvList = Mapper.Map<List<GeneralDroppedBy>, List<GeneralDroppedByViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = modeOfConvList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
