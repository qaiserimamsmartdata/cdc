﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Student;
using CDC.Services.Abstract.Tools.Student;
using CDC.ViewModel.Tools.Student;

namespace CDC.Services.Services.Tools.Student
{
    public class EnrollTypeService : IEnrollTypeService
    {
        private readonly IEntityBaseRepository<EnrollType> _enrollTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnrollTypeService" /> class.
        /// </summary>
        /// <param name="enrollTypeRepository">The enroll type repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public EnrollTypeService(
            IEntityBaseRepository<EnrollType> enrollTypeRepository, IUnitOfWork unitOfWork
            )
        {
            _enrollTypeRepository = enrollTypeRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the update enroll type.
        /// </summary>
        /// <param name="addEnrollTypeViewModel">The add enroll type view model.</param>
        /// <returns></returns>
        public int AddUpdateEnrollType(EnrollTypeViewModel addEnrollTypeViewModel)
        {
            try
            {
                if (addEnrollTypeViewModel.ID == 0)
                {
                    var count =
                        _enrollTypeRepository.FindBy(
                            m =>
                                string.Equals(m.Name, addEnrollTypeViewModel.Name, StringComparison.CurrentCultureIgnoreCase) &&
                                m.AgencyId == addEnrollTypeViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var enrollType = Mapper.Map<EnrollTypeViewModel, EnrollType>(addEnrollTypeViewModel);
                    _enrollTypeRepository.Add(enrollType);
                }
                else
                {
                    var count =
                        _enrollTypeRepository.FindBy(
                            m =>
                                string.Equals(m.Name, addEnrollTypeViewModel.Name, StringComparison.CurrentCultureIgnoreCase) &&
                                m.AgencyId == addEnrollTypeViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var enrollTypeDetail =
                        _enrollTypeRepository.FindBy(m => m.ID == addEnrollTypeViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (enrollTypeDetail != null)
                    {
                        addEnrollTypeViewModel.IsDeleted = false;
                        var enrollType = Mapper.Map<EnrollTypeViewModel, EnrollType>(addEnrollTypeViewModel);
                        _enrollTypeRepository.Edit(enrollTypeDetail, enrollType);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the enroll type.
        /// </summary>
        /// <returns></returns>
        public List<EnrollTypeViewModel> GetEnrollType(EnrollTypeViewModel addEnrollTypeViewModel)
        {
            var resultData =
                _enrollTypeRepository.GetAll()
                    .Where(g => g.AgencyId == addEnrollTypeViewModel.AgencyId && g.IsDeleted == false)
                    .ToList();
            var enrollType = Mapper.Map<List<EnrollType>, List<EnrollTypeViewModel>>(resultData);

            return enrollType;
        }

        /// <summary>
        ///     Deletes the enroll type by identifier.
        /// </summary>
        /// <param name="addEnrollTypeViewModel"></param>
        /// <returns></returns>
        public int DeleteEnrollTypeById(EnrollTypeViewModel addEnrollTypeViewModel)
        {
            var returnState = 0;
            try
            {
                long count =
                    _enrollTypeRepository.FindBy(d => d.ID == addEnrollTypeViewModel.ID && d.IsDeleted == false).Count();
                if (count <= 0) return returnState;
                var enrollTypeDetails =
                    _enrollTypeRepository.FindBy(m => m.ID == addEnrollTypeViewModel.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (enrollTypeDetails != null) enrollTypeDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }
    }
}