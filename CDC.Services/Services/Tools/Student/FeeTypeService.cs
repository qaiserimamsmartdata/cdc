﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Student;
using CDC.Services.Abstract.Tools.Student;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Services.Tools.Student
{
    public class FeeTypeService : IFeeTypeService
    {
        private readonly IEntityBaseRepository<Entities.Tools.Student.FeeType> _feetyperepository;
        private readonly IUnitOfWork _unitOfWork;
        ResponseViewModel response = new ResponseViewModel();

        public FeeTypeService(IEntityBaseRepository<Entities.Tools.Student.FeeType> feetyperepository,
                              IUnitOfWork unitOfWork)
        {
            _feetyperepository = feetyperepository;
            _unitOfWork = unitOfWork;
        }

        public ResponseViewModel GetFeeTypeById(long Id)
        {
            if (Id != null || Id != 0)
            {
                var data = _feetyperepository.GetSingle(Id);
                var EventData = Mapper.Map<FeeType, FeeTypeViewModel>(data);
                response.IsSuccess = true;
                response.Content = EventData;
                return response;
            }
            else
            {
                response.Message = "Error";
                response.IsSuccess = false;
                return response;
            }
        }

        public ResponseViewModel GetAllFeeTypes(FeeTypeViewModel feetypeVM)
        {
            try
            {
                var predicate = PredicateBuilder.True<FeeType>();
                if (feetypeVM.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == feetypeVM.AgencyId);
                if (!string.IsNullOrEmpty(feetypeVM.Name))
                    predicate = predicate.And(p => p.Name == feetypeVM.Name);

                var resultData = _feetyperepository.FindBy(f => f.AgencyId == feetypeVM.AgencyId);
                Int64 totalCount = resultData.Count();

                switch (feetypeVM.order)
                {
                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;
                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                    case "Days":
                        resultData = resultData.OrderBy(m => m.Days);
                        break;
                    case "-Days":
                        resultData = resultData.OrderByDescending(m => m.Days);
                        break;
                }

                if (string.IsNullOrEmpty(feetypeVM.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((feetypeVM.page - 1) * feetypeVM.limit).Take(feetypeVM.limit).ToList();

                var FeeTypeList = Mapper.Map<List<FeeType>, List<FeeTypeViewModel>>(returnData);
                response.TotalRows = totalCount;
                response.IsSuccess = true;
                response.Content = FeeTypeList;
            }

            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ReturnMessage = new List<string>();
                string errorMessage = ex.Message;
                response.ReturnStatus = false;
                response.ReturnMessage.Add(errorMessage);
            }
            return response;
        }

        public ResponseViewModel AddUpdateFeeTypes(FeeTypeViewModel feetypeVM)
        {
            try
            {
                if (feetypeVM.Name == null || feetypeVM.Name == "") {
                    response.IsSuccess = false;
                    response.Message = "Name cannot be empty";
                    return response;
                }
                else if (feetypeVM.Days == null || feetypeVM.Days == 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Days cannot be empty or zero";
                    return response;
                }
                else
                {
                    if (feetypeVM.ID > 0)
                    {
                        var data = _feetyperepository.GetSingle(feetypeVM.ID);
                        var model = Mapper.Map<FeeTypeViewModel, FeeType>(feetypeVM);
                        _feetyperepository.Edit(data, model);
                        _unitOfWork.Commit();
                        response.IsSuccess = true;
                        response.Message = "Fee Type Info Updated Successfully.";
                        response.Id = model.ID;
                    }
                    else
                    {
                        var model = Mapper.Map<FeeTypeViewModel, FeeType>(feetypeVM);
                        model.IsDeleted = false;
                        _feetyperepository.Add(model);
                        _unitOfWork.Commit();
                        response.IsSuccess = true;
                        response.Message = "Fee Type Info Saved Sucessfully";
                        response.Id = model.ID;
                    }
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.ReturnMessage = new List<string>();
                string errorMessage = ex.Message;
                response.ReturnStatus = false;
                response.ReturnMessage.Add(errorMessage);
            }
            return response;
        }

        public void DeleteFeeTypes(long Id, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                if (Id != 0 || Id != null)
                {
                    var Detail = _feetyperepository.FindBy(m => m.ID == Id && m.IsDeleted == false).FirstOrDefault();
                    if (Detail.ID > 0)
                    {
                        Detail.IsDeleted = true;
                        _unitOfWork.Commit();
                        responseInfo.ReturnStatus = true;
                        responseInfo.ReturnMessage.Add("Fee type successfully deleted at " + DateTime.UtcNow.ToString());
                    }
                }
                else
                {
                    responseInfo.ReturnStatus = false;
                    responseInfo.ReturnMessage.Add("Error");
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                string errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
