﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Student;
using CDC.Services.Abstract.Tools.Student;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CDC.Services.Services.Tools.Student
{
    public class DropReasonService:IDropReasonService
    {
        private readonly IEntityBaseRepository<DropReason> _dropReasonRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DropReasonService" /> class.
        /// </summary>
        /// <param name="dropReasonRepository">The drop reason repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public DropReasonService(
            IEntityBaseRepository<DropReason> dropReasonRepository, IUnitOfWork unitOfWork
            )
        {
            _dropReasonRepository = dropReasonRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the update drop reason.
        /// </summary>
        /// <param name="addDropReasonViewModel">The add drop reason view model.</param>
        /// <returns></returns>
        public int AddUpdateDropReason(DropReasonViewModel addDropReasonViewModel)
        {
            try
            {
                if (addDropReasonViewModel.ID == 0)
                {
                    var count = _dropReasonRepository.FindBy(m => m.Name.ToUpper() == addDropReasonViewModel.Name.ToUpper() && m.AgencyId == addDropReasonViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var dropReason = Mapper.Map<DropReasonViewModel, DropReason>(addDropReasonViewModel);
                    _dropReasonRepository.Add(dropReason);
                }
                else
                {
                    var count = _dropReasonRepository.FindBy(m => m.Name.ToUpper() == addDropReasonViewModel.Name.ToUpper() && m.AgencyId == addDropReasonViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var dropReasonDetail =
                        _dropReasonRepository.FindBy(m => m.ID == addDropReasonViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (dropReasonDetail != null)
                    {
                        addDropReasonViewModel.IsDeleted = false;
                        var dropReason = Mapper.Map<DropReasonViewModel, DropReason>(addDropReasonViewModel);
                        _dropReasonRepository.Edit(dropReasonDetail, dropReason);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the drop reason.
        /// </summary>
        /// <returns></returns>
        public List<DropReasonViewModel> GetDropReason(SearchFieldsViewModel addDropReasonViewModel)
        {
            var resultData = _dropReasonRepository.GetAll().Where(g => g.AgencyId == addDropReasonViewModel.AgencyId && g.IsDeleted == false).ToList();
            if (!string.IsNullOrEmpty(addDropReasonViewModel.name))
            {
                resultData = resultData.Where(x => x.Name.Contains(addDropReasonViewModel.name)).Select(x => x).ToList();
            }
            var dropReason = Mapper.Map<List<DropReason>, List<DropReasonViewModel>>(resultData);

            return dropReason;
        }

        /// <summary>
        ///     Deletes the drop reason by identifier.
        /// </summary>
        /// <param name="addDropReasonViewModel"></param>
        /// <returns></returns>
        public int DeleteDropReasonById(DropReasonViewModel addDropReasonViewModel)
        {
            var returnState = 0;
            try
            {
                long count = _dropReasonRepository.FindBy(d => d.ID == addDropReasonViewModel.ID && d.IsDeleted == false).Count();
                if (count <= 0) return returnState;
                var dropReasonDetails =
                    _dropReasonRepository.FindBy(m => m.ID == addDropReasonViewModel.ID && m.IsDeleted == false).FirstOrDefault();
                if (dropReasonDetails != null) dropReasonDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }
    }
}
