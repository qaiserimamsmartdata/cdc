﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Family;
using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CDC.Services.Services.Tools.Family
{
    public class RelationshipTypeService:IRelationshipTypeService
    {
        private readonly IEntityBaseRepository<RelationType> _relationshipTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RelationshipTypeService(
           IEntityBaseRepository<RelationType> relationshipTypeRepository,
           IUnitOfWork unitOfWork)
        {
            _relationshipTypeRepository = relationshipTypeRepository;
            _unitOfWork = unitOfWork;
        }
        public ResponseViewModel GetRelationshipTypeList(SearchFieldsViewModel relationshipTypeViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var obj = _relationshipTypeRepository.GetAll().Where(g => g.AgencyId == relationshipTypeViewModel.AgencyId && g.IsDeleted == false).ToList();
                if (!string.IsNullOrEmpty(relationshipTypeViewModel.name))
                {
                    obj = obj.Where(x => x.Name.ToLower().Contains(relationshipTypeViewModel.name.ToLower())).Select(x => x).ToList();
                }
                var responsedata = Mapper.Map<List<RelationType>, List<RelationTypeViewModel>>(obj);
                response.IsSuccess = true;
                response.Message = "Relationship type retrieved Successfully";
                response.Content = responsedata;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }
        public int AddUpdateRelationshipType(RelationTypeViewModel relationshipTypeViewModel)
        {
            try
            {
                var IsExistData = _relationshipTypeRepository.FindBy(m => m.Name.ToUpper() == relationshipTypeViewModel.Name.ToUpper() && m.AgencyId == relationshipTypeViewModel.AgencyId && m.IsDeleted == false);
                if (relationshipTypeViewModel.ID == 0)
                {
                    
                    if (IsExistData.ToList().Count > 0)
                    {
                        return 3;
                    }
                    var session = Mapper.Map<RelationTypeViewModel, RelationType>(relationshipTypeViewModel);
                    _relationshipTypeRepository.Add(session);
                }
                else
                {

                    if (IsExistData.ToList().Count > 0)
                        if (IsExistData.ToList().FirstOrDefault().ID != relationshipTypeViewModel.ID)
                        {
                            return 3;
                        }
                    var sessionDetail =
                        _relationshipTypeRepository.FindBy(m => m.ID == relationshipTypeViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (sessionDetail != null)
                    {
                        relationshipTypeViewModel.IsDeleted = false;
                        var session = Mapper.Map<RelationTypeViewModel, RelationType>(relationshipTypeViewModel);
                        _relationshipTypeRepository.Edit(sessionDetail, session);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public ResponseViewModel DeleteRelationshipType(RelationTypeViewModel relationshipTypeViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var obj = Mapper.Map<RelationTypeViewModel, RelationType>(relationshipTypeViewModel);
                _relationshipTypeRepository.SoftDelete(obj);
                _unitOfWork.Commit();
                response.IsSuccess = true;
                response.Message = "Relationship type deleted Successfully";
                response.Content = relationshipTypeViewModel;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
                response.Content = relationshipTypeViewModel;
            }
            return response;
        }

        public void GetAllRelationshipType(SearchRelationTypeListViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<RelationType>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

                var resultData = _relationshipTypeRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var newList = Mapper.Map<List<RelationType>, List<RelationTypeViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = newList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }


    }
}
