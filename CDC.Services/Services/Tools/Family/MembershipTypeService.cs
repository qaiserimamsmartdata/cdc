﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Family;
using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CDC.Services.Services.Tools.Family
{
    public class MembershipTypeService:IMembershipTypeService
    {
        private readonly IEntityBaseRepository<MembershipType> _mebershipTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public MembershipTypeService(
           IEntityBaseRepository<MembershipType> mebershipTypeRepository,
           IUnitOfWork unitOfWork)
        {
            _mebershipTypeRepository = mebershipTypeRepository;
            _unitOfWork = unitOfWork;
        }
        public ResponseViewModel GetMembershipTypeList(SearchFieldsViewModel membershipTypeViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var obj = _mebershipTypeRepository.GetAll().Where(g => g.AgencyId == membershipTypeViewModel.AgencyId && g.IsDeleted == false).ToList();
                if (!string.IsNullOrEmpty(membershipTypeViewModel.name))
                {
                    obj = obj.Where(x => x.Name.ToLower().Contains(membershipTypeViewModel.name.ToLower())).Select(x => x).ToList();
                }
                var responsedata = Mapper.Map<List<MembershipType>, List<MembershipTypeViewModel>>(obj);
                response.IsSuccess = true;
                response.Message = "Membership type retrieved successfully";
                response.Content = responsedata;
            }
            catch (Exception)
            { 
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }
        public int AddUpdateMembershipType(MembershipTypeViewModel membershipTypeViewModel)
        {
            try
            {
                var IsExistData = _mebershipTypeRepository.FindBy(m => m.Name.ToUpper() == membershipTypeViewModel.Name.ToUpper() && m.AgencyId == membershipTypeViewModel.AgencyId && m.IsDeleted == false);
                if (membershipTypeViewModel.ID == 0)
                {
                  
                    if (IsExistData.ToList().Count > 0)
                    {
                        return 3;
                    }
                    var session = Mapper.Map<MembershipTypeViewModel, MembershipType>(membershipTypeViewModel);
                    _mebershipTypeRepository.Add(session);
                }
                else
                {

                    if (IsExistData.ToList().Count > 0)
                        if (IsExistData.ToList().FirstOrDefault().ID != membershipTypeViewModel.ID)
                        {
                            return 3;
                        }
                    var sessionDetail =
                        _mebershipTypeRepository.FindBy(m => m.ID == membershipTypeViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (sessionDetail != null)
                    {
                        membershipTypeViewModel.IsDeleted = false;
                        var session = Mapper.Map<MembershipTypeViewModel, MembershipType>(membershipTypeViewModel);
                        _mebershipTypeRepository.Edit(sessionDetail, session);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public ResponseViewModel DeleteMembershipType(MembershipTypeViewModel membershipTypeViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var obj = Mapper.Map<MembershipTypeViewModel, MembershipType>(membershipTypeViewModel);
                _mebershipTypeRepository.SoftDelete(obj);
                _unitOfWork.Commit();
                response.IsSuccess = true;
                response.Message = "Membership type deleted successfully";
                response.Content = membershipTypeViewModel;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
                response.Content = membershipTypeViewModel;
            }
            return response;
        }

        public void GetAllMembershipType(SearchMembershipTypeListViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<MembershipType>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

                var resultData = _mebershipTypeRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var newList = Mapper.Map<List<MembershipType>, List<MembershipTypeViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = newList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}
