﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Family;
using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using LinqKit;

namespace CDC.Services.Services.Tools.Family
{
    public class InfoSourceService : IInfoSourceService
    {
        private readonly IEntityBaseRepository<InfoSource> _infoSourceRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="InfoSourceService" /> class.
        /// </summary>
        /// <param name="infoSourceRepository">The info source repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public InfoSourceService(
            IEntityBaseRepository<InfoSource> infoSourceRepository, IUnitOfWork unitOfWork
            )
        {
            _infoSourceRepository = infoSourceRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the info source.
        /// </summary>
        /// <param name="addInfoSourceViewModel">Add info source view model.</param>
        /// <returns></returns>
        public int AddInfoSource(InfoSourceViewModel addInfoSourceViewModel)
        {
            try
            {
                var infoSource = new InfoSource
                {
                    ID = addInfoSourceViewModel.ID,
                    Name = addInfoSourceViewModel.Name,
                    IsDeleted = false
                };
                _infoSourceRepository.Add(infoSource);

                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the info source.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<InfoSourceViewModel> GetInfoSource()
        {
            var resultData = _infoSourceRepository.GetAll();

            var returnData = resultData.Select(m =>
                new InfoSourceViewModel
                {
                    ID = m.ID,
                    Name = m.Name
                }).ToList();
            return returnData;
        }

        /// <summary>
        ///     Updates info source.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="infoSourceId"></param>
        /// <returns></returns>
        public int UpdateInfoSource(InfoSourceViewModel model, long infoSourceId)
        {
            var resultData = 0;
            try
            {
                var infoSourceDetail =
                    _infoSourceRepository.FindBy(m => m.ID == infoSourceId && m.IsDeleted == false).FirstOrDefault();
                if (infoSourceDetail != null)
                {
                    infoSourceDetail.Name = model.Name;
                    _unitOfWork.Commit();
                    resultData = 1;
                    return resultData;
                }
                return resultData;
            }
            catch (Exception)
            {
                return resultData;
            }
        }

        /// <summary>
        ///     Deletes info source by identifier.
        /// </summary>
        /// <param name="infoSourceId">The info source identifier.</param>
        /// <returns></returns>
        public int DeleteInfoSourceById(long infoSourceId)
        {
            var returnState = 0;
            try
            {
                long count = _infoSourceRepository.FindBy(d => d.ID == infoSourceId && d.IsDeleted == false).Count();
                if (count <= 0) return returnState;
                var infoSourceDetails =
                    _infoSourceRepository.FindBy(m => m.ID == infoSourceId && m.IsDeleted == false).FirstOrDefault();
                if (infoSourceDetails != null) infoSourceDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void SaveInfoSource()
        {
            _unitOfWork.Commit();
        }


        public ResponseViewModel GetInfoSourceList(SearchFieldsViewModel infoSourceViewModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var obj =
                    _infoSourceRepository.GetAll()
                        .Where(g => g.AgencyId == infoSourceViewModel.AgencyId && g.IsDeleted == false)
                        .ToList();
                if (!string.IsNullOrEmpty(infoSourceViewModel.name))
                {
                    obj = obj.Where(x => x.Name.ToLower().Contains(infoSourceViewModel.name.ToLower())).Select(x => x).ToList();
                }
                var responsedata = Mapper.Map<List<InfoSource>, List<InfoSourceViewModel>>(obj);
                response.IsSuccess = true;
                response.Message = "Info source retrieved successfully";
                response.Content = responsedata;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }

        public int AddUpdateInfoSource(InfoSourceViewModel infoSourceViewModel)
        {
            try
            {
                var IsExistData =
                      _infoSourceRepository.FindBy(
                          m =>
                              m.Name.ToUpper().Trim().Replace(" ", "") == infoSourceViewModel.Name.ToUpper().Trim().Replace(" ", "") &&
                              m.AgencyId == infoSourceViewModel.AgencyId && m.IsDeleted == false);
                if (infoSourceViewModel.ID == 0)
                {
                  
                    if (IsExistData.ToList().Count > 0)
                    {
                        return 3;
                    }
                    var infoSource = Mapper.Map<InfoSourceViewModel, InfoSource>(infoSourceViewModel);
                    _infoSourceRepository.Add(infoSource);
                }
                else
                {

                    if (IsExistData.ToList().Count > 0)
                        if (IsExistData.ToList().FirstOrDefault().ID != infoSourceViewModel.ID)
                        {
                            return 3;
                        }
                    var infoSourceDetail =
                        _infoSourceRepository.FindBy(
                            m =>
                                m.ID == infoSourceViewModel.ID && m.AgencyId == infoSourceViewModel.AgencyId &&
                                m.IsDeleted == false)
                            .FirstOrDefault();
                    if (infoSourceDetail != null)
                    {
                        infoSourceViewModel.IsDeleted = false;
                        var infoSource = Mapper.Map<InfoSourceViewModel, InfoSource>(infoSourceViewModel);
                        _infoSourceRepository.Edit(infoSourceDetail, infoSource);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public ResponseViewModel DeleteInfoSource(InfoSourceViewModel infoSourceViewModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var obj = Mapper.Map<InfoSourceViewModel, InfoSource>(infoSourceViewModel);
                _infoSourceRepository.SoftDelete(obj);
                _unitOfWork.Commit();
                response.IsSuccess = true;
                response.Message = "Info source deleted successfully";
                response.Content = infoSourceViewModel;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
                response.Content = infoSourceViewModel;
            }
            return response;
        }

        public void GetAllInfoSource(SearchInfoSourceListViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<InfoSource>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

                //var resultData = _infoSourceRepository.GetAll();
                //resultData = resultData.Where(m => m.AgencyId == model.AgencyId);
                //long totalCount = resultData.Count();

                var resultData = _infoSourceRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var newList = Mapper.Map<List<InfoSource>, List<InfoSourceViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = newList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

    }
}