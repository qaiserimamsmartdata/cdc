﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Family;
using CDC.Services.Abstract.Tools.Family;
using CDC.Services.Services.Tools.Category;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using LinqKit;

namespace CDC.Services.Services.Tools.Family
{
    public class GradeLevelService : IGradeLevelService
    {
        private readonly IEntityBaseRepository<GradeLevel> _gradeLevelRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryService" /> class.
        /// </summary>
        /// <param name="gradeLevelRepository">The grade level repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public GradeLevelService(
            IEntityBaseRepository<GradeLevel> gradeLevelRepository,
            IUnitOfWork unitOfWork)
        {
            _gradeLevelRepository = gradeLevelRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the grade level.
        /// </summary>
        /// <param name="addGradeLevelViewModel">The add grade level view model.</param>
        /// <returns></returns>
        public int AddGradeLevel(GradeLevelViewModel addGradeLevelViewModel)
        {
            try
            {
                var gradeLevel = new GradeLevel
                {
                    ID = addGradeLevelViewModel.ID,
                    Name = addGradeLevelViewModel.Name,
                    IsDeleted = false
                };
                _gradeLevelRepository.Add(gradeLevel);

                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the grade level.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GradeLevelViewModel> GetGradeLevel()
        {
            var resultData = _gradeLevelRepository.GetAll();

            var returnData = resultData.Select(m =>
                new GradeLevelViewModel
                {
                    ID = m.ID,
                    Name = m.Name
                }).ToList();
            return returnData;
        }

        /// <summary>
        ///     Updates grade level.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="gradeLevelId"></param>
        /// <returns></returns>
        public int UpdateGradeLevel(GradeLevelViewModel model, long gradeLevelId)
        {
            var resultData = 0;
            try
            {
                var gradeLevelDetail =
                    _gradeLevelRepository.FindBy(m => m.ID == gradeLevelId && m.IsDeleted == false).FirstOrDefault();
                if (gradeLevelDetail != null)
                {
                    gradeLevelDetail.Name = model.Name;
                    _unitOfWork.Commit();
                    resultData = 1;
                    return resultData;
                }
                return resultData;
            }
            catch (Exception)
            {
                return resultData;
            }
        }

        /// <summary>
        ///     Deletes gradeLevel by identifier.
        /// </summary>
        /// <param name="gradeLevelId"></param>
        /// <returns></returns>
        public int DeleteGradeLevelById(long gradeLevelId)
        {
            var returnState = 0;
            try
            {
                long count = _gradeLevelRepository.FindBy(d => d.ID == gradeLevelId && d.IsDeleted == false).Count();
                if (count <= 0) return returnState;
                var gradeLevelDetails =
                    _gradeLevelRepository.FindBy(m => m.ID == gradeLevelId && m.IsDeleted == false).FirstOrDefault();
                if (gradeLevelDetails != null) gradeLevelDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void SaveGradeLevel()
        {
            _unitOfWork.Commit();
        }

        #region Yatish Raj

        public ResponseViewModel GetGradeLevelList(SearchFieldsViewModel gradeLevelViewModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var obj =
                    _gradeLevelRepository.GetAll()
                        .Where(g => g.AgencyId == gradeLevelViewModel.AgencyId && g.IsDeleted == false)
                        .ToList();
                if (!string.IsNullOrEmpty(gradeLevelViewModel.name))
                {
                    obj = obj.Where(x => x.Name.ToLower().Contains(gradeLevelViewModel.name.ToLower())).Select(x => x).ToList();
                }
                var responsedata = Mapper.Map<List<GradeLevel>, List<GradeLevelViewModel>>(obj);
                response.IsSuccess = true;
                response.Message = "Grade level retrieved successfully";
                response.Content = responsedata;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;


        }

        public int AddUpdateGradeLevel(GradeLevelViewModel gradeLevelViewModel)
        {
            try
            {
                var IsExistData =
                        _gradeLevelRepository.FindBy(
                            m =>
                                m.Name.ToUpper().Trim().Replace(" ","") == gradeLevelViewModel.Name.ToUpper().Trim().Replace(" ", "") &&
                                m.AgencyId == gradeLevelViewModel.AgencyId && m.IsDeleted == false);
                if (gradeLevelViewModel.ID == 0)
                {

                    if (IsExistData.ToList().Count > 0)
                    {
                        return 3;
                    }
                    var gradeLevel = Mapper.Map<GradeLevelViewModel, GradeLevel>(gradeLevelViewModel);
                    _gradeLevelRepository.Add(gradeLevel);
                }
                else
                {
                  if(IsExistData.ToList().Count > 0)
                    if (IsExistData.ToList().FirstOrDefault().ID!= gradeLevelViewModel.ID)
                    {
                        return 3;
                    }
                    var gradeLevelDetail =
                        _gradeLevelRepository.FindBy(
                            m =>
                                m.ID == gradeLevelViewModel.ID && m.AgencyId == gradeLevelViewModel.AgencyId &&
                                m.IsDeleted == false)
                            .FirstOrDefault();
                    if (gradeLevelDetail != null)
                    {
                        gradeLevelViewModel.IsDeleted = false;
                        var gradeLevel = Mapper.Map<GradeLevelViewModel, GradeLevel>(gradeLevelViewModel);
                        _gradeLevelRepository.Edit(gradeLevelDetail, gradeLevel);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public ResponseViewModel DeleteGradeLevel(GradeLevelViewModel gradeLevelViewModel)
        {
            var response = new ResponseViewModel();
            try
            {
                var obj = Mapper.Map<GradeLevelViewModel, GradeLevel>(gradeLevelViewModel);
                _gradeLevelRepository.SoftDelete(obj);
                _unitOfWork.Commit();
                response.IsSuccess = true;
                response.Message = "Grade level deleted successfully";
                response.Content = gradeLevelViewModel;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
                response.Content = gradeLevelViewModel;
            }
            return response;
        }

        public void GetAllGradeLevel(SearchGradeLevelListViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<GradeLevel>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

                //var resultData = _gradeLevelRepository.GetAll();
                //resultData = resultData.Where(m => m.AgencyId == model.AgencyId);
                //long totalCount = resultData.Count();
                var resultData = _gradeLevelRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var newList = Mapper.Map<List<GradeLevel>, List<GradeLevelViewModel>>(returnData);
   
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = newList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        #endregion
    }
}