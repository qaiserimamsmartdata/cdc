﻿using AutoMapper;
using CDC.Data.AsyncRepositories;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Family;
using CDC.Entities.Tools.Family;
using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Services.Services.Tools.Family
{
    public class SecurityQuestionsService:ISecurityQuestionsService
    {

        private readonly IEntityBaseRepository<SecurityQuestions> _questionsTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SecurityQuestionsService(
           IEntityBaseRepository<SecurityQuestions> questionsTypeRepository,
           IUnitOfWork unitOfWork)
        {
            _questionsTypeRepository = questionsTypeRepository;
            _unitOfWork = unitOfWork;
        }
        public ResponseViewModel GetSecurityQuestionsList(SearchFieldsViewModel questionTypeViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var obj = _questionsTypeRepository.GetAll().ToList();
                
                response.IsSuccess = true;
                response.Content = obj;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }
    }
}
