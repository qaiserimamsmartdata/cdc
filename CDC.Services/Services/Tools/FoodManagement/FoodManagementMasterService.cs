﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Staff;
using CDC.Entities.User;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using LinqKit;
using CDC.Services.Abstract.Tools.FoodManagementMaster;
using CDC.Entities.Tools.FoodManagement;
using CDC.ViewModel.Tools.FoodManagementMaster;
using System.Web.Http;

namespace CDC.Services.Services.Tools.FoodManagement
{
    public class FoodManagementMasterService : IFoodManagementMasterService
    {
        private readonly IEntityBaseRepository<FoodManagementMaster> _foodMasterRepository;
        private readonly IUnitOfWork _unitOfWork;
        public FoodManagementMasterService(
            IEntityBaseRepository<FoodManagementMaster> foodMasterRepository,
            IUnitOfWork unitOfWork)
        {
            _foodMasterRepository = foodMasterRepository;
            _unitOfWork = unitOfWork;

        }
      
        public FoodManagementMasterViewModel GetFoodMasterById(long foodMasterId)
        {
            var obj = _foodMasterRepository.GetSingle(foodMasterId);
            var foodMasterObj = Mapper.Map<FoodManagementMaster, FoodManagementMasterViewModel>(obj);
            return foodMasterObj;
        }
    
        public void GetAllFoodManagementMaster(SearchFoodMasterViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<FoodManagementMaster>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.ItemName))
                    predicate =
                        predicate.And(p => p.MealItemId.ToString() == model.ItemName);
                if (model.MinAgeTo > 0)
                    predicate = predicate.And(p => p.MinAgeTo == model.MinAgeTo);
                if (model.MinAgeFrom > 0)
                    predicate = predicate.And(p => p.MinAgeFrom == model.MinAgeFrom);
                if (model.MaxAgeFrom > 0)
                    predicate = predicate.And(p => p.MaxAgeFrom == model.MaxAgeFrom);
                if (model.MaxAgeTo > 0)
                    predicate = predicate.And(p => p.MaxAgeTo == model.MaxAgeTo);
                if (model.MealTypeId > 0)
                    predicate = predicate.And(p => p.MealTypeId == model.MealTypeId);

                var resultData = _foodMasterRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                //resultData=resultData.OrderBy(m => m.ID);
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "ItemName":
                        resultData = resultData.OrderBy(m => m.MealItem.Name);
                        break;
                    case "-ItemName":
                        resultData = resultData.OrderByDescending(m => m.MealItem.Name);
                        break;
                    //case "PhoneNumber":
                    //    resultData = resultData.OrderBy(m => m.PhoneNumber);
                    //    break;
                    //case "-PhoneNumber":
                    //    resultData = resultData.OrderByDescending(m => m.PhoneNumber);
                    //    break;
                    //case "Email":
                    //    resultData = resultData.OrderBy(m => m.Email);
                    //    break;
                    //case "-Email":
                    //    resultData = resultData.OrderByDescending(m => m.Email);
                    //    break;
                    //case "Gender":
                    //    resultData = resultData.OrderBy(m => m.Gender);
                    //    break;
                    //case "-Gender":
                    //    resultData = resultData.OrderByDescending(m => m.Gender);
                    //    break;
                    //case "DateHired":
                    //    resultData = resultData.OrderBy(m => m.DateHired);
                    //    break;
                    //case "-DateHired":
                    //    resultData = resultData.OrderByDescending(m => m.DateHired);
                    //    break;
                    //case "Designation":
                    //    resultData = resultData.OrderBy(m => m.Position.Name);
                    //    break;
                    //case "-Designation":
                    //    resultData = resultData.OrderByDescending(m
                    //        => m.Position.Name);
                        //break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.MealItem.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                //foreach (var item in returnData)
                //{
                //    if (item.StaffScheduleList.Count > 0)
                //    {
                //        item.StaffScheduleList = item.StaffScheduleList.Where(s => s.IsDeleted == false).ToList();
                //    }
                //}
                var staffLst = Mapper.Map<List<FoodManagementMaster>, List<FoodManagementMasterViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
     
        public void AddFoodMaster(FoodManagementMasterViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {

                var FoodManagementMaster = Mapper.Map<FoodManagementMasterViewModel, FoodManagementMaster>(model);

                _foodMasterRepository.Add(FoodManagementMaster);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Food details added successfully");
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }


      
        public void UpdateFoodMaster(FoodManagementMasterViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var foodDetail =
                    _foodMasterRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapFoodManagementMasterViewModelInfo = Mapper.Map<FoodManagementMasterViewModel, FoodManagementMaster>(model);
                _foodMasterRepository.Edit(foodDetail, mapFoodManagementMasterViewModelInfo);
                _unitOfWork.Commit();

                responseInfo.ReturnStatus = true;
                responseInfo.ReturnMessage.Add("Food details updated successfully");

            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
     
        public void DeleteFoodMasterById(long foodMasterId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var staffDetails =
                    _foodMasterRepository.FindBy(m => m.ID == foodMasterId && m.IsDeleted == false).FirstOrDefault();
                if (staffDetails != null && staffDetails.ID > 0)
                {
                    staffDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Food details successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}