﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Services.Abstract.Tools.Category;
using CDC.ViewModel.Tools.Category;
using CDC.ViewModel.Common;
using LinqKit;
using CDC.Entities.Tools.Category;
using AutoMapper;

namespace CDC.Services.Services.Tools.Category
{
    /// <summary>
    /// </summary>
    /// <seealso cref="CDC.Services.Abstract.Tools.Category.ICategoryService" />
    public class CategoryService : ICategoryService
    {
        private readonly IEntityBaseRepository<Entities.Tools.Category.Category> _categoryRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryService" /> class.
        /// </summary>
        /// <param name="categoryRepository">The category repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public CategoryService(
            IEntityBaseRepository<Entities.Tools.Category.Category> categoryRepository, IUnitOfWork unitOfWork
            )
        {
            _categoryRepository = categoryRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the category.
        /// </summary>
        /// <param name="categoryViewModel">The add category view model.</param>
        /// <returns></returns>
        public int AddCategory(CategoryViewModel categoryViewModel)
        {
            try
            {
                var IsExistData =
                    _categoryRepository.FindBy(
                        m =>
                            m.Name.ToUpper().Trim().Replace(" ", "") == categoryViewModel.Name.ToUpper().Trim().Replace(" ", "") &&
                            m.AgencyId == categoryViewModel.AgencyId && m.IsDeleted == false);
                if (IsExistData.ToList().Count > 0)
                    return 3;
                var category = new Entities.Tools.Category.Category
                {
                    ID = categoryViewModel.ID,
                    Name = categoryViewModel.Name,
                    IsDeleted = false,
                    AgencyId = categoryViewModel.AgencyId,
                   
                };
                _categoryRepository.Add(category);

                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the category.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CategoryViewModel> GetCategory(SearchFieldsViewModel model)
        {
            var resultData = _categoryRepository.GetAll().Where(m=>m.AgencyId==model.AgencyId);
            if (!string.IsNullOrEmpty(model.name))
            {
                resultData = resultData.Where(x => x.Name.ToLower().Contains(model.name.ToLower())).Select(x => x).AsQueryable();
            }
            var returnData = resultData.Select(m =>
                new CategoryViewModel
                {
                    ID = m.ID,
                    Name = m.Name
                }).ToList();
            return returnData;
        }

        /// <summary>
        ///     Updates the category.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        public int UpdateCategory(CategoryViewModel model, long categoryId)
        {
            var resultData = 0;
            try
            {
                var IsExistData =
                 _categoryRepository.FindBy(
                     m =>
                         m.Name.ToUpper().Trim().Replace(" ", "") == model.Name.ToUpper().Trim().Replace(" ", "") && m.AgencyId == model.AgencyId && m.IsDeleted == false);

                if (IsExistData.ToList().Count > 0)
                    if (IsExistData.FirstOrDefault().ID != categoryId)
                        return 3;
                    var categoryDetail =
                    _categoryRepository.FindBy(m => m.ID == categoryId && m.IsDeleted == false).FirstOrDefault();
              
                if (categoryDetail != null)
                {
                    categoryDetail.Name = model.Name;
                    _unitOfWork.Commit();
                    resultData = 1;
                    return resultData;
                }
                return resultData;
            }
            catch (Exception)
            {
                return resultData;
            }
        }

        /// <summary>
        ///     Deletes the category by identifier.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        public int DeleteCategoryById(long categoryId)
        {
            var returnState = 0;
            try
            {
                long count = _categoryRepository.FindBy(d => d.ID == categoryId && d.IsDeleted == false).Count();
                if (count <= 0) return returnState;
                var categoryDetails =
                    _categoryRepository.FindBy(m => m.ID == categoryId && m.IsDeleted == false).FirstOrDefault();
                if (categoryDetails != null) categoryDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch(Exception ex)
            {
                return returnState;
            }
        }

        public void SaveCategory()
        {
            _unitOfWork.Commit();
        }

        public void GetAllCategory(SearchCategoryListViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Entities.Tools.Category.Category>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

                var resultData = _categoryRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var newList = Mapper.Map<List<Entities.Tools.Category.Category>, List<CategoryViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = newList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

    }
}