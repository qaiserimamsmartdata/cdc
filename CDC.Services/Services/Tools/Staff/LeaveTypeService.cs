﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Staff;
using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Tools.Staff;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CDC.Services.Services.Tools.Staff
{
    public class LeaveTypeService : ILeaveTypeService
    {
        private readonly IEntityBaseRepository<LeaveType> _leaveTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeaveTypeService"/> class.
        /// </summary>
        /// <param name="leaveTypeRepository">The leave type repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public LeaveTypeService(
            IEntityBaseRepository<LeaveType> leaveTypeRepository, IUnitOfWork unitOfWork
            )
        {
            _leaveTypeRepository = leaveTypeRepository;
            _unitOfWork = unitOfWork;
        }
         /// <summary>
        ///     Adds the update LeaveType.
        /// </summary>
        /// <param name="addLeaveTypeViewModel">The add LeaveType view model.</param>
        /// <returns></returns>
        public int AddUpdateLeaveType(LeaveTypeViewModel addLeaveTypeViewModel)
        {
            try
            {
                if (addLeaveTypeViewModel.ID == 0)
                {
                    var leaveType = Mapper.Map<LeaveTypeViewModel, LeaveType>(addLeaveTypeViewModel);
                    _leaveTypeRepository.Add(leaveType);
                }
                else
                {
                    var leaveTypeOld =
                        _leaveTypeRepository.FindBy(m => m.ID == addLeaveTypeViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (leaveTypeOld != null)
                    {
                        addLeaveTypeViewModel.IsDeleted = false;
                        var leaveTypeNew = Mapper.Map<LeaveTypeViewModel, LeaveType>(addLeaveTypeViewModel);
                        _leaveTypeRepository.Edit(leaveTypeOld, leaveTypeNew);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        /// <summary>
        ///     Gets the LeaveType.
        /// </summary>
        /// <returns></returns>
        public List<LeaveTypeViewModel> GetLeaveType()
        {
            var leaveTypeResult = new List<LeaveTypeViewModel>();
            try
            {
                var resultData = _leaveTypeRepository.GetAll().ToList();
                if(resultData.Count > 0)
                {
                    leaveTypeResult = Mapper.Map<List<LeaveType>, List<LeaveTypeViewModel>>(resultData);
                }
                return leaveTypeResult;
            }
            catch (Exception)
            {
                return leaveTypeResult;
            }
        }
        /// <summary>
        /// Deletes the leave type by identifier.
        /// </summary>
        /// <param name="leaveTypeId">The leave type identifier.</param>
        /// <returns></returns>
        public int DeleteLeaveTypeById(long leaveTypeId)
        {
            var returnState = 0;
            try
            {
                long resultCount = _leaveTypeRepository.FindBy(d => d.ID == leaveTypeId && d.IsDeleted == false).Count();
                if (resultCount <= 0) return returnState;
                var leaveTypeDetails =
                    _leaveTypeRepository.FindBy(m => m.ID == leaveTypeId && m.IsDeleted == false).FirstOrDefault();
                if (leaveTypeDetails != null) leaveTypeDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }
    }
}
