﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Staff;
using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Tools.Staff;
using CDC.ViewModel.Common;
using LinqKit;

namespace CDC.Services.Services.Tools.Staff
{
    /// <summary>
    /// </summary>
    /// <seealso>
    ///     <cref>CDC.Services.Abstract.Tools.Class.IdesignationService</cref>
    /// </seealso>
    public class DesignationService : IDesignationService
    {
        private readonly IEntityBaseRepository<Position> _designationRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DesignationService" /> class.
        /// </summary>
        /// <param name="designationRepository">The designation repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public DesignationService(
            IEntityBaseRepository<Position> designationRepository, IUnitOfWork unitOfWork
            )
        {
            _designationRepository = designationRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the update designation.
        /// </summary>
        /// <param name="addDesignationViewModel"></param>
        /// <returns></returns>
        public int AddUpdateDesignation(PositionViewModel addDesignationViewModel)
        {
            try
            {
                if (addDesignationViewModel.ID == 0)
                {
                    //var count = _designationRepository.FindBy(m => m.Name.ToUpper() == addDesignationViewModel.Name.ToUpper() 
                    //                            && m.AgencyId == addDesignationViewModel.AgencyId && m.IsDeleted == false).Count();
                    //if (count > 0)
                    //{
                    //    return 3;
                    //}
                    var designation = Mapper.Map<PositionViewModel, Position>(addDesignationViewModel);
                    _designationRepository.Add(designation);
                }
                else
                {
                    //var count = _designationRepository.FindBy(m => m.Name.ToUpper() == addDesignationViewModel.Name.ToUpper()
                    //                            && m.AgencyId == addDesignationViewModel.AgencyId && m.IsDeleted == false).Count();
                    //if (count > 0)
                    //{
                    //    return 3;
                    //}
                    var designationDetail =
                        _designationRepository.FindBy(m => m.ID == addDesignationViewModel.ID)
                            .FirstOrDefault();
                    if (designationDetail != null)
                    {
                        addDesignationViewModel.IsDeleted = false;
                        var designation = Mapper.Map<PositionViewModel, Position>(addDesignationViewModel);
                        _designationRepository.Edit(designationDetail, designation);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the designation.
        /// </summary>
        /// <returns></returns>
        public List<PositionViewModel> GetDesignation(SearchFieldsViewModel model)
        {
            var resultData = _designationRepository.GetAll().Where(m => m.AgencyId == model.AgencyId && m.IsDeleted == false).ToList();
            if (!string.IsNullOrEmpty(model.name))
            {
                resultData = resultData.Where(x => x.Name.Contains(model.name)).Select(x => x).ToList();
            }
            var designationResult = Mapper.Map<List<Position>, List<PositionViewModel>>(resultData);
            return designationResult;
        }


        /// <summary>
        ///     Deletes the designation by identifier.
        /// </summary>
        /// <param name="designationId">The designation identifier.</param>
        /// <returns></returns>
        public int DeleteDesignationById(PositionViewModel model)
        {
            var returnState = 0;
            try
            {
                long count =
                    _designationRepository.FindBy(d => d.ID == model.ID && d.IsDeleted == false)
                        .Count();
                if (count <= 0) return returnState;
                var designationDetails =
                    _designationRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (designationDetails != null) designationDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void GetAllDesignation(SearchPositionViewModel model,out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Position>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));
      
                //var resultData = _designationRepository.GetAll();
                //resultData = resultData.Where(m => m.AgencyId == model.AgencyId);
                //long totalCount = resultData.Count();

                var resultData = _designationRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if(string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var positionList = Mapper.Map<List<Position>, List<PositionViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = positionList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}