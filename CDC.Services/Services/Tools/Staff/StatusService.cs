﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Staff;
using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Tools.Staff;
using CDC.ViewModel.Common;

namespace CDC.Services.Services.Tools.Staff
{
    /// <summary>
    /// </summary>
    /// <seealso cref="CDC.Services.Abstract.Tools.Staff.IStatusServices" />
    public class StatusService : IStatusServices
    {
        private readonly IEntityBaseRepository<Status> _statusRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StatusService" /> class.
        /// </summary>
        /// <param name="statusRepository">The Status repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public StatusService(
            IEntityBaseRepository<Status> statusRepository, IUnitOfWork unitOfWork
            )
        {
            _statusRepository = statusRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the update Status.
        /// </summary>
        /// <param name="addStatusViewModel">The add Status view model.</param>
        /// <returns></returns>
        public int AddUpdateStatus(StatusViewModel addStatusViewModel)
        {
            try
            {
                if (addStatusViewModel.ID == 0)
                {
                    var status = Mapper.Map<StatusViewModel, Status>(addStatusViewModel);
                    _statusRepository.Add(status);
                }
                else
                {
                    var statusDetail =
                        _statusRepository.FindBy(m => m.ID == addStatusViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (statusDetail != null)
                    {
                        addStatusViewModel.IsDeleted = false;
                        var status = Mapper.Map<StatusViewModel, Status>(addStatusViewModel);
                        _statusRepository.Edit(statusDetail, status);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the Status.
        /// </summary>
        /// <returns></returns>
        public List<StatusViewModel> GetStatus()
        {
            var statusList = new List<StatusViewModel>();
            try
            {
                var resultData = _statusRepository.GetAll().ToList();
                if (resultData.Count > 0)
                {
                    statusList = Mapper.Map<List<Status>, List<StatusViewModel>>(resultData);
                }
                return statusList;
            }
            catch (Exception)
            {
                return statusList;
            }
        }

        public int DeleteStatusById(long statusId)
        {
            var returnState = 0;
            try
            {
                long count = _statusRepository.FindBy(d => d.ID == statusId && d.IsDeleted == false).Count();
                if (count <= 0) return returnState;
                var statusDetails =
                    _statusRepository.FindBy(m => m.ID == statusId && m.IsDeleted == false).FirstOrDefault();
                if (statusDetails != null) statusDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }
    }
}