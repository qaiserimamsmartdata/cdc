﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Staff;
using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;

namespace CDC.Services.Services.Tools.Staff
{
    public class PayTypeService : IPayTypeService
    {
        private readonly IEntityBaseRepository<PayType> _payTypeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PayTypeService
            (IEntityBaseRepository<PayType> payTypeRepository,
                IUnitOfWork unitOfWork)
        {
            _payTypeRepository = payTypeRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the update PayType.
        /// </summary>
        /// <param name="addPayTypeViewModel">The add PayType view model.</param>
        /// <param name="responseInfo"></param>
        /// <returns></returns>
        public void AddUpdatePayType(PayTypeViewModel addPayTypeViewModel, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                if (addPayTypeViewModel.ID == 0)
                {
                    var payType = Mapper.Map<PayTypeViewModel, PayType>(addPayTypeViewModel);
                    _payTypeRepository.Add(payType);
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("PayType added successfully");
                }
                else
                {
                    var payTypeDetail =
                        _payTypeRepository.FindBy(m => m.ID == addPayTypeViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (payTypeDetail != null)
                    {
                        addPayTypeViewModel.IsDeleted = false;
                        var payType = Mapper.Map<PayTypeViewModel, PayType>(addPayTypeViewModel);
                        _payTypeRepository.Edit(payTypeDetail, payType);
                    }
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("PayType successfully updated at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Gets the PayType.
        /// </summary>
        /// <returns></returns>
        public void GetPayType(out ResponseViewModel responseInfo)
        {
            //var payTypeResult = new List<PayTypeViewModel>();
            responseInfo = new ResponseViewModel();
            try
            {
                var resultData = _payTypeRepository.GetAll().ToList();
                var payTypeResult = Mapper.Map<List<PayType>, List<PayTypeViewModel>>(resultData);
                responseInfo.TotalRows = resultData.Count;
                responseInfo.IsSuccess = true;
                responseInfo.Content = payTypeResult;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Deletes the pay type by identifier.
        /// </summary>
        /// <param name="payTypeId">The pay type identifier.</param>
        /// <param name="responseInfo">The response information.</param>
        public void DeletePayTypeById(long payTypeId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var payTypeDetails =
                    _payTypeRepository.FindBy(m => m.ID == payTypeId && m.IsDeleted == false).FirstOrDefault();
                if (payTypeDetails != null && payTypeDetails.ID > 0)
                {
                    payTypeDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Paytype successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }
    }
}