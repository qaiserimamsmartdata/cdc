﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Class;
using CDC.Services.Abstract.Tools.Class;
using CDC.ViewModel.Tools.Class;
using CDC.ViewModel.Common;
using LinqKit;

namespace CDC.Services.Services.Tools.Class
{
    /// <summary>
    ///     The Session Service
    /// </summary>
    /// <seealso cref="CDC.Services.Abstract.Tools.Class.ISessionService" />
    public class SessionService : ISessionService
    {
        private readonly IEntityBaseRepository<Session> _sessionRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SessionService" /> class.
        /// </summary>
        /// <param name="sessionRepository">The session repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public SessionService(
            IEntityBaseRepository<Session> sessionRepository, IUnitOfWork unitOfWork
            )
        {
            _sessionRepository = sessionRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the update session.
        /// </summary>
        /// <param name="addSessionViewModel">The add session view model.</param>
        /// <returns></returns>
        public int AddUpdateSession(SessionViewModel addSessionViewModel)
        {
            try
            {
                if (addSessionViewModel.ID == 0)
                {
                    var count = _sessionRepository.FindBy(m => m.Name.ToUpper() == addSessionViewModel.Name.ToUpper() && m.AgencyId == addSessionViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var session = Mapper.Map<SessionViewModel, Session>(addSessionViewModel);
                    _sessionRepository.Add(session);
                }
                else
                {
                    var count = _sessionRepository.FindBy(m => m.Name.ToUpper() == addSessionViewModel.Name.ToUpper() && m.AgencyId == addSessionViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var sessionDetail =
                        _sessionRepository.FindBy(m => m.ID == addSessionViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (sessionDetail != null)
                    {
                        addSessionViewModel.IsDeleted = false;
                        var session = Mapper.Map<SessionViewModel, Session>(addSessionViewModel);
                        _sessionRepository.Edit(sessionDetail, session);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)  
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the session.
        /// </summary>
        /// <returns></returns>
        public List<SessionViewModel> GetSession(SearchFieldsViewModel addSessionViewModel)
        {
            var resultData = _sessionRepository.GetAll().Where(g => g.AgencyId == addSessionViewModel.AgencyId && g.IsDeleted == false).ToList();
            if (!string.IsNullOrEmpty(addSessionViewModel.name))
            {
                resultData = resultData.Where(x => x.Name.ToLower().Contains(addSessionViewModel.name.ToLower())).Select(x => x).ToList();
            }
            var session = Mapper.Map<List<Session>, List<SessionViewModel>>(resultData);

            return session;
        }



        /// <summary>
        ///     Deletes the session by identifier.
        /// </summary>
        /// <param name="addSessionViewModel"></param>
        /// <returns></returns>
        public int DeleteSessionById(SessionViewModel addSessionViewModel)
        {
            var returnState = 0;
            try
            {
                long count = _sessionRepository.FindBy(d => d.ID == addSessionViewModel.ID && d.IsDeleted == false).Count();
                if (count <= 0) return returnState;
                var sessionDetails =
                    _sessionRepository.FindBy(m => m.ID == addSessionViewModel.ID && m.IsDeleted == false).FirstOrDefault();
                if (sessionDetails != null) sessionDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void GetAllSession(SearchSessionListViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Session>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

                var resultData = _sessionRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var newList = Mapper.Map<List<Session>, List<SessionViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = newList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

    }
}