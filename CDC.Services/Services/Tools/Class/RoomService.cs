﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Class;
using CDC.Services.Abstract.Tools.Class;
using CDC.ViewModel.Tools.Class;
using LinqKit;
using CDC.ViewModel.Common;

namespace CDC.Services.Services.Tools.Class
{
    /// <summary>
    /// </summary>
    /// <seealso cref="CDC.Services.Abstract.Tools.Class.IRoomService" />
    public class RoomService : IRoomService
    {
        private readonly IEntityBaseRepository<Room> _roomRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoomService" /> class.
        /// </summary>
        /// <param name="roomRepository">The room repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public RoomService(
            IEntityBaseRepository<Room> roomRepository, IUnitOfWork unitOfWork
            )
        {
            _roomRepository = roomRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the update room.
        /// </summary>
        /// <param name="addRoomViewModel">The add room view model.</param>
        /// <returns></returns>
        public int AddUpdateRoom(RoomViewModel addRoomViewModel)
        {
            try
            {
                if (addRoomViewModel.ID == 0)
                {
                    var count = _roomRepository.FindBy(m => m.Name.ToUpper().Trim().Replace(" ", "") == addRoomViewModel.Name.ToUpper().Trim().Replace(" ", "") && m.AgencyId == addRoomViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var room = Mapper.Map<RoomViewModel, Room>(addRoomViewModel);
                    _roomRepository.Add(room);
                }
                else
                {
                    var count = _roomRepository.FindBy(m => m.Name.ToUpper().Trim().Replace(" ", "") == addRoomViewModel.Name.ToUpper().Trim().Replace(" ", "") && m.AgencyId == addRoomViewModel.AgencyId && m.ID != addRoomViewModel.ID && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var roomDetail =
                        _roomRepository.FindBy(m => m.ID == addRoomViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (roomDetail != null)
                    {
                        addRoomViewModel.IsDeleted = false;
                        var room = Mapper.Map<RoomViewModel, Room>(addRoomViewModel);
                        _roomRepository.Edit(roomDetail, room);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the room.
        /// </summary>
        /// <returns></returns>

        public List<RoomViewModel> GetRoom(RoomViewModel addRoomViewModel)
        {
            
            var predicate = PredicateBuilder.True<Room>();
            if (addRoomViewModel.AgencyLocationInfoId > 0)
                predicate = predicate.And(p => p.AgencyLocationInfoId == addRoomViewModel.AgencyLocationInfoId);
            if (!string.IsNullOrEmpty(addRoomViewModel.Name))
            {
                predicate = predicate.And(p => p.Name.ToLower().Contains(addRoomViewModel.Name.ToLower()));
            }
            var resultData = _roomRepository.GetAll().Where(g => g.AgencyId == addRoomViewModel.AgencyId && g.IsDeleted == false);
            var returnData1 = resultData.AsExpandable().Where(predicate).AsExpandable().ToList();
           
           
          
            var room = Mapper.Map<List<Room>, List<RoomViewModel>>(returnData1);
            return room;
        }

        /// <summary>
        ///     Deletes the room by identifier.
        /// </summary>
        /// <param name="addRoomViewModel"></param>
        /// <returns></returns>
        public int DeleteRoomById(RoomViewModel addRoomViewModel)
        {
            var returnState = 0;
            try
            {
                long count = _roomRepository.FindBy(d => d.ID == addRoomViewModel.ID && d.AgencyId == addRoomViewModel.AgencyId && d.IsDeleted == false).Count();
                if (count <= 0) return returnState;
                var roomDetails =
                    _roomRepository.FindBy(m => m.ID == addRoomViewModel.ID && m.AgencyId == addRoomViewModel.AgencyId && m.IsDeleted == false).FirstOrDefault();
                if (roomDetails != null) roomDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void GetAllRoom(SearchRoomListViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<Room>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

                var resultData = _roomRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var newList = Mapper.Map<List<Room>, List<RoomViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = newList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

    }
}