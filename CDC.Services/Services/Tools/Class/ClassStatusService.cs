﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Tools.Class;
using CDC.Services.Abstract.Tools.Class;
using CDC.ViewModel.Tools.Class;
using CDC.ViewModel.Common;
using LinqKit;

namespace CDC.Services.Services.Tools.Class
{
    /// <summary>
    ///     The Status Service
    /// </summary>
    /// <seealso>
    ///     <cref>CDC.Services.Abstract.Tools.Class.IStatusService</cref>
    /// </seealso>
    public class ClassStatusService : IClassStatusService
    {
        private readonly IEntityBaseRepository<ClassStatus> _classstatusRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ClassStatusService" /> class.
        /// </summary>
        /// <param>
        ///     The class status repository.
        ///     <name>classStatusRepository</name>
        /// </param>
        /// <param name="classstatusRepository"></param>
        /// <param name="unitOfWork">The unit of work.</param>
        public ClassStatusService(
            IEntityBaseRepository<ClassStatus> classstatusRepository, IUnitOfWork unitOfWork
            )
        {
            _classstatusRepository = classstatusRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the update class status.
        /// </summary>
        /// <param name="addclassstatusViewModel"></param>
        /// <returns></returns>
        public int AddUpdateClassStatus(ClassStatusViewModel addclassstatusViewModel)
        {
            try
            {
                if (addclassstatusViewModel.ID == 0)
                {
                    var count =
                        _classstatusRepository.FindBy(
                            m =>
                                m.Name.ToUpper() == addclassstatusViewModel.Name.ToUpper() &&
                                m.AgencyId == addclassstatusViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var classStatus = Mapper.Map<ClassStatusViewModel, ClassStatus>(addclassstatusViewModel);
                    _classstatusRepository.Add(classStatus);
                }
                else
                {
                    var count =
                        _classstatusRepository.FindBy(
                            m =>
                                m.Name.ToUpper() == addclassstatusViewModel.Name.ToUpper() &&
                                m.AgencyId == addclassstatusViewModel.AgencyId && m.IsDeleted == false).Count();
                    if (count > 0)
                    {
                        return 3;
                    }
                    var classStatusDetail =
                        _classstatusRepository.FindBy(m => m.ID == addclassstatusViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (classStatusDetail != null)
                    {
                        addclassstatusViewModel.IsDeleted = false;
                        var classStatus = Mapper.Map<ClassStatusViewModel, ClassStatus>(addclassstatusViewModel);
                        _classstatusRepository.Edit(classStatusDetail, classStatus);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        ///     Gets the class status.
        /// </summary>
        /// <returns></returns>
        public List<ClassStatusViewModel> GetClassStatus(SearchFieldsViewModel addclassstatusViewModel)
        {
            var resultData =
                _classstatusRepository.GetAll()
                    .Where(g => g.AgencyId == addclassstatusViewModel.AgencyId && g.IsDeleted == false)
                    .ToList();
            if (!string.IsNullOrEmpty(addclassstatusViewModel.name))
            {
                resultData = resultData.Where(x => x.Name.ToLower().Contains(addclassstatusViewModel.name.ToLower())).Select(x => x).ToList();
            }
            var classStatus = Mapper.Map<List<ClassStatus>, List<ClassStatusViewModel>>(resultData);

            return classStatus;
        }

        /// <summary>
        ///     Deletes the class status by identifier.
        /// </summary>
        /// <returns></returns>
        public int DeleteClassStatusById(ClassStatusViewModel addclassstatusViewModel)
        {
            var returnState = 0;
            try
            {
                long count =
                    _classstatusRepository.FindBy(d => d.ID == addclassstatusViewModel.ID && d.IsDeleted == false)
                        .Count();
                if (count <= 0) return returnState;
                var classStatusDetails =
                    _classstatusRepository.FindBy(m => m.ID == addclassstatusViewModel.ID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (classStatusDetails != null) classStatusDetails.IsDeleted = true;
                _unitOfWork.Commit();
                returnState = 2;
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        public void GetAllClassStatus(SearchClassStatusListViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<ClassStatus>();
                predicate = predicate.And(p => p.IsDeleted == false);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.Name))
                    predicate = predicate.And(p => p.Name.ToLower().Contains(model.Name.ToLower()));

    
                var resultData = _classstatusRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();

                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;

                    case "Name":
                        resultData = resultData.OrderBy(m => m.Name);
                        break;

                    case "-Name":
                        resultData = resultData.OrderByDescending(m => m.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Name);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var newList = Mapper.Map<List<ClassStatus>, List<ClassStatusViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = newList;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}