using System;
using System.Linq;
using System.Security.Principal;
using CDC.Services.Abstract.User;
using CDC.Entities.User;
using CDC.Data.Repositories;
using CDC.ViewModel.Common;

namespace CDC.Services.Services.User
{
    public class MembershipService : IMembershipService
    {
        #region Variables
        
        private readonly IEntityBaseRepository<Users> _userRepository;

        #endregion
        public MembershipService(IEntityBaseRepository<Users> userRepository)
        {
            _userRepository = userRepository;
        }

       
        public ResponseViewModel ValidateToken(string Token)
        {
            var membershipCtx = new ResponseViewModel(); 
            
            var user = _userRepository.FindBy(x=>x.Token==Token).FirstOrDefault();
            if (user != null && DateTime.UtcNow < user.TokenExpiryDate)
            {
                membershipCtx.user = user;
                string[] rolesArr = {user.Role.RoleName};
                var identity = new GenericIdentity(user.ID.ToString());
                membershipCtx.principal = new GenericPrincipal(
                    identity,rolesArr);
            }
            return membershipCtx;
        }
    
    }
}
