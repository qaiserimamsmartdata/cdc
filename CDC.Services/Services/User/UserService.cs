﻿using System;
using System.Linq;
using AutoMapper;
using CDC.Data.Repositories;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Staff;
using CDC.Entities.User;
using CDC.Services.Abstract.User;
using CDC.ViewModel;
using CDC.ViewModel.AgencyRegistration;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using CDC.ViewModel.User;
using CDC.Data.Infrastructure;
using System.Collections.Generic;
using CDC.Entities.Schedule;
using CDC.Entities.Class;
using CDC.Entities.Family;
using CDC.ViewModel.Family;
using System.Text;
using CDC.Services.Abstract;
using System.Security.Cryptography;
using System.IO;
using CDC.Entities.NewParent;
using CDC.ViewModel.NewParent;

namespace CDC.Services.Services.User
{
    public class UserService : IUserService
    {
        private readonly IEntityBaseRepository<AgencyRegistrationInfo> _agencyRegistrationInfoRepository;
        private readonly IEntityBaseRepository<Roles> _rolesRepository;
        private readonly IEntityBaseRepository<StaffInfo> _staffInfoRepository;
        private readonly IEntityBaseRepository<Users> _userRepository;
        private readonly IEntityBaseRepository<ClassInfo> _classInfoRepository;
        private readonly IEntityBaseRepository<ForgotPasswordLog> _forgotPasswordLogRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentscheduleRepository;
        private readonly IEntityBaseRepository<ParentInfo> _parentInfoRepository;
        private readonly IEntityBaseRepository<tblParentInfo> _tblParentInfoRepository;
        private readonly IEntityBaseRepository<FamilyInfo> _familyInfoRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommonService _iCommonService;
        private static readonly byte[] Key =
       {
            0x12, 0xe3, 0x4a, 0xa1, 0x45, 0xd2, 0x56, 0x7c, 0x54, 0xac, 0x67, 0x9f,
            0x45, 0x6e, 0xaa, 0x56
        };

        // Initialization Vector to be used for encryption
        private static readonly byte[] IV = { 0x12, 0xe3, 0x4a, 0xa1, 0x45, 0xd2, 0x56, 0x7c };


        public UserService(IEntityBaseRepository<Users> userRepository,
            IEntityBaseRepository<AgencyRegistrationInfo> agencyRegistrationInfoRepository,
            IEntityBaseRepository<ClassInfo> classInfoRepository,
            IEntityBaseRepository<StudentSchedule> studentscheduleRepository,
            IEntityBaseRepository<StaffInfo> staffInfoRepository, IEntityBaseRepository<Roles> rolesRepository,
            IEntityBaseRepository<ParentInfo> parentInfoRepository,
            IEntityBaseRepository<tblParentInfo> tblParentInfoRepository,
            IEntityBaseRepository<FamilyInfo> familyInfoRepository,
            IEntityBaseRepository<ForgotPasswordLog> forgotPasswordLogRepository, IUnitOfWork unitOfWork,ICommonService iCommonService)
        {
            _userRepository = userRepository;
            _classInfoRepository = classInfoRepository;
            _tblParentInfoRepository = tblParentInfoRepository;
            _agencyRegistrationInfoRepository = agencyRegistrationInfoRepository;
            _staffInfoRepository = staffInfoRepository;
            _rolesRepository = rolesRepository;
            _forgotPasswordLogRepository = forgotPasswordLogRepository;
            _unitOfWork = unitOfWork;
            _studentscheduleRepository = studentscheduleRepository;
            _parentInfoRepository = parentInfoRepository;
            _familyInfoRepository = familyInfoRepository;
            _iCommonService = iCommonService;
        }
        public static string GetDecryptedData(string input)
        {
            try
            {
                //Modified By Sanvir to use Smartcare Decryption Algos on Nov 03,2007
                var tripledes = new TripleDESCryptoServiceProvider();
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, tripledes.CreateDecryptor(Key, IV), CryptoStreamMode.Write);
                var cipherbytes = Convert.FromBase64String(input);
                cs.Write(cipherbytes, 0, cipherbytes.Length);
                cs.FlushFinalBlock();

                //construct the string
                var decryptedArray = ms.ToArray();
                var characters = new char[43693];
                var dec = Encoding.UTF8.GetDecoder();

                var charlen = dec.GetChars(decryptedArray, 0, decryptedArray.Length, characters, 0);
                var decrpytedString = new string(characters, 0, charlen);
                return decrpytedString;
            }
            catch (Exception)
            {
                return input;
            }
        }
        public bool IsExistUserName(string name)
        {
            var isExist = false;
            try
            {

                var tempData = _agencyRegistrationInfoRepository.FindBy(x => x.Email == name).FirstOrDefault();
                if (tempData != null && tempData.ID > 0)
                    isExist = true;
            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;
        }
        public bool EnrolledCapacity(IDViewModel model)
        {
            var isExist = false;
            try
            {

                var count = _studentscheduleRepository.FindBy(x => x.ClassId == model.ClassId && x.IsEnrolled).ToList().Count();

                var classCount = _classInfoRepository.FindBy(x => x.ID == model.ClassId).Select(m => m.EnrollCapacity).FirstOrDefault();

                if (count == classCount)
                {
                    isExist = true;
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return isExist;
        }
       
        public ResponseViewModel FindUser(string userName, string password)
        {
            var responseViewModelInfo = new ResponseViewModel();
            try
            {
                var user = _userRepository.FindBy(m => m.EmailId == userName && m.Password == password).OrderByDescending(m => m.ID).FirstOrDefault();
                var userLoginInfo = Mapper.Map<Users, UserViewModel>(user);
                if (userLoginInfo != null)
                {
                    ///for token generation data
                    if ((!string.IsNullOrEmpty(user.Token) && user.TokenExpiryDate == DateTime.UtcNow) || string.IsNullOrEmpty(user.Token))
                    {
                        
                        user.Token = _iCommonService.RandomString(150);
                        user.TokenExpiryDate = DateTime.UtcNow.AddMonths(1);
                    }
                    responseViewModelInfo.Token = user.Token;   
                    _userRepository.Edit(user, user);
                    _unitOfWork.Commit();
                    ////////////////////////////////////////////
                    var roleName =
                        _rolesRepository.FindBy(m => m.ID == userLoginInfo.RoleId)
                            .Select(m => m.RoleName)
                            .FirstOrDefault();
                    if (roleName != null && roleName.ToUpper() == AppConstants.Agency.ToUpper())
                    {
                        var agencyInfo =
                            _agencyRegistrationInfoRepository.FindBy(m => m.UserId == user.ID).FirstOrDefault();
                        var agencyViewModelinfo =
                            Mapper.Map<AgencyRegistrationInfo, AgencyRegistrationViewModel>(agencyInfo);
                        if (agencyInfo.AgencyTimeZones.Count() > 0)
                            agencyViewModelinfo.TimeZone = agencyInfo.AgencyTimeZones.FirstOrDefault().TimeZone;
                        agencyViewModelinfo.RoleId = userLoginInfo.RoleId;
                        agencyViewModelinfo.RoleName = AppConstants.Agency.ToUpper();
                        responseViewModelInfo.Content = agencyViewModelinfo;

                        ///added for calculation of trial period
                        //response.UtcDate = DateTime.UtcNow;
                        int days = 0;
                        if (agencyViewModelinfo.IsTrial != null && agencyViewModelinfo.IsTrial.Value)
                            days = (int)Math.Ceiling((agencyViewModelinfo.TrialEnd - DateTime.UtcNow).Value.TotalDays);

                        responseViewModelInfo.trialDays = days;
                        responseViewModelInfo.IsSuccess = true;
                    }
                    if (roleName != null && roleName.ToUpper() == AppConstants.Staff.ToUpper())
                    {
                        //var staffInfo = _staffInfoRepository.FindBy(m => m.UserId == user.ID ).FirstOrDefault();
                        var staffInfo = _staffInfoRepository.FindBy(m => m.UserId == user.ID && m.IsTimeClockUser == true).FirstOrDefault();
                        var staffViewModelinfo = Mapper.Map<StaffInfo, StaffViewModel>(staffInfo);
                        if (staffViewModelinfo != null)
                        {
                            if (staffInfo.AgencyRegistrationInfo.AgencyTimeZones.Count() > 0)
                                staffViewModelinfo.TimeZone = staffInfo.AgencyRegistrationInfo.AgencyTimeZones.FirstOrDefault().TimeZone;
                            staffViewModelinfo.RoleId = userLoginInfo.RoleId;
                            staffViewModelinfo.RoleName = AppConstants.Staff.ToUpper();
                            responseViewModelInfo.Content = staffViewModelinfo;
                            responseViewModelInfo.IsSuccess = true;
                        }
                        else
                        {
                            responseViewModelInfo.Content = null;
                            responseViewModelInfo.IsSuccess = false;
                            responseViewModelInfo.Message = AppConstants.InvalidEmailOrPassword;
                        }
                    }
                    if (roleName != null && roleName == "SuperAdmin")
                    {

                        responseViewModelInfo.Content = userLoginInfo;
                        responseViewModelInfo.IsSuccess = true;
                    }
                    if (roleName != null && roleName.ToUpper() == AppConstants.Family.ToUpper())
                    {
                        //var staffInfo = _staffInfoRepository.FindBy(m => m.UserId == user.ID ).FirstOrDefault();
                        var parentInfo = _tblParentInfoRepository.FindBy(m => m.EmailId == userName).FirstOrDefault();
                        var parentViewModelinfo = Mapper.Map<tblParentInfo, tblParentInfoViewModel>(parentInfo);
                        if (parentViewModelinfo != null)
                        {
                            //parentViewModelinfo.TimeZone = parentInfo.Agency.TimeZone;
                            //var agencyId = Convert.ToInt32(agencyIdfamily);
                            parentViewModelinfo.AgencyId = parentInfo.AgencyId;
                            parentViewModelinfo.UserId = userLoginInfo.ID;
                            parentViewModelinfo.RoleId = userLoginInfo.RoleId;
                            parentViewModelinfo.RoleName = AppConstants.Family.ToUpper();
                            responseViewModelInfo.Content = parentViewModelinfo;
                            responseViewModelInfo.IsSuccess = true;
                        }
                        else
                        {
                            responseViewModelInfo.Content = null;
                            responseViewModelInfo.IsSuccess = false;
                            responseViewModelInfo.Message = AppConstants.InvalidEmailOrPassword;
                        }
                    }
                    //if (userLoginInfo.RoleId == 3)
                    //{

                    //}
                    //responseViewModelInfo.Content = agencyViewModelinfo;
                    //responseViewModelInfo.IsSuccess = true;
                }
                else
                {
                    responseViewModelInfo.IsSuccess = false;
                    responseViewModelInfo.Message = AppConstants.UserDoesNotExist;
                }
            }
            catch (Exception ex)
            {
                responseViewModelInfo.IsSuccess = false;
                responseViewModelInfo.Message = AppConstants.SomethingWentWrong;
            }
            return responseViewModelInfo;
        }

        public ResponseViewModel GetUserRoleId(string roleName)
        {
            var roleInfo = new ResponseViewModel();
            var roleDetail = _rolesRepository.FindBy(m => m.RoleName.ToUpper() == roleName.ToUpper()).FirstOrDefault();
            var rolesViewModelInfo = Mapper.Map<Roles, RolesViewModel>(roleDetail);
            if (rolesViewModelInfo != null)
            {
                roleInfo.IsSuccess = true;
                roleInfo.Content = rolesViewModelInfo;
            }
            else
            {
                roleInfo.IsSuccess = false;
                roleInfo.Message = AppConstants.InvalidRoleName;
            }
            return roleInfo;
        }

        public ResponseViewModel GetUserRoleName(long roleId)
        {
            var roleInfo = new ResponseViewModel();
            var roleDetail = _rolesRepository.FindBy(m => m.ID == roleId).FirstOrDefault();
            var rolesViewModelInfo = Mapper.Map<Roles, RolesViewModel>(roleDetail);
            if (rolesViewModelInfo != null)
            {
                roleInfo.IsSuccess = true;
                roleInfo.Content = rolesViewModelInfo;
            }
            else
            {
                roleInfo.IsSuccess = false;
                roleInfo.Message = AppConstants.InvalidRoleName;
            }
            return roleInfo;
        }
        public ResponseViewModel ForgotPassword(UserViewModel model)
        {
            var responseViewModel = new ResponseViewModel();
            try
            {
                Users userObj = _userRepository.FindBy(f => f.EmailId == model.EmailId).FirstOrDefault();

                AgencyRegistrationInfo agencyObj = _agencyRegistrationInfoRepository.FindBy(f => f.Email == model.EmailId).FirstOrDefault();
                //long UserID = _commonService.GetUserIdFromUserName(username);
                if (userObj == null)
                {
                    responseViewModel.Content = null;
                    responseViewModel.IsSuccess = false;
                    responseViewModel.Message = "Please enter valid emailid";
                }
                else
                {
                    userObj.Password = model.Password;
                    _userRepository.Edit(userObj, userObj);
                    _unitOfWork.Commit();

                    agencyObj.Password = model.Password;
                    _agencyRegistrationInfoRepository.Edit(agencyObj, agencyObj);
                    _unitOfWork.Commit();

                    UserViewModel response = Mapper.Map<Users, UserViewModel>(userObj);
                    responseViewModel.Content = response;
                    responseViewModel.IsSuccess = true;
                    responseViewModel.Message = "Your new password has been sent on your email";
                }
            }
            catch (Exception)
            {
                responseViewModel.Content = null;
                responseViewModel.IsSuccess = false;
                responseViewModel.Message = "Please enter valid emailid";
            }
            return responseViewModel;
        }

        public ResponseViewModel ResetParentPassword(UserViewModel model)
        {

            var responseViewModel = new ResponseViewModel();
            try
            {
                Users usersObj = _userRepository.FindBy(x => x.RoleId == model.RoleId && x.EmailId == model.EmailId).FirstOrDefault();
                var primaryPassword = _userRepository.FindBy(x => x.RoleId == model.RoleId && x.EmailId == model.EmailId).Select(x => x.Password).FirstOrDefault().ToString();
                //Modified By Sanvir to use Smartcare Decryption Algos on Nov 03,2007
                var tripledes = new TripleDESCryptoServiceProvider();
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, tripledes.CreateDecryptor(Key, IV), CryptoStreamMode.Write);
                var cipherbytes = Convert.FromBase64String(primaryPassword);
                cs.Write(cipherbytes, 0, cipherbytes.Length);
                cs.FlushFinalBlock();

                //construct the string
                var decryptedArray = ms.ToArray();
                var characters = new char[43693];
                var dec = Encoding.UTF8.GetDecoder();

                var charlen = dec.GetChars(decryptedArray, 0, decryptedArray.Length, characters, 0);
                var decrpytedString = new string(characters, 0, charlen);
                if (decrpytedString == model.OldPassword)
                {
                    usersObj.Password = model.Password;
                    _userRepository.Edit(usersObj, usersObj);
                    _unitOfWork.Commit();
                    responseViewModel.IsSuccess = true;
                    responseViewModel.Message = "Password reset successfully, please login to continue.";
                }
                else
                {
                    responseViewModel.Content = null;
                    responseViewModel.IsSuccess = false;
                    responseViewModel.Message = "Old password is incorrect.";
                }

            }
            catch(Exception)
            {
                responseViewModel.Content = null;
                responseViewModel.IsSuccess = false;
                responseViewModel.Message = "Something went wrong. Please try again later.";
            }


            return responseViewModel;
        }
        public ResponseViewModel ResetPassword(UserViewModel userViewModel)
        {
            //var forgotPasswordLogData=_forgotPasswordLogRepository.FindBy(m => m.Guid == userViewModel.Guid);
            //var existingUser = _userRepository.GetSingleByUsername(userViewModel.Username);
            var responseViewModel = new ResponseViewModel();
            userViewModel.EmailId = GetDecryptedData(userViewModel.EmailId.Trim());
            var existingUser = _forgotPasswordLogRepository.FindBy(f => f.Guid == userViewModel.Guid).FirstOrDefault();
            if (existingUser == null)
            {
                responseViewModel.Content = userViewModel;
                responseViewModel.IsSuccess = false;
                responseViewModel.Message = "Link already used. Please generate another link to reset password.";
            }
            else
            {
                var forgotPasswordInfo = Mapper.Map<ForgotPasswordLog, ForgotPasswordLogViewModel>(existingUser);

                if (forgotPasswordInfo.User.RoleId == 1)
                {
                    //RoleId is 1 for Agency
                    AgencyRegistrationInfo agencyObj = _agencyRegistrationInfoRepository.FindBy(f => f.Email == forgotPasswordInfo.User.EmailId).FirstOrDefault();

                    //Update password in AgencyRegistration Table 
                    agencyObj.Password = userViewModel.Password;
                    _agencyRegistrationInfoRepository.Edit(agencyObj, agencyObj);
                }
                else if (forgotPasswordInfo.User.RoleId == 2)
                {
                    //RoleId is 2 for Staff
                }
                else
                {
                    //RoleId is 3 for SuperAdmin
                }


                //Update password in User Table 
                var user = new Users()
                {
                    ID = existingUser.User.ID,
                    EmailId = forgotPasswordInfo.User.EmailId,
                    Password = userViewModel.Password,
                    IsDeleted = false,
                    RoleId = existingUser.User.RoleId
                };
                _userRepository.Edit(existingUser.User, user);



                //Update IsDeleted in ForgotPasswordLog Table 
                _forgotPasswordLogRepository.SoftDelete(existingUser);
                _unitOfWork.Commit();

                responseViewModel.Content = userViewModel;
                responseViewModel.IsSuccess = true;
                responseViewModel.Message = "Password has been changed successfully.";

            }
            return responseViewModel;
        }

        /// <summary>
        ///     Adds the ForgotPasswordLog.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddForgotPasswordLog(ForgotPasswordLogViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                Users userObj = _userRepository.FindBy(f => f.EmailId == model.EmailId).FirstOrDefault();
                if (userObj != null)
                {
                    model.UserId = userObj.ID;
                    var forgotPasswordInfo = Mapper.Map<ForgotPasswordLogViewModel, ForgotPasswordLog>(model);
                    _forgotPasswordLogRepository.Add(forgotPasswordInfo);
                    _unitOfWork.Commit();
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("");
                }
                else
                {
                    transaction.IsExist = false;
                    transaction.ReturnStatus = false;
                }
                //transaction.ReturnMessage.Add(AppConstants.StaffAddedSuccesfully);
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
    }
}