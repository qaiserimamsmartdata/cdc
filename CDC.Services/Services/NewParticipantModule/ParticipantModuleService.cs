﻿using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Association;
using CDC.Entities.NewParent;
using CDC.Entities.NewParticipant;
using CDC.Entities.Schedule;
using CDC.Services.Abstract.Family;
using CDC.Services.Abstract.NewParticipantModule;
using CDC.ViewModel.Common;
using CDC.ViewModel.FamilyPortal;
using CDC.ViewModel.NewAssociation;
using CDC.ViewModel.NewParent;
using CDC.ViewModel.NewParticipant;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Security.Cryptography;
using System.IO;
using CDC.Entities.User;
using CDC.ViewModel.ParentPortal;

namespace CDC.Services.Services.NewParticipantModule
{
    public class ParticipantModuleService : IParticipantModuleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEntityBaseRepository<tblParticipantInfo> _participantInfoRepository;
        private readonly IEntityBaseRepository<tblParentInfo> _parentInfoRepository;
        private readonly IEntityBaseRepository<Users> _userRepository;
        private readonly IEntityBaseRepository<tblParentParticipantMapping> _parentParticipantRepository;
        private readonly IEntityBaseRepository<NewParticipantAssociationMaster> _participantAssociationRepository;
        private readonly IEntityBaseRepository<tblParticipantAssociatedParticipantMap> _participantParticipantRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IPasswordService _ipasswordservice;
        private readonly IEntityBaseRepository<Roles> _roleRepository;
        public ParticipantModuleService(IUnitOfWork unitOfWork,
            IEntityBaseRepository<NewParticipantAssociationMaster> participantAssociationRepository,
            IEntityBaseRepository<tblParticipantAssociatedParticipantMap> participantParticipantRepository,
            IEntityBaseRepository<tblParticipantInfo> participantInfoRepository,
            IEntityBaseRepository<Users> userRepository,
            IEntityBaseRepository<tblParentParticipantMapping> parentParticipantRepository,
            IEntityBaseRepository<tblParentInfo> parentInfoRepository,
            IEntityBaseRepository<Roles> roleRepository,
            IEntityBaseRepository<StudentSchedule> studentScheduleRepository, IPasswordService ipasswordservice)
        {
            _unitOfWork = unitOfWork;
            _participantInfoRepository = participantInfoRepository;
            _participantParticipantRepository = participantParticipantRepository;
            _parentInfoRepository = parentInfoRepository;
            _parentParticipantRepository = parentParticipantRepository;
            _ipasswordservice = ipasswordservice;
            _userRepository = userRepository;
            _participantAssociationRepository = participantAssociationRepository;
            _studentScheduleRepository = studentScheduleRepository;
            _roleRepository = roleRepository;
        }

        public ResponseViewModel getAssociationType(SearchFieldsViewModel associationTypeViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var obj = _participantAssociationRepository.GetAll().Where(g => g.AgencyId == associationTypeViewModel.AgencyId && g.IsDeleted == false).ToList();
                var responsedata = Mapper.Map<List<NewParticipantAssociationMaster>, List<tblParticipantAssociationMasterViewModel>>(obj);
                response.IsSuccess = true;
                response.Content = responsedata;

            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }
        public ResponseViewModel getAssociationParticipant(SearchFieldsViewModel associationParticipantTypeViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                tblParticipantInfoViewModel participantView = new tblParticipantInfoViewModel();
                var obj = _participantInfoRepository.GetAll().Where(g => g.AgencyId == associationParticipantTypeViewModel.AgencyId && g.IsDeleted == false).ToList();
                var responsedata = Mapper.Map<List<tblParticipantInfo>, List<tblParticipantInfoViewModel>>(obj);
                response.IsSuccess = true;
                response.Content = responsedata;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }
        /// <summary>
        ///     Gets all staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo"></param>
        public void GetAllParticipant(SearchParticipantViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<tblParticipantInfo>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate =
                        predicate.And(
                            p =>
                                p.FirstName.ToLower().Contains(model.ParticipantName.ToLower()) ||
                                p.LastName.ToLower().Contains(model.ParticipantName.ToLower()));
                //if(model.FamilyId!=null&& model.FamilyId>0)
                //{
                //    _parentParticipantRepository.FindBy(m=>m.NewParentInfoID==model.FamilyId).Distinct
                //    predicate = predicate.And(p => p. == model.FamilyId);
                //}
                var resultData = _participantInfoRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.FirstName);
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.FirstName);
                        break;
                    case "Gender":
                        resultData = resultData.OrderBy(m => m.Gender);
                        break;
                    case "-Gender":
                        resultData = resultData.OrderByDescending(m => m.Gender);
                        break;
                    case "DateOfBirth":
                        resultData = resultData.OrderBy(m => m.DateOfBirth);
                        break;
                    case "-DateOfBirth":
                        resultData = resultData.OrderByDescending(m => m.DateOfBirth);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.FirstName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var participantLst = Mapper.Map<List<tblParticipantInfo>, List<tblParticipantInfoViewModel>>(returnData);
                participantLst.ForEach(m =>
                {
                    var classList =
                       _studentScheduleRepository.FindBy(f => f.StudentId == m.ID && f.IsEnrolled)
                           .Select(s => s.ClassInfo)
                           .ToList();
                    var className = classList.Aggregate("", (current, classes) => current + classes.ClassName + ",");
                    if (classList.Count > 0)
                    {
                        m.MappedClass = className.Substring(0, className.Length - 1);
                    }
                });
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = participantLst;

            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        public ResponseViewModel getAssociationParents(List<tblParentParticipantMappingViewModel> associationParentTypeViewModel)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                List<tblParentInfo> parentList = new List<tblParentInfo>();
                foreach (var item in associationParentTypeViewModel)
                {
                    var obj = _parentParticipantRepository.GetAll().Where(g => g.IsDeleted == false && g.NewParticipantInfoID == item.NewParticipantInfoID).Select(m => m.NewParentInfo).ToList();
                    parentList.AddRange(obj);
                }

                var responsedata = Mapper.Map<List<tblParentInfo>, List<tblParentInfoViewModel>>(parentList);

                response.IsSuccess = true;
                response.Content = responsedata;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }
        public ResponseViewModel DeleteParticipant(tblParticipantInfoViewModel participantVm)
        {
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                var obj = _participantInfoRepository.GetAll().Where(g => g.ID == participantVm.ID && g.IsDeleted == false).FirstOrDefault();
                obj.IsDeleted = true;
                _unitOfWork.Commit();
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Please try again";
            }
            return response;
        }
        public ResponseViewModel RegisterParticipant(tblParticipantInfoViewModel model)
        {
            var response = new ResponseViewModel();
            ResponseViewModel responseViewModel = new ResponseViewModel();
            try
            {
                model.FullName = model.FirstName + " " + model.LastName;
                var paticipantResponse = AddParticipantInfo(model);
                model.ParticipantID = paticipantResponse.Id;
                if (model.Radio == "newParent")
                {
                    response.Content = AddParent(model).Content;
                }
                else if(model.Radio=="existing")
                {
                     response = AddParticipantMapping(model);
                    if (model.RadioParent == "existing")
                    {
                         response.Content = AddParentMapping(model).Content;
                    }
                    else if (model.RadioParent == "newParent")
                    {
                        response.Content = AddParentExisting(model);
                    }
                }
                response.IsSuccess = true;
            }
                
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Please Try Again";
            }
            return response;
        }
        public ResponseViewModel AddParticipantInfo(tblParticipantInfoViewModel ParticipantVm)
        {
            var respose = new ResponseViewModel();
            try
            {
                tblParticipantInfo oldModel = new tblParticipantInfo();
                var model = Mapper.Map<tblParticipantInfoViewModel, tblParticipantInfo>(ParticipantVm);
                if(model.ID>0)
                {
                    oldModel = _participantInfoRepository.GetAll().Where(x => x.ID == model.ID).FirstOrDefault();
                }
                model.CreatedBy = model.AgencyId.Value;
                model.CreatedDate = DateTime.UtcNow;
                model.IsDeleted = false;
                if (model.ID > 0 && oldModel != null)
                {
                    model.ID = oldModel.ID;
                    _participantInfoRepository.Edit(oldModel, model);
                }
                else
                {
                    _participantInfoRepository.Add(model);
                }
                _unitOfWork.Commit();
                respose.IsSuccess = true;
                respose.Message = "Participant Info Saved Successfully";
                respose.Id = model.ID;
            }
            catch (Exception ex)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }
        public ResponseViewModel AddParent(tblParticipantInfoViewModel model)
        {
            var response = new ResponseViewModel();
            var existingParent = _parentParticipantRepository.GetAll().Where(x => x.NewParticipantInfoID == model.ParticipantID).ToList();
            existingParent.ForEach(m => _parentParticipantRepository.Delete(m));
            //foreach (var item in participantIds)
            //{
            //    item.IsDeleted = true;
            //    _unitOfWork.Commit();
            //}
            var parentInfo = Mapper.Map<List<tblParentInfoViewModel>, List<tblParentInfo>>(model.ParentInfo);
            foreach (var item in parentInfo)
            {
                tblParentInfo oldModel = new tblParentInfo();
                if (item.ID > 0)
                {
                    oldModel = _parentInfoRepository.GetAll().Where(x => x.ID == item.ID).FirstOrDefault();
                }
                item.FullName = item.FirstName + " " + item.LastName;
                item.CreatedBy = 1;
                item.ImagePath = "assets/images/avatars/avatar-5.png";
                item.CreatedDate = DateTime.UtcNow;
                item.IsDeleted = false;
                if(item.ID>0 && oldModel != null)
                {
                    item.ID = oldModel.ID;
                    _parentInfoRepository.Edit(oldModel, item);
                }
                else
                {
                    _parentInfoRepository.Add(item);
                }
                
                _unitOfWork.Commit();
                response.Message = "Parent details saved successfully";
            }

            var parentVm = Mapper.Map<List<tblParentInfo>, List<tblParentInfoViewModel>>(parentInfo);

            /////Adding parent as a User in user table
            List<ParentPortalViewModel> portalVmLst = new List<ParentPortalViewModel>();
            foreach (var item in parentVm)
            {
                var parentPassword = _ipasswordservice.Generate();
                portalVmLst.Add(new ParentPortalViewModel()
                {
                    PortalEmail = item.EmailId,
                    PortalPassword = GetEncryptedData(parentPassword),
                    RoleId=4
                });

                SavePortalUser(portalVmLst);
            }
            
            tblParentParticipantMapping parentParticipantObj = new tblParentParticipantMapping();
            foreach (var item in parentVm)
            {
                var parentMapping = _parentParticipantRepository.GetAll().Where(x => x.NewParticipantInfoID == model.ParticipantID && x.NewParentInfoID==item.ID).FirstOrDefault();
                if (parentMapping != null)
                {
                    parentMapping.IsDeleted = false;
                    _unitOfWork.Commit();
                }
                else
                {
                    parentParticipantObj.IsDeleted = false;
                    parentParticipantObj.NewParticipantInfoID = model.ParticipantID;
                    parentParticipantObj.NewParentInfoID = item.ID;
                    _parentParticipantRepository.Add(parentParticipantObj);
                    _unitOfWork.Commit();
                }
            }
           
            response.IsSuccess = true;
            response.Content = portalVmLst;
            return response;
        }


        public ResponseViewModel SavePortalUser(List<ParentPortalViewModel> portalVmLst)
        {
            ResponseViewModel responseViewmodel = new ResponseViewModel();
            try
            {
                foreach (var item  in  portalVmLst)
                {
                    var userInfo = _userRepository.FindBy(m => m.EmailId == item.PortalEmail).ToList();
                    if (userInfo.Count <= 0)
                    {
                        //User Login information
                        var roleId = _roleRepository.FindBy(m => m.RoleName == "Family").FirstOrDefault().ID;
                        var userLoginInfo = new Users
                        {
                            RoleId = roleId,
                            EmailId = item.PortalEmail,
                            Password = item.PortalPassword,
                            IsDeleted = false
                        };
                        _userRepository.Add(userLoginInfo);
                        _unitOfWork.Commit();
                        responseViewmodel.IsSuccess = true;
                        responseViewmodel.ReturnMessage.Add("Family login is created");
                    }
                }
                
            }
            catch (Exception)
            {
                responseViewmodel.IsSuccess = true;
                responseViewmodel.ReturnMessage.Add("Unable to create family login");

            }
            return responseViewmodel;
        }
        public ResponseViewModel AddParentExisting(tblParticipantInfoViewModel model)
        {
            var response = new ResponseViewModel();
            var participantIds = _parentParticipantRepository.GetAll().Where(x => x.NewParticipantInfoID == model.ParticipantID).ToList();
            foreach (var item in participantIds)
            {
                item.IsDeleted = true;
                _unitOfWork.Commit();
            }
            var parentInfo = Mapper.Map<List<tblParentInfoViewModel>, List<tblParentInfo>>(model.ParentInfoExisting);
            foreach (var item in parentInfo)
            {
                tblParentInfo oldModel = new tblParentInfo();
                if (item.ID > 0)
                {
                    oldModel = _parentInfoRepository.GetAll().Where(x => x.ID == item.ID).FirstOrDefault();
                }
                item.FullName = item.FirstName + " " + item.LastName;
                item.CreatedBy = 1;
                item.CreatedDate = DateTime.UtcNow;
                item.IsDeleted = false;
                if (item.ID > 0 && oldModel != null)
                {
                    item.ID = oldModel.ID;
                    _parentInfoRepository.Edit(oldModel, item);
                }
                else
                {
                    _parentInfoRepository.Add(item);
                }

                _unitOfWork.Commit();
                response.Message = "Parent details saved successfully";
            }
            var parentVm = Mapper.Map<List<tblParentInfo>, List<tblParentInfoViewModel>>(parentInfo);
            tblParentParticipantMapping parentParticipantObj = new tblParentParticipantMapping();
            foreach (var item in parentVm)
            {
                var parentMapping = _parentParticipantRepository.GetAll().Where(x => x.NewParticipantInfoID == model.ParticipantID && x.NewParentInfoID == item.ID).FirstOrDefault();
                if (parentMapping != null)
                {
                    parentMapping.IsDeleted = false;
                    _unitOfWork.Commit();
                }
                else
                {
                    parentParticipantObj.IsDeleted = false;
                    parentParticipantObj.NewParticipantInfoID = model.ParticipantID;
                    parentParticipantObj.NewParentInfoID = item.ID;
                    _parentParticipantRepository.Add(parentParticipantObj);
                    _unitOfWork.Commit();
                }
            }
            response.IsSuccess = true;
            response.Content = parentVm;
            return response;
        }
        public ResponseViewModel AddParticipantMapping(tblParticipantInfoViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                var participantMapping = Mapper.Map<List<tblParticipantAssociatedParticipantViewModel>, List<tblParticipantAssociatedParticipantMap>>(model.ParticipantAssociatedParticipantMapping);
                tblParticipantAssociatedParticipantMap participantMappingObj = new tblParticipantAssociatedParticipantMap();

                var existingParticipants = _participantParticipantRepository.GetAll().Where(x => x.NewParticipantInfoId == model.ParticipantID).ToList();
                existingParticipants.ForEach(m => _participantParticipantRepository.Delete(m));

                foreach (var item in participantMapping)
                {
                    
                        participantMappingObj.IsDeleted = false;
                        participantMappingObj.AssociatedParticipantInfoId = item.AssociatedParticipantInfoId;
                        participantMappingObj.NewParticipantInfoId = model.ParticipantID;
                        participantMappingObj.AssociationTypeMasterID = item.AssociationTypeMasterID;
                       _participantParticipantRepository.Add(participantMappingObj);
                       _unitOfWork.Commit();
                }
                model.CreatedBy = model.AgencyId;
                model.CreatedDate = DateTime.UtcNow;
                model.IsDeleted = false;
                respose.IsSuccess = true;
                respose.Content = participantMappingObj;
            }
            catch (Exception ex)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }
        public ResponseViewModel AddParentMapping(tblParticipantInfoViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                var parentMapping = Mapper.Map<List<tblParentParticipantMappingViewModel>, List<tblParentParticipantMapping>>(model.ParentParticipantMapping);
                tblParentParticipantMapping participantMappingObj = new tblParentParticipantMapping();
                var existingParticipant = _parentParticipantRepository.GetAll().Where(x => x.NewParticipantInfoID==model.ParticipantID).ToList();
                existingParticipant.ForEach(m => _parentParticipantRepository.Delete(m));
                foreach (var item in parentMapping)
                {
                        participantMappingObj.IsDeleted = false;
                        participantMappingObj.NewParentInfoID = item.NewParentInfoID;
                        participantMappingObj.NewParticipantInfoID = model.ParticipantID;
                        participantMappingObj.RelationTypeId = item.RelationTypeId;
                        _parentParticipantRepository.Add(participantMappingObj);
                        _unitOfWork.Commit();
                }
                model.CreatedBy = model.AgencyId;
                model.CreatedDate = DateTime.UtcNow;
                model.IsDeleted = false;
                respose.IsSuccess = true;
                respose.Content = participantMappingObj;
            }
            catch (Exception ex)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }
        public ResponseViewModel ParticipantParentDetails(tblParticipantInfoViewModel participantVm)
        {
            var response = new ResponseViewModel();
            tblParticipantInfoViewModel participantDetails = new tblParticipantInfoViewModel();
            var ParticipantDetail = _participantInfoRepository.GetAll().Where(x => x.ID == participantVm.ParticipantID).FirstOrDefault();
            participantDetails = Mapper.Map<tblParticipantInfo, tblParticipantInfoViewModel>(ParticipantDetail);
            if (participantVm.Radio == "newParent")
            {
                participantDetails.ParentInfo = new List<tblParentInfoViewModel>();
                var parentParticipant = _parentParticipantRepository.GetAll().Where(x => x.NewParticipantInfoID == participantVm.ParticipantID).ToList();
                foreach (var item in parentParticipant)
                {
                    var parentParticipantMap = Mapper.Map<tblParentInfo, tblParentInfoViewModel>(item.NewParentInfo);
                    participantDetails.ParentInfo.Add(parentParticipantMap);
                }

            }
            else if (participantVm.Radio == "existing")
            {
                var paticipantParticipantDetail = _participantParticipantRepository.GetAll().Where(n => n.NewParticipantInfoId == participantVm.ParticipantID).ToList();
                participantDetails.ParticipantAssociatedParticipantMapping = new List<tblParticipantAssociatedParticipantViewModel>();
                participantDetails.ParticipantAssociatedParticipantMapping = Mapper.Map<List<tblParticipantAssociatedParticipantMap>, List<tblParticipantAssociatedParticipantViewModel>>(paticipantParticipantDetail);

                if (participantVm.RadioParent == "existing")
                {
                    var parentParticipant = _parentParticipantRepository.GetAll().Where(n => n.NewParticipantInfoID == participantVm.ParticipantID).ToList();
                    participantDetails.ParentParticipantMapping = new List<tblParentParticipantMappingViewModel>();
                    participantDetails.ParentParticipantMapping = Mapper.Map<List<tblParentParticipantMapping>, List<tblParentParticipantMappingViewModel>>(parentParticipant);
                }
                else if (participantVm.RadioParent == "newParent")
                {
                    var parentParticipant = _parentParticipantRepository.GetAll().Where(v => v.NewParticipantInfoID == participantVm.ParticipantID).ToList();
                    foreach (var item in parentParticipant)
                    {
                        var parentParticipantMap = Mapper.Map<tblParentInfo, tblParentInfoViewModel>(item.NewParentInfo);
                        participantDetails.ParentInfoExisting = new List<tblParentInfoViewModel>();
                        participantDetails.ParentInfoExisting.Add(parentParticipantMap);
                    }
                }
            }
            response.Content = participantDetails;
            response.IsSuccess = true;
            return response;


        }
        private static readonly byte[] Key =
         {
            0x12, 0xe3, 0x4a, 0xa1, 0x45, 0xd2, 0x56, 0x7c, 0x54, 0xac, 0x67, 0x9f,
            0x45, 0x6e, 0xaa, 0x56
        };

        // Initialization Vector to be used for encryption
        private static readonly byte[] IV = { 0x12, 0xe3, 0x4a, 0xa1, 0x45, 0xd2, 0x56, 0x7c };

        /// <summary>
        ///     Gets the encrypted data.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string GetEncryptedData(string input)
        {
            try
            {
                var tripledes = new TripleDESCryptoServiceProvider();
                var inputByteArray = Encoding.UTF8.GetBytes(input);
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, tripledes.CreateEncryptor(Key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();

                var str = Convert.ToBase64String(ms.ToArray());
                cs.Clear();
                return str;
            }
            catch (Exception)
            {
                return input;
            }
        }
    }

}
