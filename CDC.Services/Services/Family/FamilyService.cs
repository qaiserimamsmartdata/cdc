﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Common;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Family;
using LinqKit;
using System.Data.Entity;
using CDC.Entities.User;
using CDC.ViewModel.FamilyPortal;
using System.Transactions;
using CDC.Services.Abstract.Schedule;
using CDC.ViewModel.Schedule;
using CDC.Entities.NewParent;

namespace CDC.Services.Services.Family
{
    public class FamilyService : IFamilyService
    {
        private readonly IStudentScheduleService _istudentscheduleservice;
        private readonly ICommonService _commonService;
        private readonly IEntityBaseRepository<ContactInfoMap> _contactInfoMapRepository;
        private readonly IEntityBaseRepository<ContactInfo> _contactInfoRepository;
        private readonly IEntityBaseRepository<FamilyInfo> _familyInfoRepository;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familyStudentMapRepository;
        private readonly IEntityBaseRepository<ParentInfo> _parentInfoRepository;
        private readonly IEntityBaseRepository<tblParentInfo> _tblParentInfoRepository;
        private readonly IEntityBaseRepository<PaymentInfo> _paymentInfoRepository;
        private readonly IEntityBaseRepository<ReferenceDetail> _referenceDetailRepository;
        private readonly IEntityBaseRepository<StudentDetail> _studentDetailRepository;
        private readonly IEntityBaseRepository<StudentInfo> _studentInfoRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IEntityBaseRepository<Users> _userRepository;
        private readonly IEntityBaseRepository<Roles> _roleRepository;
        private readonly IUnitOfWork _unitOfWork;

        public FamilyService(IEntityBaseRepository<FamilyInfo> familyInfoRepository, IStudentScheduleService istudentscheduleservice,
            IEntityBaseRepository<ParentInfo> parentInfoRepository,
            IEntityBaseRepository<ReferenceDetail> referenceDetailRepository,
            IEntityBaseRepository<ContactInfo> contactInfoRepository,
            IEntityBaseRepository<FamilyStudentMap> familyStudentMapRepository,
            IEntityBaseRepository<ContactInfoMap> contactInfoMapRepository,
            IEntityBaseRepository<StudentInfo> studentInfoRepository,
            IEntityBaseRepository<StudentDetail> studentDetailRepository,
            IEntityBaseRepository<tblParentInfo> tblParentInfoRepository,
            IEntityBaseRepository<StudentSchedule> studentScheduleRepository,
            IEntityBaseRepository<PaymentInfo> paymentInfoRepository,
            IEntityBaseRepository<Users> userRepository,
            IEntityBaseRepository<Roles>  roleRepository,
            ICommonService commonService,
            IUnitOfWork unitOfWork)
        {
            _familyInfoRepository = familyInfoRepository;
            _tblParentInfoRepository = tblParentInfoRepository;
            _istudentscheduleservice = istudentscheduleservice;
            _parentInfoRepository = parentInfoRepository;
            _referenceDetailRepository = referenceDetailRepository;
            _contactInfoRepository = contactInfoRepository;
            _familyStudentMapRepository = familyStudentMapRepository;
            _contactInfoMapRepository = contactInfoMapRepository;
            _studentInfoRepository = studentInfoRepository;
            _studentDetailRepository = studentDetailRepository;
            _studentScheduleRepository = studentScheduleRepository;
            _paymentInfoRepository = paymentInfoRepository;
            _userRepository = userRepository;
            _commonService = commonService;
            _roleRepository = roleRepository;
            _unitOfWork = unitOfWork;
        }

        public ResponseViewModel RegisterFamily(FamilyRegisterViewModel model)
        {
            var respose = new ResponseViewModel();
            ResponseViewModel responseViewModel = new ResponseViewModel();
            ResponseInformation transaction;
            model.PaymentInfo.CardType = 1;
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    var familyInfoRespose = AddFamilyInfo(model.FamilyInfo);
                    if (familyInfoRespose.IsSuccess)
                    {
                        model.ReferenceDetail.FamilyId = familyInfoRespose.Id;
                        model.ContactInfo.FamilyId = familyInfoRespose.Id;
                        model.StudentDetail.ForEach(e => e.Student.FamilyId = familyInfoRespose.Id);
                        model.ParentInfo.ForEach(e => e.FamilyId = familyInfoRespose.Id);
                        model.PaymentInfo.FamilyId = familyInfoRespose.Id;
                        if(model.ReferenceDetail.InfoSourceId>0)
                        AddReferenceDetail(model.ReferenceDetail);
                        SaveParentInfo(model.ParentInfo);
                        SaveContactInfo(model.ContactInfo);
                        var result=SaveStudentDetail(model.StudentDetail);
                        var data = (List<StudentDetailViewModel>) result.Content;
                        model.StudentId = data.FirstOrDefault().ID;
                        if(model.ClassId > 0)
                        {
                            StudentScheduleViewModel scheduleModel = new StudentScheduleViewModel();
                            scheduleModel.ClassId = model.ClassId;
                            scheduleModel.AgencyId = model.AgencyId;
                            scheduleModel.StudentId = model.StudentId;
                            scheduleModel.ID = model.StudentId;
                            scheduleModel.StartDate = model.StartDate;
                            _istudentscheduleservice.AddStudentSchedule( scheduleModel, out transaction);
                            scheduleModel.ID = transaction.ID;
                            responseViewModel.Content = model;
                            responseViewModel.ReturnStatus = transaction.ReturnStatus;
                            responseViewModel.IsSuccess = transaction.ReturnStatus;
                            responseViewModel.ReturnMessage = transaction.ReturnMessage;
                            responseViewModel.ValidationErrors = transaction.ValidationErrors;
                            responseViewModel.IsExist = transaction.IsExist;
                            responseViewModel.IsEligible = transaction.IsEligible;

                        }
                       
                        SavePaymentInfo(model.PaymentInfo);
                        SavePortalUser(model.PortalInfo);
                        tran.Complete();
                        respose.IsSuccess = true;
                        respose.Message = "Family Details Saved Successfully";
                        respose.Content = model;
                    }
                    else
                    {
                        respose.IsSuccess = false;
                        respose.Message = "Please Try Again";
                    }
                }
            }
            catch (Exception)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }

        public ResponseViewModel SetIsLoggedFirstTime(long ParentID)
        {
            var responseViewModelInfo = new ResponseViewModel();
            try
            {
                var parentDetailOld =
                    _tblParentInfoRepository.FindBy(m => m.ID == ParentID && m.IsDeleted == false)
                        .FirstOrDefault();
                if (parentDetailOld != null)
                {
                    parentDetailOld.IsLoggedFirstTime = true;
                    _tblParentInfoRepository.Edit(parentDetailOld, parentDetailOld);
                    _unitOfWork.Commit();
                    responseViewModelInfo.IsSuccess = true;
                }
                else
                    responseViewModelInfo.IsSuccess = false;
            }
            catch (Exception)
            {
                responseViewModelInfo.IsSuccess = false;
            }
            return responseViewModelInfo;
        }
        public ResponseViewModel AddFamilyInfo(FamilyInfoViewModel familyVm)
        {
            var respose = new ResponseViewModel();
            try
            {
                if (familyVm.ID > 0)
                {
                    var oldObj = _familyInfoRepository.GetSingle(familyVm.ID);
                    var model = Mapper.Map<FamilyInfoViewModel, FamilyInfo>(familyVm);
                    model.ModifiedDate = DateTime.UtcNow;
                    _familyInfoRepository.Edit(oldObj, model);
                    _unitOfWork.Commit();
                    respose.IsSuccess = true;
                    respose.Message = "Family Info Updated Successfully";
                    respose.Id = model.ID;
                }
                else
                {
                    var model = Mapper.Map<FamilyInfoViewModel, FamilyInfo>(familyVm);
                    model.CreatedBy = model.AgencyId;
                    model.CreatedDate = DateTime.UtcNow;
                    model.IsDeleted = false;
                    _familyInfoRepository.Add(model);
                    _unitOfWork.Commit();
                    respose.IsSuccess = true;
                    respose.Message = "Family Info Saved Successfully";
                    respose.Id = model.ID;
                }
            }
            catch (Exception ex) 
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }

        public ResponseViewModel AddReferenceDetail(ReferenceDetailViewModel referenceVm)
        {
            var respose = new ResponseViewModel();
            try
            {
                var model = Mapper.Map<ReferenceDetailViewModel, ReferenceDetail>(referenceVm);
                model.CreatedDate = DateTime.UtcNow;
                model.IsDeleted = false;
                _referenceDetailRepository.Add(model);
                _unitOfWork.Commit();
                respose.IsSuccess = true;
                respose.Message = "Reference Detail Saved Successfully";
                respose.Id = model.ID;
            }
            catch (Exception)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }
        public ResponseViewModel SavePortalUser(FamilyPortalViewModel portalVm)
        {
            ResponseViewModel responseViewmodel = new ResponseViewModel();
            try
            {
                if(portalVm!=null)
                {
                    portalVm.RoleId = 3;
                }
                if (portalVm != null)
                {
                    var userInfo= _userRepository.FindBy(m => m.EmailId == portalVm.PortalEmail).ToList();
                    if(userInfo.Count<=0)
                    {
                        //User Login information
                        var roleId= _roleRepository.FindBy(m => m.RoleName == "Family").FirstOrDefault().ID;
                        var userLoginInfo = new Users
                        {
                            RoleId = roleId,
                            EmailId = portalVm.PortalEmail,
                            Password = portalVm.PortalPassword,
                            IsDeleted = false
                        };
                        _userRepository.Add(userLoginInfo);
                        _unitOfWork.Commit();
                        responseViewmodel.IsSuccess = true;
                        responseViewmodel.ReturnMessage.Add("Family login is created");
                    }

                }
            }
            catch (Exception)
            {
                responseViewmodel.IsSuccess = true;
                responseViewmodel.ReturnMessage.Add("Unable to create family login");

            }
            return responseViewmodel;
        }

        public ResponseViewModel SaveParentInfo(List<ParentInfoViewModel> parentVm)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<List<ParentInfoViewModel>, List<ParentInfo>>(parentVm);

            foreach (var item in model)
            {
                if (item.ID > 0)
                {
                    var oldObj = _parentInfoRepository.GetSingle(item.ID);
                    item.ModifiedDate = DateTime.UtcNow;
                    _parentInfoRepository.Edit(oldObj, item);
                    _unitOfWork.Commit();
                    respose.Message = "Parent details updated successfully";
                }
                else
                {
                    item.CreatedBy = 1;
                    item.CreatedDate = DateTime.UtcNow;
                    item.IsDeleted = false;
                    _parentInfoRepository.Add(item);
                    _unitOfWork.Commit();
                    respose.Message = "Parent details saved successfully";
                }
            }
            var responseData = Mapper.Map<List<ParentInfo>, List<ParentInfoViewModel>>(model);

            respose.IsSuccess = true;
            respose.Content = responseData;
            return respose;
        }

        public ResponseViewModel SaveContactInfo(ContactInfoViewModel contactInfoVm)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<ContactInfoViewModel, ContactInfo>(contactInfoVm);
            if (model.ID > 0)
            {
                var oldObj = _contactInfoRepository.GetSingle(model.ID);
                model.ModifiedDate = DateTime.UtcNow;
                _contactInfoRepository.Edit(oldObj, model);
                _unitOfWork.Commit();
                respose.Message = "Contact Details Updated Successfully";
            }
            else
            {
                model.CreatedBy = 1;
                model.CreatedDate = DateTime.UtcNow;
                model.IsDeleted = false;
                _contactInfoRepository.Add(model);
                _unitOfWork.Commit();
                respose.Message = "Contact Details Saved Successfully";
                var contactMapObj = new ContactInfoMap
                {
                    ContactId = model.ID,
                    FamilyId = contactInfoVm.FamilyId
                };
                MapContactInfo(contactMapObj);
            }
            var responseData = Mapper.Map<ContactInfo, ContactInfoViewModel>(model);

            respose.IsSuccess = true;

            respose.Content = responseData;
            return respose;
        }

        public ResponseViewModel MapContactInfo(ContactInfoMap model)
        {
            var respose = new ResponseViewModel();
            try
            {
                if (model.FamilyId > 0 && model.StaffId == null)
                {
                    var oldObj =
                        _contactInfoMapRepository.FindBy(
                            f => f.ContactId == model.ContactId && f.FamilyId == model.FamilyId).FirstOrDefault();
                    if (oldObj == null)
                    {
                        model.CreatedBy = 1;
                        model.CreatedDate = DateTime.UtcNow;
                        model.IsDeleted = false;
                        _contactInfoMapRepository.Add(model);
                        _unitOfWork.Commit();
                    }
                }
                else if (model.StaffId > 0 && model.FamilyId == null)
                {
                    var oldObj =
                        _contactInfoMapRepository.FindBy(
                            f => f.ContactId == model.ContactId && f.FamilyId == model.StaffId).FirstOrDefault();
                    if (oldObj == null)
                    {
                        model.CreatedBy = 1;
                        model.CreatedDate = DateTime.UtcNow;
                        model.IsDeleted = false;
                        _contactInfoMapRepository.Add(model);
                        _unitOfWork.Commit();
                    }
                }
                var responseData = Mapper.Map<ContactInfoMap, ContactInfoMapViewModel>(model);

                respose.IsSuccess = true;
                respose.Message = "Contact is Mapped Successfully";
                respose.Content = responseData;
            }
            catch (Exception)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }

        public ResponseViewModel SaveStudentInfo(StudentInfoViewModel studentInfoVm)
        {
            var respose = new ResponseViewModel();
            try
            {
                var model = Mapper.Map<StudentInfoViewModel, StudentInfo>(studentInfoVm);
                if (model.ID > 0)
                {
                    var oldObj = _studentInfoRepository.GetSingle(model.ID);
                    model.ModifiedDate = DateTime.UtcNow;
                    _studentInfoRepository.Edit(oldObj, model);
                    _unitOfWork.Commit();
                }
                else
                {
                    model.CreatedBy = 1;
                    model.CreatedDate = DateTime.UtcNow;
                    model.IsDeleted = false;
                    _studentInfoRepository.Add(model);
                    _unitOfWork.Commit();

                    var mapObj = new FamilyStudentMap
                    {
                        FamilyId = Convert.ToInt64(studentInfoVm.FamilyId),
                        StudentId = model.ID,
                        CreatedBy = 1,
                        CreatedDate = DateTime.UtcNow,
                        IsDeleted = false
                    };
                    _familyStudentMapRepository.Add(mapObj);
                    _unitOfWork.Commit();
                }
                var responseData = Mapper.Map<StudentInfo, ParentInfoViewModel>(model);

                respose.IsSuccess = true;
                respose.Message = "Participant Info Saved Successfully";
                respose.Content = responseData;
            }
            catch (Exception)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }

        public ResponseViewModel SaveStudentDetail(List<StudentDetailViewModel> studentDetailVm)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<List<StudentDetailViewModel>, List<StudentDetail>>(studentDetailVm);
            var studentDetailViewModel = studentDetailVm.FirstOrDefault();
            if (studentDetailViewModel != null)
            {
                var familyId = Convert.ToInt64(studentDetailViewModel.Student.FamilyId);
                foreach (var item in model)
                {
                    if (item.ID > 0)
                    {
                        item.ModifiedDate = DateTime.UtcNow;
                        item.Student.ModifiedDate = DateTime.UtcNow;
                        var studentDetailOldObj = _studentDetailRepository.GetSingle(item.ID);
                        _studentDetailRepository.Edit(studentDetailOldObj, item);
                        var studentInfoOldObj = _studentInfoRepository.GetSingle(item.Student.ID);
                        _studentInfoRepository.Edit(studentInfoOldObj, item.Student);
                        _unitOfWork.Commit();
                        respose.Message = "Participant Details Updated Successfully";
                    }
                    else
                    {
                        item.CreatedBy = 1;
                        item.CreatedDate = DateTime.UtcNow;
                        item.IsDeleted = false;
                        item.Student.CreatedBy = 1;
                        item.Student.CreatedDate = DateTime.UtcNow;
                        item.Student.IsDeleted = false;
                        _studentDetailRepository.Add(item);
                        _unitOfWork.Commit();

                        var mapObj = new FamilyStudentMap
                        {
                            FamilyId = familyId,
                            StudentId = item.Student.ID,
                            CreatedBy = 1,
                            CreatedDate = DateTime.UtcNow,
                            IsDeleted = false
                        };
                        _familyStudentMapRepository.Add(mapObj);
                        _unitOfWork.Commit();
                        respose.Message = "Participant Details Saved Successfully";
                    }
                }
            }
            var responseData = Mapper.Map<List<StudentDetail>, List<StudentDetailViewModel>>(model);
            respose.IsSuccess = true;
            respose.Content = responseData;
            return respose;
        }

        public ResponseViewModel SavePaymentInfo(PaymentInfoViewModel paymentInfoVm)
        {
            var respose = new ResponseViewModel();
            var model = Mapper.Map<PaymentInfoViewModel, PaymentInfo>(paymentInfoVm);
            if (model.ID > 0)
            {
                var oldObj = _paymentInfoRepository.GetSingle(model.ID);
                model.ModifiedDate = DateTime.UtcNow;
                _paymentInfoRepository.Edit(oldObj, model);
                _unitOfWork.Commit();
                respose.Message = "Payment Details Updated Successfully";
            }
            else
            {
                model.CreatedBy = 1;
                model.CreatedDate = DateTime.UtcNow;
                model.IsDeleted = false;
                _paymentInfoRepository.Add(model);
                _unitOfWork.Commit();
                respose.Message = "Payment Details Saved Successfully";
            }
            var responseData = Mapper.Map<PaymentInfo, PaymentInfoViewModel>(model);

            respose.IsSuccess = true;

            respose.Content = responseData;
            return respose;
        }

        public ResponseViewModel DeleteParentInfo(ParentInfoViewModel model)
        {
            var respose = new ResponseViewModel();
            if(model.IsPrimary==true)
            {
                respose.IsSuccess = true;
                respose.Message = "You cannot delete the primary parent.";
            }
            else
            {
                var objToDelete = Mapper.Map<ParentInfoViewModel, ParentInfo>(model);
                model.ModifiedDate = DateTime.UtcNow;
                _parentInfoRepository.SoftDelete(objToDelete);
                _unitOfWork.Commit();
                respose.IsSuccess = true;
                respose.Message = "Parent deleted successfully.";
                respose.Content = model;
            }
            return respose;
        }

        public ResponseViewModel GetFamilyInfo(SearchFamilyViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<FamilyInfo>();
                if (model.FamilyId > 0)
                    predicate = predicate.And(p => p.ID == model.FamilyId);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.FamilyName))
                    predicate = predicate.And(p => p.FamilyName.Contains(model.FamilyName));
                if (model.Date!=new DateTime())
                    predicate = predicate.And(p => DbFunctions.TruncateTime(p.CreatedDate) == DbFunctions.TruncateTime(model.Date));
                var resultData = _familyInfoRepository.GetAll().AsExpandable().Where(predicate).AsExpandable();

                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "FamilyName":
                        resultData = resultData.OrderBy(m => m.FamilyName);
                        break;
                    case "-FamilyName":
                        resultData = resultData.OrderByDescending(m => m.FamilyName);
                        break;
                    case "ParticipantCount":
                        resultData = resultData.OrderBy(m => m.FamilyStudentMap.Count);
                        break;
                    case "-ParticipantCount":
                        resultData = resultData.OrderByDescending(m => m.FamilyStudentMap.Count);
                        break;
                    case "DateAdded":
                        resultData = resultData.OrderBy(m => m.CreatedDate);
                        break;
                    case "-DateAdded":
                        resultData = resultData.OrderByDescending(m => m.CreatedDate);
                        break;
                    case "Email":
                        resultData = resultData.OrderBy(m => m.ParentInfo.FirstOrDefault().EmailId);
                        break;
                    case "-Email":
                        resultData = resultData.OrderByDescending(m => m.ParentInfo.FirstOrDefault().EmailId);
                        break;
                    case "Country":
                        resultData =
                            resultData.OrderBy(m => m.ContactInfoMap.FirstOrDefault().Contact.Country.CountryName);
                        break;
                    case "-Country":
                        resultData =
                            resultData.OrderByDescending(
                                m => m.ContactInfoMap.FirstOrDefault().Contact.Country.CountryName);
                        break;
                    case "City":
                        resultData = resultData.OrderBy(m => m.ContactInfoMap.FirstOrDefault().Contact.CityName);
                        break;
                    case "-City":
                        resultData =
                            resultData.OrderByDescending(m => m.ContactInfoMap.FirstOrDefault().Contact.CityName);
                        break;
                    case "Phone":
                        resultData = resultData.OrderBy(m => m.ParentInfo.FirstOrDefault().Mobile);
                        break;
                    case "-Phone":
                        resultData = resultData.OrderByDescending(m => m.ParentInfo.FirstOrDefault().Mobile);
                        break;
                }
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1)*model.limit).Take(model.limit).ToList();

                var familyList = Mapper.Map<List<FamilyInfo>, List<FamilyInfoViewModel>>(returnData);
                familyList.ForEach(
                    e => e.FamilyStudentMap = e.FamilyStudentMap.Where(w => w.IsDeleted == false).ToList());
                if(model.order== "DateAdded")
                {
                 
                }
                respose.TotalRows = totalCount;
                respose.IsSuccess = true;
                respose.Content = familyList;
            }
            catch (Exception ex)
            {
                respose.IsSuccess = false;
                respose.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                respose.ReturnStatus = false;
                respose.ReturnMessage.Add(errorMessage);
            }
            return respose;
        }

        public ResponseViewModel GetFamilyContactInfo(SearchFamilyViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<ContactInfoMap>();
                if (model.FamilyId > 0)
                    predicate = predicate.And(p => p.FamilyId == model.FamilyId);
                var resultData = _contactInfoMapRepository.FindBy(f => f.FamilyId == model.FamilyId);
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.CreatedDate);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1)*model.limit).Take(model.limit).ToList();
                var contactObj = returnData.Select(s => s.Contact).ToList();
                var familyList = Mapper.Map<List<ContactInfo>, List<ContactInfoViewModel>>(contactObj);
                respose.TotalRows = totalCount;
                respose.IsSuccess = true;
                respose.Content = familyList;
            }
            catch (Exception ex)
            {
                respose.IsSuccess = false;
                respose.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                respose.ReturnStatus = false;
                respose.ReturnMessage.Add(errorMessage);
            }
            return respose;
        }

        public ResponseViewModel GetFamilyStudentList(SearchFamilyViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<FamilyStudentMap>();
                if (model.FamilyId > 0)
                    predicate = predicate.And(p => p.FamilyId == model.FamilyId);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.Family.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.StudentName))
                {
                    predicate =
                       predicate.And(
                           p =>
                               p.Student.FirstName.Contains(model.StudentName) ||
                               p.Student.LastName.Contains(model.StudentName));
                }
                var studentInfo = _familyStudentMapRepository.GetAll().AsExpandable().Where(predicate).AsExpandable();
                var studentid = studentInfo.Select(s => s.Student.ID).ToArray();
                var resultData = _studentDetailRepository.FindBy(f => studentid.Contains(f.ID));
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.Student.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.Student.ID);
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.Student.FirstName);
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.Student.FirstName);
                        break;
                    case "LastName":
                        resultData = resultData.OrderBy(m => m.Student.LastName);
                        break;
                    case "-LastName":
                        resultData = resultData.OrderByDescending(m => m.Student.LastName);
                        break;
                    
                    case "Gender":
                        resultData = resultData.OrderBy(m => m.Student.Gender);
                        break;
                    case "-Gender":
                        resultData = resultData.OrderByDescending(m => m.Student.Gender);
                        break;

                    case "BirthDate":
                        resultData = resultData.OrderBy(m => m.Student.DateOfBirth);
                        break;
                    case "-BirthDate":
                        resultData = resultData.OrderByDescending(m => m.Student.DateOfBirth);
                        break;
                    case "Age":
                        resultData = resultData.OrderBy(m => m.Student.DateOfBirth);
                        break;
                    case "-Age":
                        resultData = resultData.OrderByDescending(m => m.Student.DateOfBirth);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.CreatedDate);
                var resultData1 = resultData.Skip((model.page - 1)*model.limit).Take(model.limit).ToList();
                var familyList = Mapper.Map<List<StudentDetail>, List<StudentDetailViewModel>>(resultData1);
                foreach (var item in familyList)
                {
                    var classList =
                        _studentScheduleRepository.FindBy(f => f.StudentId == item.studentId && f.IsEnrolled)
                            .Select(s => s.ClassInfo)
                            .ToList();
                    var className = classList.Aggregate("", (current, classes) => current + classes.ClassName + ",");
                    if (classList.Count > 0)
                    {
                        item.MappedClass = className.Substring(0, className.Length - 1);
                    }
                }
                familyList.ForEach(
                    e =>
                        e.Student.Age =
                            (e.Student.DateOfBirth == null
                                ? ""
                                : _commonService.CalculateAge(Convert.ToDateTime(e.Student.DateOfBirth))));
                respose.TotalRows = totalCount;
                respose.IsSuccess = true;
                respose.Content = familyList;
            }
            catch (Exception ex)
            {
                respose.IsSuccess = false;
                respose.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                respose.ReturnStatus = false;
                respose.ReturnMessage.Add(errorMessage);
            }
            return respose;
        }

        public ResponseViewModel GetFamilyStudentDetails(SearchFamilyViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<FamilyStudentMap>();
                if (model.FamilyId > 0)
                    predicate = predicate.And(p => p.FamilyId == model.FamilyId);
                if (model.StudentId > 0)
                    predicate = predicate.And(p => p.StudentId == model.StudentId);
                if (!string.IsNullOrEmpty(model.StudentName))
                    predicate =
                        predicate.And(
                            p =>
                                p.Student.FirstName.Contains(model.StudentName) ||
                                p.Student.LastName.Contains(model.StudentName));

                //if (model.StatusId > 0)
                //    predicate = predicate.And(p => p.StatusId == model.StatusId);
                //if (model.DesignationId > 0)
                //    predicate = predicate.And(p => p.DesignationId == model.DesignationId);

                var studentInfo = _familyStudentMapRepository.GetAll().AsExpandable().Where(predicate).AsExpandable();
                var studentid = studentInfo.Select(s => s.Student.ID).ToArray();
                var resultData = _studentDetailRepository.FindBy(f => studentid.Contains(f.ID));
                long totalCount = resultData.Count();
                //resultData=resultData.OrderBy(m => m.ID);
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.Student.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.Student.ID);
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.Student.FirstName);
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.Student.FirstName);
                        break;
                    case "LastName":
                        resultData = resultData.OrderBy(m => m.Student.LastName);
                        break;
                    case "-LastName":
                        resultData = resultData.OrderByDescending(m => m.Student.LastName);
                        break;
                    //case "Active":
                    //    resultData = resultData.OrderBy(m => m.Active);
                    //    break;
                    //case "-Active":
                    //    resultData = resultData.OrderByDescending(m => m.Active);
                    //    break;
                    case "Gender":
                        resultData = resultData.OrderBy(m => m.Student.Gender);
                        break;
                    case "-Gender":
                        resultData = resultData.OrderByDescending(m => m.Student.Gender);
                        break;

                    case "BirthDate":
                        resultData = resultData.OrderBy(m => m.Student.DateOfBirth);
                        break;
                    case "-BirthDate":
                        resultData = resultData.OrderByDescending(m => m.Student.DateOfBirth);
                        break;
                    case "Age":
                        resultData = resultData.OrderBy(m => m.Student.DateOfBirth);
                        break;
                    case "-Age":
                        resultData = resultData.OrderByDescending(m => m.Student.DateOfBirth);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.CreatedDate);
                var resultData1 = resultData.Skip((model.page - 1)*model.limit).Take(model.limit).ToList();
                var studentList = Mapper.Map<List<StudentDetail>, List<StudentDetailViewModel>>(resultData1);
                studentList.ForEach(
                    e =>
                        e.Student.Age =
                            (e.Student.DateOfBirth == null
                                ? ""
                                : _commonService.CalculateAge(Convert.ToDateTime(e.Student.DateOfBirth))));
                respose.TotalRows = totalCount;
                respose.IsSuccess = true;
                respose.Content = studentList;
            }
            catch (Exception ex)
            {
                respose.IsSuccess = false;
                respose.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                respose.ReturnStatus = false;
                respose.ReturnMessage.Add(errorMessage);
            }
            return respose;
        }

        public ResponseViewModel GetFamilyParentList(SearchFamilyViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<ParentInfo>();
                if (!string.IsNullOrEmpty(model.ParentName))
                    predicate =
                        predicate.And(
                            p => p.FirstName.Contains(model.ParentName) || p.LastName.Contains(model.ParentName));

                var resultData = _parentInfoRepository.FindBy(f => f.FamilyId == model.FamilyId);
                long totalCount = resultData.Count();
                //resultData=resultData.OrderBy(m => m.ID);
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.CreatedDate);
                var resultData1 =
                    resultData.AsExpandable()
                        .Where(predicate)
                        .Skip((model.page - 1)*model.limit)
                        .Take(model.limit)
                        .ToList();
                var parentList = Mapper.Map<List<ParentInfo>, List<ParentInfoViewModel>>(resultData1);
                respose.TotalRows = totalCount;
                respose.IsSuccess = true;
                respose.Content = parentList;
            }
            catch (Exception ex)
            {
                respose.IsSuccess = false;
                respose.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                respose.ReturnStatus = false;
                respose.ReturnMessage.Add(errorMessage);
            }
            return respose;
        }

        public ResponseViewModel AddParentAndContactInfo(FamilyRegisterViewModel model)
        {
            var contactInfo = SaveContactInfo(model.ContactInfo);
            return contactInfo;
        }

        public ResponseViewModel DeleteStudentInfo(long studentInfoId)
        {
            var respose = new ResponseViewModel();
            try
            {
                var studentInfoObj = _studentInfoRepository.GetSingle(studentInfoId);
                _studentInfoRepository.SoftDelete(studentInfoObj);
                var studentdetailObj =
                    _studentDetailRepository.FindBy(f => f.studentId == studentInfoId).SingleOrDefault();
                _studentDetailRepository.SoftDelete(studentdetailObj);
                var studentFamilyMapObj =
                    _familyStudentMapRepository.FindBy(f => f.StudentId == studentInfoId).SingleOrDefault();
                _familyStudentMapRepository.SoftDelete(studentFamilyMapObj);
                _unitOfWork.Commit();

                respose.IsSuccess = true;
                respose.Message = "Participant Detail Deleted Successfully";
            }
            catch (Exception)
            {
                respose.IsSuccess = false;
                respose.Message = "Please Try Again";
            }
            return respose;
        }
    }
}