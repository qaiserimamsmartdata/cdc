﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Schedule;
using CDC.Services.Abstract.Schedule;
using CDC.ViewModel.Common;
using CDC.ViewModel.Schedule;
using LinqKit;
using CDC.Entities.Class;
using CDC.Entities.Family;
using System.Data.Entity;
using CDC.Entities.NewParticipant;
using System.Data.Entity.Core.Objects;

namespace CDC.Services.Services.Schedule
{
    public class StudentScheduleService : IStudentScheduleService
    {
        private readonly IEntityBaseRepository<StudentSchedule> _studentscheduleRepository;
        private readonly IEntityBaseRepository<ClassInfo> _classInfoRepository;
        private readonly IEntityBaseRepository<StudentInfo> _studentInfoRepository;
        private readonly IEntityBaseRepository<tblParticipantInfo> _participantInfoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StudentScheduleService(IEntityBaseRepository<StudentSchedule> studentscheduleRepository,
            IEntityBaseRepository<ClassInfo> classInfoRepository,
            IEntityBaseRepository<StudentInfo> studentInfoRepository,
            IEntityBaseRepository<tblParticipantInfo> participantInfoRepository,
        IUnitOfWork unitOfWork)
        {
            _studentscheduleRepository = studentscheduleRepository;
            _classInfoRepository = classInfoRepository;
            _studentInfoRepository = studentInfoRepository;
            _participantInfoRepository = participantInfoRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Adds the student schedule.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddStudentSchedule(StudentScheduleViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            model.EndDate = DateTime.UtcNow;          
            try
            {
                var isExist =
                    _studentscheduleRepository.FindBy(f => f.ClassId == model.ClassId && f.StudentId == model.StudentId && f.IsEnrolled)
                        .ToList();
                if (isExist.Count > 0)
                {
                    transaction.ReturnStatus = true;
                    transaction.IsExist = true;
                    transaction.ReturnMessage.Add("Participant is already enrolled in this class");
                }
                else
                {
                    ClassInfo classModel = _classInfoRepository.FindBy(x => x.ID == model.ClassId).FirstOrDefault();
                    if (classModel != null && (classModel.OnGoing==null || classModel.OnGoing==false))
                    {
                        if(classModel.ClassEndDate.Value.Date< EntityFunctions.TruncateTime(DateTime.UtcNow))
                        {
                            transaction.ReturnStatus = true;
                            transaction.IsEligible = false;
                            transaction.ReturnMessage.Add("Participant cannot be enrolled in this class.");
                        }
                        else
                        {
                            transaction = CreateStudentSchedule(model,classModel);
                        }
                    }
                    else
                    {
                        transaction = CreateStudentSchedule(model, classModel);
                    }
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        private ResponseInformation CreateStudentSchedule(StudentScheduleViewModel model, ClassInfo classModel)
        {
            ResponseInformation transaction = new ResponseInformation();
            var studentschedule = Mapper.Map<StudentScheduleViewModel, StudentSchedule>(model);

            //StudentInfo studentModel = _studentInfoRepository.FindBy(x => x.ID == model.StudentId).FirstOrDefault();
            tblParticipantInfo  studentModel = _participantInfoRepository.FindBy(x => x.ID == model.StudentId).FirstOrDefault();
            int age = 0;
            if (studentModel!=null)
            age = DateTime.UtcNow.Year - studentModel.DateOfBirth.Value.Year;
            if ((classModel.MinAgeFrom <= age && age <= classModel.MaxAgeFrom)|| (classModel.MinAgeFrom <= age && classModel.MaxAgeFrom==0))
            {
                studentschedule.RoomId = classModel.RoomId ?? 0;
                studentschedule.IsEnrolled = true;
                _studentscheduleRepository.Add(studentschedule);
                _unitOfWork.Commit();
                transaction.ID = studentschedule.ID;
                transaction.ReturnStatus = true;
                transaction.IsEligible = true;
                transaction.ReturnMessage.Add("Participant enrolled in this class successfully");
            }
            else
            {
                transaction.ReturnStatus = true;
                transaction.IsEligible = false;
                transaction.ReturnMessage.Add("Participant is not eligible for the selected class.");
            }
            return transaction;
        }

        /// <summary>
        ///     Gets all student schedule.
        /// </summary>
        /// 
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public List<StudentScheduleViewModel> GetAllStudentSchedule(StudentScheduleViewModelNew model,
            out ResponseInformation transaction) //SearchStudentScheduleViewModel model, 
        {
            transaction = new ResponseInformation();
            PredicateBuilder.True<StudentSchedule>();
            try
            {
                var resultData = _studentscheduleRepository.GetAll().Where(m => m.StudentId == model.studentId);
                resultData = resultData.OrderBy(m => m.ID);
                var classLst = Mapper.Map<List<StudentSchedule>, List<StudentScheduleViewModel>>(resultData.ToList());
                transaction.ReturnStatus = true;
                return classLst;
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
                return null;
            }
        }

        public void UpdateStudentSchedule(StudentScheduleViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var studentscheduleDetail =
                    _studentscheduleRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapstudentscheduleDetail = Mapper.Map<StudentScheduleViewModel, StudentSchedule>(model);
                _studentscheduleRepository.Edit(studentscheduleDetail, mapstudentscheduleDetail);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Student Schedule successfully updated at " +
                                              DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }

        /// <summary>
        ///     Deletes the student schedule by identifier.
        /// </summary>
        /// <param name="scheduleId">The schedule identifier.</param>
        /// <param name="transaction">The transaction.</param>
        public void DeleteStudentScheduleById(long scheduleId, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var studentSheduleDetails =
                    _studentscheduleRepository.FindBy(m => m.ID == scheduleId && m.IsDeleted == false).FirstOrDefault();
                if (studentSheduleDetails != null)
                {
                    studentSheduleDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    transaction.ReturnStatus = true;
                    transaction.ReturnMessage.Add("Student Schedule successfully updated at " +
                                                  DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                }
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }
        public List<StudentScheduleViewModel> GetUnPaidHistory(SearchStudentScheduleViewModel model,out ResponseInformation transaction) //SearchStudentScheduleViewModel model, 
        {
            transaction = new ResponseInformation();
            var predicate = PredicateBuilder.True<StudentSchedule>();
            try
            {
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.ClassInfo.AgencyId == model.AgencyId);
                if (model.StudentId > 0)
                    predicate = predicate.And(p => p.StudentId == model.StudentId);
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate = predicate.And(p => p.StudentInfo.FirstName.Contains(model.ParticipantName)|| p.StudentInfo.LastName.Contains(model.ParticipantName));
                if (model.FromDate != null)
                    predicate=predicate.And(p => DbFunctions.TruncateTime(p.PaymentStatusList.FirstOrDefault().scheduleDate.Value) >= DbFunctions.TruncateTime(model.FromDate.Value));
                 if (model.ToDate != null)
                predicate = predicate.And(p => DbFunctions.TruncateTime(p.PaymentStatusList.FirstOrDefault().scheduleDate.Value) <= DbFunctions.TruncateTime(model.ToDate.Value));
                
                if (model.PaymentStatus != null)
                    predicate = predicate.And(p => p.PaymentStatusList.FirstOrDefault().PaymentStatus == model.PaymentStatus);
                if (model.ClassId>0)
                    predicate = predicate.And(p => p.ClassInfo.ID== model.ClassId);
                var resultData = _studentscheduleRepository.GetAll().AsExpandable().Where(predicate);
                long resultCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "ParticipantName":
                        resultData = resultData.OrderBy(m => m.StudentInfo.FirstName);
                        break;
                    case "-ParticipantName":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.FirstName);
                        break;
                    case "ClassName":
                        resultData = resultData.OrderBy(m => m.ClassInfo.ClassName);
                        break;
                    case "-ClassName":
                        resultData = resultData.OrderByDescending(m => m.ClassInfo.ClassName);
                        break;
                    case "Fees":
                        resultData = resultData.OrderBy(m => m.ClassInfo.Fees);
                        break;
                    case "-Fees":
                        resultData = resultData.OrderByDescending(m => m.ClassInfo.Fees);
                        break;
                    case "ScheduleDate":
                        resultData = resultData.OrderBy(m => m.PaymentStatusList.FirstOrDefault().scheduleDate);
                        break;
                    case "-ScheduleDate":
                        resultData = resultData.OrderByDescending(m => m.PaymentStatusList.FirstOrDefault().scheduleDate);
                        break;
                    case "ModifiedDate":
                        resultData = resultData.OrderBy(m => m.PaymentStatusList.FirstOrDefault().ModifiedDate);
                        break;
                    case "-ModifiedDate":
                        resultData = resultData.OrderByDescending(m => m.PaymentStatusList.FirstOrDefault().ModifiedDate);
                        break;
                    case "Amount":
                        resultData = resultData.OrderBy(m => m.PaymentStatusList.FirstOrDefault().Fees);
                        break;
                    case "-Amount":
                        resultData = resultData.OrderByDescending(m => m.PaymentStatusList.FirstOrDefault().Fees);
                        break;
                    case "PaymentMode":
                        resultData = resultData.OrderBy(m => m.PaymentStatusList.FirstOrDefault().PaymentMode);
                        break;
                    case "-PaymentMode":
                        resultData = resultData.OrderByDescending(m => m.PaymentStatusList.FirstOrDefault().PaymentMode);
                        break;
                }
                var returnData1 = resultData.AsExpandable().Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var unPaidList = Mapper.Map<List<StudentSchedule>, List<StudentScheduleViewModel>>(returnData1.ToList());
                transaction.ReturnStatus = true;
                transaction.TotalRows = resultCount;
                return unPaidList;
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
                return null;
            }
        }
    }
}