﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Students;
using CDC.Services.Abstract.Student;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;
using CDC.ViewModel.Student;
using LinqKit;
using CDC.Entities.Family;

namespace CDC.Services.Services.Student
{
    public class StudentService : IStudentService
    {
        private readonly IEntityBaseRepository<Entities.Students.Student> _studentRepository;
        private readonly IEntityBaseRepository<ParentInfo> _parentInfoRepository;
        private readonly IEntityBaseRepository<ContactInfo> _contactInfoRepository;
        private readonly IEntityBaseRepository<GuardianInfo> _guardianInfoRepository;
        private readonly IEntityBaseRepository<EnrollmentInfo> _enrollmentInfoRepository;
        private readonly IEntityBaseRepository<Entities.Skills.Skills> _skillRepository;
        //private readonly IEntityBaseRepository<Customer> _userRepository;
        //private readonly IEntityBaseRepository<Country> _countryRepository;
        //private readonly IEntityBaseRepository<State> _stateRepository;
        //private readonly IEntityBaseRepository<City> _cityRepository; 
        private readonly IUnitOfWork _unitOfWork;
        //public CommonService(
        //    IEntityBaseRepository<Customer> userRepository,
        //    IEntityBaseRepository<Country> countryRepository,
        //    IEntityBaseRepository<State> stateRepository,
        //    IEntityBaseRepository<City> cityRepository, 
        //    IUnitOfWork unitOfWork
        //    )
        //{
        //    _userRepository = userRepository;
        //    _countryRepository = countryRepository;
        //    _stateRepository = stateRepository;
        //    _cityRepository = cityRepository; 
        //    _unitOfWork = unitOfWork;
        //}
        public StudentService(
          IEntityBaseRepository<Entities.Students.Student> studentRepository, IEntityBaseRepository<ParentInfo> parentInfoRepository, IEntityBaseRepository<ContactInfo> contactInfoRepository, IEntityBaseRepository<GuardianInfo> guardianInfoRepository, IEntityBaseRepository<EnrollmentInfo> enrollmentInfoRepository,
           IEntityBaseRepository<Entities.Skills.Skills> skillRepository, IUnitOfWork unitOfWork
          )
        {
            _studentRepository = studentRepository;
            _parentInfoRepository = parentInfoRepository;
            _contactInfoRepository = contactInfoRepository;
            _guardianInfoRepository = guardianInfoRepository;
            _enrollmentInfoRepository = enrollmentInfoRepository;
            _skillRepository = skillRepository;
            _unitOfWork = unitOfWork;
        }

        public int AddStudent(AddStudentViewModel addStudentViewModel)
        {

            try
            {
                if (addStudentViewModel.Startdate.ToString("dd/MM/yyyy") == "01/01/0001")
                {
                    addStudentViewModel.Startdate = DateTime.Now;
                }
                if (addStudentViewModel.Enddate.ToString("dd/MM/yyyy") == "01/01/0001")
                {
                    addStudentViewModel.Enddate = DateTime.Now;
                }
                if (addStudentViewModel.Postalcode == null)
                {
                    addStudentViewModel.Postalcode = "0";
                }

                var student = new Entities.Students.Student()
                {
                    FirstName = addStudentViewModel.Firstname,
                    LastName = addStudentViewModel.Lastname,
                    Gender = addStudentViewModel.Gender,
                    DateOfBirth = addStudentViewModel.Dateofbirth,
                    ParentInfos = new List<ParentInfo>()
                    {
                        new ParentInfo
                        {
                            //FatherName=addStudentViewModel.Fathername,
                            //MotherName = addStudentViewModel.Mothername,
                            FirstName=addStudentViewModel.Fathername,
                            LastName = addStudentViewModel.Mothername,
                            Mobile = addStudentViewModel.ParentsMobile,
                            EmailId = addStudentViewModel.Email,
                            IsDeleted = false,
                        }
                    },
                    GuardianInfos = new List<GuardianInfo>()
                    {
                        new GuardianInfo
                        {
                            FirstName=addStudentViewModel.Guardianfirstname,
                            LastName = addStudentViewModel.Guardianlastname,
                            Mobile =addStudentViewModel.Guardianmobile,
                            EmailId= addStudentViewModel.Guardianemail,
                            RelationId = addStudentViewModel.Relation,
                            IsDeleted = false
                        }
                    },
                    ContactInfos = new List<ContactInfo>()
                    {
                        new ContactInfo
                        {
                            Address=addStudentViewModel.Address,
                            CountryId =addStudentViewModel.Country,
                            StateId = addStudentViewModel.State,
                            CityName = addStudentViewModel.City,
                            PostalCode = addStudentViewModel.Postalcode,
                            IsDeleted = false
                        }
                    },
                    EnrollmentInfos = new List<EnrollmentInfo>()
                    {
                        new EnrollmentInfo
                        {
                            ClassId = addStudentViewModel.ClassID,
                            RoomId = addStudentViewModel.Room,
                            StartDate = addStudentViewModel.Startdate,
                            EndDate = addStudentViewModel.Enddate,
                            AdditionalInfo = addStudentViewModel.AdditionalInformation,
                            IsDeleted = false
                        }
                    },
                    IsDeleted = false
                };
                _studentRepository.Add(student);

                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                
                return 0;
            }
        }

        public dynamic GetStudentById(long studentId)
        {

            var studentData = _studentRepository.FindBy(m => m.ID == studentId).FirstOrDefault();

            var returnData = new
            {
                ID = studentData.ID,
                Firstname = studentData.FirstName,
                Lastname = studentData.LastName,
                Gender = studentData.Gender,
                Dateofbirth = studentData.DateOfBirth.Value.ToShortDateString(),
                Fathername = studentData.ParentInfos.FirstOrDefault().FirstName,
                Mothername = studentData.ParentInfos.FirstOrDefault().LastName,
                ParentsMobile = studentData.ParentInfos.FirstOrDefault().Mobile,
                Email = studentData.ParentInfos.FirstOrDefault().EmailId,
                Guardianfirstname = studentData.GuardianInfos.FirstOrDefault().FirstName,
                Guardianlastname = studentData.GuardianInfos.FirstOrDefault().LastName,
                Guardianemail = studentData.GuardianInfos.FirstOrDefault().EmailId,
                Guardianmobile = studentData.GuardianInfos.FirstOrDefault().Mobile,
                Relation = studentData.GuardianInfos.FirstOrDefault().RelationId,
                Address = studentData.ContactInfos.FirstOrDefault().Address,
                City = studentData.ContactInfos.FirstOrDefault().CityName,
                State = studentData.ContactInfos.FirstOrDefault().StateId,
                Country = studentData.ContactInfos.FirstOrDefault().CountryId,
                Postalcode = studentData.ContactInfos.FirstOrDefault().PostalCode,
                ClassID = studentData.EnrollmentInfos.FirstOrDefault().ClassId,
                Room = studentData.EnrollmentInfos.FirstOrDefault().RoomId,
                Startdate = studentData.EnrollmentInfos.FirstOrDefault().StartDate.Value.ToShortDateString(),
                Enddate = studentData.EnrollmentInfos.FirstOrDefault().EndDate.Value.ToShortDateString(),
                AdditionalInformation = studentData.EnrollmentInfos.FirstOrDefault().AdditionalInfo
            };

            return returnData;
        }

        public List<StudentViewModel> GetAllStudents(SearchStudentViewModel model)
        {
            var predicate = PredicateBuilder.False<Entities.Students.Student>();
            predicate = predicate.Or(p => p.FirstName.Contains(model.name));
            predicate = predicate.Or(p => p.LastName.Contains(model.name));
            if (model.classid > 0)
            {
                predicate = predicate.Or(p => p.EnrollmentInfos.FirstOrDefault().ClassId == model.classid);
            }
            var resultData = _studentRepository.GetAll().AsExpandable().Where(predicate).OrderBy(m => m.ID);
            switch (model.order)
            {
                case "id":
                    resultData = resultData.OrderBy(m => m.ID);
                    break;
                case "-id":
                    resultData = resultData.OrderByDescending(m => m.ID);
                    break;
                case "FirstName":
                    resultData = resultData.OrderBy(m => m.FirstName);
                    break;
                case "-FirstName":
                    resultData = resultData.OrderByDescending(m => m.FirstName);
                    break;
                case "ClassId":
                    resultData = resultData.OrderBy(m => m.EnrollmentInfos.OrderBy(k => k.ClassId));
                    break;
                case "-ClassId":
                    resultData = resultData.OrderByDescending(m => m.EnrollmentInfos.OrderBy(k => k.ClassId));
                    break;
                case "Room":
                    resultData = resultData.OrderBy(m => m.EnrollmentInfos.OrderBy(k => k.RoomId));
                    break;
                case "-Room":
                    resultData = resultData.OrderByDescending(m => m.EnrollmentInfos.FirstOrDefault().RoomId);
                    break;
                case "Gender":
                    resultData = resultData.OrderBy(m => m.Gender);
                    break;
                case "-Gender":
                    resultData = resultData.OrderByDescending(m => m.Gender);
                    break;
                case "StartDate":
                    resultData = resultData.OrderBy(m => m.EnrollmentInfos.FirstOrDefault().StartDate);
                    break;
                case "-StartDate":
                    resultData = resultData.OrderByDescending(m => m.EnrollmentInfos.FirstOrDefault().StartDate);
                    break;
                case "EndDate":
                    resultData = resultData.OrderBy(m => m.EnrollmentInfos.FirstOrDefault().EndDate);
                    break;
                case "-EndDate":
                    resultData = resultData.OrderByDescending(m => m.EnrollmentInfos.FirstOrDefault().EndDate);
                    break;
            }

            var returnData = resultData.Select(m =>
               new StudentViewModel
               {
                   ID = m.ID,
                   Firstname = m.FirstName,
                   Lastname = m.LastName,
                   Gender = m.Gender,
                   ClassID = m.EnrollmentInfos.FirstOrDefault().ClassId,
                   Room = m.EnrollmentInfos.FirstOrDefault().RoomId,
                   Dateofbirth = m.DateOfBirth,
                   Startdate = m.EnrollmentInfos.FirstOrDefault().StartDate.Value,
                   Enddate = m.EnrollmentInfos.FirstOrDefault().EndDate.Value,
               }).Skip((model.page - 1) * model.limit).Take(model.limit).ToList();

            return returnData;
        }
        
        

        public int UpdateStudentDetail(AddStudentViewModel model)
        {
            if (model.Guardianmobile == null) model.Guardianmobile = "";
            int resultData = 0;
            try
            {
                var studentDetail = _studentRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                if (studentDetail != null)
                {
                    studentDetail.FirstName = model.Firstname;
                    studentDetail.LastName = model.Lastname;
                    studentDetail.Gender = model.Gender;
                    studentDetail.DateOfBirth = model.Dateofbirth;

                    //var parentInfo = studentDetail.ParentInfos.FirstOrDefault(m => m.StudentId == studentDetail.ID);
                    //parentInfo.FatherName = model.Fathername;
                    //parentInfo.MotherName = model.Mothername;
                    //parentInfo.Mobile = model.ParentsMobile;
                    //parentInfo.EmailId = model.Email;

                    //var contactInfo = studentDetail.ContactInfos.FirstOrDefault(m => m.StudentId == studentDetail.ID);
                    //contactInfo.CountryId = model.Country;
                    //contactInfo.StateId = model.State;
                    //contactInfo.City = model.City;
                    //contactInfo.Address = model.Address;
                    //contactInfo.PostalCode = model.Postalcode;

                    var guardianInfo = studentDetail.GuardianInfos.FirstOrDefault(m => m.StudentId == studentDetail.ID);
                    guardianInfo.FirstName = model.Guardianfirstname;
                    guardianInfo.LastName = model.Guardianlastname;
                    guardianInfo.Mobile = model.Guardianmobile;
                    guardianInfo.EmailId = model.Guardianemail;
                    guardianInfo.RelationId = model.Relation;

                    var enrollmentInfo = studentDetail.EnrollmentInfos.FirstOrDefault(m => m.StudentId == studentDetail.ID);
                    enrollmentInfo.ClassId = model.ClassID;
                    enrollmentInfo.AdditionalInfo = model.AdditionalInformation;
                    enrollmentInfo.StartDate = model.Startdate;
                    enrollmentInfo.EndDate = model.Enddate;
                    enrollmentInfo.RoomId = model.Room;
                    _unitOfWork.Commit();
                    resultData = 2;
                    return resultData;
                }
                else
                {
                    return resultData;
                }

            }
            catch (Exception)
            {
                return resultData;
            }
        }
        public int DeleteStudentById(long studentId)
        {
            int returnState = 0;
            ResponseViewModel response = new ResponseViewModel();
            try
            {
                    long count = 0;
                    count = _studentRepository.FindBy(d => d.ID == studentId && d.IsDeleted == false).Count();
                    if (count > 0)
                    {
                    var studentDetails = _studentRepository.FindBy(m => m.ID == studentId && m.IsDeleted == false).FirstOrDefault();
                    studentDetails.IsDeleted = true;

                    //var parentInfo = studentDetails.ParentInfos.FirstOrDefault(m => m.StudentId == studentId);
                    //parentInfo.IsDeleted = true;


                    // var contactInfo = studentDetails.ContactInfos.FirstOrDefault(m => m.StudentId == studentId);
                    //contactInfo.IsDeleted = true;


                    var guardianInfo = studentDetails.GuardianInfos.FirstOrDefault(m => m.StudentId == studentId);
                    
                    guardianInfo.IsDeleted= true;

                    var enrollmentInfo = studentDetails.EnrollmentInfos.FirstOrDefault(m => m.StudentId == studentId);
                    enrollmentInfo.IsDeleted = true;

                    _unitOfWork.Commit();
                    returnState = 2;
                        return returnState;
                    }
                    else
                    {
                        return returnState;
                    }
                
            }
            catch
            {
                return returnState;
            }
        }
        //public CustomerGridViewModel GetEmployeeList(int currentPage, int currentPageSize, int filterCustomersColumn, string filter = null)
        //{
        //    CustomerGridViewModel customerViewModel = new CustomerGridViewModel(); 
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(filter))
        //        {
        //            var tempData = _userRepository.GetAll()
        //                    //.OrderBy(c => c.ID)
        //                    //.Skip(currentPage * currentPageSize)
        //                    //.Take(currentPageSize)
        //                    .ToList();
        //            /////
        //            if (filterCustomersColumn == 1)
        //                tempData = tempData.Where(c => c.Name.ToLower().Contains(filter)).ToList();
        //            else if (filterCustomersColumn == 2)
        //                tempData = tempData.Where(c => c.Phone.ToLower().Contains(filter)).ToList();
        //            else  if (filterCustomersColumn == 3)
        //                tempData = tempData.Where(c => c.Email.ToLower().Contains(filter)).ToList();
        //            else  if (filterCustomersColumn == 4)
        //            {
        //                try
        //                {
        //                    var time = Convert.ToDateTime(filter);
        //                    tempData = tempData.Where(c => c.DOB.GetValueOrDefault() == time).ToList();
        //                }
        //                catch (Exception ex)
        //                {

        //                    throw ex;
        //                }

        //            }
        //            else  if (filterCustomersColumn == 5)
        //                tempData = tempData.Where(c => c.City.CityName.ToLower().Contains(filter)).ToList();

        //            ////
        //            customerViewModel.customerList = (from c in tempData
        //                                              select new CustomerViewModel
        //                                              {
        //                                                  ID=c.ID,
        //                                                  Name = c.Name,
        //                                                  Email = c.Email,
        //                                                  Phone = c.Phone,
        //                                                  DOB = c.DOB,
        //                                                  CityId = c.CityId,
        //                                                  ProfileImageURL = c.ProfileImageURL,
        //                                                  IsDeleted = c.IsDeleted,
        //                                                  CityName = c.City.CityName,
        //                                               })
        //                                              .ToList();

        //            customerViewModel.totalCount = _userRepository.FindBy(c => c.Name.ToLower().Contains(filter))
        //                    .Skip(currentPage * currentPageSize)
        //                    .Take(currentPageSize).Count();
        //        }
        //        else
        //        {
        //            var tempData = _userRepository.GetAll()
        //                     .OrderBy(c=>c.ID)
        //                     .Skip(currentPage * currentPageSize)
        //                     .Take(currentPageSize)
        //                     .ToList();
        //            customerViewModel.customerList = (from c in tempData
        //                                              select new CustomerViewModel
        //                                              {
        //                                                  ID = c.ID,
        //                                                  Name = c.Name,
        //                                                  Email = c.Email,
        //                                                  Phone = c.Phone,
        //                                                  DOB = c.DOB,
        //                                                  CityId = c.CityId,
        //                                                  ProfileImageURL = c.ProfileImageURL,
        //                                                  IsDeleted = c.IsDeleted,
        //                                                  CityName = c.City.CityName,
        //                                              })
        //                                             .ToList();
        //            customerViewModel.totalCount = _userRepository.GetAll().Count();

        //        }  
        //    }
        //    catch (Exception)
        //    {

        //    }
        //    return customerViewModel;
        //}

        //public int DeleteEmployee(int customerId = 0)
        //{
        //   var tempdataToBeDeleted =_userRepository.GetSingle(customerId);
        //   try
        //   {
        //       _userRepository.SoftDelete(tempdataToBeDeleted);
        //       _unitOfWork.Commit();
        //       return 1;
        //   }
        //   catch (Exception)
        //   {

        //       return 0;
        //   }
        //}
        //public CustomerViewModel GetEmployeeDetailsById(int customerId = 0)
        //{
        //    CustomerViewModel customerViewModel= new CustomerViewModel ();
        //    try
        //    {
        //        var tempData = _userRepository.FindBy(c => c.ID == customerId).ToList();
        //        customerViewModel = (from c in tempData
        //                                          select new CustomerViewModel
        //                                          {
        //                                              ID = c.ID,
        //                                              Name = c.Name,
        //                                              Email = c.Email,
        //                                              Phone = c.Phone,
        //                                              DOB = c.DOB,
        //                                              CityId = c.CityId,
        //                                              CountryId = c.City.State.CountryId,
        //                                              StateId = c.City.StateId,
        //                                              ProfileImageURL = c.ProfileImageURL,
        //                                              IsDeleted = c.IsDeleted,
        //                                              CityName = c.City.CityName,
        //                                          }).FirstOrDefault();
        //    }
        //    catch (Exception)
        //    {
        //        new CustomerViewModel();

        //    }
        //    return customerViewModel;
        //}
        //public List<CountryViewModel> GetCountryList()
        //{
        //    List<CountryViewModel> countryViewModel = new List<CountryViewModel>();
        //    try
        //    {
        //        var tempData = _countryRepository.GetAll().ToList();
        //        countryViewModel = (from c in tempData
        //                             select new CountryViewModel
        //                             {
        //                                 ID = c.ID,
        //                                 CountryName = c.CountryName, 
        //                             }).ToList(); 
        //    }
        //    catch (Exception)
        //    {
        //        return new List<CountryViewModel>();
        //    }
        //    return countryViewModel;
        //}
        //public List<StateViewModel> GetStateByCountryId(long countryId)
        //{
        //    List<StateViewModel> stateViewModel = new List<StateViewModel>();
        //    try
        //    {
        //        var tempData = _stateRepository.FindBy(x => x.CountryId == countryId).ToList();
        //        stateViewModel = (from c in tempData
        //                            select new StateViewModel
        //                            {
        //                                ID = c.ID,
        //                                CountryId=c.CountryId,
        //                                StateName=c.StateName,
        //                                CountryName=c.Country.CountryName
        //                            }).ToList();
        //    }
        //    catch (Exception)
        //    {
        //        return new List<StateViewModel>();
        //    }
        //    return stateViewModel;
        //}
        //public List<CityViewModel> GetCityByCountryId(long stateId)
        //{
        //    List<CityViewModel> cityViewModel = new List<CityViewModel>();
        //    try
        //    {
        //        var tempData = _cityRepository.FindBy(x => x.StateId == stateId).ToList();
        //        cityViewModel = (from c in tempData
        //                          select new CityViewModel
        //                          {
        //                              ID = c.ID,
        //                              CityName = c.CityName,
        //                              StateName = c.State.StateName,
        //                              StateId=c.StateId,
        //                              CountryName = c.State.Country.CountryName
        //                          }).ToList();
        //    }
        //    catch (Exception)
        //    {
        //        return new List<CityViewModel>();
        //    }
        //    return cityViewModel;
        //}

        //public int UpdateEmployee(Customer customerViewModel)
        //{

        //    try
        //    {
        //       var oldData= _userRepository.FindBy(x => x.ID == customerViewModel.ID).FirstOrDefault();
        //       _userRepository.Edit(oldData, customerViewModel);
        //        _unitOfWork.Commit();
        //        return 1;
        //    }
        //    catch (Exception)
        //    {

        //        return 0;
        //    }
        //}
        public List<SkillViewModel> GetSkills()
        {

            Int32 totalCount = _skillRepository.GetAll().Count();

            var resultData = _skillRepository.GetAll();

            var returnData = resultData.Select(m =>
                new SkillViewModel
                {
                    ID = m.ID,
                    Skill=m.Skill,
                    SubSkill=m.SubSkill,
                    ClassesRequired = m.ClassesRequired,
                    DaysRequired = m.DaysRequired,
                    Details = m.Details,

                }).ToList();
            return returnData;
        }

        public int AddSkill(SkillViewModel addSkillViewModel)
        {

            try
            {
                var skill = new Entities.Skills.Skills()
                {
                    Skill = addSkillViewModel.Skill,
                    SubSkill = addSkillViewModel.SubSkill,
                    Details = addSkillViewModel.Details,
                    DaysRequired = addSkillViewModel.DaysRequired,
                    ClassesRequired=addSkillViewModel.ClassesRequired,
                    CategoryId=addSkillViewModel.CategoryId,
                    IsDeleted = false
                };
                _skillRepository.Add(skill);

                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {

                return 0;
            }
        }
    }
}
