﻿using System;
using System.Linq;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Class;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.Services.Abstract.Class;
using CDC.ViewModel.Common;
using CDC.ViewModel.Student;

namespace CDC.Services.Services.Class
{
    public class WaitListService : IWaitListService
    {
        private readonly IEntityBaseRepository<StudentDetail> _studentDetailRepository;
        private readonly IEntityBaseRepository<StudentInfo> _studentInfoRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IEntityBaseRepository<WaitListMap> _waitListMapRepository;

        public WaitListService(IEntityBaseRepository<WaitListMap> waitListMapRepository,
            IEntityBaseRepository<StudentSchedule> studentScheduleRepository,
            IEntityBaseRepository<StudentInfo> studentInfoRepository,
            IEntityBaseRepository<StudentDetail> studentDetailRepository, IUnitOfWork unitOfWork)
        {
            _waitListMapRepository = waitListMapRepository;
            _studentScheduleRepository = studentScheduleRepository;
            _studentInfoRepository = studentInfoRepository;
            _studentDetailRepository = studentDetailRepository;
            _unitOfWork = unitOfWork;
        }

        public ResponseViewModel MapWaitList(WaitListMap obj)
        {
            var response = new ResponseViewModel();
            try
            {
                if (IsStudentAllreadyinWaitList(obj) == false)
                {
                    obj.CreatedBy = 1;
                    obj.CreatedDate = DateTime.UtcNow;
                    obj.IsDeleted = false;
                    _waitListMapRepository.Add(obj);
                    _unitOfWork.Commit();
                    response.IsSuccess = true;
                    response.Message = "Added to WaitList SuccessFully";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Student is already mapped to wait list";
                }
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please Try Again";
            }
            return response;
        }

        public ResponseViewModel GetWaitListStudentList(WaitListMap obj)
        {
            var response = new ResponseViewModel();
            try
            {
                var today = DateTime.Today;
                var studentListObj =
                    _studentScheduleRepository.FindBy(f => f.ClassId == obj.ClassId)
                        .Where(x => x.StartDate > today)
                        .ToList();

                var studentListModel = studentListObj.Select(student => (from si in _studentInfoRepository.GetAll()
                    join sd in _studentDetailRepository.GetAll() on si.ID equals sd.studentId
                    where sd.studentId == student.StudentId
                    select new StudentViewModel
                    {
                        Firstname = si.FirstName, Lastname = si.LastName, Dateofbirth = si.DateOfBirth, EnrolledDate = si.CreatedDate, GenderName = si.Gender == 1 ? "Male" : "Female", Email = sd.Email, PhoneNumber = sd.PhoneNumber
                    }).FirstOrDefault()).ToList();
                if (studentListModel.Count > 0)
                {
                    response.IsSuccess = true;
                    response.Message = "Data Retrieved Successfully";
                    response.Content = studentListModel;
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "No Data Found";
                }
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please Try Again";
            }
            return response;
        }

        public ResponseViewModel DeleteMappedWaitList(WaitListMap obj)
        {
            var response = new ResponseViewModel();
            try
            {
                if (obj != null)
                {
                    _waitListMapRepository.SoftDelete(obj);
                    _unitOfWork.Commit();
                    response.IsSuccess = true;
                    response.Message = "Student removed from WaitList SuccessFully";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Student is already mapped to wait list";
                }
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please Try Again";
            }
            return response;
        }

        public ResponseViewModel SaveWaitListNotes(WaitListMap obj)
        {
            var response = new ResponseViewModel();
            try
            {
                if (obj != null)
                {
                    var oldObj = _waitListMapRepository.GetSingle(obj.ID);
                    obj.ModifiedDate = DateTime.UtcNow;
                    _waitListMapRepository.Edit(oldObj, obj);
                    _unitOfWork.Commit();
                    response.IsSuccess = true;
                    response.Message = "Notes Saved SuccessFully";
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Please Try Again";
                }
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Please Try Again";
            }
            return response;
        }

        public bool IsStudentAllreadyinWaitList(WaitListMap obj)
        {
            var isExist = _waitListMapRepository.FindBy(f => f.StudentId == obj.StudentId).ToList();
            if (isExist.Count > 0)
            {
                return true;
            }
            return false;
        }
    }
}