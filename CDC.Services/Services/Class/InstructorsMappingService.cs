﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Class;
using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using LinqKit;

namespace CDC.Services.Services.Class
{
    public class InstructorsMappingService : IInstructorsMappingService
    {
        private readonly IEntityBaseRepository<InstructorClassMap> _instructorsMappingRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ClassService" /> class.
        /// </summary>
        /// <param name="instructorsMappingRepository"></param>
        /// <param name="unitOfWork">The unit of work.</param>
        public InstructorsMappingService(
            IEntityBaseRepository<InstructorClassMap> instructorsMappingRepository, IUnitOfWork unitOfWork
            )
        {
            _instructorsMappingRepository = instructorsMappingRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Gets the instructor class map by identifier.
        /// </summary>
        /// <param name="instructorId">The instructor identifier.</param>
        /// <returns></returns>
        public InstructorClassMapViewModel GetInstructorClassMapById(long instructorId)
        {
            var obj = _instructorsMappingRepository.GetSingle(instructorId);
            var instructorObj = Mapper.Map<InstructorClassMap, InstructorClassMapViewModel>(obj);
            return instructorObj;
        }

        public void GetAllInstructorClassMap(SearchInstructorViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<InstructorClassMap>();
                if (model.ClassId > 0)
                    predicate = predicate.And(p => p.ClassId == model.ClassId);
                //if (!string.IsNullOrEmpty(model.StaffName))
                //    predicate = predicate.And(p => p.FirstName.Contains(model.StaffName) || p.LastName.Contains(model.StaffName));
                //if (model.StatusId > 0)
                //    predicate = predicate.And(p => p.StatusId == model.StatusId);
                //if (model.DesignationId > 0)
                //    predicate = predicate.And(p => p.DesignationId == model.DesignationId);

                var resultData = _instructorsMappingRepository.GetAll();
                long totalCount = resultData.Count();
                //resultData=resultData.OrderBy(m => m.ID);
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "InstructorName":
                        resultData = resultData.OrderBy(m => m.Staff.FirstName);
                        break;
                    case "-InstructorName":
                        resultData = resultData.OrderByDescending(m => m.Staff.FirstName);
                        break;
                    case "DesignationName":
                        resultData = resultData.OrderBy(m => m.Staff.Position.Name);
                        break;
                    case "-DesignationName":
                        resultData = resultData.OrderByDescending(m => m.Staff.Position.Name);
                        break;
                    case "Email":
                        resultData = resultData.OrderBy(m => m.Staff.Email);
                        break;
                    case "-Email":
                        resultData = resultData.OrderByDescending(m => m.Staff.Email);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.Staff.FirstName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1)*model.limit).Take(model.limit).ToList();

                var staffLst = Mapper.Map<List<InstructorClassMap>, List<InstructorClassMapViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }

        /// <summary>
        ///     Adds the instructor class map.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="transaction">The transaction.</param>
        public void AddInstructorClassMap(InstructorClassMapViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {
                var instructorInfo = Mapper.Map<InstructorClassMapViewModel, InstructorClassMap>(model);
                //if (IsStaffAlreadyinInstructorList(instructorInfo) == false)
                //{
                instructorInfo.CreatedBy = 1;
                instructorInfo.CreatedDate = DateTime.UtcNow;
                instructorInfo.IsDeleted = false;

                _instructorsMappingRepository.Add(instructorInfo);
                _unitOfWork.Commit();
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Added to InstructorsList SuccessFully");
                //}
                //else
                //{
                //    transaction.ReturnStatus = false;
                //    transaction.ReturnMessage.Add("Staff is already mapped to InstructorsList");
                //}
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Updates the instructor class map.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="responseInfo">The response information.</param>
        public void UpdateInstructorClassMap(InstructorClassMapViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var instructorMapDetail =
                    _instructorsMappingRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                var mapInstructorViewModelInfo = Mapper.Map<InstructorClassMapViewModel, InstructorClassMap>(model);
                _instructorsMappingRepository.Edit(instructorMapDetail, mapInstructorViewModelInfo);
                _unitOfWork.Commit();
                responseInfo.ReturnStatus = true;
                responseInfo.ReturnMessage.Add("Instructor detail successfully updated at " +
                                               DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        ///     Deletes the instructor class map by identifier.
        /// </summary>
        public ResponseViewModel DeleteInstructorClassMapById(InstructorClassMapViewModel model)
        {
            var responseInfo = new ResponseViewModel();
            try
            {
                var objtoDelete = Mapper.Map<InstructorClassMapViewModel, InstructorClassMap>(model);
                _instructorsMappingRepository.SoftDelete(objtoDelete);
                _unitOfWork.Commit();
                responseInfo.IsSuccess = true;
                responseInfo.Message = "Instructor successfully removed from this class";
            }
            catch (Exception)
            {
                responseInfo.IsSuccess = false;
                responseInfo.Message = "Please try again";
            }
            return responseInfo;
        }

        //private bool IsStaffAlreadyinInstructorList(InstructorClassMap obj)
        //{
        //    var IsExist = _instructorsMappingRepository.FindBy(f => f.StaffId == obj.StaffId).ToList();
        //    if (IsExist.Count > 0)
        //    {
        //        return true;
        //    }

        //    else
        //    {
        //        return false;
        //    }
        //}
    }
}