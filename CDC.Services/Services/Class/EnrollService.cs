﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Repositories;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using CDC.ViewModel.Family;
using LinqKit;
using CDC.Data.Infrastructure;
using CDC.Entities.NewParticipant;
using CDC.ViewModel.NewParticipant;
using System.Data.Entity.Core.Objects;
using CDC.Entities.NewParent;

namespace CDC.Services.Services.Class
{
    public class EnrollService : IEnrollService
    {
        private readonly ICommonService _commonService;
        private readonly IEntityBaseRepository<FamilyStudentMap> _familystudentMapRepository;
        private readonly IEntityBaseRepository<tblParticipantInfo> _participantRepository;
        private readonly IEntityBaseRepository<StudentDetail> _studentDetailRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;
        private readonly IEntityBaseRepository<tblParentParticipantMapping> _tblParentParticipantMappingRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ClassService" /> class.
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="commonService"></param>
        /// <param name="studentScheduleRepository"></param>
        /// <param name="familystudentMapRepository"></param>
        /// <param name="studentDetailRepository"></param>
        public EnrollService(
            IEntityBaseRepository<StudentSchedule> studentScheduleRepository,
            IEntityBaseRepository<FamilyStudentMap> familystudentMapRepository,
            IEntityBaseRepository<tblParticipantInfo> participantRepository,
            IEntityBaseRepository<StudentDetail> studentDetailRepository,
            IEntityBaseRepository<tblParentParticipantMapping> tblParentParticipantMappingRepository,
            IUnitOfWork unitOfWork,
            ICommonService commonService)
        {
            _studentScheduleRepository = studentScheduleRepository;
            _familystudentMapRepository = familystudentMapRepository;
            _participantRepository = participantRepository;
            _studentDetailRepository = studentDetailRepository;
            _commonService = commonService;
            _tblParentParticipantMappingRepository = tblParentParticipantMappingRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Get Current Enroll List.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>List<EnrollListViewModel />.</returns>
        public ResponseViewModel GetEnrollList(SearchStudentScheduleViewModel model)
        {
            var response = new ResponseViewModel();
            List<long> studentIds=new List<long>();

            var predicate = PredicateBuilder.True<StudentSchedule>();
            try
            {
                List<StudentSchedule> resultData;
                if (model.AgencyId > 0)
                {
                    if (model.FamilyId > 0)
                        studentIds = _tblParentParticipantMappingRepository.FindBy(a => a.NewParentInfoID == model.FamilyId).Select(i => i.NewParticipantInfoID.Value).ToList();
                    else
                        studentIds = _participantRepository.FindBy(a => a.AgencyId == model.AgencyId).Select(i => i.ID).ToList();
                }
                //if (model.FamilyId > 0)
                //{

                //    resultData = _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value) && EntityFunctions.TruncateTime(f.StartDate) <= EntityFunctions.TruncateTime(DateTime.UtcNow) && f.IsEnrolled).ToList();
                //}
                //else if (model.ClassId > 0)
                //{
                //    resultData = _studentScheduleRepository.FindBy(f => f.ClassId == model.ClassId).ToList();
                //}
                //else
                //{

                //    resultData =_studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value))
                //               .Where(x => EntityFunctions.TruncateTime(x.StartDate) <= EntityFunctions.TruncateTime(DateTime.UtcNow) && x.IsEnrolled)
                //               .ToList();
                //}

                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate =
                        predicate.And(
                            p =>
                                p.StudentInfo.FirstName.ToLower().Contains(model.ParticipantName.ToLower()) ||
                                p.StudentInfo.LastName.ToLower().Contains(model.ParticipantName.ToLower()));

                if (model.StudentId > 0)
                    predicate = predicate.And(p => p.StudentInfo.ID == model.StudentId);

                if (model.StatusId!=null)
                {
                    var enrollStatus = model.StatusId == 1 ? true : false;
                    predicate = predicate.And(p => p.IsEnrolled == enrollStatus);
                }
                if (model.StartDate!=null)
                {
                    predicate = predicate.And(p => EntityFunctions.TruncateTime(p.StartDate) == EntityFunctions.TruncateTime(model.StartDate));
                }
                else
                {
                    predicate = predicate.And(p => EntityFunctions.TruncateTime(p.StartDate) <= EntityFunctions.TruncateTime(DateTime.UtcNow));
                }
                if(studentIds.Count>0)
                {
                    predicate = predicate.And(p => studentIds.Contains(p.StudentId.Value));
                }
                if (model.ClassId > 0)
                {
                    predicate = predicate.And(p => p.ClassId==model.ClassId);
                }
            
                resultData = _studentScheduleRepository.GetAll().AsExpandable().Where(predicate).ToList();
                response.TotalRows = resultData.Count;
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.StudentInfo.ID).ToList();
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.ID).ToList();
                        break;
                    case "StudentName":
                        resultData = resultData.OrderBy(m => m.StudentInfo.FirstName).ToList();
                        break;
                    case "-StudentName":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.FirstName).ToList();
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.StudentInfo.FirstName).ToList();
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.FirstName).ToList();
                        break;
                    case "ClassName":
                        resultData = resultData.OrderBy(m => m.ClassInfo.ClassName).ToList();
                        break;
                    case "-ClassName":
                        resultData = resultData.OrderByDescending(m => m.ClassInfo.ClassName).ToList();
                        break;
                    case "StartDate":
                        resultData = resultData.OrderBy(m => m.StartDate).ToList();
                        break;
                    case "-StartDate":
                        resultData = resultData.OrderByDescending(m => m.StartDate).ToList();
                        break;
                    case "Room":
                        resultData = resultData.OrderBy(m => m.ClassInfo.Room.Name).ToList();
                        break;
                    case "-Room":
                        resultData = resultData.OrderByDescending(m => m.ClassInfo.Room.Name).ToList();
                        break;
                }
                resultData =
                    resultData.AsQueryable()                        
                        .Skip((model.page - 1) * model.limit)
                        .Take(model.limit)
                        .ToList();
                var responseData = Mapper.Map<List<StudentSchedule>, List<EnrollListViewModel>>(resultData);
                responseData.ForEach(
                    e =>
                        e.Age =
                            (e.DateOfBirth == null ? "" : _commonService.CalculateAge(Convert.ToDateTime(e.DateOfBirth))));
                response.IsSuccess = true;
                response.Message = "Data Retrieved Successfully";
                response.Content = responseData;
                return response;
            }
            catch (Exception EX)
            {
                response.IsSuccess = false;
                response.Message = "Data Retrieved Successfully";
                return response;
            }
        }

        /// <summary>
        ///     Get Future Enroll List.
        /// </summary>
        /// <returns>List<EnrollListViewModel />.</returns>
        public ResponseViewModel GetFutureEnrollList(SearchStudentScheduleViewModel model)
        {
         var response = new ResponseViewModel();
            List<long> studentIds;

            var predicate = PredicateBuilder.True<StudentSchedule>();
            try
            {
                List<StudentSchedule> resultData;
                var today = DateTime.UtcNow;
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate =
                        predicate.And(
                            p =>
                                p.StudentInfo.FirstName.ToLower().Contains(model.ParticipantName.ToLower()) ||
                                p.StudentInfo.LastName.ToLower().Contains(model.ParticipantName.ToLower()));

                if (model.StudentId > 0)
                    predicate = predicate.And(p => p.StudentInfo.ID == model.StudentId);
                if (model.FamilyId > 0)
                {
                    studentIds =
                        _tblParentParticipantMappingRepository.FindBy(f => f.NewParentInfoID == model.FamilyId)
                            .Select(s => s.NewParticipantInfoID.Value).Distinct()
                            .ToList();

                    resultData = _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value)&& f.StartDate > today).AsExpandable().Where(predicate).AsExpandable().ToList();
                }
                else if (model.ClassId > 0)
                {
                    resultData = _studentScheduleRepository.FindBy(f => f.ClassId == model.ClassId).AsExpandable().Where(predicate).AsExpandable().ToList();
                }
                else
                {

                    //studentIds =
                    //    _familystudentMapRepository.GetAll()
                    //        .Where(w => w.Family.AgencyId == model.AgencyId)
                    //        .Select(m => m.StudentId)
                    //        .ToList();

                    studentIds =
                        _participantRepository.GetAll()
                            .Where(w => w.AgencyId == model.AgencyId)
                            .Select(m => m.ID)
                            .ToList();

                    resultData =
                        _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value))
                            .Where(x => x.StartDate > today).AsExpandable().Where(predicate).AsExpandable()
                            .ToList();
                }
                response.TotalRows = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.StudentInfo.ID).ToList();
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.StartDate).ToList();
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.StudentInfo.FirstName).ToList();
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.FirstName).ToList();
                        break;
                    case "LastName":
                        resultData = resultData.OrderBy(m => m.StudentInfo.LastName).ToList();
                        break;
                    case "-LastName":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.LastName).ToList();
                        break;
                    case "ClassName":
                        resultData = resultData.OrderBy(m => m.ClassInfo.ClassName).ToList();
                        break;
                    case "-ClassName":
                        resultData = resultData.OrderByDescending(m => m.ClassInfo.ClassName).ToList();
                        break;
                    case "StartDate":
                        resultData = resultData.OrderBy(m => m.StartDate).ToList();
                        break;
                    case "-StartDate":
                        resultData = resultData.OrderByDescending(m => m.StartDate).ToList();
                        break;
                    case "Room":
                        resultData = resultData.OrderBy(m => m.ClassInfo.Room.Name).ToList();
                        break;
                    case "-Room":
                        resultData = resultData.OrderByDescending(m => m.ClassInfo.Room.Name).ToList();
                        break;
                }
                resultData =
                    resultData.AsQueryable()
                        .Where(predicate)
                        .Skip((model.page - 1) * model.limit)
                        .Take(model.limit)
                        .ToList();
                var responseData = Mapper.Map<List<StudentSchedule>, List<EnrollListViewModel>>(resultData);
                responseData.ForEach(
                    e =>
                        e.Age =
                            (e.DateOfBirth == null ? "" : _commonService.CalculateAge(Convert.ToDateTime(e.DateOfBirth))));
                response.IsSuccess = true;
                response.Message = "Data Retrieved Successfully";
                response.Content = responseData;
                return response;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Data Retrieved Successfully";
                return response;
            }
        }

        //GetParticipantWaitingListByClassId
        /// <summary>
        ///     Get Participant WaitingList By ClassId.
        /// </summary>
        /// <returns>List<EnrollListViewModel />.</returns>
        public ResponseViewModel GetParticipantWaitingListByClassId(SearchStudentScheduleViewModel model)
        {
            var response = new ResponseViewModel();
            List<long> studentIds;
            var predicate = PredicateBuilder.True<StudentSchedule>();
            try
            {
                var today = DateTime.UtcNow;
                    studentIds =
                        _participantRepository.GetAll()
                            .Where(w => w.AgencyId == model.AgencyId)
                            .Select(m => m.ID)
                            .ToList();
                    var resultData = _studentScheduleRepository.FindBy(f => studentIds.Contains(f.StudentId.Value))
                        .Where(x => EntityFunctions.TruncateTime(x.StartDate) > EntityFunctions.TruncateTime(today) && x.ClassId == model.ClassId)
                        .ToList();
                
                response.TotalRows = resultData.Count;
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate =
                        predicate.And(
                            p =>
                                p.StudentInfo.FirstName.ToLower().Contains(model.ParticipantName.ToLower()) ||
                                p.StudentInfo.LastName.ToLower().Contains(model.ParticipantName.ToLower()));
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.StudentInfo.ID).ToList();
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.StartDate).ToList();
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.StudentInfo.FirstName).ToList();
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.FirstName).ToList();
                        break;
                    case "LastName":
                        resultData = resultData.OrderBy(m => m.StudentInfo.LastName).ToList();
                        break;
                    case "-LastName":
                        resultData = resultData.OrderByDescending(m => m.StudentInfo.LastName).ToList();
                        break;
                    case "ClassName":
                        resultData = resultData.OrderBy(m => m.ClassInfo.ClassName).ToList();
                        break;
                    case "-ClassName":
                        resultData = resultData.OrderByDescending(m => m.ClassInfo.ClassName).ToList();
                        break;
                    case "StartDate":
                        resultData = resultData.OrderBy(m => m.StartDate).ToList();
                        break;
                    case "-StartDate":
                        resultData = resultData.OrderByDescending(m => m.StartDate).ToList();
                        break;
                    case "Room":
                        resultData = resultData.OrderBy(m => m.ClassInfo.Room.Name).ToList();
                        break;
                    case "-Room":
                        resultData = resultData.OrderByDescending(m => m.ClassInfo.Room.Name).ToList();
                        break;
                }
                resultData =
                    resultData.AsQueryable()
                        .Where(predicate)
                        .Skip((model.page - 1) * model.limit)
                        .Take(model.limit)
                        .ToList();
                var responseData = Mapper.Map<List<StudentSchedule>, List<EnrollListViewModel>>(resultData);
                responseData.ForEach(
                    e =>
                        e.Age =
                            (e.DateOfBirth == null ? "" : _commonService.CalculateAge(Convert.ToDateTime(e.DateOfBirth))));
                response.IsSuccess = true;
                response.Message = "Data Retrieved Successfully";
                response.Content = responseData;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Data Retrieved Successfully";
                return response;
            }
        }

        public ResponseViewModel UpdateFutureEnrollList(EnrollListViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                var oldStudentSchedule = _studentScheduleRepository.FindBy(x => x.ID == model.ID).FirstOrDefault();
                var newStudentSchedule = Mapper.Map<EnrollListViewModel, StudentSchedule>(model);
                if (oldStudentSchedule != null)
                {
                    newStudentSchedule.EndDate = oldStudentSchedule.EndDate;
                    newStudentSchedule.IsDeleted = false;
                    _studentScheduleRepository.Edit(oldStudentSchedule, newStudentSchedule);
                }
                _unitOfWork.Commit();
                respose.IsSuccess = true;
                respose.Message = "Student updated successfully";
            }
            catch (Exception )
            {
                respose.IsSuccess = false;
                respose.Message = "Something went wrong";
            }

            return respose;
        }

        public ResponseViewModel DeleteFutureEnrolledStudent(EnrollListViewModel model)
        {
            var respose = new ResponseViewModel();
            try
            {
                if (model.StudentId != 0)
                {

                    var oldStudentSchedule = _studentScheduleRepository.FindBy(x => x.ID == model.ID).FirstOrDefault();
                    _studentScheduleRepository.SoftDelete(oldStudentSchedule);
                    _unitOfWork.Commit();

                    respose.IsSuccess = true;
                    respose.Message = "Student deleted successfully.";
                }
            }
            catch (Exception)
            {
                respose.IsSuccess = false;
                respose.Message = "Something went wrong.";
            }

            return respose;

        }

        public ResponseViewModel GetUnEnrolledStudentList(SearchStudentScheduleViewModel model)
        {
            var response = new ResponseViewModel();
            var predicate = PredicateBuilder.True<tblParticipantInfo>();
            try
            {
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate =
                        predicate.And(
                            p =>

                                p.FirstName.ToLower().Contains(model.ParticipantName.ToLower()) ||
                                p.LastName.ToLower().Contains(model.ParticipantName.ToLower()));
                List<long> mappedStudentIds = new List<long>();
                List<long> participantIds = new List<long>();
                if (model.AgencyId > 0)
                {
                    if (model.FamilyId != null && model.FamilyId > 0)
                    {
                        participantIds =_tblParentParticipantMappingRepository.FindBy(m => m.NewParentInfoID == model.FamilyId).Select(m => m.NewParticipantInfoID.Value).ToList();
                        mappedStudentIds = _studentScheduleRepository.FindBy(f => f.ClassInfo.AgencyId == model.AgencyId && f.IsEnrolled && participantIds.Contains(f.StudentId.Value)).Select(s => s.StudentId.Value).ToList();
                    }
                    else
                        mappedStudentIds = _studentScheduleRepository.FindBy(f => f.ClassInfo.AgencyId == model.AgencyId && f.IsEnrolled).Select(s => s.StudentId.Value).ToList();

                }
                List<tblParticipantInfo> resultData=new List<tblParticipantInfo>();
                if (model.ClassId > 0)
                {
                    resultData = _participantRepository.GetAll().Where(w => !mappedStudentIds.Contains(w.ID)).AsExpandable().Where(predicate).AsExpandable().ToList();
                }
                else
                {
                    if (model.FamilyId > 0)
                    {
                        resultData = _participantRepository.GetAll().Where(w => !mappedStudentIds.Contains(w.ID)&& participantIds.Contains(w.ID)).AsExpandable().Where(predicate).AsExpandable().ToList();
                    }
                    else
                    {
                        //var resultData1 = _familystudentMapRepository.GetAll().Where(w => w.Family.AgencyId == model.AgencyId && !mappedstudent.Contains(w.StudentId)).Select(s => s.StudentId).ToList();

                        //resultData = _studentDetailRepository.GetAll().Where(w => resultData1.Contains(w.studentId)).AsExpandable().Where(predicate).AsExpandable().ToList();
                       // resultData = _participantRepository.GetAll().Where(a => a.AgencyId == model.AgencyId && !mappedstudent.Contains(a.ID)).Where(predicate).AsExpandable().ToList();

                        resultData = _participantRepository.GetAll().Where(a => a.AgencyId == model.AgencyId && !mappedStudentIds.Contains(a.ID)).AsExpandable().Where(predicate).AsExpandable().ToList();
                    }
                }
                response.TotalRows = resultData.Count;
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID).ToList();
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID).ToList();
                        break;
                    case "FirstName":
                        resultData = resultData.OrderBy(m => m.FirstName).ToList();
                        break;
                    case "-FirstName":
                        resultData = resultData.OrderByDescending(m => m.FirstName).ToList();
                        break;
                    case "LastName":
                        resultData = resultData.OrderBy(m => m.LastName).ToList();
                        break;
                    case "-LastName":
                        resultData = resultData.OrderByDescending(m => m.LastName).ToList();
                        break;
                    case "DateOfBirth":
                    case "Age":
                        resultData = resultData.OrderBy(m => m.DateOfBirth).ToList();
                        break;
                    case "-DateOfBirth":
                    case "-Age":
                        resultData = resultData.OrderByDescending(m => m.DateOfBirth).ToList();
                        break;

                    case "Gender":
                        resultData = resultData.OrderBy(m => m.Gender).ToList();
                        break;
                    case "-Gender":
                        resultData = resultData.OrderByDescending(m => m.Gender).ToList();
                        break;
                    //case "PhoneNumber":
                    //    resultData = resultData.OrderBy(m => m.PhoneNumber).ToList();
                    //    break;
                    //case "-PhoneNumber":
                    //    resultData = resultData.OrderByDescending(m => m.PhoneNumber).ToList();
                    //    break;
                }
                resultData =
                    resultData.AsQueryable()
                        .Where(predicate)
                        .Skip((model.page - 1) * model.limit)
                        .Take(model.limit)
                        .ToList();
                var responseData = Mapper.Map<List<tblParticipantInfo>, List<tblParticipantInfoViewModel>>(resultData);
                responseData.ForEach(
                    e =>
                        e.Age =
                            (e.DateOfBirth == null
                                ? ""
                                : _commonService.CalculateAge(Convert.ToDateTime(e.DateOfBirth))));
                response.IsSuccess = true;
                response.Message = "Data Retrieved Successfully";
                response.Content = responseData;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Data Retrieved Successfully";
                return response;
            }
        }

        public ResponseViewModel CancelEnrollMent(long EnrollMentId)
        {
            var response = new ResponseViewModel();
            try
            {

                var enrolledStudent = _studentScheduleRepository.FindBy(x => x.ID == EnrollMentId).FirstOrDefault();
                var oldEnrolledDetails = enrolledStudent;
                if (enrolledStudent != null)
                {
                    enrolledStudent.IsEnrolled = false;
                    enrolledStudent.EndDate = DateTime.Now;
                    _studentScheduleRepository.Edit(oldEnrolledDetails, enrolledStudent);
                }
                _unitOfWork.Commit();
                response.IsSuccess = true;
                response.Message = "Enrollment cancelled successfully.";
            }
           catch(Exception)
            {
                response.IsSuccess = false;
                response.Message = "Something went wrong";
            }
            return response;
        }
        public ResponseViewModel GetEnrollHistory(SearchStudentScheduleViewModel model)
        {
            var response = new ResponseViewModel();
            var predicate = PredicateBuilder.True<StudentSchedule>();
            try
            {
                if (!string.IsNullOrEmpty(model.ParticipantName))
                    predicate =
                        predicate.And(
                            p =>
                                p.StudentInfo.FirstName.ToLower().Contains(model.ParticipantName.ToLower()) ||
                                p.StudentInfo.LastName.ToLower().Contains(model.ParticipantName.ToLower()));
                if (model.StudentId>0)
                    predicate =predicate.And(p =>p.StudentId == model.StudentId);
                if (model.ClassId > 0)
                    predicate = predicate.And(p => p.ClassId == model.ClassId);
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.ClassInfo.AgencyId == model.AgencyId);
                if (model.StartDate!=null)
                    predicate = predicate.And(p => p.StartDate.Day==model.StartDate.Value.Day && p.StartDate.Month == model.StartDate.Value.Month && p.StartDate.Year == model.StartDate.Value.Year);
                if(model.FamilyId>0)
                {
                    var studentIds = _tblParentParticipantMappingRepository.FindBy(m => m.NewParentInfoID == model.FamilyId).Select(m => m.NewParticipantInfoID).ToList();
                    predicate = predicate.And(p => studentIds.Contains(p.StudentId.Value));
                }

                var enrolledStudent = _studentScheduleRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = enrolledStudent.Count();
                switch (model.order)
                {
                    case "id":
                        enrolledStudent = enrolledStudent.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        enrolledStudent = enrolledStudent.OrderByDescending(m => m.ID);
                        break;
                    case "ParticipantName":
                        enrolledStudent = enrolledStudent.OrderBy(m => m.StudentInfo.FirstName);
                        break;
                    case "-ParticipantName":
                        enrolledStudent = enrolledStudent.OrderByDescending(m => m.StudentInfo.FirstName);
                        break;
                    case "ClassName":
                        enrolledStudent = enrolledStudent.OrderBy(m => m.ClassInfo.ClassName);
                        break;
                    case "-ClassName":
                        enrolledStudent = enrolledStudent.OrderByDescending(m => m.ClassInfo.ClassName);
                        break;
                    case "StartDate":
                        enrolledStudent = enrolledStudent.OrderBy(m => m.StartDate);
                        break;
                    case "-StartDate":
                        enrolledStudent = enrolledStudent.OrderByDescending(m => m.StartDate);
                        break;
                    case "Status":
                        enrolledStudent = enrolledStudent.OrderBy(m => m.IsEnrolled);
                        break;
                    case "-Status":
                        enrolledStudent = enrolledStudent.OrderByDescending(m => m.IsEnrolled);
                        break;
                    //case "Status":
                    //    enrolledStudent = enrolledStudent.OrderBy(m =>);
                    //    break;
                    //case "-Status":
                    //    enrolledStudent = enrolledStudent.OrderByDescending(m => m.IsEnrolled);
                    //    break;
                }
                enrolledStudent =enrolledStudent.AsExpandable().Skip((model.page - 1) * model.limit).Take(model.limit);
                var responseData = Mapper.Map<List<StudentSchedule>, List<EnrollListViewModel>>(enrolledStudent.ToList());
                response.IsSuccess = true;
                response.Content = responseData;
                response.TotalRows = totalCount;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Something went wrong";
            }
            return response;
        }
    }
}