﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Class;
using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using LinqKit;

namespace CDC.Services.Services.Class
{
    /// <summary>
    ///     The Lesson Plan Service
    /// </summary>
    /// <seealso cref="CDC.Services.Abstract.Class.ILessonPlanService" />
    public class LessonPlanService : ILessonPlanService
    {
        private readonly IEntityBaseRepository<LessonPlan> _lessonPlanRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="LessonPlanService" /> class.
        /// </summary>
        /// <param name="lessonPlanRepository">The lesson plan repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public LessonPlanService(IEntityBaseRepository<LessonPlan> lessonPlanRepository, IUnitOfWork unitOfWork)
        {
            _lessonPlanRepository = lessonPlanRepository;
            _unitOfWork = unitOfWork;
        }


        /// <summary>
        ///     Gets the lesson plan.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="totalCount">The total count.</param>
        /// <returns></returns>
        public List<LessonPlanViewModel> GetLessonPlan(SearchLessonPlanViewModel model, out int totalCount)
        {
            var predicate = PredicateBuilder.False<LessonPlan>();
            predicate = predicate.Or(p => p.Name.Contains(model.name));
            var resultData = _lessonPlanRepository.GetAll().AsExpandable().Where(w=>w.ClassId==model.ClassId).Where(predicate).OrderBy(m => m.ID);
            switch (model.order)
            {
                case "Name":
                    resultData = resultData.OrderBy(m => m.Name);
                    break;
                case "-Name":
                    resultData = resultData.OrderByDescending(m => m.Name);
                    break;
                case "Theme":
                    resultData = resultData.OrderBy(m => m.Theme);
                    break;
                case "-Theme":
                    resultData = resultData.OrderByDescending(m => m.Theme);
                    break;
                case "Order":
                    resultData = resultData.OrderBy(m => m.Order);
                    break;
                case "-Order":
                    resultData = resultData.OrderByDescending(m => m.Order);
                    break;
                case "Date":
                    resultData = resultData.OrderBy(m => m.Date);
                    break;
                case "-Date":
                    resultData = resultData.OrderByDescending(m => m.Date);
                    break;
                case "Description":
                    resultData = resultData.OrderBy(m => m.Description);
                    break;
                case "-Description":
                    resultData = resultData.OrderByDescending(m => m.Description);
                    break;
                case "SpecialInstructions":
                    resultData = resultData.OrderBy(m => m.SpecialInstructions);
                    break;
                case "-SpecialInstructions":
                    resultData = resultData.OrderByDescending(m => m.SpecialInstructions);
                    break;
            }
            totalCount = resultData.Count();
            var returnData =
                Mapper.Map<List<LessonPlan>, List<LessonPlanViewModel>>(
                    resultData.Skip((model.page - 1)*model.limit).Take(model.limit).ToList());
            return returnData;
        }

        /// <summary>
        ///     Adds the update lesson plan.
        /// </summary>
        /// <param name="addLessonPlanViewModel">The add lesson plan view model.</param>
        /// <returns></returns>
        public int AddUpdateLessonPlan(LessonPlanViewModel addLessonPlanViewModel)
        {
            //var provider = new PhotoMultipartFormDataStreamProvider(this.workingFolder);

            //await addLessonPlanViewModel.UploadFile.Content.ReadAsMultipartAsync(provider);
            //var photos = provider.FileData[0].LocalFileName;
            //var photos = new List<PhotoViewModel>();

            //foreach (var file in provider.FileData)
            //{
            //    var fileInfo = new FileInfo(file.LocalFileName);

            //    photos.Add(new PhotoViewModel
            //    {
            //        Name = fileInfo.Name,
            //        Created = fileInfo.CreationTime,
            //        Modified = fileInfo.LastWriteTime,
            //        Size = fileInfo.Length / 1024
            //    });
            //}

            //return photos;
            //addLessonPlanViewModel.UploadFile = photos;
            try
            {
                if (addLessonPlanViewModel.ID == 0)
                {
                    var lessonPlan = Mapper.Map<LessonPlanViewModel, LessonPlan>(addLessonPlanViewModel);
                    _lessonPlanRepository.Add(lessonPlan);
                }
                else
                {
                    var lessonPlanDetail =
                        _lessonPlanRepository.FindBy(m => m.ID == addLessonPlanViewModel.ID && m.IsDeleted == false)
                            .FirstOrDefault();
                    if (lessonPlanDetail != null)
                    {
                        addLessonPlanViewModel.IsDeleted = false;
                        var lessonPlan = Mapper.Map<LessonPlanViewModel, LessonPlan>(addLessonPlanViewModel);
                        _lessonPlanRepository.Edit(lessonPlanDetail, lessonPlan);
                    }
                }
                _unitOfWork.Commit();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        //public async Task<LessonPlanViewModel> AddUpdateLessonPlan(HttpRequestMessage request)
        //{
        //    var provider = new PhotoMultipartFormDataStreamProvider(this.workingFolder);

        //    await request.Content.ReadAsMultipartAsync(provider);

        //    var photos = new LessonPlan();
        //    foreach (var file in provider.FileData)
        //    {
        //        var fileInfo = new FileInfo(file.LocalFileName);
        //        photos.Name = fileInfo.Name;
        //        photos.Date = fileInfo.
        //        photos.Name = fileInfo.Name;
        //        photos.Name = fileInfo.Name;
        //        photos.Name = fileInfo.Name;
        //        photos.Name = fileInfo.Name;
        //        photos.Name = fileInfo.Name;
        //        photos.Name = fileInfo.Name;
        //        _lessonPlanRepository.Add(photos);
        //    }
        //    _unitOfWork.Commit();
        //    return photos;
        //}
        /// <summary>
        ///     Deletes the lesson plan by identifier.
        /// </summary>
        /// <param name="lessonPlanId">The lesson plan identifier.</param>
        /// <returns></returns>
        public int DeleteLessonPlanById(long lessonPlanId)
        {
            var returnState = 0;
            try
            {
                long count = _lessonPlanRepository.FindBy(d => d.ID == lessonPlanId && d.IsDeleted == false).Count();
                if (count > 0)
                {
                    var skillDetail =
                        _lessonPlanRepository.FindBy(m => m.ID == lessonPlanId && m.IsDeleted == false).FirstOrDefault();
                    if (skillDetail != null) skillDetail.IsDeleted = true;
                    _unitOfWork.Commit();
                    returnState = 2;
                    return returnState;
                }
                return returnState;
            }
            catch
            {
                return returnState;
            }
        }

        //    }
        //        throw new ArgumentException("the destination path " + this.workingFolder + " could not be found");
        //    {
        //    if (!Directory.Exists(this.workingFolder))
        //{
        //private void CheckTargetDirectory()
        //}
    }
}