﻿using System;
using System.Collections.Generic;

using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;

using CDC.ViewModel.Common;

using LinqKit;
using CDC.Services.Abstract.Class;
using CDC.Entities.Class;
using CDC.ViewModel.Class;

namespace CDC.Services.Services.Class
{
    public class FoodManagementMealPatternService : IFoodManagementMealPatternService
    {
        private readonly IEntityBaseRepository<FoodManagementMealPattern> _foodMealPatternRepository;
        private readonly IEntityBaseRepository<FoodManagementMealPatternItems> _foodManagementMealPatternItemsRepository;
        private readonly IUnitOfWork _unitOfWork;
        public FoodManagementMealPatternService(
            IEntityBaseRepository<FoodManagementMealPattern> foodMealPatternRepository,
            IEntityBaseRepository<FoodManagementMealPatternItems> foodManagementMealPatternItemsRepository,
            IUnitOfWork unitOfWork)
        {
            _foodMealPatternRepository = foodMealPatternRepository;
            _foodManagementMealPatternItemsRepository = foodManagementMealPatternItemsRepository;
            _unitOfWork = unitOfWork;

        }
      
        public FoodManagementMealPatternViewModel GetFoodMealPatternById(long FoodMealPatternId)
        {
            var obj = _foodMealPatternRepository.GetSingle(FoodMealPatternId);
            var foodMealPatternObj = Mapper.Map<FoodManagementMealPattern, FoodManagementMealPatternViewModel>(obj);
            return foodMealPatternObj;
        }
    
        public void GetAllFoodManagementMealPattern(SearchFoodMealPatternViewModel model, out ResponseViewModel responseInfo)
        {
            responseInfo = new ResponseViewModel();
            try
            {
                var predicate = PredicateBuilder.True<FoodManagementMealPattern>();
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (!string.IsNullOrEmpty(model.AgeGroup))
                    predicate =
                        predicate.And(p => p.AgeGroup==model.AgeGroup);
                if (!string.IsNullOrEmpty(model.ItemName))
                    predicate =
                         predicate.And(p => p.FoodManagementMealPatternItemsInfos.FirstOrDefault().FoodManagementMaster.MealItem.Name.Contains(model.ItemName));
                
                if (model.MealTypeId>0)
                    predicate =
                        predicate.And(p => p.FoodManagementMealPatternItemsInfos.FirstOrDefault().FoodManagementMaster.MealTypeId == model.MealTypeId);
                var resultData = _foodMealPatternRepository.GetAll().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "Item":
                        resultData = resultData.OrderBy(m => m.FoodManagementMealPatternItemsInfos.FirstOrDefault().FoodManagementMaster.MealItem.Name);
                        break;
                    case "-Item":
                        resultData = resultData.OrderByDescending(m => m.FoodManagementMealPatternItemsInfos.FirstOrDefault().FoodManagementMaster.MealItem.Name);
                        break;
                    case "Meal":
                        resultData = resultData.OrderBy(m => m.FoodManagementMealPatternItemsInfos.FirstOrDefault().FoodManagementMaster.MealType.Name);
                        break;
                    case "-Meal":
                        resultData = resultData.OrderByDescending(m => m.FoodManagementMealPatternItemsInfos.FirstOrDefault().FoodManagementMaster.MealType.Name);
                        break;
                }
                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.AgeGroup);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();
                var staffLst = Mapper.Map<List<FoodManagementMealPattern>, List<FoodManagementMealPatternViewModel>>(returnData);
                responseInfo.TotalRows = totalCount;
                responseInfo.IsSuccess = true;
                responseInfo.Content = staffLst;
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
     
        public void AddFoodMealPattern(FoodManagementMealPatternViewModel model, out ResponseInformation transaction)
        {
            transaction = new ResponseInformation();
            try
            {

                var foodManagementMealPattern = Mapper.Map<FoodManagementMealPatternViewModel, FoodManagementMealPattern>(model);

                _foodMealPatternRepository.Add(foodManagementMealPattern);
                _unitOfWork.Commit();

                if (foodManagementMealPattern.ID > 0)
                {
                    if (model.ItemList != null && model.ItemList.Count>0)
                    {
                        foreach (var item in model.ItemList)
                        {
                            if (item.IsChecked)
                            {
                                var entity = Mapper.Map<FoodManagementMealPatternItemsViewModel, FoodManagementMealPatternItems>(item);
                                entity.FoodManagementMealPatternId = foodManagementMealPattern.ID;
                                entity.FoodManagementMasterId = item.ID;
                                _foodManagementMealPatternItemsRepository.Add(entity);
                            }
                        }
                        _unitOfWork.Commit();
                    }
                }
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Food meal pattern details added successfully");
            }
            catch (Exception ex)
            {
                transaction.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                transaction.ReturnStatus = false;
                transaction.ReturnMessage.Add(errorMessage);
            }
        }


      
        public void UpdateFoodMealPattern(FoodManagementMealPatternViewModel model, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {

                var foodDetail =
                    _foodMealPatternRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                model.IsDeleted = false;
                var mapFoodManagementMealPatternViewModelInfo = Mapper.Map<FoodManagementMealPatternViewModel, FoodManagementMealPattern>(model);
                _foodMealPatternRepository.Edit(foodDetail, mapFoodManagementMealPatternViewModelInfo);
                _unitOfWork.Commit();

                var existingData = _foodManagementMealPatternItemsRepository.All.Where(m => m.FoodManagementMealPatternId == model.ID).ToList();
                existingData.ForEach(m => _foodManagementMealPatternItemsRepository.Delete(m));

                if (foodDetail != null && foodDetail.ID > 0)
                {
                    if (model.ItemList != null && model.ItemList.Count > 0)
                    {
                        foreach (var item in model.ItemList)
                        {
                            if (item.IsChecked)
                            {
                                var entity = Mapper.Map<FoodManagementMealPatternItemsViewModel, FoodManagementMealPatternItems>(item);
                                entity.FoodManagementMealPatternId = foodDetail.ID;
                                entity.FoodManagementMasterId = item.ID;
                                _foodManagementMealPatternItemsRepository.Add(entity);
                            }
                        }
                        _unitOfWork.Commit();
                    }
                }
                responseInfo.ReturnStatus = true;
                responseInfo.ReturnMessage.Add("Food  meal pattern details updated successfully");

            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
     
        public void DeleteFoodMealPatternById(long FoodMealPatternId, out ResponseInformation responseInfo)
        {
            responseInfo = new ResponseInformation();
            try
            {
                var mealDetails =
                    _foodMealPatternRepository.FindBy(m => m.ID == FoodMealPatternId && m.IsDeleted == false).FirstOrDefault();
                if (mealDetails != null && mealDetails.ID > 0)
                {
                    mealDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    responseInfo.ReturnStatus = true;
                    responseInfo.ReturnMessage.Add("Food meal pattern details successfully deleted at " + DateTime.UtcNow);
                }
            }
            catch (Exception ex)
            {
                responseInfo.ReturnMessage = new List<string>();
                var errorMessage = ex.Message;
                responseInfo.ReturnStatus = false;
                responseInfo.ReturnMessage.Add(errorMessage);
            }
        }
    }
}