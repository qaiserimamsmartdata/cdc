﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CDC.Data.Infrastructure;
using CDC.Data.Repositories;
using CDC.Entities.Class;
using CDC.Entities.Schedule;
using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using LinqKit;
using CDC.Entities.Tools.Class;
using CDC.Services.Abstract;

namespace CDC.Services.Services.Class
{
    /// <summary>
    /// Class  Service
    /// </summary>
    /// <seealso cref="IClassService" />
    public class ClassService : IClassService
    {
        private readonly IEntityBaseRepository<ClassInfo> _classRepository;
        private readonly IEntityBaseRepository<ClassStatus> _classStatusRepository;
        private readonly IEntityBaseRepository<StudentSchedule> _studentScheduleRepository;

        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommonService _icommonService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassService"/> class.
        /// </summary>
        /// <param name="classRepository">The class repository.</param>
        /// <param name="classStatusRepository"></param>
        /// <param name="studentScheduleRepository"></param>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="icommonService"></param>
        public ClassService(
          IEntityBaseRepository<ClassInfo> classRepository,
          IEntityBaseRepository<ClassStatus> classStatusRepository,
          IEntityBaseRepository<StudentSchedule> studentScheduleRepository,
          IUnitOfWork unitOfWork,
          ICommonService icommonService
          )
        {
            _classRepository = classRepository;
            _classStatusRepository = classStatusRepository;
            _unitOfWork = unitOfWork;
            _studentScheduleRepository = studentScheduleRepository;
            _icommonService = icommonService;
        }

        /// <summary>
        /// Gets the class by identifier.
        /// </summary>
        /// <returns></returns>
        public ClassViewModel GetById(long id,string timezone)
        {
            var resultData = _classRepository.GetSingle(id);
            resultData.StartTime = resultData.StartTime==null?null: _icommonService.ConvertTimeFromUTC(DateTime.UtcNow, resultData.StartTime.Value, timezone);
            resultData.EndTime = resultData.EndTime==null?null:_icommonService.ConvertTimeFromUTC(DateTime.UtcNow, resultData.EndTime.Value, timezone);
            resultData.ClassStartDate = resultData.ClassStartDate;
            resultData.ClassEndDate = resultData.ClassEndDate;
            resultData.RegistrationStartDate = resultData.RegistrationStartDate;
            resultData.AgeCutOffDate = resultData.AgeCutOffDate;

            var returnData = Mapper.Map<ClassInfo, ClassViewModel>(resultData);
            return returnData;
        }


        /// <summary>
        /// Gets all classes by filter.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="resp"></param>
        /// <returns></returns>
        public void All(SearchClassViewModel model,out ResponseInformation resp)
        {
            resp = new ResponseInformation();
            try
            {
                var predicate = PredicateBuilder.True<ClassInfo>();

                if (!string.IsNullOrEmpty(model.ClassName))
                    predicate = predicate.And(p => p.ClassName.Contains(model.ClassName));
                if (model.AgencyId > 0)
                    predicate = predicate.And(p => p.AgencyId == model.AgencyId);
                if (model.SessionId > 0)
                    predicate = predicate.And(p => p.SessionId == model.SessionId);
                if (model.StatusId > 0)
                    predicate = predicate.And(p => p.ClassStatusId == model.StatusId);
                if (model.CategoryId > 0)
                    predicate = predicate.And(p => p.CategoryId == model.CategoryId);
                var resultData = _classRepository.GetAll().AsExpandable().Where(predicate).AsExpandable();
                long totalCount = resultData.Count();
                if (model.ClassStartDate != null && model.ClassEndDate != null)
                {
                    resultData = resultData.Where(m => m.ClassStartDate >= model.ClassStartDate && m.ClassEndDate <= model.ClassEndDate);
                }
                else if (model.ClassStartDate != null && model.ClassEndDate == null)
                {

                    resultData = resultData.Where(m => m.ClassStartDate >= model.ClassStartDate);
                }
                switch (model.order)
                {
                    case "id":
                        resultData = resultData.OrderBy(m => m.ID);
                        break;
                    case "-id":
                        resultData = resultData.OrderByDescending(m => m.ID);
                        break;
                    case "ClassName":
                        resultData = resultData.OrderBy(m => m.ClassName);
                        break;
                    case "-ClassName":
                        resultData = resultData.OrderByDescending(m => m.ClassName);
                        break;
                    case "CategoryName":
                        resultData = resultData.OrderBy(m => m.CategoryId);
                        break;
                    case "-CategoryName":
                        resultData = resultData.OrderByDescending(m => m.CategoryId);
                        break;
                    case "RegistrationDate":
                        resultData = resultData.OrderBy(m => m.RegistrationStartDate);
                        break;
                    case "-RegistrationDate":
                        resultData = resultData.OrderByDescending(m => m.RegistrationStartDate);
                        break;
                    case "ClassStartDate":
                        resultData = resultData.OrderBy(m => m.ClassStartDate);
                        break;
                    case "-ClassStartDate":
                        resultData = resultData.OrderByDescending(m => m.ClassStartDate);
                        break;
                    case "ClassEndDate":
                        resultData = resultData.OrderBy(m => m.ClassEndDate);
                        break;
                    case "-ClassEndDate":
                        resultData = resultData.OrderByDescending(m => m.ClassEndDate);
                        break;
                    case "StartTime":
                        resultData = resultData.OrderBy(m => m.StartTime);
                        break;
                    case "-StartTime":
                        resultData = resultData.OrderByDescending(m => m.StartTime);
                        break;
                    case "EndTime":
                        resultData = resultData.OrderBy(m => m.EndTime);
                        break;
                    case "-EndTime":
                        resultData = resultData.OrderByDescending(m => m.EndTime);
                        break;
                    case "Status":
                        resultData = from result in resultData
                                     join cs in _classStatusRepository.GetAll()
                                     on result.ClassStatusId equals cs.ID into joinedData
                                     from cs in joinedData.DefaultIfEmpty()
                                     orderby cs.Name
                                     select result;
                        break;
                    case "-Status":
                        resultData = from result in resultData
                                     join cs in _classStatusRepository.GetAll()
                                     on result.ClassStatusId equals cs.ID into joinedData
                                     from cs in joinedData.DefaultIfEmpty()
                                     orderby cs.Name descending
                                     select result;
                        break;

                }

                if (string.IsNullOrEmpty(model.order))
                    resultData = resultData.OrderByDescending(m => m.ClassName);
                var returnData = resultData.AsExpandable().Where(predicate).AsExpandable()
                    .Skip((model.page - 1) * model.limit).Take(model.limit).ToList();

                var classLst = Mapper.Map<List<ClassInfo>, List<ClassViewModel>>(returnData);
                foreach (var item in classLst)
                {
                    item.TotalCnt = totalCount;
                    item.EnrolledParticipants = _studentScheduleRepository.FindBy(f => f.ClassId == item.ID && f.IsEnrolled).ToList().Count;
                }
                resp.ReturnMessage.Add("Class list retrieved successfully");
                resp.Content = classLst;
                resp.TotalRows = totalCount;
                resp.ReturnStatus = true;
            }
            catch(Exception ex)
            {
                resp.ReturnMessage.Add(ex.Message);
                resp.ReturnStatus = false;
            }
        }

        /// <summary>
        /// Adds the class.
        /// </summary>
        /// <returns></returns>
        public void Add(ClassViewModel model, out ResponseInformation resp)
        {
            resp = new ResponseInformation();
            try
            {

                //model.StartTime = _icommonService.ConvertToUTC(new DateTime((DateTime.UtcNow.Date + model.StartTime.Value).Ticks, DateTimeKind.Unspecified), model.TimeZone).Value.TimeOfDay;
                //model.EndTime = _icommonService.ConvertToUTC(new DateTime((DateTime.UtcNow.Date + model.EndTime.Value).Ticks, DateTimeKind.Unspecified), model.TimeZone).Value.TimeOfDay;
                var classinfo = Mapper.Map<ClassViewModel, ClassInfo>(model);
                _classRepository.Add(classinfo);
                _unitOfWork.Commit();
                resp.ReturnStatus = true;
                resp.ReturnMessage.Add("Class has been added successfully");
            }
            catch (Exception ex)
            {
                resp.ReturnStatus = false;
                resp.ReturnMessage.Add(ex.Message);

            }
        }

        /// <summary>
        /// Updates the class detail by classID.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="resp"></param>
        /// <returns></returns>
        public void Update(ClassViewModel model,out ResponseInformation resp)
        {
            
            resp = new ResponseInformation();
            try
            {
                if (model.RegistrationStartDate != null)
                    model.RegistrationStartDate = model.RegistrationStartDate.Value.Date;
                if (model.ClassStartDate != null)
                    model.ClassStartDate = model.ClassStartDate.Value.Date;
                if (model.ClassEndDate != null)
                    model.ClassEndDate = model.ClassEndDate.Value.Date;
                if (model.AgeCutOffDate != null)
                    model.AgeCutOffDate = model.AgeCutOffDate.Value.Date;

                var classDetail = _classRepository.FindBy(m => m.ID == model.ID && m.IsDeleted == false).FirstOrDefault();
                int enrolledParticipant = _studentScheduleRepository.FindBy(f => f.ClassId == model.ID && f.IsEnrolled).ToList().Count;
                if (model.EnrollCapacity >= enrolledParticipant)
                {
                    model.StartTime = _icommonService.ConvertToUTC(new DateTime((DateTime.UtcNow.Date + model.StartTime.Value).Ticks, DateTimeKind.Unspecified),model.TimeZone).Value.TimeOfDay;
                    model.EndTime = _icommonService.ConvertToUTC(new DateTime((DateTime.UtcNow.Date + model.EndTime.Value).Ticks, DateTimeKind.Unspecified), model.TimeZone).Value.TimeOfDay;
                    var abc = Mapper.Map<ClassViewModel, ClassInfo>(model);
                    _classRepository.Edit(classDetail, abc);
                    _unitOfWork.Commit();
                    resp.ReturnStatus = true;
                    resp.ReturnMessage.Add("Class updated successfully.");
                }
                else
                {
                    resp.ReturnStatus = true;
                    resp.ReturnMessage.Add("EnrollCapacity_Error");
                }
            }
            catch (Exception ex)
            {
                resp.ReturnStatus = false;
                resp.ReturnMessage.Add(ex.Message);
            }
        }

        /// <summary>
        /// Deletes the class by identifier.
        /// </summary>
        /// <returns></returns>
        public void Delete(long id,out ResponseInformation resp)
        {
            resp = new ResponseInformation();
            try
            {
                var studentDetails = _classRepository.FindBy(m => m.ID == id && m.IsDeleted == false).FirstOrDefault();
                if (studentDetails != null)
                {
                    studentDetails.IsDeleted = true;
                    _unitOfWork.Commit();
                    resp.ReturnMessage.Add("Class deleted successfully");
                }
                else
                    resp.ReturnMessage.Add("Class is not available.");
                resp.ReturnStatus = true;
                
            }
            catch (Exception ex)
            {
                resp.ReturnStatus = false;
                resp.ReturnMessage.Add(ex.Message);
            }
        }

    }
}
