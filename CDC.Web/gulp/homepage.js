'use strict';

var gulp = require('gulp');
var path = require('path');

var paths = gulp.paths;

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('homepage1',function() {
    return gulp.src(
        paths.homepage + '/**/*.html'
    ).pipe(
        $.minifyHtml({
        empty: true,
        spare: true,
        quotes: true
      }))
      .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'app',
      root: 'app'
    }))
    .pipe(gulp.dest(paths.tmp + '/partials/'));
    
});