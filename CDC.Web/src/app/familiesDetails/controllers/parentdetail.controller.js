(function () {
    'use strict';

    angular
        .module('app.family')
        .controller('ParentDetailController', ParentDetailController)
        .controller('AddParentController', AddParentController)
        .controller('EditParentController', EditParentController);

    /* @ngInject */
    function ParentDetailController($scope, $mdDialog, $state, $http, FamilyService,
        AgencyService, notificationService, CommonService,
        filerService, HOST_URL, $rootScope, imageUploadService, $localStorage) {
        var vm = this;

        // if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        vm.FamilyId = 0;
        var familyDetails = JSON.parse(localStorage.getItem('familyDetails'));
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        vm.detailParentInfo = detailParentInfo;
        vm.showAddParentDialogue = showAddParentDialogue;
        vm.showEditParentDialogue = showEditParentDialogue;
        vm.showDeleteParentConfirmDialoue = showDeleteParentConfirmDialoue;


        vm.reset4 = reset4;
        vm.toggleRight4 = filerService.toggleRight4();
        vm.isOpenRight4 = filerService.isOpenRight4();
        vm.close4 = filerService.close4();
        vm.ImageUrlPath = HOST_URL.url;
        vm.GetParentListByFamily = GetParentListByFamily;
        $scope.id = 0;

        if (familyDetails.ID == undefined)
            $state.go('triangular.families');
        vm.columns = {
            ParentName: 'Parent Name',
            EmailId: 'Email',
            Mobile: 'Mobile',
            RelationName: 'Relation',
            ID: 'ID',
            ProfilePic: 'Image'
        };
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FamilyId: familyDetails.ID,
            name: '',
            ParentName: ''
        };
        getSecurityQuestions();
        function getSecurityQuestions() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSecurityQuestions();

            myPromise.then(function (resolve) {
                vm.AllQuestions = null;
                vm.AllQuestions = eval(resolve.Content);
            }, function (reject) {

            });

        };
        vm.ParentForms = [];
        vm.ParentFormsObj = {
            FamilyName: familyDetails.FamilyName,
            FirstName: '',
            LastName: '',
            Mobile: '',
            EmailId: '',
            ImagePath: '',
            SecurityQuestionId: '',
            SecurityQuestionAnswer: '',
            RelationTypeId: 0
        };

        GetParentListByFamily();



        function detailParentInfo(data) {
            // ClassService.SetId(data.ID);
            // $state.go('triangular.Parents', { obj: data });
        };

        $rootScope.$on("GetParentListByFamily", function () {
            GetParentListByFamily();
        });

        function GetParentListByFamily() {
            $scope.ParentName = vm.query.ParentName == "" ? "All" : vm.query.ParentName;
            vm.promise = FamilyService.GetParentListByIdService(vm.query);
            vm.promise.then(function (response) {
                if (response.ContactInfo.Content.length > 0) {
                    localStorage.setItem('response', JSON.stringify(response));
                    vm.NoData = false;
                    vm.ParentList = null;
                    vm.ParentCount = null;
                    vm.ParentList = response.ContactInfo.Content;
                    vm.ParentCount = response.ContactInfo.TotalRows;
                    $scope.selected = [];
                }
                else {
                    if (response.ContactInfo.Content.length == 0) {
                        vm.NoData = true;
                        vm.ParentList = [];
                        vm.ParentCount = 0;
                        notificationService.displaymessage('No Parent list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get parent detail at the moment. Please try again after some time.');
                    }
                }
                vm.close4();
            });
        };


        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function showAddParentDialogue() {
            $mdDialog.show({
                controller: AddParentController,
                controllerAs: 'vm',
                // controller: ParentDetailController,
                // controllerAs: 'vm',
                templateUrl: 'app/familiesDetails/views/pages/addParent.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                items: { ParentList: vm.ParentList },
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function showEditParentDialogue(Parent) {
            $mdDialog.show({
                controller: EditParentController,
                controllerAs: 'vm',
                templateUrl: 'app/familiesDetails/views/pages/addParent.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                items: { Parent: Parent, ParentList: vm.ParentList },
            });
        }
        function showDeleteParentConfirmDialoue(event, Id) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this Parent?')
                .textContent('All of this parent data will be deleted.')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                DeleteFamilyParentById(Id);
            }, function () {
                $scope.hide();
            });
        }
        function DeleteFamilyParentById(model) {
            debugger
            vm.showProgressbar = true;
            vm.promise = FamilyService.DeleteFamilyParent(model);
            vm.promise.then(function (response) {
                if (response.ParentInfo.IsSuccess == true) {
                    debugger
                    notificationService.displaymessage(response.ParentInfo.Message);
                    vm.showProgressbar = false;
                    GetParentListByFamily();
                }
                else {
                    notificationService.displaymessage('Error occured while deleting, Please try again later.');
                }
            });
        }
        function reset4() {
            vm.query.ParentName = '';
            $scope.ParentName = "";
            GetParentListByFamily();
            // GetClassList();
        };
        function addParent(parentForm, model) {
            if (parentForm.$valid) {
                vm.showProgressbar = true;
                vm.ParentFormsObj.FamilyId = JSON.parse(localStorage.getItem('familyDetails')).ID;

                vm.ParentForms.push(vm.ParentFormsObj);
                $http.post(HOST_URL.url + '/api/Family/saveParentInfo', vm.ParentForms).then(function (response) {
                    if (response.data.ContactInfo.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.ContactInfo.Message);
                        cancelClick();
                        GetParentListByFamily();
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
    }

    function EditParentController($scope, imageUploadService, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService,
        $timeout, $mdToast, $rootScope, $q, HOST_URL, items, FamilyDetailsService, CommonService) {
        var vm = this;

        vm.status = 'idle';  // idle | uploading | complete

        var fileList;
        ////////////////
        vm.showProgressbar = false;
        vm.id = items.Parent.ID;
        vm.DisplayText = "Update Parent Details";
        vm.$storage = localStorage;
        vm.updateParent = updateParent;
        $scope.DisplayText = "Update Parent";
        vm.cancelClick = cancelClick;
        vm.query = {
            AgencyId: vm.$storage.agencyId
        };
        getRelationType();
        vm.ImageUrlPath = HOST_URL.url;
        getSMSCarrier();
        vm.ParentForms = [];
        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadImage($files) {
            //imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage = $files;
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image size should not exceed 2MB');
                return;

            }
            var fileExtension = $scope.ProfileImage[0].name;
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {

                uploadStarted();

                $timeout(uploadComplete, 2000);
                imageUploadService.uploadImage($files, vm.UploadSuccess)
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        function uploadStarted() {
            vm.status = 'uploading';
        }
        getSecurityQuestions();
        function getSecurityQuestions() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSecurityQuestions();

            myPromise.then(function (resolve) {
                vm.AllQuestions = null;
                vm.AllQuestions = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image uploaded successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }
        function uploadReset() {
            vm.status = 'idle';
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ParentFormsObj.ImagePath = data.Content;
        }
        vm.ParentFormsObj = items.Parent;
        vm.ParentFormsObj.FamilyName = JSON.parse(localStorage.getItem('familyDetails')).FamilyName;
        vm.ProfilePicComplete = vm.ParentFormsObj.ImagePath;
        vm.ParentFormsObj.SecurityQuestionId = items.Parent.SecurityQuestionId == null ? '' : items.Parent.SecurityQuestionId.toString();
        vm.ParentFormsObj.RelationTypeId = items.Parent.RelationTypeId == null ? '' : items.Parent.RelationTypeId.toString();
        vm.ParentFormsObj.SMSCarrierId = items.Parent.SMSCarrierId == null ? '' : items.Parent.SMSCarrierId.toString();
        vm.ParentFormsObj.SecurityCode = JSON.parse(localStorage.getItem('familyDetails')).SecurityKey;
        vm.ParentFormsObj.IsEventMailReceived = items.Parent.IsEventMailReceived == null ? false : items.Parent.IsEventMailReceived;
        vm.ParentFormsObj.IsParticipantAttendanceMailReceived = items.Parent.IsParticipantAttendanceMailReceived == null ? false : items.Parent.IsParticipantAttendanceMailReceived;
        function getRelationType() {
            vm.promise = CommonService.getRelationType(vm.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllRelationType = null;
                    vm.AllRelationType = eval(response.Content);
                }
            });

        }

        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        }

        vm.DefaultImage = HOST_URL.url + "assets/images/profilepic/images4.png";
        function updateParent(parentForm, model) {
            if (parentForm.$valid) {
                vm.showProgressbar = true;
                vm.ParentFormsObj.FamilyId = JSON.parse(localStorage.getItem('familyDetails')).ID;
                vm.ParentFormsObj.ImagePath = vm.ProfilePic == undefined ? model.ImagePath : vm.ProfilePic;


                vm.AllSMSCarrier.forEach(function (element) {
                    if (element.ID == vm.ParentFormsObj.SMSCarrierId)
                        vm.ParentFormsObj.SMSCarrierEmailAddress = element.CarrierAddress;
                }, this);
                vm.ParentForms.push(vm.ParentFormsObj);
                $http.post(HOST_URL.url + '/api/Family/saveParentInfo', vm.ParentForms).then(function (response) {
                    if (response.data.ContactInfo.IsSuccess == true) {
                        vm.showProgressbar = false;
                        //$state.go('triangular.Familydetails', {}, { reload: true });
                        notificationService.displaymessage(response.data.ContactInfo.Message);
                        cancelClick();
                        $rootScope.$emit("GetParentListByFamily", {});

                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };


    }
    function AddParentController($scope, imageUploadService, $http, $state, $mdDialog, $mdEditDialog, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, FamilyService, items, $filter) {
        var vm = this;
        vm.showProgressbar = false;
        vm.cancelClick = cancelClick;
        vm.addParent = addParent;
        vm.ImageUrlPath = HOST_URL.url;
        vm.status = 'idle';  // idle | uploading | complete
        var familyDetails = JSON.parse(localStorage.getItem('familyDetails'));
        var fileList;
        vm.cancelClick = cancelClick;
        function cancelClick() {
            $mdDialog.cancel();
        }
        ////////////////
        // vm.ProfilePicComplete = "assets/images/avatars/avatar-5.png";        

        vm.id = 0;
        $scope.DisplayText = "Add Parent Details";
        $scope.query = {
            AgencyId: localStorage.agencyId
        };
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FamilyId: familyDetails.ID,
            name: '',
            ParentName: ''
        };
        vm.ParentForms = [];
        vm.ParentFormsObj = {
            FamilyName: familyDetails.FamilyName,
            FirstName: '',
            LastName: familyDetails.FamilyName,
            Mobile: '',
            EmailId: '',
            ImagePath: '',
            IsPrimary: false,
            IsEventMailReceived: false,
            IsParticipantAttendanceMailReceived: false,
            RelationTypeId: 0
        };
        getSecurityQuestions();
        function getSecurityQuestions() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSecurityQuestions();

            myPromise.then(function (resolve) {
                vm.AllQuestions = null;
                vm.AllQuestions = eval(resolve.Content);
            }, function (reject) {

            });

        };
        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadImage($files) {
            //imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image size should not exceed 2MB');
                return;

            }
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {
                uploadStarted();

                $timeout(uploadComplete, 2000);
                imageUploadService.uploadImage($files, vm.UploadSuccess)
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ParentFormsObj.ImagePath = data.Content;
        }
        function uploadStarted() {
            vm.status = 'uploading';
        }
        // vm.selectedRelation = selectedRelation;
        // function selectedRelation(data, ID) {

        //     var length = items.ParentList.length;
        //     if (length >= 1) {

        //         for (var i = 0; i <= length; i++) {
        //             if (items.ParentList[i].RelationTypeId == data) {
        //                 vm.ParentFormsObj.RelationTypeId = '';
        //                 notificationService.displaymessage('Relation is already taken.');
        //             }

        //         }
        //     }
        // }
        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image uploaded successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }
        vm.CheckPrimary = CheckPrimary;
        function CheckPrimary(data) {

            var length = items.ParentList.length;

            if (length >= 1) {

                for (var i = 0; i <= length - 1; i++) {
                    if (items.ParentList[i].IsPrimary == true) {
                        vm.ParentFormsObj.IsPrimary = false;
                        notificationService.displaymessage('A parent is already primary.');
                    }

                }
            }

        }
        getRelationType();
        getSMSCarrier();
        function getRelationType() {
            vm.promise = CommonService.getRelationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllRelationType = null;
                    vm.AllRelationType = eval(response.Content);
                }
            });

        };
        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        };
        // function GetParentListByFamily() {
        //     debugger
        //     vm.promise = FamilyService.GetParentListByIdService(vm.query);
        //     vm.promise.then(function (response) {
        //         if (response.ContactInfo.Content.length > 0) {
        //             debugger
        //             // items.ParentList = response.ContactInfo.Content;
        //             vm.ParentList = null;
        //             vm.ParentCount = null;
        //             vm.ParentList = response.ContactInfo.Content;
        //             $scope.selected = [];
        //         }
        //         else {
        //             if (response.ContactInfo.Content.length == 0) {
        //                 items = [];
        //                 vm.ParentCount = 0;
        //                 notificationService.displaymessage('No Parent detail found.');
        //             }
        //             else {
        //                 notificationService.displaymessage('Unable to get Parent detail at the moment. Please try again after some time.');
        //             }
        //         }
        //     });
        // };
        vm.DefaultImage = HOST_URL.url + "assets/images/profilepic/images4.png";
        function addParent(parentForm, model) {
            if (parentForm.$valid) {
                vm.showProgressbar = true;
                vm.ParentFormsObj.FamilyId = familyDetails.ID;
                vm.ParentFormsObj.ImagePath = vm.ProfilePic == undefined ? vm.DefaultImage : vm.ProfilePic;
                vm.ParentFormsObj.SecurityCode = familyDetails.SecurityKey;
                vm.AllSMSCarrier.forEach(function (element) {
                    if (element.ID == vm.ParentFormsObj.SMSCarrierId)
                        vm.ParentFormsObj.SMSCarrierEmailAddress = element.CarrierAddress;
                }, this);
                //  var OtherData = $filter('filter')(vm.AllSMSCarrier, {ID:vm.ParentFormsObj.SMSCarrierId});
                //  var FilteredData=  $.parseJSON(vm.ParentFormsObj.SMSCarrier);
                // vm.ParentFormsObj.SMSCarrierEmailAddress=FilteredData.CarrierAddress;
                // vm.ParentFormsObj.SMSCarrierId=FilteredData
                //  vm.ParentFormsObj.Mobile=vm.$storage.familyDetails.ContactInfoMap[0].PhoneCode+vm.ParentFormsObj.Mobile;
                vm.ParentForms.push(vm.ParentFormsObj);
                $http.post(HOST_URL.url + '/api/Family/saveParentInfo', vm.ParentForms).then(function (response) {
                    if (response.data.ContactInfo.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.ContactInfo.Message);
                        cancelClick();
                        //GetParentListByFamily();
                        $rootScope.$emit("GetParentListByFamily", {});

                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }


    }

})();