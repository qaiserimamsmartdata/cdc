(function () {
    'use strict';

    angular
        .module('app.family')
        .controller('PaymentDetailController', PaymentDetailController);
    // .controller('AddPaymentController', AddPaymentController)
    // .controller('EditPaymentController', EditPaymentController);

    /* @ngInject */
    function PaymentDetailController($scope, $mdDialog, $state, $http, FamilyService,
        AgencyService, $localStorage, notificationService, CommonService,
        filerService, HOST_URL, $rootScope) {
        var vm = this;
        $scope.$storage = localStorage;
        //Card Expiry Date

        var year = new Date().getFullYear();
        var range = [];
        $scope.mask = true;
        range.push(year);
        for (var i = 1; i < 11; i++) {
            range.push(year + i);
        }
        $scope.years = range;
        // if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        if (JSON.parse(localStorage.getItem('familyDetails')).ID == undefined)
            $state.go('triangular.families');
        vm.FamilyRegister = {};

        vm.FamilyRegister.PaymentInfo = JSON.parse(localStorage.getItem('familyDetails')).PaymentInfo[0];
        if (vm.FamilyRegister.PaymentInfo.CardType == '3') {
            vm.pattern = "9999-999999-99999";
            vm.CVV = "4";
            vm.CVVmax = "4";
            vm.FamilyRegister.PaymentInfo.CardNumber = parseFloat(vm.FamilyRegister.PaymentInfo.CardNumber);
            var cardNumber = (vm.FamilyRegister.PaymentInfo.CardNumber).toString();
            vm.FamilyRegister.PaymentInfo.CardNumberstring = "XXXX-XXXXXX-X-" + cardNumber.substring(11);

        }
        else {
            vm.pattern = "9999-9999-9999-9999";
            vm.CVV = "3";
            vm.CVVmax = "3";
            vm.FamilyRegister.PaymentInfo.CardNumber = parseFloat(vm.FamilyRegister.PaymentInfo.CardNumber);
            var cardNumber = (vm.FamilyRegister.PaymentInfo.CardNumber).toString();
            vm.FamilyRegister.PaymentInfo.CardNumberstring = "XXXX-XXXX-XXXX-" + cardNumber.substring(12);
            // vm.FamilyRegister.PaymentInfo.CardNumber = parseFloat(vm.FamilyRegister.PaymentInfo.CardNumber);
        }
        // if (vm.FamilyRegister.PaymentInfo.CardType == 'American Express') {
        //     vm.FamilyRegister.PaymentInfo.CardNumber = '';
        //     vm.FamilyRegister.PaymentInfo.CardNumberAmericanExpress = vm.FamilyRegister.PaymentInfo.CardNumber;
        // }

        vm.checkStatus = checkStatus;
        function checkStatus() {
            vm.FamilyRegister.PaymentInfo.CardNumber = '';
            vm.FamilyRegister.PaymentInfo.CVV = '';
            if (vm.FamilyRegister.PaymentInfo.CardType != '3') {
                vm.pattern = "9999-9999-9999-9999";
                vm.CVV = "3";
                vm.CVVmax = "3";
                vm.placeholder = "____-____-____-____";

            }
            else {
                vm.pattern = "9999-999999-99999";
                vm.CVV = "4";
                vm.CVVmax = "4";
                vm.placeholder = "____-______-_____";
            }
        }
        vm.show = show;
        vm.hide = hide;
        function show() {
            $scope.mask = false;
        }
        function hide() {
            $scope.mask = true;
        }

        if (vm.FamilyRegister.PaymentInfo != undefined) {
            vm.FamilyRegister.PaymentInfo.CardExpiryMonth = vm.FamilyRegister.PaymentInfo.CardExpiryMonth == undefined ? "" : vm.FamilyRegister.PaymentInfo.CardExpiryMonth.toString();
            vm.FamilyRegister.PaymentInfo.CardExpiryYear = vm.FamilyRegister.PaymentInfo.CardExpiryYear == undefined ? "" : vm.FamilyRegister.PaymentInfo.CardExpiryYear.toString();
        }
        vm.updatePayment = updatePayment;
        function updatePayment(PaymentForm, model) {
            if (PaymentForm.$valid) {
                vm.showProgressbar = true;
                // if (model.CardType == 'American Express')
                //     model.CardNumber = model.CardNumberAmericanExpress;
                // else
                //     model.CardNumber = model.CardNumber;
                $http.post(HOST_URL.url + '/api/Family/savePaymentInfo', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.Message);
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                    }
                });
            }
        }

    }
})();