// (function () {
//     'use strict';
//     angular
//         .module('app.family')
//         .controller('AddParticipantController', AddParticipantController);

//     /* @ngInject */
//     function AddParticipantController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $sessionStorage, notificationService,
//         $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, AgencyService) {
//         var vm = this;

//         vm.addFamily = addFamily;
//         vm.showProgressbar = false;
//         vm.GetAllCountry = GetAllCountry;
//         vm.GetState = GetState;
//         // vm.GetState = GetState;
//         // vm.getCity = getCity;
//         $scope.id = 0;
//         // var bDate = new Date();
//         // bDate = bDate.setDate(bDate.getDate() - 365);
//         // vm.minBirthDate = new Date(bDate);
//         // vm.todayData = new Date();
//         $scope.AttributeNameID = 1;
//         vm.ParticipantForms = [];
//         vm.ParticipantFormsObj = {
//             FirstName: '',
//             LastName: '',
//             GenderId: '',
//             DateOfBirth: '',
//             PhoneNumber: '',
//             Email: '',
//             SchoolName: '',
//             GradeLevelId: '',
//             Transportation: '',
//             Disabilities: '',
//             Allergies: '',
//             Medications: '',
//             PrimaryDoctor: '',
//             AttributeNameID: $scope.AttributeNameID
//         };
//         vm.ParticipantForms.push(vm.ParticipantFormsObj);
//         vm.AddParentForm = AddParentForm;
//         function AddParentForm(data) {
//             if (data == 11) {
//                 notificationService.displaymessage("Maximum 10 participants can be added");
//                 return;
//             }

//             vm.ParticipantForms.push(angular.copy(vm.ParticipantFormsObj));
//             $scope.AttributeNameID = data;
//         }
//         var bDate = new Date();
//         bDate = bDate.setDate(bDate.getDate() - 365);
//         vm.dateOfBirth = new Date(bDate);
//         GetAllCountry();
//         vm.FamilyRegister = {};
//         // vm.FamilyRegister.FamilyInfo = {};
//         // vm.FamilyRegister.ReferenceDetail = {};
//         // vm.FamilyRegister.ParentInfo = {};
//         // vm.FamilyRegister.ContactInfo = {};
//         // vm.FamilyRegister.StudentDetail=vm.ParticipantForms;
//         vm.FamilyRegister.StudentDetail = {};
//         vm.FamilyRegister.StudentDetail.Student = {};
//         function GetAllCountry() {
//             vm.AllCountries = null; // Clear previously loaded state list
//             var myPromise = CommonService.getCountries();

//             myPromise.then(function (resolve) {
//                 vm.AllCountries = null;
//                 vm.AllCountries = resolve;
//                 $scope.CountryTextToShow = "--Select Country--";
//             }, function (reject) {

//             });

//         };
//         function GetState(country) {
//             //Load State
//             vm.States = null;
//             var myPromise = CommonService.getStates(country);
//             myPromise.then(function (resolve) {
//                 vm.States = null;
//                 vm.States = resolve;
//                 $scope.StateTextToShow = "--Select States--";
//             }, function (reject) {

//             });

//         };

//         function addParticipant(studentForm, model) {
//             debugger;
//             if (studentForm.$valid) {
//                  model.FamilyId = $sessionStorage.familyDetails.ID;
//                 $http.post(HOST_URL.url + '/api/Family/saveStudentInfo', model).then(function (response) {
//                     if (response.data.FamilyRegister.IsSuccess == true) {
//                         vm.showProgressbar = false;
//                         notificationService.displaymessage(response.data.FamilyRegister.Message);
//                         // ResetFields();
//                         $state.go('triangular.families');
//                     }
//                     else {
//                         vm.showProgressbar = false;
//                         notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
//                     }
//                 });
//             }
//         }
//         function NotificationMessageController(message) {
//             $timeout(function () {
//                 $rootScope.$broadcast('newMailNotification');
//                 $mdToast.show({
//                     template: '<md-toast><span flex>' + message + '</span></md-toast>',
//                     position: 'bottom right',
//                     hideDelay: 5000
//                 });
//             }, 100);
//         };
//     }

// })();