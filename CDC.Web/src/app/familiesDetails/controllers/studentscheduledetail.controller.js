(function () {
    'use strict';

    angular
        .module('app.family')
        .controller('StudentScheduleDetailController', StudentScheduleDetailController);

    /* @ngInject */
    function StudentScheduleDetailController($scope,HOST_URL, $mdDialog, $state, FamilyService, AgencyService, $localStorage, notificationService, filerService) {
        var vm = this;
        vm.toggleRight1 = filerService.toggleRight1();
        vm.isOpenRight1 = filerService.isOpenRight1();
        vm.close1 = filerService.close1();
        vm.reset = reset;
        vm.ImageUrlPath=HOST_URL.url;
        vm.detailClassInfo = detailClassInfo;
        vm.detailParticipantInfo = detailParticipantInfo;

        if (JSON.parse(localStorage.getItem('familyDetails')).ID == undefined)
            $state.go('triangular.families');
        vm.columns = {
            StudentName: 'Participant',
            Image: 'Image',
            ClassName: 'Class Name',
            // SessionName: 'Session',
            // Session: 'Session',
            StartDate: 'Start Date',
            EndDate: 'End Date',
            // EndDate: 'EndDate',
            // Mon: 'Mon',
            // Tue: 'Tue',
            // Wed: 'Wed',
            // Thu: 'Thu',
            // Fri: 'Fri',
            // Sat: 'Sat',
            //Room: 'Room',
            Status: 'Status',
            ID: 'ID'
        };
        function detailClassInfo(data) {
            // ClassService.SetId(data.ID);
            //$scope.$storage.classId = data.ID;
            localStorage.ClassId = data.ClassId;
            //  $scope.$storage.className = data.ClassName;
            $state.go('triangular.classdetails', { id: data.ClassId });
        };
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-ClassName',
            page: 1,
            StatusId: 0,
            FamilyId: JSON.parse(localStorage.getItem('familyDetails')).ID,
            ParticipantName: '',
            StartDate: null
        };
        function detailParticipantInfo(data) {
            data.RedirectFrom = "enrollList";
            // ClassService.SetId(data.ID);
             if(localStorage.Portal=='AGENCY')
            $state.go('triangular.studentdetails', { obj: data });
            else
            $state.go('triangular.parentstudentdetails', { obj: data });
        };
        vm.GetStudentScheduleList = GetStudentScheduleList;
        GetStudentScheduleList();

        function GetStudentScheduleList() {
            $scope.ParticipantName=$scope.query.ParticipantName==""?"All":$scope.query.ParticipantName;
            $scope.StartDate=$scope.query.StartDate==null || $scope.query.StartDate==""?"All":$scope.query.StartDate;
            $scope.Status=$scope.query.StatusId==0?"All":$scope.query.StatusId==1?"Enrolled":"Not Enrolled"; 
            vm.promise = FamilyService.GetStudentScheduleListByIdService($scope.query);
            vm.promise.then(function (response) {
                if (response.enrollList.Content.length > 0) {
                    vm.NoData = false;
                    vm.ScheduleList = response.enrollList.Content;
                    // vm.StudentList.FirstName ="Parag";
                    vm.ScheduleCount = response.enrollList.TotalRows;
                    $scope.selected = [];

                }
                else {
                    if (response.enrollList.Content.length == 0) {
                        vm.NoData = true;
                        vm.ScheduleList = [];
                        vm.ScheduleCount = 0;
                        notificationService.displaymessage('No Participant enrollment history found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get schedule list at the moment. Please try again after some time.');
                    }
                }
                vm.close1();
            });
        };


        function reset() {
            $scope.query.ParticipantName = '';
            $scope.query.StartDate = $scope.query.StartDate != null ? null : '';
            $scope.query.StatusId = 0;
            GetStudentScheduleList();
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();