(function () {
    'use strict';

    angular
        .module('app.family')
        .controller('FamilyInnerDetailController', FamilyInnerDetailController)
        .controller('AddParticipantController', AddParticipantController)
        .controller('EditParticipantController', EditParticipantController)
        .controller('AddParticipantScheduleController', AddParticipantScheduleController);

    /* @ngInject */
    function FamilyInnerDetailController($rootScope, $scope, filerService, $mdDialog, $state, FamilyService, AgencyService, $localStorage, notificationService, CommonService, HOST_URL) {
        var vm = this;
        $scope.$storage = localStorage;
        // if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }

        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        $scope.StudentName = "";
        vm.detailParticipantInfo = detailParticipantInfo;
        vm.showAddParticipantDialogue = showAddParticipantDialogue;
        vm.showEditParticipantDialogue = showEditParticipantDialogue;
        vm.GetStudentScheduleList = GetStudentScheduleList;
        vm.showDeleteParticipantConfirmDialoue = showDeleteParticipantConfirmDialoue;
        $scope.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        vm.goToSchedule = goToSchedule;
        vm.ImageUrlPath = HOST_URL.url;
        vm.GetStudentListByFamily = GetStudentListByFamily;
        vm.GetEnrolledParticipantsCount = GetEnrolledParticipantsCount;
        GetPricingPlanCount();
        if (JSON.parse(localStorage.getItem('familyDetails')).ID == undefined)
            $state.go('triangular.families');
        vm.columns = {
            Image: 'Image',
            FirstName: 'First Name',
            LastName: 'Last Name',
            MappedClass: 'Enrolled in Class',
            Schedule: 'Enroll',
            Active: 'Active',
            Gender: 'Gender',
            BirthDate: 'Birth Date',
            Age: 'Age',
            Grade: 'Grade',
            EnrolledClasses: 'Enrolled Classes',
            // FEnrolled: 'FEnrolled',
            // Wait: 'Wait',
            ID: 'ID'
        };

        // GetStudentScheduleList();
        function GetStudentScheduleList() {
            vm.promise = FamilyService.GetAllParticipantsByIdService(vm.query);
            vm.promise.then(function (response) {
                if (response.ContactInfo.Content.length > 0) {
                    // vm.NoData = false;
                    vm.StudentList = response.ContactInfo.Content;
                    vm.participantCount = response.ContactInfo.TotalRows;
                    // vm.StudentList.FirstName ="Parag";
                    // vm.FamilyCount = response.FamilyList.TotalRows;
                    $scope.selected = [];

                }
                else {
                    if (response.ContactInfo.Content.length == 0) {
                        //vm.NoData = true;
                        vm.StudentList = [];
                        vm.participantCount = 0;
                        notificationService.displaymessage('No Participant list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get participant list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        };

        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FamilyId: JSON.parse(localStorage.getItem('familyDetails')).ID,
            name: '',
            StudentName: ''
        };
        GetStudentListByFamily();
        GetEnrolledParticipantsCount();

        $rootScope.$on("GetStudentListByFamily", function () {
            GetStudentListByFamily();
        });
        $rootScope.$on("GetEnrolledParticipantsCount", function () {
            GetEnrolledParticipantsCount();
        });
        function reset() {
            vm.query.StudentName = '';
            $scope.StudentName = "";
            GetStudentListByFamily();

        }
        function GetEnrolledParticipantsCount() {
            $scope.query = {
                AgencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getEnrolledParticipantsCount($scope.query);
            vm.promise.then(function (responseForEnrolledParticipants) {
                if (responseForEnrolledParticipants.IsSuccess) {
                    localStorage.TotalEnrolledParticipants = responseForEnrolledParticipants.TotalEnrolledParticipants;
                }
            });
        }
        function GetPricingPlanCount() {
            $scope.query = {
                AgencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getPricingPlanCount($scope.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess) {
                    localStorage.MaxNumberOfParticipants = response.MaximumParticipants;
                }
            });
        }
        function detailParticipantInfo(data) {
            data.RedirectFrom = "StudentList";
            // ClassService.SetId(data.ID);
             if(localStorage.Portal=='AGENCY')
            $state.go('triangular.studentdetails', { obj: data });
            else
            $state.go('triangular.parentstudentdetails', { obj: data });
        };
        function GetStudentListByFamily() {

            $scope.StudentName = vm.query.StudentName == "" ? "All" : vm.query.StudentName;
            vm.promise = FamilyService.GetStudentListByIdService(vm.query);
            vm.promise.then(function (response) {
                if (response.ContactInfo.Content.length > 0) {
                    vm.NoData = false;
                    vm.StudentList = response.ContactInfo.Content;
                    vm.participantCount = response.ContactInfo.TotalRows;
                    $scope.selected = [];
                }
                else {
                    if (response.ContactInfo.Content.length == 0) {
                        vm.NoData = true;
                        vm.StudentList = [];
                        vm.participantCount = 0;
                        notificationService.displaymessage('No Participant list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get participant detail at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        };

        function goToSchedule(participant) {
            // $scope.$storage.studentIdforSchedule = id;
            if (localStorage.TotalEnrolledParticipants == localStorage.MaxNumberOfParticipants) {
                var confirm = $mdDialog.confirm()
                    .title('Sorry,enrolled participants have reached maximum limit.')
                    .textContent('Would you like to upgrade plan?')
                    .ariaLabel('Lucky day')
                    .ok('Please do it.')
                    .cancel('Cancel');

                $mdDialog.show(confirm).then(function () {
                    $state.go('PriceUpdate.pricing');
                }, function () {
                });
            }
            else {
                $mdDialog.show({
                    controller: AddParticipantScheduleController,
                    controllerAs: 'vm',
                    templateUrl: 'app/studentSchedule/schedule/views/addschedule.tmpl.html',
                    parent: angular.element(document.body),
                    escToClose: true,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                    items: { participant: participant }
                });
            }
        }

        $scope.hide = function () {
            $mdDialog.hide();
        };


        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function showAddParticipantDialogue() {
            //   if (localStorage.ParticipantsCount == localStorage.MaxNumberOfParticipants) {
            //         notificationService.displaymessage('Sorry,Maximum participants reached.');
            //         return;
            //     }
            $mdDialog.show({
                controller: AddParticipantController,
                controllerAs: 'vm',
                templateUrl: 'app/familiesDetails/views/addParticipant.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function showEditParticipantDialogue(participant) {
            $mdDialog.show({
                controller: EditParticipantController,
                controllerAs: 'vm',
                templateUrl: 'app/familiesDetails/views/addParticipant.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                items: { studentdetail: participant },
            });
        }
        function showDeleteParticipantConfirmDialoue(event, Id) {

            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this participant?')
                .textContent('All of this participant data will be deleted.')
                .ariaLabel('Lucky day')
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                DeleteFamilyParticipantById(Id);
            }, function () {
                // $scope.hide();
            });
        }
        function DeleteFamilyParticipantById(Id) {


            var model = {
                StudentId: Id
            }
            vm.promise = FamilyService.CheckParticipantClass(model);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    notificationService.displaymessage('Participant cannot be deleted as this participant  is already enrolled with this class .');
                    return;
                }
                else {
                    vm.showProgressbar = true;
                    vm.promise = FamilyService.DeleteFamilyParticipantById(Id);
                    vm.promise.then(function (response) {
                        if (response.StudentInfo.IsSuccess == true) {
                            notificationService.displaymessage('Participant deleted successfully.');
                            vm.showProgressbar = false;
                            GetStudentListByFamily();
                        }
                        else {
                            notificationService.displaymessage('Error occured while deleting, Please try again later.');
                        }
                    });
                }
            })
        }
    }
    function EditParticipantController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService,
        $timeout, $mdToast, $rootScope, $q, HOST_URL, items, FamilyDetailsService, CommonService, imageUploadService) {
        var vm = this;
        var vm = this;
        vm.status = 'idle';  // idle | uploading | complete
        var fileList;
        vm.showProgressbar = false;
        $scope.id = items.studentdetail.ID;
        vm.ProfilePicComplete = items.studentdetail.Student.ImagePath;
        $scope.DisplayText = "Update Participant Details";
        $scope.$storage = localStorage;
        $scope.query = {
            AgencyId: $scope.$storage.agencyId
        };
        vm.ParticipantForms = [];
        vm.ImageUrlPath = HOST_URL.url;
        //vm.ProfilePicComplete = "assets/images/avatars/avatar-5.png";
        // vm.ParticipantFormsObj = {
        //     PhoneNumber: '',
        //     Email: '',
        //     SchoolName: '',
        //     GradeLevelId: '',
        //     Transportation: '',
        //     Disabilities: '',
        //     Allergies: '',
        //     Medications: '',
        //     PrimaryDoctor: '',
        //     AttributeParticipantNameID: $scope.AttributeParticipantNameID
        // };
        // vm.ParticipantFormsObj.Student = {
        //     FirstName: '',
        //     LastName: '',
        //     GenderId: '',
        //     DateOfBirth: '',
        // };
        // vm.Participant = {};
        // vm.Participant.Student = {};
        vm.ParticipantFormsObj = items.studentdetail;
        vm.ParticipantFormsObj.GradeLevelId = items.studentdetail.GradeLevelId == null ? '' : items.studentdetail.GradeLevelId.toString();
        vm.ParticipantFormsObj.Student.DateOfBirth = new Date(vm.ParticipantFormsObj.Student.DateOfBirth);
        vm.ParticipantFormsObj.Student.Gender = vm.ParticipantFormsObj.Student.Gender.toString();
        vm.ParticipantFormsObj.Student.MembershipTypeId = vm.ParticipantFormsObj.Student.MembershipTypeId == undefined ? "" : vm.ParticipantFormsObj.Student.MembershipTypeId.toString();

        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);

        //for min 80years.
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);

        vm.updateParticipant = updateParticipant;
        vm.cancelClick = cancelClick;
        getGradeLevel();
        getMembership();
        function getGradeLevel() {
            vm.promise = CommonService.getGradeLevel($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllGradeLevel = null;
                    vm.AllGradeLevel = eval(response.Content);
                }
            });
        };
        function getMembership() {
            vm.promise = CommonService.getMembership($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllMembership = null;
                    vm.AllMembership = eval(response.Content);
                }
            });
        };

        function uploadStarted() {
            vm.status = 'uploading';
        }

        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image Uploaded Successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }
        function updateParticipant(studentForm, model) {
            if (studentForm.$valid) {
                vm.showProgressbar = true;
                vm.ParticipantForms = [];
                vm.ParticipantFormsObj.Student.ImagePath = vm.ProfilePic == undefined ? model.Student.ImagePath : vm.ProfilePic;
                vm.ParticipantForms.push(model);
                $http.post(HOST_URL.url + '/api/Family/saveStudentInfo', vm.ParticipantForms).then(function (response) {
                    if (response.data.ContactInfo.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.ContactInfo.Message);
                        cancelClick();
                        $rootScope.$emit("GetStudentListByFamily", {});
                        // $state.go('triangular.Familydetails', {}, { reload: true });
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };

        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadImage($files) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image Size should not exceed 2MB');
                return;

            }
            //imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {
                uploadStarted();

                $timeout(uploadComplete, 2000);
                imageUploadService.uploadImage($files, vm.UploadSuccess)
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ParticipantFormsObj.Student.ImagePath = data.Content;
        }

    }
    function AddParticipantController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, imageUploadService) {

        var vm = this;
        vm.showProgressbar = false;
        vm.status = 'idle';  // idle | uploading | complete


        var fileList;
        vm.cancelClick =
            function cancelClick() {
                $mdDialog.cancel();
            }

        vm.cancelClick = cancelClick;
        vm.addParticipant = addParticipant;
        vm.startDate = new Date();
        $scope.$storage = localStorage;
        
        $scope.query = {
            AgencyId: $scope.$storage.agencyId
        };
        $scope.id = 0;
        $scope.DisplayText = "Add Participant Details";
        vm.ImageUrlPath = HOST_URL.url;
        vm.Participant = {};
        vm.Participant.Student = {};
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);

        //for min 80years.
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);

        vm.ParticipantForms = [];
        vm.ParticipantFormsObj = {
            PhoneNumber: '',
            Email: '',
            SchoolName: '',
            GradeLevelId: '',
            Transportation: '',
            Disabilities: '',
            Allergies: '',
            Medications: '',
            PrimaryDoctor: '',
            Description: '',
            AttributeParticipantNameID: $scope.AttributeParticipantNameID
        };
        vm.ParticipantFormsObj.Student = {
            FirstName: '',
            LastName: JSON.parse(localStorage.getItem('familyDetails')).FamilyName,
            Gender: '',
            DateOfBirth: '',
            ImagePath: "assets/images/avatars/avatar-5.png"
        };
        //vm.ProfilePicComplete = "assets/images/avatars/avatar-5.png";
        getGradeLevel();
        getMembership();
        function getGradeLevel() {
            vm.promise = CommonService.getGradeLevel($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllGradeLevel = null;
                    vm.AllGradeLevel = eval(response.Content);
                }
            });
        }
        function getMembership() {
            vm.promise = CommonService.getMembership($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllMembership = null;
                    vm.AllMembership = eval(response.Content);
                }
            })
        }
        vm.DefaultImage = "assets/images/avatars/avatar-5.png";
        function addParticipant(studentForm, model) {
            if (studentForm.$valid) {
                vm.showProgressbar = true;
                vm.ParticipantFormsObj.Student.FamilyId = JSON.parse(localStorage.getItem('familyDetails')).ID;
                vm.ParticipantFormsObj.Student.ImagePath = vm.ProfilePic == undefined ? vm.DefaultImage : vm.ProfilePic;
                vm.ParticipantForms.push(model);
                $http.post(HOST_URL.url + '/api/Family/saveStudentInfo', vm.ParticipantForms).then(function (response) {
                    if (response.data.ContactInfo.IsSuccess == true) {
                        vm.showProgressbar = false;
                        localStorage.ParticipantsCount = localStorage.ParticipantsCount + 1;
                        notificationService.displaymessage(response.data.ContactInfo.Message);
                        cancelClick();
                        $rootScope.$emit("GetStudentListByFamily", {});
                        // $state.go('triangular.Familydetails', {}, { reload: true });
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadImage($files) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image Size should not exceed 2MB');
                return;

            }

            uploadStarted();

            $timeout(uploadComplete, 2000);
            imageUploadService.uploadImage($files, vm.UploadSuccess)
        }

        function uploadStarted() {
            vm.status = 'uploading';
        }

        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image Uploaded Successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ParticipantFormsObj.Student.ImagePath = data.Content;
        }
    }
    function AddParticipantScheduleController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, items, ClassService) {
        var vm = this;
        vm.showProgressbar = false;
        vm.cancelClick = cancelClick;
        vm.addSchedule = addSchedule;
        $scope.$storage = localStorage;
        vm.scheduleData = {};
        $scope.id = 0;
        $scope.ParticipantName = items.participant.Student.FirstName + ' ' + items.participant.Student.LastName;
        $scope.DisplayText = "Add Schedule";
        vm.Schedule = {};
        vm.Schedule.Student = {};
        var bDate = new Date();
        vm.startDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);

        //for min 80years.
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);

        vm.getFeesAmount = getFeesAmount;
        vm.Discount = Discount;
        function Discount(data) {
            if (data > vm.scheduleData.TutionFees) {
                vm.scheduleData.PaidFees = "";
                notificationService.displaymessage("Discount cannot be more than Fee amount.")
            }
        }
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList($scope.$storage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }

        function GetClassById() {
            var model = {
                ClassId: vm.scheduleData.ClassId,
                TimeZone: localStorage.TimeZone
            }
            vm.promise = ClassService.GetClassListByIdService(model);
            vm.promise.then(function (response) {
                debugger
                if (response.classList.ID > 0) {
                    if (response.classList.OnGoing == null || response.classList.OnGoing == 0) {
                        $scope.ClassStartDate = new Date(response.classList.ClassStartDate);
                        $scope.EndDate = new Date(response.classList.ClassEndDate);
                    }
                }
            });
        }

        function getFeesAmount(data) {
            //Set min schedule date 
            $scope.ClassStartDate = "";
            $scope.EndDate = "";
            GetClassById();

            vm.AllClasses.forEach(function (element) {
                if (vm.scheduleData.ClassId == element.ID) {
                    vm.scheduleData.TutionFees = element.Fees;
                    vm.scheduleData.PaidFees = element.Fees;
                    vm.FeesType = element.Name;
                }
            }, this);

            var model = {
                "ClassId": data,
                "AgencyId": localStorage.agencyId
            }
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/User/EnrolledCapacity', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
                if (response == true) {
                    vm.scheduleData.ClassId = null;
                    notificationService.displaymessage('Class has reached its maximum capacity.');

                }
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

            //  var model={
            //     ClassId:data,
            //     AgencyId:localStorage.agencyId
            // }
            //  $http.post(HOST_URL.url + '/api/User/EnrolledCapacity', model).then(function (response) {
            //        if (response.data.IsSuccess == true) {
            //             if (response == true) {
            //                vm.scheduleData.ClassId="";
            //                 notificationService.displaymessage(response.data.ReturnMessage[0]);
            //             }
            //              else {
            //                return;
            //             }
            //             // $state.go('triangular.studentschedule', {}, { reload: true });

            //        }
            //  });

        }
        function addSchedule(scheduleform, model) {
            debugger
            if (scheduleform.$valid) {
                vm.showProgressbar = true;
                // vm.Schedule.Student.FamilyId = localStorage.familyDetails.ID;
                // model.StudentId = items.participant.ID;
                model.StudentId = items.participant.ID;
                $http.post(HOST_URL.url + '/api/StudentSchedule/AddSchedule', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        if (response.data.IsExist == true) {
                            vm.showProgressbar = false;
                            notificationService.displaymessage(response.data.ReturnMessage[0]);
                        } else {
                            if (response.data.IsEligible == false) {
                                vm.showProgressbar = false;
                                vm.scheduleData.StartDate = "";
                                notificationService.displaymessage(response.data.ReturnMessage[0]);
                            }
                            else {
                                notificationService.displaymessage(response.data.ReturnMessage[0]);
                                cancelClick();
                                $rootScope.$emit("GetStudentListByFamily", {});
                                $rootScope.$emit("GetEnrolledParticipantsCount", {});
                            }
                            // $state.go('triangular.Familydetails', {}, { reload: true });
                        }
                        // $state.go('triangular.studentschedule', {}, { reload: true });
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
    }

})();