(function () {
    'use strict';

    angular
        .module('app.family')
        .controller('FamilyDetailController', FamilyDetailController);

    /* @ngInject */
    function FamilyDetailController($scope, notificationService, $mdDialog, $state, $localStorage) {
        var vm = this;
        $scope.$storage = localStorage;
        //    if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) 
        //     {
        //         $state.go('authentication.login');
        //         notificationService.displaymessage("You must login first.");
        //         return;
        //     } 
        vm.redirectToFamily = redirectToFamily;
        function redirectToFamily() {
            if (localStorage.Portal == 'AGENCY')
                $state.go('triangular.families');
            else
                $state.go('triangular.parentfamilies');

        }
        if (JSON.parse(localStorage.getItem('familyDetails')).ID == undefined) {
            if (localStorage.Portal == 'AGENCY')
                $state.go('triangular.families');
            else
                $state.go('triangular.parentfamilies');
        }
        else
            vm.familyname = JSON.parse(localStorage.getItem('familyDetails')).FamilyName;
    }

})();