(function () {
    'use strict';

    angular
        .module('app.family')
        .controller('familydetailupdate', familydetailupdate);

    /* @ngInject */
    function familydetailupdate($scope, $timeout,$mdToast,FamilyService, $localStorage, notificationService, CommonService, HOST_URL, $http,imageUploadService) {
        var vm = this;
          $scope.$storage = localStorage;
    //    if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) 
    //     {
    //         $state.go('authentication.login');
    //         notificationService.displaymessage("You must login first.");
    //         return;
    //     }
    var familyDetails=JSON.parse(localStorage.getItem('familyDetails'));
         vm.status = 'idle';  // idle | uploading | complete
           var fileList;
        vm.GetFamilyDatailInfo = GetFamilyDatailInfo;
        vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;
        vm.updateParent = updateParent;
        vm.ImageUrlPath = HOST_URL.url;
        vm.updateContactDatails = updateContactDatails;
        vm.ParentForms = [];
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FamilyId: familyDetails.ID,
            name: '',
            StudentName: '',
            AgencyId: localStorage.agencyId
        };
        GetAllCountry();
        getInfoSource();
        getMembership();
        getRelationType();
        GetFamilyDatailInfo();
         vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadImage($files) {
           
            //imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
               var size = $files[0].size;
            if(size > 2097152)
            {
             notificationService.displaymessage('Image size should not exceed 2MB');
             return;

            }
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {

                imageUploadService.uploadImage($files, vm.UploadSuccess)
                    uploadStarted();

                $timeout(uploadComplete, 2000);
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.FamilyRegister.ParentInfo.ImagePath =  data.Content;
        }
           function uploadStarted() {
            vm.status = 'uploading';
        }

        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image uploaded successfully';
            for(var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }
    
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            var myPromise = CommonService.getCountries();

            myPromise.then(function (resolve) {
                vm.AllCountries = null;
                vm.AllCountries = resolve;
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            myPromise.then(function (resolve) {
                vm.States = null;
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        function getInfoSource() {
            vm.promise = CommonService.getInfoSource(vm.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllInfoSource = null;
                    vm.AllInfoSource = eval(response.Content);
                }
            });

        };
        function getMembership() {
            vm.promise = CommonService.getMembership(vm.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllMembership = null;
                    vm.AllMembership = eval(response.Content);
                }
            });

        };
        function getRelationType() {
            vm.promise = CommonService.getRelationType(vm.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllRelationType = null;
                    vm.AllRelationType = eval(response.Content);
                }
            });

        };
        vm.FamilyRegister = {};
        vm.FamilyRegister.FamilyInfo = {};
        vm.FamilyRegister.ReferenceDetail = {};
        vm.FamilyRegister.ParentInfo = [];
        //vm.FamilyRegister.ParentInfo.push(vm.ParentFormsObj);

        vm.FamilyRegister.ContactInfo = {};
        function GetFamilyDatailInfo() {
            vm.showProgressbarForDisplay = true;
            vm.promise = FamilyService.GetFamilyListService(vm.query);
            vm.promise.then(function (response) {
                if (response.FamilyRegister.IsSuccess == true) {
                    
                    vm.FamilyRegister = response.FamilyRegister.Content[0];
                    vm.FamilyRegister.SecurityKey=parseFloat(vm.FamilyRegister.SecurityKey);
                    vm.FamilyRegister.ParentInfo = response.FamilyRegister.Content[0].ParentInfo[0];
                //    vm.FamilyRegister.ParentInfo.ImagePath=vm.FamilyRegister.ParentInfo[0].ImagePath==undefined?'':vm.FamilyRegister.ParentInfo[0].ImagePath;
                    vm.FamilyRegister.ParentInfo.RelationTypeId =  vm.FamilyRegister.ParentInfo.RelationTypeId.toString();
                    vm.FamilyRegister.ContactInfo = response.FamilyRegister.Content[0].ContactInfoMap[0].Contact;
                    vm.FamilyRegister.ContactInfo.CountryId = vm.FamilyRegister.ContactInfo.CountryId.toString();
                    vm.FamilyRegister.ContactInfo.StateId = vm.FamilyRegister.ContactInfo.StateId.toString();
                    vm.FamilyRegister.ContactInfo.MembershipTypeId = vm.FamilyRegister.ContactInfo.MembershipTypeId==undefined?"":vm.FamilyRegister.ContactInfo.MembershipTypeId.toString();
                    vm.FamilyRegister.ContactInfo.Mobile=parseFloat(vm.FamilyRegister.ContactInfo.Mobile);
                    vm.FamilyRegister.ContactInfo.RelationshipToParticipantId=vm.FamilyRegister.ContactInfo.RelationshipToParticipantId.toString();
                    GetState(vm.FamilyRegister.ContactInfo.CountryId);
                    vm.showProgressbarForDisplay = false;
                    $scope.selected = [];
                }
                else {
                    vm.showProgressbarForDisplay = false;
                    notificationService.displaymessage('Unable to get family detail at the moment. Please try again after some time.');
                }
            });
        };
        function updateParent(parentForm, model) {
            if (parentForm.$valid) {
                vm.showProgressbar = true;
                vm.FamilyRegister.ParentInfo.FamilyId = familyDetails.ID;
                vm.ParentForms.push(vm.FamilyRegister.ParentInfo);
                $http.post(HOST_URL.url + '/api/Family/saveParentInfo', vm.ParentForms).then(function (parentresponse) {
                    if (parentresponse.data.ContactInfo.IsSuccess == true) {
                        var FamilyInfoModel={};
                        FamilyInfoModel.FamilyInfo=vm.FamilyRegister;
                        $http.post(HOST_URL.url + '/api/Family/updateFamilyName', FamilyInfoModel).then(function (response) {
                            if (response.data.IsSuccess == true) {
                                vm.showProgressbar = false;
                                   vm.ParentForms=[];
                                notificationService.displaymessage(parentresponse.data.ContactInfo.Message);
                            }
                        });                      
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function updateContactDatails(contactForm, model) {
            if (contactForm.$valid) {
                vm.showProgressbarForContact = true;
                var newmodel = {};
                newmodel.ContactInfo = model.ContactInfo;
                $http.post(HOST_URL.url + '/api/Family/addContact', newmodel).then(function (response) {
                    if (response.data.ContactInfo.IsSuccess == true) {
                        vm.showProgressbarForContact = false;
                        notificationService.displaymessage(response.data.ContactInfo.Message);
                    }
                    else {
                        vm.showProgressbarForContact = false;
                        notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                    }
                });
            }
        }
    }

})();