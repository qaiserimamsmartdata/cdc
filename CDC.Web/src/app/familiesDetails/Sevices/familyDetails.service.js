﻿(function () {
    'use strict';

    angular
        .module('app.family')
        .factory('FamilyDetailsService', FamilyDetailsService);

    FamilyDetailsService.$inject = ['$http', '$q', 'HOST_URL', '$rootScope'];

    /* @ngInject */
    function FamilyDetailsService($http, $q, HOST_URL, $rootScope) {
        return {
            GetParticipantStudentByIdService: GetParticipantStudentByIdService,
            DeleteFamilyParticipantById: DeleteFamilyParticipantById
        };

        function GetParticipantStudentByIdService(ParticipantId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/GetFamilyStudentById?StudentId=' + ParticipantId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function DeleteFamilyParticipantById(ParticipantId) {
            var deferred = $q.defer();
            var model={ID:ParticipantId};
            
            $http.post(HOST_URL.url + '/api/Family/deleteStudentInfo',model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            
        };

    }
})();