(function () {
    'use strict';

    angular
        .module('app.messages')
        .config(moduleConfig)
        .constant('EMAIL_ROUTES', [{
            state: 'triangular.email.inbox',
            name: 'Inbox',
            url: '/email/inbox'
        }
            //     state: 'triangular.email.trash',
            //     name: 'Trash',
            //     url: '/email/trash'
            , {
                state: 'triangular.email.sent',
                name: 'Sent',
                url: '/email/sent'
            }]);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider, EMAIL_ROUTES) {

        $stateProvider
            .state('triangular.email', {
                abstract: true,
                views: {
                    'toolbar@triangular': {
                        templateUrl: 'app/messages/layout/toolbar/toolbar.tmpl.html',
                        controller: 'EmailToolbarController',
                        controllerAs: 'vm'
                        // templateUrl: 'app/layouts/toolbar/toolbar.tmpl.html',
                        // controller: 'ToolbarController',
                        // controllerAs: 'vm'
                    }
                },
                data: {
                    layout: {
                        footer: false,
                        contentClass: 'triangular-non-scrolling'
                    },
                    permissions: {
                        only: ['viewmessages']
                    }
                }
            });

        angular.forEach(EMAIL_ROUTES, function (route) {

            if (route.name == 'Inbox') {
                $stateProvider
                    .state(route.state, {
                        url: route.url,
                        views: {
                            '@triangular': {
                                templateUrl: 'app/messages/inbox.tmpl.html',
                                controller: 'InboxController',
                                controllerAs: 'vm'
                            }
                        },
                        resolve: {
                            emails: function ($http, $q, $localStorage, HOST_URL) {
                                var model = {
                                    UserId: localStorage.UserId,
                                    RoleId: localStorage.RoleId,
                                    TimeZone: localStorage.TimeZone
                                }
                                return $http({

                                    method: 'POST',
                                    url: HOST_URL.url + 'api/Email/Inbox',
                                    data: model
                                });

                            },
                            contacts: function ($http, HOST_URL, $localStorage) {
                                var model = {
                                    UserId: localStorage.UserId,
                                    RoleId: localStorage.RoleId,
                                    AgencyId: localStorage.agencyId
                                }
                                return $http({
                                    method: 'POST',
                                    url: HOST_URL.url + 'api/Email/Contacts',
                                    data: model
                                });
                            }
                        }
                    });
            }

            if (route.name == 'Sent') {
                $stateProvider
                    .state(route.state, {
                        url: route.url,
                        views: {
                            '@triangular': {
                                templateUrl: 'app/messages/sent.tmpl.html',
                                controller: 'SentController',
                                controllerAs: 'vm'
                            }
                        },
                        resolve: {
                            emails: function ($http, $q, $localStorage, HOST_URL) {
                                var model = {
                                    UserId: localStorage.UserId,
                                    RoleId: localStorage.RoleId,
                                    TimeZone: localStorage.TimeZone
                                }
                                return $http({

                                    method: 'POST',
                                    url: HOST_URL.url + 'api/Email/Sent',
                                    data: model
                                });

                            },
                            contacts: function ($http, HOST_URL, $localStorage) {
                                var model = {
                                    UserId: localStorage.UserId,
                                    RoleId: localStorage.RoleId,
                                    AgencyId: localStorage.agencyId
                                }
                                return $http({
                                    method: 'POST',
                                    url: HOST_URL.url + 'api/Email/Contacts',
                                    data: model
                                });
                            }
                        }
                    });
            }

        });

        angular.forEach(EMAIL_ROUTES, function (route) {
            if (route.name == 'Inbox') {
                $stateProvider
                    .state(route.state + '.email', {
                        url: '/mail/:emailID',
                        templateUrl: 'app/messages/email.tmpl.html',
                        controller: 'EmailController',
                        controllerAs: 'vm',
                        resolve: {
                            email: function ($stateParams, emails) {

                                emails = emails.data;
                                var foundEmail = false;
                                for (var i = 0; i < emails.length; i++) {
                                    if (emails[i].id === $stateParams.emailID) {
                                        foundEmail = emails[i];
                                        break;
                                    }
                                }
                                // var unreadLength = emails.data.length;

                                // for (var i = 0; i < unreadLength; i++) {
                                //     badgecount = badgecount + 1;
                                // }

                                return foundEmail;



                            }
                        },
                        onEnter: function ($state, email) {
                            if (false === email) {
                                $state.go(route.state);
                            }
                        }
                    });
            }

            if (route.name == 'Sent') {
                $stateProvider
                    .state(route.state + '.email', {
                        url: '/mail/:emailID',
                        templateUrl: 'app/messages/emailsent.tmpl.html',
                        controller: 'EmailController',
                        controllerAs: 'vm',
                        resolve: {
                            email: function ($stateParams, emails) {

                                emails = emails.data;
                                var foundEmail = false;
                                for (var i = 0; i < emails.length; i++) {
                                    if (emails[i].id === $stateParams.emailID) {
                                        foundEmail = emails[i];
                                        break;
                                    }
                                }
                                // var unreadLength = emails.data.length;

                                // for (var i = 0; i < unreadLength; i++) {
                                //     badgecount = badgecount + 1;
                                // }

                                return foundEmail;



                            }
                        },
                        onEnter: function ($state, email) {
                            if (false === email) {
                                $state.go(route.state);
                            }
                        }
                    });
            }

        });

        window.emailMenu = {
            name: 'Messages',
            icon: 'zmdi zmdi-email',
            type: 'dropdown',
            priority: 2,
            permission: 'viewmessages',
            children: []
        };

        angular.forEach(EMAIL_ROUTES, function (route) {

            if (route.name == "Inbox") {
                emailMenu.children.push({
                    name: route.name,
                    state: route.state,
                    type: 'link',
                    badge: 0
                });
            }

            if (route.name == "Sent") {

                emailMenu.children.push({
                    name: route.name,
                    state: route.state,
                    type: 'link'


                });
            }


        });

        triMenuProvider.addMenu(emailMenu);

        triMenuProvider.addMenu({
            type: 'divider',
            priority: 2.3
        });
    }
})();
