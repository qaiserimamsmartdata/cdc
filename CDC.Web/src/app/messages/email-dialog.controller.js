(function () {
    'use strict';

    angular
        .module('app.messages')
        .controller('EmailDialogController', EmailDialogController);

    /* @ngInject */
    function EmailDialogController($timeout, $state, $scope, $localStorage, $q, $http, HOST_URL, $mdDialog, $filter, triSkins, textAngularManager, title, email, contacts, getFocus) {
        var contactsData = contacts.data;
        var vm = this;
        vm.cancel = cancel;
        vm.email = email;
        if(title=="Forward")
        {
            vm.email.to=[];
        }
        vm.title = title;
        vm.send = send;
        vm.showCCSIcon = 'zmdi zmdi-account-add';
        vm.showCCS = false;
        vm.toggleCCS = toggleCCS;
        vm.triSkin = triSkins.getCurrent();
        vm.queryContacts = queryContacts;

        ///////////////

        function cancel() {
            $mdDialog.cancel();
        }

        function toggleCCS() {
            vm.showCCS = !vm.showCCS;
            vm.showCCSIcon = vm.showCCS ? 'zmdi zmdi-account' : 'zmdi zmdi-account-add';
        }

        function send(data) {

            data.RoleId = localStorage.RoleId;
            data.UserId = localStorage.UserId;
            data.Host = HOST_URL.url;


            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Email/ComposeMessage', data).success(function (response, status, headers, config) {
                $state.go('triangular.email.inbox', {}, { reload: true });
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            $mdDialog.hide(vm.email);
            return deferred.promise;

        }

        function queryContacts($query) {
            var lowercaseQuery = angular.lowercase($query);
            return contactsData.filter(function (contact) {
                var lowercaseName = angular.lowercase(contact.name);
                if (lowercaseName.indexOf(lowercaseQuery) !== -1) {
                    return contact;
                }
            });
        }

        ////////////////
        if (getFocus) {
            $timeout(function () {
                // Retrieve the scope and trigger focus
                var editorScope = textAngularManager.retrieveEditor('emailBody').scope;
                editorScope.displayElements.text.trigger('focus');
            }, 500);
        }
    }
})();