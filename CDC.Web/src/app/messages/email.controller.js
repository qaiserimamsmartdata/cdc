(function () {
    'use strict';

    angular
        .module('app.messages')
        .controller('EmailController', EmailController);

    /* @ngInject */
    function EmailController($scope, apiService,$q, $http, HOST_URL, $localStorage, $stateParams, $mdDialog, $mdToast, $filter, emails, email, contacts) {
        var vm = this;
        vm.closeEmail = closeEmail;
        vm.deleteEmail = deleteEmail;
        vm.email = email;
        vm.emailAction = emailAction;

        // vm.Failed = Failed;
        // vm.GetAgencyByFamilyId = GetAgencyByFamilyId;
        // GetAgencyByFamilyId();
        // function GetAgencyByFamilyId() {

        //     var model = { FamilyId: localStorage.FamilyId };
        //     apiService.post('/api/Email/GetAgencyByFamilyId', model, getSucess, Failed)
        // }
        // function getSucess(result) {
        //     alert('hi');
        //     vm.agencyData = result.data.Content;
        //     console.log(vm.agencyData);
        // }
        // function Failed(result) {
        //     //notificationService.displaymessage('Please try again after some time.');
        // }
        // GetData();
        // function GetData() {
         
        //     var model = { FamilyId: localStorage.FamilyId };
        //     var deferred = $q.defer();
        //     $http.post(HOST_URL.url + 'api/Email/GetAgencyByFamilyId', model).success(function (response, status, headers, config) {
        //         alert(response);
        //         deferred.resolve(response);
        //     }).error(function (errResp) {
        //         deferred.reject({ message: "Really bad" });
        //     });
        // }

        /////////////////

        function closeEmail() {
            $scope.$emit('closeEmail');
        }

        function deleteEmail(email) {
            $scope.$emit('deleteEmail', email);
        }

        function emailAction($event, title) {
            var replyEmail = {
                to: [],
                cc: [],
                bcc: [],
                // add r.e to subject if there is one
                subject: email.subject === '' ? '' : $filter('triTranslate')('Re: ') + email.subject,
                // wrap previous content in blockquote and add new line
                content: '<br><br><blockquote>' + email.content + '</blockquote>'
            };

            // get contact and add it to to if replying
              angular.forEach(contacts.data, function(contact) {
                  //replyEmail.to.push(contact);
                if(contact.email === email.from.email) {
                    replyEmail.to.push(contact);
                }
            });

            openEmail($event, replyEmail, $filter('triTranslate')(title));
        }

        function openEmail($event, email, title) {
            $mdDialog.show({
                controller: 'EmailDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/messages/email-dialog.tmpl.html',
                targetEvent: $event,
                locals: {
                    title: title,
                    email: email,
                    contacts: contacts,
                    getFocus: true
                },
                focusOnOpen: false
            })
                .then(function (email) {
                    // send email sent event
                    $scope.$emit('sendEmail', email);
                }, cancelEmail);

            function cancelEmail() {
                $mdToast.show(
                    $mdToast.simple()
                        .content($filter('triTranslate')('Message has been cancelled.'))
                        .position('bottom right')
                        .hideDelay(3000)
                );
            }
        }
    }
})();
