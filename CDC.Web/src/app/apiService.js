(function () {
    'use strict';

    angular
        .module('app')
        .factory('apiService', apiService);

    apiService.$inject = ['$http', '$location', 'notificationService', '$rootScope', '$q', 'HOST_URL', '$localStorage', '$cookies','$window'];

    function apiService($http, $location, $rootScope, notificationService, $templateRequest, HOST_URL, $q, $localStorage, $state, $cookies,$window) {
        var service = {
            get: get,
            post: post,
            checkLoginOnce: checkLoginOnce
        };
        //$rootScope.HOST_URL = { 'url': 'http://localhost:51824/' };
        function checkLoginOnce($q, $http, $state, $location,$localStorage, PortalName,$window) {
            var deferred = $q.defer();
            if (localStorage.Portal != undefined) {
                // if (PortalName == "AGENCY") {
                    if ((PortalName == "AGENCY" && localStorage.Portal=="AGENCY" && localStorage.agencyId != undefined) 
                    || (PortalName == "STAFF" && localStorage.Portal == "STAFF" && localStorage.staffId != undefined)
                     || (PortalName == "SuperAdmin" &&localStorage.Portal == "SuperAdmin" && localStorage.SuperAdminId != undefined)
                     || (PortalName == "FAMILY" &&localStorage.Portal == "FAMILY" && localStorage.FamilyId != undefined)
                     ||(PortalName == "STAFF" && localStorage.Portal=="STAFF" &&  localStorage.staffId != undefined))
                    {
                        deferred.resolve();
                        return deferred.promise;
                    }
                // }
                else if (localStorage.Portal == "AGENCY") {
                    $window.location.href='/dashboard';
                    //deferred.reject();
                }
                else if (localStorage.Portal == "STAFF") {
                    $window.location.href='/staffdashboard';
                    //deferred.reject();
                }
                else if (localStorage.Portal == "SuperAdmin") {
                    $window.location.href='/superadmindashboard';
                    //deferred.reject();
                }
                else if (localStorage.Portal == "FAMILY") {
                   $window.location.href='/parent/dashboard';
                }

            } else {
                if (PortalName == "LOGIN")
                    deferred.resolve();
                else
                    $location.url('/login');
                // deferred.reject();               
            }
            return deferred.promise;
        };
        function get(resturl, config, success, failure) {
            resturl = HOST_URL.url + resturl;
            return $http.get(resturl, config)
                .then(function (result) {
                    success(result);
                }, function (error) {
                    if (error.status == '401') {
                        notificationService.displaymessage('Authentication required.');
                        $rootScope.previousState = $location.path();
                        $location.path('/login');
                    }
                    else if (failure != null) {
                        failure(error);
                    }
                });
        }

        function post(resturl, data, success, failure) {
            resturl = HOST_URL.url + resturl;
            return $http.post(resturl, data)
                .then(function (result) {
                    success(result);
                }, function (error) {
                    if (error.status == '401') {
                        notificationService.displaymessage('Authentication required.');
                        $rootScope.previousState = $location.path();
                        $location.path('/login');
                    }
                    else if (failure != null) {
                        failure(error);
                    }
                });
        }
        return service;
    }
})();