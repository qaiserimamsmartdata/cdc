(function() {
    'use strict';

    angular
        .module('superadmindashboard')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
        .state('triangular.superadmindashboard', {
            url: '/superadmindashboard',
            templateUrl: 'app/superadmindashboard/superadmindashboard.tmpl.html',
            // set the controller to load for this page
            controller: 'SuperAdminDashBoardController',
            controllerAs: 'vm',
           
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    //contentClass: 'layout-column'
                    //contentClass: 'overlay-10'
                }
            },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"SuperAdmin",$window);
                    }
                }
        });

        triMenuProvider.addMenu({
            name: 'Dashboard',
            icon: 'fa fa-tachometer',
            type: 'link',
            permission: 'viewSuperAdminDashBoard',
            state: 'triangular.superadmindashboard',
            priority: 1
            // children: [{
            //     name: 'Start Page',
            //     state: 'triangular.seed-page',
            //     type: 'link'
            // }]
        });
    }
})();
