(function () {
    'use strict';

    angular
        .module('superadmindashboard')
        .controller('SuperAdminDashBoardController', SuperAdminDashBoardController)
        .controller('ShowAgencySummaryController', ShowAgencySummaryController);


    /* @ngInject */
    function SuperAdminDashBoardController($scope, $timeout, $mdToast,notificationService, $rootScope, $state, $interval, CommonService, $localStorage, $q, HOST_URL, $mdDialog) {
        var vm = this;
        GetAllCount();
        vm.currentUser = localStorage.displayName;
        vm.TotalAgencyCount = 0;
        vm.PricingPlanAmountSum = 0;
        vm.TimeClockUsersPlanAmountSum = 0;
        vm.PricingPlanCount = 0;
        vm.TimeClockUserPlanCount = 0;
        vm.TotalSum = 0;
        vm.redirectToAgency = redirectToAgency;
        localStorage.ParticipantAttendancePage = false;
        vm.redirectToPricingPlan = redirectToPricingPlan;
        vm.redirectToTimeClockUserPlan = redirectToTimeClockUserPlan;
        vm.agencyListLimit = 3;
        vm.buttonText = 1;
        vm.moreClick = moreClick;
          vm.lessClick = lessClick;
        function moreClick() {
            vm.agencyListLimit = vm.AgencyList.length
            vm.buttonText = 0;
        }
        
         function lessClick() {
            vm.agencyListLimit = 3
            vm.buttonText = 1;
        }
        function redirectToAgency() {
            $state.go("triangular.superadmin");
        }

        function redirectToPricingPlan() {
             $state.go("triangular.participantsubscription");
         }
         function redirectToTimeClockUserPlan() {
             $state.go("triangular.timeclocksubscription");
         }
        //      if (localStorage.SuperAdminId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        // vm.TotalAgencyTimeClockUsersPlans = 0;
        // vm.TotalAgencyPlans = 0;
        function GetAllCount() {
            var myPromise = CommonService.getSuperAdminDashboardAllData(null);
            myPromise.then(function (resolve) {
                if (resolve.IsSuccess == true) {
                    vm.PricingPlanAmountSum = resolve.Content.PricingPlanAmountSum;
                    vm.TimeClockUsersPlanAmountSum = resolve.Content.TimeClockUsersPlanAmountSum;
                    vm.PricingPlanCount = resolve.Content.PricingPlanCount;
                    vm.TimeClockUserPlanCount = resolve.Content.TimeClockUserPlanCount;
                    vm.TotalAgencyCount = resolve.Content.TotalAgencyCount;
                    vm.AgencyList = resolve.Content.AgencyList;
                    vm.TotalSum = vm.TimeClockUsersPlanAmountSum + vm.PricingPlanAmountSum;
                    vm.TotalAgencyPlans = resolve.Content.AgencyPricingPlansSum;
                    vm.TotalAgencyTimeClockUsersPlans = resolve.Content.AgencyTimeClockUsersPlansSum;
                    assignPricngPlanDataToPieChart();
                    assignTimeClockUsersPlanDataToPieChart();
                }

            }, function (reject) {

            });

        };
        vm.PricingPlanlabels = ['Organization', 'Plans'];
        vm.Timeclockuserslabels = ['Organization', 'Plans'];

        vm.dataPricingPlan = [];
        vm.dataTimeclockusers = [];
        function assignPricngPlanDataToPieChart() {
            if (vm.TotalAgencyCount > 0) {
                vm.dataPricingPlan.push(vm.TotalAgencyCount);
                vm.dataPricingPlan.push(vm.TotalAgencyPlans);
            }

        }
        function assignTimeClockUsersPlanDataToPieChart() {
            if (vm.TotalAgencyCount > 0) {
                vm.dataTimeclockusers.push(vm.TotalAgencyCount);
                vm.dataTimeclockusers.push(vm.TotalAgencyTimeClockUsersPlans);
            }
        }
        vm.showAddParticipantDialogue = showAddParticipantDialogue
        function showAddParticipantDialogue(id) {
            //   if (localStorage.ParticipantsCount == localStorage.MaxNumberOfParticipants) {
            //         notificationService.displaymessage('Sorry,Maximum participants reached.');
            //         return;
            //     }
            $mdDialog.show({
                controller: ShowAgencySummaryController,
                controllerAs: 'vm',
                templateUrl: 'app/superadmindashboard/agencySummary.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                AgencyData: { id: id } // Only for -xs, -sm breakpoints.
            });
        };


        // $interval(assignPricngPlanDataToPieChart, 5000);
        // $interval(assignTimeClockUsersPlanDataToPieChart, 5000);
    }
    function ShowAgencySummaryController($filter, $scope, notificationService, $http, $state, $stateParams, $localStorage, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService, AgencyService, AgencyData) {
        var vm = this;


        // vm.  = cancelUpdate;

        // function cancelUpdate() {
        //     $state.go("triangular.families");
        // }
        vm.displaytext = "";
        GetAllCountry();
        vm.cancelClick=cancelClick;
         function cancelClick() {
            $mdDialog.cancel();
        }
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            // $scope.CountryTextToShow = "Please Wait..."; // this will show until load states from database
            var myPromise = CommonService.getCountries();

            myPromise.then(function (resolve) {
                vm.AllCountries = resolve;
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        vm.agency = {};
        GetAgencyList();
        function GetAgencyList() {

            vm.promise = AgencyService.GetAgencyListByIdService(AgencyData.id);
            vm.promise.then(function (response) {
                if (response.agencyList.ID > 0) {
                    vm.agency.agencyname = response.agencyList.AgencyName;
                    vm.displaytext = vm.agency.agencyname;
                    vm.agency.ContactPersonFirstName = response.agencyList.ContactPersonFirstName;
                    vm.agency.ContactPersonLastName = response.agencyList.ContactPersonLastName;
                    vm.agency.address = response.agencyList.Address;
                    vm.agency.email = response.agencyList.Email;
                    vm.agency.countryid = response.agencyList.CountryId;
                    vm.agency.stateid = response.agencyList.StateId;
                    vm.agency.cityname = response.agencyList.CityName;
                    vm.agency.postalcode = response.agencyList.PostalCode;
                    vm.agency.phonenumber = parseFloat(response.agencyList.PhoneNumber);
                    vm.agency.password = response.agencyList.Password;
                    vm.agency.isloggedfirsttime = response.agencyList.IsLoggedFirstTime;
                    vm.agency.OwnerFirstName = response.agencyList.OwnerFirstName;
                    vm.agency.OwnerLastName = response.agencyList.OwnerLastName;
                    vm.agency.UserId = response.agencyList.UserId;
                    vm.agency.PricingPlanId = response.agencyList.PricingPlanId;
                    vm.agency.TimeClockUsersPlanId = response.agencyList.TimeClockUsersPlanId;
                    GetState(vm.agency.countryid);
                }
                else {
                    if (response.agencyList.ID == 0) {
                        vm.agencyList = [];
                        vm.agencyCount = 0;
                        NotificationMessageController('Invalid found.');
                    }
                    else {
                        NotificationMessageController('Unable to get organization summary information at the moment. Please try again after some time.');
                    }
                }
            });

        };
        vm.cancel = cancel;
        vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;
        function cancel() {
            $mdDialog.cancel();
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
})();