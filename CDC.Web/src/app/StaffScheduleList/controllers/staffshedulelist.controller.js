(function () {
    'use strict';
    angular
        .module('app.staffschedule')
        .controller('StaffScheduleListController', StaffScheduleListController);

    function StaffScheduleListController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        StaffScheduleListService,StaffService, CommonService, filerService) {
        var vm = this;
        vm.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.staffscheduleCount = 0;
        vm.selected = [];
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StaffId:  localStorage.staffId == null ? StaffService.Id : localStorage.staffId ,
            AgencyId: localStorage.agencyId,
        };
        vm.GetStaffScheduleList = GetStaffScheduleList;
        vm.reset = reset;
        GetStaffScheduleList();
        function GetStaffScheduleList() {
            vm.promise = StaffScheduleListService.GetStaffScheduleListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoData = false;
                        vm.staffscheduleList = {};
                        vm.staffscheduleList = response.Content;
                        vm.staffscheduleCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoData = true;
                        vm.staffscheduleList = [];
                        vm.staffscheduleCount = 0;
                        NotificationMessageController('No Staff Schedule list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get staff schedule list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
        function reset() {
            vm.query.Name = '';
            vm.query.PriorityId = 0,
                vm.query.DueDate = null,
                GetStaffSkillList();
        };

    }
})();