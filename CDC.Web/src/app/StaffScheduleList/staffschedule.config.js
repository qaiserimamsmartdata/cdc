
(function () {
    'use strict';

    angular
        .module('app.staffschedule')
        .config(moduleConfig);

    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider

            .state('triangular.staffschedule', {
                url: '/staffschedule',
                templateUrl: 'app/StaffScheduleList/views/staffschedulelist.tmpl.html',
                controller: 'StaffScheduleListController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewStaffSchedule']
                    }
                }
            })

            ;

        // triMenuProvider.addMenu({
        //     name: 'Staff Schedule',
        //     icon: 'fa fa-calendar',
        //     type: 'link',
        //     priority: 1.9,
        //     permission: 'viewStaffSchedule',
        //     state: 'triangular.staffschedule'
        // });

    }
})();
