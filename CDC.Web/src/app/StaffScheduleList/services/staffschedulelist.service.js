(function () {
    'use strict';

    angular
        .module('app.staffschedule')
        .factory('StaffScheduleListService', StaffScheduleListService);

    StaffScheduleListService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function StaffScheduleListService($http, $q, HOST_URL) {
        return {
            GetStaffScheduleListService: GetStaffScheduleListService,
        };


        function GetStaffScheduleListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StaffSchedule/GetAllScheduleStaffWise',model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    }
})();