(function() {
    'use strict';

    angular
        .module('app.agencyevent')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
          .state('triangular.agencyevent', {
                url: '/agencyevent',
                templateUrl: 'app/AgencyEvent/agencyevent.tmpl.html',
                // set the controller to load for this page
                controller: 'AgencyEventController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewAgencyEvent']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
        
        ;

        triMenuProvider.addMenu({
            name: 'Events',
            icon: 'fa fa-calendar',
            type: 'link',
            permission: 'viewAgencyEvent',
            state: 'triangular.agencyevent',
            priority: 7

        });
    }
})();
