(function () {
    'use strict';

    angular
        .module('app.agencyevent')
        .controller('AgencyEventController', AgencyEventController);

    /* @ngInject */
    function AgencyEventController($scope, $http, $localStorage, HOST_URL, $window) {
        var vm = this;
        $scope.agencyId = localStorage.agencyId;
        localStorage.HostUrl = HOST_URL.url;
        localStorage.ParticipantAttendancePage = false;
        $scope.IsLoaded = false;
        window.loadAgencyEvent = function () {
            $scope.IsLoaded = true;
        }
    }
})();