(function() {
    'use strict';

    angular
        .module('app.store')
        .controller('StorePageController', StorePageController);

    /* @ngInject */
    function StorePageController() {
        var vm = this;
        vm.testData = ['triangular', 'is', 'great'];
    }
})();