(function () {
    'use strict';

    angular
        .module('app.store')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.store', {
                url: '/store',
                templateUrl: 'app/Store/views/store.tmpl.html',
                // set the controller to load for this page
                controller: 'StorePageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewStore']
                    }
                }
            });

        // triMenuProvider.addMenu({
        //     name: 'Store',
        //     icon: 'fa fa-tree',
        //     type: 'dropdown',
        //     permission: 'viewStore',
        //     priority: 1.1,
        //     children: [{
        //         name: 'Store',
        //         state: 'triangular.store',
        //         type: 'link'
        //     }]
        // });
    }
})();
