(function() {
    'use strict';
    angular
        .module('app.paytype')
        .factory('payTypeService', payTypeService);
    payTypeService.$inject = ['$http', '$q', 'HOST_URL'];

    function payTypeService($http, $q, HOST_URL) {

        return {
            GetPayTypeListService: GetPayTypeListService,
            DeletePayTypeById: DeletePayTypeById
        };

        function GetPayTypeListService(model) {
            var deferred = $q.defer();
            $http.get(HOST_URL.url + '/api/PayType/GetPayType', model).success(function(response) {
                deferred.resolve(response);
            }).error(function() {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }

        function DeletePayTypeById(payTypeId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/PayType/DeletePayTypeById?payTypeId=' + payTypeId).success(function(response) {
                deferred.resolve(response);
            }).error(function() {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }
    }
    /* @ngInject */
})();