(function () {
    'use strict';
    angular
        .module('app.paytype')
        .controller('PayTypeController', payTypeController)
        .controller('UpdatePayTypeController', updatePayTypeController);


    function payTypeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, payTypeService, HOST_URL) {
        var vm = this;
        vm.GetPayTypeList = getPayTypeList;
        vm.showDeletePayTypeConfirmDialoue = showDeletePayTypeConfirmDialoue;
        vm.showUpdatePayTypeDialoue = showUpdatePayTypeDialoue;
        vm.addPayType = addPayType;
        vm.showProgressbar = false;
        vm.master = {};
        vm.PayType = {
            Name: ''
        };
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.payTypeCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: ''
        };
        $scope.selected = [];
        vm.columns = {
            Name: 'PayType Name',
            ID: 'ID'
        };
        getPayTypeList();

        function getPayTypeList() {
            vm.promise = payTypeService.GetPayTypeListService($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.payTypeList = response.Content;
                    vm.payTypeCount = response.Content.length;
                    $scope.selected = [];
                } else {
                    if (response.Content.length === 0) {
                        vm.payTypeList = [];
                        vm.payTypeCount = 0;
                        notificationMessageController('No payType found.');
                    } else {
                        notificationMessageController('Unable to get payType list at the moment. Please try again after some time.');
                    }
                }
            });

        }

        function addPayType(registerPayType) {
            if (registerPayType.$valid) {
                var model = {
                    Name: registerPayType.PayType.$modelValue
                };
                $http.post(HOST_URL.url + '/api/PayType/AddUpdatePayType', model).then(function (response) {
                    if (response.data.IsSuccess === true) {
                        vm.showProgressbar = false;
                        registerPayType.$setPristine();
                        registerPayType.$setUntouched();
                        vm.PayType = angular.copy(vm.master);
                        getPayTypeList();
                        notificationMessageController('PayType added Successfully.');
                    } else {
                        vm.showProgressbar = false;
                        notificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function showUpdatePayTypeDialoue(event, data) {
            $mdDialog.show({
                controller: updatePayTypeController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/PayType/Views/updatedpaytype.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { paytypedetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }
        function showDeletePayTypeConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are You Sure to delete this PayType permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deletePayType(data);
            }, function () {
                $scope.hide();
            });
        }
        function deletePayType(data) {
            vm.promise = payTypeService.DeletePayTypeById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess === true) {
                    getPayTypeList();
                } else {

                    notificationMessageController('Unable to get PayType list at the moment. Please try again after some time.');
                }
            });
        }

        function notificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        }
    }

    function updatePayTypeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, payTypeService, HOST_URL, items) {

        var vm = this;
        vm.updatePayType = updatePayType;
        vm.payType = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        getPayTypeDetailsbyId();
        function getPayTypeDetailsbyId() {

            /// <summary>
            /// Gets the PayType detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.payType.Name = items.paytypedetail.Name;
            vm.payType.ID = items.paytypedetail.ID;
        }

        function updatePayType(updatePayType) {
            if (updatePayType.$valid) {
                var model = {
                    ID: vm.payType.ID,
                    Name: updatePayType.PayType.$viewValue
                };

                $http.post(HOST_URL.url + '/api/PayType/AddUpdatePayType', model).then(function (response) {
                    if (response.data.IsSuccess === true) {
                        vm.showProgressbar = false;
                        notificationMessageController('PayType updated Successfully.');
                        items.paytypedetail.Name = updatePayType.PayType.$viewValue;
                        $scope.hide();
                    } else {
                        vm.showProgressbar = false;
                        notificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $scope.hide();
                    }
                });
            }
        }
        function notificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        }
    }

    /* @ngInject */
})();