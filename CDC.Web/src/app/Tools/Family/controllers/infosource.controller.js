(function () {
    'use strict';

    function updateInfoSourceController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, items, apiService, $localStorage) {

        var vm = this;
        vm.infoSource = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getInfoSourceDetailsbyId() {
            /// <summary>
            /// Gets the info source detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.infoSource.Name = items.infosourcedetail.Name;
            vm.infoSource.ID = items.infosourcedetail.ID;
        }

        getInfoSourceDetailsbyId();

        function updateInfoSource(updateInfoSource) {
            if (updateInfoSource.$valid) {
                var model = {
                    ID: vm.infoSource.ID,
                    Name: updateInfoSource.InfoSource.$viewValue,
                    AgencyId: localStorage.agencyId
                };

                apiService.post('/api/InfoSource/AddUpdateInfoSource', model, UpdateInfoSourceSuccess,
                    UpdateRequestFailed);

            }
        }

        function UpdateInfoSourceSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Info source updated successfully.');
                items.infosourcedetail.Name = $scope.vm.infoSource.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateInfoSource = updateInfoSource;
    }



    function InfoSourceController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage, filerService) {
        var vm = this;
          $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 

        vm.addInfoSource = addInfoSource;
        vm.showProgressbar = false;
        vm.master = {};
        vm.InfoSource = {
            Name: ''
        };
        vm.toggleRight1 = filerService.toggleRight1();
        vm.isOpenRight1 = filerService.isOpenRight1();
        vm.close1 = filerService.close1();
        vm.reset = reset;
        vm.GetInfoSourceList = getInfoSourceList;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.infoSourceCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId
        };
        getInfoSourceList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Info Source',
            ID: 'ID'
        };

        function getInfoSourceList() {
            // var model = {
            //     AgencyId: localStorage.agencyId
            // };
            //apiService.post('/api/InfoSource/GetInfoSourceList', $scope.query, GetInfoSourceSuccess,RequestFailed);
            $scope.InfoSource=$scope.query.name==""?"All":$scope.query.name;
                   apiService.post('/api/InfoSource/GetAllInfoSource', $scope.query, GetInfoSourceSuccess,RequestFailed);
            vm.close1();

        }
        vm.showProgressbar=true;
        function GetInfoSourceSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoToDoData = false;
                vm.infoSourceList = response.data.Content;
                vm.infoSourceCount = response.data.TotalRows;
                vm.showProgressbar=false;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoToDoData = true;
                    vm.infoSourceList = [];
                    vm.infoSourceCount = 0;
                    vm.showProgressbar=false;
                    notificationService.displaymessage('No Info Source found.');
                } else {
                    notificationService.displaymessage('Unable to get info source list at the moment. Please try again after some time.');
                }
            }
        }

        function addInfoSource(registerInfoSource) {
            if (registerInfoSource.$valid) {
                var model = {
                    Name: registerInfoSource.InfoSource.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerInfoSource;
                apiService.post('/api/InfoSource/AddUpdateInfoSource', model, AddInfoSourceSuccess,RequestFailed);

            }
        }

        function AddInfoSourceSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.infoSource.Name = "";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.InfoSource = angular.copy(vm.master);
                getInfoSourceList();
                notificationService.displaymessage('Info source added successfully.');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function deleteInfoSource(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/InfoSource/DeleteInfoSource', data, DeleteInfoSourceSuccess,
                RequestFailed);
        }

        function DeleteInfoSourceSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Info source deleted successfully.')
                getInfoSourceList();
            } else {
                notificationService.displaymessage('Unable to get info Source list at the moment. Please try again after some time.');
            }
        }

        function reset() {
            $scope.query.name = '';
            $scope.InfoSource="All";
            getInfoSourceList();
        }

        function showDeleteInfoSourceConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this info source permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteInfoSource(data);
            }, function () {
                $scope.hide();
            });
        }

        vm.showDeleteInfoSourceConfirmDialoue = showDeleteInfoSourceConfirmDialoue;

        function showUpdateInfoSourceDialoue(event, data) {
            $mdDialog.show({
                controller: updateInfoSourceController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Family/Views/updateinfosource.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { infosourcedetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.showUpdateInfoSourceDialoue = showUpdateInfoSourceDialoue;
    }

    angular
        .module('app.dropdownfamily')
        .controller('InfoSourceController', InfoSourceController)
        .controller('UpdateInfoSourceController', updateInfoSourceController);
    /* @ngInject */
})();