(function () {
    'use strict';

    function updateRelationshipTypeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage) {

        var vm = this;
        vm.relationshipType = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getRelationshipTypeDetailsbyId() {
            /// <summary>
            /// Gets the Relationship Type detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.relationshipType.Name = items.relationshiptypedetail.Name;
            vm.relationshipType.ID = items.relationshiptypedetail.ID;
        }

        getRelationshipTypeDetailsbyId();

        function updateRelationshipType(updateRelationshipType) {
            if (updateRelationshipType.$valid) {
                var model = {
                    ID: vm.relationshipType.ID,
                    Name: updateRelationshipType.RelationshipType.$viewValue,
                    AgencyId: localStorage.agencyId
                };

                apiService.post('/api/RelationshipType/AddUpdateRelationshipType', model, updateRelationshipTypeSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateRelationshipTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Relationship type updated successfully.');
                items.relationshiptypedetail.Name = $scope.vm.relationshipType.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please try again")
        }

        vm.updateRelationshipType = updateRelationshipType;
    }

    function RelationshipTypeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage, filerService) {
        var vm = this;
          $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 

        vm.addRelationshipType = addRelationshipType;
        vm.showProgressbar = false;
        vm.master = {};
        vm.RelationshipType = {
            Name: ''
        };
        vm.toggleRight3 = filerService.toggleRight3();
        vm.isOpenRight3 = filerService.isOpenRight3();
        vm.close3 = filerService.close3();
        vm.reset = reset;
        vm.GetRelationshipTypeList = getRelationshipTypeList;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.relationshipTypeCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId
        };
        getRelationshipTypeList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Relationship Type',
            ID: 'ID'
        };

        function getRelationshipTypeList() {
            // var model = {
            //     AgencyId: localStorage.agencyId
            // };
            // apiService.post('/api/RelationshipType/GetRelationshipTypeList', $scope.query, getRelationshipTypeSuccess,RequestFailed);
            $scope.RelationshipType=$scope.query.name==""?"All":$scope.query.name;
            apiService.post('/api/RelationshipType/GetAllRelationshipType', $scope.query, getRelationshipTypeSuccess,RequestFailed);            
            vm.close3();
        }
        vm.showProgressbar=true;
        function getRelationshipTypeSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoToDoData = false;
                vm.relationshipTypeList = response.data.Content;
                vm.relationshipTypeCount = response.data.TotalRows;
                vm.showProgressbar=false;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoToDoData = true;
                    vm.relationshipTypeList = [];
                    vm.relationshipTypeCount = 0;
                    vm.showProgressbar=false;
                    notificationService.displaymessage('No Relationship Type found.');
                } else {
                    notificationService.displaymessage('Unable to get relationship type list at the moment. Please try again after some time.');
                }
            }
        }

        function addRelationshipType(registerRelationshipType) {
            if (registerRelationshipType.$valid) {
                var model = {
                    Name: registerRelationshipType.RelationshipType.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerRelationshipType;
                apiService.post('/api/RelationshipType/AddUpdateRelationshipType', model, AddRelationshipTypeSuccess,
                    RequestFailed);

            }
        }

        function AddRelationshipTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.RelationshipType.Name = "";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.RelationshipType = angular.copy(vm.master);

                getRelationshipTypeList();
                notificationService.displaymessage('Relationship type added successfully.');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function deleteRelationshipType(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/RelationshipType/DeleteRelationshipType', data, DeleteRelationshipTypeSuccess,
                RequestFailed);
        }

        function DeleteRelationshipTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Relationship type deleted successfully.')
                getRelationshipTypeList();
            } else {

                notificationService.displaymessage('Unable to get relationship type list at the moment. Please try again after some time.');
            }
        }

        function reset() {
            $scope.query.name = '';
            getRelationshipTypeList();
        }

        function showDeleteRelationshipTypeConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this relationship type permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteRelationshipType(data);
            }, function () {
                $scope.hide();
            });
        }

        vm.showDeleteRelationshipTypeConfirmDialoue = showDeleteRelationshipTypeConfirmDialoue;

        function showUpdateRelationshipTypeDialoue(event, data) {
            $mdDialog.show({
                controller: updateRelationshipTypeController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Family/Views/updaterelationshiptype.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { relationshiptypedetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.showUpdateRelationshipTypeDialoue = showUpdateRelationshipTypeDialoue;
    }

    angular
        .module('app.dropdownfamily')
        .controller('RelationshipTypeController', RelationshipTypeController)
        .controller('UpdateRelationshipTypeController', updateRelationshipTypeController);
    /* @ngInject */
})();