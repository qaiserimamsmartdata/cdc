(function () {
    'use strict';
    angular
        .module('app.dropdownfamily')
        .controller('GradeLevelController', GradeLevelController)
        .controller('UpdateGradeLevelController', updateGradeLevelController);

    function updateGradeLevelController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage) {

        var vm = this;
        vm.gradeLevel = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getGradeLevelDetailsbyId() {
            /// <summary>
            /// Gets the grade level detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.gradeLevel.Name = items.gradeleveldetail.Name;
            vm.gradeLevel.ID = items.gradeleveldetail.ID;
        }

        getGradeLevelDetailsbyId();

        function updateGradeLevel(updateGradeLevel) {
            if (updateGradeLevel.$valid) {
                var model = {
                    ID: vm.gradeLevel.ID,
                    Name: updateGradeLevel.GradeLevel.$viewValue,
                    AgencyId: localStorage.agencyId
                };

                apiService.post('/api/GradeLevel/AddUpdateGradeLevel', model, updateGradeLevelSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateGradeLevelSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Grade level updated successfully !');
                items.gradeleveldetail.Name = $scope.vm.gradeLevel.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateGradeLevel = updateGradeLevel;
    }

    function GradeLevelController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage,filerService) {
        var vm = this;
         $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 
        localStorage.ParticipantAttendancePage = false;
        vm.addGradeLevel = addGradeLevel;
        vm.showProgressbar = false;
        vm.master = {};
        vm.GradeLevel = {
            Name: ''
        };
        vm.GetGradeLevelList = getGradeLevelList;
        vm.showDeleteGradeLevelConfirmDialoue = showDeleteGradeLevelConfirmDialoue;
        vm.showUpdateGradeLevelDialoue = showUpdateGradeLevelDialoue;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.gradeLevelCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId,
        };
        getGradeLevelList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Grade Level',
            ID: 'ID'
        };

        function getGradeLevelList() {
            // var model = {
            //     AgencyId: localStorage.agencyId
            // };
            //apiService.post('/api/GradeLevel/GetGradeLevelList', $scope.query, getGradeLevelSuccess,RequestFailed);
            $scope.GradeLevel=$scope.query.name==""?"All":$scope.query.name;
            apiService.post('/api/GradeLevel/GetAllGradeLevel', $scope.query, getGradeLevelSuccess,RequestFailed);
            vm.close();
        }
        vm.showProgressbar=true;
        function getGradeLevelSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoToDoData = false;
                vm.gradeLevelList = response.data.Content;
                vm.gradeLevelCount = response.data.TotalRows;
                vm.showProgressbar=false;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoToDoData = true;
                    vm.gradeLevelList = [];
                    vm.gradeLevelCount = 0;
                    vm.showProgressbar=false;
                    notificationService.displaymessage('No Grade level list found.');
                } else {
                    notificationService.displaymessage('Unable to get grade level list at the moment. Please try again after some time.');
                }
            }
        }

        function addGradeLevel(registerGradeLevel) {
            if (registerGradeLevel.$valid) {
                var model = {
                    Name: registerGradeLevel.GradeLevel.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerGradeLevel;
                apiService.post('/api/GradeLevel/AddUpdateGradeLevel', model, AddGradeLevelSuccess,
                    RequestFailed);
            }
        }

        function AddGradeLevelSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.GradeLevel.Name = "";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.GradeLevel = angular.copy(vm.master);
                getGradeLevelList();
                notificationService.displaymessage('Grade level added successfully !');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function deleteGradeLevel(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/GradeLevel/DeleteGradeLevel', data, DeleteGradeLevelSuccess,
                RequestFailed);
        }

        function DeleteGradeLevelSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Grade level deleted successfully.')
                getGradeLevelList();

            } else {
                notificationService.displaymessage('Unable to get grade level list at the moment. Please try again after some time.');
            }
        }

        function reset() {
            $scope.query.name = '';
            $scope.GradeLevel="All";
            getGradeLevelList();
        }

        function showDeleteGradeLevelConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this grade level permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteGradeLevel(data);
            }, function () {
                $scope.hide();
            });
        }

        function showUpdateGradeLevelDialoue(event, data) {
            $mdDialog.show({
                controller: updateGradeLevelController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Family/Views/updategradelevel.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { gradeleveldetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
    }
    /* @ngInject */
})();