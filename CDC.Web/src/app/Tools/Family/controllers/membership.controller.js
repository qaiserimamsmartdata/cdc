(function () {
    'use strict';

    function updateMembershipTypeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage) {

        var vm = this;
        vm.membershipType = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getMembershipTypeDetailsbyId() {
            /// <summary>
            /// Gets the membership type detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.membershipType.Name = items.membershiptypedetail.Name;
            vm.membershipType.ID = items.membershiptypedetail.ID;
        }

        getMembershipTypeDetailsbyId();

        function updateMembershipType(updateMembershipType) {
            if (updateMembershipType.$valid) {
                var model = {
                    ID: vm.membershipType.ID,
                    Name: updateMembershipType.MembershipType.$viewValue,
                    AgencyId: localStorage.agencyId
                };

                apiService.post('/api/MembershipType/AddUpdateMembershipType', model, updateMembershipTypeSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateMembershipTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Membership type updated successfully.');
                items.membershiptypedetail.Name = $scope.vm.membershipType.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateMembershipType = updateMembershipType;
    }

    function MembershipTypeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage, filerService) {
        var vm = this;
          $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 

        vm.addMembershipType = addMembershipType;
        vm.showProgressbar = false;
        vm.master = {};
        vm.MembershipType = {
            Name: ''
        };
        vm.toggleRight2 = filerService.toggleRight2();
        vm.isOpenRight2 = filerService.isOpenRight2();
        vm.close2 = filerService.close2();
        vm.reset = reset;
        vm.GetMembershipTypeList = getMembershipTypeList;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.membershipTypeCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId
        };
        getMembershipTypeList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Membership Type',
            ID: 'ID'
        };

        function getMembershipTypeList() {
            // var model = {
            //     AgencyId: localStorage.agencyId
            // };
            // apiService.post('/api/MembershipType/GetMembershipTypeList', $scope.query, GetMembershipTypeSuccess,RequestFailed);
            $scope.MembershipType=$scope.query.name==""?"All":$scope.query.name;
            apiService.post('/api/MembershipType/GetAllMembershipType', $scope.query, GetMembershipTypeSuccess,RequestFailed);
            
            vm.close2()
        }

        vm.showProgressbar=true;
        function GetMembershipTypeSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoToDoData = false;
                vm.membershipTypeList = response.data.Content;
                vm.membershipTypeCount = response.data.TotalRows;
                vm.showProgressbar=false;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoToDoData = true;
                    vm.membershipTypeList = [];
                    vm.membershipTypeCount = 0;
                    vm.showProgressbar=false;
                    notificationService.displaymessage('No Membership Type found.');
                } else {
                    notificationService.displaymessage('Unable to get membership type list at the moment. Please try again after some time.');
                }
            }
        }

        function addMembershipType(registerMembershipType) {
            if (registerMembershipType.$valid) {
                var model = {
                    Name: registerMembershipType.MembershipType.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerMembershipType;
                apiService.post('/api/MembershipType/AddUpdateMembershipType', model, AddMembershipTypeSuccess,
                    RequestFailed);

            }
        }

        function AddMembershipTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.MembershipType.Name = "";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.MembershipType = angular.copy(vm.master);
                getMembershipTypeList();
                notificationService.displaymessage('Membership type added successfully.');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function deleteMembershipType(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/MembershipType/DeleteMembershipType', data, DeleteMembershipTypeSuccess,
                RequestFailed);
        }

        function DeleteMembershipTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Membership deleted successfully.')
                getMembershipTypeList();
            } else {

                notificationService.displaymessage('Unable to get membership type list at the moment. Please try again after some time.');
            }
        }

        function reset() {
            $scope.query.name = '';
            $scope.MembershipType="All"
            getMembershipTypeList();
        }

        function showDeleteMembershipTypeConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this membership type permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteMembershipType(data);
            }, function () {
                $scope.hide();
            });
        }

        vm.showDeleteMembershipTypeConfirmDialoue = showDeleteMembershipTypeConfirmDialoue;

        function showUpdateMembershipTypeDialoue(event, data) {
            $mdDialog.show({
                controller: updateMembershipTypeController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Family/Views/updatemembershiptype.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { membershiptypedetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.showUpdateMembershipTypeDialoue = showUpdateMembershipTypeDialoue;
    }

    angular
        .module('app.dropdownfamily')
        .controller('MembershipTypeController', MembershipTypeController)
        .controller('UpdateMembershipTypeController', updateMembershipTypeController);
    /* @ngInject */
})();