(function () {
    'use strict';
    angular
        .module('app.dropdownfamily')
        .factory('infoSourceService', infoSourceService);
    infoSourceService.$inject = ['$http', '$q', 'HOST_URL'];

    function infoSourceService($http, $q, HOST_URL) {

        return {
            GetInfoSourceListService: getInfoSourceListService,
            DeleteInfoSourceById: deleteInfoSourceById
        };

        function getInfoSourceListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/InfoSource/GetInfoSource', model).success(function (response) {
                deferred.resolve(response);
            }).error(function () {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }

        function deleteInfoSourceById(infoSourceId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/InfoSource/DeleteInfoSourceById?InfoSourceId=' + infoSourceId).success(function (response) {
                deferred.resolve(response);
            }).error(function () {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }
    }
    /* @ngInject */
})();