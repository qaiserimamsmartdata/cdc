(function () {
    'use strict';
    angular
        .module('app.dropdownfamily')
        .factory('gradeLevelService', gradeLevelService);
    gradeLevelService.$inject = ['$http', '$q', 'HOST_URL'];

    function gradeLevelService($http, $q, HOST_URL) {

        return {
            GetGradeLevelListService: getGradeLevelListService,
            DeleteGradeLevelById: deleteGradeLevelById
        };

        function getGradeLevelListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/GradeLevel/GetGradeLevel', model).success(function (response) {
                deferred.resolve(response);
            }).error(function () {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }

        function deleteGradeLevelById(gradeLevelId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/GradeLevel/DeleteGradeLevelById?GradeLevelId=' + gradeLevelId).success(function (response) {
                deferred.resolve(response);
            }).error(function () {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }
    }
    /* @ngInject */
})();