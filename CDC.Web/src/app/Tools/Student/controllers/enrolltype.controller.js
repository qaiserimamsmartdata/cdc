(function () {
    'use strict';
    angular
        .module('app.dropdownfamily')
        .controller('EnrollTypeController', EnrollTypeController)
        .controller('UpdateEnrollTypeController', UpdateEnrollTypeController);

    function UpdateEnrollTypeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage) {

        var vm = this;
        vm.enrollType = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getEnrollTypeDetailsbyId() {
            /// <summary>
            /// Gets the enroll type detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.enrollType.Name = items.enrolltypedetail.Name;
            vm.enrollType.ID = items.enrolltypedetail.ID;
        }

        getEnrollTypeDetailsbyId();

        function updateEnrollType(updateEnrollType) {
            if (updateEnrollType.$valid) {
                var model = {
                    ID: vm.enrollType.ID,
                    Name: updateEnrollType.EnrollType.$viewValue,
                    AgencyId: localStorage.agencyId
                };

                apiService.post('/api/EnrollType/AddUpdateEnrollType', model, updateEnrollTypeSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateEnrollTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Enroll type updated successfully.');
                items.enrolltypedetail.Name = $scope.vm.enrollType.Name;
                $mdDialog.hide();
            } else{
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
                $mdDialog.hide();
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateEnrollType = updateEnrollType;
    }

    function EnrollTypeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage) {
        var vm = this;
        vm.addEnrollType = addEnrollType;
        vm.showProgressbar = false;
        vm.master = {};
        vm.EnrollType = {
            Name: ''
        };
        vm.GetEnrollTypeList = getEnrollTypeList;
        vm.showDeleteEnrollTypeConfirmDialoue = showDeleteEnrollTypeConfirmDialoue;
        vm.showUpdateEnrollTypeDialoue = showUpdateEnrollTypeDialoue;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.enrollTypeCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: ''
        };
        getEnrollTypeList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Enroll Type',
            ID: 'ID'
        };

        function getEnrollTypeList() {
            var model = {
                AgencyId: localStorage.agencyId
            };
            apiService.post('/api/EnrollType/GetEnrollType', model, getEnrollTypeSuccess,
                RequestFailed);
        }

        function getEnrollTypeSuccess(response) {
            if (response.data.enrollType.length > 0) {
                vm.enrollTypeList = response.data.enrollType;
                vm.enrollTypeCount = response.data.enrollType.length;
                $scope.selected = [];
            } else {
                if (response.data.enrollType.length === 0) {
                    vm.enrollTypeList = [];
                    vm.enrollTypeCount = 0;
                    notificationService.displaymessage('No enroll type found.');
                } else {
                    notificationService.displaymessage('Unable to get enroll type list at the moment. Please try again after some time.');
                }
            }
        }

        function addEnrollType(registerEnrollType) {
            if (registerEnrollType.$valid) {
                var model = {
                    Name: registerEnrollType.EnrollType.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerEnrollType;
                apiService.post('/api/EnrollType/AddUpdateEnrollType', model, AddEnrollTypeSuccess,
                    RequestFailed);
            }
        }

        function AddEnrollTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.EnrollType.Name = "";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.EnrollType = angular.copy(vm.master);
                getEnrollTypeList();
                notificationService.displaymessage('Enroll type added successfully.');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function deleteEnrollType(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/EnrollType/DeleteEnrollTypeById', data, DeleteEnrollTypeSuccess,
                RequestFailed);
        }

        function DeleteEnrollTypeSuccess(response) {
            if (response.data.IsSuccess === true) {
                getEnrollTypeList();
            } else {
                notificationService.displaymessage('Unable to get enroll type list at the moment. Please try again after some time.');
            }
        }

        function showDeleteEnrollTypeConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this enroll type permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteEnrollType(data);
            }, function () {
                $scope.hide();
            });
        }

        function showUpdateEnrollTypeDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateEnrollTypeController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Student/Views/updateenrolltype.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { enrolltypedetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
    }
    /* @ngInject */
})();