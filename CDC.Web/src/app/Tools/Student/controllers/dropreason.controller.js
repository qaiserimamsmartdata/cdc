(function () {
    'use strict';
    angular
        .module('app.dropdownstudent')
        .controller('DropReasonController', DropReasonController)
        .controller('UpdateDropReasonController', UpdateDropReasonController);

    function UpdateDropReasonController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage) {

        var vm = this;
        vm.dropReason = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getDropReasonDetailsbyId() {
            /// <summary>
            /// Gets the drop reason detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.dropReason.Name = items.dropreasondetail.Name;
            vm.dropReason.ID = items.dropreasondetail.ID;
        }

        getDropReasonDetailsbyId();

        function updateDropReason(updateDropReason) {
            if (updateDropReason.$valid) {
                var model = {
                    ID: vm.dropReason.ID,
                    Name: updateDropReason.DropReason.$viewValue,
                    AgencyId: localStorage.agencyId
                };

                apiService.post('/api/DropReason/AddUpdateDropReason', model, updateDropReasonSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateDropReasonSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Drop reason updated successfully.');
                items.dropreasondetail.Name = $scope.vm.dropReason.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
                $mdDialog.hide();
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateDropReason = updateDropReason;
    }

    function DropReasonController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage, filerService) {
        var vm = this;
        vm.addDropReason = addDropReason;
        vm.showProgressbar = false;
        vm.master = {};
        vm.DropReason = {
            Name: ''
        };
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        vm.GetDropReasonList = getDropReasonList;
        vm.showDeleteDropReasonConfirmDialoue = showDeleteDropReasonConfirmDialoue;
        vm.showUpdateDropReasonDialoue = showUpdateDropReasonDialoue;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.dropReasonCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId
        };
        getDropReasonList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Drop Reason',
            ID: 'ID'
        };

        function getDropReasonList() {
            // var model = {
            //     AgencyId: localStorage.agencyId
            // };
            apiService.post('/api/DropReason/GetDropReason', $scope.query, getDropReasonSuccess,
                RequestFailed);
            vm.close();
        }

        function getDropReasonSuccess(response) {
            if (response.data.dropReason.length > 0) {
                vm.dropReasonList = response.data.dropReason;
                vm.dropReasonCount = response.data.dropReason.length;
                $scope.selected = [];
            } else {
                if (response.data.dropReason.length === 0) {
                    vm.dropReasonList = [];
                    vm.dropReasonCount = 0;
                    notificationService.displaymessage('No drop reason found.');
                } else {
                    notificationService.displaymessage('Unable to get drop reason list at the moment. Please try again after some time.');
                }
            }
        }

        function addDropReason(registerDropReason) {
            if (registerDropReason.$valid) {
                var model = {
                    Name: registerDropReason.DropReason.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerDropReason;
                apiService.post('/api/DropReason/AddUpdateDropReason', model, AddDropReasonSuccess,
                    RequestFailed);
            }
        }

        function AddDropReasonSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.DropReason.Name = "";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.DropReason = angular.copy(vm.master);
                getDropReasonList();
                notificationService.displaymessage('Drop reason added successfully.');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function deleteDropReason(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/DropReason/DeleteDropReasonById', data, DeleteDropReasonSuccess,
                RequestFailed);
        }

        function DeleteDropReasonSuccess(response) {
            if (response.data.IsSuccess === true) {
                getDropReasonList();
            } else {
                notificationService.displaymessage('Unable to get drop reason list at the moment. Please try again after some time.');
            }
        }

        function reset() {
            $scope.query.name = '';
            getDropReasonList();
        }

        function showDeleteDropReasonConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this drop reason permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteDropReason(data);
            }, function () {
                $scope.hide();
            });
        }

        function showUpdateDropReasonDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateDropReasonController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Student/Views/updatedropreason.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { dropreasondetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
    }
    /* @ngInject */
})();