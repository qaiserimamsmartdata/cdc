(function () {
    'use strict';
    angular
        .module('app.dropdownstudent')
        .config(moduleConfig);
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.student', {
                url: '/participant',
                templateUrl: 'app/Tools/Student/views/student.tmpl.html',
                // set the controller to load for this page
                // controller: 'StudentController',
                // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            });
    }
    /* @ngInject */
})();