﻿(function() {
    'use strict';
    angular
        .module('app.Position')
        .factory('PositionService', PositionService);
    PositionService.$inject = ['$http', '$q', 'HOST_URL'];

    function PositionService($http, $q, HOST_URL) {

        return {
            GetPositionListService: GetPositionListService,
            DeletePositionById: DeletePositionById
        };

        function GetPositionListService(model) {
            var deferred = $q.defer();
            $http.get(HOST_URL.url + '/api/Position/GetPosition', model).success(function(response) {
                deferred.resolve(response);
            }).error(function() {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }

        function DeletePositionById(PositionId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Position/DeletePositionById?PositionId=' + PositionId).success(function(response) {
                deferred.resolve(response);
            }).error(function() {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }
    }
    /* @ngInject */
})();