(function () {
    'use strict';
    angular
        .module('app.Position')
        .controller('PositionController', PositionController)
        .controller('UpdatePositionController', updatePositionController);


    function PositionController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, PositionService, HOST_URL) {
        var vm = this;
        vm.GetPositionList = getPositionList;
        vm.showDeletePositionConfirmDialoue = showDeletePositionConfirmDialoue;
        vm.showUpdatePositionDialoue = showUpdatePositionDialoue;
        vm.addPosition = addPosition;
        vm.showProgressbar = false;
        vm.master = {};
        vm.Position = {
            Name: ''
        };
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.PositionCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: ''
        };
        $scope.selected = [];
        vm.columns = {
            Name: 'Position Name',
            ID: 'ID'
        };
        getPositionList();

        function getPositionList() {
            vm.promise = PositionService.GetPositionListService($scope.query);
            vm.promise.then(function (response) {
                if (response.Position.length > 0) {
                    vm.PositionList = response.Position;
                    vm.PositionCount = response.Position.length;
                    $scope.selected = [];
                } else {
                    if (response.Position.length === 0) {
                        vm.PositionList = [];
                        vm.PositionCount = 0;
                        notificationMessageController('No position list found.');
                    } else {
                        notificationMessageController('Unable to get Position list at the moment. Please try again after some time.');
                    }
                }
            });

        }

        function addPosition(registerPosition) {
            if (registerPosition.$valid) {
                var model = {
                    Name: registerPosition.Position.$modelValue
                };
                $http.post(HOST_URL.url + '/api/Position/AddUpdatePosition', model).then(function (response) {
                    if (response.data.IsSuccess === true) {
                        vm.showProgressbar = false;
                        registerPosition.$setPristine();
                        registerPosition.$setUntouched();
                        vm.Position = angular.copy(vm.master);
                        getPositionList();
                        notificationMessageController('Position added Successfully.');
                    } else {
                        vm.showProgressbar = false;
                        notificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function showUpdatePositionDialoue(event, data) {
            $mdDialog.show({
                controller: updatePositionController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Position/views/updatePosition.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { Positiondetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }
        function showDeletePositionConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are You Sure to delete this Position permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deletePosition(data);
            }, function () {
                $scope.hide();
            });
        }
        function deletePosition(data) {
            vm.promise = PositionService.DeletePositionById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess === true) {
                    getPositionList();
                } else {

                    notificationMessageController('Unable to get Position list at the moment. Please try again after some time.');
                }
            });
        }

        function notificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        }
    }

    function updatePositionController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, PositionService, HOST_URL, items) {

        var vm = this;
        vm.updatePosition = updatePosition;
        vm.Position = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        getPositionDetailsbyId();
        function getPositionDetailsbyId() {

            /// <summary>
            /// Gets the Position detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.Position.Name = items.Positiondetail.Name;
            vm.Position.ID = items.Positiondetail.ID;
        }
        
        function updatePosition(updatePosition) {
            if (updatePosition.$valid) {
                var model = {
                    ID: vm.Position.ID,
                    Name: updatePosition.Position.$viewValue
                };

                $http.post(HOST_URL.url + '/api/Position/AddUpdatePosition', model).then(function (response) {
                    if (response.data.IsSuccess === true) {
                        vm.showProgressbar = false;
                        notificationMessageController('Position updated Successfully.');
                        items.Positiondetail.Name = updatePosition.Position.$viewValue;
                        $mdDialog.hide();
                    } else {
                        vm.showProgressbar = false;
                        notificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }
        function notificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        }
    }

    /* @ngInject */
})();