(function() {
    'use strict';
    angular
        .module('app.Position')
        .config(moduleConfig);
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.Positionlist', {
                url: '/Position',
                templateUrl: 'app/Tools/Position/views/Positionlist.tmpl.html',
                // set the controller to load for this page
                controller: 'PositionController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.addPosition', {
                url: '/Position/add',
                templateUrl: 'app/Tools/Position/views/addPosition.tmpl.html',
                // set the controller to load for this page
                controller: 'PositionController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            });
        // triMenuProvider.addMenu({
        //     name: 'Tools',
        //     icon: 'md-cyan-theme material-icons zmdi zmdi-account-box',
        //     type: 'dropdown',
        //     priority: 1.1,
        //     children: [
        //         {
        //             name: 'Settings',
        //             state: 'triangular.editsettings',
        //             type: 'link'
        //         },
        //         {
        //             name: 'User Settings',
        //             state: 'triangular.usersetting',
        //             type: 'link'
        //         }
        //     ]
        // });
    }
    /* @ngInject */
})();