(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('SessionController', SessionController)
        .controller('UpdateSessionController', UpdateSessionController);

    /* @ngInject */

    function SessionController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, SessionService,$localStorage, HOST_URL,apiService) {
        var vm = this;
        vm.master = {};
        vm.monthValidation = false;
        vm.ValidateSession = ValidateSession;
        vm.showDeleteSessionConfirmDialoue = showDeleteSessionConfirmDialoue;
        vm.showUpdateSessionDialoue = showUpdateSessionDialoue;
        GetSessionList();
        vm.addSession = addSession;
        vm.showProgressbar = false;
        // $scope.selected = [];
        //   $scope.query = {
        //     filter: '',
        //     limit: '10',
        //     order: '-id',
        //     page: 1,
        //     status: 0,
        //     Name: '',
        //     AgencyId: localStorage.agencyId
        // };

        $scope.Months = [
            { id: 1, name: "January" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" }];

        var date = new Date();
        vm.currentYear = date.getFullYear();
        vm.currentMonth = date.getMonth() + 1;
        vm.columns = {
            Name: 'Session Name',
            ID: 'ID'
        };
        $scope.GetSessionList = GetSessionList;

        // vm.Session = {
        //     Name: ''
        // };
        // function GetSessionList() {
        //     vm.promise = SessionService.GetSessionListService($scope.query);
        //     vm.promise.then(function (response) {
        //         if (response.session.length > 0) {
        //             vm.sessionList = response.session;
        //             vm.sessionCount = response.session.length;
        //             $scope.selected = [];
        //         }
        //         else {
        //             if (response.session.length == 0) {
        //                 vm.sessionList = [];
        //                 vm.sessionCount = 0;
        //                 NotificationMessageController('No session list found.');
        //             }
        //             else {
        //                 NotificationMessageController('Unable to get session list at the moment. Please try again after some time.');
        //             }
        //         }
        //     });
        // };

        function GetSessionList() {
            apiService.post('/api/Session/GetAllSession', $scope.query, GetSessionListSuccess,RequestFailed);
            vm.close();
        }
        function GetSessionListSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.sessionList = response.data.Content;
                vm.sessionCount = response.data.TotalRows;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.sessionList = [];
                    vm.sessionCount = 0;
                    notificationService.displaymessage('No session list found.');
                } else {
                    notificationService.displaymessage('Unable to get session list at the moment. Please try again after some time.');
                }
            }
        }
          function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }


        function addSession(registerSession) {
            if (registerSession.$valid) {
                var model = {
                    Name: $scope.Months[registerSession.frommonth.$modelValue].name + " " + registerSession.fromyear.$modelValue + " - " + $scope.Months[registerSession.tomonth.$modelValue].name + " " + registerSession.toyear.$modelValue
                };
                $http.post(HOST_URL.url + '/api/Session/AddUpdateSession', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        registerSession.$setPristine();
                        registerSession.$setUntouched();
                        vm.Session = angular.copy(vm.master);
                        GetSessionList();
                        NotificationMessageController('Session added Successfully.');
                        $state.go('triangular.Sessionlist');
                    }
                    else if (response.data.Message != null && response.data.Message != "") {
                        NotificationMessageController(response.data.Message);
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function ValidateSession() {
            vm.monthValidation = false;
            if (vm.Session.fromyear == vm.Session.toyear) {
                if (vm.Session.frommonth > vm.Session.tomonth) {
                    vm.monthValidation = true;
                }
            }
        }

        function showDeleteSessionConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are You Sure to delete this Session permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteSession(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteSession(data) {
            vm.promise = SessionService.DeleteSessionById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    GetSessionList();
                }
                else {
                    NotificationMessageController('Unable to get session list at the moment. Please try again after some time.');
                }
            });
        };

        function showUpdateSessionDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateSessionController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Session/views/updatesession.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { sessiondetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

    function UpdateSessionController($scope, $filter, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, SessionService, HOST_URL, items) {

        var vm = this;
        vm.showProgressbar = false;
        vm.tmonth = null;
        vm.fmonth = null;
        vm.ValidateSession = ValidateSession;
        vm.updateSession = updateSession;
        vm.monthValidation = false;

        var date = new Date();
        vm.currentYear = date.getFullYear();
        vm.currentMonth = date.getMonth() + 1;

        vm.updatesessionmodel = {
            Name: '',
            ID: '',
            frommonth: 0,
            tomonth: 0,
            fromyear: 0,
            toyear: 0
        };

        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        $scope.Months = [
            { id: 1, name: "January" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" }];
        GetSessionDetailbyId();

        function GetSessionDetailbyId() {
            vm.fmonth = items.sessiondetail.Name.split(' - ')[0].split(' ')[0];
            vm.updatesessionmodel.frommonth = $filter('filter')($scope.Months, { name: vm.fmonth })[0].id;
            vm.updatesessionmodel.fromyear = parseFloat(items.sessiondetail.Name.split(' - ')[0].split(' ')[1]);
            vm.tmonth = items.sessiondetail.Name.split(' - ')[1].split(' ')[0];
            vm.updatesessionmodel.tomonth = $filter('filter')($scope.Months, { name: vm.tmonth })[0].id;
            vm.updatesessionmodel.toyear = parseFloat(items.sessiondetail.Name.split(' - ')[1].split(' ')[1]);
            //vm.session.Name = items.sessiondetail.Name;
            //vm.session.ID = items.sessiondetail.ID;
        }

        function ValidateSession() {
            vm.monthValidation = false;
            if (vm.updatesessionmodel.fromyear == vm.updatesessionmodel.toyear) {
                if (vm.updatesessionmodel.frommonth > vm.updatesessionmodel.tomonth) {
                    vm.monthValidation = true;
                }
            }
        }

        function updateSession(updateSessionform) {
            vm.showProgressbar = true;
            if (updateSessionform.$valid) {
                var model = {
                    ID: items.sessiondetail.ID,
                    Name: $scope.Months[updateSessionform.frommonth.$modelValue].name + " " + updateSessionform.fromyear.$modelValue + " - " + $scope.Months[updateSessionform.tomonth.$modelValue].name + " " + updateSessionform.toyear.$modelValue

                };
                $http.post(HOST_URL.url + '/api/Session/AddUpdateSession', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        //$scope.GetSessionList;
                        NotificationMessageController('Session updated Successfully.');
                        items.sessiondetail.Name = model.Name;
                        $mdDialog.hide();
                    }
                    else if (response.data.Message != null && response.data.Message != "") {
                        vm.showProgressbar = false;
                        NotificationMessageController(response.data.Message);
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
})();