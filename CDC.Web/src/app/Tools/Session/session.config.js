(function () {
    'use strict';

    angular
        .module('app.session')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider, $urlRouterProvider) {

        $stateProvider
       
            .state('triangular.sessionlist', {
                url: '/session',
                templateUrl: 'app/Tools/Session/Views/sessionlist.tmpl.html',
                controller: 'SessionController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.addsession', {
                url: '/session/add',
                templateUrl: 'app/Tools/Session/Views/addsession.tmpl.html',
                controller: 'SessionController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            
    }
})();
