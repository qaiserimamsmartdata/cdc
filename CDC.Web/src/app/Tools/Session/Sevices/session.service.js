﻿(function () {
    'use strict';

    angular
        .module('app.session')
        .factory('SessionService', SessionService);

    SessionService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function SessionService($http, $q, HOST_URL) {
        return {
            GetSessionListService: GetSessionListService,
            DeleteSessionById:DeleteSessionById
        };

        ////////////////

        function GetSessionListService(model) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.get(HOST_URL.url + '/api/Session/GetSession', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function DeleteSessionById(SessionId) {
            var deferred = $q.defer();
                        $http.post(HOST_URL.url + '/api/Session/DeleteSessionById?SessionId='+SessionId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }
    }
})();