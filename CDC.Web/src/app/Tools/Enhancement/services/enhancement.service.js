﻿(function () {
    'use strict';

    angular
       .module('app.category')
        .factory('EnhancementService', EnhancementService);

    EnhancementService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function EnhancementService($http, $q, HOST_URL) {
        return {
            GetEnhancementListService:GetEnhancementListService,
            DeleteEnhancementById:DeleteEnhancementById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            this.Title='';
            this.Id = data.ID;
            this.Title = data.Title;
            this.EnhancementData=data;
        };

    function GetEnhancementListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Enhancement/GetAllEnhancement', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteEnhancementById(EnhancementId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Enhancement/DeleteEnhancement?enhancementId=' + EnhancementId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


    }
})();