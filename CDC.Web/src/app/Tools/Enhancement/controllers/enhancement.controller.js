(function () {
    'use strict';

    angular
        .module('app.category')
        .controller('enhancementController', enhancementController)
        .controller('AddEnhancementController', AddEnhancementController)
        .controller('UpdateEnhancementController', UpdateEnhancementController);



    /* @ngInject */
    function enhancementController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        EnhancementService, CommonService, filerService,notificationService) {
        var vm = this;
        vm.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.columns = {
            Title: 'Title',
            ID: 'ID',
            EnhancementDate: 'Enhancement Date',
            Description: 'Description',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.enhancementCount = 0;
        vm.selected = [];
        //vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            Title: '',
            EnhancementDate: undefined,
            AgencyId: localStorage.agencyId,
            Description: '',
        };
        localStorage.ParticipantAttendancePage = false;
        // $scope.number = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize;

        vm.GetEnhancementList = GetEnhancementList;
        vm.showAddEnhancementDialoue = showAddEnhancementDialoue;
        vm.showUpdateEnhancementDialoue = showUpdateEnhancementDialoue;
        vm.showDeleteEnhancementConfirmDialoue = showDeleteEnhancementConfirmDialoue;
        vm.reset = reset;
        GetEnhancementList();



        $rootScope.$on("GetEnhancementList", function () {
            GetEnhancementList();
        });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function GetEnhancementList() {
            //console.log(vm.$storage);
            //console.log(localStorage);
            $scope.Title=vm.query.Title==""?"All":vm.query.Title;
            $scope.EnhancementDate=vm.query.EnhancementDate==undefined?"All":vm.query.EnhancementDate;
            vm.promise = EnhancementService.GetEnhancementListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.enhancementList = {};
                        vm.enhancementList = response.Content;
                        vm.enhancementCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.enhancementList = [];
                        vm.enhancementCount = 0;
                        NotificationMessageController('No Enhancement list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get Enhancement list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };


        function showAddEnhancementDialoue(event, data) {
            $mdDialog.show({
                controller: AddEnhancementController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Enhancement/views/addenhancement.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            })
        };

        function showUpdateEnhancementDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateEnhancementController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Enhancement/views/updateenhancement.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { enhancementdetail: data },
                fullscreen: $scope.customFullscreen
            })
        };

        function showDeleteEnhancementConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this enhancement permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteEnhancement(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteEnhancement(data) {
            vm.promise = EnhancementService.DeleteEnhancementById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    notificationService.displaymessage('Enhancement deleted successfully.');
                    GetEnhancementList();
                }
                else {
                    NotificationMessageController('Unable to get Enhancement list at the moment. Please try again after some time.');
                }
            });
        };


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.Title = '';
            vm.query.EnhancementDate = undefined,
                vm.query.Description = '';
            GetEnhancementList();
        };



    }
    //New Controller For Add
    function AddEnhancementController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        EnhancementService, CommonService, HOST_URL) {
        var vm = this;
        vm.enhancement = {
            Title: '',
            ID: 0,
            EnhancementDate: new Date(),
            Description: '',
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        vm.addEnhancement = addEnhancement;
        vm.cancelClick = cancelClick;


        vm.todayDate = new Date();



        function addEnhancement(addEnhancement) {
            if (addEnhancement.$valid) {
                var model = {
                    ID: vm.enhancement.ID,
                    Title: vm.enhancement.Title,
                    AgencyId: localStorage.agencyId,
                    EnhancementDate: vm.enhancement.EnhancementDate,
                    Description: vm.enhancement.Description,
                };

                $http.post(HOST_URL.url + '/api/Enhancement/AddUpdateEnhancement', model).then(function (response) {

                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Enhancement Added Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetEnhancementList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }



        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }


    //New Controller For Update
    function UpdateEnhancementController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        EnhancementService, CommonService, HOST_URL, items) {
        var vm = this;
        vm.enhancement = {
            Title: '',
            ID: 0,
            EnhancementDate: new Date(),
            Description: '',
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };


        GetEnhancementDetailbyId();

        function GetEnhancementDetailbyId() {
            vm.enhancement.Title = items.enhancementdetail.Title;
            vm.enhancement.ID = items.enhancementdetail.ID;
            vm.enhancement.EnhancementDate = items.enhancementdetail.EnhancementDate == null ? new Date() : (new Date(items.enhancementdetail.EnhancementDate));
            vm.enhancement.Description = items.enhancementdetail.Description;
        }
        vm.updateEnhancement = updateEnhancement;
        vm.cancelClick = cancelClick;



        function updateEnhancement(updateEnhancement) {
            if (updateEnhancement.$valid) {
                var model = {
                    ID: vm.enhancement.ID,
                    Title: vm.enhancement.Title,
                    EnhancementDate: vm.enhancement.EnhancementDate,
                    Description: vm.enhancement.Description,
                    AgencyId: localStorage.agencyId,
                    IsDeleted: false,
                };

                $http.post(HOST_URL.url + '/api/Enhancement/AddUpdateEnhancement', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Enhancement updated Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetEnhancementList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }



        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

})();