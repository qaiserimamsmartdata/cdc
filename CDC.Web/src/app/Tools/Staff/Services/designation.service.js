﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('DesignationService', DesignationService);

    DesignationService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function DesignationService($http, $q, HOST_URL) {
        return {
            GetDesignationListService:GetDesignationListService,
            // DeleteTimeClockUserPlanById:DeleteTimeClockUserPlanById,
            // SetId: SetId
        };

    // function SetId(data) {
    //         this.Id=0;
    //         this.Name='';
    //         this.Id = data.ID;
    //         this.Name = data.Name;
    //         this.TimeClockUserPlanData=data;
    //     };

    function GetDesignationListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Designation/GetAllDesignation', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    // function DeleteTimeClockUserPlanById(TimeClockUserPlanId) {
    //         var deferred = $q.defer();
    //         $http.post(HOST_URL.url + '/api/TimeClockUserPlan/DeleteTimeClockUserPlan?timeclockuserplanId=' + TimeClockUserPlanId).success(function (response, status, headers, config) {
    //             deferred.resolve(response);
    //         }).error(function (errResp) {
    //             deferred.reject({ message: "Really bad" });
    //         });
    //         return deferred.promise;
    //     };


    }
})();