(function() {
    'use strict';
    angular
        .module('app.dropdownstaff')
        .config(moduleConfig);
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            
            .state('triangular.toolsStaff', {
                url: '/StaffSetting',
                templateUrl: 'app/Tools/Staff/views/staff.tmpl.html',
                // set the controller to load for this page
                 controller: 'PositionController',
                // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            });
        // triMenuProvider.addMenu({
        //     name: 'Settings',
        //     icon: 'md-cyan-theme material-icons zmdi zmdi-account-box',
        //     type: 'dropdown',
        //     priority: 1.1,
        //     children: [
              
        //          {
        //             name: 'Family',
        //             state: 'triangular.family',
        //             type: 'link'
        //         },
               
        //     ]
        // });
    }
    /* @ngInject */
})();