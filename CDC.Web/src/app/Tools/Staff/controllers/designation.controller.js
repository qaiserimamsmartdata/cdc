(function () {
    'use strict';
    angular
        .module('app.dropdownfamily')
        .controller('PositionController', PositionController)
        .controller('UpdatePositionController', updatePositionController);

    function updatePositionController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage) {

        var vm = this;
        vm.position = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getPositionDetailsbyId() {
            vm.position.Name = items.positiondetail.Name;
            vm.position.ID = items.positiondetail.ID;
        }
        getPositionDetailsbyId();

        function updatePosition(updatePosition) {
            if (updatePosition.$valid) {
                var model = {
                    ID: vm.position.ID,
                    Name: updatePosition.Position.$viewValue,
                    AgencyId: localStorage.agencyId
                };
                apiService.post('/api/Designation/AddUpdateDesignation', model, updatePositionSuccess, UpdateRequestFailed);
            }
        }
        function updatePositionSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Position updated successfully !');
                items.positiondetail.Name = $scope.vm.position.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
                $mdDialog.hide();
            }
        }
        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
        vm.updatePosition = updatePosition;
    }



    function PositionController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage, filerService) {
        $scope.form = {};
        var vm = this;
          $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 
        localStorage.ParticipantAttendancePage = false;

        vm.addPosition = addPosition;
        vm.showProgressbar = false;
        vm.master = {};
        vm.Position = {
            Name: ''
        };
        //vm.GetDesignationList=GetDesignationList;
        vm.GetPositionList = getPositionList;
        vm.showDeletePositionConfirmDialoue = showDeletePositionConfirmDialoue;
        vm.showUpdatePositionDialoue = showUpdatePositionDialoue;
        vm.toggleRight3 = filerService.toggleRight3();
        vm.isOpenRight3 = filerService.isOpenRight3();
        vm.close3 = filerService.close3();
        vm.reset = reset;
        vm.clear = clear;
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.positionCount = 0;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            Name: '',
            AgencyId: localStorage.agencyId
        };
        getPositionList();
        vm.selected = [];
        //vm.getPositionSuccess = getPositionSuccess;
        //vm.RequestFailed = RequestFailed;
        vm.columns = {
            Name: 'Position',
            ID: 'ID'
        };
        // function GetDesignationList() {
        //     vm.promise = DesignationService.GetDesignationListService(vm.query);
        //     vm.promise.then(function (response) {
        //         if (response.IsSuccess == true) {
        //             if (response.Content.length > 0) {
        //                 vm.positionList = {};
        //                 vm.positionList = response.Content;
        //                 vm.positionCount = response.TotalRows;
        //                 vm.selected = [];
        //             }
        //             else {
        //                 vm.positionList = [];
        //                 vm.positionCount = 0;
        //                 NotificationMessageController('No position list found.');
        //             }
        //         }
        //         else {
        //             NotificationMessageController('Unable to get position list at the moment. Please try again after some time.');
        //         }
        //         vm.close();
        //     });
        // };

        function getPositionList() {
            $scope.Position=vm.query.Name==""?"All":vm.query.Name;
            var model = {
                AgencyId: localStorage.agencyId
            };
            //apiService.post('/api/Designation/GetDesignation', model, getPositionSuccess, RequestFailed);
            apiService.post('/api/Designation/GetAllDesignation', vm.query, getPositionSuccess, RequestFailed);
        }

        vm.showProgressbar=true;
        function getPositionSuccess(response) {
            // if (response.data.designation.length > 0) {
            //     vm.positionList = response.data.designation;
            //     vm.positionCount = response.data.designation.length;
            //     vm.selected = [];
            // } else {
            //     if (response.data.designation.length === 0) {
            //         vm.positionList = [];
            //         vm.positionCount = 0;
            //         notificationService.displaymessage('No Position list found.');
            //     } else {
            //         notificationService.displaymessage('Unable to get Position list at the moment. Please try again after some time.');
            //     }
            // }
            if (response.data.Content.length > 0) {
                vm.NoToDoData = false;
                vm.positionList = response.data.Content;
                vm.positionCount = response.data.TotalRows;
                vm.showProgressbar=false;
                vm.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoToDoData = true;
                    vm.positionList = [];
                    vm.positionCount = 0;
                    vm.showProgressbar=false;
                    notificationService.displaymessage('No Position list found.');
                } else {
                    notificationService.displaymessage('Unable to get Position list at the moment. Please try again after some time.');
                }
            }
            vm.close3();


        }

        function addPosition(registerPosition) {
            if (registerPosition.$valid) {
                var model = {
                    Name: registerPosition.Position.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerPosition;
                apiService.post('/api/Designation/AddUpdateDesignation', model, AddPositionSuccess, vm.RequestFailed);
            }
        }

        function AddPositionSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.Position.Name = "";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.Position = angular.copy(vm.master);
                getPositionList();
                notificationService.displaymessage('Position added successfully !');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function deletePosition(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/Designation/DeleteDesignationById', data, DeletePositionSuccess, vm.RequestFailed);
        }

        function DeletePositionSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Position deleted successfully.')
                getPositionList();
            } else {
                notificationService.displaymessage('Unable to get Position list at the moment. Please try again after some time.');
            }
        }

        function reset() {
            vm.query.Name = '';
             $scope.Position="";
            vm.GetPositionList();
        }

        function clear(formName) {
            //vm.formName.$setUntouched();
            //vm.formName.$setPristine();
            vm.Position.Name = '';
            
            // var form = $scope[formName];
            // form.$setUntouched();
            // form.$setPristine();

           $scope.form.registerPosition.$setUntouched();
            $scope.form.registerPosition.$setPristine();
            console.log('ok');

        }


        function showDeletePositionConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this Position permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deletePosition(data);
            }, function () {
                $scope.hide();
            });
        }

        function showUpdatePositionDialoue(event, data) {
            $mdDialog.show({
                controller: updatePositionController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Staff/views/updatedesignation.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { positiondetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
    }
    /* @ngInject */
})();