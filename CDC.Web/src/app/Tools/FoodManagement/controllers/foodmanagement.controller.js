(function () {
    'use strict';

    angular
        .module('app.category')
        .controller('FoodManagementMasterController', FoodManagementMasterController)
        .controller('UpdateFoodController', UpdateFoodController);

    /* @ngInject */
    function FoodManagementMasterController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        FoodMasterService, CommonService, $filter, filerService, HOST_URL) {
        var vm = this;
        vm.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.foodMasterListCount = 0;
        vm.columns = {
            Age: 'Age group',
            ID: 'ID',
            Meal: 'Meal type',
            Item: 'Meal Item',
            Serving: 'Serving size'
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.holidayCount = 0;
        vm.selected = [];
        //vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            MinAgeFrom: '',
            MinAgeTo: '',
            MaxAgeFrom: '',
            MaxAgeTo: '',
            MealTypeId: '',
            ItemName: '',
            AgencyId: localStorage.agencyId,
            ServingSizeId: '',
            ServingSizeTitleId: '',
            ServingSizeProportionId: ''
        };
        localStorage.ParticipantAttendancePage = false;
        // $scope.number = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize;
        vm.AllAgeYears = CommonService.getYears();
        vm.AllAgeMonths = CommonService.getMonths();
        getMealTypeData();
        getMealItemData();
        getMealServeSizeData();
        getMealServeQuantityData();
        getMealServeTypesData();
        function getMealTypeData() {
            vm.AllMealTypes = null; // Clear previously loaded state list
            var myPromise = CommonService.getMealTypeData();
            myPromise.then(function (resolve) {
                vm.AllMealTypes = null;
                vm.AllMealTypes = eval(resolve.Content);
            }, function (reject) {

            });

        };

        function getMealServeSizeData() {
            vm.AllMealServeSizes = null; // Clear previously loaded state list
            var myPromise1 = CommonService.getMealServeSizeData({ AgencyId: localStorage.agencyId });
            myPromise1.then(function (resolve) {
                vm.AllMealServeSizes = null;
                vm.AllMealServeSizes = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function getMealServeQuantityData() {
            vm.AllMealServeQuantity = null; // Clear previously loaded state list
            var myPromise2 = CommonService.getMealServeQuantityData({ AgencyId: localStorage.agencyId });
            myPromise2.then(function (resolve1) {
                vm.AllMealServeQuantity = null;
                vm.AllMealServeQuantity = eval(resolve1.Content);
            }, function (reject) {

            });

        };
        function getMealServeTypesData() {
            vm.AllMealServeTypes = null; // Clear previously loaded state list
            var myPromise3 = CommonService.getMealServeTypesData({ AgencyId: localStorage.agencyId });
            myPromise3.then(function (resolve2) {
                vm.AllMealServeTypes = null;
                vm.AllMealServeTypes = eval(resolve2.Content);
            }, function (reject) {

            });

        };

        function getMealItemData() {
            vm.AllMealItems = null;// Clear previously loaded state list
            var myPromise = CommonService.getMealItemData({ AgencyId: localStorage.agencyId });
            myPromise.then(function (resolve) {
                vm.AllMealItems = null;
                vm.AllMealItems = eval(resolve.Content);
            }, function (reject) {

            });

        };
        vm.GetFoodMasterList = GetFoodMasterList;

        // vm.showAddHolidayDialoue = showAddHolidayDialoue;
        // vm.showUpdateHolidayDialoue = showUpdateHolidayDialoue;
        vm.showDeleteFoodDetailsConfirmDialoue = showDeleteFoodDetailsConfirmDialoue;
        vm.AddFoodMaster = AddFoodMaster;
        vm.reset = reset;
        GetFoodMasterList();


        $rootScope.$on("GetFoodMasterList", function () {
            GetFoodMasterList();
        });
        // $rootScope.$on("GetHolidayList", function () {
        //     GetHolidayList();
        // });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        // var ServingSizeProportion = [
        //     { ID: 0, name: " " },
        //     { ID: 1, name: "1/10" },
        //     { ID: 2, name: "1/8" },
        //     { ID: 3, name: "1/7" },
        //     { ID: 4, name: "1/6" },
        //     { ID: 5, name: "1/5" },
        //     { ID: 6, name: "1/4" },
        //     { ID: 7, name: "1/3" },
        //     { ID: 8, name: "1/2" },
        //     { ID: 9, name: "2/3" },
        //     { ID: 10, name: "3/4" },
        //     { ID: 11, name: "7/8" }
        // ]
        // var ServingSizeTitle = [
        //     { ID: 0, name: "cl" },
        //     { ID: 1, name: "cups" },
        //     { ID: 2, name: "fl ozes" },
        //     { ID: 3, name: "grams" },
        //     { ID: 4, name: "kgs" },
        //     { ID: 5, name: "lbs" },
        //     { ID: 6, name: "litres" },
        //     { ID: 7, name: "mls" },
        //     { ID: 8, name: "oz" },
        //     { ID: 9, name: "pints" },
        //     { ID: 10, name: "quats" },
        //     { ID: 11, name: "servings" },
        //     { ID: 12, name: "tbsp" },
        //     { ID: 13, name: "tsps" }
        // ]
        function GetFoodMasterList() {
            var MealItem='';
            if(vm.query.ItemName!="")
            {
            var MealData = $filter('filter')(vm.AllMealItems, { ID: parseInt(vm.query.ItemName)}, true);
             MealItem=MealData[0].Name;
            }
            $scope.MealItem = MealItem == "" ? "All" : MealItem;

            $scope.MinAgeFrom = vm.query.MinAgeFrom == "" ? "All" : vm.query.MinAgeFrom;
            $scope.MinAgeTo = vm.query.MinAgeTo == "" ? "All" : vm.query.MinAgeTo;
            $scope.MaxAgeFrom = vm.query.MaxAgeFrom == "" ? "All" : vm.query.MaxAgeFrom;
            $scope.MaxAgeTo = vm.query.MaxAgeTo == "" ? "All" : vm.query.MaxAgeTo;
            $scope.MealTypeId = vm.query.MealTypeId == "" ? "All" : vm.query.MealTypeId;
            if ($scope.MinAgeFrom == "All" && $scope.MinAgeTo == "All" && $scope.MaxAgeFrom == "All" && $scope.MaxAgeTo == "All") {
                $scope.Agegroup = "All";
            }
            else {
                $scope.Agegroup = $scope.MinAgeFrom + " years " + $scope.MinAgeTo + " months - " + $scope.MaxAgeFrom + " years " + $scope.MaxAgeTo + " months";
            }
            if ($scope.MealTypeId == 1) {
                $scope.MealName = "Breakfast";
            }
            else if ($scope.MealTypeId == 2) {
                $scope.MealName = "Lunch";
            }
            else if ($scope.MealTypeId == 3) {
                $scope.MealName = "Snacks";
            }
            else if ($scope.MealTypeId == 4) {
                $scope.MealName = "Dinner";
            }
            else {
                $scope.MealName = "All";
            }
            if ($scope.ItemNameId == 1) {
                $scope.ItemName = "Milk"
            }
            else if ($scope.ItemNameId == 2) {
                $scope.ItemName = "Vegetable"
            }
            else if ($scope.ItemNameId == 3) {
                $scope.ItemName = "Fruit"
            }
            else if ($scope.ItemNameId == 4) {
                $scope.ItemName = "Yoghurt"
            }
            else if ($scope.ItemNameId == 5) {
                $scope.ItemName = "Cheese"
            }
            else if ($scope.ItemNameId == 6) {
                $scope.ItemName = "Bread slice"
            }
            else if ($scope.ItemNameId == 7) {
                $scope.ItemName = "Meat"
            }
            else if ($scope.ItemNameId == 8) {
                $scope.ItemName = "Fish"
            }
            else if ($scope.ItemNameId == 9) {
                $scope.ItemName = "Egg"
            }
            else if ($scope.ItemNameId == 10) {
                $scope.ItemName = "Cooked by dry beans"
            }
            else if ($scope.ItemNameId == 11) {
                $scope.ItemName = "Cooked dry peas"
            }
            else {
                $scope.ItemName = "All";
            }


            vm.MaxAge = MaxAge;
            vm.MinAge = MinAge;
            vm.MaxMonth = MaxMonth;
            function MaxMonth(data) {
                if (vm.food.MaxAgeFrom != vm.food.MinAgeFrom)
                    return;
                else {
                    if (parseInt(data) < parseInt(vm.food.MinAgeTo)) {
                        vm.food.MaxAgeTo = "";
                        NotificationMessageController("Maximum age should be greater than minimum age.")
                    }
                }

            }
            vm.MinMon = MinMon;
            function MinMon(data) {
                if (
                    vm.food.MaxAgeFrom != vm.food.MinAgeFrom)
                    return
                else {

                    if (parseInt(data) > parseInt(vm.food.MaxAgeTo)) {
                        vm.food.MinAgeTo = "";
                        NotificationMessageController("Minimum age should be less than maximum age.")
                    }
                }
            }
            function MaxAge(data) {
                debugger
                var maxAge = parseInt(data);
                var minAge = parseInt(vm.food.MinAgeFrom);
                if (maxAge < minAge) {
                    vm.food.MaxAgeFrom = "";
                    NotificationMessageController("Maximum age should be greater than minimum age.");
                }
            }
            function MinAge(data) {
                debugger
                var maxAge = parseInt(vm.food.MaxAgeFrom);
                var minAge = parseInt(data);
                if (minAge > maxAge) {
                    vm.food.MinAgeFrom = "";
                    NotificationMessageController("Minimum age should be less than maximum age.");
                }
            }
            vm.promise = FoodMasterService.GetFoodMasterListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.foodMasterList = {};
                        vm.foodMasterList = response.Content;

                        vm.foodMasterList.forEach(function (food) {
                            vm.AllMealServeSizes.forEach(function (value) {
                                if (food.ServingSizeId == value.ID) {

                                    food.ServingSizeName = value.Name;
                                }
                            });

                        })
                        vm.foodMasterList.forEach(function (food) {
                            vm.AllMealServeQuantity.forEach(function (value) {
                                if (food.ServingSizeProportionId == value.ID) {

                                    food.ServingSizeProportionName = value.Name;
                                }
                            });

                        })
                        vm.foodMasterList.forEach(function (food) {
                            vm.AllMealServeQuantity.forEach(function (value) {
                                if (food.ServingSizeProportionId == value.ID) {

                                    food.ServingSizeProportionName = value.Name;
                                }
                            });

                        })
                        vm.foodMasterList.forEach(function (food) {
                            vm.AllMealServeTypes.forEach(function (value) {
                                if (food.ServingSizeTitleId == value.ID) {

                                    food.ServingSizeTitleName = value.Name;
                                }
                            });

                        })
                        vm.foodMasterList.forEach(function (element) {
                            var OtherData = $filter('filter')(vm.AllAgeYears, { ID: element.MinAgeFrom }, true);
                            element.MinAgeFromName = OtherData[0].Name;
                            var OtherData1 = $filter('filter')(vm.AllAgeMonths, { ID: element.MinAgeTo }, true);
                            element.MinAgeToName = OtherData1[0].Name;
                            var OtherData3 = $filter('filter')(vm.AllAgeYears, { ID: element.MaxAgeFrom }, true);
                            element.MaxAgeFromName = OtherData3[0].Name;
                            var OtherData4 = $filter('filter')(vm.AllAgeMonths, { ID: element.MaxAgeTo }, true);
                            element.MaxAgeToName = OtherData4[0].Name;
                            element.Agegroup = element.MinAgeFromName + ' ' + element.MinAgeToName + ' - ' + element.MaxAgeFromName + ' ' + element.MaxAgeToName;

                        }, this);


                        vm.foodMasterCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.foodMasterList = [];
                        vm.foodMasterCount = 0;
                        NotificationMessageController('No Food details list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get food master list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };
        vm.showUpdateFoodDetailsConfirmDialoue = showUpdateFoodDetailsConfirmDialoue;
        function showUpdateFoodDetailsConfirmDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateFoodController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/FoodManagement/views/updatefood.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { foodDetail: data },
                fullscreen: $scope.customFullscreen
            })
        };
        //model.$setPristine();

        function AddFoodMaster(registerfood, model) {
            // model.StaffLocationList = vm.selectedUser;
            model.AgencyId = localStorage.agencyId;
            $http.post(HOST_URL.url + '/api/foodManagementMaster/AddFoodMaster', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    vm.food = {};
                    registerfood.$setPristine();
                    registerfood.$setUntouched();
                    NotificationMessageController(response.data.ReturnMessage[0]);
                    // ResetFields();
                    // $state.go('triangular.staff');
                    GetFoodMasterList();
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        function UpdateFoodMaster(model) {
            // model.StaffLocationList = vm.selectedUser;

            $http.post(HOST_URL.url + '/api/foodManagementMaster/UpdateFoodMaster', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    NotificationMessageController(response.data.ReturnMessage[0]);
                    // ResetFields();
                    // $state.go('triangular.staff');
                    GetFoodMasterList();
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }

        function showDeleteFoodDetailsConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this food details permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteFoodDetails(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteFoodDetails(data) {
            vm.promise = FoodMasterService.DeleteFoodMasterById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Food details deleted successfully.');
                    GetFoodMasterList();
                }
                else {
                    NotificationMessageController('Unable to get food master details list at the moment. Please try again after some time.');
                }
            });
        };


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.ItemName = '';
            vm.query.MinAgeFrom = '',
                vm.query.MinAgeTo = '';
            vm.query.MaxAgeFrom = '';
            vm.query.MaxAgeTo = '';
            vm.query.MealTypeId = '';
            vm.query.ItemName = '';
            GetFoodMasterList();
        };
    }
    function UpdateFoodController($scope, $http, FoodMasterService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        HolidayService, CommonService, filerService, HOST_URL, items) {
        var vm = this;
        vm.foodupdate = items.foodDetail;
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        vm.close = filerService.close();

        localStorage.ParticipantAttendancePage = false;
        vm.AllAgeYears = CommonService.getYears();
        vm.AllAgeMonths = CommonService.getMonths();
        getMealTypeData();
        getMealItemData();
        getMealServeSizeData();
        getMealServeQuantityData();
        getMealServeTypesData();
        function getMealTypeData() {
            vm.AllMealTypes = null; // Clear previously loaded state list
            var myPromise = CommonService.getMealTypeData();
            myPromise.then(function (resolve) {
                vm.AllMealTypes = null;
                vm.AllMealTypes = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function getMealItemData() {
            vm.AllMealItems = null;// Clear previously loaded state list
            var myPromise = CommonService.getMealItemData({ AgencyId: localStorage.agencyId });
            myPromise.then(function (resolve) {
                vm.AllMealItems = null;
                vm.AllMealItems = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function getMealServeSizeData() {
            vm.AllMealServeSizes = null; // Clear previously loaded state list
            var myPromise1 = CommonService.getMealServeSizeData({ AgencyId: localStorage.agencyId });
            myPromise1.then(function (resolve) {
                vm.AllMealServeSizes = null;
                vm.AllMealServeSizes = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function getMealServeQuantityData() {
            vm.AllMealServeQuantity = null; // Clear previously loaded state list
            var myPromise2 = CommonService.getMealServeQuantityData({ AgencyId: localStorage.agencyId });
            myPromise2.then(function (resolve1) {
                vm.AllMealServeQuantity = null;
                vm.AllMealServeQuantity = eval(resolve1.Content);
            }, function (reject) {

            });

        };
        function getMealServeTypesData() {
            vm.AllMealServeTypes = null; // Clear previously loaded state list
            var myPromise3 = CommonService.getMealServeTypesData({ AgencyId: localStorage.agencyId });
            myPromise3.then(function (resolve2) {
                vm.AllMealServeTypes = null;
                vm.AllMealServeTypes = eval(resolve2.Content);
            }, function (reject) {

            });

        };
        vm.GetFoodMasterList = GetFoodMasterList;
        function GetFoodMasterList() {
            //console.log(vm.$storage);
            //console.log(localStorage);
            // $scope.Title=vm.query.Title==""?"All":vm.query.Title;
            // $scope.HolidayDate=vm.query.HolidayDate==undefined?"All":vm.query.HolidayDate;
            vm.promise = FoodMasterService.GetFoodMasterListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.foodMasterList = {};
                        vm.foodMasterList = response.Content;
                        vm.foodMasterCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.foodMasterList = [];
                        vm.foodMasterCount = 0;
                        NotificationMessageController('No Food details list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get food master list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };
        GetFoodDetailbyId();
        vm.MaxAge = MaxAge;
        vm.MinAge = MinAge;
        vm.MaxMonth = MaxMonth;
        function MaxMonth(data) {
            // if (vm.foodupdate.MaxAgeFrom != vm.foodupdate.MinAgeFrom)
            //     return;
            // else {
            if (parseInt(data) < parseInt(vm.foodupdate.MinAgeTo)) {
                vm.foodupdate.MaxAgeTo = "";
                NotificationMessageController("Maximum age should be greater than minimum age.")
            }
            //}

        }
        vm.MinMon = MinMon;
        function MinMon(data) {

            if (parseInt(data) > parseInt(vm.foodupdate.MaxAgeTo)) {
                vm.foodupdate.MinAgeTo = "";
                NotificationMessageController("Minimum age should be less than maximum age.")
            }

        }
        function MaxAge(data) {
            debugger
            var maxAge = parseInt(data);
            var minAge = parseInt(vm.foodupdate.MinAgeFrom);
            if (maxAge < minAge) {
                vm.foodupdate.MaxAgeFrom = "";
                NotificationMessageController("Maximum age should be greater than minimum age.");
            }
        }
        function MinAge(data) {
            debugger
            var maxAge = parseInt(vm.foodupdate.MaxAgeFrom);
            var minAge = parseInt(data);
            if (minAge > maxAge) {
                vm.foodupdate.MinAgeFrom = "";
                NotificationMessageController("Minimum age should be less than maximum age.");
            }
        }
        function GetFoodDetailbyId() {
            vm.foodupdate.MinAgeFrom = items.foodDetail.MinAgeFrom.toString();
            vm.foodupdate.MinAgeTo = items.foodDetail.MinAgeTo.toString();
            vm.foodupdate.MaxAgeFrom = items.foodDetail.MaxAgeFrom.toString();
            vm.foodupdate.MaxAgeTo = items.foodDetail.MaxAgeTo.toString();
            vm.foodupdate.MealTypeId = items.foodDetail.MealTypeId.toString();
            vm.foodupdate.MealItemId = items.foodDetail.MealItemId.toString();
            vm.foodupdate.ServingSizeId = items.foodDetail.ServingSizeId.toString();
            vm.foodupdate.ServingSizeProportionId = items.foodDetail.ServingSizeProportionId.toString();
            vm.foodupdate.ServingSizeTitleId = items.foodDetail.ServingSizeTitleId.toString();




        }
        vm.updateFoodData = updateFoodData;
        vm.cancelClick = cancelClick;



        function updateFoodData(model) {
            // if (updateFood.$valid) {
            //     var model = {
            //         AgencyId: localStorage.agencyId,
            //         IsDeleted: false,
            //         ID:vm.foodDetail.ID,
            //         MinAgeFrom: vm.foodupdate.MinAgeFrom,
            //         MinAgeTo: vm.foodupdate.MinAgeTo,
            //         MaxAgeFrom: vm.foodupdate.MaxAgeFrom,
            //         MaxAgeTo: vm.foodupdate.MaxAgeTo,
            //         MealType:vm.foodupdate.MealType,
            //         ItemName:vm.foodupdate.ItemName,
            //         ServingSizeId: vm.foodupdate.ServingSizeId,
            //         ServingSizeProportionId: vm.foodupdate.ServingSizeProportionId,
            //         ServingSizeTitleId: vm.foodupdate.ServingSizeTitleId

            //     };
            model.AgencyId = localStorage.agencyId;

            $http.post(HOST_URL.url + '/api/foodManagementMaster/UpdateFoodMaster', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    NotificationMessageController('Food details updated Successfully.');
                    cancelClick();

                    $rootScope.$emit("GetFoodMasterList", {});
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                    $mdDialog.hide();
                }
            });
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

})();