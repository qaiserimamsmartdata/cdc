angular
    .module('app')
    .directive('ngTitle', ['$http', 'HOST_URL','$localStorage', function (async, HOST_URL,$localStorage) {
        return {
            
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                elem.on('change', function (evt) {
                    if (attrs.ngTitle == 'Title') {
                        scope.$apply(function () {
                            
                            var val = elem.val();
                            
                            var req = { "name": val,"agencyId": localStorage.agencyId}
                            var ajaxConfiguration = { method: 'POST', url: HOST_URL.url + 'api/Common/isexistTitle', data: req };
                            async(ajaxConfiguration)
                                .success(function (data, status, headers, config) {
                                    ctrl.$setValidity('title', !data);
                                });
                        });
                    }                   
                });
            }
        }
    }]);