﻿(function () {
    'use strict';

    angular
       .module('app.category')
        .factory('FoodMasterService', FoodMasterService);

    FoodMasterService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function FoodMasterService($http, $q, HOST_URL) {
        return {
            GetFoodMasterListService:GetFoodMasterListService,
            DeleteFoodMasterById:DeleteFoodMasterById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            this.Title='';
            this.Id = data.ID;
            this.Title = data.Title;
            this.HolidayData=data;
        };
        function GetHolidayWithoutLimit(model){
            

        }
    function GetFoodMasterListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/foodManagementMaster/GetAllFoodManagementMaster', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteFoodMasterById(foodMasterId) {
            var deferred = $q.defer();
            var ID=foodMasterId.ID;
            
            $http.post(HOST_URL.url + '/api/foodManagementMaster/DeleteFoodMasterById?foodMasterId=' + ID).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


    }
})();