﻿(function () {
    'use strict';

    angular
       .module('app.category')
        .factory('HolidayService', HolidayService);

    HolidayService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function HolidayService($http, $q, HOST_URL) {
        return {
            GetHolidayWithoutLimit:GetHolidayWithoutLimit,
            GetHolidayListService:GetHolidayListService,
            DeleteHolidayById:DeleteHolidayById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            this.Title='';
            this.Id = data.ID;
            this.Title = data.Title;
            this.HolidayData=data;
        };
        function GetHolidayWithoutLimit(model){
            

        }
    function GetHolidayListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Holiday/GetAllHoliday', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteHolidayById(HolidayId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Holiday/DeleteHoliday?holidayId=' + HolidayId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


    }
})();