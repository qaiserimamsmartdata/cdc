(function () {
    'use strict';

    angular
        .module('app.category')
        .controller('holidayController', holidayController)
        .controller('AddHolidayController', AddHolidayController)
        .controller('UpdateHolidayController', UpdateHolidayController);



    /* @ngInject */
    function holidayController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        HolidayService, CommonService, filerService) {
        var vm = this;
        vm.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.columns = {
            Title: 'Title',
            ID: 'ID',
            HolidayDate: 'Holiday Date',
            Description: 'Description',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.holidayCount = 0;
        vm.selected = [];
        localStorage.ParticipantAttendancePage = false;
        //vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            Title: '',
            HolidayDate: undefined,
            AgencyId: localStorage.agencyId,
            Description: '',
        };

        // $scope.number = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize;

        vm.GetHolidayList = GetHolidayList;
      
        vm.showAddHolidayDialoue = showAddHolidayDialoue;
        vm.showUpdateHolidayDialoue = showUpdateHolidayDialoue;
        vm.showDeleteHolidayConfirmDialoue = showDeleteHolidayConfirmDialoue;
        vm.reset = reset;
        GetHolidayList();



        $rootScope.$on("GetHolidayList", function () {
            GetHolidayList();
        });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function GetHolidayList() {
            //console.log(vm.$storage);
            //console.log(localStorage);
            $scope.Title=vm.query.Title==""?"All":vm.query.Title;
            $scope.HolidayDate=vm.query.HolidayDate==undefined?"All":vm.query.HolidayDate;
            vm.promise = HolidayService.GetHolidayListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.holidayList = {};
                        vm.holidayList = response.Content;
                        vm.holidayCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.holidayList = [];
                        vm.holidayCount = 0;
                        NotificationMessageController('No Holiday list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get Holiday list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };

     
        function showAddHolidayDialoue(event, data) {
            $mdDialog.show({
                controller: AddHolidayController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Holiday/views/addholiday.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            })
        };

        function showUpdateHolidayDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateHolidayController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Holiday/views/updateholiday.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { holidaydetail: data },
                fullscreen: $scope.customFullscreen
            })
        };

        function showDeleteHolidayConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this holiday permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteHoliday(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteHoliday(data) {
            vm.promise = HolidayService.DeleteHolidayById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Holiday deleted successfully.')
                    GetHolidayList();
                }
                else {
                    NotificationMessageController('Unable to get Holiday list at the moment. Please try again after some time.');
                }
            });
        };


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.Title = '';
            vm.query.HolidayDate = null,
                vm.query.Description = '';
                $scope.Title="";
                $scope.HolidayDate=undefined;
            GetHolidayList();
        };



    }
    //New Controller For Add
    function AddHolidayController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        HolidayService, CommonService, HOST_URL) {
        var vm = this;
        vm.holiday = {
            Title: '',
            ID: 0,
            HolidayDate: new Date(),
            Description: '',
        };
      
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
       
        vm.addHoliday = addHoliday;
        vm.cancelClick = cancelClick;
        vm.holiday.HolidayDate=null;
          vm.checkTitle=checkTitle;
        vm.todayDate = new Date();


        
        
           function checkTitle(model){
               var req = { "name": model,"agencyId": localStorage.agencyId}
           var deferred = $q.defer();
            $http.post(HOST_URL.url + 'api/Common/isexistTitle', req).success(function (response, status, headers, config) {
                deferred.resolve(response);
                if(response==true)
                {
               vm.holiday.Title=null;
            NotificationMessageController('Holiday Name already exists.');
           
                }
             }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }

    

        function addHoliday(addHoliday) {
            if (addHoliday.$valid) {
                var model = {
                    ID: vm.holiday.ID,
                    Title: vm.holiday.Title,
                    AgencyId: localStorage.agencyId,
                    HolidayDate: vm.holiday.HolidayDate,
                    Description: vm.holiday.Description,
                };

                $http.post(HOST_URL.url + '/api/Holiday/AddUpdateHoliday', model).then(function (response) {

                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Holiday Added Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetHolidayList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }



        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }


    //New Controller For Update
    function UpdateHolidayController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        HolidayService, CommonService, HOST_URL, items) {
        var vm = this;
        vm.holiday = {
            Title: '',
            ID: 0,
            HolidayDate: new Date(),
            Description: '',
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };


        GetHolidayDetailbyId();

        function GetHolidayDetailbyId() {
            vm.holiday.Title = items.holidaydetail.Title;
            vm.holiday.ID = items.holidaydetail.ID;
            vm.holiday.HolidayDate = items.holidaydetail.HolidayDate == null ? new Date() : (new Date(items.holidaydetail.HolidayDate));
            vm.holiday.Description = items.holidaydetail.Description;
        }
        vm.updateHoliday = updateHoliday;
        vm.cancelClick = cancelClick;



        function updateHoliday(updateHoliday) {
            if (updateHoliday.$valid) {
                var model = {
                    ID: vm.holiday.ID,
                    Title: vm.holiday.Title,
                    HolidayDate: vm.holiday.HolidayDate,
                    Description: vm.holiday.Description,
                    AgencyId: localStorage.agencyId,
                    IsDeleted: false,
                };

                $http.post(HOST_URL.url + '/api/Holiday/AddUpdateHoliday', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Holiday updated Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetHolidayList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }



        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

})();