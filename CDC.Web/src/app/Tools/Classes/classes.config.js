(function () {
    'use strict';
    angular
        .module('app.dropdownclass')
        .config(moduleConfig);
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.classes', {
                url: '/class',
                templateUrl: 'app/Tools/Classes/Views/classes.tmpl.html',
                // set the controller to load for this page
                // controller: 'CategoryController',
                // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            });
    }
    /* @ngInject */
})();