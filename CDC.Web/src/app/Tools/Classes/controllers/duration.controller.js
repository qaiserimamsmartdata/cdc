(function () {
    'use strict';

    function updateDurationController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q,HOST_URL, items, notificationService, apiService) {

        var vm = this;
         $scope.$storage = $sessionStorage;
      //   $scope.$storage = $sessionStorage;
         if($sessionStorage.agencyId == undefined || null)
        {
            $state.go('authentication.login');
            notificationService.displaymessage("You must login first.");
            return;
        } 
        vm.duration = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getDurationDetailsbyId() {
            /// <summary>
            /// Gets the duration detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.duration.Name = items.durationdetail.Name;
            vm.duration.ID = items.durationdetail.ID;
        }

        getDurationDetailsbyId();

        function updateDuration(updateDuration) {
            if (updateDuration.$valid) {
                var model = {
                    ID: vm.duration.ID,
                    Name: updateDuration.Duration.$viewValue
                };

                apiService.post('/api/Duration/AddUpdateDuration', model, updateDurationSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateDurationSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Duration updated Successfully.');
                items.durationdetail.Name = $scope.vm.duration.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                $mdDialog.hide();
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateDuration = updateDuration;
    }

    function DurationController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService) {
        var vm = this;
        $scope.selected = [];
        vm.columns = {
            Name: 'Duration',
            ID: 'ID'
        };

        function getDurationList() {

            apiService.post('/api/Duration/GetDurationList', null, getDurationSuccess,
                RequestFailed);
        }

        function getDurationSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoData = false;
                vm.durationList = response.data.Content;
                vm.durationCount = response.data.Content.length;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoData = true;
                    vm.durationList = [];
                    vm.durationCount = 0;
                    notificationService.displaymessage('No Duration list found.');
                } else {
                    notificationService.displaymessage('Unable to get duration list at the moment. Please try again after some time.');
                }
            }
        }

        function addDuration(registerDuration) {
            if (registerDuration.$valid) {
                var model = {
                    Name: registerDuration.Duration.$modelValue
                };

                apiService.post('/api/Duration/AddUpdateDuration', model, AddDurationSuccess,
                    RequestFailed);

            }
        }

        function AddDurationSuccess(response) {
            if (response.data.Content != null) {
                vm.showProgressbar = false;
                //angular.element(document.getElementsByName("registerDuration")).$setUntouched();
                //angular.element(document.getElementsByName("registerDuration")).$setUntouched();
                vm.Duration = angular.copy(vm.master);

                getDurationList();
                notificationService.displaymessage('Duration added Successfully.');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
            }
        }

        vm.addDuration = addDuration;
        vm.showProgressbar = false;
        vm.master = {};
        vm.Duration = {
            Name: ''
        };
        vm.GetDurationList = getDurationList;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.durationCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: ''
        };
        getDurationList();

        function deleteDuration(data) {
            apiService.post('/api/Duration/DeleteDuration', data, DeleteDurationSuccess,
                RequestFailed);
        }

        function DeleteDurationSuccess(response) {
            if (response.data.IsSuccess === true) {
                getDurationList();
            } else {

                notificationService.displaymessage('Unable to get duration list at the moment. Please try again after some time.');
            }
        }

        function showDeleteDurationConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are You Sure to delete this duration permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteDuration(data);
            }, function () {
                $scope.hide();
            });
        }

        vm.showDeleteDurationConfirmDialoue = showDeleteDurationConfirmDialoue;

        function showUpdateDurationDialoue(event, data) {
            $mdDialog.show({
                controller: updateDurationController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Family/Views/updateduration.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { durationdetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.showUpdateDurationDialoue = showUpdateDurationDialoue;
    }

    angular
        .module('app.dropdownclass')
        .controller('DurationController', DurationController)
        .controller('UpdateDurationController', updateDurationController);
    /* @ngInject */
})();