(function () {
    'use strict';
    angular
        .module('app.dropdownclass')
        .controller('SessionController', SessionController)
        .controller('UpdateSessionController', updateSessionController);

    function updateSessionController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage) {

        var vm = this;
        $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        vm.session = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getSessionDetailsbyId() {
            /// <summary>
            /// Gets the session detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.session.Name = items.sessiondetail.Name;
            vm.session.ID = items.sessiondetail.ID;
        }

        getSessionDetailsbyId();

        function updateSession(updateSession) {
            if (updateSession.$valid) {
                var model = {
                    ID: vm.session.ID,
                    Name: updateSession.Session.$viewValue,
                    AgencyId: localStorage.agencyId
                };

                apiService.post('/api/Session/AddUpdateSession', model, updateSessionSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateSessionSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Session updated successfully !');
                items.sessiondetail.Name = $scope.vm.session.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                $mdDialog.hide();
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateSession = updateSession;
    }

    function SessionController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage, filerService) {
        var vm = this;
        vm.addSession = addSession;

        vm.showProgressbar = false;
        vm.master = {};
        vm.Session = {
            Name: ''
        };
        vm.GetSessionList = getSessionList;
        vm.showDeleteSessionConfirmDialoue = showDeleteSessionConfirmDialoue;
        vm.showUpdateSessionDialoue = showUpdateSessionDialoue;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.sessionCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId
        };
        getSessionList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Session',
            ID: 'ID'
        };

        function getSessionList() {
            // var model = {
            //     AgencyId: localStorage.agencyId
            // };
            // apiService.post('/api/Session/GetSession', $scope.query, getSessionSuccess,
            //     RequestFailed);

            apiService.post('/api/Session/GetAllSession', $scope.query, getSessionSuccess,
                RequestFailed);
        }

        vm.showProgressbar = true;
        function getSessionSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.sessionList = response.data.Content;
                vm.sessionCount = response.data.TotalRows;
                vm.showProgressbar = false;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.sessionList = [];
                    vm.sessionCount = 0;
                    vm.showProgressbar = false;
                    notificationService.displaymessage('No Session list found.');
                } else {
                    notificationService.displaymessage('Unable to get session list at the moment. Please try again after some time.');
                }
            }
        }
        //        function getSessionSuccess(response) {
        //     if (response.data.session.length > 0) {
        //         vm.sessionList = response.data.session;
        //         vm.sessionCount = response.data.session.length;
        //         $scope.selected = [];
        //     } else {
        //         if (response.data.session.length === 0) {
        //             vm.sessionList = [];
        //             vm.sessionCount = 0;
        //             notificationService.displaymessage('No session list found.');
        //         } else {
        //             notificationService.displaymessage('Unable to get session list at the moment. Please try again after some time.');
        //         }
        //     }
        // }

        function addSession(registerSession) {
            if (registerSession.$valid) {
                var model = {
                    Name: registerSession.Session.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerSession;
                apiService.post('/api/Session/AddUpdateSession', model, AddSessionSuccess,
                    RequestFailed);
            }
        }

        function AddSessionSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.Session = angular.copy(vm.master);
                getSessionList();
                notificationService.displaymessage('Session name added successfully !');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage('Session name you are trying to add already exist.');
            }
        }

        function deleteSession(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/Session/DeleteSessionById', data, DeleteSessionSuccess,
                RequestFailed);
        }

        function DeleteSessionSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Session deleted successfully.')
                getSessionList();
            } else {
                notificationService.displaymessage('Unable to get session list at the moment. Please try again after some time.');
            }
        }

        function showDeleteSessionConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this session permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteSession(data);
            }, function () {
                $scope.hide();
            });
        }

        function showUpdateSessionDialoue(event, data) {
            $mdDialog.show({
                controller: updateSessionController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Classes/Views/updatesession.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { sessiondetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function reset() {
            $scope.query.name = '';
            getSessionList();
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please try again")
        }
    }
    /* @ngInject */
})();