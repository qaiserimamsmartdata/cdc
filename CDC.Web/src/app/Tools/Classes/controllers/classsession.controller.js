(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('ClassSessionController', ClassSessionController)
        .controller('UpdateClassSessionController', UpdateClassSessionController);

    /* @ngInject */

    function ClassSessionController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, SessionService, HOST_URL, $localStorage, notificationService, apiService, filerService) {
        var vm = this;
        // $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        $scope.Session = {
            frommonth: '',
            fromyear: '',
            tomonth: '',
            toyear: ''
        };
        $scope.Session1 = {
            frommonth: '',
            fromyear: '',
            tomonth: '',
            toyear: ''

        };
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId
        };
        vm.master = {};
        vm.toggleRight2 = filerService.toggleRight2();
        vm.isOpenRight2 = filerService.isOpenRight2();
        vm.close2 = filerService.close2();
        vm.reset = reset;
        vm.monthValidation = false;
        vm.ValidateSession = ValidateSession;
        vm.GetSearchedSessionList = GetSearchedSessionList;
        vm.ValidateSearchSession = ValidateSearchSession;
        vm.showDeleteSessionConfirmDialoue = showDeleteSessionConfirmDialoue;
        vm.showUpdateSessionDialoue = showUpdateSessionDialoue;
        GetSessionList();
        vm.addSession = addSession;
        vm.showProgressbar = false;
        $scope.selected = [];
        $scope.Months = [
            { id: 1, name: "January" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" }];

        var date = new Date();
        vm.currentYear = date.getFullYear();
        vm.currentMonth = date.getMonth() + 1;
        vm.columns = {
            Name: 'Session Name',
            ID: 'ID'
        };
        vm.GetSessionList = GetSessionList;

        function GetSessionList(registerSession) {
            $scope.Period = "All";
            // apiService.post('/api/Session/GetSession', $scope.query, getSessionSuccess,
            //     RequestFailed);
            apiService.post('/api/Session/GetAllSession', $scope.query, getSessionSuccess,
                RequestFailed);
            vm.close2();
        };


        function GetSearchedSessionList(registerSession) {

            $scope.query.name = $scope.Months[$scope.Session1.frommonth].name + " " + $scope.Session1.fromyear + " - " + $scope.Months[$scope.Session1.tomonth].name + " " + $scope.Session1.toyear;
            $scope.Period=$scope.query.name;
            apiService.post('/api/Session/GetSession', $scope.query, getSearchedSessionSuccess,
                RequestFailed);
            vm.close2();
        };


        function getSessionSuccess(response) {

            if (response.data.Content.length > 0) {
                vm.NoToDoData = false;
                vm.sessionList = response.data.Content;
                vm.sessionCount = response.data.TotalRows;
                $scope.selected = [];
            }
            else {
                if (response.data.Content.length == 0) {
                    vm.NoToDoData = true;
                    vm.sessionList = [];
                    vm.sessionCount = 0;
                    notificationService.displaymessage('No Session list found.');
                }
                else {
                    notificationService.displaymessage('Unable to get session list at the moment. Please try again after some time.');
                }
            }
        }
         function getSearchedSessionSuccess(response) {
            if (response.data.session.length > 0) {
                vm.sessionList = response.data.session;
                vm.sessionCount = response.data.session.length;
                $scope.selected = [];
            }
            else {
                if (response.data.session.length == 0) {
                    vm.sessionList = [];
                    vm.sessionCount = 0;
                    notificationService.displaymessage('No session list found.');
                }
                else {
                    notificationService.displaymessage('Unable to get session list at the moment. Please try again after some time.');
                }
            }
        }

        function addSession(registerSession) {

            if (registerSession.$valid) {
                var model = {
                    Name: $scope.Months[registerSession.frommonth.$modelValue - 1].name + " " + registerSession.fromyear.$modelValue + " - " + $scope.Months[registerSession.tomonth.$modelValue - 1].name + " " + registerSession.toyear.$modelValue,
                    AgencyId: localStorage.agencyId
                };

                apiService.post('api/Session/AddUpdateSession', model, AddSessionSuccess,
                    RequestFailed);

                function AddSessionSuccess(response) {

                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        $scope.Session = {
                            frommonth: '',
                            fromyear: '',
                            tomonth: '',
                            toyear: ''
                        };

                        registerSession.$setPristine();
                        registerSession.$setUntouched();
                        vm.Session = angular.copy(vm.master);
                        GetSessionList();
                        notificationService.displaymessage('Session added Successfully.');
                        // $state.go('triangular.Sessionlist');
                    }
                    else if (response.data.Message != null && response.data.Message != "") {
                        notificationService.displaymessage(response.data.Message);
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                }
            }
        }

        function reset(registerSession1) {

            $scope.Session1 = {};
            registerSession1.$setPristine();
            registerSession1.$setUntouched();
            $scope.query.name = '';
            GetSessionList();
        }

        function ValidateSession() {

            vm.monthValidation = false;
            if ($scope.Session.fromyear == $scope.Session.toyear) {
                if ($scope.Session.frommonth > $scope.Session.tomonth) {
                    vm.monthValidation = true;
                }
            }
        }

        function ValidateSearchSession() {
            vm.monthValidation1 = false;
            if ($scope.Session1.fromyear == $scope.Session1.toyear) {
                if ($scope.Session1.frommonth > $scope.Session1.tomonth) {
                    vm.monthValidation1 = true;
                }
            }
        }

        function showDeleteSessionConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this Session permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteSession(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteSession(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('api/Session/DeleteSessionById', data, DeleteSessionSuccess,
                RequestFailed);

            function DeleteSessionSuccess(response) {
                if (response.data.IsSuccess == true) {
                    notificationService.displaymessage('Session deleted successfully.')
                    GetSessionList();
                }
                else {
                    notificationService.displaymessage('Unable to get session list at the moment. Please try again after some time.');
                }
            }
        };

        function showUpdateSessionDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateClassSessionController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Classes/Views/updatesession.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { sessiondetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
        };

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        };
    }

    function UpdateClassSessionController($scope, $filter, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, SessionService, HOST_URL, items, $localStorage, notificationService, apiService) {

        var vm = this;
        vm.showProgressbar = false;
        vm.tmonth = null;
        vm.fmonth = null;
        vm.ValidateSession = ValidateSession;
        vm.updateSession = updateSession;
        vm.monthValidation = false;
        vm.monthValidation1 = false;

        var date = new Date();
        vm.currentYear = date.getFullYear();
        vm.currentMonth = date.getMonth() + 1;

        vm.updatesessionmodel = {
            Name: '',
            ID: '',
            frommonth: 0,
            tomonth: 0,
            fromyear: 0,
            toyear: 0
        };

        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        $scope.Months = [
            { id: 1, name: "January" },
            { id: 2, name: "February" },
            { id: 3, name: "March" },
            { id: 4, name: "April" },
            { id: 5, name: "May" },
            { id: 6, name: "June" },
            { id: 7, name: "July" },
            { id: 8, name: "August" },
            { id: 9, name: "September" },
            { id: 10, name: "October" },
            { id: 11, name: "November" },
            { id: 12, name: "December" }];
        GetSessionDetailbyId();

        function GetSessionDetailbyId() {
            vm.fmonth = items.sessiondetail.Name.split(' - ')[0].split(' ')[0];
            vm.updatesessionmodel.frommonth = $filter('filter')($scope.Months, { name: vm.fmonth })[0].id;
            vm.updatesessionmodel.fromyear = parseFloat(items.sessiondetail.Name.split(' - ')[0].split(' ')[1]);
            vm.tmonth = items.sessiondetail.Name.split(' - ')[1].split(' ')[0];
            vm.updatesessionmodel.tomonth = $filter('filter')($scope.Months, { name: vm.tmonth })[0].id;
            vm.updatesessionmodel.toyear = parseFloat(items.sessiondetail.Name.split(' - ')[1].split(' ')[1]);
            //vm.session.Name = items.sessiondetail.Name;
            //vm.session.ID = items.sessiondetail.ID;
        }

        function ValidateSession() {
            vm.monthValidation = false;
            if (vm.updatesessionmodel.fromyear == vm.updatesessionmodel.toyear) {
                if (vm.updatesessionmodel.frommonth > vm.updatesessionmodel.tomonth) {
                    vm.monthValidation = true;
                }
            }
        }

        function updateSession(updateSessionform) {
            vm.showProgressbar = true;
            if (updateSessionform.$valid) {
                var model = {
                    ID: items.sessiondetail.ID,
                    Name: $scope.Months[updateSessionform.frommonth.$modelValue].name + " " + updateSessionform.fromyear.$modelValue + " - " + $scope.Months[updateSessionform.tomonth.$modelValue].name + " " + updateSessionform.toyear.$modelValue,
                    AgencyId: localStorage.agencyId

                };

                apiService.post('/api/Session/AddUpdateSession', model, UpdateSessionSuccess,
                    RequestFailed);

                function UpdateSessionSuccess(response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        //$scope.GetSessionList;
                        notificationService.displaymessage('Session updated Successfully.');
                        items.sessiondetail.Name = model.Name;
                        $mdDialog.hide();
                    }
                    else if (response.data.Message != null && response.data.Message != "") {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.Message);
                        $mdDialog.hide();
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                }
            }
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
    }
})();