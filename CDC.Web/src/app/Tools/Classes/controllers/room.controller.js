(function () {
    'use strict';

    function updateRoomController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage, CommonService) {

        var vm = this;
        //  $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 
        vm.getLocations = getLocations;
        vm.room = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        getLocations();
        function getLocations() {
            vm.promise = CommonService.getAgencyLocations(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllLocations = null;
                    vm.AllLocations = eval(response.Content);
                }
            });

        };
        function getRoomDetailsbyId() {
            vm.room.Name = items.roomdetail.Name;
            vm.room.ID = items.roomdetail.ID;
            vm.room.AgencyLocationInfoId = items.roomdetail.AgencyLocationInfoId.toString();
        }

        getRoomDetailsbyId();

        function updateRoom(updateRoom) {
            if (updateRoom.$valid) {
                var model = {
                    ID: vm.room.ID,
                    Name: updateRoom.Room.$viewValue,
                    AgencyId: localStorage.agencyId,
                    AgencyLocationInfoId:updateRoom.location.$modelValue
                };

                apiService.post('/api/Room/AddUpdateRoom', model, updateRoomSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateRoomSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Room updated successfully.');
                items.roomdetail.Name = $scope.vm.room.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
                $mdDialog.hide();
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateRoom = updateRoom;
    }
    function RoomController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage, filerService,CommonService) {
        var vm = this;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId
        };
        vm.addRoom = addRoom;
        vm.getLocations = getLocations;
        vm.toggleRight1 = filerService.toggleRight1();
        vm.isOpenRight1 = filerService.isOpenRight1();
        vm.close1 = filerService.close1();
        vm.reset = reset;
        vm.showProgressbar = false;
        vm.master = {};
        vm.Room = {
            Name: ''
        };
        vm.GetRoomList = getRoomList;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.roomCount = 0;
       
        getLocations();
        getRoomList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Room',
            ID: 'ID'
        };
        function getLocations() {
            vm.promise = CommonService.getAgencyLocations(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllLocations = null;
                    vm.AllLocations = eval(response.Content);
                    vm.Room.AgencyLocationInfoId = vm.AllLocations[0].ID.toString();
                }
            });

        };
        function getRoomList() {
            $scope.query.agencyId=localStorage.agencyId;
            $scope.Room=$scope.query.name==""?"All":$scope.query.name;
            // apiService.post('/api/Room/GetRoom', $scope.query, getRoomSuccess,
            //     RequestFailed);
                  apiService.post('/api/Room/GetAllRoom', $scope.query, getRoomSuccess,
                RequestFailed);
        }
        vm.showProgressbar=true;
           function getRoomSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoToDoData = false;
                vm.roomList = response.data.Content;
                vm.roomCount = response.data.TotalRows;
                vm.showProgressbar=false;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoToDoData = true;
                    vm.roomList = [];
                    vm.roomCount = 0;
                    vm.showProgressbar=false;
                    notificationService.displaymessage('No Room list found.');
                } else {
                    notificationService.displaymessage('Unable to load room list at this moment. Please try again after some time.');
                }
            }
            vm.close1();
        }

        // function getRoomSuccess(response) {
        //     if (response.data.room.length > 0) {
        //         vm.roomList = response.data.room;
        //         vm.roomCount = response.data.room.length;
        //         $scope.selected = [];
        //     } else {
        //         if (response.data.room.length === 0) {
        //             vm.roomList = [];
        //             vm.roomCount = 0;
        //             notificationService.displaymessage('No room list found.');
        //         } else {
        //             notificationService.displaymessage('Unable to room list at the moment. Please try again after some time.');
        //         }
        //     }
        //     vm.close1();
        // }

        function addRoom(registerRoom) {
            if (registerRoom.$valid) {
                var model = {
                    Name: registerRoom.Room.$modelValue,
                    AgencyId: localStorage.agencyId,
                    AgencyLocationInfoId: registerRoom.location.$modelValue
                };
                vm.formName = registerRoom;
                apiService.post('/api/Room/AddUpdateRoom', model, AddRoomSuccess,
                    RequestFailed);

            }
        }

        function AddRoomSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.Room = angular.copy(vm.master);
                getRoomList();
                notificationService.displaymessage('Room name added successfully.');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function reset() {
            $scope.query.name = '';
            $scope.Room="";
            getRoomList();
        }

        function deleteRoom(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/Room/DeleteRoomById', data, DeleteRoomSuccess,
                RequestFailed);
        }

        function DeleteRoomSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Room deleted successfully.')
                getRoomList();
            } else {

                notificationService.displaymessage('Unable to get room list at the moment. Please try again after some time.');
            }
        }

        function showDeleteRoomConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this room permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteRoom(data);
            }, function () {
                $scope.hide();
            });
        }

        vm.showDeleteRoomConfirmDialoue = showDeleteRoomConfirmDialoue;

        function showUpdateRoomDialoue(event, data) {
            $mdDialog.show({
                controller: updateRoomController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Classes/Views/updateroom.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { roomdetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.showUpdateRoomDialoue = showUpdateRoomDialoue;
    }
    angular
        .module('app.dropdownclass')
        .controller('RoomController', RoomController)
        .controller('UpdateRoomController', updateRoomController);
    /* @ngInject */
})();