// (function() {
//     'use strict';

//     function updateCategoryController($scope, $http, $state ,notificationService, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, categoryService, HOST_URL, items) {

//         var vm = this;
//             $scope.$storage = $sessionStorage;
//       //   $scope.$storage = $sessionStorage;
//          if($sessionStorage.agencyId == undefined || null)
//         {
//             $state.go('authentication.login');
//             notificationService.displaymessage("You must login first.");
//             return;
//         } 


//         vm.category = {
//             Name: '',
//             ID: ''
//         };
//         $scope.hide = function() {
//             $mdDialog.hide();
//             $mdDialog.destroy();
//             $mdDialog.cancel();
//         };

//         function getCategoryDetailsbyId() {

//             /// <summary>
//             /// Gets the category detail by identifier.
//             /// </summary>
//             /// <returns></returns>
//             vm.category.Name = items.categorydetail.Name;
//             vm.category.ID = items.categorydetail.ID;
//         }

//         getCategoryDetailsbyId();

//         function notificationMessageController(message) {
//             $timeout(function() {
//                 $rootScope.$broadcast('newMailNotification');
//                 $mdToast.show({
//                     template: '<md-toast><span flex>' + message + '</span></md-toast>',
//                     position: 'bottom right',
//                     hideDelay: 5000
//                 });
//             }, 100);

//         }

//         function updateCategory(updateCategory) {
//             if (updateCategory.$valid) {
//                 var model = {
//                     ID: vm.category.ID,
//                     Name: updateCategory.Category.$viewValue
//                 };

//                 $http.post(HOST_URL.url + '/api/Category/UpdateCategory', model).then(function(response) {
//                     if (response.data.IsSuccess === true) {
//                         vm.showProgressbar = false;
//                         notificationMessageController('Category updated Successfully.');
//                         items.categorydetail.Name = updateCategory.Category.$viewValue;
//                         $mdDialog.hide();
//                     } else {
//                         vm.showProgressbar = false;
//                         notificationMessageController('Unable to update at the moment. Please try again after some time.');
//                         $mdDialog.hide();
//                     }
//                 });
//             }
//         }

//         vm.updateCategory = updateCategory;;
//     }

//     function categoryController($scope, $http,$sessionStorage,$state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, categoryService, HOST_URL) {
//         var vm = this;
//          $scope.$storage = $sessionStorage;
//       //   $scope.$storage = $sessionStorage;
//          if($sessionStorage.agencyId == undefined || null)
//         {
//             $state.go('authentication.login');
//             notificationService.displaymessage("You must login first.");
//             return;
//         } 


//         $scope.selected = [];
//         vm.columns = {
//             Name: 'Category Name',
//             ID: 'ID'
//         };
//    vm.query = {
//             AgencyId: $sessionStorage.agencyId
//         };
//         function notificationMessageController(message) {
//             $timeout(function() {
//                 $rootScope.$broadcast('newMailNotification');
//                 $mdToast.show({
//                     template: '<md-toast><span flex>' + message + '</span></md-toast>',
//                     position: 'bottom right',
//                     hideDelay: 5000
//                 });
//             }, 100);

//         }

//         function getCategoryList() {
        
        
//             vm.promise = categoryService.GetCategoryListService(vm.query);
//             vm.promise.then(function(response) {
//                 if (response.category.length > 0) {
//                     vm.NoData = false;
//                     vm.categoryList = response.category;
//                     vm.categoryCount = response.category.length;
//                     $scope.selected = [];
//                 } else {
//                     if (response.category.length === 0) {
//                         vm.NoData = true;
//                         vm.categoryList = [];
//                         vm.categoryCount = 0;
//                         notificationMessageController('No Category list found.');
//                     } else {
//                         notificationMessageController('Unable to get category list at the moment. Please try again after some time.');
//                     }
//                 }
//             });

//         }

//         function addCategory(registerCategory) {
//             debugger
//             if (registerCategory.$valid) {
//                 var model = {
//                     Name: registerCategory.Category.$modelValue
//                 };
//                 $http.post(HOST_URL.url + '/api/Category/AddCategory', model).then(function(response) {
//                     if (response.data === 1) {
//                         vm.showProgressbar = false;
//                         registerCategory.$setPristine();
//                         registerCategory.$setUntouched();
//                         vm.Category = angular.copy(vm.master);
//                         getCategoryList();
//                         notificationMessageController('Category added Successfully.');
//                     } else {
//                         vm.showProgressbar = false;
//                         notificationMessageController('Unable to add at the moment. Please try again after some time.');
//                     }
//                 });
//             }
//         }

//         vm.addCategory = addCategory;
//         vm.showProgressbar = false;
//         vm.master = {};
//         vm.Category = {
//             Name: ''
//         };
//         vm.GetCategoryList = getCategoryList;
//         $scope.Pageno = 0;
//         vm.skip = 0;
//         vm.take = 0;
//         vm.categoryCount = 0;
//         $scope.query = {
//             filter: '',
//             limit: '10',
//             order: '-id',
//             page: 1,
//             status: 0,
//             name: ''
//         };
//         getCategoryList();

//         function deleteCategory(data) {
//             vm.promise = categoryService.DeleteCategoryById(data);
//             vm.promise.then(function(response) {
//                 if (response.IsSuccess === true) {
//                     notificationMessageController('Category deleted successfully.')
//                     getCategoryList();
//                 } else {

//                     notificationMessageController('Unable to get category list at the moment. Please try again after some time.');
//                 }
//             });
//         }

//         function showDeleteCategoryConfirmDialoue(event, data) {
//             var confirm = $mdDialog.confirm()
//                 .title('Are You Sure to delete this category permanently?')
//                 .ariaLabel('Lucky day')
//                 .targetEvent(event)
//                 .ok('Delete')
//                 .cancel('Cancel');
//             $mdDialog.show(confirm).then(function() {
//                 deleteCategory(data);
//             }, function() {
//                 $scope.hide();
//             });
//         }

//         vm.showDeleteCategoryConfirmDialoue = showDeleteCategoryConfirmDialoue;

//         function showUpdateCategoryDialoue(event, data) {
//             $mdDialog.show({
//                 controller: updateCategoryController,
//                 controllerAs: 'vm',
//                 templateUrl: 'app/Tools/Classes/Views/updatecategory.tmpl.html',
//                 parent: angular.element(document.body),
//                 targetEvent: event,
//                 escToClose: true,
//                 clickOutsideToClose: true,
//                 items: { categorydetail: data },
//                 fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
//             });
//         }

//         vm.showUpdateCategoryDialoue = showUpdateCategoryDialoue;

       
//     }

//     angular
//         .module('app.category')
//         .controller('CategoryController', categoryController)
//         .controller('UpdateCategoryController', updateCategoryController);

//     /* @ngInject */
// })();