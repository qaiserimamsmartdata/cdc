(function () {
    'use strict';
    angular
        .module('app.dropdownclass')
        .controller('StatusController', StatusController)
        .controller('UpdateStatusController', updateStatusController);

    function updateStatusController($scope, $http, $state, $stateParams,$localStorage, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService) {

        var vm = this;
         $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 
        vm.status = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getStatusDetailsbyId() {
            /// <summary>
            /// Gets the status detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.status.Name = items.statusdetail.Name;
            vm.status.ID = items.statusdetail.ID;
        }

        getStatusDetailsbyId();

        function updateStatus(updateStatus) {
            if (updateStatus.$valid) {
                var model = {
                    ID: vm.status.ID,
                    Name: updateStatus.Status.$viewValue
                };

                apiService.post('/api/Status/AddUpdateStatus', model, updateStatusSuccess,
                    UpdateRequestFailed);
            }

        }

        function updateStatusSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Status updated Successfully.');
                items.statusdetail.Name = $scope.vm.status.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                $mdDialog.hide();
            }
        }

        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }

        vm.updateStatus = updateStatus;
    }

    function StatusController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService) {
        var vm = this;
        vm.addStatus = addStatus;
        vm.showProgressbar = false;
        vm.master = {};
        vm.Status = {
            Name: ''
        };
        vm.GetStatusList = getStatusList;
        vm.showDeleteStatusConfirmDialoue = showDeleteStatusConfirmDialoue;
        vm.showUpdateStatusDialoue = showUpdateStatusDialoue;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.statusCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: ''
        };
        getStatusList();
        $scope.selected = [];
        vm.columns = {
            Name: 'Status',
            ID: 'ID'
        };

        function getStatusList() {
            $scope.Status=$scope.query.name==""?"All":$scope.query.name;
            apiService.post('/api/Status/GetStatusList', null, getStatusSuccess,
                RequestFailed);
        }

        vm.showProgressbar=true;
        function getStatusSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoData = false;
                vm.statusList = response.data.Content;
                vm.statusCount = response.data.Content.length;
                vm.showProgressbar=false;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoData = true;
                    vm.statusList = [];
                    vm.statusCount = 0;
                    vm.showProgressbar=false;
                    notificationService.displaymessage('No Status list found.');
                } else {
                    notificationService.displaymessage('Unable to get status list at the moment. Please try again after some time.');
                }
            }
        }

        function addStatus(registerStatus) {
            if (registerStatus.$valid) {
                var model = {
                    Name: registerStatus.Status.$modelValue
                };
                vm.formName = registerStatus;
                apiService.post('/api/Status/AddUpdateStatus', model, AddStatusSuccess,
                    RequestFailed);
            }
        }

        function AddStatusSuccess(response) {
            if (response.data.IsSuccess=== true) {
                vm.showProgressbar = false;
                vm.Status.Name="";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.Status = angular.copy(vm.master);
                getStatusList();
                notificationService.displaymessage('Status added successfully.');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage('Status you are trying to add already exist.');
            }
        }

        function deleteStatus(data) {
            apiService.post('/api/Status/DeleteStatus', data, DeleteStatusSuccess,
                RequestFailed);
        }

        function DeleteStatusSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Status deleted successfully.')
                getStatusList();

            } else {
                notificationService.displaymessage('Unable to get status list at the moment. Please try again after some time.');
            }
        }

        function showDeleteStatusConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this status permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteStatus(data);
            }, function () {
                $scope.hide();
            });
        }

        function showUpdateStatusDialoue(event, data) {
            $mdDialog.show({
                controller: updateStatusController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Classes/Views/updatestatus.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { statusdetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
    }
    /* @ngInject */
})();