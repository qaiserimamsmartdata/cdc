(function () {
    'use strict';

    angular
        .module('app.room')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider, $urlRouterProvider) {

        $stateProvider
       
            .state('triangular.roomlist', {
                url: '/room',
                templateUrl: 'app/Tools/Room/views/roomlist.tmpl.html',
                // set the controller to load for this page
                controller: 'RoomController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.addroom', {
                url: '/room/add',
                templateUrl: 'app/Tools/Room/views/addroom.tmpl.html',
                // set the controller to load for this page
                controller: 'RoomController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            
        

        // triMenuProvider.addMenu({
        //     name: 'Tools',
        //     icon: 'md-cyan-theme material-icons zmdi zmdi-account-box',
        //     type: 'dropdown',
        //     priority: 1.1,
        //     children: [
        //         {
        //             name: 'Settings',
        //             state: 'triangular.editsettings',
        //             type: 'link'
        //         },
        //         // {
        //         //     name: 'Category',
        //         //     state: 'triangular.categorylist',
        //         //     type: 'link'
        //         // }, {
        //         //     name: 'Add Category',
        //         //     state: 'triangular.addcategory',
        //         //     type: 'link'
        //         // }
        //         ]
        // });
    }
})();
