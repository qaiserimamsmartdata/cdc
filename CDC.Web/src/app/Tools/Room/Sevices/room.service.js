﻿(function () {
    'use strict';

    angular
        .module('app.room')
        .factory('RoomService', RoomService);

    RoomService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function RoomService($http, $q, HOST_URL) {
        return {
            GetRoomListService: GetRoomListService,
            DeleteRoomById:DeleteRoomById
        };

        ////////////////

        function GetRoomListService(model) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.get(HOST_URL.url + '/api/Room/GetRoom', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function DeleteRoomById(RoomId) {
            var deferred = $q.defer();
                        $http.post(HOST_URL.url + '/api/Room/DeleteRoomById?RoomId='+RoomId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }
    }
})();