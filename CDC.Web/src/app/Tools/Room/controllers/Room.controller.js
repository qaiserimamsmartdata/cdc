(function () {
    'use strict';

    angular
        .module('app.room')
        .controller('RoomController', RoomController)
        .controller('UpdateRoomController', UpdateRoomController);

    /* @ngInject */

    function RoomController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, RoomService, HOST_URL,CommonService) {
        var vm = this;
        vm.master = {};
        vm.getLocations=getLocations;
        $scope.selected = [];
        vm.columns = {
            Name: 'Room Name',
            ID: 'ID'
        };
        vm.showDeleteRoomConfirmDialoue=showDeleteRoomConfirmDialoue;
        vm.showUpdateRoomDialoue=showUpdateRoomDialoue;
        getLocations();
        GetRoomList();
        vm.addRoom = addRoom;
        vm.showProgressbar = false;

        vm.Room = {
            Name: '',

        };
          
        function GetRoomList() {
            vm.promise = RoomService.GetRoomListService($scope.query);
            vm.promise.then(function (response) {
                if (response.room.length > 0) {
                    vm.roomList = response.room;
                    vm.roomCount = response.room.length;
                    $scope.selected = [];
                }
                else {
                    if (response.room.length == 0) {
                        vm.roomList = [];
                        vm.roomCount = 0;
                        NotificationMessageController('No room list found.');
                    }
                    else {
                        NotificationMessageController('Unable to get room list at the moment. Please try again after some time.');
                    }
                }
            });

        };
      
        function addRoom(registerRoom) {
            if (registerRoom.$valid) {
                var model = {
                    Name: registerRoom.Room.$modelValue
                };

                $http.post(HOST_URL.url + '/api/Room/addUpdateRoom', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        registerRoom.$setPristine();
                        registerRoom.$setUntouched();
                        vm.Room = angular.copy(vm.master);
                        GetRoomList();
                        NotificationMessageController('Room added Successfully.');
                        $state.go('triangular.Roomlist');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function showDeleteRoomConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are You Sure to delete this Room permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteRoom(data);
            }, function () {
                $scope.hide();
            });
        };
        function deleteRoom(data) {
            vm.promise = RoomService.DeleteRoomById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    GetRoomList();
                }
                else {

                    NotificationMessageController('Unable to get room list at the moment. Please try again after some time.');
                }
            });
        };
        function showUpdateRoomDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateRoomController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Room/views/updateroom.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { roomdetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
function UpdateRoomController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, RoomService, HOST_URL, items) {

        var vm = this;
        vm.room = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        GetRoomDetailbyId();


        function GetRoomDetailbyId() {
            
            vm.room.Name = items.roomdetail.Name;
            vm.room.ID=items.roomdetail.ID;
        }
        vm.updateRoom = updateRoom;

        function updateRoom(updateRoom) {
            if (updateRoom.$valid) {
                var model = {
                    ID: vm.room.ID,
                    Name: updateRoom.Room.$viewValue
                };

                $http.post(HOST_URL.url + '/api/Room/AddUpdateRoom', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Room updated Successfully.');
                         items.roomdetail.Name=updateRoom.Room.$viewValue
                        $mdDialog.hide();
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }
      
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
})();