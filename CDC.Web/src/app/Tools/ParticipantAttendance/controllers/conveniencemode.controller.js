(function () {
    'use strict';
    angular
        .module('app.dropdownparticipantattendance')
        .controller('ConvenienceModeController', ConvenienceModeController)
        .controller('UpdateConvenienceModeController', UpdateConvenienceModeController);

    function UpdateConvenienceModeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, items, notificationService, apiService, $localStorage) {

        var vm = this;
        vm.Convenience = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getConvenienceDetailsbyId() {
            vm.Convenience.Name = items.Conveniencedetail.Name;
            vm.Convenience.ID = items.Conveniencedetail.ID;
        }
        getConvenienceDetailsbyId();

        function updateConvenience(updateConvenience) {
            if (updateConvenience.$valid) {
                var model = {
                    ID: vm.Convenience.ID,
                    Name: updateConvenience.Convenience.$viewValue,
                    AgencyId: localStorage.agencyId
                };
                apiService.post('/api/GeneralDroppedBy/AddUpdateGeneralDroppedBy', model, updateConvenienceSuccess, UpdateRequestFailed);
            }
        }
        function updateConvenienceSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                notificationService.displaymessage('Convenience mode updated successfully !');
                items.Conveniencedetail.Name = $scope.vm.Convenience.Name;
                $mdDialog.hide();
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
                $mdDialog.hide();
            }
        }
        function UpdateRequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
        vm.updateConvenience = updateConvenience;
    }



    function ConvenienceModeController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, notificationService, HOST_URL, apiService, $localStorage, filerService) {
        $scope.form = {};
        var vm = this;
          $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 
        localStorage.ParticipantAttendancePage = false;

        vm.addConvenience = addConvenience;
        vm.showProgressbar = false;
        vm.master = {};
        vm.Convenience = {
            Name: ''
        };
        //vm.GetModeOfConvenienceList=GetModeOfConvenienceList;
        vm.GetConvenienceList = getConvenienceList;
        vm.showDeleteConvenienceConfirmDialoue = showDeleteConvenienceConfirmDialoue;
        vm.showUpdateConvenienceDialoue = showUpdateConvenienceDialoue;
        vm.toggleRight3 = filerService.toggleRight3();
        vm.isOpenRight3 = filerService.isOpenRight3();
        vm.close3 = filerService.close3();
        vm.reset = reset;
        vm.clear = clear;
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.ConvenienceCount = 0;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            Name: '',
            AgencyId: localStorage.agencyId
        };
        getConvenienceList();
        vm.selected = [];
        //vm.getConvenienceSuccess = getConvenienceSuccess;
        //vm.RequestFailed = RequestFailed;
        vm.columns = {
            Name: 'Convenience',
            ID: 'ID'
        };
        // function GetModeOfConvenienceList() {
        //     vm.promise = ModeOfConvenienceService.GetModeOfConvenienceListService(vm.query);
        //     vm.promise.then(function (response) {
        //         if (response.IsSuccess == true) {
        //             if (response.Content.length > 0) {
        //                 vm.ConvenienceList = {};
        //                 vm.ConvenienceList = response.Content;
        //                 vm.ConvenienceCount = response.TotalRows;
        //                 vm.selected = [];
        //             }
        //             else {
        //                 vm.ConvenienceList = [];
        //                 vm.ConvenienceCount = 0;
        //                 NotificationMessageController('No Convenience list found.');
        //             }
        //         }
        //         else {
        //             NotificationMessageController('Unable to get Convenience list at the moment. Please try again after some time.');
        //         }
        //         vm.close();
        //     });
        // };

        function getConvenienceList() {
            $scope.Convenience=vm.query.Name==""?"All":vm.query.Name;
            var model = {
                AgencyId: localStorage.agencyId
            };
            //apiService.post('/api/ModeOfConvenience/GetModeOfConvenience', model, getConvenienceSuccess, RequestFailed);
            apiService.post('/api/GeneralDroppedBy/GetAllGeneralDroppedBy', vm.query, getConvenienceSuccess, RequestFailed);
        }

        vm.showProgressbar=true;
        function getConvenienceSuccess(response) {
            // if (response.data.ModeOfConvenience.length > 0) {
            //     vm.ConvenienceList = response.data.ModeOfConvenience;
            //     vm.ConvenienceCount = response.data.ModeOfConvenience.length;
            //     vm.selected = [];
            // } else {
            //     if (response.data.ModeOfConvenience.length === 0) {
            //         vm.ConvenienceList = [];
            //         vm.ConvenienceCount = 0;
            //         notificationService.displaymessage('No Convenience list found.');
            //     } else {
            //         notificationService.displaymessage('Unable to get Convenience list at the moment. Please try again after some time.');
            //     }
            // }
            if (response.data.Content.length > 0) {
                vm.NoToDoData = false;
                vm.ConvenienceList = response.data.Content;
                vm.ConvenienceCount = response.data.TotalRows;
                vm.showProgressbar=false;
                vm.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoToDoData = true;
                    vm.ConvenienceList = [];
                    vm.ConvenienceCount = 0;
                    vm.showProgressbar=false;
                    notificationService.displaymessage('No Convenience mode list found.');
                } else {
                    notificationService.displaymessage('Unable to get Convenience list at the moment. Please try again after some time.');
                }
            }
            vm.close3();


        }

        function addConvenience(registerConvenience) {
            if (registerConvenience.$valid) {
                var model = {
                    Name: registerConvenience.ConvenienceMode.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                vm.formName = registerConvenience;
                apiService.post('/api/GeneralDroppedBy/AddUpdateGeneralDroppedBy', model, AddConvenienceSuccess, vm.RequestFailed);
            }
        }

        function AddConvenienceSuccess(response) {
            if (response.data.IsSuccess === true) {
                vm.showProgressbar = false;
                vm.Convenience.Name = "";
                vm.formName.$setUntouched();
                vm.formName.$setPristine();
                vm.Convenience = angular.copy(vm.master);
                getConvenienceList();
                notificationService.displaymessage('Convenience mode added successfully !');
            } else {
                vm.showProgressbar = false;
                notificationService.displaymessage(response.data.Message);
            }
        }

        function deleteConvenience(data) {
            data.AgencyId = localStorage.agencyId;
            apiService.post('/api/GeneralDroppedBy/DeleteGeneralDroppedBy', data, DeleteConvenienceSuccess, vm.RequestFailed);
        }

        function DeleteConvenienceSuccess(response) {
            if (response.data.IsSuccess === true) {
                notificationService.displaymessage('Convenience mode deleted successfully.')
                getConvenienceList();
            } else {
                notificationService.displaymessage('Unable to get Convenience mode list at the moment. Please try again after some time.');
            }
        }

        function reset() {
            vm.query.Name = '';
             $scope.Convenience="";
            vm.GetConvenienceList();
        }

        function clear(formName) {
            //vm.formName.$setUntouched();
            //vm.formName.$setPristine();
            vm.Convenience.Name = '';
            
            // var form = $scope[formName];
            // form.$setUntouched();
            // form.$setPristine();

           $scope.form.registerConvenience.$setUntouched();
            $scope.form.registerConvenience.$setPristine();
            console.log('ok');

        }


        function showDeleteConvenienceConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this Convenience mode permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteConvenience(data);
            }, function () {
                $scope.hide();
            });
        }

        function showUpdateConvenienceDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateConvenienceModeController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/ParticipantAttendance/views/updateconveniencemode.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { Conveniencedetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
    }
    /* @ngInject */
})();