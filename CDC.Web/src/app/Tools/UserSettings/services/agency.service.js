﻿(function () {
    'use strict';

    angular
        .module('app.category')
        .factory('AgencyService', AgencyService);

    AgencyService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function AgencyService($http, $q, HOST_URL) {
        return {
            GetAgencyListByIdService: GetAgencyListByIdService,
            SetId: SetId,
            SetIsLoggedFirstTime: SetIsLoggedFirstTime,
            GetLocations: GetLocations,
            DeleteLocationById: DeleteLocationById,
            GetPaymentByIdService: GetPaymentByIdService
        };

        function SetId(Id, Name) {
            this.AgencyId = Id;
            this.AgencyName = Name;
        };

        // function GetAgencyListByIdService(AgencyId) {
        //     var deferred = $q.defer();
        //     $http.post(HOST_URL.url + '/api/Agency/GetAgencyById?AgencyId=' + AgencyId).success(function (response, status, headers, config) {
        //         deferred.resolve(response);
        //     }).error(function (errResp) {
        //         deferred.reject({ message: "Really bad" });
        //     });
        //     return deferred.promise;
        // };
        //  function SetIsLoggedFirstTime(AgencyId) {
        //             var deferred = $q.defer();
        //             $http.post(HOST_URL.url + '/api/Agency/SetIsLoggedFirstTime?AgencyId=' + AgencyId).success(function (response, status, headers, config) {
        //                 deferred.resolve(response);
        //             }).error(function (errResp) {
        //                 deferred.reject({ message: "Really bad" });
        //             });
        //             return deferred.promise;
        //         };
        //     }
        function GetAgencyListByIdService(AgencyId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/AgencyRegistration/GetAgencyById?AgencyId=' + AgencyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function GetPaymentByIdService(AgencyId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/AgencyRegistration/GetPaymentById?AgencyId=' + AgencyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function GetLocations(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Location/GetAllLocation', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function SetIsLoggedFirstTime(AgencyId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/AgencyRegistration/SetIsLoggedFirstTime?AgencyId=' + AgencyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function DeleteLocationById(locationId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/AgencyRegistration/DeleteLocation?locationId=' + locationId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
    }
})();