(function () {
    'use strict';

    angular
        .module('app.category')
        .controller('UpdateAgencyController', UpdateAgencyController);


    /* @ngInject */
    function UpdateAgencyController($filter, $scope, notificationService, $http, $state, apiService, $stateParams, $localStorage, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService, AgencyService) {
        var vm = this;
        // $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        $scope.$storage = localStorage;
        vm.query = {
            TimeZone: ""
        };
        if (localStorage.TimeZone == null || localStorage.TimeZone == undefined)
            vm.query.TimeZone = "";
        else
            vm.query.TimeZone = localStorage.TimeZone;

        var stateParamsAgencyId = $stateParams.id;
        localStorage.ParticipantAttendancePage = false;
        // $window.alert("Hello " + newAgencyId);
        var newAgencyId = "";

        if (stateParamsAgencyId == "") {
            newAgencyId = $scope.$storage.agencyId;
        } else {
            newAgencyId = stateParamsAgencyId;
        }

        vm.cancelUpdate = cancelUpdate;
        // var AgencyId = AgencyService.AgencyId;

        vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;
        vm.agency = {
            agencyname: '',
            contactperson: '',
            address: '',
            countryid: '',
            stateid: '',
            cityname: '',
            postalcode: 0,
            phonenumber: 0,
            password: '',
            isloggedfirsttime: '',
        };
        GetAllCountry();
        GetAgencyList();
        function cancelUpdate() {
            if (stateParamsAgencyId == "") {
                $state.go("triangular.dashboard");
            } else {
                $state.go("triangular.superadmin");
            }
        }
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            // $scope.CountryTextToShow = "Please Wait..."; // this will show until load states from database
            var myPromise = CommonService.getCountries();

            myPromise.then(function (resolve) {
                vm.AllCountries = resolve;
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        vm.Failed = Failed;

        init();
        function init() {
            apiService.post("api/TimeSheet/TimeZones", null,
                getTimeZoneSuccess,
                Failed);

            function getTimeZoneSuccess(result) {
                vm.TimezoneList = result.data.Response;
                vm.TimezoneListSelect = 'Select Timezone';
                GetLocalTimezoneName();
            }
        }

        function Failed(result) {
            notificationService.displaymessage('Please try again after some time.');
        }
        function GetLocalTimezoneName() {
            // var n = new Date,
            //     t = n.getFullYear().toString() + "-" + ((n.getMonth() + 1).toString().length === 1 ? "0" + (n.getMonth() + 1).toString() : (n.getMonth() + 1).toString()) + "-" + (n.getDate().toString().length === 1 ? "0" + n.getDate().toString() : n.getDate().toString()) + "T" + (n.getHours().toString().length === 1 ? "0" + n.getHours().toString() : n.getHours().toString()) + ":" + (n.getMinutes().toString().length === 1 ? "0" + n.getMinutes().toString() : n.getMinutes().toString()) + ":" + (n.getSeconds().toString().length === 1 ? "0" + n.getSeconds().toString() : n.getSeconds().toString());
            // vm.selectedTimeZone = new Date(t + "Z").toString().match(/\(([A-Za-z\s].*)\)/)[1];
            // vm.query.TimeZone = vm.selectedTimeZone;
            // //$scope.pageChangeHandler(1);
        }
        function GetAgencyList() {

            vm.promise = AgencyService.GetAgencyListByIdService(newAgencyId);
            vm.promise.then(function (response) {
                if (response.agencyList.ID > 0) {
                    vm.agency.agencyname = response.agencyList.AgencyName;
                    vm.agency.ContactPersonFirstName = response.agencyList.ContactPersonFirstName;
                    vm.agency.ContactPersonLastName = response.agencyList.ContactPersonLastName;
                    vm.agency.address = response.agencyList.Address;
                    vm.agency.email = response.agencyList.Email;
                    vm.agency.countryid = response.agencyList.CountryId;
                    vm.agency.stateid = response.agencyList.StateId;
                    vm.agency.cityname = response.agencyList.CityName;
                    vm.agency.postalcode = response.agencyList.PostalCode;
                    vm.agency.phonenumber = parseFloat(response.agencyList.PhoneNumber);
                    vm.agency.password = response.agencyList.Password;
                    vm.agency.isloggedfirsttime = response.agencyList.IsLoggedFirstTime;
                    vm.agency.OwnerFirstName = response.agencyList.OwnerFirstName;
                    vm.agency.OwnerLastName = response.agencyList.OwnerLastName;
                    vm.agency.UserId = response.agencyList.UserId;
                    vm.agency.PricingPlanId = response.agencyList.PricingPlanId;
                    vm.query.TimeZone=response.agencyList.TimeZone;
                    vm.agency.TimeClockUsersPlanId = response.agencyList.TimeClockUsersPlanId;
                    $scope.selected = [];
                    GetState(vm.agency.countryid);
                }
                else {
                    if (response.agencyList.ID == 0) {
                        vm.agencyList = [];
                        vm.agencyCount = 0;
                        NotificationMessageController('Invalid found.');
                    }
                    else {
                        NotificationMessageController('Unable to get organization at the moment. Please try again after some time.');
                    }
                }
            });

        };
        vm.updateAgency = updateAgency;

        function updateAgency(agencyObj) {
            
            if (agencyObj.$valid) {

                var model = {
                    TimeClockUsersPlanId: vm.agency.TimeClockUsersPlanId,
                    TotalTimeClockUsers: localStorage.TimeClockUsers,
                    TotalRequireEnrollParticipants: localStorage.MaxNumberOfParticipants,
                    AgencyName: agencyObj.agencyname.$modelValue,
                    ContactPersonFirstName: agencyObj.ContactPersonFirstName.$modelValue,
                    ContactPersonLastName: agencyObj.ContactPersonLastName.$modelValue,
                    Address: agencyObj.address.$modelValue,
                    Email: agencyObj.email.$modelValue,
                    CountryId: agencyObj.countryid.$modelValue,
                    StateId: agencyObj.stateid.$modelValue,
                    CityName: agencyObj.cityname.$modelValue,
                    PostalCode: agencyObj.postalcode.$modelValue,
                    PhoneNumber: agencyObj.phonenumber.$modelValue,
                    OwnerFirstName: agencyObj.OwnerFirstName.$modelValue,
                    OwnerLastName: agencyObj.OwnerLastName.$modelValue,
                    Password: vm.agency.password,
                    IsLoggedFirstTime: vm.agency.isloggedfirsttime,
                    UserId: vm.agency.UserId,
                    PricingPlanId: vm.agency.PricingPlanId,
                    TimeZone: vm.query.TimeZone,
                    ID: newAgencyId                   
                };

                $http.post(HOST_URL.url + '/api/AgencyRegistration/UpdateAgency', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        // sessionService.setSession(response.data.Content);
                        localStorage.TimeZone = vm.query.TimeZone;
                        localStorage.profileName = response.data.Content.AgencyName;
                        if (JSON.parse(localStorage.getItem('roles'))[0] == "SuperAdmin") {
                            $state.go("triangular.superadmin", {}, { reload: true });
                        }
                        else {
                            GetAgencyList();
                            //$state.go("triangular.usersetting", {}, { reload: true });
                        }

                        NotificationMessageController('Organization updated successfully !');

                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

})();