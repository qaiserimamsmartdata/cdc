(function () {
    'use strict';

    angular
        .module('app.category')
        .controller('paymentController', paymentController);


    /* @ngInject */
    function paymentController($filter, $scope, notificationService, $http, $state, apiService, $stateParams, $localStorage, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService, AgencyService) {
        var vm = this;
        $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        // $scope.$storage = localStorage;
        $scope.mask = true;
        vm.query = {
            TimeZone: ""
        };
        if (localStorage.TimeZone == null || localStorage.TimeZone == undefined)
            vm.query.TimeZone = "";
        else
            vm.query.TimeZone = localStorage.TimeZone;



        // $window.alert("Hello " + newAgencyId);
        var stateParamsAgencyId = $stateParams.id;

        // $window.alert("Hello " + newAgencyId);
        var newAgencyId = "";

        if (stateParamsAgencyId == "") {
            newAgencyId = $scope.$storage.agencyId;
        } else {
            newAgencyId = stateParamsAgencyId;
        }

        GetPayment();
        // if (stateParamsAgencyId == "") {
        //     newAgencyId = $scope.$storage.agencyId;
        // } else {
        //     newAgencyId = stateParamsAgencyId;
        // }
        vm.GetPayment = GetPayment;
        vm.cancelUpdate = cancelUpdate;
        // var AgencyId = AgencyService.AgencyId;


        vm.agency = {
            agencyname: '',
            contactperson: '',
            address: '',
            countryid: '',
            stateid: '',
            cityname: '',
            postalcode: 0,
            phonenumber: 0,
            password: '',
            isloggedfirsttime: '',
        };

        function cancelUpdate() {
            if (stateParamsAgencyId == "") {
                $state.go("triangular.dashboard");
            } else {
                $state.go("triangular.superadmin");
            }
        }

        vm.Failed = Failed;



        function Failed(result) {
            notificationService.displaymessage('Please try again after some time.');
        }
        vm.paymentDetail = {
            CardName: '',
            CardNumber: '',
            CardType: '',
            CardExpiryMonth: '',
            CardExpiryYear: '',
            CVV: ''
        };
        var year = new Date().getFullYear();
        var range = [];
        range.push(year);
        for (var i = 1; i < 11; i++) {
            range.push(year + i);
        }
        $scope.years = range;


        // if (vm.paymentDetail.CardType == 'American Express')
        // {
        //             vm.pattern = "9999-9999-9999-999";
        //             vm.CVV="4";
        //             vm.CVVmax="4";
        // }
        // else{
        //     vm.pattern = "9999-9999-9999-9999";
        //     vm.CVV="3";
        //     vm.CVVmax="3";

        // }
        function GetPayment() {

            vm.promise = AgencyService.GetPaymentByIdService(newAgencyId);
            vm.promise.then(function (response) {
                vm.paymentDetail.ID = response.agencyList.ID;
                vm.paymentDetail.CardName = response.agencyList.CardName;
                vm.paymentDetail.CardType = response.agencyList.CardType;

                if (vm.paymentDetail.CardType == 'American Express') {
                    vm.pattern = "9999-999999-99999";
                    vm.CVV = "4";
                    vm.CVVmax = "4";
                    vm.paymentDetail.CardNumber = parseFloat(response.agencyList.CardNumber);
                    var cardNumber = (response.agencyList.CardNumber).toString();
                    vm.paymentDetail.CardNumberstring = "XXXX-XXXXXX-X-" + cardNumber.substring(11);
                }
                else {
                    vm.pattern = "9999-9999-9999-9999";
                    vm.CVV = "3";
                    vm.CVVmax = "3";
                    vm.paymentDetail.CardNumber = parseFloat(response.agencyList.CardNumber);
                    var cardNumber = (response.agencyList.CardNumber).toString();
                    vm.paymentDetail.CardNumberstring = "XXXX-XXXX-XXXX-" + cardNumber.substring(12);

                }


                vm.show = show;
                vm.hide = hide;
                function show() {
                    $scope.mask = false;
                }
                function hide() {
                    $scope.mask = true;
                }
                vm.paymentDetail.AgencyRegistrationId = response.agencyList.AgencyRegistrationId;
                vm.paymentDetail.CardExpiryMonth = response.agencyList.CardExpiryMonth.toString();
                vm.paymentDetail.CardExpiryYear = response.agencyList.CardExpiryYear.toString();
                vm.paymentDetail.CVV = response.agencyList.CVV;
            }

            )
        };
        vm.updatePayment = updatePayment;
        vm.checkStatus = checkStatus;

        function checkStatus() {
            vm.paymentDetail.CardNumber = '';
            if (vm.paymentDetail.CardType != 'American Express') {
                vm.pattern = "9999-9999-9999-9999";
                vm.placeholder = "____-____-____-____";
                vm.CVV = "3";
                vm.CVVmax = "3"
            }
            else {
                vm.pattern = "9999-999999-99999";
                vm.placeholder = "____-______-_____";
                vm.CVV = "4";
                vm.CVVmax = "4"
            }
        }
        function updatePayment(PaymentObj) {

            var model = {};
            model = PaymentObj;
            model.ID = PaymentObj.ID;
            model.AgencyRegistrationId = newAgencyId;
            model.StripeUserId = localStorage.StripeUserId;
            $http.post(HOST_URL.url + '/api/AgencyRegistration/UpdatePayment', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    if (JSON.parse(localStorage.getItem('roles'))[0] == "SuperAdmin") {
                        $state.go("triangular.superadmin", {}, { reload: true });
                    }
                    else {
                        GetPayment();
                        // $state.go("triangular.usersetting", {}, { reload: true });
                    }
                    NotificationMessageController('Payment details updated successfully !');


                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController(response.data.ReturnMessage[0]);
                }
            });
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

})();