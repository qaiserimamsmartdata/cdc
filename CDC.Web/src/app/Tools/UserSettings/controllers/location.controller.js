(function () {
    'use strict';

    angular
        .module('app.category')
        .controller('locationController', locationController);
    //.controller('EditLocationController', EditLocationController);

    /* @ngInject */
    function locationController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, StaffService, CommonService, AgencyService, filerService, $localStorage, HOST_URL) {
        var vm = this;
        $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 
        $scope.$storage = localStorage;

        var stateParamsAgencyId = $stateParams.id;
        var newAgencyId = "";
        if (stateParamsAgencyId == "") {
            newAgencyId = $scope.$storage.agencyId;
        } else {
            newAgencyId = stateParamsAgencyId;
        }

        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.showAddLocationDialogue = showAddLocationDialogue;
        vm.showEditLocationDialogue = showEditLocationDialogue;
        vm.showDeleteLocationDialogue = showDeleteLocationDialogue;
        vm.columns = {
            LocationName: 'Location',
            ContactFirstName: 'First Name',
            ContactLastName: 'Last Name',
            LocationEmailId: 'Email',
            WebsiteUrl: 'Url',
            ID: 'ID',
        };
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.staffCount = 0;
        $scope.selected = [];
        $scope.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            LocationName: '',
            AgencyId: newAgencyId
        };
        vm.GetLocationList = GetLocationList;
        vm.reset = reset;
        GetLocationList();
        $rootScope.$on("GetLocationList", function () {
            GetLocationList();
        });
        function GetLocationList() {
            vm.promise = AgencyService.GetLocations(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.locationList = {};
                        vm.locationList = response.Content;

                        vm.locationCount = response.TotalRows;
                        $scope.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.locationList = [];
                        vm.locationCount = 0;
                        NotificationMessageController('No Location list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get location list at the moment. Please try again after some time.');
                }
                vm.close();
            });

        };
        function showAddLocationDialogue() {
            $mdDialog.show({
                controller: AddLocationController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/UserSettings/views/addLocation.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function showEditLocationDialogue(event, data) {
            $mdDialog.show({
                controller: EditLocationController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/UserSettings/views/editLocation.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                items: { locationdetail: data },
                fullscreen: $scope.customFullscreen
            });
        };
        function showDeleteLocationDialogue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this location permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteLocation(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteLocation(model) {
            console.log(model);
            $http.post(HOST_URL.url + '/api/Location/DeleteLocation?LocationId=' + model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    console.log("deleted");
                    //vm.showProgressbar = false;
                    notificationService.displaymessage('Location deleted successfully.');
                    //cancelClick();
                    GetLocationList();
                    //$rootScope.$emit("GetLocationList", {});
                    //  $state.go('triangular.usersetting', {}, { reload: true });
                }
                else {
                    //vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to delete at the moment. Please try again after some time.');
                }
            });
        };

        function reset() {
            $scope.query.PositionId = 0;
            $scope.query.StatusId = 0;
            $scope.query.StaffName = '';
            GetLocationList();
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }



    function AddLocationController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, imageUploadService) {
        var vm = this;
        vm.buttonText = "Add"
        vm.showProgressbar = false;
        vm.cancelClick = cancelClick;
        vm.addLocation = addLocation;
        vm.GetState = GetState;
        $scope.$storage = localStorage;
         vm.agency = {};
        var stateParamsAgencyId = $stateParams.id;
        var newAgencyId = "";
        if (stateParamsAgencyId == "") {
            newAgencyId = $scope.$storage.agencyId;
        } else {
            newAgencyId = stateParamsAgencyId;
        }

        $scope.query = {
            AgencyId: newAgencyId
        };
        $scope.id = 0;
        $scope.DisplayText = "Add Location";
        GetAllCountry();
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            // $scope.CountryTextToShow = "Please Wait..."; // this will show until load states from database
            var myPromise = CommonService.getCountries();

            myPromise.then(function (resolve) {
                vm.AllCountries = resolve;
                vm.agency.CountryId = "1".toString();
                GetState(vm.agency.CountryId);
            }, function (reject) {

            });

        };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
               
                 vm.States = null;
                vm.States = resolve;
            }, function (reject) {

            });

        };
        function addLocation(addressForm, model) {
            if (addressForm.$valid) {
                vm.showProgressbar = true;
                model.AgencyRegistrationId = newAgencyId;
                $http.post(HOST_URL.url + '/api/Location/AddLocation', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.ReturnMessage[0]);
                        cancelClick();
                        $rootScope.$emit("GetLocationList", {});
                        //  $state.go('triangular.usersetting', {}, { reload: true });
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
    }


    function EditLocationController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, imageUploadService, items) {
        var vm = this;
        $scope.id = 0;
        vm.buttonText = "Update"
        vm.showProgressbar = false;
        vm.cancelClick = cancelClick;
        vm.editLocation = editLocation;
        $scope.$storage = localStorage;

        var stateParamsAgencyId = $stateParams.id;
        var newAgencyId = "";
        if (stateParamsAgencyId == "") {
            newAgencyId = $scope.$storage.agencyId;
        } else {
            newAgencyId = stateParamsAgencyId;
        }

        $scope.query = {
            AgencyId: newAgencyId
        };
        $scope.id = 0;
        $scope.DisplayText = "Update Location";
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        //GetAllCountry();
        //GetLocationDetailbyId=GetLocationDetailbyId;
        vm.agency = {};
        //GetLocationDetailbyId();
        vm.GetLocationDetailbyId = GetLocationDetailbyId;
        //vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;

        GetLocationDetailbyId();
        GetAllCountry();




        function GetLocationDetailbyId() {
            //console.log(items.locationdetail.LocationName);
            
            vm.agency.ID = items.locationdetail.ID;
            vm.agency.LocationName = items.locationdetail.LocationName;
            vm.agency.LocationPhoneNumber = items.locationdetail.LocationPhoneNumber;
            vm.agency.ContactFirstName = items.locationdetail.ContactFirstName;
            vm.agency.ContactLastName = items.locationdetail.ContactLastName;
            vm.agency.LocationEmailId = items.locationdetail.LocationEmailId;
            vm.agency.Description = items.locationdetail.Description;
            vm.agency.Address = items.locationdetail.Address;
            //vm.agency.CountryId = items.locationdetail.CountryId;
            //vm.agency.StateId = items.locationdetail.StateId;
            vm.agency.CityName = items.locationdetail.CityName;
            vm.agency.PostalCode = items.locationdetail.PostalCode;
            vm.agency.PhoneNumber = items.locationdetail.LocationPhoneNumber;
        }




        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            var myPromise = CommonService.getCountries();
            myPromise.then(function (resolve) {
                vm.AllCountries = null;
                vm.AllCountries = resolve;
                //$scope.CountryTextToShow = "--Select Country--";
                vm.agency.CountryId = items.locationdetail.CountryId.toString();
                GetState(items.locationdetail.CountryId);
                vm.agency.StateId = items.locationdetail.StateId.toString();

            }, function (reject) {
            });
        };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            myPromise.then(function (resolve) {
                vm.States = null;
                vm.States = resolve;

                //$scope.StateTextToShow = "--Select States--";
            }, function (reject) {
            });
        };

        function editLocation(addressForm, model) {
            if (addressForm.$valid) {
                vm.showProgressbar = true;
                model.AgencyRegistrationId = newAgencyId;
                //model.CityId = 5;
                //model.IsDeleted = null;
                console.log('call update');
                // var newObj = {};
                //angular.extend(newObj, model);
                $http.post(HOST_URL.url + '/api/Location/AddUpdateLocation', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Location Updated Successfully.');
                        //notificationService.displaymessage(response.data.ReturnMessage[0]);
                        cancelClick();
                        $rootScope.$emit("GetLocationList", {});
                        //  $state.go('triangular.usersetting', {}, { reload: true });
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
    }

})();