(function () {
    'use strict';

    angular
        .module('app.category')
        .controller('AddAgencyController', AddAgencyController);


    /* @ngInject */
    function AddAgencyController($filter,notificationService, $scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService,$localStorage) {
        var vm = this;
        //  $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 
        //   $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 

        // vm.isExistName = isExistName;
        vm.getCountry = getCountry;
        vm.getState = getState;
        vm.getCity = getCity;
        vm.addAgency = addAgency;
        $scope.submitText = "Sign Up";
        vm.reset = reset;
        vm.showProgressbar = false;
        $scope.id = 0;
        $scope.isExist = 0;

        vm.agency = {
            agencyname: '',
            contactperson: '',
            email: '',
            address: '',
            countryid: '',
            stateid: '',
            cityid: '',
            postalcode: '',
            phonenumber: ''
        };
        // vm.getCategory = getCategory;

        getCountry();
        // getState();
        // getCity();
        // getRooms();
        function addAgency(agencyObj) {
            $scope.submitText = "processing";
            if (agencyObj.$valid) {
                var model = {

                    AgencyName: agencyObj.agencyname.$modelValue,
                    ContactPerson: agencyObj.contactperson.$modelValue,
                    Email: agencyObj.email.$modelValue,
                    Address: agencyObj.address.$modelValue,
                    CountryId: agencyObj.countryid.$modelValue,
                    StateId: agencyObj.stateid.$modelValue,
                    CityId: agencyObj.cityid.$modelValue,
                    PostalCode: agencyObj.postalcode.$modelValue,
                    PhoneNumber: agencyObj.phonenumber.$modelValue
                };

                $http.post(HOST_URL.url + '/api/Agency/AddAgency', model).then(function (response) {
                    if (response.data == 1) {
                        vm.showProgressbar = false;
                        $scope.submitText = "Sign Up";
                        NotificationMessageController('Agency is registered Successfully.');
                        NotificationMessageController('Login information has been sent to your email.');
                        reset();
                        $state.go("login");
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });

            }
        }
        function reset() {

            vm.agency.agencyname = '';
            vm.agency.contactperson = '';
            vm.agency.email = '';
            vm.agency.address = '';
            vm.agency.countryid = '';
            vm.agency.stateid = '';
            vm.agency.cityid = '';
            vm.agency.postalcode = '';
            vm.agency.phonenumber = '';
        };

        function getCountry() {
            vm.promise = CommonService.getCountries();
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.AllCountries = null;
                    vm.AllStates = null;
                    vm.AllCities = null;
                    vm.AllCountries = response;
                }
            });

        };
        function getState(CountryId) {
            vm.promise = CommonService.getStates(CountryId);
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.AllStates = null;
                    vm.AllCities = null;
                    vm.AllStates = response;
                }
            });

        };
        function getCity(StateId) {
            vm.promise = CommonService.getCities(StateId);
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.AllCities = null;
                    vm.AllCities = response;
                }
            });

        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
})();