(function () {
    'use strict';

    function updateCategoryController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, categoryService, HOST_URL, items,notificationService,$localStorage) {

        var vm = this;
    //       $scope.$storage = localStorage;
    //   //   $scope.$storage = localStorage;
    //      if(localStorage.agencyId == undefined || null)
    //     {
    //         $state.go('authentication.login');
    //         notificationService.displaymessage("You must login first.");
    //         return;
    //     } 
        vm.category = {
            Name: '',
            ID: ''
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        function getCategoryDetailsbyId() {

            /// <summary>
            /// Gets the category detail by identifier.
            /// </summary>
            /// <returns></returns>
            vm.category.Name = items.categorydetail.Name;
            vm.category.ID = items.categorydetail.ID;
        }

        getCategoryDetailsbyId();

        function notificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        }

        function updateCategory(updateCategory) {
            if (updateCategory.$valid) {
                var model = {
                    ID: vm.category.ID,
                    Name: updateCategory.Category.$viewValue,
                    AgencyId:localStorage.agencyId
                };

                $http.post(HOST_URL.url + '/api/Category/UpdateCategory', model).then(function (response) {
                    if (response.data.IsSuccess === true) {
                        vm.showProgressbar = false;
                        notificationMessageController('Category updated Successfully.');
                        items.categorydetail.Name = updateCategory.Category.$viewValue;
                        $mdDialog.hide();
                    } else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.Message);
                    }
                });
            }
        }

        vm.updateCategory = updateCategory;;
    }

    function categoryController($scope, $localStorage, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, categoryService, HOST_URL, filerService,apiService,notificationService) {
        var vm = this;
        //  $scope.$storage = localStorage;
      //   $scope.$storage = localStorage;
        //  if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 

        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        // $scope.query = {
        //     AgencyId: localStorage.agencyId,
        //     name:''
        // };
        $scope.selected = [];
        vm.columns = {
            Name: 'Category Name',
            ID: 'ID'
        };

        function notificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        }

        // function getCategoryList() {
        //     vm.promise = categoryService.GetCategoryListService($scope.query);
        //     vm.promise.then(function (response) {
        //         if (response.category.length > 0) {
        //             vm.categoryList = response.category;
        //             vm.categoryCount = response.category.length;
        //             $scope.selected = [];
        //         } else {
        //             if (response.category.length === 0) {
        //                 vm.categoryList = [];
        //                 vm.categoryCount = 0;
        //                 notificationMessageController('No category list found.');
        //             } else {
        //                 notificationMessageController('Unable to get category list at the moment. Please try again after some time.');
        //             }
        //         }
        //         vm.close();
        //     });
        // }

        function getCategoryList() {
             $scope.Category=$scope.query.name==""?"All":$scope.query.name;
            apiService.post('/api/Category/GetAllCategory', $scope.query, getCategoryListSuccess,RequestFailed);
            vm.close();
        }
        function getCategoryListSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoData = false;
                vm.categoryList = response.data.Content;
                vm.categoryCount = response.data.TotalRows;
                $scope.selected = [];
            } else {
                if (response.data.Content.length === 0) {
                    vm.NoData = true;
                    vm.categoryList = [];
                    vm.categoryCount = 0;
                    notificationService.displaymessage('No Category list found.');
                } else {
                    notificationService.displaymessage('Unable to get grade level list at the moment. Please try again after some time.');
                }
            }
        }
        function RequestFailed(response) {
            notificationService.displaymessage("Please Try Again")
        }
        
        function addCategory(registerCategory) {
            if (registerCategory.$valid) {
                var model = {
                    Name: registerCategory.Category.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                $http.post(HOST_URL.url + '/api/Category/AddCategory', model).then(function (response) {
                    if (response.data.IsSuccess === true) {
                        vm.showProgressbar = false;
                        registerCategory.$setPristine();
                        registerCategory.$setUntouched();
                        vm.Category = angular.copy(vm.master);
                        getCategoryList();
                        notificationMessageController('Category added Successfully.');
                    } else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.Message);
                    }
                });
            }
        }

        vm.addCategory = addCategory;
        vm.showProgressbar = false;
        vm.master = {};
        vm.Category = {
            Name: ''
        };
        vm.GetCategoryList = getCategoryList;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.categoryCount = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            name: '',
            AgencyId: localStorage.agencyId,
        };
        getCategoryList();

        function deleteCategory(data) {
            vm.promise = categoryService.DeleteCategoryById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess === true) 
                {
                    notificationMessageController('Category deleted successfully.')
                    getCategoryList();
                } else {

                    notificationMessageController('Unable to get category list at the moment. Please try again after some time.');
                }
            });
        }

        function reset() {
            $scope.query.name = '';
            $scope.Category="";
            getCategoryList();
        }

        function showDeleteCategoryConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this category permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteCategory(data);
            }, function () {
                $scope.hide();
            });
        }

        vm.showDeleteCategoryConfirmDialoue = showDeleteCategoryConfirmDialoue;

        function showUpdateCategoryDialoue(event, data) {
            $mdDialog.show({
                controller: updateCategoryController,
                controllerAs: 'vm',
                templateUrl: 'app/Tools/Category/views/updatecategory.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { categorydetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        vm.showUpdateCategoryDialoue = showUpdateCategoryDialoue;
    }

    angular
        .module('app.category')
        .controller('CategoryController', categoryController)
        .controller('UpdateCategoryController', updateCategoryController);

    /* @ngInject */
})();

