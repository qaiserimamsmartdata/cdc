(function () {
    'use strict';
    angular
        .module('app.category')
        .config(moduleConfig);
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.editsettings', {
                url: '/settings',
                templateUrl: 'app/Tools/EditSettings/views/editsettings.html',
                // set the controller to load for this page
                // controller: 'CategoryController',
                // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.categorylist', {
                url: '/category',
                templateUrl: 'app/Tools/Category/views/categorylist.tmpl.html',
                // set the controller to load for this page
                controller: 'CategoryController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.addcategory', {
                url: '/category/add',
                templateUrl: 'app/Tools/Category/views/addcategory.tmpl.html',
                // set the controller to load for this page
                controller: 'CategoryController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.usersetting', {
                url: '/setting/:id',
                templateUrl: 'app/Tools/UserSettings/setting.tmpl.html',
                // set the controller to load for this page
                controller: 'settingpageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
              .state('triangular.superusersetting', {
                url: '/supersetting/:id',
                templateUrl: 'app/Tools/UserSettings/setting.tmpl.html',
                // set the controller to load for this page
                controller: 'settingpageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"SuperAdmin",$window);
                    }
                }
            })

            .state('triangular.toolsParticipantAttendance', {
                url: '/participantattendance',
                templateUrl: 'app/Tools/ParticipantAttendance/views/conveniencemode.tmpl.html',
                // set the controller to load for this page
                // controller: 'CategoryController',
                // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })            
            .state('triangular.toolStaffs', {
                url: '/staff',
                templateUrl: 'app/Tools/Staff/views/staff.html',
                // set the controller to load for this page
                // controller: 'CategoryController',
                // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })

            .state('triangular.holiday', {
                url: '/manageholiday',
                templateUrl: 'app/Tools/Holiday/views/holiday.tmpl.html',
                controller: 'holidayController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            
            .state('triangular.FoodManagement', {
                url: '/ManageFood',
                templateUrl: 'app/Tools/FoodManagement/views/food.tmpl.html',
                // controller: 'FoodManagementMasterController',
                //controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            // .state('triangular.enhancement', {
            //     url: '/enhancement',
            //     templateUrl: 'app/Tools/Enhancement/views/enhancement.tmpl.html',
            //     controller: 'enhancementController',
            //     controllerAs: 'vm',
            //     data: {
            //         layout: {
            //             contentClass: 'layout-column'
            //         }
            //     }
            // })

            ;
        // .state('triangular.student', {
        //     url: '/student',
        //     templateUrl: 'app/Tools/Student/views/student.html',
        //     // set the controller to load for this page
        //     // controller: 'CategoryController',
        //     // controllerAs: 'vm',
        //     // layout-column class added to make footer move to
        //     // bottom of the page on short pages
        //     data: {
        //         layout: {
        //             contentClass: 'layout-column'
        //         }
        //     }
        // });
        triMenuProvider.addMenu({
            name: 'Tools',
            icon: 'fa fa-wrench',
            type: 'dropdown',
            permission: 'viewTools',
            priority: 8,
            children: [
                {
                    name: 'Settings',
                    state: 'triangular.editsettings',
                    type: 'dropdown',
                    children: [{
                        name: 'Class',
                        state: 'triangular.classes',
                        type: 'link'
                    }, {
                            name: 'Family',
                            state: 'triangular.family',
                            type: 'link'
                        },
                        {
                            name: 'Attendance',
                            state: 'triangular.toolsParticipantAttendance',
                            type: 'link'
                        }, 
                        {
                            name: 'Staff',
                            state: 'triangular.toolsStaff',
                            type: 'link'
                        },
                            {
                            name: 'Food Management',
                            state: 'triangular.FoodManagement',
                            type: 'link'
                        },
                         
                        {
                            name: 'Holiday List',
                            state: 'triangular.holiday',
                            type: 'link'
                        }
                        // {
                        //     name: 'Enhancement',
                        //     state: 'triangular.enhancement',
                        //     type: 'link'
                        // }
                    ]
                },
                {
                    name: 'User Settings',
                    state: 'triangular.usersetting',
                    type: 'link'
                },
                ,
                //  {
                //     name: 'Class',
                //     state: 'triangular.classes',
                //     type: 'link'
                // },
                // {
                //     name: 'Family',
                //     state: 'triangular.family',
                //     type: 'link'
                // },
                //  {
                //     name: 'Staff',
                //     state: 'triangular.toolStaffs',
                //     type: 'link'
                // },
                // {
                //     name: 'Student',
                //     state: 'triangular.student',
                //     type: 'link'
                // }
            ]
        });
    }
    /* @ngInject */
})();