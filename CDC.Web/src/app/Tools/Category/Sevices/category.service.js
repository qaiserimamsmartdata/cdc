﻿(function() {
    'use strict';
    angular
        .module('app.category')
        .factory('categoryService', categoryService);
    categoryService.$inject = ['$http', '$q', 'HOST_URL'];

    function categoryService($http, $q, HOST_URL) {

        return {
            GetCategoryListService: getCategoryListService,
            DeleteCategoryById: deleteCategoryById
        };

        function getCategoryListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Category/GetCategory', model).success(function(response) {
                deferred.resolve(response);
            }).error(function() {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }

        function deleteCategoryById(categoryId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Category/DeleteCategoryById?CategoryId=' + categoryId).success(function(response) {
                deferred.resolve(response);
            }).error(function() {
                deferred.reject({ message: 'Really bad' });
            });
            return deferred.promise;
        }
    }
    /* @ngInject */
})();