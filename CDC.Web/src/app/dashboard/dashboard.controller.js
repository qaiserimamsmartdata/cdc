(function () {
    'use strict';

    angular
        .module('app.dashboard')

        .controller('DashBoardController', DashBoardController)
        .controller('ShowClassDetailController', ShowClassDetailController);

    /* @ngInject */
    function DashBoardController($scope, $timeout, $mdDialog, $stateParams, notificationService, $mdToast, $rootScope, $state, $interval, CommonService, $localStorage, $q, HOST_URL) {
        var vm = this;   
        var data = $stateParams.obj;
        vm.showpopup = false;
        localStorage.ParticipantAttendancePage = false;

        // if (localStorage.DashboardagencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        if ($stateParams.notifyTrial != null)
            notificationService.displaymessage($stateParams.notifyTrial);
      
        vm.ImageUrlPath = HOST_URL.url;
        GetAllCount();
        vm.FamilyCount = 0;
        vm.StaffCount = 0;
        vm.AttendanceCount = 0;
        vm.EnhancementCount = 0;
        vm.ParticipantCount = 0;
        vm.EventCount = 0;
        vm.redirectToFamily = redirectToFamily;
        vm.redirectToAllParticipants = redirectToAllParticipants;
        vm.redirectToAllStaff = redirectToAllStaff;
        vm.redirectToParticipantAttendance = redirectToParticipantAttendance;


        vm.classListLimit = 3;
        vm.buttonText = 1;
        vm.moreClick = moreClick;
        vm.lessClick = lessClick;
        function moreClick() {
            vm.classListLimit = vm.ClassList.length
            vm.buttonText = 0;
        }
        function lessClick() {
            vm.classListLimit = 3
            vm.buttonText = 1;
        }
        function redirectToFamily() {
            $state.go("triangular.families")
        }
        function redirectToAllParticipants() {
            $state.go("triangular.allparticipants")
        }
        function redirectToAllStaff() {
            $state.go("triangular.staff")
        }
        function redirectToParticipantAttendance() {
            $state.go("triangular.attendancehistory")
        }

        function GetAllCount() {
            var model = {
                AgencyId: localStorage.agencyId
            }
            var myPromise = CommonService.getAllCount(model);

            myPromise.then(function (resolve) {
                if (resolve.IsSuccess == true) {
                    ///For Trial Period Expires
                    var notifyTrial = null;
                    if (resolve.IsTrial == true) {
                        // var TrialStartDate = moment(new Date());
                        // var TrialEndDate = moment(new Date(resolve.TrialEnd));
                        // var days = TrialEndDate.diff(TrialStartDate, 'Days');
                        var days=resolve.trialDays;
                        if (days == 1)
                            notificationService.displaymessage("Your Trial Period will be expired within a day.");
                        else if (days <= 10 && days > 1)
                            notificationService.displaymessage("Your Trial Period will be expired within" + " " + days + " days.");

                        else if (days <= 0) {
                            // notificationService.displaymessage('You trial period expired,you can login.');
                            $state.go('authentication.login');
                            return;
                        }

                    }
                    /////////////////////////////////////////////////////////////////////////////////////////
                    vm.EventCount=resolve.Content.EventCount;
                    vm.StaffCount = resolve.Content.StaffCount;
                    vm.AttendanceCount = resolve.Content.AttendanceCount;
                    vm.EnhancementCount = resolve.Content.EnhancementCount;

                    vm.ParticipantCount = resolve.Content.ParticipantCount;

                    vm.TotalEnrolledParticipants = resolve.Content.TotalEnrolledParticipants;
                    // vm.TimeClockUsers = $sessionStorage.TimeClockUsers + $sessionStorage.AgencyTimeClockUsers;
                    vm.TimeClockStaffCount = resolve.Content.TimeClockStaffCount;
                    localStorage.TimeClockStaffCount=vm.TimeClockStaffCount;
                    vm.TimeClockUsers = resolve.Content.PricingPlanTimeClockMaxCount + resolve.Content.TimeClockUserPlanMaxCount;
                    vm.ClassList = resolve.Content.ClassList;

                    vm.AttendanceList = resolve.Content.AttendanceList;
                    vm.UnAssignedEnrollParticipantCount = resolve.Content.UnAssignedEnrollParticipantCount;
                    vm.FutureEnrollParticipantCount = resolve.Content.FutureEnrollParticipantCount;
                    vm.CurrentEnrollParticipantCount = resolve.Content.CurrentEnrollParticipantCount;

                    vm.ToDoList = resolve.Content.ToDoList;

                    vm.EnhancementList = resolve.Content.EnhancementList;

                    GetPricingPlanCount();
                    randomData();

                }

            }, function (reject) {

            });

        };


        function GetPricingPlanCount() {
            var model = {
                AgencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getPricingPlanCount(model);
            vm.promise.then(function (response) {
                if (response.IsSuccess) {
                    localStorage.MaxNumberOfParticipants = response.MaximumParticipants;
                    localStorage.setItem('PricingPlan',JSON.stringify(response.Content.PricingPlan));

                    vm.PlanParticipantCount =JSON.parse(localStorage.PricingPlan).MaxNumberOfParticipants;
                    vm.PlanName = JSON.parse(localStorage.PricingPlan).Name;
                    vm.PlanPrice = '$' + JSON.parse(localStorage.PricingPlan).Price;
                }
            });
        }


        vm.UpdatePricingPlan = UpdatePricingPlan;
        function UpdatePricingPlan() {
            $state.go('PriceUpdate.pricing');
        }
        vm.UpdateTimeClockUsersPlan = UpdateTimeClockUsersPlan;
        function UpdateTimeClockUsersPlan() {
            $state.go('TimeClockUsersPrice.timeclockusersplanpricing');
        }

        // function init() {

        //     $timeout(function() {
        //         $rootScope.$broadcast('newMailNotification');

        //         var toast = $mdToast.simple()
        //             .textContent('You have new email messages!')
        //             .action('View')
        //             .highlightAction(true)
        //             .position('bottom right');
        //         $mdToast.show(toast).then(function(response) {
        //             if (response == 'ok') {
        //                 $state.go('triangular.email.inbox');
        //             }
        //         });
        //     }, 5000);
        // }

        // init

        // init();


        //   vm.labels = ['Download Sales', 'Instore Sales', 'Mail Order'];
        // vm.options = {
        //     datasetFill: false
        // };

        /////////////

        // function randomData() {
        //     vm.data = [];
        //     for(var label = 0; label < vm.labels.length; label++) {
        //         vm.data.push(Math.floor((Math.random() * 100) + 1));
        //     }
        // }

        // init

        // randomData();

        // Simulate async data update
        // $interval(randomData, 5000);


        vm.labels = ['Enrolled', 'Unassigned', 'Future'];
        vm.options = {
            datasetFill: false
        };

        /////////////
        vm.GotoClassList = GotoClassList;
        function GotoClassList() {
            $state.go("triangular.classlist");
        }

        vm.GotoToDoList = GotoToDoList;
        function GotoToDoList() {
            $state.go("triangular.todo");
        }

        function randomData() {
            if (vm.ParticipantCount > 0)
                // vm.data = [Math.round((vm.CurrentEnrollParticipantCount * 100) / vm.ParticipantCount), Math.round((vm.UnAssignedEnrollParticipantCount * 100) / vm.ParticipantCount), Math.round((vm.FutureEnrollParticipantCount * 100) / vm.ParticipantCount)];
                vm.data = [vm.CurrentEnrollParticipantCount, vm.UnAssignedEnrollParticipantCount, vm.FutureEnrollParticipantCount];
            // for (var label = 0; label < vm.labels.length; label++) {
            //     vm.data.push(Math.floor((Math.random() * 100) + 1));
            // }
        }

        // function detailClassInfo($event, data) {
        //     // ClassService.SetId(data.ID);
        //     $scope.$storage.classId = data.ID;
        //     $sessionStorage.ClassId = data.ID;
        //     $scope.$storage.className = data.ClassName;
        //     $state.go('triangular.classdetails', {id: data.ID} );
        // };

        vm.showClassDetailDialogue = showClassDetailDialogue
        function showClassDetailDialogue(id) {
            $mdDialog.show({
                controller: ShowClassDetailController,
                controllerAs: 'vm',
                templateUrl: 'app/dashboard/classdetail.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                ClassData: { id: id } // Only for -xs, -sm breakpoints.
            });
        };

        // init

        // randomData();

        // Simulate async data update
        $interval(randomData, 5000);
    }



    function ShowClassDetailController($filter, $scope, notificationService, $http, $state, $stateParams, $localStorage, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService, AgencyService, ClassData) {
        var vm = this;
        
        if (localStorage.agencyId == undefined || null) {
            $state.go('authentication.login');
            notificationService.displaymessage("You must login first.");
            return;
        }
        vm.CheckUncheckOngoing = CheckUncheckOngoing;
        var ClassId = ClassService.Id;
        vm.query = {
            AgencyId: localStorage.agencyId
        };

        $scope.id = ClassData.id;
        vm.showProgressbar = false;
        vm.disabled = true;
        vm.setClassStartDate = setClassStartDate;
        vm.setClassEndDate = setClassEndDate;
        vm.disabledClassEndDatePicker = true;
        vm.class = {
            StartTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            EndTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            Mon: true,
            Tue: true,
            Wed: true,
            Thu: true,
            Fri: true,
            Sat: true,
            Sun: true
        };
        vm.getLocations = getLocations;
        vm.getCategory = getCategory;
        vm.getRooms = getRooms;
        vm.getSessions = getSessions;
        vm.AllAgeYears = CommonService.getYears();
        vm.AllAgeMonths = CommonService.getMonths();
        getLocations();
        getCategory();
        getSessions();
        GetClassList();
        getClassStatus();
        function CheckUncheckOngoing() {
            if (vm.class.OnGoing == true)
                vm.class.RegistrationStartDate = vm.class.ClassStartDate = vm.class.ClassEndDate = null;
        }
        function setClassStartDate(DateVal) {
            vm.disabled = false;
            vm.class.ClassStartDate = vm.class.ClassEndDate = null;
            var tempDate = new Date(DateVal);
            tempDate.setDate(tempDate.getDate() + 1);
            vm.classstartdate = tempDate;
        };
        function setClassEndDate(DateVal) {
            vm.disabledClassEndDatePicker = false;
            vm.class.ClassEndDate = null;
            var tempDate = new Date(DateVal);
            tempDate.setDate(tempDate.getDate() + 1);
            vm.classenddate = tempDate;
        };
        function GetClassList() {
            var model = {
                ClassId: ClassData.id,
                TimeZone: localStorage.TimeZone
            }
            vm.promise = ClassService.GetClassListByIdService(model);
            vm.promise.then(function (response) {
                if (response.classList.ID > 0) {
                    vm.class = response.classList;
                    vm.class.CategoryId = response.classList.CategoryId.toString();
                    vm.class.SessionId = response.classList.SessionId == null ? '' : response.classList.SessionId.toString();
                    vm.class.ClassStatusId = response.classList.ClassStatusId.toString();
                    vm.class.AgencyLocationInfoId = response.classList.AgencyLocationInfoId.toString();
                    vm.class.RoomId = response.classList.RoomId.toString();
                    vm.class.MinAgeFrom = response.classList.MinAgeFrom.toString();
                    vm.class.MinAgeTo = response.classList.MinAgeTo.toString();
                    vm.class.MaxAgeFrom = response.classList.MaxAgeFrom.toString();
                    vm.class.MaxAgeTo = response.classList.MaxAgeTo.toString();
                    vm.class.RegistrationStartDate = vm.class.RegistrationStartDate == null ? '' : (new Date(response.classList.RegistrationStartDate));
                    vm.class.ClassStartDate = vm.class.ClassStartDate == null ? '' : (new Date(response.classList.ClassStartDate));
                    vm.class.ClassEndDate = vm.class.ClassEndDate == null ? '' : (new Date(response.classList.ClassEndDate));
                    vm.class.AgeCutOffDate = new Date(response.classList.AgeCutOffDate);
                    vm.class.StartTime = new Date(new Date().toDateString() + ' ' + response.classList.StartTime),
                        vm.class.EndTime = new Date(new Date().toDateString() + ' ' + response.classList.EndTime);
                    vm.class.AgencyLocationInfoId = response.classList.AgencyLocationInfoId.toString();
                    getRooms(vm.class.AgencyLocationInfoId);
                    $scope.selected = [];
                }
                else {
                    if (response.classList.ID == 0) {
                        vm.classList = [];
                        vm.classCount = 0;
                        NotificationMessageController('Invalid found.');
                    }
                    else {
                        NotificationMessageController('Unable to get class list at the moment. Please try again after some time.');
                    }
                }
            });
        };

        function getCategory() {
            vm.promise = CommonService.getCategory(vm.query);
            vm.promise.then(function (response) {
                if (response.category.length > 0) {
                    vm.AllCategories = null;
                    vm.AllCategories = eval(response.category);
                }
            });
        };
        function getRooms(LocationId) {
            vm.query.AgencyLocationInfoId = LocationId;
            vm.promise = CommonService.getRooms(vm.query);
            vm.promise.then(function (response) {
                if (response.room.length > 0) {
                    vm.AllRooms = null;
                    vm.AllRooms = eval(response.room);
                }
            });
        };
        function getSessions() {
            vm.promise = CommonService.getSessions();
            vm.promise.then(function (response) {
                if (response.session.length > 0) {
                    vm.AllSessions = null;
                    vm.AllSessions = eval(response.session);
                }
            });
        };
        function getLocations() {
            vm.promise = CommonService.getAgencyLocations(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllLocations = null;
                    vm.AllLocations = eval(response.Content);
                }
            });
        };

        function getClassStatus() {
            vm.promise = CommonService.getClassStatus();
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllClassStatus = null;
                    vm.AllClassStatus = eval(response.Content);
                }
            });

        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
        vm.cancel = cancel;
        function cancel() {
            $mdDialog.cancel();
        };
    }


})();