(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.dashboard', {
                url: '/dashboard',
                templateUrl: 'app/dashboard/dashboard.tmpl.html',
                // set the controller to load for this page
                controller: 'DashBoardController',
                controllerAs: 'vm',
                params: {
                    obj: null,
                    notifyTrial: null
                },
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        //contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }

            });

        triMenuProvider.addMenu({
            name: 'Dashboard',
            icon: 'fa fa-tachometer',
            type: 'link',
            permission: 'viewDashBoard',
            state: 'triangular.dashboard',
            priority: 1
            // children: [{
            //     name: 'Start Page',
            //     state: 'triangular.seed-page',
            //     type: 'link'
            // }]
        });
    }
})();
