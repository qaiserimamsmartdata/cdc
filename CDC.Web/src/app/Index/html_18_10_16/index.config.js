(function () {
    'use strict';

    angular
        .module('app.startup')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('startup', {
                abstract: true,
                views: {
                    'root': {
                        templateUrl: 'app/Index/html_18_10_16/layout/index.tmpl.html'
                    }
                },
                data: {
                    permissions: {
                        only: ['viewAuthentication']
                    }
                }
            })

            .state('startup.index', {
                // url: '/signup',
                // templateUrl: 'app/examples/authentication/signup/signup.tmpl.html',
                // controller: 'SignupController',
                // controllerAs: 'vm'
                url: '/index',
                templateUrl: 'app/Index/html_18_10_16/index.html',
                // controller: 'AddAgencyController',
                // controllerAs: 'vm'
            })
     
    }
})();
