(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.allparticipants', {
                url: '/participants/all',
                templateUrl: 'app/Participants/views/allparticipants.tmpl.html',
                // set the controller to load for this page
                controller: 'allParticipantsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        //contentClass: 'layout-column'
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }

            })
            .state('triangular.enrolledparticipants', {
                url: '/participants/enrolled',
                templateUrl: 'app/Participants/views/enrolledparticipants.tmpl.html',
                // set the controller to load for this page
                controller: 'EnrolledParticipantsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.unenrolledparticipants', {
                url: '/participants/Unassigned',
                templateUrl: 'app/Participants/views/unenrolledparticipants.tmpl.html',
                controller: 'UnEnrolledParticipantsController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.futureenrolledparticipants', {
                url: '/participants/futureenrolled',
                templateUrl: 'app/Participants/views/futureenrolledparticipants.tmpl.html',
                controller: 'FutureEnrolledParticipantsController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column  overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.attendancehistory', {
                url: '/participants/attendanceHistory',
                templateUrl: 'app/Participants/views/attendanceHistory.tmpl.html',
                // set the controller to load for this page
                controller: 'AttendanceHistoryController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.enrollmenthistory', {
                url: '/participants/enrollmentHistory',
                templateUrl: 'app/Participants/views/enrollmenthistory.tmpl.html',
                controller: 'EnrollmentHistoryController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.parentallparticipants', {
                url: '/all',
                templateUrl: 'app/Participants/views/allparticipants.tmpl.html',
                // set the controller to load for this page
                controller: 'allParticipantsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        //contentClass: 'layout-column'
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }

            })
            .state('triangular.parentenrolledparticipants', {
                url: '/enrolled',
                templateUrl: 'app/Participants/views/enrolledparticipants.tmpl.html',
                // set the controller to load for this page
                controller: 'EnrolledParticipantsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }
            })
            .state('triangular.parentunenrolledparticipants', {
                url: '/Unassigned',
                templateUrl: 'app/Participants/views/unenrolledparticipants.tmpl.html',
                controller: 'UnEnrolledParticipantsController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }
            })
            .state('triangular.parentfutureenrolledparticipants', {
                url: '/futureenrolled',
                templateUrl: 'app/Participants/views/futureenrolledparticipants.tmpl.html',
                controller: 'FutureEnrolledParticipantsController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column  overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }
            })
            .state('triangular.parentattendancehistory', {
                url: '/attendanceHistory',
                templateUrl: 'app/Participants/views/attendanceHistory.tmpl.html',
                // set the controller to load for this page
                controller: 'AttendanceHistoryController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }
            })
            .state('triangular.parentenrollmenthistory', {
                url: '/enrollmentHistory',
                templateUrl: 'app/Participants/views/enrollmenthistory.tmpl.html',
                controller: 'EnrollmentHistoryController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }
            }).state('triangular.addparticipantmodule', {
                url: '/AddParticipant',
                templateUrl: 'app/ParticipantModule/views/addparticipant.tmpl.html',
                controller: 'AddParticipantModuleController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.participantmodulelist', {
                url: '/ParticipantList',
                templateUrl: 'app/ParticipantModule/views/participantlist.tmpl.html',
                controller: 'ParticipantModuleListing',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            });

        triMenuProvider.addMenu({
            name: 'Participants',
            icon: 'fa fa-child',
            type: 'dropdown',
            permission: 'viewParticipants',
            priority: 4,
            children: [
                {
                    name: 'Add Participant',
                    state: 'triangular.addparticipantmodule',
                    priority: 1,
                    type: 'link',
                    permission: 'viewAgencyAddParticipant'
                },
                {
                    name: 'Participant List',
                    state: 'triangular.participantmodulelist',
                    priority: 2,
                    type: 'link',
                    permission: 'viewAgencyListParticipant'
                },
                {
                    name: 'All',
                    state: 'triangular.allparticipants',
                    type: 'link',
                    permission: 'viewAllparticipants'
                }
                , {
                    name: 'Current Enrollment',
                    state: 'triangular.enrolledparticipants',
                    type: 'link',
                    permission: 'viewEnrolledparticipants'
                },
                {
                    name: 'Future Enrollment',
                    state: 'triangular.futureenrolledparticipants',
                    type: 'link',
                    permission: 'viewFutureenrolledparticipants'
                },
                {
                    name: 'Unassigned',
                    state: 'triangular.unenrolledparticipants',
                    type: 'link',
                    permission: 'viewUnenrolledparticipants'
                },
                {
                    name: 'Attendance History',
                    state: 'triangular.attendancehistory',
                    type: 'link',
                    permission: 'viewAttendancehistory'
                },
                {
                    name: 'Enrollment History',
                    state: 'triangular.enrollmenthistory',
                    type: 'link',
                    permission: 'viewEnrollmenthistory'
                },
                {
                    name: 'All',
                    state: 'triangular.parentallparticipants',
                    type: 'link',
                    permission: 'viewParentAllparticipants'
                }
                , {
                    name: 'Current Enrollment',
                    state: 'triangular.parentenrolledparticipants',
                    type: 'link',
                    permission: 'viewParentEnrolledparticipants'
                },
                {
                    name: 'Future Enrollment',
                    state: 'triangular.parentfutureenrolledparticipants',
                    type: 'link',
                    permission: 'viewParentFutureEnrolledparticipants'
                },
                {
                    name: 'Unassigned',
                    state: 'triangular.parentunenrolledparticipants',
                    type: 'link',
                    permission: 'viewParentUnenrolledparticipants'
                },
                {
                    name: 'Attendance History',
                    state: 'triangular.parentattendancehistory',
                    type: 'link',
                    permission: 'viewParentAttendancehistory'
                },
                {
                    name: 'Enrollment History',
                    state: 'triangular.parentenrollmenthistory',
                    type: 'link',
                    permission: 'viewParentEnrollmenthistory'
                }
            ]
        });
    }
})();
