(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('allParticipantsController', allParticipantsController)
        .controller('AddAllEnrollParticipantController', AddAllEnrollParticipantController);
    /* @ngInject */
    function allParticipantsController($scope, $mdDialog, $state, ParticipantService, AgencyService, $localStorage, notificationService, filerService, HOST_URL, CommonService) {
        var vm = this;
        $scope.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        vm.goToSchedule = goToSchedule;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        vm.ImageUrlPath = HOST_URL.url;
        vm.detailParticipantInfo = detailParticipantInfo;
        vm.columns = {
            Image: 'Image',
            FirstName: 'Name',
            LastName: 'Last Name',
            MappedClass: 'Enrolled in Class',
            Schedule: 'Enroll',
            Active: 'Active',
            Gender: 'Gender',
            BirthDate: 'Birth Date',
            Age: 'Age',
            Grade: 'Grade',
            EnrolledClasses: 'Enrolled Classes',
            ID: 'ID'
        };
        ///For Family Portal
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        /////////////////////////
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FamilyId: vm.FamilyId,
            name: '',
            ParticipantName: '',
            AgencyId: localStorage.agencyId
        };
        vm.GetStudentScheduleList = GetStudentScheduleList;
        vm.reset = reset;
        GetEnrolledParticipantsCount();
        GetPricingPlanCount();
        GetStudentScheduleList();
        $scope.selected = [];
        function GetEnrolledParticipantsCount() {
            vm.promise = CommonService.getEnrolledParticipantsCount($scope.query);
            vm.promise.then(function (responseForEnrolledParticipants) {
                if (responseForEnrolledParticipants.IsSuccess) {
                    localStorage.TotalEnrolledParticipants = responseForEnrolledParticipants.TotalEnrolledParticipants;
                }
            });
        }
        function detailParticipantInfo(data) {
            data.RedirectFrom = "allparticipants";
            if (localStorage.Portal == 'AGENCY')
                $state.go('triangular.studentdetails', { obj: data });
            else
                $state.go('triangular.parentstudentdetails', { obj: data });
        };
        function GetPricingPlanCount() {
            vm.promise = CommonService.getPricingPlanCount($scope.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess) {
                    localStorage.MaxNumberOfParticipants = response.MaximumParticipants;
                }
            });
        }
        vm.showProgressbar = true;
        vm.participantCount = 0
        function GetStudentScheduleList() {
            $scope.StudentName = $scope.query.StudentName == undefined ? "All" : $scope.query.StudentName;
            vm.promise = ParticipantService.getAllParticipant($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.NoToDoData = false;
                    vm.StudentList = response.Content;
                    vm.participantCount = response.TotalRows;
                    vm.showProgressbar = false;
                    $scope.selected = [];

                }
                else {
                    if (response.Content.length == 0) {
                        vm.NoToDoData = true;
                        vm.StudentList = [];
                        vm.participantCount = 0;
                        vm.showProgressbar = false;
                        notificationService.displaymessage('No Participant list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get participant list at the moment. Please try again after some time.');
                    }
                }
                vm.close()
            });
        };


        function goToSchedule(participant) {
            if (localStorage.TotalEnrolledParticipants == localStorage.MaxNumberOfParticipants) {
                var confirm = $mdDialog.confirm()
                    .title('Sorry,Enrolled participants have reached maximum limit!')
                    .textContent('Would you like to upgrade plan?.')
                    .ariaLabel('Lucky day')
                    .ok('Please do it!')
                    .cancel('Cancel');

                $mdDialog.show(confirm).then(function () {
                    $state.go('PriceUpdate.pricing');
                }, function () {
                });
            }
            else {
                $mdDialog.show({
                    controller: AddAllEnrollParticipantController,
                    controllerAs: 'vm',
                    templateUrl: 'app/studentSchedule/schedule/views/addschedule.tmpl.html',
                    parent: angular.element(document.body),
                    escToClose: true,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                    items: { participant: participant }
                });
            }
        }

        $scope.hide = function () {
            $mdDialog.hide();
        }

        $scope.cancel = function () {
            $mdDialog.cancel();
        }
        function reset() {
            $scope.query.StudentName = undefined;
            GetStudentScheduleList();
        }
    }
    function AddAllEnrollParticipantController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, items, ClassService) {
        var vm = this;
        vm.showProgressbar = false;
        vm.cancelClick = cancelClick;
        vm.addSchedule = addSchedule;
        $scope.$storage = localStorage;
        vm.scheduleData = {};
        $scope.id = 0;
        $scope.ParticipantName = items.participant.FirstName + ' ' + items.participant.LastName;
        $scope.DisplayText = "Add Schedule";
        vm.Schedule = {};
        vm.Schedule.Student = {};
        var bDate = new Date();
        vm.startDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);
        vm.getFeesAmount = getFeesAmount;
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList($scope.$storage.agencyId);
            vm.promise.then(function (response) {
                debugger
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }
        vm.Fee = Fee;
        function Fee(data) {
            if (data < vm.scheduleData.PaidFees) {
                vm.vm.scheduleData.TutionFee = "";
                notificationService.displaymessage("Discount cannot be more than Fee amount.")
            }
        }
        vm.Discount = Discount;
        function Discount(data) {
            if (data > vm.scheduleData.TutionFees) {
                vm.scheduleData.PaidFees = "";
                notificationService.displaymessage("Discount cannot be more than Fee amount.")
            }
        }
        function GetClassById() {
            var model = {
                ClassId: vm.scheduleData.ClassId,
                TimeZone: localStorage.TimeZone
            }
            vm.promise = ClassService.GetClassListByIdService(model);
            vm.promise.then(function (response) {
                debugger
                if (response.classList.ID > 0) {
                    if (response.classList.OnGoing == null || response.classList.OnGoing == 0) {
                        $scope.ClassStartDate = new Date(response.classList.ClassStartDate);
                        $scope.EndDate = new Date(response.classList.ClassEndDate);
                    }
                }
            });
        }
        function getFeesAmount(data) {
            $scope.ClassStartDate = "";
            $scope.EndDate = "";
            GetClassById();
            vm.AllClasses.forEach(function (element) {
                if (vm.scheduleData.ClassId == element.ID) {
                    vm.scheduleData.TutionFees = element.Fees;
                    vm.FeesType = element.Name;
                }
            }, this);

            var model = {
                "ClassId": data,
                "AgencyId": localStorage.agencyId
            }
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/User/EnrolledCapacity', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
                if (response == true) {
                    vm.scheduleData.ClassId = null;
                    notificationService.displaymessage('Class has reached its maximum capacity.');

                }
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }
        function addSchedule(scheduleform, model) {

            if (scheduleform.$valid) {
                vm.showProgressbar = true;
                model.AgencyId = localStorage.agencyId;
                model.StudentId = items.participant.ID;
                $http.post(HOST_URL.url + '/api/StudentSchedule/AddSchedule', model).then(function (response) {
                    debugger
                    if (response.data.IsSuccess == true) {
                        if (response.data.IsExist == true) {
                            vm.showProgressbar = false;
                            notificationService.displaymessage(response.data.ReturnMessage[0]);
                        } else {
                            if (response.data.IsEligible == false) {
                                vm.showProgressbar = false;
                                vm.scheduleData.StartDate = "";
                                notificationService.displaymessage(response.data.ReturnMessage[0]);
                            }
                            else {
                                notificationService.displaymessage(response.data.ReturnMessage[0]);
                                cancelClick();
                                $state.go('triangular.allparticipants', {}, { reload: true });
                            }

                        }
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
    }
})();