(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('FutureEnrolledParticipantsController', FutureEnrolledParticipantsController);

    /* @ngInject */
    function FutureEnrolledParticipantsController($scope, $mdDialog, $state, ParticipantService, AgencyService, $localStorage, notificationService, filerService, $rootScope, HOST_URL) {
        var vm = this;
        $scope.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.ImageUrlPath = HOST_URL.url;
        $scope.selected = [];
        vm.detailParticipantInfo = detailParticipantInfo;
        vm.columns = {
            Image: 'Image',
            FirstName: 'Name',
            LastName: 'Last Name',
            ClassName: 'Class Name',
            // SessionName: 'Session',
            // Session: 'Session',
            StartDate: 'StartDate',
            // EndDate: 'EndDate',
            // Mon: 'Mon',
            // Tue: 'Tue',
            // Wed: 'Wed',
            // Thu: 'Thu',
            // Fri: 'Fri',
            // Sat: 'Sat',
            Room: 'Room',
            ID: 'ID'
        };
        function detailParticipantInfo(data) {
            // ClassService.SetId(data.ID);
            data.RedirectFrom = "FutureEnrolledParticipants";
              if(localStorage.Portal=='AGENCY')
            $state.go('triangular.studentdetails', { obj: data });
            else
            $state.go('triangular.parentstudentdetails', { obj: data });
        };
        //For Family Portal
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        ///////////
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            // FamilyId: localStorage.familyDetails.ID,
            FamilyId: vm.FamilyId,
            name: '',
            ParticipantName: '',
            AgencyId: localStorage.agencyId
        };
        vm.GetFutureEnrolledStudent = GetFutureEnrolledStudent;
        vm.showDeleteFutureStudentConfirmDialoue = showDeleteFutureStudentConfirmDialoue;
        vm.showUpdateFutureStudentDialoue = showUpdateFutureStudentDialoue;
        vm.reset = reset;

        GetFutureEnrolledStudent();

        $rootScope.$on("GetFutureEnrolledStudent", function () {
            GetFutureEnrolledStudent();
        });
        vm.showProgressbar = true;
        vm.participantScheduleCount =0;
        function GetFutureEnrolledStudent() {
            $scope.StudentName=$scope.query.ParticipantName==""?"All":$scope.query.ParticipantName;
            vm.promise = ParticipantService.getFutureEnrolledStudent($scope.query);
            vm.promise.then(function (response) {
                if (response.enrollList.Content.length > 0) {
                    vm.NoToDoData = false;
                    vm.ScheduleList = response.enrollList.Content;
                    vm.participantScheduleCount = response.enrollList.TotalRows;
                    vm.showProgressbar = false;
                    $scope.selected = [];

                }
                else {
                    if (response.enrollList.Content.length == 0) {
                        vm.NoToDoData = true;
                        vm.ScheduleList = [];
                        vm.participantScheduleCount = 0;
                        vm.showProgressbar = false;
                        notificationService.displaymessage('No Future enrolled participant list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get future enroll list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        };

        function showUpdateFutureStudentDialoue(event, data) {
            $mdDialog.show({
                controller: 'UpdateFutureEnrolledStudentController',
                controllerAs: 'vm',
                templateUrl: 'app/Participants/views/updatefutureenrolledstudent.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                items: { studentdetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
        };

        function showDeleteFutureStudentConfirmDialoue(event, data) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this student permanently?')
                //   .textContent('All of this branch data and all "Branch Managers/Delivery Personals" associated with this class  will be deleted.')
                .ariaLabel('Lucky day')
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                DeleteFutureEnrolledStudent(data);
            }, function () {
                $scope.hide();
            });
        }

        function DeleteFutureEnrolledStudent(model) {
            vm.showProgressbar = true;
            vm.promise = FamilyService.DeleteFutureEnrolledStudent(model);
            vm.promise.then(function (response) {
                if (response.StudentInfo.IsSuccess == true) {
                    notificationService.displaymessage('Participant deleted Successfully.');
                    vm.showProgressbar = false;
                    GetFutureEnrolledStudent();
                }
                else {
                    notificationService.displaymessage('Error occured while deleting, Please try again later.');
                }
            })
        }

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function reset() {
            $scope.query.ParticipantName = '';
            GetFutureEnrolledStudent();
        }
    }
})();