(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('EnrolledParticipantsController', EnrolledParticipantsController);

    /* @ngInject */
    function EnrolledParticipantsController($scope, $rootScope, $mdDialog, $state, ParticipantService, AgencyService, $localStorage, notificationService, filerService, HOST_URL, apiService, CommonService) {
        var vm = this;

        $scope.$storage = localStorage;
        if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
            $state.go('authentication.login');
            notificationService.displaymessage("You must login first.");
            return;
        }
        localStorage.ParticipantAttendancePage = false;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.ImageUrlPath = HOST_URL.url;
        vm.detailParticipantInfo = detailParticipantInfo;
        $scope.selected = [];
        vm.CancelEnrollment = CancelEnrollment;
        // if (localStorage.familyDetails.ID == undefined)
        //     $state.go('triangular.families');
        vm.columns = {
            Image: 'Image',
            FirstName: 'Name',
            LastName: 'Last Name',
            ClassName: 'Class Name',
            // SessionName: 'Session',
            // Session: 'Session',
            StartDate: 'Start Date',
            // EndDate: 'EndDate',
            // Mon: 'Mon',
            // Tue: 'Tue',
            // Wed: 'Wed',
            // Thu: 'Thu',
            // Fri: 'Fri',
            // Sat: 'Sat',
            Room: 'Room',
            ID: 'ID'
        };
        function detailParticipantInfo(data) {
            data.RedirectFrom = "enrolledparticipants";
            // ClassService.SetId(data.ID);
            if (localStorage.Portal == 'AGENCY')
                $state.go('triangular.studentdetails', { obj: data });
            else
                $state.go('triangular.parentstudentdetails', { obj: data });
        };

        //For Family Portal
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        ///////////
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            // FamilyId: localStorage.familyDetails.ID,
            FamilyId: vm.FamilyId,
            name: '',
            ParticipantName: '',
            AgencyId: localStorage.agencyId
        };
        vm.GetStudentScheduleList = GetStudentScheduleList;
        vm.reset = reset;

        GetStudentScheduleList();
        $rootScope.$on("GetStudentScheduleList", function () {
            GetStudentScheduleList();
        });
        vm.showProgressbar = true;
        vm.participantScheduleCount = 0;
        function GetStudentScheduleList() {
            $scope.StudentName = $scope.query.ParticipantName == "" ? "All" : $scope.query.ParticipantName;
            vm.promise = ParticipantService.getStudentScheduleListByIdService($scope.query);
            vm.promise.then(function (response) {

                if (response.enrollList.Content.length > 0) {
                    vm.NoToDoData = false;
                    vm.ScheduleList = response.enrollList.Content;
                    vm.participantScheduleCount = response.enrollList.TotalRows;
                    vm.showProgressbar = false;
                    $scope.selected = [];

                }
                else {
                    if (response.enrollList.Content.length == 0) {
                        vm.NoToDoData = true;
                        vm.ScheduleList = [];
                        vm.participantScheduleCount = 0;
                        vm.showProgressbar = false;
                        notificationService.displaymessage('No Current enrolled participant list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get unenroll list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        };
        GetEnrolledParticipantsCount();
        function GetEnrolledParticipantsCount() {
            vm.promise = CommonService.getEnrolledParticipantsCount($scope.query);
            vm.promise.then(function (responseForEnrolledParticipants) {
                if (responseForEnrolledParticipants.IsSuccess) {
                    localStorage.TotalEnrolledParticipants = responseForEnrolledParticipants.TotalEnrolledParticipants;
                }
            });
        }
        vm.goToSchedule = goToSchedule;
        function goToSchedule(participant) {
            // $scope.$storage.studentIdforSchedule = id;

            // if (localStorage.TotalEnrolledParticipants == localStorage.MaxNumberOfParticipants) {
            //     var confirm = $mdDialog.confirm()
            //         .title('Sorry,Enrolled Participants have reached maximum limit!')
            //         .textContent('Would you like to upgrade plan?.')
            //         .ariaLabel('Lucky day')
            //         .ok('Please do it!')
            //         .cancel('Cancel');

            //     $mdDialog.show(confirm).then(function () {
            //         $state.go('PriceUpdate.pricing');
            //     }, function () {
            //     });
            // }
            // else {
            $mdDialog.show({
                controller: TransferEnrollParticipantController,
                controllerAs: 'vm',
                templateUrl: 'app/Participants/views/transferschedule.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                items: { participant: participant }
            });
            // }
        }

        function CancelEnrollment(schedule) {

            var confirm = $mdDialog.confirm()
                .title('Are you sure to cancel the enrollment of ' + schedule.StudentFullName + ' for class ' + schedule.ClassName + '?')
                .ariaLabel('Lucky day')
                .ok('Ok')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {

                var EnrollMentId = schedule.ID;
                apiService.post('/api/EnrollClass/DeleteEnrollMent', EnrollMentId,
                    CancelEnrollmentSuccess,
                    Failed);
            });

            vm.close();
        }

        function CancelEnrollmentSuccess(response) {
            if (response.data.CancelenrollList.IsSuccess == true) {
                notificationService.displaymessage('Enrollment cancelled successfully.');
                GetStudentScheduleList();
            }
            else {
                notificationService.displaymessage('Something went wrong.');
            }

        }

        function Failed() {
            notificationService.displaymessage('Something went wrong.')
        }

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function reset() {
            $scope.query.ParticipantName = '';
            $scope.StudentName = "";
            GetStudentScheduleList();
        }
    }
    function TransferEnrollParticipantController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, items) {
        var vm = this;
        vm.showProgressbar = false;
        vm.cancelClick = cancelClick;
        vm.addSchedule = addSchedule;
        $scope.$storage = localStorage;
        vm.scheduleData = {};
        $scope.id = 0;
        $scope.ParticipantName = items.participant.FirstName + ' ' + items.participant.LastName;
        $scope.DisplayText = "Add Schedule";
        vm.Schedule = {};
        vm.Schedule.Student = {};
        var bDate = new Date();
        vm.startDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);
        vm.getFeesAmount = getFeesAmount;

        GetClass_Room_Lesson();
        vm.Discount = Discount;
        function Discount(data) {
            if (data > vm.scheduleData.TutionFees) {
                vm.scheduleData.PaidFees = "";
                notificationService.displaymessage("Discount cannot be more than Fee amount.")
            }
        }
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList($scope.$storage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                    vm.AllClasses.forEach(function (element, index) {
                        if (element.ID == items.participant.ClassId)
                            vm.AllClasses.splice(index, 1);
                    }, this);
                    // vm.AllClasses =$filter('filter')(vm.AllClasses, {ID:items.participant.ClassId});
                }
            });
        }
        function getFeesAmount() {
            vm.AllClasses.forEach(function (element) {
                if (vm.scheduleData.ClassId == element.ID) {
                    vm.scheduleData.TutionFees = element.Fees;
                    // vm.scheduleData.PaidFees = element.Fees;
                    vm.FeesType = element.Name;
                }
            }, this);
        }
        function addSchedule(scheduleform, model) {
            if (scheduleform.$valid) {
                vm.showProgressbar = true;
                // vm.Schedule.Student.FamilyId = localStorage.familyDetails.ID;
                items.participant.ClassId = model.ClassId;
                items.participant.StartDate = model.StartDate;
                $http.post(HOST_URL.url + '/api/EnrollClass/UpdateFutureEnrolledStudent', items.participant).then(function (response) {
                    if (response.statusText == "OK") {
                        vm.showProgressbar = false;
                        cancelClick();
                        notificationService.displaymessage("Transferred successfully");
                        $rootScope.$emit("GetStudentScheduleList", {});
                    } else {
                        notificationService.displaymessage("Please try again some time");
                        cancelClick();
                    }
                    // $state.go('triangular.studentschedule', {}, { reload: true });

                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        // function NotificationMessageController(message) {
        //     debugger;
        //     $timeout(function () {
        //         $rootScope.$broadcast('newMailNotification');
        //         $mdToast.show({
        //             template: '<md-toast><span flex>' + message + '</span></md-toast>',
        //             position: 'bottom right',
        //             hideDelay: 5000
        //         });
        //     }, 100);
        // };


    }
})();