(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('UpdateFutureEnrolledStudentController', UpdateFutureEnrolledStudentController);

    /* @ngInject */
    function UpdateFutureEnrolledStudentController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, items, $localStorage) {
        var vm = this;
        vm.ImageUrlPath = HOST_URL.url;
        vm.updateFutureEnrolledStudent = updateFutureEnrolledStudent;
        vm.cancelClick = cancelClick;
        vm.showProgressbar = false;
        GetFutureEnrolledStudentDetailbyId();
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/GetClass_Room_Lesson',
                 data:{
                    AgencyId:localStorage.agencyId,
                }
            };
            $http(req).then(function successCallback(response) {
                if (response.data.IsSuccess) {
                    vm.AllClasses = eval(response.data.Content.classList);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        //NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
                    }
                }
            }, function errorCallback(response) {
                //NotificationMessageController('Unable to retreive country list at the moment.');
            });
        }


        function updateFutureEnrolledStudent(UpdateStudent, model) {
            if (UpdateStudent.$valid) {
                // var model = {
                //     FirstName: UpdateStudent.FirstName.$modelValue,
                //     LastName: UpdateStudent.LastName.$modelValue,
                //     ClassId: UpdateStudent.classes.$modelValue,
                //     StartDate: UpdateStudent.StartDate.$modelValue,
                //     AgencyId: localStorage.agencyId,
                //     StudentId: model.StudentId,
                //     ID: model.ID
                    
                // };

                $http.post(HOST_URL.url + '/api/EnrollClass/UpdateFutureEnrolledStudent', model).then(function (response) {
                    if (response.data.response.IsSuccess == true) {
                        vm.showProgressbar = false;
                        // sessionService.setSession(response.data.Content);
                        NotificationMessageController('Paritcipant enrollment updated successfully.');
                        cancelClick();
                        $rootScope.$emit("GetFutureEnrolledStudent", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        cancelClick();
                    }
                });

            }
        }

        function GetFutureEnrolledStudentDetailbyId() {
            vm.futureStudent = items.studentdetail;
            vm.futureStudent.ClassId = (items.studentdetail.ClassId).toString();
            vm.futureStudent.Startdate = new Date(items.studentdetail.Startdate);
            vm.futureStudent.RoomId=items.studentdetail.RoomId;
            vm.futureStudent.IsEnrolled=true;
        }

        function getRooms() {
            vm.promise = CommonService.getRooms();
            vm.promise.then(function (response) {
                if (response.room.length > 0) {
                    vm.AllRooms = null;
                    vm.AllRooms = eval(response.room);
                }
            });

        };

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
})();