(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('UnEnrolledParticipantsController', UnEnrolledParticipantsController)
        .controller('AddEnrollParticipantController', AddEnrollParticipantController);

    /* @ngInject */
    function UnEnrolledParticipantsController($scope, $mdDialog, $state, ParticipantService, $localStorage, notificationService, filerService, HOST_URL, CommonService) {
        $scope.selected = [];
        var vm = this;
        $scope.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.detailParticipantInfo = detailParticipantInfo;
        vm.reset = reset;
        vm.goToSchedule = goToSchedule;
        vm.ImageUrlPath = HOST_URL.url;
        vm.columns = {
            Image: 'Image',
            FirstName: 'Name',
            LastName: 'Last Name',
            Enroll: 'Enroll',
            Gender: 'Gender',
            PhoneNumber: 'Phone Number',
            DateOfBirth: 'Date Of Birth',
            Age: 'Age',
            ID: 'ID'
        };
        function detailParticipantInfo(data) {
            data.RedirectFrom = "UnEnrolledParticipants";
            $state.go('triangular.studentdetails', { obj: data });
        };
        //For Family Portal
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        ///////////
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FamilyId: vm.FamilyId,
            name: '',
            AgencyId: localStorage.agencyId
        };
        vm.GetUnenrollParticipantList = GetUnenrollParticipantList;

        vm.GetEnrolledParticipantsCount = GetEnrolledParticipantsCount;
        GetEnrolledParticipantsCount();
        GetUnenrollParticipantList();
        GetPricingPlanCount();
        function GetEnrolledParticipantsCount() {
            vm.promise = CommonService.getEnrolledParticipantsCount($scope.query);
            vm.promise.then(function (responseForEnrolledParticipants) {
                if (responseForEnrolledParticipants.IsSuccess) {
                    localStorage.TotalEnrolledParticipants = responseForEnrolledParticipants.TotalEnrolledParticipants;
                }
            });
        }
        function GetPricingPlanCount() {
            vm.promise = CommonService.getPricingPlanCount($scope.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess) {
                    localStorage.MaxNumberOfParticipants = response.MaximumParticipants;
                }
            });
        }
        vm.showProgressbar = true;
        vm.UnEnrollListCount = 0;
        function GetUnenrollParticipantList() {
            $scope.StudentName = $scope.query.ParticipantName == undefined ? "All" : $scope.query.ParticipantName;
            vm.promise = ParticipantService.getUnEnrollListService($scope.query);
            vm.promise.then(function (response) {
                vm.NoToDoData = false;
                if (response.unenrollList.Content.length > 0) {
                    vm.UnEnrollList = response.unenrollList.Content;
                    vm.UnEnrollListCount = response.unenrollList.TotalRows;
                    vm.showProgressbar = false;
                    $scope.selected = [];

                }
                else {
                    if (response.unenrollList.Content.length == 0) {
                        vm.NoToDoData = true;
                        vm.UnEnrollList = [];
                        vm.UnEnrollListCount = 0;
                        vm.showProgressbar = false;
                        notificationService.displaymessage('No Unassigned participant list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get unassigned list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        }
        function goToSchedule(participant) {
            // $scope.$storage.studentIdforSchedule = id;
            if (localStorage.TotalEnrolledParticipants == localStorage.MaxNumberOfParticipants) {
                var confirm = $mdDialog.confirm()
                    .title('Sorry,enrolled participants have reached maximum limit!')
                    .textContent('Would you like to upgrade plan?')
                    .ariaLabel('Lucky day')
                    .ok('Please do it.')
                    .cancel('Cancel');

                $mdDialog.show(confirm).then(function () {
                    $state.go('PriceUpdate.pricing');
                }, function () {
                });
            }
            else {
                $mdDialog.show({
                    controller: AddEnrollParticipantController,
                    controllerAs: 'vm',
                    templateUrl: 'app/studentSchedule/schedule/views/addschedule.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: event,
                    escToClose: true,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                    items: { participant: participant }
                });
            }
        }
        $scope.hide = function () {
            $mdDialog.hide();
        }

        $scope.cancel = function () {
            $mdDialog.cancel();
        }
        function reset() {
            $scope.query.ParticipantName = undefined;
            GetUnenrollParticipantList();
        }
    }
    function AddEnrollParticipantController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, items, ClassService) {
        var vm = this;
        vm.showProgressbar = false;
        vm.cancelClick = cancelClick;
        vm.addSchedule = addSchedule;
        $scope.$storage = localStorage;
        vm.scheduleData = {};
        $scope.id = 0;
        $scope.ParticipantName = items.participant.FirstName + ' ' + items.participant.LastName;
        $scope.DisplayText = "Add Schedule";
        vm.Schedule = {};
        vm.Schedule.Student = {};
        var bDate = new Date();
        vm.startDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);
        vm.getFeesAmount = getFeesAmount;
        vm.Discount = Discount;
        function Discount(data) {
            if (data > vm.scheduleData.TutionFees) {
                vm.scheduleData.PaidFees = "";
                notificationService.displaymessage("Discount cannot be more than Fee amount.")
            }
        }
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList($scope.$storage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }

        function GetClassById() {
            var model = {
                ClassId: vm.scheduleData.ClassId,
                TimeZone: localStorage.TimeZone
            }
            vm.promise = ClassService.GetClassListByIdService(model);
            vm.promise.then(function (response) {
                debugger
                if (response.classList.ID > 0) {
                    if (response.classList.OnGoing == null || response.classList.OnGoing == 0) {
                        $scope.ClassStartDate = new Date(response.classList.ClassStartDate);
                        $scope.EndDate = new Date(response.classList.ClassEndDate);
                    }
                }
            });
        }


        function getFeesAmount(data) {
            //Set min schedule date
            debugger
            $scope.ClassStartDate = "";
            $scope.EndDate = "";
            GetClassById();

            vm.AllClasses.forEach(function (element) {
                if (vm.scheduleData.ClassId == element.ID) {
                    vm.scheduleData.TutionFees = element.Fees;                    
                    vm.FeesType = element.Name;
                }
            }, this);

            var model = {
                "ClassId": data,
                "AgencyId": localStorage.agencyId
            }
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/User/EnrolledCapacity', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
                if (response == true) {
                    vm.scheduleData.ClassId = null;
                    notificationService.displaymessage('Class has reached its maximum capacity.');

                }
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }
        function addSchedule(scheduleform, model) {
            debugger
            if (scheduleform.$valid) {
                vm.showProgressbar = true;
                // vm.Schedule.Student.FamilyId = localStorage.familyDetails.ID;
                model.StudentId = items.participant.ID;
                $http.post(HOST_URL.url + '/api/StudentSchedule/AddSchedule', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        if (response.data.IsExist == true) {
                            vm.showProgressbar = false;
                            notificationService.displaymessage(response.data.ReturnMessage[0]);
                        } else {
                            if (response.data.IsEligible == false) {
                                vm.showProgressbar = false;
                                vm.scheduleData.StartDate = "";
                                notificationService.displaymessage(response.data.ReturnMessage[0]);
                            }
                            else {
                                notificationService.displaymessage(response.data.ReturnMessage[0]);
                                cancelClick();
                                $state.go('triangular.enrolledparticipants', {}, { reload: true });
                            }
                        }
                        // $state.go('triangular.studentschedule', {}, { reload: true });
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        // function NotificationMessageController(message) {
        //     debugger;
        //     $timeout(function () {
        //         $rootScope.$broadcast('newMailNotification');
        //         $mdToast.show({
        //             template: '<md-toast><span flex>' + message + '</span></md-toast>',
        //             position: 'bottom right',
        //             hideDelay: 5000
        //         });
        //     }, 100);
        // };


    }
})();