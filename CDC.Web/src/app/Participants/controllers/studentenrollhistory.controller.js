(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('EnrollmentHistoryController', EnrollmentHistoryController);
    //For Showing student attendance history details -Tulesh
    function EnrollmentHistoryController($scope, $mdDialog, $state, $stateParams, $localStorage,
        notificationService, filerService, apiService, ClassService, HOST_URL, CommonService,$filter) {
        var vm = this;
        vm.reset4 = reset4;
        vm.toggleRight4 = filerService.toggleRight4();
        vm.isOpenRight4 = filerService.isOpenRight4();
        vm.close4 = filerService.close4();
        $scope.$storage = localStorage;
         var studentId=0;
         localStorage.ParticipantAttendancePage = false;
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }
        vm.ImageUrlPath = HOST_URL.url;
        vm.columns = {
            // Image: 'Image',
            ParticipantName: 'Participant',
            ClassName: 'Class',
            StartDate: 'Start Date',
            Status: 'Status',
            ID: 'ID'
        };
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-ClassName',
            page: 1,
            AgencyId: $scope.$storage.agencyId,
            StudentId: studentId,
            ParticipantName: '',
            ClassId: 0,
            StartDate: null,
            FamilyId:localStorage.FamilyId
        };
        function reset4() {
            $scope.query.ClassId = 0;
            $scope.query.ParticipantName='';
            GetStudentScheduleList();
        }
        vm.GetStudentScheduleList = GetStudentScheduleList;
        GetStudentScheduleList();
        vm.enrollHistoryListCount=0;
        function GetStudentScheduleList() {
            if ($scope.query.ClassId > 0) {
                var ClassData = $filter('filter')( vm.AllClasses, { ID: $scope.query.ClassId });
                $scope.Class = ClassData[0].ClassName;
            }
            else{
                $scope.Class="All";
            }
            $scope.StudentName = $scope.query.ParticipantName == ""? "All" : $scope.query.ParticipantName;
            vm.promise = ClassService.GetEnrollmentHistory($scope.query);
            vm.promise.then(function (response) {
                if (response.enrollHistory.Content.length > 0) {
                    vm.NoData = false;
                    vm.enrollHistoryList = response.enrollHistory.Content;
                    vm.enrollHistoryListCount = response.enrollHistory.TotalRows;
                    $scope.selected = [];

                }
                else {
                    if (response.enrollHistory.Content.length == 0) {
                        vm.NoData = true;
                        vm.enrollHistoryList = [];
                        vm.enrollHistoryListCount = 0;
                    }
                    else {
                        // NotificationMessageController('Unable to get Schedule list at the moment. Please try again after some time.');
                    }
                }
                vm.close4();
            });
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();