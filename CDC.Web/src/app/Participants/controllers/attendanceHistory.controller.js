(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('AttendanceHistoryController', AttendanceHistoryController);

    /* @ngInject */
    function AttendanceHistoryController($scope, $mdDialog, $state, $localStorage,$filter, notificationService, filerService, apiService, HOST_URL, CommonService) {
        $scope.selected = [];
        var vm = this;
        $scope.$storage = localStorage;
        // if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        localStorage.ParticipantAttendancePage = false;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        vm.attendancestartDate = new Date();

        //vm.disabled = true;
        vm.ImageUrlPath = HOST_URL.url;
        vm.columns = {
            Image: 'Image',
            StudentName: 'Participant',
            ClassName: 'Class Name',
            AttendanceDate: 'Attendance Date',
            DropedByName: 'Sign In By',
            InTime: 'Sign In Time',
            PickupByName: 'Sign Out By',
            OutTime: 'Sign Out Time'
        };
        ///For Family Portal
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        /////////////////////////
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-AttendanceDate',
            page: 1,
            status: 0,
            FromDate: new Date(),
            ToDate: new Date(),
            ParticipantName: '',
            FamilyId: vm.FamilyId,
            AgencyId: localStorage.agencyId,
            TimeZoneName: localStorage.TimeZone,
            ClassId: 0
        };
        var toDate = $scope.query.ToDate;
        $scope.query.FromDate.setDate(toDate.getDate() - 6);
        vm.getAttendanceHistoryList = getAttendanceHistoryList;
        vm.getAttendanceHistoryListSuccess = getAttendanceHistoryListSuccess;
        vm.failed = failed;
        vm.setStartDate = setStartDate;
        vm.MaxDate = new Date();
        vm.setToDate = setToDate;
        function setToDate() {
            $scope.query.ToDate = new Date();
        }
        GetClass_Room_Lesson();
        vm.detailParticipantInfo = detailParticipantInfo;
        function detailParticipantInfo(data) {
            // ClassService.SetId(data.ID);
            data.RedirectFrom = "attendanceHistory";
            if(localStorage.Portal=='AGENCY')
            $state.go('triangular.studentdetails', { obj: data });
            else
            $state.go('triangular.parentstudentdetails', { obj: data });
        };
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }
        getAttendanceHistoryList();
        function getAttendanceHistoryList() {
             if ($scope.query.ClassId > 0) {
                var ClassData = $filter('filter')( vm.AllClasses, { ID: $scope.query.ClassId });
                $scope.Class = ClassData[0].ClassName;
            }
            else{
                $scope.Class="All";
            }
            $scope.StudentName = $scope.query.StudentName == undefined ? "All" : $scope.query.StudentName;
            $scope.fromDate = $scope.query.FromDate;
            $scope.toDate = $scope.query.ToDate;
            apiService.post('/api/StudentAttendance/GetAttendanceHistory', $scope.query,
                getAttendanceHistoryListSuccess,
                failed);
            vm.close();
        }
        vm.showProgressbar = true;
        vm.attendanceHistoryCount = 0;
        function getAttendanceHistoryListSuccess(response) {
            if (response.data.Content.length > 0) {
                vm.NoData = false;
                vm.attendanceHistoryList = response.data.Content;
                vm.attendanceHistoryCount = response.data.TotalRows;
                vm.showProgressbar = false;
                vm.close()
            }
            else {
                vm.NoData = true;
                vm.attendanceHistoryList = [];
                vm.attendanceHistoryCount = 0;
                vm.showProgressbar = false;
                notificationService.displaymessage('No Attendance history list found.');
            }

        }
        function failed() {
            notificationService.displaymessage('No Attendance History found.');
        }
        function setStartDate(DateVal) {
            vm.disabled = false;
            var tempDate = new Date(DateVal);
            vm.attendanceendDate = tempDate;
        };
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function reset() {
            $scope.query.StudentName = undefined;
            $scope.StudentName = "";
            $scope.query.ClassId = 0;
            $scope.query.ToDate = new Date();
            var fromDate = new Date();
            fromDate.setDate($scope.query.ToDate.getDate() - 6);
            $scope.query.FromDate = fromDate;
            getAttendanceHistoryList();
        }
    }
})();