﻿(function () {
    'use strict';

    angular
        .module('app.superadmin')
        .factory('TimeClockUserPlanService', TimeClockUserPlanService);

    TimeClockUserPlanService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function TimeClockUserPlanService($http, $q, HOST_URL) {
        return {
            GetTimeClockUserPlanListService:GetTimeClockUserPlanListService,
            DeleteTimeClockUserPlanById:DeleteTimeClockUserPlanById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            this.Name='';
            this.Id = data.ID;
            this.Name = data.Name;
            this.TimeClockUserPlanData=data;
        };

    function GetTimeClockUserPlanListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/TimeClockUserPlan/GetAllTimeClockUserPlan', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteTimeClockUserPlanById(TimeClockUserPlanId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/TimeClockUserPlan/DeleteTimeClockUserPlan?timeclockuserplanId=' + TimeClockUserPlanId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


    }
})();