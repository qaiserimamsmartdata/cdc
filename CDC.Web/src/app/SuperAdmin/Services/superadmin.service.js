﻿(function () {
    'use strict';

    angular
        .module('app.superadmin')
        .factory('SuperAdminService', SuperAdminService);

    SuperAdminService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function SuperAdminService($http, $q, HOST_URL) {
        return {
            GetAgencyListService:GetAgencyListService,
            DeleteAgencyById:DeleteAgencyById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            this.AgencyName='';
            this.Id = data.ID;
            this.AgencyName = data.Agency;
            this.AgencyData=data;
        };

    function GetAgencyListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/SuperAdmin/GetAllAgency', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteAgencyById(AgencyId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/SuperAdmin/DeleteAgency?agencyId=' + AgencyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };


    }
})();