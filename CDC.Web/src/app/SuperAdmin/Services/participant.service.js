﻿(function () {
    'use strict';

    angular
        .module('app.superadmin')
        .factory('ParticipantPlanService', ParticipantPlanService);

    ParticipantPlanService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function ParticipantPlanService($http, $q, HOST_URL) {
        return {
            GetParticipantPlanListService:GetParticipantPlanListService,
            DeleteParticipantPlanById:DeleteParticipantPlanById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            this.Name='';
            this.Id = data.ID;
            this.Name = data.Name;
            this.ParticipantPlanData=data;
        };

    function GetParticipantPlanListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/ParticipantPlan/GetAllParticipantPlan', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteParticipantPlanById(ParticipantPlanId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/ParticipantPlan/DeleteParticipantPlan?participantplanId=' + ParticipantPlanId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


    }
})();