(function () {
    'use strict';

    angular
        .module('app.superadmin')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider

            .state('triangular.superadmin', {
                url: '/superadmin',
                templateUrl: 'app/SuperAdmin/views/superadmin.tmpl.html',
                controller: 'SuperAdminController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewSuperAdmin']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"SuperAdmin",$window);
                    }
                }
            })
            .state('triangular.paymenthistory', {
                url: '/superadmin/paymenthistory',
                templateUrl: 'app/SuperAdmin/views/paymenthistory.tmpl.html',
                controller: 'PaymentHistoryController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewSuperAdmin']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"SuperAdmin",$window);
                    }
                }
            })
            .state('triangular.participantsubscription', {
                url: '/superadmin/participant',
                templateUrl: 'app/SuperAdmin/views/participant.tmpl.html',
                controller: 'ParticipantPlanController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewSuperAdmin']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"SuperAdmin",$window);
                    }
                }
            })
            .state('triangular.timeclocksubscription', {
                url: '/superadmin/timeclockuser',
                templateUrl: 'app/SuperAdmin/views/timeclockuser.tmpl.html',
                controller: 'TimeClockUserPlanController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewSuperAdmin']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"SuperAdmin",$window);
                    }
                }
            })

            .state('triangular.enhancement', {
                url: '/enhancement',
                templateUrl: 'app/Tools/Enhancement/views/enhancement.tmpl.html',
                controller: 'enhancementController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewSuperAdmin']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"SuperAdmin",$window);
                    }
                }
            })

            ;

        triMenuProvider.addMenu({
            name: 'Super Admin',
            icon: 'fa fa-user',
            type: 'dropdown',
            priority: 1.1,
            permission: 'viewSuperAdmin',
            children: [{
                name: 'Organization List',
                state: 'triangular.superadmin',
                type: 'link'
            },
                {
                    name: 'Subscription',
                    //state: 'triangular.superadmin',
                    type: 'dropdown',
                    children: [{
                        name: 'Pricing Plan',
                        state: 'triangular.participantsubscription',
                        type: 'link'
                    }, {
                            name: 'Time Clock User Plan',
                            state: 'triangular.timeclocksubscription',
                            type: 'link'
                        },
                    ]
                },
                {
                name: 'Payment History',
                state: 'triangular.paymenthistory',
                type: 'link'
            }
            ]
        });

        triMenuProvider.addMenu({
            name: 'Enhancement',
            icon: 'fa fa-edit',
            type: 'link',
            priority: 1.3,
            permission: 'viewSuperAdmin',
            state: 'triangular.enhancement',

        });

    }
})();
