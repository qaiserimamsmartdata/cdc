
(function () {
    'use strict';

    angular
        .module('app.superadmin')
        .controller('ParticipantPlanController', ParticipantPlanController)
        .controller('AddParticipantPlanController', AddParticipantPlanController)
        .controller('UpdateParticipantPlanController', UpdateParticipantPlanController);

    /* @ngInject */
    function ParticipantPlanController($scope, notificationService, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        ParticipantPlanService, CommonService, filerService) {
        var vm = this;
        // $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        // if (localStorage.SuperAdminId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        localStorage.ParticipantAttendancePage = false;
        // vm.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.columns = {
            Name: 'Name',
            Price: 'Price',
            MinNumberOfParticipants: 'MinNumberOfParticipants',
            MaxNumberOfParticipants: 'MaxNumberOfParticipants',
            TimeClockUsers: 'TimeClockUsers',
            ID: 'ID',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.participantplanCount = 0;
        vm.selected = [];
        vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            participant: 0,
            Name: '',
            PositionId: 0,
        };
        //vm.getPositions = getPositions;
        vm.GetParticipantPlanList = GetParticipantPlanList;
        vm.showAddParticipantPlanDialoue = showAddParticipantPlanDialoue;
        vm.showUpdateParticipantPlanDialoue = showUpdateParticipantPlanDialoue;
        vm.showDeleteParticipantPlanConfirmDialoue = showDeleteParticipantPlanConfirmDialoue;

        vm.reset = reset;
        //vm.detailStaffInfo = detailStaffInfo;
        //getPositions();
        GetParticipantPlanList();

        $rootScope.$on("GetParticipantPlanList", function () {
            GetParticipantPlanList();
        });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function GetParticipantPlanList() {
            $scope.Pricinglan = vm.query.Name == "" ? "All" : vm.query.Name;
            vm.promise = ParticipantPlanService.GetParticipantPlanListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.participantplanList = {};
                        vm.participantplanList = response.Content;
                        vm.participantplanCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.participantplanList = [];
                        vm.participantplanCount = 0;
                        NotificationMessageController('No Participant Plans list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get participant plan list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };


        function showAddParticipantPlanDialoue(event, data) {
            $mdDialog.show({
                controller: AddParticipantPlanController,
                controllerAs: 'vm',
                templateUrl: 'app/SuperAdmin/views/addparticipantplan.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                //items: { participantplandetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
        };

        function showUpdateParticipantPlanDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateParticipantPlanController,
                controllerAs: 'vm',
                templateUrl: 'app/SuperAdmin/views/updateparticipantplan.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { participantplandetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
        };

        function showDeleteParticipantPlanConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this Participant Plan permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteParticipantPlan(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteParticipantPlan(data) {
            vm.promise = ParticipantPlanService.DeleteParticipantPlanById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Plan deleted successfully.')
                    GetParticipantPlanList();

                }
                else {
                    NotificationMessageController('Unable to get participant plan list at the moment. Please try again after some time.');
                }
            });
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.Name = '';
            GetParticipantPlanList();
        };



    }
    //New Controller For Add
    function AddParticipantPlanController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q,
        ParticipantPlanService, HOST_URL,CommonService) {
        var vm = this;
        vm.participantplan = {
            Name: '',
            Price: 0,
            MinNumberOfParticipants: 0,
            MaxNumberOfParticipants: 0,
            TimeClockUsers: 0,
            ID: 0
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        vm.addParticipantPlan = addParticipantPlan;
        vm.cancelClick = cancelClick;
        GetPricingPlans();
        function GetPricingPlans() {
            vm.promise = CommonService.getPricingPlans(null);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllPlans = null;
                    vm.AllPlans = eval(response.Content);
                }
            });
        };

        function addParticipantPlan(addParticipantPlan) {
            if (addParticipantPlan.$valid) {
                var model = {
                    ID: vm.participantplan.ID,
                    Name: vm.participantplan.Name,
                    Price: vm.participantplan.Price,
                    MinNumberOfParticipants: vm.participantplan.MinNumberOfParticipants,
                    MaxNumberOfParticipants: vm.participantplan.MaxNumberOfParticipants,
                    TimeClockUsers: vm.participantplan.TimeClockUsers,
                    IsTrial:vm.participantplan.IsTrial
                    // AssociationPlanId:vm.participantplan.AssociationPlanId
                };

                $http.post(HOST_URL.url + '/api/ParticipantPlan/AddUpdateParticipantPlan', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Participant Plan Added Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetParticipantPlanList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function cancelClick() {
            $mdDialog.hide();
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }


    //New Controller For Update
    function UpdateParticipantPlanController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q,
        ParticipantPlanService, HOST_URL, items) {
        var vm = this;
        vm.participantplan = {
            Name: '',
            Price: 0,
            MinNumberOfParticipants: 0,
            MaxNumberOfParticipants: 0,
            TimeClockUsers: 0,
            ID: 0
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        GetParticipantPlanDetailbyId();


        function GetParticipantPlanDetailbyId() {
            vm.participantplan.Name = items.participantplandetail.Name;
            vm.participantplan.Price = items.participantplandetail.Price;
            vm.participantplan.MinNumberOfParticipants = items.participantplandetail.MinNumberOfParticipants;
            vm.participantplan.MaxNumberOfParticipants = items.participantplandetail.MaxNumberOfParticipants;
            vm.participantplan.TimeClockUsers = items.participantplandetail.TimeClockUsers;
            vm.participantplan.ID = items.participantplandetail.ID;
            vm.participantplan.StripePlanId = items.participantplandetail.StripePlanId;
            vm.participantplan.IsTrial = items.participantplandetail.IsTrial;
        }
        vm.updateParticipantPlan = updateParticipantPlan;
        vm.cancelClick = cancelClick;

        function updateParticipantPlan(updateParticipantPlan) {
            if (updateParticipantPlan.$valid) {
                var model = {
                    ID: vm.participantplan.ID,
                    Name: vm.participantplan.Name,
                    Price: vm.participantplan.Price,
                    MinNumberOfParticipants: vm.participantplan.MinNumberOfParticipants,
                    MaxNumberOfParticipants: vm.participantplan.MaxNumberOfParticipants,
                    TimeClockUsers: vm.participantplan.TimeClockUsers,
                    StripePlanId: vm.participantplan.StripePlanId,
                    IsTrial:vm.participantplan.IsTrial
                };

                $http.post(HOST_URL.url + '/api/ParticipantPlan/AddUpdateParticipantPlan', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Participant Plan updated Successfully !');
                        cancelClick();
                        $rootScope.$emit("GetParticipantPlanList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

})();