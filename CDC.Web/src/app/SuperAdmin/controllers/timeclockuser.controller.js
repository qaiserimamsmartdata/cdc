
(function () {
    'use strict';

    angular
        .module('app.superadmin')
        .controller('TimeClockUserPlanController', TimeClockUserPlanController)
        .controller('AddTimeClockUserPlanController', AddTimeClockUserPlanController)
        .controller('UpdateTimeClockUserPlanController', UpdateTimeClockUserPlanController);



    /* @ngInject */
    function TimeClockUserPlanController($scope, notificationService, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        TimeClockUserPlanService, CommonService, filerService) {
        var vm = this;
        // $scope.$storage = localStorage;
        // //   $scope.$storage = localStorage;
        // if (localStorage.SuperAdminId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        // vm.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.columns = {
            Name: 'Name',
            Price: 'Price',
            MaxNumberOfTimeClockUsers: 'MaxNumberOfTimeClockUsers',
            ID: 'ID',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.timeclockuserplanCount = 0;
        vm.selected = [];
        vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            timeclockuser: 0,
            Name: '',
            PositionId: 0,
        };

        vm.GetTimeClockUserPlanList = GetTimeClockUserPlanList;
        vm.showAddTimeClockUserPlanDialoue = showAddTimeClockUserPlanDialoue;
        vm.showUpdateTimeClockUserPlanDialoue = showUpdateTimeClockUserPlanDialoue;
        vm.showDeleteTimeClockUserPlanConfirmDialoue = showDeleteTimeClockUserPlanConfirmDialoue;
        vm.reset = reset;
        GetTimeClockUserPlanList();

        $rootScope.$on("GetTimeClockUserPlanList", function () {
            GetTimeClockUserPlanList();
        });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function GetTimeClockUserPlanList() {
            $scope.Pricinglan = vm.query.Name == "" ? "All" : vm.query.Name;
            vm.promise = TimeClockUserPlanService.GetTimeClockUserPlanListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.timeclockuserplanList = {};
                        vm.timeclockuserplanList = response.Content;
                        vm.timeclockuserplanCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.timeclockuserplanList = [];
                        vm.timeclockuserplanCount = 0;
                        NotificationMessageController('No Time Clock User plan list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get timeclockuser plan list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };


        function showAddTimeClockUserPlanDialoue(event, data) {
            $mdDialog.show({
                controller: AddTimeClockUserPlanController,
                controllerAs: 'vm',
                templateUrl: 'app/SuperAdmin/views/addtimeclockuserplan.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            })
        };

        function showUpdateTimeClockUserPlanDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateTimeClockUserPlanController,
                controllerAs: 'vm',
                templateUrl: 'app/SuperAdmin/views/updatetimeclockuserplan.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { timeclockuserplandetail: data },
                fullscreen: $scope.customFullscreen
            })
        };

        function showDeleteTimeClockUserPlanConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this TimeClockUser Plan permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteTimeClockUserPlan(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteTimeClockUserPlan(data) {
            vm.promise = TimeClockUserPlanService.DeleteTimeClockUserPlanById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Plan deleted successfully.');
                    GetTimeClockUserPlanList();
                }
                else {
                    NotificationMessageController('Unable to get TimeClockUser plan list at the moment. Please try again after some time.');
                }
            });
        };


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.Name = '';
            GetTimeClockUserPlanList();
        };



    }
    //New Controller For Add
    function AddTimeClockUserPlanController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q,
        TimeClockUserPlanService, HOST_URL) {
        var vm = this;
        vm.timeclockuserplan = {
            Name: '',
            Price: 0,
            MaxNumberOfTimeClockUsers: 0,
            ID: 0
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        vm.addTimeClockUserPlan = addTimeClockUserPlan;
        vm.cancelClick = cancelClick;

        function addTimeClockUserPlan(addTimeClockUserPlan) {
            if (addTimeClockUserPlan.$valid) {
                var model = {
                    ID: vm.timeclockuserplan.ID,
                    Name: vm.timeclockuserplan.Name,
                    Price: vm.timeclockuserplan.Price,
                    MaxNumberOfTimeClockUsers: vm.timeclockuserplan.MaxNumberOfTimeClockUsers,
                };

                $http.post(HOST_URL.url + '/api/TimeClockUserPlan/AddUpdateTimeClockUserPlan', model).then(function (response) {

                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('TimeClockUser Plan Added Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetTimeClockUserPlanList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }


    //New Controller For Update
    function UpdateTimeClockUserPlanController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q,
        TimeClockUserPlanService, HOST_URL, items) {
        var vm = this;
        vm.timeclockuserplan = {
            Name: '',
            Price: 0,
            MaxNumberOfTimeClockUsers: 0,
            ID: 0
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        GetTimeClockUserPlanDetailbyId();

        function GetTimeClockUserPlanDetailbyId() {
            vm.timeclockuserplan.Name = items.timeclockuserplandetail.Name;
            vm.timeclockuserplan.Price = items.timeclockuserplandetail.Price;
            vm.timeclockuserplan.MaxNumberOfTimeClockUsers = items.timeclockuserplandetail.MaxNumberOfTimeClockUsers;
            vm.timeclockuserplan.ID = items.timeclockuserplandetail.ID;
            vm.timeclockuserplan.StripePlanId = items.timeclockuserplandetail.StripePlanId;
        }
        vm.updateTimeClockUserPlan = updateTimeClockUserPlan;
        vm.cancelClick = cancelClick;

        function updateTimeClockUserPlan(updateTimeClockUserPlan) {
            if (updateTimeClockUserPlan.$valid) {
                var model = {
                    ID: vm.timeclockuserplan.ID,
                    Name: vm.timeclockuserplan.Name,
                    Price: vm.timeclockuserplan.Price,
                    MaxNumberOfTimeClockUsers: vm.timeclockuserplan.MaxNumberOfTimeClockUsers,
                    StripePlanId: vm.timeclockuserplan.StripePlanId
                };

                $http.post(HOST_URL.url + '/api/TimeClockUserPlan/AddUpdateTimeClockUserPlan', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('TimeClockUser Plan updated Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetTimeClockUserPlanList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

})();