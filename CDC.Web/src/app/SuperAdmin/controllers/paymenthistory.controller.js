
(function () {
    'use strict';

    angular
        .module('app.superadmin')
        .controller('PaymentHistoryController', PaymentHistoryController);

    /* @ngInject */
    function PaymentHistoryController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        notificationService, PaymentHistoryService, filerService, $window) {
        var vm = this;
        // $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        // if (localStorage.SuperAdminId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }

        vm.printDiv = printDiv;
        function printDiv(printable) {

            var printContents = document.getElementById(printable).innerHTML;
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();
        }
        localStorage.ParticipantAttendancePage = false;
        // vm.$storage = localStorage;

        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.printClick = printClick;
        vm.columns = {
            AgencyName: 'Organization',
            PhoneNumber: 'Phone Number',
            Email: 'Email',
            PlanName: 'Plan',
            PlanAmount: 'Amount',
            CreatedDate: 'Date',
            StartDate: 'Start Date',
            ExpiryDate: 'Expiry Date',
            EventType: 'Payment',
            ID: 'ID',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.agencyCount = 0;
        vm.selected = [];
        vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-CreatedDate',
            page: 1,
            StatusId: 0,
            agency: 0,
            AgencyName: '',
            Email: '',
            FromDate: new Date((new Date()).setMonth((new Date()).getMonth() - 1)),
            ToDate: new Date(),
            PaymentStatus: ''
        };
        vm.GetAgencyPaymeHistoryList = GetAgencyPaymeHistoryList;
        vm.editAgency = editAgency;
        vm.reset = reset;
        vm.MaxDate = new Date();
        vm.setToDate = setToDate;
        function setToDate() {
            vm.query.ToDate = new Date();
        }
        GetAgencyPaymeHistoryList();

        function GetAgencyPaymeHistoryList() {
            $scope.Organization = vm.query.AgencyName == "" ? "All" : vm.query.AgencyName;
            $scope.Email = vm.query.Email == "" ? "All" : vm.query.Email;
            $scope.PaymentStatus = vm.query.PaymentStatus == "" ? "All" : vm.query.PaymentStatus == "invoice.payment_succeeded" ? "Succeeded" : "Failed";
            vm.promise = PaymentHistoryService.GetAgencyPaymentHistory(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.agencyPaymentList = {};
                        vm.agencyPaymentList = response.Content;
                        vm.agencyPaymentListCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.agencyPaymentList = [];
                        vm.agencyPaymentListCount = 0;
                        NotificationMessageController('No payment history found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get payment history at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
        function editAgency($event, data) {
            $state.go('triangular.superusersetting', { id: data.OrganizationID });
        };
        function reset() {
            vm.query.AgencyName = '';
            vm.query.FromDate = new Date((new Date()).setMonth((new Date()).getMonth() - 1));
            vm.query.ToDate = new Date();
            vm.query.Email = '';
            vm.query.PaymentStatus = '';
            GetAgencyPaymeHistoryList();
        };
        function printClick() {
            $window.print();
        }
    }
})();