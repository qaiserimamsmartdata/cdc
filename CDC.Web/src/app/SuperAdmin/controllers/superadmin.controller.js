
(function () {
    'use strict';

    angular
        .module('app.superadmin')
        .controller('SuperAdminController', SuperAdminController);

    /* @ngInject */
    function SuperAdminController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        SuperAdminService, notificationService, CommonService, AgencyService, filerService) {
        var vm = this;
        $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        // if (localStorage.SuperAdminId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }

        localStorage.ParticipantAttendancePage = false;
        // vm.$storage = localStorage;

        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.columns = {
            AgencyName: 'Organization Name',
            PhoneNumber: 'Phone Number',
            Email: 'Email',
            Address: 'Address',
            ID: 'ID',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.agencyCount = 0;
        vm.selected = [];
        vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            agency: 0,
            AgencyName: '',
            PositionId: 0,
        };
        //vm.getPositions = getPositions;
        vm.GetAgencyList = GetAgencyList;
        vm.editAgency = editAgency;
        vm.GoToPricing= GoToPricing;
        vm.showDeleteAgencyConfirmDialoue = showDeleteAgencyConfirmDialoue;
        vm.reset = reset;
        //vm.detailStaffInfo = detailStaffInfo;
        //getPositions();
        GetAgencyList();



        function GetAgencyList() {
            $scope.Organization=vm.query.AgencyName==""?"All":vm.query.AgencyName;
            vm.promise = SuperAdminService.GetAgencyListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.agencyList = {};
                        vm.agencyList = response.Content;
                        vm.agencyCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.agencyList = [];
                        vm.agencyCount = 0;
                        NotificationMessageController('No Organization list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get organization list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };

        function editAgency($event, data) {
            // vm.agencyId = data.ID;
            // vm.$storage.agencyName = data.AgencyName;
            // $state.go('triangular.usersetting');
            $state.go('triangular.superusersetting', { id: data.ID });
        };

        function GoToPricing() {
            
            $state.go('Price.pricing', {Redirection:'Super'});
        }

        function showDeleteAgencyConfirmDialoue(event, data) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this organization permanently?')
                //   .textContent('All of this branch data and all "Branch Managers/Delivery Personals" associated with this class  will be deleted.')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteAgency(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteAgency(data) {
            vm.promise = SuperAdminService.DeleteAgencyById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Organization deleted successfully.')
                    GetAgencyList();
                }
                else {
                    NotificationMessageController('Unable to get organization list at the moment. Please try again after some time.');
                }
            });
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.AgencyName = '';
            GetAgencyList();
        };



    }
})();