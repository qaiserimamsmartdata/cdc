(function() {
    'use strict';

    angular
        .module('app.parentportaldashboard')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
        .state('triangular.parentportaldashboard', {
            url: '/parent/dashboard',
            templateUrl: 'app/parentportaldashboard/parentdashboard.tmpl.html',
            // set the controller to load for this page
            controller: 'ParentDashBoardController',
            controllerAs: 'vm',
           
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column overlay-10'
                }
            },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"FAMILY",$window);
                    }
                }
        });

        triMenuProvider.addMenu({
            name: 'Dashboard',
            icon: 'fa fa-tachometer',
            type: 'link',
            permission: 'viewParentDashBoard',
            state: 'triangular.parentportaldashboard',
            priority: 1
            // children: [{
            //     name: 'Start Page',
            //     state: 'triangular.seed-page',
            //     type: 'link'
            // }]
        });
    }
})();
