(function () {
    'use strict';

    angular
        .module('app.parentportaldashboard')
        .controller('ParentDashBoardController', ParentDashBoardController);

    /* @ngInject */
    function ParentDashBoardController($scope, $timeout, $mdDialog, $stateParams, notificationService, $mdToast, $rootScope, $state, $interval, CommonService, $localStorage, $q, HOST_URL) {
        var vm = this;
        //vm.checkCookieIntro=checkCookieIntro;
        $scope.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        var data = $stateParams.obj;
        vm.showpopup = false;
        vm.ImageUrlPath = HOST_URL.url;
        if (localStorage.FamilyId == undefined || null) {
            $state.go('authentication.login');
            notificationService.displaymessage("You must login first.");
            return;
        }
        // vm.detailClassInfo=detailClassInfo;

        // GetPricingPlanCount();

        // if (data == false) {
        //     $scope.ShouldAutoStart = true;
        // }
        // $scope.ShouldAutoStart = true;
        // $timeout(
        // function showmodel() {
        //     vm.showpopup=true;
        //     var id = '#dialog';

        //     //Get the screen height and width
        //     var maskHeight = $(document).height();
        //     var maskWidth = $(window).width();

        //     //Set heigth and width to mask to fill up the whole screen
        //     $('#mask').css({ 'width': maskWidth, 'height': maskHeight });

        //     //transition effect
        //     $('#mask').fadeIn(500);
        //     $('#mask').fadeTo("slow", 0.9);

        //     //Get the window height and width
        //     var winH = $(window).height();
        //     var winW = $(window).width();

        //     //Set the popup window to center
        //     $(id).css('top', winH / 2 - $(id).height() / 2);
        //     $(id).css('left', winW / 2 - $(id).width() / 2);

        //     //transition effect
        //     $(id).fadeIn(2000);

        //     //if close button is clicked
        //     $('.window .close').click(function (e) {
        //         //Cancel the link behavior
        //         e.preventDefault();

        //         $('#mask').hide();
        //         $('.window').hide();
        //     });

        //     //if mask is clicked
        //     $('#startButton').click(function () {
        //         $(this).hide();
        //         $('.window').hide();
        //     });
        // }
        // , 100);
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        // $scope.CompletedEvent = function (scope) {
        //     console.log("Completed Event called");
        // };

        // $scope.ExitEvent = function (scope) {
        //     console.log("Exit Event called");
        // };

        // $scope.ChangeEvent = function (targetElement, scope) {
        //     console.log("Change Event called");
        //     console.log(targetElement);  //The target element
        //     console.log(this);  //The IntroJS object
        // };

        // $scope.BeforeChangeEvent = function (targetElement, scope) {
        //     console.log("Before Change Event called");
        //     console.log(targetElement);
        // };

        // $scope.AfterChangeEvent = function (targetElement, scope) {
        //     console.log("After Change Event called");
        //     console.log(targetElement);
        // };

        // $scope.IntroOptions = {
        //     steps: [
        //         {
        //             element: document.querySelector('#step1'),
        //             intro: "<b>Add Family to start.</b>",
        //             position: 'bottom'
        //         },
        //         {
        //             element: document.querySelectorAll('#step2')[0],
        //             intro: "<b>Add participants into family registeration.</b>",
        //             position: 'bottom'
        //         },
        //         {
        //             element: document.querySelector('#step3'),
        //             intro: "<b>Add staff to classes you've registered.</b>",
        //             position: 'bottom'
        //         },
        //         {
        //             element: document.querySelector('#step4'),
        //             intro: "<b>You can see the signed in participants here!</b>",
        //             position: 'bottom'

        //         },
        //         {
        //             element: document.querySelector('#step5'),
        //             intro: "<b>You are all set to go!</b>",
        //             position: 'bottom'

        //         },


        //     ],
        //     showStepNumbers: false,
        //     exitOnOverlayClick: true,
        //     exitOnEsc: true,
        //     nextLabel: '<i class="fa fa-hand-o-right" aria-hidden="true"></i>',
        //     prevLabel: '<i class="fa fa-hand-o-left" aria-hidden="true"></i>',
        //     skipLabel: 'Exit',
        //     doneLabel: 'Go!'
        // };

        // document.getElementById('startButton').onclick = function () {
        //     introJs().setOption('doneLabel', 'Next page').start().oncomplete(function () {
        //         // window.location.href = 'app/families/views/addFamily.tmpl.html?multipage=true';
        //         window.location.href = '#/families/add?multipage=true';
        //     });
        // };

        //     function checkCookieIntro(){
        //    var cookie=getCookie("mySite");

        //    if (cookie==null || cookie=="") {
        //       setCookie("mySite", "1",365);
        //         //change this to whatever function you need to call to run the intro
        //       }
        // }

        // document.getElementById('startButton').onclick = function () {
        //     introJs().setOption('doneLabel', 'Next page').start().oncomplete(function () {
        //         window.location.path = '/families/add/:multipage?';
        //     });
        // };
        //  intro.js('.firstStep').start();
        //introJs().goToStep(5).start();
        vm.ImageUrlPath = HOST_URL.url;
        GetAllCount();
        vm.FamilyCount = 0;
        vm.StaffCount = 0;
        vm.AttendanceCount = 0;
        vm.EnhancementCount = 0;
        vm.ParticipantCount = 0;
        vm.EventCount = 0;
        vm.ContactCount = 0;
        vm.DailyStatusCount = 0;

        //  vm.AgencyCount = 0;
        vm.redirectToFamily = redirectToFamily;
        vm.redirectToEvents = redirectToEvents;
        vm.redirectToAllParticipants = redirectToAllParticipants;
        vm.redirectToAllStaff = redirectToAllStaff;
        vm.redirectToParticipantAttendance = redirectToParticipantAttendance;
        vm.redirectToLeave = redirectToLeave;


        vm.classListLimit = 3;
        vm.buttonText = 1;
        vm.moreClick = moreClick;
        vm.lessClick = lessClick;
        function moreClick() {
            vm.classListLimit = vm.ClassList.length
            vm.buttonText = 0;
        }
        function lessClick() {
            vm.classListLimit = 3
            vm.buttonText = 1;
        }
        function redirectToEvents() {
            $state.go("triangular.familyevent");
        }
        function redirectToFamily() {
            $state.go("triangular.parentfamilies")
        }
        function redirectToAllParticipants() {
            $state.go("triangular.parentallparticipants")
        }
        function redirectToAllStaff() {
            $state.go("triangular.staff")
        }
        function redirectToParticipantAttendance() {
            $state.go("triangular.parentattendancehistory")
        }
        function redirectToLeave() {
            $state.go("triangular.parentdailystatus")
        }

        function GetAllCount() {
            var model = {
                FamilyId: localStorage.FamilyId,
                TimeZone: localStorage.TimeZone,
                 AgencyId :localStorage.agencyId
            }
            vm.promise = CommonService.getParentDashboardAllData(model);

            vm.promise.then(function (resolve) {
                debugger
                if (resolve.IsSuccess == true) {
                    vm.ParticipantCount = resolve.Content.ParticipantCount;
                    vm.ContactCount = resolve.Content.ContactCount;
                    vm.AttendanceCount = resolve.Content.AttendanceCount;
                    vm.DailyStatusCount = resolve.Content.DailyStatusCount;
                    vm.EventCount = resolve.Content.EventCount;
                    vm.attendanceList = resolve.Content.attendanceList;


                    // vm.ParticipantCount = resolve.Content.ParticipantCount;

                    // vm.TotalEnrolledParticipants = resolve.Content.TotalEnrolledParticipants;

                    // vm.TimeClockUsers = localStorage.TimeClockUsers + localStorage.AgencyTimeClockUsers;

                    // vm.TimeClockStaffCount = resolve.Content.TimeClockStaffCount;
                    // vm.ClassList = resolve.Content.ClassList;

                    // vm.AttendanceList = resolve.Content.AttendanceList;
                    // vm.UnAssignedEnrollParticipantCount = resolve.Content.UnAssignedEnrollParticipantCount;
                    // vm.FutureEnrollParticipantCount = resolve.Content.FutureEnrollParticipantCount;
                    // vm.CurrentEnrollParticipantCount = resolve.Content.CurrentEnrollParticipantCount;

                    // vm.ToDoList = resolve.Content.ToDoList;

                    // vm.EnhancementList = resolve.Content.EnhancementList;

                    // GetPricingPlanCount();
                    // randomData();

                }

            }, function (reject) {

            });

        };

        vm.showParticipantDetailDialogue = showParticipantDetailDialogue;

        function showParticipantDetailDialogue(participant) {
            $mdDialog.show({
                controller: ShowParticipantDetailsController,
                controllerAs: 'vm',
                templateUrl: 'app/parentportaldashboard/participantSummary.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                items: { studentdetail: participant },
            });
        }

        function GetPricingPlanCount() {
            var model = {
                AgencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getPricingPlanCount(model);
            vm.promise.then(function (response) {
                if (response.IsSuccess) {
                    localStorage.MaxNumberOfParticipants = response.MaximumParticipants;
                    localStorage.setItem('PricingPlan', JSON.Stringify(response.Content.PricingPlan));
                    vm.PlanParticipantCount = JSON.parse(localStorage.PricingPlan).MaxNumberOfParticipants;
                    vm.PlanName = JSON.parse(localStorage.PricingPlan).Name;
                    vm.PlanPrice = '$' + JSON.parse(localStorage.PricingPlan).Price;
                }
            });
        }


        vm.UpdatePricingPlan = UpdatePricingPlan;
        function UpdatePricingPlan() {
            $state.go('PriceUpdate.pricing');
        }
        vm.UpdateTimeClockUsersPlan = UpdateTimeClockUsersPlan;
        function UpdateTimeClockUsersPlan() {
            $state.go('TimeClockUsersPrice.timeclockusersplanpricing');
        }

        // function init() {

        //     $timeout(function() {
        //         $rootScope.$broadcast('newMailNotification');

        //         var toast = $mdToast.simple()
        //             .textContent('You have new email messages!')
        //             .action('View')
        //             .highlightAction(true)
        //             .position('bottom right');
        //         $mdToast.show(toast).then(function(response) {
        //             if (response == 'ok') {
        //                 $state.go('triangular.email.inbox');
        //             }
        //         });
        //     }, 5000);
        // }

        // init

        // init();


        //   vm.labels = ['Download Sales', 'Instore Sales', 'Mail Order'];
        // vm.options = {
        //     datasetFill: false
        // };

        /////////////

        // function randomData() {
        //     vm.data = [];
        //     for(var label = 0; label < vm.labels.length; label++) {
        //         vm.data.push(Math.floor((Math.random() * 100) + 1));
        //     }
        // }

        // init

        // randomData();

        // Simulate async data update
        // $interval(randomData, 5000);


        vm.labels = ['Enrolled', 'Unassigned', 'Future'];
        vm.options = {
            datasetFill: false
        };

        /////////////
        vm.GotoClassList = GotoClassList;
        function GotoClassList() {
            $state.go("triangular.classlist");
        }

        vm.GotoToDoList = GotoToDoList;
        function GotoToDoList() {
            $state.go("triangular.todo");
        }

        function randomData() {
            if (vm.ParticipantCount > 0)
                // vm.data = [Math.round((vm.CurrentEnrollParticipantCount * 100) / vm.ParticipantCount), Math.round((vm.UnAssignedEnrollParticipantCount * 100) / vm.ParticipantCount), Math.round((vm.FutureEnrollParticipantCount * 100) / vm.ParticipantCount)];
                vm.data = [vm.CurrentEnrollParticipantCount, vm.UnAssignedEnrollParticipantCount, vm.FutureEnrollParticipantCount];
            // for (var label = 0; label < vm.labels.length; label++) {
            //     vm.data.push(Math.floor((Math.random() * 100) + 1));
            // }
        }

        // function detailClassInfo($event, data) {
        //     // ClassService.SetId(data.ID);
        //     $scope.$storage.classId = data.ID;
        //     localStorage.ClassId = data.ID;
        //     $scope.$storage.className = data.ClassName;
        //     $state.go('triangular.classdetails', {id: data.ID} );
        // };

        vm.showClassDetailDialogue = showClassDetailDialogue
        function showClassDetailDialogue(id) {
            $mdDialog.show({
                controller: ShowClassDetailController,
                controllerAs: 'vm',
                templateUrl: 'app/dashboard/classdetail.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                ClassData: { id: id } // Only for -xs, -sm breakpoints.
            });
        };

        // init

        // randomData();

        // Simulate async data update
        $interval(randomData, 5000);
    }


    function ShowParticipantDetailsController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService,
        $timeout, $mdToast, $rootScope, $q, HOST_URL, items, FamilyDetailsService, CommonService, imageUploadService) {
        var vm = this;
        if (items.studentdetail == undefined || items.studentdetail == null) {
            $state.go('triangular.parentportaldashboard');
            return;
        }
        $scope.query = {
            AgencyId: localStorage.agencyId
        };
        vm.ParticipantName = items.studentdetail.FirstName + ' ' + items.studentdetail.LastName;
        GetParticipantDetails();
        function GetParticipantDetails() {
            var model = {
                StudentId: items.studentdetail.ID
            }
            vm.promise = CommonService.getParticipantInformation(model);
            vm.promise.then(function (response) {
                if (response.Content != null) {
                    vm.ParticipantFormsObj = null;
                    vm.ParticipantFormsObj = eval(response.Content);

                    vm.ParticipantFormsObj.Student.Gender = vm.ParticipantFormsObj.Student.Gender.toString();
                    vm.ParticipantFormsObj.Student.DateOfBirth = new Date(vm.ParticipantFormsObj.Student.DateOfBirth);
                    vm.ParticipantFormsObj.Student.MembershipTypeId = (vm.ParticipantFormsObj.Student.MembershipTypeId == undefined || vm.ParticipantFormsObj.Student.MembershipTypeId == null) ? 0 : vm.ParticipantFormsObj.Student.MembershipTypeId.toString();
                    vm.ParticipantFormsObj.GradeLevelId = (vm.ParticipantFormsObj.GradeLevelId == undefined || vm.ParticipantFormsObj.GradeLevelId == null) ? 0 : vm.ParticipantFormsObj.GradeLevelId.toString();
                }
            });
        }

        vm.cancelClick = cancelClick;
        getGradeLevel();
        getMembership();
        function getGradeLevel() {
            vm.promise = CommonService.getGradeLevel($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllGradeLevel = null;
                    vm.AllGradeLevel = eval(response.Content);
                }
            });
        };
        function getMembership() {
            vm.promise = CommonService.getMembership($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllMembership = null;
                    vm.AllMembership = eval(response.Content);
                }
            });
        };


        function cancelClick() {
            $mdDialog.cancel();
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };


    }



    function ShowClassDetailController($filter, $scope, notificationService, $http, $state, $stateParams, $localStorage, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService, AgencyService, ClassData) {
        var vm = this;
        $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        vm.CheckUncheckOngoing = CheckUncheckOngoing;
        var ClassId = ClassService.Id;
        vm.query = {
            AgencyId: localStorage.agencyId
        };

        $scope.id = ClassData.id;
        vm.showProgressbar = false;
        vm.disabled = true;
        vm.setClassStartDate = setClassStartDate;
        vm.setClassEndDate = setClassEndDate;
        vm.disabledClassEndDatePicker = true;
        vm.class = {
            StartTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            EndTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            Mon: true,
            Tue: true,
            Wed: true,
            Thu: true,
            Fri: true,
            Sat: true,
            Sun: true
        };
        vm.getLocations = getLocations;
        vm.getCategory = getCategory;
        vm.getRooms = getRooms;
        vm.getSessions = getSessions;
        vm.AllAgeYears = CommonService.getYears();
        vm.AllAgeMonths = CommonService.getMonths();
        getLocations();
        getCategory();
        getSessions();
        GetClassList();
        function CheckUncheckOngoing() {
            if (vm.class.OnGoing == true)
                vm.class.RegistrationStartDate = vm.class.ClassStartDate = vm.class.ClassEndDate = null;
        }
        function setClassStartDate(DateVal) {
            vm.disabled = false;
            vm.class.ClassStartDate = vm.class.ClassEndDate = null;
            var tempDate = new Date(DateVal);
            tempDate.setDate(tempDate.getDate() + 1);
            vm.classstartdate = tempDate;
        };
        function setClassEndDate(DateVal) {
            vm.disabledClassEndDatePicker = false;
            vm.class.ClassEndDate = null;
            var tempDate = new Date(DateVal);
            tempDate.setDate(tempDate.getDate() + 1);
            vm.classenddate = tempDate;
        };
        function GetClassList() {
            var model = {
                ClassId: ClassData.id,
                TimeZone: localStorage.TimeZone
            }
            vm.promise = ClassService.GetClassListByIdService(model);
            vm.promise.then(function (response) {
                if (response.classList.ID > 0) {
                    vm.class = response.classList;
                    vm.class.CategoryId = response.classList.CategoryId.toString();
                    vm.class.StatusId = response.classList.StatusId.toString();
                    vm.class.SessionId = response.classList.SessionId == null ? '' : response.classList.SessionId.toString();
                    vm.class.StatusId = response.classList.StatusId.toString();
                    vm.class.AgencyLocationInfoId = response.classList.AgencyLocationInfoId.toString();
                    vm.class.RoomId = response.classList.RoomId.toString();
                    vm.class.MinAgeFrom = response.classList.MinAgeFrom.toString();
                    vm.class.MinAgeTo = response.classList.MinAgeTo.toString();
                    vm.class.MaxAgeFrom = response.classList.MaxAgeFrom.toString();
                    vm.class.MaxAgeTo = response.classList.MaxAgeTo.toString();
                    vm.class.RegistrationStartDate = vm.class.RegistrationStartDate == null ? '' : (new Date(response.classList.RegistrationStartDate));
                    vm.class.ClassStartDate = vm.class.ClassStartDate == null ? '' : (new Date(response.classList.ClassStartDate));
                    vm.class.ClassEndDate = vm.class.ClassEndDate == null ? '' : (new Date(response.classList.ClassEndDate));
                    vm.class.AgeCutOffDate = new Date(response.classList.AgeCutOffDate);
                    vm.class.StartTime = new Date(new Date().toDateString() + ' ' + response.classList.StartTime),
                        vm.class.EndTime = new Date(new Date().toDateString() + ' ' + response.classList.EndTime);
                    vm.class.AgencyLocationInfoId = response.classList.AgencyLocationInfoId.toString();
                    getRooms(vm.class.AgencyLocationInfoId);
                    $scope.selected = [];
                }
                else {
                    if (response.classList.ID == 0) {
                        vm.classList = [];
                        vm.classCount = 0;
                        NotificationMessageController('Invalid found.');
                    }
                    else {
                        NotificationMessageController('Unable to get class list at the moment. Please try again after some time.');
                    }
                }
            });
        };

        function getCategory() {
            vm.promise = CommonService.getCategory(vm.query);
            vm.promise.then(function (response) {
                if (response.category.length > 0) {
                    vm.AllCategories = null;
                    vm.AllCategories = eval(response.category);
                }
            });
        };
        function getRooms(LocationId) {
            vm.query.AgencyLocationInfoId = LocationId;
            vm.promise = CommonService.getRooms(vm.query);
            vm.promise.then(function (response) {
                if (response.room.length > 0) {
                    vm.AllRooms = null;
                    vm.AllRooms = eval(response.room);
                }
            });
        };
        function getSessions() {
            vm.promise = CommonService.getSessions();
            vm.promise.then(function (response) {
                if (response.session.length > 0) {
                    vm.AllSessions = null;
                    vm.AllSessions = eval(response.session);
                }
            });
        };
        function getLocations() {
            vm.promise = CommonService.getAgencyLocations(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllLocations = null;
                    vm.AllLocations = eval(response.Content);
                }
            });
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
        vm.cancel = cancel;
        function cancel() {
            $mdDialog.cancel();
        };
    }
})();