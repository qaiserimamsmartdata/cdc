(function () {
    'use strict';

    angular
        .module('staffdashboard')
        .controller('StaffDashBoardController', StaffDashBoardController);

    /* @ngInject */
    function StaffDashBoardController($scope, $timeout, $mdToast, $rootScope, $state, $interval, CommonService, notificationService, $localStorage, $q, HOST_URL) {
        var vm = this;
        // $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        // if (localStorage.staffId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        localStorage.ParticipantAttendancePage = false;
        vm.ImageUrlPath = HOST_URL.url;
        GetAllCount();
        vm.FamilyCount = 0;
        vm.StaffCount = 0;
        vm.AttendanceCount = 0;
        vm.ParticipantCount = 0;
        vm.TotalHours = 0;
        vm.LeaveCount = 0;
        vm.TotalBreaks = 0;
        vm.TotalLunch = 0;
        vm.EventCount=0;
        vm.data1 = [];
        vm.redirectToAttendance = redirectToAttendance;
        vm.redirectToLeave = redirectToLeave;
        vm.redirectToEvents = redirectToEvents;
        vm.redirectTotimeclock = redirectTotimeclock;

        function redirectToAttendance() {
            $state.go("triangular.staffportalattendance");
        }
        function redirectToLeave() {
            $state.go("triangular.applyleave");
        }
        function redirectToEvents() {
            $state.go("triangular.staffevent");
        }
        function redirectTotimeclock() {
            $state.go("triangular.timesheet");
        }
        function GetAllCount() {
            var model = {
                AgencyId: localStorage.agencyId,
                StaffId: localStorage.staffId,
                TimeZone:localStorage.TimeZone
            }
            var myPromise = CommonService.getStaffDashboardAllData(model);

            myPromise.then(function (resolve) {
                if (resolve.IsSuccess == true) {
                    vm.TotalHours = resolve.Content.TotalWorkingHours;

                    vm.AttendanceCount = resolve.Content.AttendanceCount;
                    // vm.ParticipantCount = resolve.Content.ParticipantCount;
                    vm.EventCount=resolve.Content.EventCount;
                    vm.LeaveCount = resolve.Content.LeaveCount;
                    vm.TimesheetViewModelData = resolve.Content.TimesheetViewModelData;
                    vm.StartTime = resolve.Content.StartTime;
                    vm.InTime = resolve.Content.InTime;
                    vm.OutTime = resolve.Content.OutTime;

                    vm.TotalBreaks = resolve.Content.TotalBreaks;
                    vm.TotalLunch = resolve.Content.TotalLunch;
                    if (vm.TotalLunch > 0) {
                        vm.data1.push(vm.TotalLunch);
                        // if(vm.TotalBreaks>0)
                        vm.data1.push(vm.TotalBreaks);
                        // randomData();
                    }

                }

            }, function (reject) {

            });

        };
        vm.UpdatePricingPlan = UpdatePricingPlan;
        function UpdatePricingPlan() {
            $state.go('PriceUpdate.pricing');
        }
        vm.UpdateTimeClockUsersPlan = UpdateTimeClockUsersPlan;
        function UpdateTimeClockUsersPlan() {
            $state.go('TimeClockUsersPrice.timeclockusersplanpricing');
        }
        vm.labels1 = ['Lunch', 'Breaks'];
        vm.options1 = {
            datasetFill: false
        };

        /////////////

        // function randomData1() {
        //     vm.data1 = [];
        //     for (var label = 0; label < vm.labels1.length; label++) {
        //         vm.data1.push(Math.floor((Math.random() * 100) + 1));
        //     }
        // }
        // randomData1();
        // function init() {

        //     $timeout(function() {
        //         $rootScope.$broadcast('newMailNotification');

        //         var toast = $mdToast.simple()
        //             .textContent('You have new email messages!')
        //             .action('View')
        //             .highlightAction(true)
        //             .position('bottom right');
        //         $mdToast.show(toast).then(function(response) {
        //             if (response == 'ok') {
        //                 $state.go('triangular.email.inbox');
        //             }
        //         });
        //     }, 5000);
        // }

        // init

        // init();


        //   vm.labels = ['Download Sales', 'Instore Sales', 'Mail Order'];
        // vm.options = {
        //     datasetFill: false
        // };

        /////////////

        // function randomData() {
        //     vm.data = [];
        //     for(var label = 0; label < vm.labels.length; label++) {
        //         vm.data.push(Math.floor((Math.random() * 100) + 1));
        //     }
        // }

        // init

        // randomData();

        // Simulate async data update
        // $interval(randomData, 5000);


        vm.labels = ['Enrolled(%)', 'Unassigned(%)', 'Future(%)'];
        vm.options = {
            datasetFill: false
        };

        /////////////
        vm.GotoClassList = GotoClassList;
        function GotoClassList() {
            $state.go("triangular.classlist");
        }
        function randomData() {
            vm.data = [Math.round((vm.CurrentEnrollParticipantCount * 100) / vm.ParticipantCount), Math.round((vm.UnAssignedEnrollParticipantCount * 100) / vm.ParticipantCount), Math.round((vm.FutureEnrollParticipantCount * 100) / vm.ParticipantCount)];
            // for (var label = 0; label < vm.labels.length; label++) {
            //     vm.data.push(Math.floor((Math.random() * 100) + 1));
            // }
        }

        // init

        // randomData();

        // Simulate async data update
        $interval(randomData, 5000);








    }
})();