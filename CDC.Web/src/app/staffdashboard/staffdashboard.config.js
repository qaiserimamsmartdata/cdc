(function() {
    'use strict';

    angular
        .module('staffdashboard')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
        .state('triangular.staffdashboard', {
            url: '/staffdashboard',
            templateUrl: 'app/staffdashboard/staffdashboard.tmpl.html',
            // set the controller to load for this page
            controller: 'StaffDashBoardController',
            controllerAs: 'vm',
           
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    //contentClass: 'layout-column'
                }
            },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"STAFF",$window);
                    }
                }
        });

        triMenuProvider.addMenu({
            name: 'Dashboard',
            icon: 'fa fa-tachometer',
            type: 'link',
            permission: 'viewStaffDashBoard',
            state: 'triangular.staffdashboard',
            priority: 1
            // children: [{
            //     name: 'Start Page',
            //     state: 'triangular.seed-page',
            //     type: 'link'
            // }]
        });
    }
})();
