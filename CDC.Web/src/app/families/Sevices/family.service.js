﻿(function () {
    'use strict';

    angular
        .module('app.family')
        .factory('FamilyService', FamilyService);

    FamilyService.$inject = ['$http', '$q', 'HOST_URL', '$rootScope'];

    /* @ngInject */
    function FamilyService($http, $q, HOST_URL, $rootScope) {
        return {
            GetFamilyListService: GetFamilyListService,
            GetFamilyListByIdService: GetFamilyListByIdService,
            GetStudentListByIdService: GetStudentListByIdService,
            GetFutureEnrolledStudent: GetFutureEnrolledStudent,
            DeleteFutureEnrolledStudent: DeleteFutureEnrolledStudent,
            SetIsLoggedFirstTime:SetIsLoggedFirstTime,
            GetStudentScheduleListByIdService: GetStudentScheduleListByIdService,
            DeleteFamilyById: DeleteFamilyById,
            DeleteFamilyParticipantById: DeleteFamilyParticipantById,
            CheckParticipantClass:CheckParticipantClass,
            GetParentListByIdService: GetParentListByIdService,
            SetId: SetId,
            DeleteFamilyParent: DeleteFamilyParent,
            GetUnEnrollListService: GetUnEnrollListService,
            GetStudentDetailsByIdService: GetStudentDetailsByIdService,
            GetAllParticipantsByIdService:GetAllParticipantsByIdService
        };

        function SetId(data) {
            this.Id = 0;
            this.FamilyName = '';
            this.Id = data.ID;
            this.FamilyName = data.FamilyName;
            this.FamilyData = data;
        }

        function GetFamilyListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/getFamilyInfo', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }
        function GetStudentListByIdService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/getFamilyStudentList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

        }
        function GetStudentDetailsByIdService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/getFamilyStudentDetails', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

        }
        function GetParentListByIdService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/getFamilyParentList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

        };
        function GetStudentScheduleListByIdService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/EnrollClass/GetCurrentEnrollList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
          function GetAllParticipantsByIdService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/getFamilyStudentList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

        };
        function GetFutureEnrolledStudent(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/EnrollClass/GetFutureEnrollList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

        };
        function GetUnEnrollListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/EnrollClass/GetUnEnrolledStudentList', model).success(function (response) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: errResp });
            });
            return deferred.promise;

        };
        function GetFamilyListByIdService(FamilyId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/GetFamilyById?FamilyId=' + FamilyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function DeleteFamilyById(FamilyId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Family/DeleteFamily?FamilyId=' + FamilyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function DeleteFamilyParent(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/deleteParentInfo', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function DeleteFamilyParticipantById(ParticipantId) {
            var deferred = $q.defer();
            var model = { ID: ParticipantId };

            $http.post(HOST_URL.url + '/api/Family/deleteStudentInfo', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

        };
            function SetIsLoggedFirstTime(ParentID) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/SetIsLoggedFirstTime?ParentID=' + ParentID).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function CheckParticipantClass(model){
            
             var deferred=$q.defer();
             $http.post(HOST_URL.url + '/api/common/GetParticipantDeleteMessage',model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

        };

        function DeleteFutureEnrolledStudent(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/EnrollClass/DeleteFutureEnrolledStudent', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }
    }
})();