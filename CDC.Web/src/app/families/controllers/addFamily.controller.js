(function () {
    'use strict';
    angular
        .module('app.family')
        .controller('AddFamilyController', AddFamilyController);


    /* @ngInject */
    function AddFamilyController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $localStorage, notificationService,
        $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, AgencyService, imageUploadService) {
        var vm = this;

        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        vm.status = 'idle';  // idle | uploading | complete
        localStorage.ParticipantAttendancePage = false;
        var fileList;
        $scope.$storage = localStorage;
        vm.addFamily = addFamily;
        vm.index = 0;
        vm.showProgressbar = false;
        vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;
        vm.cancel = cancel;
        vm.setLastName = setLastName;
        vm.ImageUrlPath = HOST_URL.url;
        vm.CarrierAddress = CarrierAddress;
        vm.showAddFamilyquickDialogue = showAddFamilyquickDialogue;
        vm.ProfilePicComplete = "assets/images/avatars/avatar-5.png";
        vm.StudentProfilePicComplete = "assets/images/avatars/avatar-5.png";
        // vm.GetState = GetState;
        // vm.getCity = getCity;
        $scope.id = 0;
        // var bDate = new Date();
        // bDate = bDate.setDate(bDate.getDate() - 365);
        // vm.minBirthDate = new Date(bDate);
        // vm.todayData = new Date();
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            studentclass: 0,
            name: '',
            AgencyId: $scope.$storage.agencyId
        };
        vm.placeholder = "____-____-____-____";
        vm.pattern = "9999-9999-9999-9999";
        vm.CVV = "3";
        vm.CVVmax = "4";
        vm.checkStatus = checkStatus;
        function checkStatus() {
            vm.FamilyRegister.PaymentInfo.CardNumber = '';
            vm.FamilyRegister.PaymentInfo.CVV = '';
            if (vm.FamilyRegister.PaymentInfo.CardType != '3') {
                vm.pattern = "9999-9999-9999-9999";
                vm.placeholder = "____-____-____-____";
                vm.CVV = "3";
                vm.CVVmax = "3";
            }
            else {
                vm.pattern = "9999-999999-99999";
                vm.placeholder = "____-______-_____";
                vm.CVV = "4";
                vm.CVVmax = "4";
            }
        }
        vm.ParentForms = [];
        vm.ParentFormsObj = {
            FamilyName: '',
            FirstName: '',
            LastName: '',
            HomePhone: '',
            SecurityKey: '',
            SecurityQuestionId:'',
            SecurityQuestionAnswer:'',
            Mobile: '',
            EmailId: '',
            IsPrimary: false,
            IsEventMailReceived: true,
            SMSCarrierEmailAddress: '',
            IsParticipantAttendanceMailReceived: true,
            ImagePath: "assets/images/avatars/avatar-5.png",
            AttributeNameID: $scope.AttributeNameID
        };
        getSMSCarrier();
        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function CarrierAddress(data) {
            vm.AllSMSCarrier.forEach(function (element) {
                if (element.ID == data) {
                    vm.ParentFormsObj.SMSCarrierEmailAddress = element.CarrierAddress;
                }
            }, this);

        }
        vm.ParentForms.push(vm.ParentFormsObj);
        vm.AddParentForm = AddParentForm;
        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadImage($files, data) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image size should not exceed 2MB');
                return;

            }
            console.log(data);
            vm.index = data;
            //imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {

                imageUploadService.uploadImage($files, vm.UploadSuccess)
                uploadStarted();

                $timeout(uploadComplete, 2000);
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        vm.ProfilePicList = [];
        // vm.ProfilePicList.ProfilePic={};


        function UploadSuccess(data) {

            vm.ProfilePicList.ProfilePic = data.Content;
            vm.ProfilePicList.push(vm.ProfilePicList.ProfilePic);
            vm.ProfilePicComplete = HOST_URL.url + data.Content;
            vm.ParentForms[vm.index].ImagePath = data.Content;
        }

        function uploadStarted() {
            vm.status = 'uploading';
        }

        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image uploaded successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }
        vm.uploadImage1 = uploadImage1;
        vm.UploadSuccess1 = UploadSuccess1;
        function uploadImage1($files, data) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image size should not exceed 2MB');
                return;

            }
            vm.index = data;
            //imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage1 = $files;
            var fileExtension = $scope.ProfileImage1[0].name;
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {

                imageUploadService.uploadImage($files, vm.UploadSuccess1)
                uploadStarted();

                $timeout(uploadComplete, 2000);
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        // vm.ProfilePicList1 = [];
        function UploadSuccess1(data) {

            // vm.ProfilePicList1.ProfilePic1 = data.Content;
            // vm.ProfilePicList1.push(vm.ProfilePicList1.ProfilePic1);
            // vm.ProfilePicComplete = HOST_URL.url + data.Content;
            vm.ParticipantForms[vm.index].Student.ImagePath = data.Content;

            // vm.ProfilePic1 = data.Content;
            // vm.StudentProfilePicComplete = HOST_URL.url + data.Content;
        }

        function AddParentForm(data) {
            debugger
            vm.ParentFormsObj = {
                FamilyName: '',
                FirstName: '',
                LastName: vm.ParentForms[0].LastName,
                HomePhone: '',
                Mobile: '',
                EmailId: '',
                SMSCarrierId: '',
                SecurityQuestionId:'',
                SecurityQuestionAnswer:'',
                IsPrimary: false,
                SecurityKey: '',
                ImagePath: "assets/images/avatars/avatar-5.png",
                AttributeNameID: $scope.AttributeNameID
            };


            vm.ParentForms.push(angular.copy(vm.ParentFormsObj));
            var length = vm.ParentForms.length;

            vm.ParentForms[length - 1].ImagePath = "assets/images/avatars/avatar-5.png";
            console.log(vm.ParentForms);
            $scope.AttributeNameID = data;
        }

        vm.CheckPrimary = CheckPrimary;

        function CheckPrimary(data) {

            var length = vm.ParentForms.length;
            if (length > 1) {

                for (var i = 0; i < length - 1; i++) {
                    if (vm.ParentForms[i].IsPrimary == true) {
                        vm.ParentForms[length - 1].IsPrimary = false;
                        //notificationService.displaymessage('A parent is already primary');

                    }

                }
            }
            
        }
        vm.CheckSecurity = CheckSecurity;

        function CheckSecurity(data) {

            var length = vm.ParentForms.length;
            if (length > 1) {

                for (var i = 0; i < length - 1; i++) {
                    if (vm.ParentForms[i].SecurityKey == data) {
                        vm.ParentForms[length - 1].SecurityKey = "";
                        notificationService.displaymessage("Parents can't have same Pin Number");

                    }

                }
            }
            
        }
        vm.CheckMobile = CheckMobile;

        function CheckMobile(data) {

            var length = vm.ParentForms.length;
            if (length > 1) {

                for (var i = 0; i < length - 1; i++) {
                    if (vm.ParentForms[i].Mobile == data) {
                        vm.ParentForms[length - 1].Mobile = "";
                        notificationService.displaymessage("Parents can't have same Phone Number");

                    }

                }
            }
            
        }



        function setLastName(data) {

            vm.ParentForms.forEach(function (element) {
                element.LastName = data;
            }, this);
            vm.ParticipantForms.forEach(function (element) {
                element.Student.LastName = data;
            }, this);
        }
        vm.ParticipantForms = [];
        vm.ParticipantFormsObj = {
            PhoneNumber: '',
            Email: '',
            SchoolName: '',
            GradeLevelId: '',
            Transportation: '',
            Disabilities: '',
            Allergies: '',
            Medications: '',
            PrimaryDoctor: '',
            Description: '',
            AttributeParticipantNameID: $scope.AttributeParticipantNameID
        };
        vm.ParticipantFormsObj.Student = {
            FirstName: '',
            LastName: '',
            Gender: '',
            DateOfBirth: '',
            ImagePath: "assets/images/avatars/avatar-5.png",
            MembershipTypeId: ''
        };

        vm.ParticipantForms.push(vm.ParticipantFormsObj);
        vm.AddParticipantForm = AddParticipantForm;
        function AddParticipantForm(data) {
            if (data == 11) {
                notificationService.displaymessage("Maximum 10 participants can be added");
                return;
            }
            vm.ParticipantFormsObj = {
                PhoneNumber: '',
                Email: '',
                SchoolName: '',
                GradeLevelId: '',
                Transportation: '',
                Disabilities: '',
                Allergies: '',
                Medications: '',
                PrimaryDoctor: '',
                Description: '',
                AttributeParticipantNameID: $scope.AttributeParticipantNameID
            };
            vm.ParticipantFormsObj.Student = {
                FirstName: '',
                LastName: vm.ParticipantForms[0].Student.LastName,
                Gender: '',
                DateOfBirth: '',
                ImagePath: "assets/images/avatars/avatar-5.png",
                MembershipTypeId: ''
            };
            vm.ParticipantForms.push(angular.copy(vm.ParticipantFormsObj));
            var length = vm.ParticipantForms.length;
            vm.ParticipantForms[length - 1].Student.ImagePath = "assets/images/avatars/avatar-5.png";
            //console.log(vm.ParentForms);
            // $scope.AttributeNameID = data;

            // vm.ParticipantForms.push(angular.copy(vm.ParticipantFormsObj));
            $scope.AttributeParticipantNameID = data;
        }
        //for max2016
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);

        //for min 80years.
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);
        //Card Expiry Date
        var year = new Date().getFullYear();
        var range = [];
        range.push(year);
        for (var i = 1; i < 11; i++) {
            range.push(year + i);
        }
        $scope.years = range;


        GetAllCountry();
        vm.FamilyRegister = {};
        vm.FamilyRegister.FamilyInfo = {};
        vm.FamilyRegister.PaymentInfo = {};
        // vm.FamilyRegister.FamilyInfo=vm.ParentForms;
        vm.FamilyRegister.ReferenceDetail = {};
        vm.FamilyRegister.ParentInfo = [];
        vm.FamilyRegister.ParentInfo = vm.ParentForms;
        vm.FamilyRegister.ContactInfo = {};
        // vm.FamilyRegister.StudentDetail=vm.ParticipantForms;
        vm.FamilyRegister.StudentDetail = [];
        // vm.FamilyRegister.StudentDetail.Student = {};
        vm.FamilyRegister.StudentDetail = vm.ParticipantForms;
        vm.removeParent = removeParent;
        vm.removeParticipant = removeParticipant;
        getInfoSource();
        getMembership();
        getRelationType();
        getGradeLevel();
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            var myPromise = CommonService.getCountries();

            myPromise.then(function (resolve) {
                vm.AllCountries = null;
                vm.AllCountries = resolve;
                vm.FamilyRegister.ContactInfo.CountryId = "1".toString();
                GetState(vm.FamilyRegister.ContactInfo.CountryId);
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };


        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            myPromise.then(function (resolve) {
                vm.States = null;
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        function getInfoSource() {
            vm.promise = CommonService.getInfoSource($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllInfoSource = null;
                    vm.AllInfoSource = eval(response.Content);
                }
            });

        };
        function showAddFamilyquickDialogue() {
            $mdDialog.show({
                controller: AddParticipantController,
                controllerAs: 'vm',
                templateUrl: 'app/families/views/quickAddfamily.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function getMembership() {
            vm.promise = CommonService.getMembership($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllMembership = null;
                    vm.AllMembership = eval(response.Content);
                }
            });

        };
        function getRelationType() {
            vm.promise = CommonService.getRelationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllRelationType = null;
                    vm.AllRelationType = eval(response.Content);
                }
            });

        };
        getSecurityQuestions();
        function getSecurityQuestions() {
            vm.AllQuestions = null; // Clear previously loaded state list
            var myPromise = CommonService.getSecurityQuestions($scope.query);

            myPromise.then(function (resolve) {
                vm.AllQuestions = null;
                vm.AllQuestions = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function getGradeLevel() {
            vm.promise = CommonService.getGradeLevel($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllGradeLevel = null;
                    vm.AllGradeLevel = eval(response.Content);
                }
            });

        };
        //     if (RegExp('multipage', 'gi').test(window.location.search)) {
        //     introJs().start();
        //   }
        $scope.isDisabled = false;
        function addFamily(referralForm, ParentForm, contactForm, studentForm, paymentForm, model) {
            if (referralForm.$valid && ParentForm.$valid && contactForm.$valid && studentForm.$valid && paymentForm.$valid) {
                vm.showProgressbar = true;
                $scope.isDisabled = true;
                // if (model.FamilyRegister.PaymentInfo.CardType == 'American Express')
                //     model.FamilyRegister.PaymentInfo.CardNumber = model.FamilyRegister.PaymentInfo.CardNumberAmericanExpress;
                // else
                //     model.FamilyRegister.PaymentInfo.CardNumber = model.FamilyRegister.PaymentInfo.CardNumber;

                model.FamilyInfo.AgencyId = localStorage.agencyId;
                model.PortalInfo = {
                    PortalEmail: '',
                    RoleId: '',
                    PortalPassword: ''
                }
                $http.post(HOST_URL.url + '/api/Family/AddFamily', model).then(function (response) {
                    if (response.data.FamilyRegister.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.FamilyRegister.Message);
                        // ResetFields();
                        $state.go('triangular.families');
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancel() {
            $state.go("triangular.families");
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };


        function removeParent(ParentForm) {
            vm.ParentForms.pop(ParentForm);
        }
        function removeParticipant(ParticipantForm) {
            vm.ParticipantForms.pop(ParticipantForm);
        }



    }
    function AddParticipantController($scope, ClassService, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, imageUploadService) {

        var vm = this;
        vm.showProgressbar = false;
        vm.status = 'idle';  // idle | uploading | complete
        var fileList;
        vm.addQuickFamily = addQuickFamily;
        vm.cancelClick =
            function cancelClick() {
                $mdDialog.cancel();
            }

        vm.cancelClick = cancelClick;
        //for max2016
        var bDate = new Date();
        vm.getFeesAmount = getFeesAmount;
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);

        //for min 80years.
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);
        // vm.startDate = new Date();
        $scope.$storage = localStorage;
        vm.CarrierAddress = CarrierAddress;
        vm.FamilyRegister = {};
        vm.ParentInfo = {
            FirstName: '',
            SMSCarrierId: '',
            Mobile: '',
            EmailId: '',
            RelationTypeId: '',
            SecurityKey: '',
            SecurityQuestionId:'',
            SecurityQuestionAnswer:'',
            LastName: '',
            HomePhone: '',
            IsPrimary: false,
            IsEventMailReceived: false,
            IsParticipantAttendanceMailReceived: false
        }
        function GetClassById() {
            var model = {
                ClassId: vm.scheduleData.ClassId,
                TimeZone: localStorage.TimeZone
            }
            vm.promise = ClassService.GetClassListByIdService(model);
            vm.promise.then(function (response) {
                debugger
                if (response.classList.ID > 0) {
                    if (response.classList.OnGoing == null || response.classList.OnGoing == 0) {
                        $scope.ClassStartDate = new Date(response.classList.ClassStartDate);
                        $scope.EndDate = new Date(response.classList.ClassEndDate);
                    }
                }
            });
        }

        function getFeesAmount(data) {
            $scope.ClassStartDate = "";
            $scope.EndDate = "";
            GetClassById();
            vm.AllClasses.forEach(function (element) {
                if (vm.scheduleData.ClassId == element.ID) {
                    vm.scheduleData.TutionFees = element.Fees;
                    vm.FeesType = element.Name;
                }
            }, this);

            var model = {
                "ClassId": data,
                "AgencyId": localStorage.agencyId
            }
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/User/EnrolledCapacity', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
                if (response == true) {
                    vm.scheduleData.ClassId = null;
                    notificationService.displaymessage('Class has reached its maximum capacity.');

                }
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList($scope.$storage.agencyId);
            vm.promise.then(function (response) {
                debugger
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }
        vm.FamilyRegister.FamilyInfo = {};
        vm.FamilyRegister.PaymentInfo = {};
        vm.FamilyRegister.ReferenceDetail = {};
        vm.FamilyRegister.ParentInfo = [];
        vm.FamilyRegister.ContactInfo = {
            CountryId: '',
            EmergencyFirstName: '',
            EmergencyLastName: '',
            RelationshipToParticipantId: '',
            EmergencyPhoneNumber: '',
            PostalCode: ''
        };
        vm.FamilyRegister.StudentDetail = [];
        vm.FamilyRegister.StudentDetail.Student = {};
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            studentclass: 0,
            name: '',
            AgencyId: $scope.$storage.agencyId
        };
        vm.StudentDetails = {
            Student: {
                FirstName: '',
                DateOfBirth: '',
                Gender: '',
                LastName: '',
                MembershipTypeId: ''
            }
        }
        vm.scheduleData = {};
        $scope.isDisabled = false;
        $scope.DisplayText = "Quick Family Registration";
        vm.ImageUrlPath = HOST_URL.url;
        function addQuickFamily(model) {
            $scope.isDisabled = true;
            vm.showProgressbar = true;
            vm.FamilyRegister.FamilyInfo.SecurityKey = vm.ParentInfo.SecurityKey;
            vm.ParentInfo.LastName = model.FamilyInfo.FamilyName;
            vm.ParentInfo.HomePhone = vm.ParentInfo.Mobile;
            vm.ParentInfo.IsPrimary = true;
            vm.ParentInfo.IsEventMailReceived = true;
            vm.ParentInfo.IsParticipantAttendanceMailReceived = true;
            model.ContactInfo.CountryId = 1;
            model.ContactInfo.EmergencyFirstName = vm.ParentInfo.FirstName;
            model.ContactInfo.EmergencyLastName = model.FamilyInfo.FamilyName;
            model.ContactInfo.RelationshipToParticipantId = vm.ParentInfo.RelationTypeId;
            model.ContactInfo.EmergencyPhoneNumber = vm.ParentInfo.Mobile;
            vm.StudentDetails.Student.LastName = model.FamilyInfo.FamilyName;
            vm.StudentDetails.Student.MembershipTypeId = 1;
            model.FamilyInfo.AgencyId = localStorage.agencyId;
            model.AgencyId = localStorage.agencyId;
            model.PortalInfo = {
                PortalEmail: '',
                RoleId: '',
                PortalPassword: ''
            }
            model.ParentInfo.push(vm.ParentInfo);
            model.StudentDetail.push(vm.StudentDetails);
            $http.post(HOST_URL.url + '/api/Family/AddFamily', model).then(function (response) {
                if (response.data.FamilyRegister.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.FamilyRegister.Message);
                    // ResetFields();
                    cancelClick();
                    $state.go('triangular.families');
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });

        }
        getSMSCarrier();
        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        };
        getSecurityQuestions();
        function getSecurityQuestions() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSecurityQuestions();

            myPromise.then(function (resolve) {
                vm.AllQuestions = null;
                vm.AllQuestions = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function CarrierAddress(data) {
            vm.AllSMSCarrier.forEach(function (element) {
                if (element.ID == data) {
                    vm.ParentInfo.SMSCarrierEmailAddress = element.CarrierAddress;
                    console.log(vm.ParentInfo.SMSCarrierEmailAddress);
                }
            }, this);

        }
        GetState();
        function GetState() {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(1);
            myPromise.then(function (resolve) {
                vm.States = null;
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        getRelationType();
        function getRelationType() {
            vm.promise = CommonService.getRelationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllRelationType = null;
                    vm.AllRelationType = eval(response.Content);
                }
            });

        };
        vm.ProfilePicComplete = "assets/images/avatars/avatar-5.png";
        vm.DefaultImage = "assets/images/avatars/avatar-5.png";

        function cancelClick() {
            $mdDialog.cancel();
        }
        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadImage($files) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image Size should not exceed 2MB');
                return;

            }

            uploadStarted();

            $timeout(uploadComplete, 2000);
            imageUploadService.uploadImage($files, vm.UploadSuccess)
        }

        function uploadStarted() {
            vm.status = 'uploading';
        }

        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image Uploaded Successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ParticipantFormsObj.Student.ImagePath = data.Content;
        }
    }
})();