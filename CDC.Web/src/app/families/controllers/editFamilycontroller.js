(function () {
    'use strict';

    angular
        .module('app.family')
        .controller('EditFamilyController', EditFamilyController);

    /* @ngInject */
    function EditFamilyController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, FamilyService,$localStorage) {
        var vm = this;
          $scope.$storage = localStorage;
        // if(localStorage.agencyId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        vm.updateFamily = updateFamily;
        vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;
        vm.showProgressbar = false;
        $scope.id = $stateParams.id;
        GetAllCountry();
        GetFamilyByID();

        function GetFamilyByID() {
            if ($stateParams.id == 0) {
                $state.go('triangular.families');
                return false;
            }
            else {
                vm.promise = FamilyService.GetFamilyListByIdService($stateParams.id);
                vm.promise.then(function (response) {
                    if (response.Family != undefined && response.Family != null) {
                        vm.Family = response.Family;
                        vm.Family.Dateofbirth = new Date(response.Family.Dateofbirth);
                        vm.Family.Startdate = new Date(response.Family.Startdate);
                        vm.Family.Enddate = new Date(response.Family.Enddate);
                        vm.Family.ParentsMobile = parseFloat(response.Family.ParentsMobile);
                        vm.Family.Guardianmobile = parseFloat(response.Family.Guardianmobile);
                        vm.Family.Country = response.Family.Country.toString();
                        vm.Family.State = response.Family.State.toString();
                        vm.Family.Postalcode = parseFloat(response.Family.Postalcode);
                        GetState();
                        $scope.selected = [];
                    }
                    else {
                        NotificationMessageController('Unable to get family details at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function updateFamily(referralForm, familyForm, contactForm, studentForm, model) {
            if (referralForm.$valid && familyForm.$valid && contactForm.$valid) {

                $http.post(HOST_URL.url + '/api/Familys/UpdateFamily', model).then(function (response) {

                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController(response.data.ReturnMessage[0]);
                        $state.go('triangular.families');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };

        // Common Services to call Country state and City

        function GetAllCountry() {
            $scope.id = $state.params.id;
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/country',
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllCountries = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        //NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to get country list at the moment.');
            });
        }

        function GetState() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/state',
                data: vm.Family.Country
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllStates = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        NotificationMessageController('Unable to retreive state list at the moment.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to retreive state list at the moment.');
            });
        }

        function getCity() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/getstatescity',
                data: vm.Family.State
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllCity = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        NotificationMessageController('Unable to retreive city list at the moment.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to retreive city list at the moment.');
            });
        }
    }
})();