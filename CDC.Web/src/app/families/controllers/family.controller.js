(function () {
    'use strict';

    angular
        .module('app.family')
        .controller('FamilyController', FamilyController)
        .controller('AddSendEmailController', AddSendEmailController);

    /* @ngInject */
    function FamilyController($scope, $http, $state, $mdDialog, $localStorage, notificationService,
        $timeout, $log, $mdToast, $q, FamilyService, filerService, HOST_URL) {
        var vm = this;
        $scope.$storage = localStorage;
        // if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        localStorage.ParticipantAttendancePage = false;
        $scope.RegisteredDate=$scope.RegisteredDate==null? undefined :$scope.RegisteredDate;
        vm.GetFamilyList = GetFamilyList;
        vm.detailFamilyInfo = detailFamilyInfo;
        vm.editFamily = editFamily;
        vm.RedirectToAddFamily = RedirectToAddFamily;
        vm.showDeleteFamilyDialoue = showDeleteFamilyDialoue;
        vm.reset = reset;
        vm.ImageUrlPath = HOST_URL.url;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.SendEmailDialogue = SendEmailDialogue;

        vm.columns = {
            FamilyName: 'Family Name',
            ParticipantCount: 'No of Participants',
            Email: 'Email',
            DateAdded: 'Date Added',
            City: 'City',
            Phone: 'Phone',
            ID: 'ID',
            ProfilePic: 'Image'
        };

        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.FamilyCount = 0;
        $scope.selected = [];
        $scope.isSuperAdmin = true;
        //For Family Portal
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        ///////////
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            Familyclass: 0,
            name: '',
            FamilyId: vm.FamilyId,
            AgencyId: $scope.$storage.agencyId
        };
        GetFamilyList();
        vm.ParticipantsCount = 0;
        vm.showProgressbar = true;
        function GetFamilyList() {
            debugger;
            $scope.FamilyName=$scope.query.FamilyName==undefined?"All":$scope.query.FamilyName;
            $scope.RegisteredDate=$scope.query.Date==null || $scope.query.Date==""?"All":new Date($scope.query.Date);
            vm.promise = FamilyService.GetFamilyListService($scope.query);
            vm.promise.then(function (response) {

                if (response.FamilyRegister.Content.length > 0) {
                    vm.FamilyList = response.FamilyRegister.Content;
                    vm.FamilyList.forEach(function (element) {
                        vm.ParticipantsCount = vm.ParticipantsCount + element.FamilyStudentMap.length;
                    }, this);
                    localStorage.ParticipantsCount = vm.ParticipantsCount;
                    vm.FamilyCount = response.FamilyRegister.TotalRows;
                    vm.showProgressbar = false;
                    $scope.selected = [];
                    vm.NoData = false;
                }
                else {
                    if (response.FamilyRegister.Content.length == 0) {
                        localStorage.ParticipantsCount = vm.ParticipantsCount;
                        vm.NoData = true;
                        vm.FamilyList = [];
                        vm.FamilyCount = 0;
                        vm.showProgressbar = false;
                        if(vm.FamilyId>0)
                        notificationService.displaymessage('No Family list found.');
                        else
                        notificationService.displaymessage('No Families have been added, click the + below to add one.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get family list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        };

        function editFamily(familyID) {
            $state.go('triangular.updateFamily', { id: familyID });
        }

        function showDeleteFamilyDialoue(event, Id) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this family?')
                .textContent('All of this family data will be deleted.')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                DeleteFamilyById(Id);
            }, function () {
                $scope.hide();
            });
        }

        function DeleteFamilyById(Id) {
            vm.promise = FamilyService.DeleteFamilybyIdService(Id);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    notificationService.displaymessage('Family deleted Successfully.');
                    GetFamilyList();
                }
                else {
                    notificationService.displaymessage('Error occured while deleting, Please try again later.');
                }
            });


            // var req = {
            //     method: 'POST',
            //     url: HOST_URL.url + '/api/Familys/DeleteFamilyById',
            //     data: { FamilyId: FamilyId },
            //     headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            // };
            // $http(req).then(function successCallback(response) {
            //     if (response.data.IsSuccess == true) {
            //         $scope.selected = [];
            //         GetFamilyList();
            //         NotificationMessageController('Deleted Successfully.');
            //     }
            //     else {
            //         NotificationMessageController(response.data.Message);
            //     }
            // }, function errorCallback() {
            //     NotificationMessageController('Unable to delete Family at the moment. Please try again after some time.');
            // });
        }
        function detailFamilyInfo($event, data) {
            $scope.$storage.setItem('familyDetails',JSON.stringify(data));
            if(localStorage.Portal=='AGENCY')
            $state.go('triangular.Familydetails');
            else
            $state.go('triangular.ParentFamilydetails');
        };
        function RedirectToAddFamily() {
            // if (localStorage.ParticipantsCount == localStorage.MaxNumberOfParticipants) {
            //     notificationService.displaymessage('Sorry,Maximum participants reached.');
            //     return;
            // }
            $state.go('triangular.addFamily');
        };

        function reset() {
            debugger
            $scope.query.FamilyName = undefined;
            $scope.RegisteredDate=undefined;
            $scope.query.Date = $scope.query.Date != null ? null : '';
            $scope.FamilyName="";
            GetFamilyList();
        };
        vm.tttt = "patggh";
        function SendEmailDialogue(family) {
            $mdDialog.show({
                controller: 'AddSendEmailController',
                controllerAs: 'vm',
                templateUrl: 'app/families/views/email/email-dialog.tmpl.html',
                // targetEvent: $event,
                locals: {
                    email: {
                        to: [],
                        cc: [],
                        bcc: [],
                        subject: '',
                        content: ''
                    },
                    getFocus: false,
                    family: family
                }
            })
                .then(function (email) {
                    // sendEmail(null, email);
                }, emailCancel);

            function emailCancel() {
                // $mdToast.show(
                //     $mdToast.simple()
                //     .content($filter('triTranslate')('Email canceled'))
                //     .position('bottom right')
                //     .hideDelay(3000)
                // );
            }
        }

        // function SendEmailDialogue(family) {
        //     // $scope.$storage.studentIdforSchedule = id;
        //     $mdDialog.show({
        //         controller: AddSendEmailController,
        //         controllerAs: 'vm',
        //         // temlateUrl: 'app/families/views/email/email-dialog.tmpl.html',
        //         templateUrl: 'app/families/views/email/email-dialog.tmpl.html',
        //         parent: angular.element(document.body),
        //         targetEvent: event,
        //         escToClose: true,
        //         clickOutsideToClose: true,
        //         // fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
        //         locals: {
        //             getFocus: false
        //         },
        //         items: { family: family }
        //     });
        // }
    }
    function AddSendEmailController($timeout, $mdDialog, $filter, triSkins, textAngularManager, getFocus, family, CommonService, notificationService, $localStorage) {
        // var contactsData = contacts.data;

        var vm = this;
        vm.cancel = cancel;
        // vm.email = email;
        vm.title = "Family-" + family.FamilyName;
        vm.send = send;
        vm.showCCSIcon = 'zmdi zmdi-account-add';
        vm.showCCS = false;
        vm.toggleCCS = toggleCCS;
        vm.triSkin = triSkins.getCurrent();
        vm.queryContacts = queryContacts;
        vm.Guardians = family.ParentInfo;
        vm.Participants = family.FamilyStudentMap;
        vm.sendEmail = sendEmail;
        vm.model = {};
        ///////////////
        vm.GuardianInfo = [];
        vm.ParitcipantInfo = [];
        function cancel() {
            $mdDialog.cancel();
        }

        function toggleCCS() {
            vm.showCCS = !vm.showCCS;
            vm.showCCSIcon = vm.showCCS ? 'zmdi zmdi-account' : 'zmdi zmdi-account-add';
        }

        function send() {
            $mdDialog.hide(vm.email);
        }

        function queryContacts($query) {
            var lowercaseQuery = angular.lowercase($query);
            return family.ParentInfo.filter(function (parents) {
                var lowercaseName = angular.lowercase(parents.FullName);
                if (lowercaseName.indexOf(lowercaseQuery) !== -1) {
                    vm.EmailId = parents.EmailId;
                    return parents;
                }
            });
        }
        function sendEmail() {
            vm.model.GuardianInfo = [];
            vm.model.Regards = [];
            vm.model.GuardianInfo = vm.GuardianInfo;
            vm.model.AgencyName = localStorage.AgencyName;
            vm.model.Regards = vm.ParitcipantInfo;
            vm.showProgressbar = true;
            vm.promise = CommonService.EmailToGuardians(vm.model);
            vm.promise.then(function (response) {
                if (response == "OK") {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Emails have been sent.');
                    cancel();
                }
                else {
                    notificationService.displaymessage('Emails have not been sent.');
                    cancel();
                }
            });

        }
        ////////////////
        if (getFocus) {
            $timeout(function () {
                // Retrieve the scope and trigger focus
                var editorScope = textAngularManager.retrieveEditor('emailBody').scope;
                editorScope.displayElements.text.trigger('focus');
            }, 500);
        }
    }
})();