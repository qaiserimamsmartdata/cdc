angular
    .module('app')
    .directive('ngParent', ['$http', 'HOST_URL','$localStorage', function (async, HOST_URL,$localStorage) {
      
        return {
            require: 'ngModel',
            
            link: function (scope, elem, attrs, ctrl) {
                elem.on('blur', function (evt) {
                    if (attrs.ngParent == 'parentusername') {
                        scope.$apply(function () {
                            var val = elem.val();
                            
                            var req = { "name": val} 
                            var ajaxConfiguration = { method: 'POST', url: HOST_URL.url + 'api/User/isexistparentusername', data: req };
                            async(ajaxConfiguration)
                                .success(function (data, status, headers, config) {
                                    ctrl.$setValidity('parent', !data);
                                });
                        });
                    }                   
                });
            }
        }
    }]);