angular
    .module('app')
    .directive('ngMobile', ['$http', 'HOST_URL','$localStorage', function (async, HOST_URL,$localStorage) {
      
        return {
            require: 'ngModel',
            
            link: function (scope, elem, attrs, ctrl) {
                elem.on('blur', function (evt) {
                    if (attrs.ngMobile == 'mobileNumber') {
                        scope.$apply(function () {
                            var val = elem.val().replace("(","");
                            var val2=val.replace(")","");
                            var val3=val2.replace("-","");
                            var val4=val3.replace(" ","");
                            var req = { "name": val4,
                                        "AgencyId":localStorage.agencyId} 
                            var ajaxConfiguration = { method: 'POST', url: HOST_URL.url + 'api/User/isexistmobileNumber', data: req };
                            async(ajaxConfiguration)
                                .success(function (data, status, headers, config) {
                                    ctrl.$setValidity('mobileNumber', !data);
                                });
                        });
                    }                   
                });
            }
        }
    }]);