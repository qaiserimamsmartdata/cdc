(function () {
    'use strict';

    angular
        .module('app.family')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.addFamily', {
                url: '/families/add',
                templateUrl: 'app/families/views/addFamily.tmpl.html',
                controller: 'AddFamilyController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.updatefamily', {
                url: '/families/update',
                templateUrl: 'app/families/views/addFamily.tmpl.html',
                controller: 'EditFamilyController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.families', {
                url: '/families',
                templateUrl: 'app/families/views/family.tmpl.html',
                // set the controller to load for this page
                controller: 'FamilyController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
             .state('triangular.parentfamilies', {
                url: '/parent/families',
                templateUrl: 'app/families/views/family.tmpl.html',
                // set the controller to load for this page
                controller: 'FamilyController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"FAMILY",$window);
                    }
                }
            })
            .state('triangular.Familydetails', {
                url: '/family/details',
                templateUrl: 'app/familiesDetails/views/family_detail.tmpl.html',
                // set the controller to load for this page
                controller: 'FamilyDetailController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            
             .state('triangular.ParentFamilydetails', {
                url: '/parent/family/details',
                templateUrl: 'app/familiesDetails/views/family_detail.tmpl.html',
                // set the controller to load for this page
                controller: 'FamilyDetailController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"FAMILY",$window);
                    }
                }
            });
        // triMenuProvider.addMenu({
        //     name: 'Families',
        //     icon: 'fa fa-group',
        //     type: 'dropdown',
        //     id: 'step1',
        //     priority: 3,
        //     permission: 'viewFamily',
        //     children: [{
        //         name: 'Family Registration',
        //         state: 'triangular.addFamily',
        //         type: 'link',
        //         permission: 'viewFamilyRegistration'
        //     },
        //         {
        //             name: 'Family List',
        //             state: 'triangular.families',
        //             type: 'link',
        //             permission: 'viewFamilyList'
        //         },
        //         {
        //             name: 'Family List',
        //             state: 'triangular.parentfamilies',
        //             type: 'link',
        //             permission: 'viewParentFamilyList'
        //         }]
        // });
    }
})();
