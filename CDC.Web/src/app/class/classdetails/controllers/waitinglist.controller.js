(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('WaitingListByClassController', WaitingListByClassController);

    /* @ngInject */
    function WaitingListByClassController($scope, $mdDialog, $state, $stateParams, ClassService, FamilyService, AgencyService, $localStorage, notificationService, filerService, $rootScope, HOST_URL) {
         var data = $stateParams.id;
        var vm = this;
        $scope.$storage = localStorage; 
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.ImageUrlPath = HOST_URL.url;
        $scope.selected = [];
        vm.columns = {
            Image: 'Image',
            FirstName: 'First Name',
            LastName: 'Last Name',
            ClassName: 'Class Name',
            StartDate: 'StartDate',
            Room: 'Room',
            ID: 'ID'
        };

        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FamilyId: 0,
            name: '',
            ParticipantName: '',
           ClassId: $scope.$storage.classId,
            AgencyId: localStorage.agencyId
        };
        vm.GetWaitingListByClass = GetWaitingListByClass;
        vm.reset = reset;

        GetWaitingListByClass();

        $rootScope.$on("GetWaitingListByClass", function () {
            GetWaitingListByClass();
        });
        function GetWaitingListByClass() {
            $scope.ParticipantName=$scope.query.ParticipantName==""?"All":$scope.query.ParticipantName;
            vm.promise = ClassService.GetWaitingListByClassService($scope.query);
            vm.promise.then(function (response) {
                if (response.enrollList.Content.length > 0) {
                    vm.NoData = false;
                    vm.WaitingList = response.enrollList.Content;
                    vm.WaitingListCount = response.enrollList.TotalRows;
                    $scope.selected = [];
                }
                else {
                    if (response.enrollList.Content.length == 0) {
                        vm.NoData = true;
                        vm.WaitingList = [];
                        vm.WaitingListCount = 0;
                    }
                    else {
                        notificationService.displaymessage('Unable to get waiting list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        };


        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function reset() {
            $scope.query.ParticipantName = '';
            GetWaitingListByClass();
        }
    }
})();