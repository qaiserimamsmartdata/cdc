(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('FoodManagementMealPatternController', FoodManagementMealPatternController)
        .controller('UpdateFoodMealController', UpdateFoodMealController);

    /* @ngInject */
    function FoodManagementMealPatternController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        FoodMasterService, CommonService, filerService, HOST_URL, $filter, FoodMealService) {
        var vm = this;
        vm.$storage = localStorage;
        vm.toggleRight3 = filerService.toggleRight3();
        vm.isOpenRight3 = filerService.isOpenRight3();
        vm.close3 = filerService.close3();
        vm.reset = reset;
        vm.columns = {
            Age: 'Age group',
            ID: 'ID',
            Meal: 'Meal type',
            Item: 'Item name',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.holidayCount = 0;
        vm.selected = [];
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            ItemName: '',
            MealTypeId: '',
            AgencyId: localStorage.agencyId,
            Description: '',
        };
        $scope.myCountry = {
            selected: {}
        };
        vm.AllAgeYears = CommonService.getYears();
        vm.AllAgeMonths = CommonService.getMonths();
        vm.getItems = getItems;
        vm.ClearFields = ClearFields;
        getFoodMasterData();
        getMealTypeData();
        function getMealTypeData() {
            vm.AllMealTypes = null; 
            var myPromise = CommonService.getMealTypeData();
            myPromise.then(function (resolve) {
                vm.AllMealTypes = null;
                vm.AllMealTypes = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function getItems(MealID) {
            vm.ItemList = [];
            var MinAgeFrom = 0;
            var MinAgeTo = 0;
            var MaxAgeFrom = 0;
            var MaxAgeTo = 0;
            vm.AllFoodMaster.forEach(function (element) {
                if (vm.mealPattern.AgeGroupId == element.ID) {
                    MinAgeFrom = element.MinAgeFrom;
                    MinAgeTo = element.MinAgeTo;
                    MaxAgeFrom = element.MaxAgeFrom;
                    MaxAgeTo = element.MaxAgeTo;
                }
            })
            vm.AllFoodMaster.forEach(function (element) {
                if (element.MealTypeId == MealID && element.MinAgeFrom == MinAgeFrom && element.MinAgeTo == MinAgeTo && element.MaxAgeFrom == MaxAgeFrom && element.MaxAgeTo == MaxAgeTo) {
                    vm.ItemList.push(element);
                }

            }, this);
        }
        function getFoodMasterData() {
            vm.AllMealTypes = null;
            var myPromise = CommonService.getFoodMasterData(vm.query);
            myPromise.then(function (resolve) {
                vm.AllFoodMaster = null;
                vm.AllFoodMaster = eval(resolve.Content);
                vm.AllFoodMaster.forEach(function (element) {
                    var OtherData = $filter('filter')(vm.AllAgeYears, { ID: element.MinAgeFrom }, true);
                    element.MinAgeFromName = OtherData[0].Name;
                    var OtherData1 = $filter('filter')(vm.AllAgeMonths, { ID: element.MinAgeTo }, true);
                    element.MinAgeToName = OtherData1[0].Name;
                    var OtherData3 = $filter('filter')(vm.AllAgeYears, { ID: element.MaxAgeFrom }, true);
                    element.MaxAgeFromName = OtherData3[0].Name;
                    var OtherData4 = $filter('filter')(vm.AllAgeMonths, { ID: element.MaxAgeTo }, true);
                    element.MaxAgeToName = OtherData4[0].Name;
                    element.Agegroup = element.MinAgeFromName + ' ' + element.MinAgeToName + ' - ' + element.MaxAgeFromName + ' ' + element.MaxAgeToName;

                }, this);

            }, function (reject) {

            });

        };
        vm.foodMealListCount = 0;
        vm.GetFoodMealList = GetFoodMealList;
        $rootScope.$on("GetFoodMealList", function () {
            GetFoodMealList();
        });
        vm.showDeleteFoodMealDetailsConfirmDialoue = showDeleteFoodMealDetailsConfirmDialoue;
        vm.AddFoodMealPattern = AddFoodMealPattern;
        GetFoodMealList();
        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function GetFoodMealList() {
            vm.promise = FoodMealService.GetFoodMealListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.foodMealList = {};
                        vm.foodMealList = response.Content;
                        vm.foodMealListCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.foodMealList = [];
                        vm.foodMealListCount = 0;
                        NotificationMessageController('No food meal list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get food meal list at the moment. Please try again after some time.');
                }
                vm.close3();
            });
        };


        function AddFoodMealPattern(model, formMealPattern) {
            vm.AllFoodMaster.forEach(function (element) {
                if (element.ID == model.AgeGroupId) {
                    model.AgeGroup = element.Agegroup;
                }
            }, this);
            model.ItemList = vm.ItemList;
            model.ClassInfoId = localStorage.classId;
            model.AgencyId = localStorage.agencyId;
            $http.post(HOST_URL.url + '/api/foodManagementMealPattern/AddFoodMealPattern', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    NotificationMessageController(response.data.ReturnMessage[0]);
                    GetFoodMealList();
                    ClearFields(formMealPattern);

                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        function ClearFields(formMealPattern) {
            vm.mealPattern.AgeGroupId = 0;
            vm.mealPattern.MealTypeId = 0;
            vm.ItemList = {};
            formMealPattern.$setPristine();
            formMealPattern.$setUntouched();
        }
        vm.showUpdateFoodMealDetailsConfirmDialoue = showUpdateFoodMealDetailsConfirmDialoue;
        function showUpdateFoodMealDetailsConfirmDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateFoodMealController,
                controllerAs: 'vm',
                templateUrl: 'app/class/classdetails/views/details/pages/updatemealpattern.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { foodDetail: data, AllFoodMaster: vm.AllFoodMaster },
                fullscreen: $scope.customFullscreen
            })
        };



        function showDeleteFoodMealDetailsConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this food meal details permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteFoodMealDetails(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteFoodMealDetails(data) {
            vm.promiseDelete = FoodMealService.DeleteFoodMealById(data.ID);
            vm.promiseDelete.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Food meal details deleted successfully.')
                    GetFoodMealList();
                }
                else {
                    NotificationMessageController('Unable to delete food meal details at the moment. Please try again after some time.');
                }
            });
        };


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
        function reset() {
            vm.query.ItemName = '';
            vm.query.MealTypeId =0;
            vm.GetFoodMealList();
        }

    }
    function UpdateFoodMealController($scope, $http, FoodMasterService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        HolidayService, CommonService, filerService, HOST_URL, items, $filter) {
        var vm = this;
        //vm.foodupdate = items.foodDetail;
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        vm.close = filerService.close();
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            Title: '',
            HolidayDate: undefined,
            AgencyId: localStorage.agencyId,
            Description: '',
        };

        vm.AllAgeYears = CommonService.getYears();
        vm.AllAgeMonths = CommonService.getMonths();
        vm.getItems = getItems;
        getFoodMasterData();
        getMealTypeData();

        function getMealTypeData() {
            vm.AllMealTypes = null; // Clear previously loaded state list
            var myPromise = CommonService.getMealTypeData();
            myPromise.then(function (resolve) {
                vm.AllMealTypes = null;
                vm.AllMealTypes = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function getItems(MealID) {
            vm.ItemList = [];
            var MinAgeFrom = 0;
            var MinAgeTo = 0;
            var MaxAgeFrom = 0;
            var MaxAgeTo = 0;
            vm.AllFoodMaster.forEach(function (element) {
                if (vm.mealPattern.AgeGroupId == element.ID) {
                    MinAgeFrom = element.MinAgeFrom;
                    MinAgeTo = element.MinAgeTo;
                    MaxAgeFrom = element.MaxAgeFrom;
                    MaxAgeTo = element.MaxAgeTo;
                }
            })
            vm.AllFoodMaster.forEach(function (element) {
                if (element.MealTypeId == MealID && element.MinAgeFrom == MinAgeFrom && element.MinAgeTo == MinAgeTo && element.MaxAgeFrom == MaxAgeFrom && element.MaxAgeTo == MaxAgeTo) {
                    vm.ItemList.push(element);
                }

            }, this);
        }
        function getFoodMasterData() {
            vm.AllMealTypes = null; // Clear previously loaded state list
            var myPromise = CommonService.getFoodMasterData(vm.query);
            myPromise.then(function (resolve) {
                vm.AllFoodMaster = null;
                vm.AllFoodMaster = eval(resolve.Content);
                vm.AllFoodMaster.forEach(function (element) {
                    var OtherData = $filter('filter')(vm.AllAgeYears, { ID: element.MinAgeFrom }, true);
                    element.MinAgeFromName = OtherData[0].Name;
                    var OtherData1 = $filter('filter')(vm.AllAgeMonths, { ID: element.MinAgeTo }, true);
                    element.MinAgeToName = OtherData1[0].Name;
                    var OtherData3 = $filter('filter')(vm.AllAgeYears, { ID: element.MaxAgeFrom }, true);
                    element.MaxAgeFromName = OtherData3[0].Name;
                    var OtherData4 = $filter('filter')(vm.AllAgeMonths, { ID: element.MaxAgeTo }, true);
                    element.MaxAgeToName = OtherData4[0].Name;
                    element.Agegroup = element.MinAgeFromName + ' ' + element.MinAgeToName + ' - ' + element.MaxAgeFromName + ' ' + element.MaxAgeToName;

                }, this);

            }, function (reject) {

            });

        };
        vm.mealPattern = {};
        vm.ItemList = [];
        GetFoodDetailbyId();

        function GetFoodDetailbyId() {
            vm.mealPattern.AgeGroupId = items.foodDetail.AgeGroupId.toString();
            vm.mealPattern.ID = items.foodDetail.ID;
            vm.mealPattern.MealTypeId = items.foodDetail.FoodManagementMealPatternItemsInfos[0].FoodManagementMaster.MealTypeId.toString();
            vm.ItemList = [];
            var MinAgeFrom = 0;
            var MinAgeTo = 0;
            var MaxAgeFrom = 0;
            var MaxAgeTo = 0;
            items.AllFoodMaster.forEach(function (element) {
                if (vm.mealPattern.AgeGroupId == element.ID) {
                    MinAgeFrom = element.MinAgeFrom;
                    MinAgeTo = element.MinAgeTo;
                    MaxAgeFrom = element.MaxAgeFrom;
                    MaxAgeTo = element.MaxAgeTo;
                }
            })
            items.AllFoodMaster.forEach(function (element) {
                if (element.MealTypeId == vm.mealPattern.MealTypeId && element.Agegroup == items.foodDetail.AgeGroup) {
                    vm.ItemList.push(element);
                }

            }, this);
            items.foodDetail.FoodManagementMealPatternItemsInfos.forEach(function (element) {
                vm.ItemList.forEach(function (element1) {
                    if (element1.ID == element.FoodManagementMasterId)
                        element1.IsChecked = true;
                }, this);

            }, this);

        }
        vm.updateFoodData = updateFoodData;
        vm.cancelClick = cancelClick;


        vm.showProgressbar = false;
        function updateFoodData(model) {
            vm.showProgressbar = true;
            items.AllFoodMaster.forEach(function (element) {
                if (element.ID == model.AgeGroupId) {
                    model.AgeGroup = element.Agegroup;
                }
            }, this);
            model.ItemList = vm.ItemList;
            model.ClassInfoId = localStorage.classId;
            model.AgencyId = localStorage.agencyId;
            $http.post(HOST_URL.url + '/api/foodManagementMealPattern/UpdateFoodMealPattern', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    NotificationMessageController('Food meal details updated Successfully.');
                    vm.showProgressbar = false;
                    cancelClick();

                    $rootScope.$emit("GetFoodMealList", {});
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                    $mdDialog.hide();
                }
            });
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
})();