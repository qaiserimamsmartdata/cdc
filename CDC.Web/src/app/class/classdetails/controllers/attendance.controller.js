(function () {
    'use strict';

    angular
        .module('app.attendance')
        .controller('AttendanceController', AttendanceController)
        .controller('AddAttendanceController', AddAttendanceController)
        .controller('EditAttendanceController', EditAttendanceController);

    /* @ngInject */
    function AttendanceController($scope, $mdDialog, $state, $http, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, CommonService, AgencyService, AttendanceService, filerService) {
        var vm = this;
        vm.AttendanceList = AttendanceList;
        vm.showAddAttendaceDialoue = showAddAttendaceDialoue;
        vm.showEditAttendaceDialoue = showEditAttendaceDialoue;
        vm.reset = reset;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.staffCount = 0;
        $scope.selected = [];
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            Name: '',
            Date: '',
            ClassId: 0,
            AgencyId: AgencyService.AgencyId
        };

        AttendanceList();
        function AttendanceList() {
            vm.promise = AttendanceService.GetScheduledData($scope.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AttendanceList = null;
                    vm.AttendanceList = eval(response.Content);

                    for (var i = 0; i < vm.AttendanceList.length; i++) {
                        vm.AttendanceList[i].InTime = new Date(new Date().toDateString() + ' ' + vm.AttendanceList[i].InTime);
                        vm.AttendanceList[i].OutTime = new Date(new Date().toDateString() + ' ' + vm.AttendanceList[i].OutTime);
                    }
                }
                vm.close();
            });
        }
        function reset() {
            $scope.query.ClassId = 0;
            $scope.query.Name = '';
            getScheduledData();
        };
        function showAddAttendaceDialoue() {
            $mdDialog.show({
                controller: AddAttendanceController,
                controllerAs: 'vm',
                templateUrl: 'app/Attendance/views/attendance.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function showEditAttendaceDialoue(event, attendanceData) {
            $mdDialog.show({
                controller: EditAttendanceController,
                controllerAs: 'vm',
                templateUrl: 'app/Attendance/views/attendance.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { attendanceData: attendanceData },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
    function AddAttendanceController($scope, $mdDialog, $state, $http, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, CommonService, AgencyService, AttendanceService, filerService) {
        var vm = this;
        vm.SaveAttendance = SaveAttendance;
        vm.getInstructors = getInstructors;
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        vm.AttendanceData = {
            StaffScheduleId: 0,
            InTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            OutTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            OnLeave: '',
        };
        getInstructors();
        function getInstructors() {
            vm.promise = CommonService.getScheduleInstructors();
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllScheduledStaff = null;
                    vm.AllScheduledStaff = eval(response.Content.staffScheduleList);
                }
            });
        }
        function SaveAttendance(sheduleData) {
            vm.AttendanceData.InTime = (moment(vm.AttendanceData.InTime).format('HH:mm:ss'));
            vm.AttendanceData.OutTime = (moment(vm.AttendanceData.OutTime).format('HH:mm:ss'));
            vm.AttendanceData.AttendanceMarkedId = AgencyService.AgencyId;
            vm.promise = AttendanceService.AddAttendance(vm.AttendanceData);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.showProgressbar = false;
                    NotificationMessageController(response.ReturnMessage[0]);
                    reset();
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        function reset() {
            vm.AttendanceData = {
                StaffScheduleId: 0,
                InTime: new Date(1970, 0, 1, 14, 57, 0),
                OutTime: new Date(1970, 0, 1, 14, 57, 0),
                OnLeave: '',
            };
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
    function EditAttendanceController($scope, $mdDialog, $state, $http, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, CommonService, AgencyService, AttendanceService, filerService, items) {
        var vm = this;
        vm.getInstructors = getInstructors;
        vm.getAttendanceDetailsbyId = getAttendanceDetailsbyId;
        vm.UpdateAttendance = UpdateAttendance;
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        vm.AttendanceData = {
            StaffScheduleId: 0,
            InTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            OutTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            OnLeave: '',
        };
        $scope.Id=0;
        getInstructors();
        function getInstructors() {
            vm.promise = CommonService.getScheduleInstructors();
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllScheduledStaff = null;
                    vm.AllScheduledStaff = eval(response.Content.staffScheduleList);
                }
            });
        }
        function getAttendanceDetailsbyId() {
            vm.AttendanceData.InTime = (moment(items.attendanceData.InTime).format('HH:mm:ss'));
            vm.AttendanceData.OutTime = (moment(items.attendanceData.OutTime).format('HH:mm:ss'));
            vm.AttendanceData.ID = items.attendanceData.ID;
            vm.AttendanceData.StaffScheduleId = items.attendanceData.StaffScheduleId;
            $scope.Id=items.attendanceData.ID;
        }
        function UpdateAttendance(sheduleData) {
            vm.AttendanceData.InTime = (moment(vm.AttendanceData.InTime).format('HH:mm:ss'));
            vm.AttendanceData.OutTime = (moment(vm.AttendanceData.OutTime).format('HH:mm:ss'));
            vm.AttendanceData.AttendanceMarkedId = AgencyService.AgencyId;
            vm.promise = AttendanceService.AddAttendance(vm.AttendanceData);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.showProgressbar = false;
                    NotificationMessageController(response.ReturnMessage[0]);
                    reset();
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        function reset() {
            vm.AttendanceData = {
                StaffScheduleId: 0,
                InTime: new Date(1970, 0, 1, 14, 57, 0),
                OutTime: new Date(1970, 0, 1, 14, 57, 0),
                OnLeave: '',
            };
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
})();