(function () {
  'use strict';

  angular
    .module('app.class')
    .controller('MappedInstructorController', MappedInstructorController);

  /* @ngInject */
  function MappedInstructorController($scope, $http, $mdDialog, $mdEditDialog, $timeout, $stateParams, $state, ClassService, CommonService, $mdToast, $rootScope, $q, HOST_URL, MappedinstructorService,$localStorage) {
    $scope.$storage = localStorage;
    var ClassId = $scope.$storage.classId;
    if (ClassId == null)
      $state.go('triangular.classlist');
    else
      $scope.id = ClassId;
    var vm = this;
    $scope.Id = 0;
    vm.columns = {
      InstructorName: 'Name',
      PositionName: 'Position',
      Email: 'Email'  
    };
    vm.addMappedInstructor = addMappedInstructor;
    vm.editMapppedInstructor = editMapppedInstructor;
    vm.updateMappedInstructor = updateMappedInstructor;
    vm.deleteMappedInstructor = deleteMappedInstructor;
    vm.GetInstructorList = GetInstructorList;
    vm.GetAllMappedInstructorList = GetAllMappedInstructorList;
    vm.showDeleteInstructorConfirmDialoue = showDeleteInstructorConfirmDialoue;
    vm.reset = reset;
    $scope.query = {
      filter: '',
      limit: '10',
      order: '-id',
      page: 1,
      InstructorName: '',
      PositionName: '',
      Email: ''
    };
    GetInstructorList();
    GetAllMappedInstructorList();
    function GetInstructorList() {
      vm.showProgressbar = true;
      vm.promise = CommonService.getStaff();
      vm.promise.then(function (response) {
        if (response.IsSuccess) {
          vm.showProgressbar = false;
          vm.AllStaffs = null;
          vm.AllStaffs = eval(response.Content.staffList);
        }
      });
    };
    function GetAllMappedInstructorList() {
      var model = {};
      model = $scope.query;
      model.ClassId=ClassId;
      vm.promise = MappedinstructorService.GetAllMappedInstructorList($scope.query);
      vm.promise.then(function (response) {
        if (response.IsSuccess == true) {
          if (response.Content.length > 0) {
            vm.mapppedInstructorList = {};
            vm.mapppedInstructorList = response.Content;
            vm.mapppedInstructorListCount = response.TotalRows;
            $scope.selected = [];
          }
          else {
            vm.mapppedInstructorList = [];
            vm.mapppedInstructorListCount = 0;
            NotificationMessageController('No list found.');
          }
        }
        else {
          NotificationMessageController('Unable to get  list at the moment. Please try again after some time.');
        }
      });
    }
    function addMappedInstructor(formInstructor) {
      if (formInstructor.$valid) {
        vm.showProgressbar = true;
        vm.instructor.ClassId = ClassId;
        var model = {};
        model = vm.instructor;
        $http.post(HOST_URL.url + '/api/InstructorClassMap/AddInstructorClassMap', model).then(function (response) {
          if (response.data.IsSuccess == true) {
            vm.showProgressbar = false;
            reset();
            GetAllMappedInstructorList();
            NotificationMessageController(response.data.ReturnMessage[0]);
          }
          else {
            vm.showProgressbar = false;
            NotificationMessageController('Unable to add at the moment. Please try again after some time.');
          }
        });

      }
    }
    function editMapppedInstructor($event, data) {
      $http.post(HOST_URL.url + '/api/InstructorClassMap/GetInstructorClassMapById?instructorId=' + data.ID).then(function (response) {
        vm.showProgressbar = true;
        if (response.data.instructorClassMapList.ID > 0) {
          vm.showProgressbar = false;
          vm.instructor = response.data.instructorClassMapList;
          $scope.Id = 1;
          $scope.selected = [];
        }
        else {
          if (response.data.instructorClassMapList.ID == 0) {
            vm.showProgressbar = false;
            vm.staff = [];
            NotificationMessageController('Invalid found.');
          }
          else {
            vm.showProgressbar = false;
            NotificationMessageController('Unable to get list at the moment. Please try again after some time.');
          }
        }
      });
    };
    function updateMappedInstructor(formInstructor) {
      if (formInstructor.$valid) {
        vm.showProgressbar = true;
        var model = {};
        model = vm.instructor;
        $http.post(HOST_URL.url + '/api/InstructorClassMap/UpdateInstructorClassMap', model).then(function (response) {
          if (response.data.IsSuccess == true) {
            vm.showProgressbar = false;
            $scope.Id = 0;
            reset();
            GetAllMappedInstructorList();
            NotificationMessageController(response.data.ReturnMessage[0]);
          }
          else {
            vm.showProgressbar = false;
            NotificationMessageController('Unable to update at the moment. Please try again after some time.');
          }
        });
      }
    }
    function showDeleteInstructorConfirmDialoue(event, data) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
        .title('Are you sure to delete this mapped instructor permanently?')
        //   .textContent('All of this branch data and all "Branch Managers/Delivery Personals" associated with this class  will be deleted.')
        .ariaLabel('Lucky day')
        .targetEvent(event)
        .ok('Delete')
        .cancel('Cancel');
      $mdDialog.show(confirm).then(function () {
        deleteMappedInstructor(data);
      }, function () {
        $scope.hide();
      });
    }
    function deleteMappedInstructor(data) {
      $http.post(HOST_URL.url + '/api/InstructorClassMap/DeleteInstructorClassMapById?instructorId=' + data).then(function (response) {
        vm.showProgressbar = true;
        if (response.data.IsSuccess == true) {
          vm.showProgressbar = false;
          $scope.Id = 0;
          GetAllMappedInstructorList();
          NotificationMessageController(response.data.ReturnMessage[0]);
        }
        else {
          vm.showProgressbar = false;
          NotificationMessageController('Unable to get list at the moment. Please try again after some time.');
        }
      });
    };
    function reset() {
      $scope.Id = 0;
      vm.instructor.StaffId = 0;
    };
    function NotificationMessageController(message) {
      $timeout(function () {
        $rootScope.$broadcast('newMailNotification');
        $mdToast.show({
          template: '<md-toast><span flex>' + message + '</span></md-toast>',
          position: 'bottom right',
          hideDelay: 5000
        });
      }, 100);

    };
  }
})();

