﻿(function () {
    'use strict';

    angular
        .module('app.class')
        .factory('MappedinstructorService', MappedinstructorService);

    MappedinstructorService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function MappedinstructorService($http, $q, HOST_URL) {
        return {
            GetAllMappedInstructorList: GetAllMappedInstructorList
        };
      
        function SetId(Id) {
            this.Id=Id;
         }
        function GetAllMappedInstructorList(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/InstructorClassMap/GetAllInstructorClassMap', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
    }
})();