(function () {
  'use strict';

  angular
    .module('app.class')
    .controller('MappedInstructorController', MappedInstructorController);

  /* @ngInject */
  function MappedInstructorController($scope,$http,$mdDialog, $stateParams, $state, ClassService, CommonService, $mdToast, $rootScope, $q, HOST_URL,notificationService,apiService,$sessionStorage) {
    var ClassId = ClassService.Id;
    if (ClassId == null)
      $state.go('triangular.classlist');
    else

      $scope.id = ClassId;
    var vm = this;
    vm.addMappedInstructor = addMappedInstructor;
    vm.editMapppedInstructor = editMapppedInstructor;
    vm.updateMappedInstructor = updateMappedInstructor;
    vm.deleteMappedInstructor = deleteMappedInstructor;
    vm.deleteMappedInstructorSuccess=deleteMappedInstructorSuccess;
    vm.GetInstructorList = GetInstructorList;
    vm.showDeleteInstructorConfirmDialoue = showDeleteInstructorConfirmDialoue;
    vm.GetAllMappedInstructorList= GetAllMappedInstructorList;
    vm.GetAllMappedInstructorListSuccess=GetAllMappedInstructorListSuccess;
    vm.Failed=Failed;
    vm.reset = reset;
    GetInstructorList();

    function GetInstructorList() {
      vm.promise = CommonService.getStaff();
      vm.promise.then(function (response) {
        if (response.IsSuccess) {
          vm.AllStaffs = null;
          vm.AllStaffs = eval(response.Content.staffList);
        }
      });
    };

    function addMappedInstructor(formInstructor) {
      if (formInstructor.$valid) {
        vm.instructor.ClassId = ClassId;
        var model={};
        model=vm.instructor;
        $http.post(HOST_URL.url + '/api/InstructorClassMap/AddInstructorClassMap', model).then(function (response) {
          if (response.data.IsSuccess == true) {
            vm.showProgressbar = false;
            notificationService.displaymessage(response.data.ReturnMessage[0]);
            vm.GetAllMappedInstructorList();
            vm.reset(formInstructor);
          }
          else {
            vm.showProgressbar = false;
            notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
          }
        });

      }
    }

    function editMapppedInstructor($event, data) {
      $http.post(HOST_URL.url + '/api/InstructorClassMap/GetInstructorClassMapById?staffId=' + data.ID).then(function (response) {
        if (response.instructorClassMapList.ID > 0) {
          vm.staff = response.instructorClassMapList;

          $scope.selected = [];
        }
        else {
          if (response.instructorClassMapList.ID == 0) {
            vm.staff = [];
            vm.staffCount = 0;
            notificationService.displaymessage('Invalid found.');
          }
          else {
            notificationService.displaymessage('Unable to get list at the moment. Please try again after some time.');
          }
        }
      });
    };

    function updateMappedInstructor(formInstructor) {
      if (formInstructor.$valid) {

        $http.post(HOST_URL.url + '/api/InstructorClassMap/UpdateInstructorClassMap', model).then(function (response) {
          if (response.data.IsSuccess == true) {
            vm.showProgressbar = false;
            notificationService.displaymessage('Instructor removed from class successfully.');
          }
          else {
            vm.showProgressbar = false;
            notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
          }
        });
      }
    }
    function showDeleteInstructorConfirmDialoue(event, data) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
        .title('Are you sure to delete this mapped instructor permanently?')
        .ariaLabel('Lucky day')
        .targetEvent(event)
        .ok('Delete')
        .cancel('Cancel');
      $mdDialog.show(confirm).then(function () {
        deleteMappedInstructor(data);
      }, function () {
        $scope.hide();
      });
    }
    function deleteMappedInstructor(data) {
      apiService.post('/api/InstructorClassMap/DeleteInstructorClassMapById',data,
        deleteMappedInstructorSuccess,Failed)
    };
       function deleteMappedInstructorSuccess(result) {
       vm.GetAllMappedInstructorList();
      }

    function GetAllMappedInstructorList(){
      vm.GetAllMappedInstructorVm= {};
      vm.GetAllMappedInstructorVm.ClassId= $sessionStorage.ClassId;
      vm.GetAllMappedInstructorVm.page =1;
      vm.GetAllMappedInstructorVm.limit =10;
        apiService.post('/api/InstructorClassMap/GetAllInstructorClassMap',vm.GetAllMappedInstructorVm,
        GetAllMappedInstructorListSuccess,Failed)
    }
  function GetAllMappedInstructorListSuccess(result){
    vm.mapppedInstructorList= result.data.Content
  }
  function Failed(result){
    notificationService.displaymessage('Unable to get list at the moment. Please try again after some time.');
  }
  function reset(form) {
        vm.instructor=null;
        form.$setPristine();
        form.$setUntouched();
        $scope.resetCount++;
    };
  vm.GetAllMappedInstructorList();
  }
})();

