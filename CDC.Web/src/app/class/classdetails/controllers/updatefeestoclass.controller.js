(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('UpdateFeesToClassController', UpdateFeesToClassController);
    /* @ngInject */
    function UpdateFeesToClassController($filter, $scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService, AgencyService, $localStorage, notificationService) {
        var vm = this;
        if (localStorage.classId == null || localStorage.classId == undefined)
            $state.go('triangular.classlist');
        var ClassId = ClassService.Id;
        vm.query = {
            AgencyId: localStorage.agencyId
        };
        vm.showProgressbar = false;
        vm.getFeesType = getFeesType;
        getFeesType();
        GetClassDetails();
        function getFeesType() {
            vm.promise = CommonService.getFeesType(vm.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllFeesType = null;
                    vm.AllFeesType = eval(response.Content);
                }
            });
        };
        function GetClassDetails() {
            var model={
                ClassId:$scope.$storage.classId,
                TimeZone:localStorage.TimeZone
            }
            vm.promise = ClassService.GetClassListByIdService(model);
            vm.promise.then(function (response) {
                if (response.classList.ID > 0) {
                    vm.class = response.classList;
                    vm.class.MaxAgeTo = response.classList.MaxAgeTo.toString();
                    $scope.selected = [];
                }
                else {
                    if (response.classList.ID == 0) {
                        vm.class = [];
                        NotificationMessageController('Invalid found.');
                    }
                    else {
                        NotificationMessageController('Unable to get class details at the moment. Please try again after some time.');
                    }
                }
            });
        };
        vm.updatePayment = updatePayment;
        function updatePayment(paymentForm) {
            if (paymentForm.$valid) {
                $http.post(HOST_URL.url + '/api/Class/Update', vm.class).then(function (response) {
                    if (response.data.ReturnStatus == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Fees has been updated successfully.');
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                    }
                });
            }
        }
    }
})();