(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('WaitListController', WaitListController);

    /* @ngInject */
    function WaitListController($scope, $mdDialog, $stateParams, $state, ClassService, StudentService, apiService, notificationService, $localStorage) {
        var vm = this;
        vm.GetStudentList = GetStudentList;
        vm.getstudentList1 = getstudentList1;
        vm.saveWaitList = saveWaitList;
        vm.deleteWaitList = deleteWaitList;
        vm.deleteMappedWaitListSuccess = deleteMappedWaitListSuccess;
        vm.mapStudentToWaitList = mapStudentToWaitList;
        vm.mapWaitListSuccess = mapWaitListSuccess;
        vm.Failed = Failed;
        function getstudentList1() {
            GetStudentList();
            $state.go('triangular.mapstudentlist');
        }

        vm.columns = {
            Name: 'NAME',
            Class: 'CLASS',
            Room: 'ROOM',
            Gender: 'GENDER',
            BirthDate: 'DATE OF BIRTH',
            Startdate: 'START DATE',
            Enddate: 'END DATE',
            ID: 'ID'
        };

        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.studentCount = 0;
        $scope.selected = [];
        $scope.isSuperAdmin = true;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            studentclass: 0,
            name: ''
        };
        function GetStudentList() {
            vm.promise = StudentService.GetStudentListService($scope.query);
            vm.promise.then(function (response) {
                if (response.studentList.length > 0) {
                    vm.NoData = false;
                    vm.studentList = response.studentList;
                    vm.studentCount = response.studentList.length;
                    $scope.selected = [];
                }
                else {
                    if (response.studentList.length == 0) {
                        vm.NoData = false;
                        vm.studentList = [];
                        vm.studentCount = 0;
                        notificationService.displaymessage('No Waiting list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get participant list at the moment. Please try again after some time.');
                    }
                }
            });
        };

        function mapStudentToWaitList(abc) {
            vm.selectedLocationALLdata = {};
            vm.selectedLocationALLdata.StudentId = abc;
            vm.selectedLocationALLdata.ClassId = $rootScope.ClassId;
            apiService.post('/api/WaitList/MapWaitList', vm.selectedLocationALLdata,
                mapWaitListSuccess,
                Failed);
        }
        function mapWaitListSuccess(result) {
            var response = result.data.WaitList.Message;
            notificationService.displaymessage(result.data.WaitList.Message);
        }
        function Failed(result) {
            notificationService.displaymessage("Please try again");
        }

        function getMappedWaitList() {
            vm.getMappedListRequestVm = {};
            vm.getMappedListRequestVm.ClassId =localStorage.ClassId;
           
            apiService.post('/api/WaitList/GetStudentWaitList', vm.getMappedListRequestVm,
                getMappedWaitListSuccess,
                Failed);
        }
        function getMappedWaitListSuccess(result) {
            vm.WaitListObj = result.data.WaitList.Content;
        }
        function saveWaitList($event, waitlist) {
            apiService.post('/api/WaitList/saveWaitListNotes', waitlist,
                mapWaitListSuccess,
                Failed);
        }
        function deleteWaitList($event, waitlist) {
            apiService.post('/api/WaitList/deleteMappedWaitList', waitlist,
                deleteMappedWaitListSuccess,
                Failed);
        }
        function deleteMappedWaitListSuccess(result) {
            notificationService.displaymessage(result.data.WaitList.Message);
            getMappedWaitList();
        }
        getMappedWaitList();
        GetStudentList();

    }
})();