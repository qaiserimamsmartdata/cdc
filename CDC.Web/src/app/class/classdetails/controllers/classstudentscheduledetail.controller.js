(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('ClassStudentScheduleDetailController', ClassStudentScheduleDetailController);

    /* @ngInject */
    function ClassStudentScheduleDetailController($scope, notificationService, $mdDialog, $state, ClassService, AgencyService, $localStorage, HOST_URL, filerService) {
        var vm = this;
        $scope.$storage = localStorage;
        vm.toggleRight1 = filerService.toggleRight1();
        vm.isOpenRight1 = filerService.isOpenRight1();
        vm.close1 = filerService.close1();
        vm.reset = reset;

        vm.ImageUrlPath = HOST_URL.url;
        if ($scope.$storage.classId == undefined)
            $state.go('triangular.classlist');
        vm.columns = {
            Image: 'Image',
            StudentName: 'Participant',
            ClassName: 'Class Name',
            StartDate: 'Start Date',
            EndDate: 'End Date',
            Status: 'Status',
            ID: 'ID'
        };

        $scope.query = {
            filter: '',
            limit: '10',
            order: '-ClassName',
            page: 1,
            StatusId: 1,
            ClassId: $scope.$storage.classId,
            ParticipantName: '',
            StartDate: new Date()
        };
        vm.GetStudentScheduleList = GetStudentScheduleList;
        GetStudentScheduleList();
        function GetStudentScheduleList() {
            $scope.ParticipantName = $scope.query.ParticipantName == "" ? "All" : $scope.query.ParticipantName;
            $scope.StartDate = $scope.query.StartDate == null || $scope.query.StartDate == "" ? "All" : $scope.query.StartDate;
            $scope.Status = $scope.query.StatusId == 0 ? "All" : $scope.query.StatusId == 1 ? "Enrolled" : "Cancelled";
            vm.promise = ClassService.GetStudentScheduleListByIdService($scope.query);
            vm.promise.then(function (response) {
                if (response.enrollList.Content.length > 0) {
                    vm.NoData = false;
                    vm.ScheduleList = response.enrollList.Content;
                    vm.ScheduleCount = response.enrollList.TotalRows;
                    $scope.selected = [];

                }
                else {
                    if (response.enrollList.Content.length == 0) {
                        vm.NoData = true;
                        vm.ScheduleList = [];
                        vm.ScheduleCount = 0;
                    }
                    else {
                    
                    }
                }
                vm.close1();
            });
        };

        function reset() {
            $scope.query.ParticipantName = '';
            $scope.query.StatusId = 1;
             $scope.query.StartDate= new Date();
            GetStudentScheduleList();
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();