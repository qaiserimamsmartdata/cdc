(function () {
  'use strict';

  angular
    .module('app.class')
    .controller('ClassDetailsController', ClassDetailsController);


  /* @ngInject */
  function ClassDetailsController($scope, $mdDialog, notificationService, $stateParams, $state, ClassService, $localStorage) {
    var data = $stateParams.id;
    var vm = this;
    $scope.$storage = localStorage;
    if ($scope.$storage.className != null)
      vm.classname = $scope.$storage.className;
    else
      $state.go('triangular.classlist');

    vm.showTabDialog = showTabDialog;
    function showTabDialog(ev) {
      $mdDialog.show({
        controller: testclassController,
        templateUrl: 'app/class/views/pages/dialogbox/transfer_student.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      })
        .then(function (answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
          $scope.status = 'You cancelled the dialog.';
        });
    };


    //Read note dialog box start//
    vm.readnote = readnote;
    function readnote(ev) {
      $mdDialog.show({
        controller: testclassController,
        templateUrl: 'app/class/views/pages/dialogbox/readnote.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,

      })
        .then(function (answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
          $scope.status = 'You cancelled the dialog.';
        });
    };
    //Read note dialog box end//
    //Dropbox form dialog box start//
    vm.dropboxfrom = dropboxfrom;
    function dropboxfrom(ev) {
      $mdDialog.show({
        controller: testclassController,
        templateUrl: 'app/class/views/pages/dialogbox/dropboxform.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,

      })
        .then(function (answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
          $scope.status = 'You cancelled the dialog.';
        });
    };


    //Dropbox form dialog box end//
    //Dropbox form cancel dialog box start//
    vm.dropboxformcancel = dropboxformcancel;
    function dropboxformcancel(ev) {
      $mdDialog.show({
        controller: testclassController,
        templateUrl: 'app/class/views/pages/dialogbox/dropboxformcancel.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,

      })
        .then(function (answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
          $scope.status = 'You cancelled the dialog.';
        });
    };
    //Dropbox form cancel dialog box end//

    //Schedule student dialog box start//
    vm.schedulestudent = schedulestudent;
    function schedulestudent(ev) {
      $mdDialog.show({
        controller: testclassController,
        templateUrl: 'app/class/views/pages/dialogbox/schedule_student.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,

      })
        .then(function (answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
          $scope.status = 'You cancelled the dialog.';
        });
    };
    //Schedule student dialog box end//


    $scope.hide = function () {
      $mdDialog.hide();
    };

    $scope.cancel = function () {
      $mdDialog.cancel();
    };

  }



})();

