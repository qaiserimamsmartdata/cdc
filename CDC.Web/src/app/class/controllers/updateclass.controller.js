(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('UpdateClassController', UpdateClassController);


    /* @ngInject */
    function UpdateClassController($filter, $scope, notificationService, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService, AgencyService, $localStorage) {

        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };


        var vm = this;
        vm.Title = "Update Class Information";
        $scope.$storage = localStorage;
        vm.CheckUncheckOngoing = CheckUncheckOngoing;
        var ClassId = ClassService.Id;
        vm.query = {
            AgencyId: localStorage.agencyId
        };
        if ($scope.$storage.classId == null || $scope.$storage.classId == undefined)
            $state.go('triangular.classlist');
        else
            $scope.id = $scope.$storage.classId;
        vm.showProgressbar = false;
        vm.disabled = true;
        vm.MaxAge = MaxAge;
        vm.MinAge = MinAge;
        vm.setClassStartDate = setClassStartDate;
        vm.setClassEndDate = setClassEndDate;
        vm.disabledClassEndDatePicker = true;

        vm.class = {
            AgeCutOffDate: null,
            RegistrationStartDate: null,
            ClassStartDate: null,
            ClassEndDate: null,
            StartTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            EndTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            Mon: true,
            Tue: true,
            Wed: true,
            Thu: true,
            Fri: true,
            Sat: true,
            Sun: true
        };
        vm.getLocations = getLocations;
        vm.getCategory = getCategory;
        vm.getRooms = getRooms;
        vm.getSessions = getSessions;
        vm.AllAgeYears = CommonService.getYears();
        vm.AllAgeMonths = CommonService.getMonths();
        getLocations();
        getCategory();
        getSessions();
        GetClassList();
        getClassStatus();
        function CheckUncheckOngoing() {
            if (vm.class.OnGoing == true)
                vm.class.RegistrationStartDate = vm.class.ClassStartDate = vm.class.ClassEndDate = null;

        }
        function setClassStartDate(DateVal) {

            vm.disabled = false;
            vm.class.ClassStartDate = vm.class.ClassEndDate = null;
            var tempDate = new Date(DateVal.getUTCFullYear(), DateVal.getUTCMonth(), DateVal.getUTCDate(), DateVal.getUTCHours(), DateVal.getUTCMinutes(), DateVal.getUTCSeconds());
            tempDate.setDate(tempDate.getDate() + 1);
            vm.classstartdate = tempDate;
        };
        function setClassEndDate(DateVal) {
            vm.disabledClassEndDatePicker = false;
            vm.class.ClassEndDate = null;
            var tempDate = new Date(DateVal.getUTCFullYear(), DateVal.getUTCMonth(), DateVal.getUTCDate(), DateVal.getUTCHours(), DateVal.getUTCMinutes(), DateVal.getUTCSeconds());
            tempDate.setDate(tempDate.getDate() + 1);
            vm.classenddate = tempDate;
        };
        vm.MaxMonth = MaxMonth;
        function MaxMonth(data) {
            if (vm.class.MaxAgeFrom != vm.class.MinAgeFrom)
                return;
            else {
                if (parseInt(data) < parseInt(vm.class.MinAgeTo)) {
                    vm.class.MaxAgeTo = "";
                    notificationService.displaymessage("Maximum age should be greater than minimum age.")
                }
            }

        }
        vm.MinMon = MinMon;
        function MinMon(data) {
            if (vm.class.MaxAgeFrom != vm.class.MinAgeFrom)
                return
            else {
                if (parseInt(data) > parseInt(vm.class.MaxAgeTo)){
                    vm.class.MinAgeTo = "";
                    notificationService.displaymessage("Minimum age should be less than maximum age.")
                }
            }
        }
        function MaxAge(data) {
            var maxAge = parseInt(data);
            var minAge = parseInt(vm.class.MinAgeFrom);
            if (maxAge < minAge) {
                vm.class.MaxAgeFrom = "";
                notificationService.displaymessage("Maximum age should be greater than minimum age.");
            }
        }
        function MinAge(data) {
            var maxAge = parseInt(vm.class.MaxAgeFrom);
            var minAge = parseInt(data);
            if (minAge > maxAge) {
                vm.class.MinAgeFrom = "";
                notificationService.displaymessage("Minimum age should be less than maximum age.");
            }
        }
        function GetClassList() {
            var model = {
                ClassId: $scope.$storage.classId,
                TimeZone: localStorage.TimeZone
            }
            vm.promise = ClassService.GetClassListByIdService(model);
            vm.promise.then(function (response) {
                if (response.classList.ID > 0) {
                    if (response.classList.OnGoing == false) {
                        var dt = new Date();
                        vm.class.RegistrationStartDate = vm.class.ClassStartDate = vm.class.ClassEndDate = vm.class.AgeCutOffDate = new Date(dt.getUTCFullYear(), dt.getUTCMonth(), dt.getUTCDate(), dt.getUTCHours(), dt.getUTCMinutes(), dt.getUTCSeconds());
                    }
                    vm.class = response.classList;
                    vm.class.CategoryId = response.classList.CategoryId.toString();
                    vm.class.ClassStatusId = response.classList.ClassStatusId.toString();
                    vm.class.SessionId = response.classList.SessionId == null ? '' : response.classList.SessionId.toString();
                    vm.class.AgencyLocationInfoId = response.classList.AgencyLocationInfoId.toString();
                    vm.class.RoomId = response.classList.RoomId == null ? null : response.classList.RoomId.toString();
                    vm.class.MinAgeFrom = response.classList.MinAgeFrom.toString();
                    vm.class.MinAgeTo = response.classList.MinAgeTo.toString();
                    vm.class.MaxAgeFrom = response.classList.MaxAgeFrom.toString();
                    vm.class.MaxAgeTo = response.classList.MaxAgeTo.toString();
                    vm.class.StartTime = new Date(new Date().toDateString() + ' ' + response.classList.StartTime),
                        vm.class.EndTime = new Date(new Date().toDateString() + ' ' + response.classList.EndTime);
                    var dt1=  response.classList.RegistrationStartDate == null ? null : (new Date(response.classList.RegistrationStartDate));
                    var dt2 = response.classList.ClassStartDate == null ? null : (new Date(response.classList.ClassStartDate));
                    var dt3 = response.classList.ClassEndDate == null ? null : (new Date(response.classList.ClassEndDate));
                    var dt4 = response.classList.AgeCutOffDate == null ? null : (new Date(response.classList.AgeCutOffDate));

                    if (dt1 != null)
                        setClassStartDate(dt1);
                    if (dt2 != null)
                        setClassEndDate(dt2);
                    vm.class.RegistrationStartDate = dt1;
                    vm.class.ClassStartDate =dt2;
                    vm.class.ClassEndDate = dt3;
                    vm.class.AgeCutOffDate = dt4;

                    vm.class.AgencyLocationInfoId = response.classList.AgencyLocationInfoId.toString();
                    getRooms(vm.class.AgencyLocationInfoId);
                    $scope.selected = [];
                }
                else {
                    if (response.classList.ID == 0) {
                        vm.classList = [];
                        vm.classCount = 0;
                        notificationService.displaymessage('Invalid found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get class list at the moment. Please try again after some time.');
                    }
                }
            });

        };


        vm.updateClass = updateClass;
        function updateClass(registerClass) {
            if (registerClass.$valid) {
                vm.class.AgencyId = $scope.$storage.agencyId;
                vm.class.StartTime = moment(vm.class.StartTime).format('HH:mm:ss');
                vm.class.EndTime = moment(vm.class.EndTime).format('HH:mm:ss');
                vm.class.TimeZone = localStorage.TimeZone;
                $http.post(HOST_URL.url + '/api/Class/Update', vm.class).then(function (response) {
                    if (response.data.ReturnStatus == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.ReturnMessage[0]);
                        $state.go('triangular.classlist');
                    }
                    else {
                        vm.showProgressbar = false;
                        if (response.data.ReturnMessage == "EnrollCapacity_Error") {
                            notificationService.displaymessage('Unable to update the class, Please check the Enrolled capacity.');
                        }
                        else {
                            notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                        }
                        $state.go('triangular.classlist');
                    }
                });
            }
        }

        vm.cancelUpdate = cancelUpdate;
        function cancelUpdate() {
            $state.go('triangular.classlist');
        };

        function getCategory() {
            vm.promise = CommonService.getCategory(vm.query);
            vm.promise.then(function (response) {
                if (response.category.length > 0) {
                    vm.AllCategories = null;
                    vm.AllCategories = eval(response.category);
                }
            });

        };

        function getRooms(LocationId) {
            vm.query.AgencyLocationInfoId = LocationId;
            vm.promise = CommonService.getRooms(vm.query);
            vm.promise.then(function (response) {
                if (response.room.length > 0) {
                    vm.AllRooms = null;
                    vm.AllRooms = eval(response.room);
                }
            });

        };

        function getClassStatus() {
            vm.promise = CommonService.getClassStatus();
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllClassStatus = null;
                    vm.AllClassStatus = eval(response.Content);
                     vm.AllClassStatus= $filter('orderBy')(vm.AllClassStatus, 'Name');
                }
            });

        };

        function getSessions() {
            vm.promise = CommonService.getSessions();
            vm.promise.then(function (response) {
                if (response.session.length > 0) {
                    vm.AllSessions = null;
                    vm.AllSessions = eval(response.session);
                }
            });

        };
        function getLocations() {
            vm.promise = CommonService.getAgencyLocations(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllLocations = null;
                    vm.AllLocations = eval(response.Content);
                }
            });

        };
    }

})();