(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('ClassController', ClassController);


    /* @ngInject */

    function ClassController($scope, notificationService, $http, $state, $stateParams, $mdDialog, $mdEditDialog,
        $localStorage, $timeout, $mdToast, $rootScope, $q, ClassService, CommonService, AgencyService, filerService,$filter) {
        var vm = this;
        $scope.$storage = localStorage;      
        localStorage.ParticipantAttendancePage = false;
        vm.GetClassList = GetClassList;
        vm.getCategory = getCategory;
        vm.disabledClassEndDatePicker = true;
        vm.setClassEndDate = setClassEndDate;
        vm.getSessions = getSessions;
        vm.columns = {
            ClassName: 'Classes',
            SessionName: 'Session',
            CategoryName: 'Category',
            RegistrationDate: 'Registration Date',
            ClassStartDate: 'Start Date',
            ClassEndDate: 'End Date',
            StartTime: 'Start Time',
            EndTime: 'End Time',
            ID: 'ID',
            Status: 'Status',
            EnrolledParticipants: 'Enrolled Participants'
        };
        vm.editClass = editClass;
        vm.detailClassInfo = detailClassInfo;
        vm.reset = reset;
        vm.showDeleteClassConfirmDialoue = showDeleteClassConfirmDialoue;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.classCount = 0;
        $scope.selected = [];
        $scope.isSuperAdmin = true;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            statusid:0,
            studentclass: 0,
            name: '',
            categoryid:0,
            AgencyId: $scope.$storage.agencyId
        };

        GetClassList();
        getCategory();
        getSessions();

        function getCategory() {
            vm.promise = CommonService.getCategory($scope.query);
            vm.promise.then(function (response) {
                if (response.category.length > 0) {
                    vm.AllCategories = null;
                    vm.AllCategories = eval(response.category);
                }
            });
        };

         vm.showProgressbar = true;
        function GetClassList() {
            $scope.Class=$scope.query.ClassName==undefined?"All":$scope.query.ClassName;
            if ($scope.query.categoryid > 0) {
                var CategoryData = $filter('filter')( vm.AllCategories, { ID: $scope.query.categoryid });
                $scope.Category = CategoryData[0].Name;
            }
            else{
                $scope.Category="All";
            }
             $scope.Status = $scope.query.statusid==0?"All":$scope.query.statusid==1?"Active":"InActive";
            
            vm.promise = ClassService.GetClassListService($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.NoClassData = false;
                    vm.classList = response.Content;
                    vm.classCount = response.TotalRows;
                    $scope.selected = [];
                     vm.showProgressbar = false;
                }
                else {
                    if (response.Content.length == 0) {
                        vm.NoClassData = true;
                        vm.classList = [];
                        vm.classCount = 0;
                         vm.showProgressbar = false;
                        NotificationMessageController('No Class list found.');
                    }
                    else {
                        NotificationMessageController('Unable to get class list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        
        };
            getClassStatus();
        function editClass($event, data) {
            $scope.$storage.classId = null;
            $scope.$storage.classId = data.ID;       
            $state.go('triangular.updateclass');
        };
         function getClassStatus() {
            vm.promise = CommonService.getClassStatus();
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllClassStatus = null;
                    vm.AllClassStatus = eval(response.Content);
                }
            });

        };
        function showDeleteClassConfirmDialoue(event, data) {
            
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this class permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteClass(data);
            }, function () {
               
            });
        }
        function deleteClass(data) {
            var model={
                ClassId:data
            }
            vm.promise = ClassService.CheckClassForDelete(model);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Class cannot be deleted as this class is already enrolled .');
                    return;
                }
                else {
                    vm.promise = ClassService.DeleteClassById(data);
                    vm.promise.then(function (response1) {
                        if (response1.ReturnStatus == true) {
                            NotificationMessageController('Class deleted successfully.');
                            GetClassList();
                        }
                        else {

                            NotificationMessageController('Unable to get class list at the moment. Please try again after some time.');
                        }
                    });
                };

            })
        }


        function setClassEndDate(DateVal) {
                vm.disabledClassEndDatePicker = false;
                vm.minclassenddate = new Date(DateVal);
                $scope.query.classenddate = null;

        };

        function reset() {
            $scope.query.categoryid = null;
            $scope.query.statusid = 0;
            $scope.query.sessionid = 0;
            $scope.query.classenddate = $scope.query.classstartdate = null;
            $scope.query.ClassName = undefined;
            vm.minclassenddate = null;
            vm.disabledClassEndDatePicker = true;
            GetClassList();
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
        function getSessions() {
            vm.promise = CommonService.getSessions();
            vm.promise.then(function (response) {
                if (response.session.length > 0) {
                    vm.AllSessions = null;
                    vm.AllSessions = eval(response.session);
                }
            });

        };
        function detailClassInfo($event, data) {
            $scope.$storage.classId = data.ID;
            localStorage.ClassId = data.ID;
            $scope.$storage.className = data.ClassName;
            $state.go('triangular.classdetails', { id: data.ID });
        };
    }

})();