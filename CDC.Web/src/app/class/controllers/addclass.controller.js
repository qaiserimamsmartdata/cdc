(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('AddClassController', AddClassController);


    /* @ngInject */
    function AddClassController($filter, $scope, notificationService, $http, $state, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, $localStorage) {
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };
        var vm = this;
        $scope.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        vm.CheckUncheckOngoing = CheckUncheckOngoing;
        vm.addClass = addClass;
        vm.showProgressbar = false;
        vm.Title = "Add Class Information";
        vm.SubmitText = "Add Class";
        $scope.id = 0;
        $scope.isExist = 0;
        vm.enableDate = false;
        vm.registrationstartDate = new Date();
        vm.class = {};
        vm.MaxAge = MaxAge;
        vm.MinAge = MinAge;
        vm.class.OnGoing = true;
        vm.disabled = true;
        vm.setClassStartDate = setClassStartDate;
        vm.setClassEndDate = setClassEndDate;
        vm.disabledClassEndDatePicker = true;
        vm.query = {
            AgencyId: localStorage.agencyId
        };
        vm.class = {
            ID: 0,
            StartTime: new Date(new Date().toDateString() + ' ' + '09:00:00'),
            EndTime: new Date(new Date().toDateString() + ' ' + '12:00:00'),
            Mon: true,
            Tue: true,
            Wed: true,
            Thu: true,
            Fri: true,
            Sat: true,
            Sun: true
        };
        vm.getLocations = getLocations;
        vm.getCategory = getCategory;
        vm.getRooms = getRooms;
        vm.getSessions = getSessions;
        vm.AllAgeYears = CommonService.getYears();
        vm.AllAgeMonths = CommonService.getMonths();
        getLocations();
        getCategory();
        getSessions();
        getClassStatus();
        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;



        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.currentDate = new Date();
        this.showDatePicker = function (ev) {
            $mdpDatePicker($scope.currentDate, {
                targetEvent: ev
            }).then(function (selectedDate) {
                $scope.currentDate = selectedDate;
            });;
        };

        this.filterDate = function (date) {
            return moment(date).date() % 2 == 0;
        };

        this.showTimePicker = function (ev) {
            $mdpTimePicker($scope.currentTime, {
                targetEvent: ev
            }).then(function (selectedDate) {
                $scope.currentTime = selectedDate;
            });;
        }
        vm.MaxMonth = MaxMonth;
        function MaxMonth(data) {
            if (parseInt(data) < parseInt(vm.class.MinAgeTo)) {
                vm.class.MaxAgeTo = "";
                notificationService.displaymessage("Maximum age should be greater than minimum age.")
            }
        }
        vm.MinMon = MinMon;
        function MinMon(data) {
            if (parseInt(data) > parseInt(vm.class.MaxAgeTo)) {
                vm.class.MinAgeTo = "";
                notificationService.displaymessage("Minimum age should be less than maximum age.")
            }
        }
        function MaxAge(data) {
            debugger
            var maxAge = parseInt(data);
            var minAge = parseInt(vm.class.MinAgeFrom);
            if (maxAge < minAge) {
                vm.class.MaxAgeFrom = "";
                notificationService.displaymessage("Maximum age should be greater than minimum age.");
            }
        }
        function MinAge(data) {
            debugger
            var maxAge = parseInt(vm.class.MaxAgeFrom);
            var minAge = parseInt(data);
            if (minAge > maxAge) {
                vm.class.MinAgeFrom = "";
                notificationService.displaymessage("Minimum age should be less than maximum age.");
            }
        }

        function CheckUncheckOngoing() {
            if (vm.class.OnGoing == true)
                vm.class.RegistrationStartDate = vm.class.ClassStartDate = vm.class.ClassEndDate = null;

        }
        function addClass(registerClass) {

            if (registerClass.$valid) {
                vm.class.AgencyId = $scope.$storage.agencyId;
                vm.class.StartTime = (moment(vm.class.StartTime).format('HH:mm:ss'));
                vm.class.EndTime = (moment(vm.class.EndTime).format('HH:mm:ss'));
                vm.class.TimeZone = localStorage.TimeZone;
                $http.post(HOST_URL.url + '/api/Class/Add', vm.class).then(function (response) {

                    if (response.data.ReturnStatus == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.ReturnMessage[0]);
                        $state.go('triangular.classlist');
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });

            }
        }

        function getCategory() {
            vm.promise = CommonService.getCategory(vm.query);
            vm.promise.then(function (response) {
                if (response.category.length > 0) {
                    vm.AllCategories = null;
                    vm.AllCategories = eval(response.category);
                }
            });

        };
        function getLocations() {
            vm.promise = CommonService.getAgencyLocations(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllLocations = null;
                    vm.AllLocations = eval(response.Content);
                    vm.class.AgencyLocationInfoId = vm.AllLocations[0].ID.toString();
                    vm.AllRooms = null;
                    getRooms(vm.class.AgencyLocationInfoId);
                }
            });

        };
        function setClassStartDate(DateVal) {
            vm.disabled = false;
            vm.class.ClassStartDate = vm.class.ClassEndDate = null;
            var tempDate = new Date(DateVal);
            tempDate.setDate(tempDate.getDate() + 1);
            vm.classstartdate = tempDate;
        };
        function setClassEndDate(DateVal) {
            vm.disabledClassEndDatePicker = false;
            vm.class.ClassEndDate = null;
            var tempDate = new Date(DateVal);
            tempDate.setDate(tempDate.getDate() + 1);
            vm.classenddate = tempDate;
        };
        function getRooms(LocationId) {
            vm.AllRooms = null;
            vm.query.AgencyLocationInfoId = LocationId;
            vm.promise = CommonService.getRooms(vm.query);
            vm.promise.then(function (response) {
                if (response.room.length > 0) {
                    vm.AllRooms = null;
                    vm.class.RoomId = null;
                    vm.AllRooms = eval(response.room);
                }
            });

        };

        function getSessions() {
            vm.promise = CommonService.getSessions();
            vm.promise.then(function (response) {
                if (response.session.length > 0) {
                    vm.AllSessions = null;
                    vm.AllSessions = eval(response.session);
                }
            });

        };

        function getClassStatus() {
            vm.promise = CommonService.getClassStatus();
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllClassStatus = null;
                    vm.AllClassStatus = eval(response.Content);
                    vm.AllClassStatus = $filter('orderBy')(vm.AllClassStatus, 'Name');
                    vm.class.ClassStatusId = vm.AllClassStatus[0].ID.toString();
                }
            });

        };
    }
})();