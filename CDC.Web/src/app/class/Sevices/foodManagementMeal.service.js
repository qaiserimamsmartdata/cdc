﻿(function () {
    'use strict';

    angular
       .module('app.class')
        .factory('FoodMealService', FoodMealService);

    FoodMealService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function FoodMealService($http, $q, HOST_URL) {
        return {
            GetFoodMealListService:GetFoodMealListService,
            DeleteFoodMealById:DeleteFoodMealById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            this.Title='';
            this.Id = data.ID;
            this.Title = data.Title;
            this.HolidayData=data;
        };
        function GetHolidayWithoutLimit(model){
            

        }
    function GetFoodMealListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/foodManagementMealPattern/GetAllFoodManagementMealPattern', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteFoodMealById(ID) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/foodManagementMealPattern/DeleteFoodMealPatternById?foodMealPatternfId=' + ID).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


    }
})();