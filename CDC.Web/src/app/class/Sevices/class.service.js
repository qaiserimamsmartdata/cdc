﻿(function () {
    'use strict';

    angular
        .module('app.class')
        .factory('ClassService', ClassService);

    ClassService.$inject = ['$http', '$q', 'HOST_URL','$rootScope'];

    /* @ngInject */
    function ClassService($http, $q, HOST_URL,$rootScope) {
        return {
            GetClassListService: GetClassListService,
            GetClassListByIdService: GetClassListByIdService,
            DeleteClassById: DeleteClassById,
            CheckClassForDelete:CheckClassForDelete,
            SetId:SetId,
            GetStudentListByIdService:GetStudentListByIdService,
            GetStudentScheduleListByIdService:GetStudentScheduleListByIdService ,
            GetWaitingListByClassService:GetWaitingListByClassService,
            GetEnrollmentHistory:GetEnrollmentHistory         
        };

         function SetId(Id) {
            this.Id=Id;
            $rootScope.ClassId=Id;
        };
          function GetStudentListByIdService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Family/getFamilyStudentList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;


        };
          function GetStudentScheduleListByIdService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/EnrollClass/GetCurrentEnrollList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;

        };
        function GetClassListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Class/All', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function GetClassListByIdService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Class/GetById',model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function DeleteClassById(ClassId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Class/Delete?id=' + ClassId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function CheckClassForDelete(model){
            
            var deferred=$q.defer();
             $http.post(HOST_URL.url + '/api/common/GetDeleteMessage',model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };    

       function GetWaitingListByClassService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/EnrollClass/GetParticipantWaitingListByClassId', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function GetEnrollmentHistory(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/EnrollClass/GetEnrollmentHistory', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    }
})();