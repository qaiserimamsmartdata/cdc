(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('AddLessonPlanController', AddLessonPlanController);

    /* @ngInject */
    function AddLessonPlanController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, $upload, $localStorage) {
        var vm = this;
        vm.testData = ['triangular', 'is', 'great'];
        vm.addLessonPlan = addLessonPlan;
        vm.showProgressbar = false;
        vm.lessonplan = {
            Name: '',
            Date: '',
            Theme: '',
            Order: '',
            SpecialInstructions: '',
            Description: '',
            UploadFile: ''
        };
        function addLessonPlan(registerLessonPlan) {
            if (registerLessonPlan.$valid) {
                var model = {
                    Name: registerLessonPlan.Name.$modelValue,
                    Date: registerLessonPlan.Date.$modelValue,
                    Theme: registerLessonPlan.Theme.$modelValue,
                    Order: registerLessonPlan.Order.$modelValue,
                    SpecialInstructions: registerLessonPlan.SpecialInstructions.$modelValue,
                    Description: registerLessonPlan.Description.$modelValue,
                    UploadFile: registerLessonPlan.UploadFile.$modelValue,
                    ClassId: localStorage.classId,
                    AgencyId:localStorage.agencyId
                };
                $http.post(HOST_URL.url + '/api/LessonPlan/AddUpdateLessonPlan', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('No lesson plan found.');
                        $state.go('triangular.lessonplanlist');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }

        vm.upload = upload;

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };

        function upload($files) {
            if ($files !== null && $files.length > 0) {
                var $file = $files;
                $rootScope.upload = $upload.upload({
                    method: 'POST',
                    url: HOST_URL.url + '/api/LessonPlan/uploadAttachment',
                    file: $file
                }).success(function (data, status, headers) {
                    // file is downloaded successfully
                    if (data.IsSuccess == true) {
                        vm.lessonplan.UploadFile = $files[0].name
                    }
                    else {
                        $mdToast.show({
                            template: '<md-toast><span flex>' + 'Some error occured, please try again.' + '</span></md-toast>',
                            position: 'bottom right',
                            hideDelay: 5000
                        });
                    }
                    //callback(data, status, headers);
                }).error(function (data, status, headers) {
                    console.log(data);
                });
            }
        }
    }
})();