(function () {
    'use strict';

    angular
        .module('app.class')
        .controller('LessonPlanPageController', LessonPlanPageController)
        .controller('NewAddLessonPlanController', NewAddLessonPlanController)
        .controller('UpdateLessonPlanController', UpdateLessonPlanController);

    /* @ngInject */
    function LessonPlanPageController($scope, $http,HOST_URL, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, LessonPlanService, filerService, $localStorage) {
        var vm = this;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.URL=HOST_URL.url+"Images/Attachment/";
        vm.close = filerService.close();
        vm.testData = ['triangular', 'is', 'great'];
        $scope.selected = [];
        vm.columns = {
            ID: 'ID',
            Name: 'Name',
            Date: 'Date',
            Theme: 'Theme',
            Order: 'Order',
            SpecialInstructions: 'Special Instructions',
            Description: 'Description',
            UploadFile: 'Uploaded File'
        };
        vm.showAddLessonPlanController = showAddLessonPlanController;
        vm.showDeleteLessonPlanConfirmDialoue = showDeleteLessonPlanConfirmDialoue;
        vm.showUpdateLessonPlanDialoue = showUpdateLessonPlanDialoue;
        vm.lessonplanCount = 0;
        $scope.$storage = localStorage;
        var LessonPlanId = $stateParams.obj;
        vm.GetLessonPlanList = GetLessonPlanList;

        $scope.query = {
            filter: '',
            limit: '5',
            order: '-Name',
            page: 1,
            name: ''
        };
        GetLessonPlanList();

        $rootScope.$on("GetLessonPlanList", function () {
            GetLessonPlanList();
        });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function GetLessonPlanList() {
            $scope.query.ClassId = $scope.$storage.classId;
            vm.promise = LessonPlanService.GetLessonPlanListService($scope.query);
            vm.promise.then(function (response) {
                if (response.lessonPlan.length > 0) {
                    for (var i = 0; i < response.lessonPlan.length; i++) {
                        response.lessonPlan[i].Date = new Date(response.lessonPlan[i].Date);
                    }
                    vm.NoData = false;
                    vm.lessonplanList = response.lessonPlan;
                    vm.lessonplanCount = response.lessonPlan[0].TotalCount;
                    $scope.selected = [];
                }
                else {
                    if (response.lessonPlan.length == 0) {
                        vm.NoData = true;
                        vm.lessonplanList = [];
                        vm.lessonplanCount = 0;
                    }
                    else {
                        NotificationMessageController('Unable to get lessonplan list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        }

        function showAddLessonPlanController() {
            $mdDialog.show({
                controller: NewAddLessonPlanController,
                controllerAs: 'vm',
                templateUrl: 'app/class/LessonPlan/views/newaddlessonplan.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            });
        };

        function showUpdateLessonPlanDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateLessonPlanController,
                controllerAs: 'vm',
                templateUrl: 'app/class/LessonPlan/views/updatelessonplan.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                items: { lessonplandetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })


        };


        function showDeleteLessonPlanConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this LessonPlan permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteLessonPlan(data);
            }, function () {
                $scope.hide();
            });
        };
        function deleteLessonPlan(data) {
            vm.promise = LessonPlanService.DeleteLessonPlanById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Lesson plan deleted successfully.');
                    GetLessonPlanList();
                }
                else {

                    NotificationMessageController('Unable to get lessonplan list at the moment. Please try again after some time.');
                }
            });
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

    function NewAddLessonPlanController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, $upload, $localStorage) {
        var vm = this;
        vm.testData = ['triangular', 'is', 'great'];
        vm.addLessonPlan = addLessonPlan;
        vm.showProgressbar = false;
        vm.lessonplan = {
            Name: '',
            Date: '',
            Theme: '',
            Order: '',
            SpecialInstructions: '',
            Description: '',
            UploadFile: ''
        };
        vm.cancelClick = cancelClick;

        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        function cancelClick() {
            $mdDialog.cancel();
        }
        function addLessonPlan(registerLessonPlan) {
            if (registerLessonPlan.$valid) {
                var model = {
                    Name: registerLessonPlan.Name.$modelValue,
                    Date: registerLessonPlan.Date.$modelValue,
                    Theme: registerLessonPlan.Theme.$modelValue,
                    Order: registerLessonPlan.Order.$modelValue,
                    SpecialInstructions: registerLessonPlan.SpecialInstructions.$modelValue,
                    Description: registerLessonPlan.Description.$modelValue,
                    UploadFile: registerLessonPlan.UploadFile.$modelValue,
                    ClassId: localStorage.classId,
                    AgencyId: localStorage.agencyId
                };
                $http.post(HOST_URL.url + '/api/LessonPlan/AddUpdateLessonPlan', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('LessonPlan added Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetLessonPlanList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        vm.upload = upload;
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
        function upload($files) {
            if ($files !== null && $files.length > 0) {
                var $file = $files;
                $rootScope.upload = $upload.upload({
                    method: 'POST',
                    url: HOST_URL.url + '/api/LessonPlan/uploadAttachment',
                    file: $file
                }).success(function (data, status, headers) {
                    // file is downloaded successfully
                    if (data.IsSuccess == true) {
                        vm.lessonplan.UploadFile = $files[0].name
                    }
                    else {
                        $mdToast.show({
                            template: '<md-toast><span flex>' + 'Some error occured, please try again.' + '</span></md-toast>',
                            position: 'bottom right',
                            hideDelay: 5000
                        });
                    }
                    //callback(data, status, headers);
                }).error(function (data, status, headers) {
                    console.log(data);
                });
            }
        }
    }




    function UpdateLessonPlanController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, LessonPlanService, HOST_URL, items, $localStorage, $upload) {

        var vm = this;
        vm.lessonplan = {
            ID: '',
            Name: '',
            Date: '',
            Theme: '',
            Order: '',
            SpecialInstructions: '',
            Description: '',
            UploadFile: '',
            ClassId: localStorage.ClassId
        };
        $scope.CategoryList = [];
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();

        };
        // GetCategory();
        GetLessonPlanDetailbyId();

        vm.cancelClick = cancelClick;

        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        vm.upload = upload;
        function upload($files) {
            if ($files !== null && $files.length > 0) {
                var $file = $files;
                $rootScope.upload = $upload.upload({
                    method: 'POST',
                    url: HOST_URL.url + '/api/LessonPlan/uploadAttachment',
                    file: $file
                }).success(function (data, status, headers) {
                    // file is downloaded successfully
                    if (data.IsSuccess == true) {
                        vm.lessonplan.UploadFile = $files[0].name
                    }
                    else {
                        $mdToast.show({
                            template: '<md-toast><span flex>' + 'Some error occured, please try again.' + '</span></md-toast>',
                            position: 'bottom right',
                            hideDelay: 5000
                        });
                    }
                    //callback(data, status, headers);
                }).error(function (data, status, headers) {
                    console.log(data);
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        function GetLessonPlanDetailbyId() {
            // vm.lessonplan = items.lessonplandetail;
            vm.lessonplan.ID = items.lessonplandetail.ID;
            vm.lessonplan.Name = items.lessonplandetail.Name;
            vm.lessonplan.Date = new Date(items.lessonplandetail.Date);
            vm.lessonplan.Theme = items.lessonplandetail.Theme;
            vm.lessonplan.Order = items.lessonplandetail.Order;
            vm.lessonplan.SpecialInstructions = items.lessonplandetail.SpecialInstructions;
            vm.lessonplan.Description = items.lessonplandetail.Description;
            vm.lessonplan.UploadFile = items.lessonplandetail.UploadFile;
            vm.lessonplan.ClassId = localStorage.classId;


        }
        vm.updateLessonPlan = updateLessonPlan;

        function updateLessonPlan(registerLessonPlan) {
            if (registerLessonPlan.$valid) {
                var model = {
                    ID: vm.lessonplan.ID,
                    Name: registerLessonPlan.Name.$modelValue,
                    Date: registerLessonPlan.Date.$modelValue,
                    Theme: registerLessonPlan.Theme.$modelValue,
                    Order: registerLessonPlan.Order.$modelValue,
                    SpecialInstructions: registerLessonPlan.SpecialInstructions.$modelValue,
                    Description: registerLessonPlan.Description.$modelValue,
                    UploadFile: registerLessonPlan.UploadFile.$modelValue,
                    ClassId: localStorage.classId,
                    AgencyId: localStorage.agencyId
                };

                $http.post(HOST_URL.url + '/api/LessonPlan/AddUpdateLessonPlan', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('LessonPlan updated Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetLessonPlanList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }
        // function GetCategory() {
        //     $http.post(HOST_URL.url + '/api/Category/GetCategory').then(function (response) {
        //         if (response.data.category.length > 0) {

        //             $scope.CategoryList = response.data.category;
        //         }
        //     })
        // }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }





})();