(function () {
    'use strict';

    angular
        .module('app.class')
        .factory('LessonPlanService', LessonPlanService);

    LessonPlanService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function LessonPlanService($http, $q, HOST_URL) {

        return {
            GetLessonPlanListService: GetLessonPlanListService,
            DeleteLessonPlanById: DeleteLessonPlanById
        };
        function GetLessonPlanListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/LessonPlan/GetLessonPlan', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            })
                .error(function (errResp) {
                    deferred.reject({ message: "Really bad" });
                });
            return deferred.promise;
        };
        function DeleteLessonPlanById(LessonPlanId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/LessonPlan/DeleteLessonPlanById?LessonPlanId=' + LessonPlanId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
    }
})();