(function () {
    'use strict';

    angular
        .module('app.class')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.classlist', {
                url: '/classes',
                templateUrl: 'app/class/views/classlist.tmpl.html',
                // set the controller to load for this page
                controller: 'ClassController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewClass']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.addclass', {
                url: '/classes/add',
                templateUrl: 'app/class/views/addclass.tmpl.html',
                // set the controller to load for this page
                controller: 'AddClassController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.updateclass', {
                url: '/classes/update',
                templateUrl: 'app/class/views/addclass.tmpl.html',
                // set the controller to load for this page
                controller: 'UpdateClassController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.classdetails', {
                url: '/class/details',
                templateUrl: 'app/class/classdetails/views/details/classdetails.tmpl.html',
                // set the controller to load for this page
                controller: 'ClassDetailsController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.lessonplanlist', {
                url: '/classes/LessonPlan',
                templateUrl: 'app/class/LessonPlan/views/lessonplan.tmpl.html',
                controller: 'LessonPlanPageController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.addlessonplan', {
                url: '/classes/addlessonplan',
                templateUrl: 'app/class/LessonPlan/views/addlessonplan.tmpl.html',
                controller: 'AddLessonPlanController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.mapstudentlist', {
                url: '/classes/details/AddStudentList',
                templateUrl: 'app/class/classdetails/views/details/pages/MapWaitList.tmpl.html',
                controller: 'WaitListController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            });


            

        triMenuProvider.addMenu({
            name: 'Classes',
            icon: 'fa fa-book',
            type: 'dropdown',
            permission: 'viewClass',
            priority: 5,
            children: [
                {
                    name: 'Class list',
                    state: 'triangular.classlist',
                    type: 'link'
                },
                // {
                //     name: 'Skills',
                //     state: 'triangular.skills',
                //     type: 'link',
                // }
                // , {
                //     name: 'Add Class',
                //     state: 'triangular.addclass',
                //     type: 'link'
                // }
                // , {
                //     name: 'Lesson Plan',
                //     state: 'triangular.lessonplanlist',
                //     type: 'link'
                // }
                // , {
                //     name: 'Add Lesson Plan',
                //     state: 'triangular.addlessonplan',
                //     type: 'link'
                // }
            ]
        });
    }
})();
