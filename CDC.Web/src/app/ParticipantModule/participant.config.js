(function () {
    'use strict';

    angular
        .module('app.participantmodule')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.addparticipantmodule', {
                url: '/AddParticipant',
                templateUrl: 'app/ParticipantModule/views/addparticipant.tmpl.html',
                controller: 'AddParticipantModuleController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewParticipantModule']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
             .state('triangular.participantmodulelist', {
                url: '/ParticipantList',
                templateUrl: 'app/ParticipantModule/views/participantlist.tmpl.html',
                controller: 'ParticipantModuleListing',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewParticipantModule']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            // .state('triangular.updateparticipant', {
            //     url: '/Participant/Update',
            //     templateUrl: 'app/ParticipantModule/views/updateParticipant.tmpl.html',
            //     // set the controller to load for this page
            //     controller: 'UpdateParticipantModule',
            //     controllerAs: 'vm',
            //     // layout-column class added to make footer move to
            //     // bottom of the page on short pages
            //     data: {
            //         layout: {
            //             contentClass: 'layout-column'
            //         }
            //     },
            //     resolve: {
            //         data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
            //             return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
            //         }
            //     }
            // })
          
        triMenuProvider.addMenu({
            name: 'Participant',
            icon: 'fa fa-male',
            type: 'dropdown',
            priority: 3,
            permission: 'viewParticipantModule',
            children: [{
                name: 'Add Participant',
                state: 'triangular.addparticipantmodule',
                priority: 1,
                type: 'link'
            },
            {
                    name: 'Participant List',
                    state: 'triangular.participantmodulelist',
                    priority: 2,
                    type: 'link'
            }
            ]
        });
    }
})();
