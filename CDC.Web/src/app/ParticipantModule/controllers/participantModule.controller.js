(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('AddParticipantModuleController', AddParticipantModuleController);

    /* @ngInject */
    function AddParticipantModuleController($scope,$filter,$http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, AgencyService, $localStorage, imageUploadService, notificationService) {
        var vm = this;
        vm.ImageUrlPath = HOST_URL.url;
        var fileList;
        $scope.$storage = localStorage;
        vm.status = 'idle';
        vm.addNewAssociation = addNewAssociation;
        vm.GetState = GetState;
        vm.GetAllCountry = GetAllCountry;
        vm.addNewAssociationParent = addNewAssociationParent;
        vm.removeNewAssociation = removeNewAssociation;
        vm.ProfilePicComplete = "assets/images/avatars/avatar-5.png";
        vm.removeNewAssociationParent = removeNewAssociationParent;
        vm.addParticipantModule = addParticipantModule;
        vm.clearData = clearData;
        vm.clearDataParent = clearDataParent;
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            studentclass: 0,
            name: '',
            AgencyId: $scope.$storage.agencyId
        };
        getMembership();
        vm.AssociationFormObj = {
            AssociatedParticipantInfoId: '',
            AssociationTypeMasterID: ''
        }
        function clearData() {
            vm.AssociationForms = [{
                AssociatedParticipantInfoId: '',
                AssociationTypeMasterID: ''
            }];
            vm.ParentFormsObj = {
                AgencyId: $scope.$storage.agencyId,
                TimeZone: $scope.$storage.TimeZone
            };
            vm.ParentFormsObjExisting = {
                AgencyId: $scope.$storage.agencyId,
                TimeZone: $scope.$storage.TimeZone
            };
            vm.ParentAssociationForms = [{
                AssociationParent: '',
                RelationTypeId: ''
            }];
        }
        function clearDataParent() {
            vm.ParentFormsObjExisting = {
                AgencyId: $scope.$storage.agencyId,
                TimeZone: $scope.$storage.TimeZone
            };
            vm.ParentAssociationForms = [{
                NewParentInfoID: '',
                RelationTypeId: ''
            }];
        }
        GetAllCountry();
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            var myPromise = CommonService.getCountries();
            myPromise.then(function (resolve) {
                vm.AllCountries = resolve;
                vm.AddParticipant.CountryId = "1".toString();
                GetState(vm.AddParticipant.CountryId);
                //vm.ParentFormsObj.CountryId = "1".toString();
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        vm.AssociationForms = [{
            AssociatedParticipantInfoId: '',
            AssociationTypeMasterID: ''
        }];
        vm.ParentAssociationFormsObj = {
            AssociationParent: '',
            RelationTypeId: ''
        }
        vm.ParentAssociationForms = [{
            AssociationParent: '',
            RelationTypeId: ''
        }];
        function addNewAssociation() {
            vm.AssociationFormObj = {
                AssociatedParticipantInfoId: '',
                AssociationTypeMasterID: ''
            }
            vm.AssociationForms.push(vm.AssociationFormObj);
        }
        function addNewAssociationParent() {
            vm.ParentAssociationFormsObj = {
                AssociationParent: '',
                RelationTypeId: ''
            }
            vm.ParentAssociationForms.push(vm.ParentAssociationFormsObj);
        }
        function removeNewAssociation(AssociationForm) {
            vm.AssociationForms.pop(AssociationForm);
        }
        function removeNewAssociationParent(ParentAssociation) {
            vm.ParentAssociationForms.pop(ParentAssociation);
        }
        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function upload($files) {
            if ($files !== null && $files.length > 0) {
                fileList = $files;

                uploadStarted();

                $timeout(uploadComplete, 2000);
            }
        }
        function uploadStarted() {
            vm.status = 'uploading';
        }
        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image uploaded successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 5000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }
        function uploadImage($files) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image size should not exceed 2MB');
                return;
            }
            uploadStarted();

            $timeout(uploadComplete, 2000);
            imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {

                imageUploadService.uploadImage($files, vm.UploadSuccess)
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ProfilePicComplete = HOST_URL.url + data.Content;
        }
        vm.AddParticipant = {
            AgencyId: $scope.$storage.agencyId,
        };
        vm.AddParticipant.ParentInfo = [];
        vm.AddParticipant.ParentInfoExisting = [];
        vm.AddParticipant.ParticipantAssociatedParticipantMapping = [];
        vm.ParentForms=[];
        vm.ParentFormsObj = {
            FirstName: '',
            LastName: '',
            SMSCarrierId: '',
            Mobile: '',
            EmailId: '',
            SecurityKey: '',
            SecurityQuestionId: '',
            SecurityQuestionAnswer: '',
            RelationTypeId: '',
            Address: '',
            CountryId: 1,
            StateId: '',
            PostalCode: '',
            CityName: '',
            AgencyId: $scope.$storage.agencyId,
            TimeZone: $scope.$storage.TimeZone
        };
        vm.ParentForms.push(vm.ParentFormsObj);
        vm.AddParentForm=AddParentForm;
        function AddParentForm(data) {
            vm.ParentFormsObj = {
                FirstName: '',
                LastName: '',
                SMSCarrierId: '',
                Mobile: '',
                EmailId: '',
                SecurityKey: '',
                SecurityQuestionId: '',
                SecurityQuestionAnswer: '',
                RelationTypeId: '',
                Address: '',
                CountryId: 1,
                StateId: '',
                PostalCode: '',
                CityName: '',
                AgencyId: $scope.$storage.agencyId,
                TimeZone: $scope.$storage.TimeZone
            };
            vm.ParentForms.push(angular.copy(vm.ParentFormsObj));
        }
        vm.removeParent=removeParent;
        function removeParent(ParentFormsObj) {
            vm.ParentForms.pop(ParentFormsObj);
        }
        vm.ParentFormsExisting=[];
        vm.ParentFormsObjExisting = {
            FirstName: '',
            LastName: '',
            SMSCarrierId: '',
            Mobile: '',
            EmailId: '',
            SecurityKey: '',
            SecurityQuestionId: '',
            SecurityQuestionAnswer: '',
            RelationTypeId: '',
            Address: '',
            CountryId: 1,
            StateId: '',
            PostalCode: '',
            CityName: '',
            AgencyId: $scope.$storage.agencyId,
            TimeZone: $scope.$storage.TimeZone
        };
        vm.ParentFormsExisting.push(vm.ParentFormsObjExisting);
        vm.AddParentFormExisting=AddParentFormExisting;
        function AddParentFormExisting(data) {
            vm.ParentFormsObjExisting = {
                FirstName: '',
                LastName: '',
                SMSCarrierId: '',
                Mobile: '',
                EmailId: '',
                SecurityKey: '',
                SecurityQuestionId: '',
                SecurityQuestionAnswer: '',
                RelationTypeId: '',
                Address: '',
                CountryId: 1,
                StateId: '',
                PostalCode: '',
                CityName: '',
                AgencyId: $scope.$storage.agencyId,
                TimeZone: $scope.$storage.TimeZone
            };
            vm.ParentFormsExisting.push(angular.copy(vm.ParentFormsObjExisting));
        }
        vm.removeParentExisting=removeParentExisting;
        function removeParentExisting(ParentFormsObjExisting) {
            vm.ParentFormsExisting.pop(ParentFormsObjExisting);
        }
        vm.AddParticipant.ParentParticipantMapping = [];
        vm.AddParticipant.ParticipantAssociatedParticipantMapping = [];
        getSMSCarrier();
        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function CarrierAddress(data) {
            vm.AllSMSCarrier.forEach(function (element) {
                if (element.ID == data) {
                    vm.AddParticipant.ParentInfo.SMSCarrierEmailAddress = element.CarrierAddress;
                }
            }, this);

        }
        getRelationType();
        function getRelationType() {
            vm.promise = CommonService.getRelationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllRelationType = null;
                    vm.AllRelationType = eval(response.Content);
                }
            });

        };
        getSecurityQuestions();
        function getSecurityQuestions() {
            vm.AllQuestions = null; // Clear previously loaded state list
            var myPromise = CommonService.getSecurityQuestions($scope.query);

            myPromise.then(function (resolve) {
                vm.AllQuestions = null;
                vm.AllQuestions = eval(resolve.Content);
            }, function (reject) {

            });

        };

        function getMembership() {
            vm.promise = CommonService.getMembership($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllMembership = null;
                    vm.AllMembership = eval(response.Content);
                }
            });

        };
        getAssociationType();
        function getAssociationType() {
            vm.promise = CommonService.getAssociationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllAssociationType = null;
                    vm.AllAssociationType = eval(response.Content);
                }
            });

        };
        getAssociationParticipant();
        function getAssociationParticipant() {
            vm.promise = CommonService.getAssociationParticipant($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    console.log(response);
                    vm.AllAssociationParticipantType = null;
                    vm.AllAssociationParticipantType = eval(response.Content);

                }
            });

        };
        vm.getAssociationParent=getAssociationParent;
        function getAssociationParent(data,ID) 
        {
            vm.ParentAssociationForms = [{
                AssociationParent: '',
                RelationTypeId: ''
            }];
            data.forEach(function(element) {
                element.NewParticipantInfoID = element.AssociatedParticipantInfoId;
            }, this);
            vm.promise = CommonService.getAssociationParent(data);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    console.log(response);
                    vm.AllAssociationParent = null;
                    vm.AllAssociationParent = eval(response.Content);
                }
            });
            var length = vm.AssociationForms.length;
            for(var i=0;i<=vm.AssociationForms.length;i++){
                if(ID==vm.AssociationForms[i].AssociatedParticipantInfoId){ 
                 vm.AssociationForms[i].AssociatedParticipantInfoId="";               
                }
            }
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
        function addParticipantModule(data) {
            vm.showProgressbar = true;
            if (vm.AddParticipant.Radio == 'existing') {
                data.ParticipantAssociatedParticipantMapping = vm.AssociationForms;
                if (vm.AddParticipant.RadioParent == "existing") {
                    data.ParentParticipantMapping = vm.ParentAssociationForms;

                }
                else if (vm.AddParticipant.RadioParent == "newParent") {
                    data.ParentInfoExisting=vm.ParentFormsExisting;
                }
            }
            else if (vm.AddParticipant.Radio == "newParent") {
                data.ParentInfo=vm.ParentForms;
            }
            data.Radio = vm.AddParticipant.Radio;
            data.ImagePath = vm.ProfilePic;
            
            $http.post(HOST_URL.url + '/api/ParticipantModule/AddParticipantModule', data).then(function (response) {
                if (response.statusText == "OK") {
                    vm.showProgressbar = false;
                    notificationService.displaymessage("Participant added successfully.")
                    $state.go('triangular.participantmodulelist');
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }

    }
})();