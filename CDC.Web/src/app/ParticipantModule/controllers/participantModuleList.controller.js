(function () {
    'use strict';

    angular
        .module('app.enrolledparticipants')
        .controller('ParticipantModuleListing', ParticipantModuleListing)
        .controller('UpdateParticipantModule ', UpdateParticipantModule)
        .controller('ParticipantModuleDetail', ParticipantModuleDetail);

    /* @ngInject */
    function ParticipantModuleListing($scope, $http, ParticipantModuleService, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, StaffService, CommonService, AgencyService, filerService, $localStorage, HOST_URL, $filter) {
        var vm = this;
        $scope.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.ImageUrlPath = HOST_URL.url;
        vm.participantListCount = 0;
        vm.close = filerService.close();
        vm.columns = {
            Name: 'Name',
            Gender: 'Gender',
            DateOfBirth: 'Date of Birth',
            ID: 'ID',
            ProfilePic: 'Image',
            Address: 'Address',
            MembershipType: 'MembershipType'
        };
        $scope.Pageno = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            AgencyId: $scope.$storage.agencyId
        };
        vm.GetParticipantList = GetParticipantList;
        vm.showEditParticipantDialogue = showEditParticipantDialogue;
        vm.showDeleteStaffConfirmDialoue = showDeleteStaffConfirmDialoue;
        vm.showDetailParticipantDialogue = showDetailParticipantDialogue;
        vm.reset = reset;


        GetParticipantList();
        $rootScope.$on("GetParticipantList", function () {
            GetParticipantList();
        });
        function GetParticipantList() {
            $scope.Participant = $scope.query.ParticipantName == undefined || $scope.query.ParticipantName == "" ? "All" : $scope.query.ParticipantName;
            vm.promise = ParticipantModuleService.GetParticipantListService($scope.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        for (var i = 0; i < response.Content.length; i++) {
                            response.Content[i].DateOfBirth = new Date(response.Content[i].DateOfBirth);
                            if (response.Content[i].Gender == 1) {
                                response.Content[i].Gender = "Male";
                            }
                            else {
                                response.Content[i].Gender = "Female";
                            }
                        }
                        vm.participantList = {};
                        vm.participantList = response.Content;
                        vm.participantListCount = response.TotalRows;
                    }
                    else {
                        vm.participantList = [];
                        vm.participantListCount = 0;
                        NotificationMessageController('No Participant list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get Participant list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };
        function showEditParticipantDialogue(participant) {
            $mdDialog.show({
                controller: UpdateParticipantModule,
                controllerAs: 'vm',
                templateUrl: 'app/ParticipantModule/views/updateParticipant.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                items: { participantDetail: participant },
            });
        }
        function showDetailParticipantDialogue(participant) {
            $mdDialog.show({
                controller: ParticipantModuleDetail,
                controllerAs: 'vm',
                templateUrl: 'app/ParticipantModule/views/detailParticipant.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                items: { participantDetail: participant },
            });
        }
        function showDeleteStaffConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this participant permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteParticipant(data);
            }, function () {
                $scope.hide();
            });
        }
        function deleteParticipant(data) {
            vm.promise = ParticipantModuleService.DeleteParticipantById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    GetParticipantList();
                    NotificationMessageController('Participant deleted successfully.');

                }
                else {

                    NotificationMessageController('Unable to get staff list at the moment. Please try again after some time.');
                }
            });
        };

        function reset() {
            $scope.query.ParticipantName = '';
            $scope.Participant = undefined;
            GetParticipantList();
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
    function UpdateParticipantModule($scope, ParticipantModuleService,$filter, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService,
        $timeout, $mdToast, $rootScope, $q, HOST_URL, filerService, items, FamilyDetailsService, CommonService, imageUploadService) {
        var vm = this;
        vm.status = 'idle';  // idle | uploading | complete
        var fileList;
        vm.showProgressbar = false;
        vm.ImageUrlPath = HOST_URL.url;
        var ParticipantData = items.participantDetail;
        vm.ParticipantParentDetails = ParticipantParentDetails;
        $scope.DisplayText = "Update Participant";
        $scope.$storage = localStorage;
        vm.addNewAssociation = addNewAssociation;
        vm.addNewAssociationParent = addNewAssociationParent;
        vm.GetState = GetState;
        vm.GetAllCountry = GetAllCountry;
        vm.updateParticipantModule = updateParticipantModule;
        vm.clearData = clearData;
        vm.CarrierAddress = CarrierAddress;
        vm.clearDataParent = clearDataParent;
        vm.cancelClick = cancelClick;
        vm.close = filerService.close();
        $scope.query = {
            AgencyId: $scope.$storage.agencyId
        };
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);
        getMembership();
        function getMembership() {
            vm.promise = CommonService.getMembership($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllMembership = null;
                    vm.AllMembership = eval(response.Content);
                }
            });
        };
        vm.ParentForms = [];
        vm.ParentFormsObj = {
            FirstName: '',
            LastName: '',
            SMSCarrierId: '',
            Mobile: '',
            EmailId: '',
            SecurityKey: '',
            SecurityQuestionId: '',
            SecurityQuestionAnswer: '',
            RelationTypeId: '',
            Address: '',
            CountryId: 1,
            StateId: '',
            PostalCode: '',
            CityName: '',
            AgencyId: $scope.$storage.agencyId
        };
        // parag
        vm.ParentForms.push(vm.ParentFormsObj);
        vm.AddParentForm = AddParentForm;
        function AddParentForm(data) {
            vm.ParentFormsObj = {
                FirstName: '',
                LastName: '',
                SMSCarrierId: '',
                Mobile: '',
                EmailId: '',
                SecurityKey: '',
                SecurityQuestionId: '',
                SecurityQuestionAnswer: '',
                RelationTypeId: '',
                Address: '',
                CountryId: 1,
                StateId: '',
                PostalCode: '',
                CityName: '',
                AgencyId: $scope.$storage.agencyId
            };
            vm.ParentForms.push(angular.copy(vm.ParentFormsObj));
        }
        vm.removeParent = removeParent;
        function removeParent(ParentFormsObj) {
            vm.ParentForms.pop(ParentFormsObj);
        }
        vm.ParentFormsExisting = [];
        vm.ParentFormsObjExisting = {
            FirstName: '',
            LastName: '',
            SMSCarrierId: '',
            Mobile: '',
            EmailId: '',
            SecurityKey: '',
            SecurityQuestionId: '',
            SecurityQuestionAnswer: '',
            RelationTypeId: '',
            Address: '',
            CountryId: 1,
            StateId: '',
            PostalCode: '',
            CityName: '',
            AgencyId: $scope.$storage.agencyId
        };
        vm.ParentFormsExisting.push(vm.ParentFormsObjExisting);
        vm.AddParentFormExisting = AddParentFormExisting;
        function AddParentFormExisting(data) {
            vm.ParentFormsObjExisting = {
                FirstName: '',
                LastName: '',
                SMSCarrierId: '',
                Mobile: '',
                EmailId: '',
                SecurityKey: '',
                SecurityQuestionId: '',
                SecurityQuestionAnswer: '',
                RelationTypeId: '',
                Address: '',
                CountryId: 1,
                StateId: '',
                PostalCode: '',
                CityName: '',
                AgencyId: $scope.$storage.agencyId
            };
            vm.ParentFormsExisting.push(angular.copy(vm.ParentFormsObjExisting));
        }
        vm.removeParentExisting = removeParentExisting;
        function removeParentExisting(ParentFormsObjExisting) {
            vm.ParentFormsExisting.pop(ParentFormsObjExisting);
        }
        vm.AssociationForms = [{
            AssociatedParticipantInfoId: '',
            AssociationTypeMasterID: ''
        }];
        vm.AssociationFormObj = {
            AssociatedParticipantInfoId: '',
            AssociationTypeMasterID: ''
        }
        vm.ParentAssociationFormsObj = {
            NewParentInfoID: '',
            RelationTypeId: ''
        }
        vm.ParentAssociationForms = [{
            NewParentInfoID: '',
            RelationTypeId: ''
        }];
        function addNewAssociation() {
            vm.AssociationFormObj = {
                AssociatedParticipantInfoId: '',
                AssociationTypeMasterID: ''
            }
            vm.AssociationForms.push(vm.AssociationFormObj);
        }
        function addNewAssociationParent() {
            vm.ParentAssociationFormsObj = {
                NewParentInfoID: '',
                RelationTypeId: ''
            }
            vm.ParentAssociationForms.push(vm.ParentAssociationFormsObj);
        }
        vm.removeNewAssociation = removeNewAssociation;
        function removeNewAssociation(AssociationForm) {
            vm.AssociationForms.pop(AssociationForm);
        }
        vm.removeNewAssociationParent = removeNewAssociationParent;
        function removeNewAssociationParent(ParentAssociation) {
            vm.ParentAssociationForms.pop(ParentAssociation);
        }
        function clearDataParent() {
            vm.ParentFormsObjExisting = {
                AgencyId: $scope.$storage.agencyId
            };
            vm.ParentAssociationForms = [{
                NewParentInfoID: '',
                RelationTypeId: ''
            }];
        }
        function clearData() {
            vm.AssociationForms = [{
                AssociatedParticipantInfoId: '',
                AssociationTypeMasterID: ''
            }];
            vm.ParentFormsObj = {
                AgencyId: $scope.$storage.agencyId
            };
            vm.ParentFormsObjExisting = {
                AgencyId: $scope.$storage.agencyId
            };
            vm.ParentAssociationForms = [{
                NewParentInfoID: '',
                RelationTypeId: ''
            }];
        }
        getSecurityQuestions();
        function getSecurityQuestions() {
            vm.AllQuestions = null; // Clear previously loaded state list
            var myPromise = CommonService.getSecurityQuestions($scope.query);

            myPromise.then(function (resolve) {
                vm.AllQuestions = null;
                vm.AllQuestions = eval(resolve.Content);
            }, function (reject) {

            });

        };
        getSMSCarrier();
        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function CarrierAddress(data) {
            vm.AllSMSCarrier.forEach(function (element) {
                if (element.ID == data) {
                    if (vm.AddParticipant.ParentInfo != null) {
                        vm.AddParticipant.ParentInfo.SMSCarrierEmailAddress = element.CarrierAddress;
                    }
                    else if (vm.AddParticipant.ParentInfoExisting != null) {
                        vm.AddParticipant.ParentInfoExisting.SMSCarrierEmailAddress = element.CarrierAddress;
                    }

                }
            }, this);

        }
        getGradeLevel();
        function getGradeLevel() {
            vm.promise = CommonService.getGradeLevel($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllGradeLevel = null;
                    vm.AllGradeLevel = eval(response.Content);
                }
            });

        };
        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadStarted() {
            vm.status = 'uploading';
        }
        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image Uploaded Successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }
        function uploadReset() {
            vm.status = 'idle';
        }
        function updateParticipantModule(data) {
            vm.showProgressbar = true;
            if (vm.AddParticipant.Radio == 'existing') {
                data.ParticipantAssociatedParticipantMapping = vm.AssociationForms;
                if (vm.AddParticipant.RadioParent == "existing") {
                    data.ParentParticipantMapping = vm.ParentAssociationForms;

                }
                else if (vm.AddParticipant.RadioParent == "newParent") {
                    data.ParentInfoExisting = vm.ParentFormsExisting;

                }
            }
            else if (vm.AddParticipant.Radio == "newParent") {
                data.ParentInfo = vm.ParentForms;
            }
            data.Radio = vm.AddParticipant.Radio;
            $http.post(HOST_URL.url + '/api/ParticipantModule/AddParticipantModule', data).then(function (response) {
                if (response.statusText == "OK") {
                    vm.showProgressbar = false;
                    notificationService.displaymessage("Participant details updated successfully.")
                    $mdDialog.cancel();
                    $rootScope.$emit("GetParticipantList", {});
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
        function uploadImage($files) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image Size should not exceed 2MB');
                return;

            }
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {
                uploadStarted();

                $timeout(uploadComplete, 2000);
                imageUploadService.uploadImage($files, vm.UploadSuccess)
            } else {
                notificationService.displaymessage('Please select valid image format.');
            }
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ProfilePicComplete = vm.ImageUrlPath + data.Content;
            vm.AddParticipant.ImagePath = data.Content;
        }
        getAssociationType();
        function getAssociationType() {
            vm.promise = CommonService.getAssociationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllAssociationType = null;
                    vm.AllAssociationType = eval(response.Content);
                }
            });

        };
        getRelationType();
        function getRelationType() {
            vm.promise = CommonService.getRelationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllRelationType = null;
                    vm.AllRelationType = eval(response.Content);
                }
            });

        };
        function getAssociationParticipant(data) {
            vm.promise = CommonService.getAssociationParticipant($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllAssociationParticipantType = null;
                    vm.AllAssociationParticipantType = response.Content.filter(function(item) {
                        return item.ID !== data.ID;
                    });
                }
            });

        };
        vm.getAssociationParent = getAssociationParent;
        function getAssociationParent(data) {
            vm.ParentAssociationForms = [{
                NewParentInfoID: '',
                RelationTypeId: ''
            }];
            data.forEach(function (element) {
                element.NewParticipantInfoID = element.AssociatedParticipantInfoId;
            }, this);
            vm.promise = CommonService.getAssociationParent(data);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllAssociationParent = null;
                    vm.AllAssociationParent = eval(response.Content);
                }
            });

        };
        ParticipantParentDetails();
        function ParticipantParentDetails() {
            vm.showProgressbar = true;
            var model = {
                ParticipantID: ParticipantData.ID,
                AgencyId: $scope.$storage.agencyId,
                Radio: ParticipantData.Radio,
                RadioParent: ParticipantData.RadioParent
            }
            $http.post(HOST_URL.url + '/api/ParticipantModule/ParticipantParentDetails', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    vm.AddParticipant = response.data.Content;
                    vm.AddParticipant.DateOfBirth = new Date(vm.AddParticipant.DateOfBirth);
                    vm.AddParticipant.MembershipTypeId = vm.AddParticipant.MembershipTypeId.toString();
                    vm.AddParticipant.StateId = vm.AddParticipant.StateId.toString();
                    vm.AddParticipant.CountryId = vm.AddParticipant.CountryId.toString();
                    vm.ProfilePicComplete = vm.ImageUrlPath + vm.AddParticipant.ImagePath;
                    vm.AssociationForms = vm.AddParticipant.ParticipantAssociatedParticipantMapping;
                    getAssociationParticipant(vm.AddParticipant);
                    if (vm.AssociationForms != null) {
                        vm.AssociationForms.forEach(function (element) {
                            element.AssociatedParticipantInfoId = element.AssociatedParticipantInfoId.toString();
                            element.AssociationTypeMasterID = element.AssociationTypeMasterID.toString();
                        }, this);
                        getAssociationParent(vm.AssociationForms);
                        vm.ParentAssociationForms = vm.AddParticipant.ParentParticipantMapping;
                    }
                    else if (vm.AssociationForms == null) {
                        vm.AssociationForms = [{
                            AssociatedParticipantInfoId: '',
                            AssociationTypeMasterID: ''
                        }];
                    }

                    if (vm.ParentAssociationForms != null) {
                        vm.ParentAssociationForms.forEach(function (element) {
                            element.NewParentInfoID = element.NewParentInfoID.toString();
                            element.RelationTypeId = element.RelationTypeId.toString();
                        }, this);
                    }
                    else if (vm.ParentAssociationForms == null) {
                        vm.ParentAssociationForms = [{
                            NewParentInfoID: '',
                            RelationTypeId: ''
                        }];
                    }
                    if (vm.AddParticipant.ParentInfoExisting != null) {
                        vm.ParentFormsExisting = vm.AddParticipant.ParentInfoExisting;                       
                        vm.AddParticipant.ParentInfoExisting.forEach(function (element) {
                            element.SecurityKey = parseFloat(element.SecurityKey);
                            element.SecurityQuestionId = element.SecurityQuestionId.toString();
                            element.RelationTypeId = element.RelationTypeId.toString();
                            element.SMSCarrierId = element.SMSCarrierId.toString();
                            element.StateId = element.StateId.toString();
                            element.CountryId = element.CountryId.toString();
                        }, this);
                    }
                    if (vm.AddParticipant.ParentInfo != null) {
                        vm.ParentForms = vm.AddParticipant.ParentInfo;
                        vm.AddParticipant.ParentInfo.forEach(function (element) {
                            element.SecurityKey = parseFloat(element.SecurityKey);
                            element.SecurityQuestionId = element.SecurityQuestionId.toString();
                            element.RelationTypeId = element.RelationTypeId.toString();
                            element.SMSCarrierId = element.SMSCarrierId.toString();
                            element.StateId = element.StateId.toString();
                            element.CountryId = element.CountryId.toString();
                        }, this);
                    }
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });
        }

        GetAllCountry();
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            var myPromise = CommonService.getCountries();
            myPromise.then(function (resolve) {
                vm.AllCountries = resolve;
                //vm.AddParticipant.CountryId = "1".toString();
                //GetState(vm.AddParticipant.CountryId);
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };
        GetState(1);
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };

    }
    function ParticipantModuleDetail($scope, ParticipantModuleService, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService,
        $timeout, $mdToast, $rootScope, $q, HOST_URL, filerService, items, FamilyDetailsService, CommonService, imageUploadService) {
        var vm = this;
        vm.status = 'idle';  // idle | uploading | complete
        var fileList;
        vm.showProgressbar = false;
        vm.ImageUrlPath = HOST_URL.url;
        var ParticipantData = items.participantDetail;
        vm.ParticipantParentDetails = ParticipantParentDetails;
        $scope.DisplayText = "Participant Details";
        $scope.$storage = localStorage;
        vm.addNewAssociation = addNewAssociation;
        vm.addNewAssociationParent = addNewAssociationParent;
        vm.GetState = GetState;
        vm.GetAllCountry = GetAllCountry;
        vm.updateParticipantModule = updateParticipantModule;
        vm.clearData = clearData;
        vm.CarrierAddress = CarrierAddress;
        vm.clearDataParent = clearDataParent;
        vm.cancelClick = cancelClick;
        vm.close = filerService.close();
        $scope.query = {
            AgencyId: $scope.$storage.agencyId
        };
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);
        getMembership();
        function getMembership() {
            vm.promise = CommonService.getMembership($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllMembership = null;
                    vm.AllMembership = eval(response.Content);
                }
            });

        };
        getGradeLevel();
        function getGradeLevel() {
            vm.promise = CommonService.getGradeLevel($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllGradeLevel = null;
                    vm.AllGradeLevel = eval(response.Content);
                }
            });

        };
        vm.AssociationForms = [{
            AssociatedParticipantInfoId: '',
            AssociationTypeMasterID: ''
        }];
        vm.AssociationFormObj = {
            AssociatedParticipantInfoId: '',
            AssociationTypeMasterID: ''
        }
        vm.ParentAssociationFormsObj = {
            NewParentInfoID: '',
            RelationTypeId: ''
        }
        vm.ParentAssociationForms = [{
            NewParentInfoID: '',
            RelationTypeId: ''
        }];
        function addNewAssociation() {
            vm.AssociationFormObj = {
                AssociatedParticipantInfoId: '',
                AssociationTypeMasterID: ''
            }
            vm.AssociationForms.push(vm.AssociationFormObj);
        }
        function addNewAssociationParent() {
            vm.ParentAssociationFormsObj = {
                NewParentInfoID: '',
                RelationTypeId: ''
            }
            vm.ParentAssociationForms.push(vm.ParentAssociationFormsObj);
        }
        vm.removeNewAssociation = removeNewAssociation;
        function removeNewAssociation(AssociationForm) {
            vm.AssociationForms.pop(AssociationForm);
        }
        vm.removeNewAssociationParent = removeNewAssociationParent;
        function removeNewAssociationParent(ParentAssociation) {
            vm.ParentAssociationForms.pop(ParentAssociation);
        }
        function clearDataParent() {
            vm.ParentFormsObjExisting = {
                AgencyId: $scope.$storage.agencyId
            };
            vm.ParentAssociationForms = [{
                NewParentInfoID: '',
                RelationTypeId: ''
            }];
        }
        function clearData() {
            vm.AssociationForms = [{
                AssociatedParticipantInfoId: '',
                AssociationTypeMasterID: ''
            }];
            vm.ParentFormsObj = {
                AgencyId: $scope.$storage.agencyId
            };
            vm.ParentFormsObjExisting = {
                AgencyId: $scope.$storage.agencyId
            };

        }
        console.log(vm.AssociationForms);
        getSecurityQuestions();
        function getSecurityQuestions() {
            vm.AllQuestions = null; // Clear previously loaded state list
            var myPromise = CommonService.getSecurityQuestions($scope.query);

            myPromise.then(function (resolve) {
                vm.AllQuestions = null;
                vm.AllQuestions = eval(resolve.Content);
            }, function (reject) {

            });

        };
        getSMSCarrier();
        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function CarrierAddress(data) {
            vm.AllSMSCarrier.forEach(function (element) {
                if (element.ID == data) {
                    if (vm.AddParticipant.ParentInfo != null) {
                        vm.AddParticipant.ParentInfo.SMSCarrierEmailAddress = element.CarrierAddress;
                    }
                    else if (vm.AddParticipant.ParentInfoExisting != null) {
                        vm.AddParticipant.ParentInfoExisting.SMSCarrierEmailAddress = element.CarrierAddress;
                    }

                }
            }, this);

        }
        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function uploadStarted() {
            vm.status = 'uploading';
        }
        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image Uploaded Successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }
        function uploadReset() {
            vm.status = 'idle';
        }
        function updateParticipantModule(data) {
            vm.showProgressbar = true;
            if (vm.AddParticipant.Radio == 'existing') {
                data.ParticipantAssociatedParticipantMapping = vm.AssociationForms;
                if (vm.AddParticipant.RadioParent == "existing") {
                    data.ParentParticipantMapping = vm.ParentAssociationForms;

                }
                else if (vm.AddParticipant.RadioParent == "newParent") {
                    //data.ParentInfoExisting.push(vm.ParentFormsObjExisting);

                }
            }
            else if (vm.AddParticipant.Radio == "newParent") {
                //data.ParentInfo.push(vm.ParentFormsObj);
            }
            data.Radio = vm.AddParticipant.Radio;
            $http.post(HOST_URL.url + '/api/ParticipantModule/AddParticipantModule', data).then(function (response) {
                if (response.statusText == "OK") {
                    vm.showProgressbar = false;
                    notificationService.displaymessage("Participant details updated successfully.")
                    $mdDialog.cancel();
                    $rootScope.$emit("GetParticipantList", {});
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
        function uploadImage($files) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image Size should not exceed 2MB');
                return;

            }
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {
                uploadStarted();

                $timeout(uploadComplete, 2000);
                imageUploadService.uploadImage($files, vm.UploadSuccess)
            } else {
                notificationService.displaymessage('Please select valid image format.');
            }
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ProfilePicComplete = vm.ImageUrlPath + data.Content;
            vm.AddParticipant.ImagePath = data.Content;
        }
        getAssociationType();
        function getAssociationType() {
            vm.promise = CommonService.getAssociationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllAssociationType = null;
                    vm.AllAssociationType = eval(response.Content);
                }
            });

        };
        getRelationType();
        function getRelationType() {
            vm.promise = CommonService.getRelationType($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllRelationType = null;
                    vm.AllRelationType = eval(response.Content);
                }
            });

        };
        function getAssociationParticipant() {
            vm.promise = CommonService.getAssociationParticipant($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllAssociationParticipantType = null;
                    //vm.AllAssociationParticipantType = eval(response.Content);
                    var CategoryData = $filter('filter')(eval(response.Content), { ID: data.ID });
                }
            });

        };
        vm.getAssociationParent = getAssociationParent;
        function getAssociationParent(data) {
            vm.ParentAssociationForms = [{
                NewParentInfoID: '',
                RelationTypeId: ''
            }];
            data.forEach(function (element) {
                element.NewParticipantInfoID = element.AssociatedParticipantInfoId;
            }, this);
            vm.promise = CommonService.getAssociationParent(data);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllAssociationParent = null;
                    vm.AllAssociationParent = eval(response.Content);
                }
            });

        };
        ParticipantParentDetails();
        function ParticipantParentDetails() {
            vm.showProgressbar = true;
            var model = {
                ParticipantID: ParticipantData.ID,
                AgencyId: $scope.$storage.agencyId,
                Radio: ParticipantData.Radio,
                RadioParent: ParticipantData.RadioParent
            }
            $http.post(HOST_URL.url + '/api/ParticipantModule/ParticipantParentDetails', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    vm.AddParticipant = response.data.Content;
                    console.log(vm.AddParticipant);                           
                    vm.AddParticipant.DateOfBirth = new Date(vm.AddParticipant.DateOfBirth);
                    vm.AddParticipant.MembershipTypeId = vm.AddParticipant.MembershipTypeId.toString();
                    vm.AddParticipant.StateId = vm.AddParticipant.StateId.toString();
                    vm.AddParticipant.CountryId = vm.AddParticipant.CountryId.toString();
                    vm.ProfilePicComplete = vm.ImageUrlPath + vm.AddParticipant.ImagePath;
                    vm.AssociationForms = vm.AddParticipant.ParticipantAssociatedParticipantMapping;
                    if (vm.AddParticipant.Gender == 1) {
                        vm.AddParticipant.Gender = "Male";
                    }
                    else {
                        vm.AddParticipant.Gender = "Female";
                    }
                    ////Akshay's Code for StateName Block2
                    vm.States.forEach(function(element) {
                        if(element.ID==vm.AddParticipant.StateId)
                        {
                            vm.AddParticipant.StateName=element.StateName;
                        }
                    }, this);
                    ////
                    ////Akshay's Code for CountryName Block3
                    vm.AllCountries.forEach(function(element) {
                        if(element.Id=vm.AddParticipant.CountryId)
                        {
                            vm.AddParticipant.CountryName = element.CountryName;
                           
                        }
                    }, this);
                    ////
                    if (vm.AssociationForms != null) {
                        vm.AssociationForms.forEach(function (element) {
                            element.AssociatedParticipantInfoId = element.AssociatedParticipantInfoId.toString();
                            element.AssociationTypeMasterID = element.AssociationTypeMasterID.toString();
                        }, this);
                        getAssociationParent(vm.AssociationForms);
                        vm.ParentAssociationForms = vm.AddParticipant.ParentParticipantMapping;
                    }
                    else if (vm.AssociationForms == null) {
                        vm.AssociationForms = [{
                            AssociatedParticipantInfoId: '',
                            AssociationTypeMasterID: ''
                        }];
                    }
                    if (vm.ParentAssociationForms != null) {
                        vm.ParentAssociationForms.forEach(function (element) {
                            element.NewParentInfoID = element.NewParentInfoID.toString();
                            element.RelationTypeId = element.RelationTypeId.toString();
                        }, this);
                    }
                    else if (vm.ParentAssociationForms == null) {
                        vm.ParentAssociationForms = [{
                            NewParentInfoID: '',
                            RelationTypeId: ''
                        }];
                    }

                    if (vm.AddParticipant.ParentInfoExisting != null) {
                        vm.AddParticipant.ParentInfoExisting.forEach(function (element) {
                            vm.ParentFormsObjExisting = element;
                            vm.ParentFormsObjExisting.SecurityKey = parseFloat(element.SecurityKey);
                            vm.ParentFormsObjExisting.RelationTypeId = element.RelationTypeId.toString();
                            vm.ParentFormsObjExisting.SMSCarrierId = element.SMSCarrierId.toString();
                            vm.ParentFormsObjExisting.StateId = element.StateId.toString();
                            vm.ParentFormsObjExisting.CountryId = element.CountryId.toString();
                        }, this);
                    }

                    if (vm.AddParticipant.ParentInfo != null) {
                        vm.AddParticipant.ParentInfo.forEach(function (element) {
                            vm.ParentFormsObj = element;
                            vm.ParentFormsObj.SecurityKey = parseFloat(element.SecurityKey);
                            vm.ParentFormsObj.RelationTypeId = element.RelationTypeId.toString();
                            vm.ParentFormsObj.SMSCarrierId = element.SMSCarrierId.toString();
                            vm.ParentFormsObj.StateId = element.StateId.toString();
                            vm.ParentFormsObj.CountryId = element.CountryId.toString();
                        }, this);
                    }

                // ////Akshay's code Block1
                //     if (vm.ParentFormsObj.CountryId == 1) {
                //         vm.ParentFormsObj.CountryId = "United States"
                //     }
                //     else {
                //         vm.ParentFormsObj.CountryId = "None"
                //     }
                // ////

                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        GetAllCountry();
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            var myPromise = CommonService.getCountries();
            myPromise.then(function (resolve) {
                vm.AllCountries = resolve;
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };
        GetState(1);
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)

            myPromise.then(function (resolve) {
                vm.States = resolve;
               
                // vm.States.ID = country.States.ID.toString();
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };

    }

})();