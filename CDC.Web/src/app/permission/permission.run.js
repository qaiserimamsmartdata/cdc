(function () {
    'use strict';

    angular
        .module('app.permission')
        .run(permissionRun);

    /* @ngInject */
    function permissionRun($rootScope, $cookies, $state, PermissionStore, RoleStore, UserService) {
        // normally this would be done at the login page but to show quick
        // demo we grab username from cookie and login the user
        var cookieUser = $cookies.get('tri-user');
        if (angular.isDefined(cookieUser)) {
            UserService.login(cookieUser);
        }

        // create permissions and add check function verify all permissions
        // var permissions = ['viewEmail', 'viewGitHub', 'viewCalendar', 'viewLayouts', 'viewTodo', 'viewElements', 'viewAuthentication', 'viewCharts', 'viewMaps','viewStaff','viewAttendance','viewStore','viewReport','viewClass'];
        var permissions = ['viewFamilyEvent','viewChatbot','viewMealSchedular','viewStaffEvent','viewAgencyEvent','viewStaff','viewmessages', 'viewAttendance', 'viewStore', 'viewReport', 'viewClass',  'viewSkills', 'viewPermission', 'viewParticipants', 'viewEvents', 'viewStudents', 'viewTools','viewStaffSetting','viewLeave','viewSuperAdmin','viewStaffAttendance','viewDashBoard','viewStaffDashBoard','viewSuperAdminDashBoard','viewToDoTask','viewStaffSkill','viewStaffSchedule','viewParentPortalParticipants','viewFamilyRegistration','viewFamilyList','viewFamily','viewParentDashBoard','viewIncident','viewdailystatus','viewParentFamilyList','viewAllparticipants','viewEnrolledparticipants','viewFutureenrolledparticipants','viewUnenrolledparticipants','viewAttendancehistory','viewEnrollmenthistory','viewAgencyIncident','viewAgencydailystatus','viewParentAllparticipants','viewParentEnrolledparticipants','viewParentFutureEnrolledparticipants','viewParentUnenrolledparticipants','viewParentAttendancehistory','viewParentEnrollmenthistory','viewParentIncident','viewParentdailystatus','viewStaffLeaves','viewPortalLeaves','viewAgencyParticipantAttendance','viewStaffPortalAttendance','viewAgencyFailedSignouts','viewStaffPortalFailedSignouts','viewParticipantAttendance','viewKioskParticipantAttendance','viewAgencyAddParticipant','viewAgencyListParticipant'];
        PermissionStore.defineManyPermissions(permissions, function (permissionName) {
            var result = UserService.hasPermission(permissionName);
            return result;
        });

        // create roles for app
        RoleStore.defineManyRoles({
            'AGENCY': ['viewAgencyEvent','viewMealSchedular','viewChatbot','viewStaff','viewmessages', 'viewAttendance', 'viewStore', 'viewReport', 'viewClass', 'viewSkills', 'viewPermission', 'viewParticipants', 'viewEvents', 'viewStudents', 'viewTools','viewStaffAttendance','viewDashBoard','viewToDoTask','viewFamilyList','viewFamilyRegistration','viewFamily','viewIncident','viewdailystatus','viewAllparticipants','viewEnrolledparticipants','viewFutureenrolledparticipants','viewUnenrolledparticipants','viewAttendancehistory','viewEnrollmenthistory','viewAgencyIncident','viewAgencydailystatus','viewStaffLeaves','viewAgencyParticipantAttendance','viewAgencyFailedSignouts','viewParticipantAttendance','viewKioskParticipantAttendance','viewAgencyAddParticipant','viewAgencyListParticipant'],
            'STAFF': ['viewStaffEvent','viewAttendance','viewStaffSetting','viewLeave','viewStaffDashBoard','viewStaffSkill','viewStaffSchedule','viewStaffPortalAttendance','viewPortalLeaves','viewStaffPortalFailedSignouts'],
            'SuperAdmin':['viewSuperAdmin','viewSuperAdminDashBoard'],
            'FAMILY':['viewFamilyEvent','viewParticipants','viewmessages','viewParentFamilyList','viewParentDashBoard','viewFamily','viewIncident','viewdailystatus','viewParentAllparticipants','viewParentEnrolledparticipants','viewParentFutureEnrolledparticipants','viewParentUnenrolledparticipants','viewParentAttendancehistory','viewParentEnrollmenthistory','viewParentIncident','viewParentdailystatus']
        });


        ///////////////////////

        // default redirect if access is denied
        function accessDenied() {
            $state.go('401');
        }

        // watches

        // redirect all denied permissions to 401
        var deniedHandle = $rootScope.$on('$stateChangePermissionDenied', accessDenied);

        // remove watch on destroy
        $rootScope.$on('$destroy', function () {
            deniedHandle();
        });
    }
})();
