(function () {
    'use strict';

    angular
        .module('app')
        .factory('planrateService', planrateService);

    planrateService.$inject = ['$http', '$location', 'notificationService', '$rootScope', '$q', 'HOST_URL'];

    function planrateService($http, $location, $rootScope, notificationService, $templateRequest, HOST_URL, $q) {
        var planRate = null;
        return {
            getplanRate: function () {
                return planRate;
            },
            setplanRate: function (value) {
                planRate = value;
            }
        }
    }
})();