
angular.module('app').filter('ampmtime', function ($filter) {
    return function (input) {
        if (input == null) { return ""; }
        input = input.toString();
        var hours = (input.split(':')[0]);
        var minutes = (input.split(':')[1]);
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        hours = hours < 10 ? '0' + hours : hours;
        if (minutes.length == 2  ) {
            minutes = minutes < 10 ?  minutes : minutes;
        }
        else {
            minutes = minutes < 10 ? '0' + minutes : minutes;
        }

        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
});

// angular.module('app').filter('time', function ($filter) {
//     return function (input) {
//         if (input == null) { return ""; }

//         var _date = $filter('date')(new Date(input), 'HH:mm:ss');

//         return _date.toUpperCase();

//     };
// });
