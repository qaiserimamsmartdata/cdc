angular
    .module('app')
    .directive('hardChange', function () {
        return {
            restrict: A,
            require: 'ngModel',
            link: function (scope, elem, attr, ctrl) {
                scope.$watch(function () {
                    return ctrl;
                }, function (newVal, oldVal) {
                    if (newVal === oldVal) return;
                    scope.$eval(attr.hardChange)
                }, true);
            }
        };
    });