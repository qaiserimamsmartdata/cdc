angular
    .module('app')
    .directive('usernameAvailable', function ($timeout, $q, $http, HOST_URL) {
        return {
            restrict: 'AE',
            require: 'ngModel',
            //scope:{},
            link: function (scope, elm, attr, model) {
                model.$asyncValidators.usernameExists = function () {

                    //here you should access the backend, to check if username exists
                    //and return a promise
                    //here we're using $q and $timeout to mimic a backend call 
                    //that will resolve after 1 sec
                    var val = elm.val();
                    var datareq = { 'Name': val };
                    var defered = $q.defer();
                    $timeout(function () {
                        $http.post(HOST_URL.url + '/api/common/IsExistEmailId', datareq).success(function (response, status, headers, config) {
                            console.log(response);
                            model.$setValidity('usernameExists', response);
                            defered.resolve;
                        }).error(function (errResp) {

                        });
                    }, 1000);
                    return defered.promise;

                    // var defer = $q.defer();
                    // $timeout(function(){
                    //   model.$setValidity('usernameExists', false); 
                    //   defer.resolve;
                    // }, 1000);
                    // return defer.promise;
                };
            }
        }
    });