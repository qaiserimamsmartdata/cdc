// (function (angular) {
//  'use strict';

//  angular.directive('allowPattern', allowPattern);

//  function allowPattern() {
//   return {
//    restrict: "A",
//    compile: function (tElement, tAttrs) {
//     return function (scope, element, attrs) {
//      // I handle key events
//         element.bind("keypress paste", function (event) { 
//          var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
//          if ((event.shiftKey == true) || (keyCode != 8 && keyCode != 9 && keyCode != 37 && keyCode != 38 && keyCode != 39 && keyCode != 40)) {
//              var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode. 
//              // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
//              if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
//                  event.preventDefault();
//                  return false;
//              }
//          }
//      });
//     };
//    }
//   };
//  }

// })(angular.module('common.ui'));