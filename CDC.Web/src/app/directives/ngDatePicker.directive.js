

    angular
    .module('app')
    .directive('datetimepickerNeutralTimezone', ['$http', 'HOST_URL','$localStorage', function (async, HOST_URL,$localStorage) {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                 ctrl.$formatters.push(function (value) {
                    var date = new Date(Date.parse(value));
                    var date1 =new Date(Date.parse(value));
                    date = new Date(date.getTime() + (60000 * date.getTimezoneOffset()));
                    //var test=new Date().toLocaleString("en-US", {timeZone: "America/New_York"});
              
                    var ajaxConfiguration = { method: 'POST', url: HOST_URL.url + 'api/Common/GetCurrentDateByZone',data:{TimeZone:localStorage.TimeZone}};
                    async(ajaxConfiguration)
                        .success(function (data, status, headers, config) {
                            //  date1 =new Date(Date.parse(data));
                            var  date1 = new Date(data);
                                return data;
                        });
                     return date1;

                });

                ctrl.$parsers.push(function (value) {
                    var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
                    //return date;
                    var date1 ="";
                      var ajaxConfiguration = { method: 'POST', url: HOST_URL.url + 'api/Common/GetCurrentDateByZone',data:{TimeZone:localStorage.TimeZone}};
                    async(ajaxConfiguration)
                        .success(function (data, status, headers, config) {
                             
                             date1 = data;
                             //return data;
                        });
                     return date1;
                });
            }
        }
    }]);