
(function () {
    'use strict';

    angular
        .module('app.staffskill')
        .controller('StaffSkillController', StaffSkillController)
        .controller('AddStaffSkillController', AddStaffSkillController)
        .controller('UpdateStaffSkillController', UpdateStaffSkillController);



    /* @ngInject */
    function StaffSkillController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        StaffSkillService,StaffService, CommonService, filerService) {
        var vm = this;
        // vm.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.columns = {
            ID: 'ID',
            SkillMasterId: 'SkillMasterId',
            Description: 'Description',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.staffskillCount = 0;
        vm.selected = [];
        //vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            SkillMasterId: 0,
            StaffId:  localStorage.staffId == null ? StaffService.Id : localStorage.staffId ,
            AgencyId: localStorage.agencyId,
            Description:'',
        };
        localStorage.ParticipantAttendancePage = false;

        // $scope.number = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize;

        vm.GetStaffSkillList = GetStaffSkillList;
        vm.showAddStaffSkillDialoue = showAddStaffSkillDialoue;
        vm.showUpdateStaffSkillDialoue = showUpdateStaffSkillDialoue;
        vm.showDeleteStaffSkillConfirmDialoue = showDeleteStaffSkillConfirmDialoue;
        vm.reset = reset;
        GetStaffSkillList();

        vm.getSkillMaster = getSkillMaster;
        getSkillMaster();
        function getSkillMaster() {
            vm.addQuery = {
                filter: '',
                limit: '10',
                order: '-id',
                page: 1,
                AgencyId: localStorage.agencyId,
            };
            vm.AllSkillMaster = null;
            vm.promise = CommonService.getSkillMaster(vm.addQuery);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllSkillMaster = response.Content;
                    //console.log(response.Content);
                }
            });
        }

        $rootScope.$on("GetStaffSkillList", function () {
            GetStaffSkillList();
        });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function GetStaffSkillList() {
            //console.log(vm.$storage);
            //console.log(localStorage);
            vm.promise = StaffSkillService.GetStaffSkillListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                         vm.NoToDoData = false;
                        vm.staffskillList = {};
                        vm.staffskillList = response.Content;
                        vm.staffskillCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                         vm.NoToDoData = true;
                        vm.staffskillList = [];
                        vm.staffskillCount = 0;
                        NotificationMessageController('No Staff Skill list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get staff skill list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };


        function showAddStaffSkillDialoue(event, data) {
            $mdDialog.show({
                controller: AddStaffSkillController,
                controllerAs: 'vm',
                templateUrl: 'app/StaffSkill/views/addstaffskill.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            })
        };

        function showUpdateStaffSkillDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateStaffSkillController,
                controllerAs: 'vm',
                templateUrl: 'app/StaffSkill/views/updatestaffskill.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { staffskilldetail: data },
                fullscreen: $scope.customFullscreen
            })
        };

        function showDeleteStaffSkillConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this staff skill permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteStaffSkill(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteStaffSkill(data) {
            vm.promise = StaffSkillService.DeleteStaffSkillById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('Staff skill deleted successfully.');
                    GetStaffSkillList();
                }
                else {
                    NotificationMessageController('Unable to get staff skill list at the moment. Please try again after some time.');
                }
            });
        };


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.Name = '';
            vm.query.PriorityId = 0,
                vm.query.DueDate = null,
                GetStaffSkillList();
        };



    }
    //New Controller For Add
    function AddStaffSkillController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        StaffSkillService,StaffService, CommonService, HOST_URL) {
        var vm = this;
        vm.staffskill = {
            Name: '',
            ID: 0,
            Description : '',
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        vm.addStaffSkill = addStaffSkill;
        vm.cancelClick = cancelClick;

        vm.getSkillMaster = getSkillMaster;
        getSkillMaster();

        vm.todayDate = new Date();

 



        function addStaffSkill(addStaffSkill) {
            if (addStaffSkill.$valid) {
                var model = {
                    ID: vm.staffskill.ID,
                    SkillMasterId: vm.staffskill.SkillMasterId,
                    StaffId: localStorage.staffId == null ? StaffService.Id : localStorage.staffId,
                    AgencyId: localStorage.agencyId,
                    Description: vm.staffskill.Description,
                };

                $http.post(HOST_URL.url + '/api/StaffSkill/AddUpdateStaffSkill', model).then(function (response) {

                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Staff skill added successfully.');
                        cancelClick();
                        $rootScope.$emit("GetStaffSkillList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function getSkillMaster() {
            vm.addQuery = {
                filter: '',
                limit: '10',
                order: '-id',
                page: 1,
                AgencyId: localStorage.agencyId,
            };
            vm.AllSkillMaster = null;
            vm.promise = CommonService.getSkillMaster(vm.addQuery);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllSkillMaster = response.Content;
                    //console.log(response.Content);
                }
            });
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }


    //New Controller For Update
    function UpdateStaffSkillController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q,
        StaffSkillService,StaffService, CommonService, HOST_URL, items,$localStorage) {
        var vm = this;
        vm.staffskill = {
            ID: 0,
            SkillMasterId: 0,
            StaffId: 0,
            Description:'',
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        vm.getSkillMaster = getSkillMaster;
        getSkillMaster();

        GetStaffSkillDetailbyId();

        function GetStaffSkillDetailbyId() {
            vm.staffskill.ID = items.staffskilldetail.ID;
            vm.staffskill.SkillMasterId = items.staffskilldetail.SkillMasterId.toString();
            vm.staffskill.StaffId = items.staffskilldetail.StaffId;
             vm.staffskill.Description = items.staffskilldetail.Description;
        }
        vm.updateStaffSkill = updateStaffSkill;
        vm.cancelClick = cancelClick;



        function updateStaffSkill(updateStaffSkill) {
            if (updateStaffSkill.$valid) {
                var model = {
                    ID: vm.staffskill.ID,
                    SkillMasterId: vm.staffskill.SkillMasterId,
                    StaffId: vm.staffskill.StaffId,
                    Description: vm.staffskill.Description,
                    AgencyId: localStorage.agencyId,
                    IsDeleted: false,
                };

                $http.post(HOST_URL.url + '/api/StaffSkill/AddUpdateStaffSkill', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Staff skill updated successfully.');
                        cancelClick();
                        $rootScope.$emit("GetStaffSkillList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function getSkillMaster() {
            vm.addQuery = {
                filter: '',
                limit: '10',
                order: '-id',
                page: 1,
                AgencyId: localStorage.agencyId,
            };
            vm.AllSkillMaster = null;
            vm.promise = CommonService.getSkillMaster(vm.addQuery);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllSkillMaster = response.Content;
                    //console.log(response.Content);
                }
            });
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

})();