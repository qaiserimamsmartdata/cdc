﻿(function () {
    'use strict';

    angular
        .module('app.staffskill')
        .factory('StaffSkillService', StaffSkillService);

    StaffSkillService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function StaffSkillService($http, $q, HOST_URL) {
        return {
            GetStaffSkillListService:GetStaffSkillListService,
            DeleteStaffSkillById:DeleteStaffSkillById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            //this.Name='';
            this.Id = data.ID;
          //  this.Descri = data.Name;
            this.StaffSkillData=data;
        };

    function GetStaffSkillListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StaffSkill/GetAllStaffSkill', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteStaffSkillById(StaffSkillId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StaffSkill/DeleteStaffSkill?staffskillId=' + StaffSkillId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


    }
})();