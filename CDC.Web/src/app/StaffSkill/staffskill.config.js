
(function () {
    'use strict';

    angular
        .module('app.staffskill')
        .config(moduleConfig);

    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider

            .state('triangular.staffskill', {
                url: '/staffskill',
                templateUrl: 'app/StaffSkill/views/staffskill.tmpl.html',
                controller: 'StaffSkillController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewStaffSkill']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"STAFF",$window);
                    }
                }
            })

            ;

        triMenuProvider.addMenu({
            name: 'Staff Skill',
            icon: 'fa fa-asterisk',
            type: 'link',
            priority: 1.9,
            permission: 'viewStaffSkill',
            state: 'triangular.staffskill'
        });

    }
})();
