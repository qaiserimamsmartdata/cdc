(function() {
    'use strict';

    angular
        .module('app.mealSchedular')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
          .state('triangular.mealSchedular', {
                url: '/mealScheduler',
                templateUrl: 'app/MealSchedular/mealSchedular.tmpl.html',
                // set the controller to load for this page
                controller: 'MealSchedularController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewMealSchedular']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
        
        ;

        triMenuProvider.addMenu({
            name: 'Meal Scheduler',
            icon: 'fa fa-coffee',
            type: 'link',
            permission: 'viewMealSchedular',
            state: 'triangular.mealSchedular',
            priority: 9

        });
    }
})();
