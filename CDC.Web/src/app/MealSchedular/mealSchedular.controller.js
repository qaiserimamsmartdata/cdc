(function () {
    'use strict';

    angular
        .module('app.mealSchedular')
        .controller('MealSchedularController', MealSchedularController);

    /* @ngInject */
    function MealSchedularController($scope, $http, $localStorage, HOST_URL,$window) {
        var vm = this;
        $scope.agencyId = localStorage.agencyId;
        localStorage.ParticipantAttendancePage = false;
         $scope.IsLoaded = false;
        window.uploadDone = function () {
            $scope.IsLoaded = true;
        }
    }
})();