(function() {
    'use strict';

    angular
        .module('app.events')
        .controller('EventsPageController', EventsPageController);

    /* @ngInject */
    function EventsPageController() {
        var vm = this;
        vm.testData = ['triangular', 'is', 'great'];
    }
})();