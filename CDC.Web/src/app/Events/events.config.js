(function () {
    'use strict';

    angular
        .module('app.events')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.events', {
                url: '/events',
                templateUrl: 'app/Events/views/events.tmpl.html',
                // set the controller to load for this page
                controller: 'EventsPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewEvents']
                    }
                }
            });

        // triMenuProvider.addMenu({
        //     name: 'events',
        //     icon: 'fa fa-tree',
        //     type: 'dropdown',
        //     permission: 'viewEvents',
        //     priority: 1.1,
        //     children: [{
        //         name: 'events',
        //         state: 'triangular.events',
        //         type: 'link'
        //     }]
        // });
    }
})();
