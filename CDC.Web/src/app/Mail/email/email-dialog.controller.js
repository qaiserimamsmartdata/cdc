(function () {
    'use strict';

    angular
        .module('app.mail')
        .controller('EmailDialogController', EmailDialogController);

    /* @ngInject */
    function EmailDialogController($timeout, $q,$localStorage, $mdDialog,HOST_URL,$http, $filter, triSkins, textAngularManager, title, email, contacts, getFocus) {
        var contactsData = contacts.data;

        var vm = this;
        vm.cancel = cancel;
        vm.email = email;
        vm.title = title;
        vm.send = send;
        vm.showCCSIcon = 'zmdi zmdi-account-add';
        vm.showCCS = false;
        vm.toggleCCS = toggleCCS;
        vm.triSkin = triSkins.getCurrent();
        vm.queryContacts = queryContacts;
        // vm.contact=vm.email.to[0].from;
        //  vm.contact.name=vm.email.to.from.email;
        //  ;
        ///////////////

        function cancel() {
            $mdDialog.cancel();
        }

        function toggleCCS() {
            vm.showCCS = !vm.showCCS;
            vm.showCCSIcon = vm.showCCS ? 'zmdi zmdi-account' : 'zmdi zmdi-account-add';
        }

        function send(model) {
             var deferred = $q.defer();
            model.fromUserID= localStorage.UserId;
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Email/ReplyEmail', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            $mdDialog.hide(vm.email);
            return deferred.promise;
            
        }

            
        



        function queryContacts($query) {
            var lowercaseQuery = angular.lowercase($query);
            var contact = contactsData.Content;
            return contactsData.Content.filter(function (contact) {
                var lowercaseName = angular.lowercase(contact.name);
                if (lowercaseName.indexOf(lowercaseQuery) !== -1) {
                    return contact;
                }
            });
        }

        ////////////////
        if (getFocus) {
            $timeout(function () {
                // Retrieve the scope and trigger focus
                var editorScope = textAngularManager.retrieveEditor('emailBody').scope;
                editorScope.displayElements.text.trigger('focus');
            }, 500);
        }
    }
})();