(function () {
    'use strict';

    angular
        .module('app.mail')
        .config(moduleConfig)
        .constant('EMAIL_ROUTES', [{
            state: 'triangular.inbox',
            name: 'Inbox',
            url: '/email/inbox/:id?'
        }, {
                state: 'triangular.trash',
                name: 'Trash',
                url: '/email/trash:id?'
            }, {
                state: 'triangular.sent',
                name: 'Sent',
                url: '/email/sent:id?'
            }]);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider, EMAIL_ROUTES) {

        $stateProvider
            .state('triangular.mail', {
                abstract: true,
                views: {
                    'toolbar@triangular': {
                        templateUrl: 'app/Mail/email/layout/toolbar/toolbar.tmpl.html',
                        controller: 'EmailToolbarController',
                        controllerAs: 'vm'
                    }
                },
                data: {
                    layout: {
                        footer: false,
                        contentClass: 'triangular-non-scrolling'
                    },
                    permissions: {
                        only: ['viewMail']
                    }
                }
            });

        // if(localStorage.roles[0]=="AGENCY")
        // {

        // }

        angular.forEach(EMAIL_ROUTES, function (route) {
            $stateProvider
                .state(route.state, {
                    url: route.url,
                    views: {
                        '@triangular': {
                            templateUrl: 'app/Mail/email/inbox.tmpl.html',
                            controller: 'InboxController',
                            controllerAs: 'vm'
                        }
                    },
                    resolve: {
                        emails: function ($http, $q, HOST_URL, $localStorage) {

                            if (JSON.parse(localStorage.roles[0]) == "AGENCY") {
                                // var URL = HOST_URL.url + '/api/Email/Inbox';
                                // // var model={};
                                // return $http({
                                //     method: 'POST',
                                //     url: URL,

                                // });

                            }
                            else if (JSON.parse(localStorage.roles[0]) == "FAMILY") {
                                var URL = HOST_URL.url + '/api/Email/Inbox';
                                var model={
                                    UserId: localStorage.UserId
                                };
                                return $http({
                                    method: 'POST',
                                    url: URL,
                                   data:model

                                });
                            }
                        },
                        contacts: function ($http, HOST_URL, $localStorage) {
                            if (JSON.parse(localStorage.roles[0]) == "AGENCY") {
                                //   var model={
                                //     AgencyId:localStorage.agencyId
                                // }
                                // var URL = HOST_URL.url + '/api/Email/AgencyContacts';
                                // return $http({
                                //     method: 'GET',
                                //     url: URL,
                                //     data: model


                                // });

                            }
                            else if (JSON.parse(localStorage.roles[0]) == "FAMILY") {
                              
                              
                                var URL = HOST_URL.url + '/api/Email/FamilyContacts';
                                return $http({
                                    method: 'GET',
                                    url: URL
                                    //data:model
                                    
                                    


                                });
                            }

                        }
                    }
                });
        });


        angular.forEach(EMAIL_ROUTES, function (route) {
            $stateProvider
                .state(route.state + '.email', {
                    url: '/mail/:emailID',
                    templateUrl: 'app/Mail/email/email.tmpl.html',
                    controller: 'EmailController',
                    controllerAs: 'vm',
                    resolve: {
                        email: function ($stateParams, emails) {
                            var content = emails.data.Content;

                            var foundEmail = false;
                            for (var i = 0; i < content.length; i++) {
                                content[i].id = content[i].id.toString();
                                if (content[i].id === $stateParams.emailID) {
                                    foundEmail = content[i];
                                    break;
                                }
                            }
                            return foundEmail;
                        }
                    },
                    onEnter: function ($state, email) {
                        if (false === email) {
                            $state.go(route.state);
                        }
                    }
                });
        });

        var emailMenu = {
            name: 'Messages',
            icon: 'zmdi zmdi-email',
            type: 'dropdown',
            priority: 2,
            permission: 'viewMail',
            children: []
        };

        angular.forEach(EMAIL_ROUTES, function (route) {
            emailMenu.children.push({
                name: route.name,
                state: route.state,
                type: 'link',
                badge: Math.round(Math.random() * (20 - 1) + 1)
            });
        });

        triMenuProvider.addMenu(emailMenu);

        triMenuProvider.addMenu({
            type: 'divider',
            priority: 2.3
        });
    }
})();
