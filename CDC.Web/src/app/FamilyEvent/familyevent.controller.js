(function () {
    'use strict';

    angular
        .module('app.familyevent')
        .controller('FamilyEventController', FamilyEventController);

    /* @ngInject */
    function FamilyEventController($scope, $http, $localStorage, HOST_URL,$window) {
        var vm = this;
        $scope.agencyId = localStorage.agencyId;
        localStorage.HostUrl=HOST_URL.url;
         $scope.IsLoaded = false;
        window.uploadDone = function () {
            $scope.IsLoaded = true;
        }
    }
})();