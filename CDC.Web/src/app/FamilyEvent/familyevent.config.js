(function() {
    'use strict';

    angular
        .module('app.familyevent')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
          .state('triangular.familyevent', {
                url: '/familyevent',
                templateUrl: 'app/FamilyEvent/familyevent.tmpl.html',
                // set the controller to load for this page
                controller: 'FamilyEventController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewFamilyEvent']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"FAMILY",$window);
                    }
                }
            })
        
        ;

        triMenuProvider.addMenu({
            name: 'Events',
            icon: 'fa fa-calendar',
            type: 'link',
            permission: 'viewFamilyEvent',
            state: 'triangular.familyevent',
            priority: 7

        });
    }
})();
