(function () {
    'use strict';

    angular
        .module('app.studentSchedule')
        .factory('StudentScheduleService', StudentScheduleService);

    StudentScheduleService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function StudentScheduleService($http, $q, HOST_URL) {
        return {
            GetStudentScheduleListService: GetStudentScheduleListService,
            DeleteStaffScheduleById: DeleteStaffScheduleById,
            SetId: SetId
        };

        function SetId(data) {
            this.Id = 0;
            this.StaffName = '';
            this.Id = data.ID;
            this.StaffName = data.FullName;
            this.StaffData = data;
        }

        function GetStudentScheduleListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StudentSchedule/GetStudentSchedules', model).success(function (response, status, headers) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: errResp });
            });
            return deferred.promise;
        }

        function DeleteStaffScheduleById(scheduleId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StudentSchedule/DeleteSchedule?scheduleId=' + scheduleId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: errResp });
            });
            return deferred.promise;
        };

    }
})();