(function () {
    'use strict';

    angular
        .module('app.studentSchedule')
        .controller('studentScheduleController', studentScheduleController);

    /* @ngInject */
    function studentScheduleController($scope, $http, $rootScope, $mdDialog, $mdToast, $filter, $localStorage,
        $element, triTheming, triLayout, uiCalendarConfig, HOST_URL, StudentScheduleService) {
        var vm = this;
        $scope.$storage = localStorage;
        vm.addSchedule = addSchedule;
        vm.AllClasses = [];
        vm.scheduleId = 0;
        vm.scheduleList = [];
        $scope.query = {
            studentId: $scope.$storage.studentIdforSchedule
        };
        // vm.event.end = new Date();
        // vm.event.start = new Date();
        vm.calendarOptions = {
            contentHeight: 'auto',
            selectable: true,
            editable: true,
            header: false,
            viewRender: function (view) {
                // change day
                vm.currentDay = view.calendar.getDate();
                vm.currentView = view.name;
                // update toolbar with new day for month name
                $rootScope.$broadcast('calendar-changeday', vm.currentDay);
                // update background image for month
                triLayout.layout.contentClass = 'calendar-background-image background-overlay-static overlay-gradient-10 calendar-background-month-' + vm.currentDay.month();
            },
            dayClick: function (date, jsEvent, view) { //eslint-disable-line
                vm.currentDay = date;
            },
            eventClick: function (calEvent, jsEvent, view) { //eslint-disable-line
                if (calEvent._end == null) {
                    calEvent._end = moment(calEvent.enddate);
                    calEvent.end = moment(calEvent.enddate);
                }
                $mdDialog.show({
                    controller: 'studentScheduleDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'app/studentSchedule/schedule/views/event-dialog.tmpl.html',
                    targetEvent: jsEvent,
                    focusOnOpen: false,
                    locals: {
                        dialogData: {
                            title: 'Edit Schedule',
                            confirmButtonText: 'Save',
                            classes: vm.AllClasses,
                        },
                        event: calEvent,
                        edit: true
                    }
                })
                    .then(function (event) {
                        if (angular.isDefined(event.deleteMe) && event.deleteMe === true) {
                            vm.promise = StudentScheduleService.DeleteStaffScheduleById(event.scheduleId);
                            vm.promise.then(function (response) {
                                if (response.ReturnStatus == true) {
                                    uiCalendarConfig.calendars['triangular-calendar'].fullCalendar('removeEvents', event._id);
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content($filter('triTranslate')('Schedule Deleted'))
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );
                                }
                                else {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content($filter('triTranslate')('Error while deleting schedule, please try again later.'))
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );
                                }
                            });

                        }
                        else {
                            var model = {
                                ID: event.scheduleId,
                                StudentId: $scope.$storage.studentIdforSchedule,
                                Title: event.title,
                                ClassId: event.class,
                                StartDate: event.start.toDate(),
                                EndDate: event.end.toDate(),
                                MondayIn: (moment(event.starttimeMonday).format('HH:mm:ss')),
                                MondayOut: (moment(event.endtimeMonday).format('HH:mm:ss')),
                                TuesdayIn: (moment(event.starttimeTuesday).format('HH:mm:ss')),
                                TuesdayOut: (moment(event.endtimeTuesday).format('HH:mm:ss')),
                                WednesdayIn: (moment(event.starttimeWednesday).format('HH:mm:ss')),
                                WednesdayOut: (moment(event.endtimeWednesday).format('HH:mm:ss')),
                                ThursdayIn: (moment(event.starttimeThursday).format('HH:mm:ss')),
                                ThursdayOut: (moment(event.endtimeThursday).format('HH:mm:ss')),
                                FridayIn: (moment(event.starttimeFriday).format('HH:mm:ss')),
                                FridayOut: (moment(event.endtimeFriday).format('HH:mm:ss')),
                            };
                            $http.post(HOST_URL.url + '/api/StudentSchedule/UpdateSchedule', model).then(function (response) {
                                if (response.data.IsSuccess == true) {
                                    //uiCalendarConfig.calendars['triangular-calendar'].fullCalendar('removeEvents', event._id);
                                    //vm.eventSources[0].events.push(event);
                                    uiCalendarConfig.calendars['triangular-calendar'].fullCalendar('updateEvent', event);
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content($filter('triTranslate')('Schedule Updated'))
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );
                                }
                                else {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content($filter('triTranslate')("Error in updating schedule, Please try again later."))
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );
                                }
                            });
                        }
                    });
            }
        };

        vm.viewFormats = {
            'month': 'MMMM YYYY',
            'agendaWeek': 'w',
            'agendaDay': 'Do MMMM YYYY'
        };

        vm.eventSources = [{
            events: []
        }];

        function addSchedule(event, $event) {
            debugger
            var inAnHour = moment(vm.currentDay);
            $mdDialog.show({
                controller: 'studentScheduleDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/studentSchedule/schedule/views/event-dialog.tmpl.html',
                targetEvent: $event,
                focusOnOpen: false,
                locals: {
                    dialogData: {
                        title: 'Add Student Schedule',
                        confirmButtonText: 'Add',
                        classes: vm.AllClasses,
                    },
                    event: {
                        scheduleId: 0,
                        title: $filter('triTranslate')('New Student Schedule'),
                        //allDay: false,
                        start: inAnHour,
                        end: inAnHour,
                        class: 0,
                        stick: true
                    },
                    edit: false
                }
            })
                .then(function (event) {
                    var model = {
                        Title: event.title,
                        StudentId: $scope.$storage.studentIdforSchedule,
                        ClassId: event.class,
                        StartDate: event.start.toDate(),
                        EndDate: event.end.toDate(),
                        MondayIn: (moment(event.starttimeMonday).format('HH:mm:ss')),
                        MondayOut: (moment(event.endtimeMonday).format('HH:mm:ss')),
                        TuesdayIn: (moment(event.starttimeTuesday).format('HH:mm:ss')),
                        TuesdayOut: (moment(event.endtimeTuesday).format('HH:mm:ss')),
                        WednesdayIn: (moment(event.starttimeWednesday).format('HH:mm:ss')),
                        WednesdayOut: (moment(event.endtimeWednesday).format('HH:mm:ss')),
                        ThursdayIn: (moment(event.starttimeThursday).format('HH:mm:ss')),
                        ThursdayOut: (moment(event.endtimeThursday).format('HH:mm:ss')),
                        FridayIn: (moment(event.starttimeFriday).format('HH:mm:ss')),
                        FridayOut: (moment(event.endtimeFriday).format('HH:mm:ss')),
                    };
                    $http.post(HOST_URL.url + '/api/StudentSchedule/AddSchedule', model).then(function (response) {
                        debugger
                        if (response.data.IsSuccess == true) {
                            event.scheduleId = response.data.Content.ID;
                            event.end = randomDate(event.start, event.end);
                            vm.eventSources[0].events.push(event);
                            $mdToast.show(
                                $mdToast.simple()
                                    .content($filter('triTranslate')('Schedule Created'))
                                    .position('bottom right')
                                    .hideDelay(2000)
                            );
                        }
                        else {
                                $mdToast.show(
                                    $mdToast.simple()
                                        .content($filter('triTranslate')('Error! Adding Schedule failed, Please try again later'))
                                        .position('bottom right')
                                        .hideDelay(2000)
                                );
                        }
                    });
                });
        }

        function createRandomEvents(number, startDate, endDate) {
            vm.promise = StudentScheduleService.GetStudentScheduleListService($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.scheduleList = response.Content;
                    vm.scheduleCount = response.TotalRows;
                    $scope.selected = [];
                    AddSchedulePosts(vm.scheduleList);
                }
                else {
                    vm.scheduleList = [];
                    vm.scheduleCount = 0;
                }
            });
        }
        function AddSchedulePosts(scheduleList) {
            for (var x = 0; x < scheduleList.length; x++) {
                var randomPalette = pickRandomProperty(triTheming.palettes);
                vm.eventSources[0].events.push({
                    title: scheduleList[x].Title,
                    allDay: false,
                    start: scheduleList[x].StartDate,
                    end: randomDate(scheduleList[x].StartDate, scheduleList[x].EndDate),
                    enddate: randomDate(scheduleList[x].StartDate, scheduleList[x].EndDate),
                    scheduleId: scheduleList[x].ID,
                    class: scheduleList[x].ClassId.toString(),
                    stick: true,
                    starttimeMonday: new Date(new Date().toDateString() + ' ' + scheduleList[x].MondayIn),
                    endtimeMonday: new Date(new Date().toDateString() + ' ' + scheduleList[x].MondayOut),
                    starttimeTuesday: new Date(new Date().toDateString() + ' ' + scheduleList[x].TuesdayIn),
                    endtimeTuesday: new Date(new Date().toDateString() + ' ' + scheduleList[x].TuesdayOut),
                    starttimeWednesday: new Date(new Date().toDateString() + ' ' + scheduleList[x].WednesdayIn),
                    endtimeWednesday: new Date(new Date().toDateString() + ' ' + scheduleList[x].WednesdayOut),
                    starttimeThursday: new Date(new Date().toDateString() + ' ' + scheduleList[x].ThursdayIn),
                    endtimeThursday: new Date(new Date().toDateString() + ' ' + scheduleList[x].ThursdayOut),
                    starttimeFriday: new Date(new Date().toDateString() + ' ' + scheduleList[x].FridayIn),
                    endtimeFriday: new Date(new Date().toDateString() + ' ' + scheduleList[x].FridayOut),
                    backgroundColor: triTheming.rgba(triTheming.palettes[randomPalette]['500'].value),
                    borderColor: triTheming.rgba(triTheming.palettes[randomPalette]['500'].value),
                    textColor: triTheming.rgba(triTheming.palettes[randomPalette]['500'].contrast),
                    palette: randomPalette
                });
            }
        }

        // listeners

        $scope.$on('addSchedule', addSchedule);

        // create 10 random events for the month
        createRandomEvents(100, moment().startOf('year'), moment().endOf('year'));

        // function randomDate(start, end) {
        //     var startNumber = start.toDate().getTime();
        //     var endNumber = end.toDate().getTime();
        //     var randomTime = Math.random() * (endNumber - startNumber) + startNumber;
        //     return moment(randomTime);
        // }

        function randomDate(start, end) {
            var startNumber = new Date(start).getTime();
            var endNumber = new Date(end).getTime();
            var randomTime = Math.random() * (endNumber - startNumber) + startNumber;
            return moment(randomTime);
        }

        function pickRandomProperty(obj) {
            var result;
            var count = 0;
            for (var prop in obj) {
                if (Math.random() < 1 / ++count) {
                    result = prop;
                }
            }
            return result;
        }

        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/GetClass_Room_Lesson',
                data: {
                    SearchVM: ""
                }
            };
            $http(req).then(function successCallback(response) {
                if (response.data.IsSuccess) {
                    vm.AllClasses = eval(response.data.Content.classList);
                }
                else {
                    if (response.data.length == 0) {
                    }
                    else {

                    }
                }
            }, function errorCallback(response) {
            });
        }

        function convertMomentToTime(moment) {
            return moment.hour() + ':' + moment.minute() + ':00';
        }
    }
})();
