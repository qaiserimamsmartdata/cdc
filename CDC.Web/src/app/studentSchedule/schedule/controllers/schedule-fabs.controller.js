(function() {
    'use strict';

    angular
        .module('app.studentSchedule')
        .controller('studentScheduleFabController', studentScheduleFabController);

    /* @ngInject */
    function studentScheduleFabController($rootScope) {
        var vm = this;
        vm.addSchedule = addSchedule;

        ////////////////

        function addSchedule($event) {
            $rootScope.$broadcast('addSchedule', $event);
        }
    }
})();