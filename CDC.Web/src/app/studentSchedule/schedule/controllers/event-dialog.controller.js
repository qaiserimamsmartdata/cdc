(function () {
    'use strict';

    angular
        .module('app.studentSchedule')
        .controller('studentScheduleDialogController', studentScheduleDialogController);

    /* @ngInject */
    function studentScheduleDialogController($scope, $mdDialog, $filter, triTheming, dialogData, event, edit) {
        var vm = this;
        vm.cancelClick = cancelClick;
        vm.colors = [];
        vm.colorChanged = colorChanged;
        vm.deleteClick = deleteClick;
        vm.allDayChanged = allDayChanged;
        vm.dialogData = dialogData;
        vm.edit = edit;
        vm.event = event;
        vm.okClick = okClick;
        vm.selectedColor = null;
        // create start and end date of event
        //vm.event.start = event.start.toDate();
        //vm.startTime = null;//convertMomentToTime(event.start);
        //vm.scheduleEvent.class = vm.event.class;
        vm.scheduleId = vm.event.scheduleId;
        vm.start = event.start.toDate();
        vm.enddate = event.end.toDate();
        if (vm.event.scheduleId == 0) {
            vm.scheduleEvent = {
                title: event.title,
                starttimeMonday: new Date(new Date().toDateString() + ' ' + '09:00:00'),
                endtimeMonday: new Date(new Date().toDateString() + ' ' + '12:00:00'),
                starttimeTuesday: new Date(new Date().toDateString() + ' ' + '09:00:00'),
                endtimeTuesday: new Date(new Date().toDateString() + ' ' + '12:00:00'),
                starttimeWednesday: new Date(new Date().toDateString() + ' ' + '09:00:00'),
                endtimeWednesday: new Date(new Date().toDateString() + ' ' + '12:00:00'),
                starttimeThursday: new Date(new Date().toDateString() + ' ' + '09:00:00'),
                endtimeThursday: new Date(new Date().toDateString() + ' ' + '12:00:00'),
                starttimeFriday: new Date(new Date().toDateString() + ' ' + '09:00:00'),
                endtimeFriday: new Date(new Date().toDateString() + ' ' + '12:00:00'),
                class: event.class
            }
        }
        else {
            vm.scheduleEvent = {
                title: event.title,
                class: event.class,
                starttimeMonday: event.starttimeMonday,
                endtimeMonday: event.endtimeMonday,
                starttimeTuesday: event.starttimeTuesday,
                endtimeTuesday: event.endtimeTuesday,
                starttimeWednesday: event.starttimeWednesday,
                endtimeWednesday: event.endtimeWednesday,
                starttimeThursday: event.starttimeThursday,
                endtimeThursday: event.endtimeThursday,
                starttimeFriday: event.starttimeFriday,
                endtimeFriday: event.endtimeFriday,
            }
        }
        //vm.start = event.start.toDate();
        //vm.enddate = event.end.toDate();
        ////////////////

        function colorChanged() {
            vm.event.backgroundColor = vm.selectedColor.backgroundColor;
            vm.event.borderColor = vm.selectedColor.backgroundColor;
            vm.event.textColor = vm.selectedColor.textColor;
            vm.event.palette = vm.selectedColor.palette;
        }

        function okClick() {
            vm.event.title = vm.scheduleEvent.title;
            vm.event.class = vm.scheduleEvent.class;
            vm.event.starttimeMonday = vm.scheduleEvent.starttimeMonday;
            vm.event.endtimeMonday = vm.scheduleEvent.endtimeMonday;
            vm.event.starttimeTuesday = vm.scheduleEvent.starttimeTuesday;
            vm.event.endtimeTuesday = vm.scheduleEvent.endtimeTuesday;
            vm.event.starttimeWednesday = vm.scheduleEvent.starttimeWednesday;
            vm.event.endtimeWednesday = vm.scheduleEvent.endtimeWednesday;
            vm.event.starttimeThursday = vm.scheduleEvent.starttimeThursday;
            vm.event.endtimeThursday = vm.scheduleEvent.endtimeThursday;
            vm.event.starttimeFriday = vm.scheduleEvent.starttimeFriday;
            vm.event.endtimeFriday = vm.scheduleEvent.endtimeFriday;
            vm.event.start = updateEventDateTime(vm.start);
            vm.event.end = updateEventDateTime(vm.enddate);
            vm.event.enddate = updateEventDateTime(vm.enddate);
            vm.event.scheduleId = vm.scheduleId;
            $mdDialog.hide(vm.event);
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function deleteClick() {
            vm.event.deleteMe = true;
            $mdDialog.hide(vm.event);
        }

        function allDayChanged() {
            // if all day turned on and event already saved we need to create a new date
            if (vm.event.allDay === false && vm.event.end === null) {
                vm.event.end = moment(vm.event.start);
                vm.event.end.endOf('day');
                vm.end = vm.event.end.toDate();
                vm.endTime = convertMomentToTime(vm.event.end);
            }
        }

        function convertMomentToTime(moment) {
            return {
                hour: moment.hour(),
                minute: moment.minute()
            };
        }

        function updateEventDateTime(date) {
            var newDate = moment(date);
            return newDate;
        }

        function createDateSelectOptions() {
            // create options for time select boxes (this will be removed in favor of mdDatetime picker when it becomes available)
            vm.dateSelectOptions = {
                hours: [],
                minutes: []
            };
            // hours
            for (var hour = 0; hour <= 23; hour++) {
                vm.dateSelectOptions.hours.push(hour);
            }
            // minutes
            for (var minute = 0; minute <= 59; minute++) {
                vm.dateSelectOptions.minutes.push(minute);
            }
        }

        // init
        createDateSelectOptions();

        // create colors
        angular.forEach(triTheming.palettes, function (palette, index) {
            var color = {
                name: index.replace(/-/g, ' '),
                palette: index,
                backgroundColor: triTheming.rgba(palette['500'].value),
                textColor: triTheming.rgba(palette['500'].contrast)
            };

            vm.colors.push(color);

            if (index === vm.event.palette) {
                vm.selectedColor = color;
                vm.colorChanged();
            }
        });
    }
})();
