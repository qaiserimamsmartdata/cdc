(function () {
    'use strict';

    angular
        .module('app.studentSchedule')
        .controller('ScheduleListController', ScheduleListController)
        .controller('EditScheduleController', EditScheduleController)
        .controller('AddScheduleController', AddScheduleController);

    /* @ngInject */
    function ScheduleListController($scope, $mdDialog, $state, FamilyService, AgencyService, $localStorage, notificationService, CommonService, StudentScheduleService) {
        var vm = this;
        vm.showAddScheduleDialogue = showAddScheduleDialogue;
        vm.showEditScheduleDialogue = showEditScheduleDialogue;
        vm.showDeleteScheduleConfirmDialoue = showDeleteScheduleConfirmDialoue;
        $scope.$storage = localStorage;
        vm.goToSchedule = goToSchedule;
        $scope.query = {
            studentId: $scope.$storage.studentIdforSchedule
        };
        vm.GetScheduleListByScheduleId = GetScheduleListByScheduleId;
        if (JSON.parse($scope.$storage.familyDetails).ID == undefined)
            $state.go('triangular.families');
        vm.columns = {
            ClassName: 'Class',
            StartDate: 'StartDate',
            Active: 'Active',
            ID: 'ID'
        };
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FamilyId: JSON.parse($scope.$storage.familyDetails).ID,
            name: '',
            StudentName: ''
        };
        GetScheduleListByScheduleId();

        // function addSchedule(event, $event) {
        //     // var inAnHour = moment(vm.currentDay);
        //     $mdDialog.show({
        //         controller: 'studentScheduleDialogController',
        //         controllerAs: 'vm',
        //         templateUrl: 'app/studentSchedule/schedule/views/addschedule.tmpl.html',
        //         targetEvent: $event,
        //         focusOnOpen: false,
        //         locals: {
        //             dialogData: {
        //                 title: 'Add Student Schedule',
        //                 confirmButtonText: 'Add',
        //                 classes: vm.AllClasses,
        //             },
        //             event: {
        //                 scheduleId: 0,
        //                 title: $filter('triTranslate')('New Student Schedule'),
        //                 //allDay: false,
        //                 // start: inAnHour,
        //                 // end: inAnHour,
        //                 class: 0,
        //                 stick: true
        //             },
        //             edit: false
        //         }
        //     })
        //         .then(function (event) {


        //             $http.post(HOST_URL.url + '/api/StudentSchedule/AddSchedule', model).then(function (response) {
        //                 if (response.data.IsSuccess == true) {
        //                     event.scheduleId = response.data.Content.ID;
        //                     event.end = randomDate(event.start, event.end);
        //                     vm.eventSources[0].events.push(event);
        //                     $mdToast.show(
        //                         $mdToast.simple()
        //                             .content($filter('triTranslate')('Schedule Created'))
        //                             .position('bottom right')
        //                             .hideDelay(2000)
        //                     );
        //                 }
        //                 else {
        //                     $mdToast.show(
        //                         $mdToast.simple()
        //                             .content($filter('triTranslate')('Error! Adding Schedule failed, Please try again later'))
        //                             .position('bottom right')
        //                             .hideDelay(2000)
        //                     );
        //                 }
        //             });
        //         });
        // }

        function GetScheduleListByScheduleId() {
            vm.promise = StudentScheduleService.GetStudentScheduleListService($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.scheduleList = response.Content;
                    vm.scheduleCount = response.TotalRows;
                    $scope.selected = [];
                    // AddSchedulePosts(vm.scheduleList);
                }
                else {
                    vm.scheduleList = [];
                    vm.scheduleCount = 0;
                }
            });
        }

        function goToSchedule(id) {
            $scope.$storage.studentIdforSchedule = id;
            $state.go('triangular.studentschedule');
        }

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function showAddScheduleDialogue() {
            $mdDialog.show({
                controller: AddScheduleController,
                controllerAs: 'vm',
                templateUrl: 'app/studentSchedule/schedule/views/addschedule.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function showEditScheduleDialogue(Schedule) {
            $mdDialog.show({
                controller: EditScheduleController,
                controllerAs: 'vm',
                templateUrl: 'app/familiesDetails/views/addSchedule.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen,
                items: { studentdetail: Schedule },
            });
        }
        function showDeleteScheduleConfirmDialoue(event, Id) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this Schedule?')
                .textContent('All of this Schedule data will be deleted.')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                DeleteFamilyScheduleById(Id);
            }, function () {
                $scope.hide();
            });
        }
        function DeleteScheduleById(Id) {
            vm.showProgressbar = true;
            vm.promise = FamilyService.DeleteFamilyScheduleById(Id);
            vm.promise.then(function (response) {
                if (response.StudentInfo.IsSuccess == true) {
                    notificationService.displaymessage('Schedule deleted Successfully.');
                    vm.showProgressbar = false;
                    GetStudentListByFamily();
                }
                else {
                    notificationService.displaymessage('Error occured while deleting, Please try again later.');
                }
            });
        }
    }
    function EditScheduleController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService,
        $timeout, $mdToast, $rootScope, $q, HOST_URL, items, FamilyDetailsService, CommonService) {
        var vm = this;
        vm.showProgressbar = false;
        $scope.id = items.studentdetail.ID;
        $scope.DisplayText = "Update Schedule";
        vm.Schedule = {};
        vm.Schedule.Student = {};
        vm.Schedule = items.studentdetail;
        vm.Schedule.GradeLevelId = items.studentdetail.GradeLevelId.toString();
        vm.Schedule.Student.DateOfBirth = new Date(vm.Schedule.Student.DateOfBirth);
        vm.Schedule.Student.Gender = vm.Schedule.Student.Gender.toString();
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);
        vm.updateSchedule = updateSchedule;
        vm.cancelClick = cancelClick;
        getGradeLevel();
        function getGradeLevel() {
            vm.promise = CommonService.getGradeLevel($scope.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllGradeLevel = null;
                    vm.AllGradeLevel = eval(response.Content);
                }
            });

        };
        function updateSchedule(studentForm, model) {
            if (studentForm.$valid) {
                vm.showProgressbar = true;
                $http.post(HOST_URL.url + '/api/Family/saveStudentInfo', model).then(function (response) {
                    if (response.data.ContactInfo.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(response.data.ContactInfo.Message);
                        cancelClick();
                        $state.go('triangular.Familydetails', {}, { reload: true });
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };


    }
    function AddScheduleController($scope, $http, $state, $mdDialog, $mdEditDialog, $localStorage, notificationService, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService) {
        var vm = this;
        vm.showProgressbar = false;
        vm.cancelClick = cancelClick;
        vm.addSchedule = addSchedule;
        $scope.$storage = localStorage;

        $scope.id = 0;
        $scope.DisplayText = "Add Schedule";
        vm.Schedule = {};
        vm.Schedule.Student = {};
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);

        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/GetClass_Room_Lesson',
                data: {
                    SearchVM: ""
                }
            };
            $http(req).then(function successCallback(response) {
                if (response.data.IsSuccess) {
                    vm.AllClasses = eval(response.data.Content.classList);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        //NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
                    }
                }
            }, function errorCallback(response) {
                //NotificationMessageController('Unable to retreive country list at the moment.');
            });
        }

        function addSchedule(scheduleform, model) {
            if (scheduleform.$valid) {
                vm.showProgressbar = true;
                // vm.Schedule.Student.FamilyId = localStorage.familyDetails.ID;
                model.StudentId = $scope.$storage.studentIdforSchedule;
                $http.post(HOST_URL.url + '/api/StudentSchedule/AddSchedule', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        notificationService.displaymessage(NotificationMessageController(response.data.ReturnMessage[0]));
                        cancelClick();
                        $state.go('triangular.studentschedule', {}, { reload: true });
                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };

    }
})();