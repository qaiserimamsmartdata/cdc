(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('UpdateStaffController', UpdateStaffController);


    /* @ngInject */
    function UpdateStaffController($filter, $scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, StaffService, CommonService, AgencyService, $localStorage) {
        var vm = this;
        vm.Title = "Update Staff Information";
        $scope.$storage = localStorage;
        vm.query = {
            AgencyId: localStorage.agencyId
        };
        var StaffId = StaffService.Id;
        if ($scope.$storage.staffId == null || $scope.$storage.staffId == undefined)
            $state.go('triangular.staff');
        else
            $scope.id = $scope.$storage.staffId;
        vm.showProgressbar = false;
        vm.disabled = true;
        vm.GetAllCountry = GetAllCountry;
        vm.GetPositions = GetPositions;
        vm.GetState = GetState;
        vm.GetCity = GetCity;
        GetPositions();
        GetAllCountry();
        GetStaffList();

        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            // $scope.CountryTextToShow = "Please Wait..."; // this will show until load states from database
            var myPromise = CommonService.getCountries();

            myPromise.then(function (resolve) {
                vm.AllCountries = resolve;
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        function GetCity(stateId) {
            //Load State
            vm.Cities = null;
            var myPromise = CommonService.getCities(stateId);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.Cities = resolve;
                $scope.CityTextToShow = "--Select Cities--";
            }, function (reject) {

            });

        };
        function GetPositions() {
            vm.promise = CommonService.getPositions();
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.AllPosition = null;
                    vm.AllPosition = eval(response);
                }
            });
        };
        function GetStaffList() {
            vm.promise = StaffService.GetStaffListByIdService($scope.$storage.staffId);
            vm.promise.then(function (response) {
                if (response.staffList.ID > 0) {
                    vm.staff = response.staffList;
                    vm.staff.CountryId = vm.staff.CountryId.toString();
                    vm.staff.StateId = vm.staff.StateId.toString();
                    vm.staff.CityId = vm.staff.CityId.toString();
                    vm.staff.DateOfBirth = new Date(vm.staff.DateOfBirth);
                    vm.staff.DateOfJoining = new Date(vm.staff.DateOfJoining);
                    vm.staff.PhoneNumber = parseFloat(vm.staff.PhoneNumber);

                    GetState(vm.staff.CountryId);
                    GetCity(vm.staff.StateId);
                    AddStaff.$invalid = true;
                    $scope.selected = [];
                }
                else {
                    if (response.staffList.ID == 0) {
                        vm.staff = [];
                        vm.staffCount = 0;
                        NotificationMessageController('Invalid found.');
                    }
                    else {
                        NotificationMessageController('Unable to get staff list at the moment. Please try again after some time.');
                    }
                }
            });

        };


        vm.updateStaff = updateStaff;
        function updateStaff(AddStaff) {
            if (AddStaff.$valid) {
                var model = {};
                model = vm.staff;

                $http.post(HOST_URL.url + '/api/Staff/UpdateStaff', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Staff updated Successfully.');
                        $state.go('triangular.staff');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $state.go('triangular.classlist');
                    }
                });
            }
        }

        vm.cancelUpdate = cancelUpdate;
        function cancelUpdate() {
            $state.go('triangular.classlist');
        };

        function getCategory() {
            vm.promise = CommonService.getCategory(vm.query);
            vm.promise.then(function (response) {
                if (response.category.length > 0) {
                    vm.AllCategories = null;
                    vm.AllCategories = eval(response.category);
                }
            });

        };

        function getRooms() {
            vm.promise = CommonService.getRooms();
            vm.promise.then(function (response) {
                if (response.room.length > 0) {
                    vm.AllRooms = null;
                    vm.AllRooms = eval(response.room);
                }
            });

        };
        function getSessions() {
            vm.promise = CommonService.getSessions();
            vm.promise.then(function (response) {
                if (response.session.length > 0) {
                    vm.AllSessions = null;
                    vm.AllSessions = eval(response.session);
                }
            });

        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

})();