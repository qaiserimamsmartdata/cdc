(function () {
    'use strict';

    angular
        .module('app.studentSchedule')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            // .state('triangular.studentschedule', {
            //     // set the url of this page
            //     url: '/studentschedule',
            //     views: {
            //         '@triangular': {
            //             // set the html template to show on this page
            //             templateUrl: 'app/studentSchedule/schedule/views/schedule.tmpl.html',
            //             // set the controller to load for this page
            //             controller: 'studentScheduleController',
            //             controllerAs: 'vm'
            //         },
            //         'toolbar@triangular': {
            //             templateUrl: 'app/studentSchedule/schedule/layouts/toolbar/toolbar.tmpl.html',
            //             controller: 'studentScheduleToolbarController',
            //             controllerAs: 'vm'
            //         },
            //         'belowContent@triangular': {
            //             templateUrl: 'app/studentSchedule/schedule/views/schedule-fabs.tmpl.html',
            //             controller: 'studentScheduleFabController',
            //             controllerAs: 'vm'
            //         }
            //     },
            //     data: {
            //         layout: {
            //             contentClass: 'triangular-non-scrolling layout-column',
            //             footer: false
            //         },
            //         permissions: {
            //             only: ['viewCalendar']
            //         }
            //     }
            // })
             .state('triangular.studentschedule', {
                url: '/studentschedule',
                templateUrl: 'app/studentSchedule/schedule/views/scheduleList.tmpl.html',
                // set the controller to load for this page
                controller: 'ScheduleListController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                }
            });
        // triMenuProvider.addMenu({
        //     name: 'Student Schedule',
        //     icon: 'fa fa-tree',
        //     state: 'triangular.studentschedule',
        //     type: 'link',
        //     priority: 1.1
        // });
        // triMenuProvider.addMenu({
        //     // give the menu a name to show (should be translatable and in the il8n folder json)
        //     name: 'Calendar',
        //     // point this menu to the state we created in the $stateProvider above
        //     state: 'triangular.calendar',
        //     // set the menu type to a link
        //     type: 'link',
        //     // set an icon for this menu
        //     icon: 'zmdi zmdi-calendar-alt',
        //     // set a proirity for this menu item, menu is sorted by priority
        //     priority: 2.3,
        //     permission: 'viewCalendar'
        // });
    }
})();
