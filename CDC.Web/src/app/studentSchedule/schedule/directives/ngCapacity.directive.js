angular
    .module('app')
    .directive('ngCapacity', ['$http', 'HOST_URL','$localStorage', function (async, HOST_URL,$localStorage) {
      
        return {
           // require: 'ngModel',
            
            link: function (scope, elem, attrs, ctrl) {
                elem.on('blur', function (evt) {
                    if (attrs.ngUnique == 'capacity') {
                        scope.$apply(function () {
                            var val = elem.val();
                            
                            var req = { "ID": val, "agencyId": localStorage.agencyId} 
                            var ajaxConfiguration = { method: 'POST', url: HOST_URL.url + 'api/User/EnrolledCapacity', data: req };
                            async(ajaxConfiguration)
                                .success(function (data, status, headers, config) {
                                    ctrl.$setValidity('capacity', !data);
                                });
                        });
                    }                   
                });
            }
        }
    }]);