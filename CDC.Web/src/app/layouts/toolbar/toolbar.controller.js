(function () {
  'use strict';

  angular
    .module('triangular.components')
    .controller('ToolbarController', DefaultToolbarController);

  /* @ngInject */
  function DefaultToolbarController($scope, $injector, notificationService, AttendanceService, apiService, CommonService, HOST_URL, $rootScope, $mdMedia, $state, $element, $filter, $mdUtil, $mdSidenav, $mdToast, $timeout, $document, triBreadcrumbsService, triSettings, triLayout, AgencyService, $localStorage) {
    var vm = this;
    vm.breadcrumbs = triBreadcrumbsService.breadcrumbs;
    vm.emailNew = false;
    vm.hideMenu = hideMenu;
    vm.languages = triSettings.languages;
    vm.openSideNav = openSideNav;
    vm.hideMenuButton = hideMenuButton;
    vm.CurrentDate = new Date();
    vm.ImageUrlPath = HOST_URL.url;
    vm.switchLanguage = switchLanguage;
    vm.toggleNotificationsTab = toggleNotificationsTab;
    vm.isFullScreen = false;
    vm.fullScreenIcon = 'zmdi zmdi-fullscreen';
    vm.toggleFullScreen = toggleFullScreen;
    vm.menuswitchText = 'Hide Menu';
    vm.oldSwitchMenuStyle = 'full';
    vm.Logout = Logout;
    vm.UserSetting = UserSetting;
    vm.TimeZone = localStorage.TimeZone;
    vm.getAttendanceDataSuccess = getAttendanceDataSuccess;
    vm.getAttendanceDataSuccessPick=getAttendanceDataSuccessPick;
    vm.defaultUser = {};
    vm.Failed = Failed;

    var accessToken = "b5864e4127224f4bb36d18ceb27f1b34",
      baseUrl = "https://api.api.ai/v1/",
      $speechInput,
      $recBtn,
      $answer,
      $speech,
      recognition,
      messageRecording = "Recording...",
      messageCouldntHear = "I couldn't hear you, could you say that again?",
      messageInternalError = "Oh no, there has been an internal server error",
      messageSorry = "I'm sorry, I don't have the answer to that yet.";

    $(document).ready(function () {
      $speechInput = $("#speech");
      $recBtn = $("#rec");

      $speechInput.keypress(function (event) {
        if (event.which == 13) {
          event.preventDefault();
          send();
        }
      });
      $recBtn.on("click", function (event) {
        $('#mainBox').show();
        switchRecognition();
      });
      $(".debug__btn").on("click", function () {
        $(this).next().toggleClass("is-active");
        return false;
      });
    });
    function Failed(result) {
      vm.showProgressbar = false;
      notificationService.displaymessage('Please try again after some time.');
    }
    function startRecognition() {
      recognition = new webkitSpeechRecognition();
      recognition.continuous = false;
      recognition.interimResults = false;

      recognition.onstart = function (event) {
        respond(messageRecording);

        updateRec();

      };
      recognition.onresult = function (event) {
        recognition.onend = null;

        var text = "";
        for (var i = event.resultIndex; i < event.results.length; ++i) {
          text += event.results[i][0].transcript;
        }
        setInput(text);
        stopRecognition();
      };
      recognition.onend = function () {
        respond(messageCouldntHear);
        stopRecognition();
      };
      recognition.lang = "en-US";
      recognition.start();
    }

    function stopRecognition() {
      if (recognition) {
        recognition.stop();
        recognition = null;
      }
      updateRec();

    }

    function switchRecognition() {
      if (recognition) {
        stopRecognition();
      } else {
        startRecognition();
      }
    }

    function setInput(text) {

      $speechInput.val(text);
      if (text == "sign in") {
        //recognition= null;
        respond("Please tell the name of the participant to be signed in ");
        $('#mainBox').hide();
        $('#speech').val(" ");
        $('#mainBox').show();
        switchRecognition2();
        function switchRecognition2() {
         
            startRecognition2();
          
        }
        function startRecognition2() {
          recognition = new webkitSpeechRecognition();
          recognition.continuous = false;
          recognition.interimResults = false;

          recognition.onstart = function (event) {
            respond(messageRecording);

            updateRec();

          };
          recognition.onresult = function (event) {
            var text = "";
            for (var i = event.resultIndex; i < event.results.length; ++i) {
              text += event.results[i][0].transcript;
            }
            setInput2(text);
            stopRecognition();
          };  
          recognition.onend = function () {
            //respond(messageCouldntHear);
            stopRecognition();
          };
          recognition.lang = "en-US";
          recognition.start();
        }
        function setInput2(text) {
          $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            Name: text,
            ClassId: 0,
            Date: vm.CurrentDate,
            AgencyId: localStorage.agencyId,
            TimeZoneName: localStorage.TimeZone
          };
          $scope.ParticipantName = $scope.query.Name == "" ? "All" : $scope.query.Name;

          var data = null;
          data = $filter('filter')(vm.AllClasses, parseInt($scope.query.ClassId), true);
          $scope.ClassName = data == null || undefined ? "All" : data.length > 0 ? data[0].ClassName : "All";

          vm.DisplayDate = $scope.query.Date;
          localStorage.ClassId = $scope.query.ClassId;
          apiService.post('/api/StudentAttendance/GetAllStudentAttendanceTest', $scope.query,
            getAttendanceDataSuccess,
            Failed);

          return;
        }
      }
      else if (text == "sign out") {
        respond("Please tell the name of the participant to be signed out");
         $('#mainBox').hide();
        $('#speech').val(" ");
        $('#mainBox').show();
        switchRecognition3();
        function switchRecognition3() {
         
            startRecognition3();
          
        }
        function startRecognition3() {
          recognition = new webkitSpeechRecognition();
          recognition.continuous = false;
          recognition.interimResults = false;

          recognition.onstart = function (event) {
            respond(messageRecording);

            updateRec();

          };
          recognition.onresult = function (event) {
            var text = "";
            for (var i = event.resultIndex; i < event.results.length; ++i) {
              text += event.results[i][0].transcript;
            }
            setInput3(text);
            stopRecognition();
          };  
          recognition.onend = function () {
            //respond(messageCouldntHear);
            stopRecognition();
          };
          recognition.lang = "en-US";
          recognition.start();
        }
              function setInput3(text) {
          $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            Name: text,
            ClassId: 0,
            Date: vm.CurrentDate,
            AgencyId: localStorage.agencyId,
            TimeZoneName: localStorage.TimeZone
          };
          $scope.ParticipantName = $scope.query.Name == "" ? "All" : $scope.query.Name;

          var data = null;
          data = $filter('filter')(vm.AllClasses, parseInt($scope.query.ClassId), true);
          $scope.ClassName = data == null || undefined ? "All" : data.length > 0 ? data[0].ClassName : "All";

          vm.DisplayDate = $scope.query.Date;
          localStorage.ClassId = $scope.query.ClassId;
          apiService.post('/api/StudentAttendance/GetAllStudentAttendanceTest', $scope.query,
            getAttendanceDataSuccessPick,
            Failed);

          return;
        }
       
      }

      else{
        send();
      }
     

    }
    GetClassMaster();
    function GetClassMaster() {
      var model = {
        AgencyId: localStorage.agencyId
      }
      vm.promise = CommonService.getClassCombo(model);
      vm.promise.then(function (response) {
        debugger
        if (response.Content.classList.length > 0) {
          vm.AllClasses = null;
          vm.AllClasses = eval(response.Content.classList);
        }
      });
    }
    function getAttendanceDataSuccess(result) {

      vm.EnrolledStudentList = result.data.Content;
      if (vm.EnrolledStudentList[0] == null || vm.EnrolledStudentList[0] == undefined) {
        respond("Sorry, there is no such participant  .");
        //$sessionStorage.ParticipantName=false;
        $state.go('triangular.attendance', {}, { reload: true });
        return;
      }
      vm.EnrolledStudentListCount = result.data.TotalRows;
      vm.EnrolledStudentList.forEach(function (element) {
        var model = {
          ID: element.StudentId
        }
        vm.promise = CommonService.getParentListByStudentId(model);
        vm.promise.then(function (response) {
          if (response.Content.length > 0) {
            vm.ParentList = null;
            vm.ParentList = response.Content;
            if (element.OnLeave == null)
              element.OnLeave = false;
            element.ParentList = vm.ParentList;
            if (element.ImagePath == null)
            element.ImagePath = "assets/images/avatars/avatar-5.png";
            respond("By whom you want the participant to be dropped ?");
            $('#mainBox').hide();
            $('#speech').val(" ");
            $('#mainBox').show();
            switchRecognition1();
            function switchRecognition1() {
              if (recognition) {
                stopRecognition();
              } else {
                startRecognition1();
              }
            }
            function startRecognition1() {
              recognition = new webkitSpeechRecognition();
              recognition.continuous = false;
              recognition.interimResults = false;

              recognition.onstart = function (event) {
                respond(messageRecording);

                updateRec();

              };
              recognition.onresult = function (event) {
                recognition.onend = null;

                var text = "";
                for (var i = event.resultIndex; i < event.results.length; ++i) {
                  text += event.results[i][0].transcript;
                }
                setInput1(text);
                stopRecognition();
              };
              recognition.onend = function () {
                respond(messageCouldntHear);
                stopRecognition();
              };
              recognition.lang = "en-US";
              recognition.start();
            }
            function setInput1(text) {
              console.log(text);
              $speechInput.val(text);
              var Result;
              console.log(vm.ParentList);
              angular.forEach(vm.ParentList, function (value, key) {

                if (value.FirstName == text) {
                  Result = value;

                }


              });
              if (Result != undefined) {
                if (Result.ID > 0) {
                  var model = {};
                  model.TimeZone = localStorage.TimeZone;
                  model.AttendanceDate = $scope.query.Date;
                  model.ClassInfo = vm.EnrolledStudentList[0].ClassInfo;
                  model.StudentId = vm.EnrolledStudentList[0].StudentId;
                  model.StudentScheduleId = vm.EnrolledStudentList[0].StudentScheduleId;
                  model.AgencyId = localStorage.agencyId;
                  //--for Primary Parent to send mail
                  var PrimaryGuardianData = $filter('filter')(vm.ParentList, { IsPrimary: true });
                  var PrimaryParentID = PrimaryGuardianData[0].ID;
                  var PrimaryGuradianEmail = PrimaryGuardianData[0].EmailId;
                  var PrimaryGuradianSMSCarrier = PrimaryGuardianData[0].SMSCarrier.CarrierAddress;
                  var PrimaryGuradianPhone = PrimaryGuardianData[0].Mobile;
                  var PrimaryParentName = PrimaryGuardianData[0].FullName;
                  var PrimaryDropByRelationName = PrimaryGuardianData[0].RelationName;
                  var PrimaryPickedByRelationName = PrimaryGuardianData[0].RelationName;
                  model.GuradianEmail = Result.EmailId;
                  model.GuradianSMSCarrier = Result.SMSCarrier.CarrierAddress;
                  model.GuradianPhone = Result.Mobile;
                  model.ParentName = Result.FullName;
                  model.DropByRelationName = Result.RelationName;
                  model.DropedById = Result.ID;
                  model.OnLeave = false;

                  //for Primary

                  if (PrimaryParentID != Result.ID) {
                    model.PrimaryGuradianEmail = PrimaryGuradianEmail;
                    model.PrimaryGuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                    model.PrimaryGuradianPhone = PrimaryGuradianPhone;
                    model.PrimaryParentName = PrimaryParentName;
                    model.PrimaryDropByRelationName = PrimaryDropByRelationName;
                  }
                  vm.promise = AttendanceService.AddStudentAttendance(model);

                }
              }
              else {
                respond("Sorry, attendance can't be marked.");

                return;
              }
              vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                  // vm.showProgressbar = false;
                  notificationService.displaymessage(response.Message);
                  respond("Attendance has been marked successfully.")
                  $state.go('triangular.attendance', {}, { reload: true });
                  // cancelClick();
                  //vm.getAttendanceData();
                  // $state.go('triangular.attendance', {}, { reload: true });
                }
                else {
                  vm.showProgressbar = false;
                  getAttendanceData();
                  notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
              });

            }
          }
        });

      }, this);
      vm.showProgressbar = false;
    };
    function getAttendanceDataSuccessPick(result) {

      vm.EnrolledStudentList = result.data.Content;
      if (vm.EnrolledStudentList[0] == null || vm.EnrolledStudentList[0] == undefined) {
        respond("Sorry, there is no such participant  .");
        //$sessionStorage.ParticipantName=false;
        $state.go('triangular.attendance', {}, { reload: true });
        return;
      }
      vm.EnrolledStudentListCount = result.data.TotalRows;
      vm.EnrolledStudentList.forEach(function (element) {
        var model = {
          ID: element.StudentId
        }
        vm.promise = CommonService.getParentListByStudentId(model);
        vm.promise.then(function (response) {
          if (response.Content.length > 0) {
            vm.ParentList = null;
            vm.ParentList = response.Content;
            if (element.OnLeave == null)
              element.OnLeave = false;
            element.ParentList = vm.ParentList;
            if (element.ImagePath == null)
            element.ImagePath = "assets/images/avatars/avatar-5.png";
            respond("By whom you want the participant to be picked ?");
            $('#mainBox').hide();
            $('#speech').val(" ");
            $('#mainBox').show();
            switchRecognition4();
            function switchRecognition4() {
              if (recognition) {
                stopRecognition();
              } else {
                startRecognition4();
              }
            }
            function startRecognition4() {
              recognition = new webkitSpeechRecognition();
              recognition.continuous = false;
              recognition.interimResults = false;

              recognition.onstart = function (event) {
                respond(messageRecording);

                updateRec();

              };
              recognition.onresult = function (event) {
                recognition.onend = null;

                var text = "";
                for (var i = event.resultIndex; i < event.results.length; ++i) {
                  text += event.results[i][0].transcript;
                }
                setInput4(text);
                stopRecognition();
              };
              recognition.onend = function () {
                respond(messageCouldntHear);
                stopRecognition();
              };
              recognition.lang = "en-US";
              recognition.start();
            }
            function setInput4(text) {
              console.log(text);
              $speechInput.val(text);
              var Result;
              console.log(vm.ParentList);
              angular.forEach(vm.ParentList, function (value, key) {

                if (value.FirstName == text) {
                  Result = value;

                }


              });
              if (Result != undefined) {
                if (Result.ID > 0) {
                  var model = {};
                  model.TimeZone = localStorage.TimeZone;
                  model.AttendanceDate = $scope.query.Date;
                  model.ClassInfo = vm.EnrolledStudentList[0].ClassInfo;
                  model.StudentId = vm.EnrolledStudentList[0].StudentId;
                  model.ID=vm.EnrolledStudentList[0].ID;
                  model.StudentScheduleId = vm.EnrolledStudentList[0].StudentScheduleId;
                  model.AgencyId = localStorage.agencyId;
                  //--for Primary Parent to send mail
                  var PrimaryGuardianData = $filter('filter')(vm.ParentList, { IsPrimary: true });
                  var PrimaryParentID = PrimaryGuardianData[0].ID;
                  var PrimaryGuradianEmail = PrimaryGuardianData[0].EmailId;
                  var PrimaryGuradianSMSCarrier = PrimaryGuardianData[0].SMSCarrier.CarrierAddress;
                  var PrimaryGuradianPhone = PrimaryGuardianData[0].Mobile;
                  var PrimaryParentName = PrimaryGuardianData[0].FullName;
                  var PrimaryDropByRelationName = PrimaryGuardianData[0].RelationName;
                  var PrimaryPickedByRelationName = PrimaryGuardianData[0].RelationName;
                  model.GuradianEmail = Result.EmailId;
                  model.GuradianSMSCarrier = Result.SMSCarrier.CarrierAddress;
                  model.GuradianPhone = Result.Mobile;
                  model.ParentName = Result.FullName;
                  model.DropByRelationName = Result.RelationName;
                  model.PickedByRelationName = Result.RelationName;
                  model.DropedById = Result.ID;
                  model.PickupById=Result.ID;
                  model.OnLeave = false;

                  //for Primary

                  if (PrimaryParentID != Result.ID) {
                    model.PrimaryGuradianEmail = PrimaryGuradianEmail;
                    model.PrimaryGuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                    model.PrimaryGuradianPhone = PrimaryGuradianPhone;
                    model.PrimaryParentName = PrimaryParentName;
                    model.PrimaryDropByRelationName = PrimaryDropByRelationName;
                     model.PrimaryPickedByRelationName = PrimaryPickedByRelationName;
                    
                  }
                  vm.promise = AttendanceService.AddStudentPickUpAttendance(model);

                }
              }
              else {
                respond("Sorry, attendance can't be marked.");

                return;
              }
              vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                  // vm.showProgressbar = false;
                  notificationService.displaymessage(response.Message);
                  respond("Attendance has been marked successfully.")
                  $state.go('triangular.attendance', {}, { reload: true });
                  // cancelClick();
                  //vm.getAttendanceData();
                  // $state.go('triangular.attendance', {}, { reload: true });
                }
                else {
                  vm.showProgressbar = false;
                  getAttendanceData();
                  notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
              });

            }
          }
        });

      }, this);
      vm.showProgressbar = false;
    };
    function updateRec() {

      $recBtn.html(recognition ? "Stop" : '<i class="fa fa-microphone" style="font-size:20px;" aria-hidden="true"></i>');

    }

    function send() {
      var text = $speechInput.val();
      $.ajax({
        type: "POST",
        url: baseUrl + "query",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: {
          "Authorization": "Bearer " + accessToken
        },
        data: JSON.stringify({ query: text, lang: "en", sessionId: "f7af3355-2303-4233-85c0-3cdfe548c4db" }),

        success: function (data) {
          prepareResponse(data);
        },
        error: function () {
          respond(messageInternalError);
        }
      });
    }

    function prepareResponse(val) {

      $('#mainBox').hide();

      $answer = val.result.speech;
      var queryWords = $answer.split(" ");
      $speech = $answer;
      for (var i = 0; i < queryWords.length; i++) {
        if (queryWords[i] == "@families") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY" || JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@families", " ");
            $state.go('triangular.families', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access families";
          }
        }
        else if (queryWords[i] == "@family_reg") {
          if ( JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@family_reg", " ");
            $state.go('triangular.addFamily', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to add family";
          }

        }
        else if (queryWords[i] == "@new_staff") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@new_staff", " ");
            $state.go('triangular.addstaff', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to add staff";
          }
        }
        else if (queryWords[i] == "@all_participant") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY" || JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@all_participant", " ");
            $state.go('triangular.allparticipants', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access participants";
          }
        }
        else if (queryWords[i] == "@enrolled_participant") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY" || JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@enrolled_participant", " ");
            $state.go('triangular.enrolledparticipants', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access participants";
          }
        }
        else if (queryWords[i] == "@future_enrolled") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY" || JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@future_enrolled", " ");
            $state.go('triangular.futureenrolledparticipants', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access participants";
          }
        }
          else if (queryWords[i] == "@apply_leave") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "STAFF" ) {
            $speech = $answer.replace("@apply_leave", " ");
            $state.go('triangular.applyleave', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access to apply leave";
          }
        }
        else if (queryWords[i] == "@unassigned_participants") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY" || JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@unassigned_participants", " ");
            $state.go('triangular.unenrolledparticipants', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access participants";
          }
        }
        else if (queryWords[i] == "@attendance_history") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY" || JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@attendance_history", " ");
            $state.go('triangular.attendancehistory', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access participants";
          }
        }
        else if (queryWords[i] == "@enrollment_history") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY" || JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@enrollment_history", " ");
            $state.go('triangular.enrollmenthistory', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access participants";
          }
        }
        else if (queryWords[i] == "@class_list") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@class_list", " ");
            $state.go('triangular.classlist', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access classes";
          }
        }
        else if (queryWords[i] == "@add_class") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@add_class", " ");
            $state.go('triangular.addclass', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access classes";
          }
        }
        else if (queryWords[i] == "@staff_list") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@staff_list", " ");
            $state.go('triangular.staff', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access staff";
          }
        }
        else if (queryWords[i] == "@events") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY" || JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
            $speech = $answer.replace("@events", " ");
            $state.go('triangular.agencyevent', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access events";
          }
        }
        else if (queryWords[i] == "@participant_attendance") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
            $speech = $answer.replace("@participant_attendance", " ");
            $state.go('triangular.attendance', {}, { reload: true });

          }
          else {
            $speech = "You are not authorised to access attendance";
          }
        }
        else if (queryWords[i] == "@staff_attendance") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
            $speech = $answer.replace("@staff_attendance", " ");
            $state.go('triangular.staffattendance', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access attendance";
          }
        }
        else if (queryWords[i] == "@failed_signout") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
            $speech = $answer.replace("@failed_signout", " ");
            $state.go('triangular.failedsignouts', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access attendance";
          }
        }
        else if (queryWords[i] == "@user_settings") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@user_settings", " ");
            $state.go('triangular.usersetting', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access user settings";
          }
        }
        else if (queryWords[i] == "@class_setting") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@class_setting", " ");
            $state.go('triangular.classes', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access class settings";
          }
        }
        else if (queryWords[i] == "@family_settings") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@family_settings", " ");
            $state.go('triangular.family', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access family settings";
          }
        }
        else if (queryWords[i] == "@staff_settings") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@staff_settings", " ");
            $state.go('triangular.toolsStaff', {}, { reload: true });
          }
          else if (JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
            $speech = $answer.replace("@staff_settings", " ");
            $state.go('triangular.staffsetting', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access staff settings";
          }
        }
        else if (queryWords[i] == "@food_management") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@food_management", " ");
            $state.go('triangular.FoodManagement', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access food management";
          }
        }
        else if (queryWords[i] == "@add_holiday") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@add_holiday", " ");
            $state.go('triangular.holiday', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access holidays";
          }
        }

           else if (queryWords[i] == "@leave_record") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@leave_record", " ");
            $state.go('triangular.staffleave', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access leave record.";
          }
        }
          else if (queryWords[i] == "@staff_schedule") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@staff_schedule", " ");
            $state.go('triangular.staffschedule', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access staff schedule";
          }
        }
        else if (queryWords[i] == "@meal_schedular") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@meal_schedular", " ");
            $state.go('triangular.mealSchedular', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access meal scheduler";
          }
        }
        else if (queryWords[i] == "@incident_list") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY") {
            $speech = $answer.replace("@incident_list", " ");
            $state.go('triangular.incident', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access incident reports";
          }
        }
        else if (queryWords[i] == "@dashboard") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@dashboard", " ");
            $state.go('triangular.dashboard', {}, { reload: true });
          }
          else if (JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY") {
            $speech = $answer.replace("@dashboard", " ");
            $state.go('triangular.parentportaldashboard', {}, { reload: true });
          }
          else if (JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
            $speech = $answer.replace("@dashboard", " ");
            $state.go('triangular.staffdashboard', {}, { reload: true });
          }
          else {
            $speech = $answer.replace("@dashboard", " ");
            $state.go('triangular.superadmindashboard', {}, { reload: true });
          }
        }

        else if (queryWords[i] == "@inbox") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY") {
            $speech = $answer.replace("@inbox", " ");
            $state.go('triangular.email.inbox', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access messages";
          }
        }
        else if (queryWords[i] == "@sent") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY") {
            $speech = $answer.replace("@sent", " ");
            $state.go('triangular.email.sent', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access messages";
          }
        }
        else if (queryWords[i] == "@add_incident") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@add_incident", " ");
            $state.go('triangular.addincident', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access incident reports";
          }
        }
        else if (queryWords[i] == "@new_todo") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@new_todo", " ");
            $state.go('triangular.todo', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access to do";
          }
        }
        else if (queryWords[i] == "@todo_list") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY") {
            $speech = $answer.replace("@todo_list", " ");
            $state.go('triangular.todo', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access to do";
          }
        }
        else if (queryWords[i] == "@add_dailystatus") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY") {
            $speech = $answer.replace("@add_dailystatus", " ");
            $state.go('triangular.dailystatus', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access daily status";
          }
        }
        else if (queryWords[i] == "@dailystatus_list") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "AGENCY" || JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY") {
            $speech = $answer.replace("@dailystatus_list", " ");
            $state.go('triangular.dailystatus', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access daily status";
          }
        }
        else if (queryWords[i] == "@timeclock") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
            $speech = $answer.replace("@timeclock", " ");
            $state.go('triangular.timesheet', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access staff time clock";
          }
        }
        else if (queryWords[i] == "@staff_skill") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
            $speech = $answer.replace("@staff_skill", " ");
            $state.go('triangular.staffskill', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access staff skills";
          }
        }
        else if (queryWords[i] == "@organization_list") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "SuperAdmin") {
            $speech = $answer.replace("@organization_list", " ");
            $state.go('triangular.superadmin', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access organization list";
          }
        }
        else if (queryWords[i] == "@pricing_plan") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "SuperAdmin") {
            $speech = $answer.replace("@pricing_plan", " ");
            $state.go('triangular.participantsubscription', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access pricing plans";
          }
        }
        else if (queryWords[i] == "@timeclockplans") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "SuperAdmin") {
            $speech = $answer.replace("@timeclockplans", " ");
            $state.go('triangular.timeclocksubscription', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access time clock user plans";
          }
        }
        else if (queryWords[i] == "@enhancements") {
          if (JSON.parse(localStorage.getItem('roles'))[0] == "SuperAdmin") {
            $speech = $answer.replace("@enhancements", " ");
            $state.go('triangular.enhancement', {}, { reload: true });
          }
          else {
            $speech = "You are not authorised to access enhancements";
          }
        }
        else {
          $state.reload();
        }
      }
      var debugJSON = JSON.stringify(val, undefined, 2),
        spokenResponse = $speech;

      respond(spokenResponse);
      debugRespond(debugJSON);
    }

    function debugRespond(val) {
      $("#response").text(val);
    }

    function respond(val) {
      if (val == "") {
        val = messageSorry;
      }

      if (val !== messageRecording) {
        var msg = new SpeechSynthesisUtterance();
        msg.voiceURI = "native";
        msg.text = val;
        msg.lang = "en-US";
        window.speechSynthesis.speak(msg);
      }

      $("#spokenResponse").addClass("is-active").find(".spoken-response__text").html(val);
    }

    if ($injector.has('UserService')) {
      var UserService = $injector.get('UserService');
      vm.defaultUser = {
        profileName: localStorage.profileName,
        username: 'christos',
        Image: localStorage.Image,
        roles: []
      };

      //vm.currentUser = UserService.getCurrentUser();
    }
    else {
      // permissions are turned off so no UserService available
      // just set default user
      vm.currentUser = {
        profileName: localStorage.profileName,
        username: 'christos',
        Image: localStorage.Image,
        roles: []
      };
    }
    checkAdmin();
    // $scope.temp = "Parag";
    function checkAdmin() {
      if (JSON.parse(localStorage.getItem('roles'))[0] == "SuperAdmin" || JSON.parse(localStorage.getItem('roles'))[0] == "FAMILY") {
        $scope.temp = 1;
      }
    }
    ////////////////

    function openSideNav(navID) {
      $mdUtil.debounce(function () {
        $mdSidenav(navID).toggle();
      }, 300)();
    }
    // triLayoutProvider.setDefaultOption('sideMenuSize', 'off');

    function switchLanguage(languageCode) {
      if ($injector.has('$translate')) {
        var $translate = $injector.get('$translate');
        $translate.use(languageCode)
          .then(function () {
            $mdToast.show(
              $mdToast.simple()
                .content($filter('triTranslate')('Language Changed'))
                .position('bottom right')
                .hideDelay(500)
            );
            $rootScope.$emit('changeTitle');
          });
      }
    }


    function UserSetting() {
      if (JSON.parse(localStorage.getItem('roles'))[0] == "STAFF") {
        $state.go('triangular.staffsetting');
      }
      else {
        $state.go('triangular.usersetting');
      }
    }
    // chatbotHideMenu();
    // function chatbotHideMenu(){
    //     if($sessionStorage.chatbot==true)
    //     {
    //          vm.oldSwitchMenuStyle = triLayout.layout.sideMenuSize;
    //         triLayout.layout.sideMenuSize = 'off';
    //         vm.menuswitchText = 'Show Menu';
    //     }
    // }
    function hideMenu(primarySwitch) {
      if (primarySwitch == false) {
        triLayout.layout.sideMenuSize = vm.oldSwitchMenuStyle;
        //vm.oldSwitchMenuStyle = triLayout.layout.sideMenuSize;
        vm.menuswitchText = 'Hide Menu';
      }
      else if (primarySwitch == true) {
        vm.oldSwitchMenuStyle = triLayout.layout.sideMenuSize;
        triLayout.layout.sideMenuSize = 'off';
        vm.menuswitchText = 'Show Menu';
      }
      //alert(primarySwitch);
    }

    function hideMenuButton() {
      switch (triLayout.layout.sideMenuSize) {
        case 'hidden':
          // always show button if menu is hidden
          return false;
        case 'off':
          // never show button if menu is turned off
          return true;
        default:
          // show the menu button when screen is mobile and menu is hidden
          return $mdMedia('gt-sm');
      }
    }

    function toggleNotificationsTab(tab) {
      $rootScope.$broadcast('triSwitchNotificationTab', tab);
      vm.openSideNav('notifications');
    }

    function toggleFullScreen() {
      vm.isFullScreen = !vm.isFullScreen;
      vm.fullScreenIcon = vm.isFullScreen ? 'zmdi zmdi-fullscreen-exit' : 'zmdi zmdi-fullscreen';
      // more info here: https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API
      var doc = $document[0];
      if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        if (doc.documentElement.requestFullscreen) {
          doc.documentElement.requestFullscreen();
        } else if (doc.documentElement.msRequestFullscreen) {
          doc.documentElement.msRequestFullscreen();
        } else if (doc.documentElement.mozRequestFullScreen) {
          doc.documentElement.mozRequestFullScreen();
        } else if (doc.documentElement.webkitRequestFullscreen) {
          doc.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
      } else {
        if (doc.exitFullscreen) {
          doc.exitFullscreen();
        } else if (doc.msExitFullscreen) {
          doc.msExitFullscreen();
        } else if (doc.mozCancelFullScreen) {
          doc.mozCancelFullScreen();
        } else if (doc.webkitExitFullscreen) {
          doc.webkitExitFullscreen();
        }
      }
    }

    function Logout() {
      localStorage.clear();      
      $state.go('authentication.login');
    }

    $scope.$on('newMailNotification', function () {
      vm.emailNew = true;
    });
  }
})();
