(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoaderController', LoaderController);

    /* @ngInject */
    function LoaderController(triSettings, $scope, $q, $http, HOST_URL, $localStorage) {
        var vm = this;
        vm.triSettings = triSettings;

        // $scope.$on('$viewContentLoaded', function () {
        //     try {
        //         var badgecount = 0;
        //         var model = {
        //             UserId: $sessionStorage.UserId,
        //             RoleId: $sessionStorage.RoleId
        //         };
        //         $http.post(HOST_URL.url + '/api/Email/GetUnreadCount', model).success(function (response, status, headers, config) {
        //             window.emailMenu.children[0].badge = response;
        //             //$scope.$apply();
        //         }).error(function (errResp) {

        //         });
        //     } catch (e) {
        //     }
        // });

        UpdateBadge();
        function UpdateBadge() {
            var model = {
                UserId: localStorage.UserId,
                RoleId: localStorage.RoleId
            };
            $http.post(HOST_URL.url + '/api/Email/GetUnreadCount', model).success(function (response, status, headers, config) {
                window.emailMenu.children[0].badge = response;
            }).error(function (errResp) {

            });

        }

    }
})();
