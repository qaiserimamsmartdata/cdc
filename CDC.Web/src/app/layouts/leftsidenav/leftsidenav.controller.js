(function() {
    'use strict';

    angular
        .module('triangular.components')
        .controller('LeftSidenavController', LeftSidenavController);

    /* @ngInject */
    function LeftSidenavController(triSettings,$scope, $stateParams,triLayout) {
        var vm = this;
    //      var data=$stateParams.obj;

    // if(data == true){
    //     $scope.ShouldAutoStart = true;
    // }

        vm.layout = triLayout.layout;
        vm.sidebarInfo = {
            appName: triSettings.name,
            appLogo: triSettings.logo
        };
        vm.toggleIconMenu = toggleIconMenu;

        ////////////
//  $scope.CompletedEvent = function (scope) {
//             console.log("Completed Event called");
//         };

//         $scope.ExitEvent = function (scope) {
//             console.log("Exit Event called");
//         };

//         $scope.ChangeEvent = function (targetElement, scope) {
//             console.log("Change Event called");
//             console.log(targetElement);  //The target element
//             console.log(this);  //The IntroJS object
//         };

//         $scope.BeforeChangeEvent = function (targetElement, scope) {
//             console.log("Before Change Event called");
//             console.log(targetElement);
//         };

//         $scope.AfterChangeEvent = function (targetElement, scope) {
//             console.log("After Change Event called");
//             console.log(targetElement);
//         };

//         $scope.IntroOptions = {
//             steps: [
//                 {
//                     element: document.querySelector('#ppp'),
//                     intro: "<b>Add Family to start.</b>"
//                 },
//                 {
//                     element: document.querySelectorAll('#step2')[0],
//                     intro: "<b>Add participants into family registeration.</b>",
//                     position: 'right'
//                 },
//                 {
//                     element: document.querySelector('#step3'),
//                     intro: "<b>Add staff to classes you've registered.</b>",
//                     position: 'right'
//                 },
//                 {
//                     element: document.querySelector('#step4'),
//                     intro: "<b>You are all set to go!</b>",

//                 },


//             ],
//             showStepNumbers: false,
//             exitOnOverlayClick: true,
//             exitOnEsc: true,
//             nextLabel: '<i class="fa fa-hand-o-right" aria-hidden="true"></i>',
//             prevLabel: '<i class="fa fa-hand-o-left" aria-hidden="true"></i>',
//             skipLabel: 'Exit',
//             doneLabel: 'Thanks'
//         };
//         $scope.ShouldAutoStart = true;
        function toggleIconMenu() {
            var menu = vm.layout.sideMenuSize === 'icon' ? 'full' : 'icon';
            triLayout.setOption('sideMenuSize', menu);
        }
    }
})();
