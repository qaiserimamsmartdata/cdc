(function () {
    'use strict';

    angular
        .module('app.skills')
        .factory('SkillService', SkillService);

    SkillService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function SkillService($http, $q, HOST_URL) {
       
        return {
            GetSkillListService: GetSkillListService,
            DeleteSkillById:DeleteSkillById,
            GetSkillListByIdService:GetSkillListByIdService
        };

        ////////////////

        function GetSkillListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Skills/GetSkill', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            })
                .error(function (errResp) {
                    deferred.reject({ message: "Really bad" });
                });
            return deferred.promise;
        };
         function DeleteSkillById(SkillId) {
            var deferred = $q.defer();
                        $http.post(HOST_URL.url + '/api/Skills/DeleteSkillById?SkillId='+SkillId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
           function GetSkillListByIdService(SkillId) {
            var deferred = $q.defer();
            $http.get(HOST_URL.url + '/api/Skills/GetSkillById?SkillId='+SkillId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
    }
})();