(function () {
    'use strict';

    angular
        .module('app.skills')
        .controller('AddStudentSkillsController', AddStudentSkillsController);

    /* @ngInject */

    function AddStudentSkillsController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, $localStorage) {
        var vm = this;
        $scope.CategoryList = [];
        vm.testData = ['triangular', 'is', 'great'];
        vm.addSkill = addSkill;
        vm.showProgressbar = false;
        vm.skill = {
            Skill: '',
            Details: '',
            DaysRequired: '',
            ClassesRequired: '',
            CategoryId: ''
        };
        vm.query = {
            AgencyId: localStorage.agencyId
        };
        GetCategory();
        function addSkill(registerSkill) {
            if (registerSkill.$valid) {
                var model = {
                    Skill: registerSkill.Skill.$modelValue,
                    Details: registerSkill.Details.$modelValue,
                    DaysRequired: registerSkill.DayRequired.$modelValue,
                    ClassesRequired: registerSkill.ClassesRequired.$modelValue,
                    CategoryId: registerSkill.category.$modelValue,
                    SubSkill: registerSkill.Subskill.$modelValue,
                    AgencyId: localStorage.agencyId
                };
                $http.post(HOST_URL.url + '/api/Skills/AddSkill', model).then(function (response) {
                    if (response.data == 1) {
                        vm.showProgressbar = false;
                        // sessionService.setSession(response.data.Content);
                        NotificationMessageController('Skills added successfully.');
                        $state.go('triangular.skills');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function GetCategory() {
            $http.post(HOST_URL.url + '/api/Category/GetCategory', vm.query).then(function (response) {
                if (response.data.category.length > 0) {

                    $scope.CategoryList = response.data.category;
                }
            })
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
})();