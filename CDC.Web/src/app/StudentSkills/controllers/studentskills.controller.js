(function () {
    'use strict';

    angular
        .module('app.skills')
        .controller('SkillsPageController', SkillsPageController)
        .controller('UpdateSkillController', UpdateSkillController);
    /* @ngInject */
    function SkillsPageController($scope, $http, $state, $stateParams, $mdDialog, $localStorage, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, SkillService, filerService) {
        var vm = this;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        vm.testData = ['triangular', 'is', 'great'];
        $scope.selected = [];
        vm.columns = {
            Category: 'CATEGORY',
            Skill: 'SKILLS',
            Subskill: 'SUBSKILLS',
            Detail: 'DETAIL',
            DaysReq: 'DAYS REQ.',
            ClassesReq: 'CLASSES REQ.',
            ID: 'ID'
        };
        vm.query = {
            AgencyId: localStorage.agencyId
        };
        vm.showDeleteSkillConfirmDialoue = showDeleteSkillConfirmDialoue;
        vm.showUpdateSkillDialoue = showUpdateSkillDialoue;
        vm.skillCount = 0;
        var SkillId = $stateParams.obj;
        vm.GetSkillList = GetSkillList;
        $scope.query = {
            filter: '',
            limit: '5',
            order: '-Skill',
            page: 1,
            name: ''
        };
        GetSkillList();

        $rootScope.$on("GetSkillList", function () {
            GetSkillList();
        });

        function GetSkillList() {
            vm.promise = SkillService.GetSkillListService($scope.query);
            vm.promise.then(function (response) {
                if (response.skill.length > 0) {
                    vm.skillList = response.skill;
                    vm.skillCount = response.skill[0].TotalCount;
                    $scope.selected = [];
                }
                else {
                    if (response.skill.length == 0) {
                        vm.skillList = [];
                        vm.skillCount = 0;
                        NotificationMessageController('No skill list found.');
                    }
                    else {
                        NotificationMessageController('Unable to get skill list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        }
        function showUpdateSkillDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateSkillController,
                controllerAs: 'vm',
                templateUrl: 'app/StudentSkills/views/studentupdateskill.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { skilldetail: data },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })


        };
        function reset() {
            $scope.query.name = '';
            GetSkillList();
        };

        function showDeleteSkillConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this skill permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteSkill(data);
            }, function () {
                $scope.hide();
            });
        };
        function deleteSkill(data) {
            vm.promise = SkillService.DeleteSkillById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    GetSkillList();
                }
                else {

                    NotificationMessageController('Unable to get skill list at the moment. Please try again after some time.');
                }
            });
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
    function UpdateSkillController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, SkillService, HOST_URL, items, $localStorage) {

        var vm = this;
        vm.skill = {
            CategoryId: '',
            Skill: '',
            Subskill: '',
            Details: '',
            DaysRequired: '',
            ClassesRequired: '',
            ID: ''
        };
        $scope.CategoryList = [];
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();

        };
        vm.query = {
            AgencyId: localStorage.agencyId
        };
        GetCategory();
        GetSkillDetailbyId();


        function GetSkillDetailbyId() {
            vm.skill.ClassesRequired = items.skilldetail.ClassesRequired;
            vm.skill.DaysRequired = items.skilldetail.DaysRequired;
            vm.skill.Details = items.skilldetail.Details;
            vm.skill.ID = items.skilldetail.ID;
            vm.skill.Subskill = items.skilldetail.SubSkill == "N/A" ? "" : items.skilldetail.SubSkill;
            vm.skill.CategoryId = items.skilldetail.categoryId;
            vm.skill.Skill = items.skilldetail.skill;
        }
        vm.updateSkill = updateSkill;

        function updateSkill(registerSkill) {
            if (registerSkill.$valid) {
                var model = {
                    ID: vm.skill.ID,
                    Skill: registerSkill.Skill.$modelValue,
                    Details: registerSkill.Details.$modelValue,
                    DaysRequired: registerSkill.DayRequired.$modelValue,
                    ClassesRequired: registerSkill.ClassesRequired.$modelValue,
                    CategoryId: registerSkill.category.$modelValue,
                    SubSkill: registerSkill.Subskill.$modelValue
                };

                $http.post(HOST_URL.url + '/api/Skills/UpdateSkill', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Skill updated successfully.');
                        $rootScope.$emit("GetSkillList", {});
                        $mdDialog.hide();
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }
        function GetCategory() {
            $http.post(HOST_URL.url + '/api/Category/GetCategory', vm.query).then(function (response) {
                if (response.data.category.length > 0) {

                    $scope.CategoryList = response.data.category;
                }
            })
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
})();