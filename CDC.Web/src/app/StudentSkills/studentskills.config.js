(function () {
    'use strict';

    angular
        .module('app.skills')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.skills', {
                url: '/skills',
                templateUrl: 'app/StudentSkills/views/studentskills.tmpl.html',
                // set the controller to load for this page
                controller: 'SkillsPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewSkills']
                    }
                }
            })
            .state('triangular.AddStudentskills', {
                url: '/skills/add',
                templateUrl: 'app/StudentSkills/views/addstudentskills.tmpl.html',
                // set the controller to load for this page
                controller: 'AddStudentSkillsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                }
            })
            .state('triangular.updateskill', {
                url: '/updateskill',
                templateUrl: 'app/StudentSkills/views/studentupdateskill.tmpl.html',
                // set the controller to load for this page
                controller: 'SkillsPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                }
            });

        // triMenuProvider.addMenu({
        //     name: 'Skills',
        //     icon: 'fa fa-tree',
        //     state: 'triangular.skills',
        //     permission: 'viewSkills',
        //     type: 'link',
        //     priority: 1.1
        // });
    }
})();
