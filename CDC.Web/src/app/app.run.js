(function () {
    'use strict';

    angular
        .module('app')
        .run(runFunction)
        .factory('filerService', filerService)
     
        .directive('allowPattern', allowPattern).directive('errSrc', imageDefault);

    /* @ngInject */
    function runFunction($rootScope, $state) {

        // default redirect if access is denied
        function redirectError() {
            $state.go('500');
        }

        // watches

        // redirect all errors to permissions to 500
        var errorHandle = $rootScope.$on('$stateChangeError', redirectError);

        // remove watch on destroy
        $rootScope.$on('$destroy', function () {
            errorHandle();
        });
        $rootScope.emailFormat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $rootScope.defaultFormat = "assets/images/profilepic/images4.png";
    }

    filerService.$inject = ['$http', '$q', 'HOST_URL', '$timeout', '$mdToast', '$rootScope', '$mdSidenav', '$log'];

    function filerService($http, $q, HOST_URL, $timeout, $mdToast, $rootScope, $mdSidenav, $log) {
        return {
            close: close,
            close1: close1,
            close2: close2,
            close3: close3,
             close4: close4,
            toggleRight: buildToggler,
            toggleRight1: buildToggler1,
            toggleRight2: buildToggler2,
            toggleRight3: buildToggler3,
              toggleRight4: buildToggler4,
            isOpenRight: isOpenRight,
            isOpenRight1: isOpenRight1,
            isOpenRight2: isOpenRight2,
            isOpenRight3: isOpenRight3,
             isOpenRight4: isOpenRight4
        };

        function buildToggler() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right')
                    .toggle()
                    .then(function () {
                        $log.debug("toggle right is done");
                    });
            }
        }

        function buildToggler1() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right1')
                    .toggle()
                    .then(function () {
                        $log.debug("toggle right1 is done");
                    });
            }
        }

        function buildToggler2() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right2')
                    .toggle()
                    .then(function () {
                        $log.debug("toggle right2 is done");
                    });
            }
        }

        function buildToggler3() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right3')
                    .toggle()
                    .then(function () {
                        $log.debug("toggle right3 is done");
                    });
            }
        }
        
        function buildToggler4() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right4')
                    .toggle()
                    .then(function () {
                        $log.debug("toggle right4 is done");
                    });
            }
        }


        function close() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right').close()
                    .then(function () {
                        $log.debug("close RIGHT is done");
                    });
            }
        };

        function close1() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right1').close()
                    .then(function () {
                        $log.debug("close RIGHT is done");
                    });
            }
        };

        function close2() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right2').close()
                    .then(function () {
                        $log.debug("close RIGHT2 is done");
                    });
            }
        };

        function close3() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right3').close()
                    .then(function () {
                        $log.debug("close RIGHT3 is done");
                    });
            }
        };
         function close4() {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav('right4').close()
                    .then(function () {
                        $log.debug("close RIGHT4 is done");
                    });
            }
        };

        function isOpenRight() {
            return function () {
                return $mdSidenav('right').isOpen();
            }
        };
          function isOpenRight4() {
            return function () {
                return $mdSidenav('right4').isOpen();
            }
        };

        function isOpenRight1() {
            return function () {
                return $mdSidenav('right1').isOpen();
            }
        };

        function isOpenRight2() {
            return function () {
                return $mdSidenav('right2').isOpen();
            }
        };

        function isOpenRight3() {
            return function () {
                return $mdSidenav('right3').isOpen();
            }
        };

    }

    function allowPattern() {
        return {
            restrict: "A",
            compile: function (tElement, tAttrs) {
                return function (scope, element, attrs) {
                    // I handle key events
                    element.bind("keypress paste", function (event) {
                        var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                        if ((event.shiftKey == true) || (keyCode != 8 && keyCode != 9 && keyCode != 37 && keyCode != 38 && keyCode != 39 && keyCode != 40)) {
                            var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode. 
                            // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                            if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                                event.preventDefault();
                                return false;
                            }
                        }
                    });
                };
            }
        };
    }


    //error image directive
    function imageDefault() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });

                // attrs.$observe('lazySrc', function (value) {
                //     if (!value && attrs.errSrc) {
                //         attrs.$set('src', attrs.errSrc);
                //     }
                // });
            }
        }
    }
})();
