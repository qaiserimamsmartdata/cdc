(function () {
    'use strict';

    angular
        .module('app.incident')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.incident', {
                url: '/incident',
                templateUrl: 'app/ParticipantIncident/views/incident.tmpl.html',
                // set the controller to load for this page
                controller: 'IncidentPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.parentincident', {
                url: '/parent/participants/incident',
                templateUrl: 'app/ParticipantIncident/views/incident.tmpl.html',
                // set the controller to load for this page
                controller: 'IncidentPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }
            })
            .state('triangular.addincident', {
                url: '/incident/add',
                templateUrl: 'app/ParticipantIncident/views/addincident.tmpl.html',
                // set the controller to load for this page
                controller: 'AddIncidentController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.updateincident', {
                url: '/incident/update',
                templateUrl: 'app/ParticipantIncident/views/updateincident.tmpl.html',
                // set the controller to load for this page
                controller: 'UpdateIncidentController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                params: {
                    objIncidentData: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.parentupdateincident', {
                url: '/participant/incident/update',
                templateUrl: 'app/ParticipantIncident/views/updateincident.tmpl.html',
                // set the controller to load for this page
                controller: 'UpdateIncidentController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                params: {
                    objIncidentData: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }
            })
            .state('triangular.incidentdetails', {
                url: '/incident/details',
                templateUrl: 'app/ParticipantIncident/views/viewincident.tmpl.html',
                // set the controller to load for this page
                controller: 'ViewNewIncidentController',
                controllerAs: 'vm',
                params: {
                    objIncident: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
             .state('triangular.parentportalincidentdetails', {
                url: '/participant/incident/details',
                templateUrl: 'app/ParticipantIncident/views/viewincident.tmpl.html',
                // set the controller to load for this page
                controller: 'ViewNewIncidentController',
                controllerAs: 'vm',
                params: {
                    objIncident: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "FAMILY", $window);
                    }
                }
            });
        triMenuProvider.addMenu({
            name: 'Incidents',
            icon: 'fa fa-tree',
            id: 'step2',
            type: 'dropdown',
            priority: 9,
            permission: 'viewIncident',
            children: [{
                name: 'Incident Report',
                state: 'triangular.incident',
                type: 'link',
                permission: 'viewAgencyIncident'
            }, {
                    name: 'Incident Report',
                    state: 'triangular.parentincident',
                    type: 'link',
                    permission: 'viewParentIncident'
                }]
        });
    }
})();
