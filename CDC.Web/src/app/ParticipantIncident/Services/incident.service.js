﻿(function () {
    'use strict';

    angular
        .module('app.incident')
        .factory('IncidentService', IncidentService);

    IncidentService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function IncidentService($http, $q, HOST_URL) {
        return {
            GetincidentListService: GetincidentListService,
            GetIncidentListByIdService: GetIncidentListByIdService,
            DeleteIncidentById:DeleteIncidentById
            // DeleteStaffById: DeleteStaffById,
            // SetId: SetId
        };
 
        function SetId(data) {
            this.Id=0;
            this.StaffName='';
            this.Id = data.ID;
            this.StaffName = data.FullName;
            this.StaffData=data;
        };
        function GetincidentListService(model) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Incident/GetAllIncident', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
        function GetIncidentListByIdService(model) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Incident/GetIncidentById',model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
        function DeleteStaffById(StaffId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Staff/DeleteStaff?staffId=' + StaffId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
   function DeleteIncidentById(IncidentId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Incident/DeleteIncident?incidentId=' + IncidentId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
    }
})();