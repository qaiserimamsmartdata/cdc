(function () {
    'use strict';
    angular
        .module('app.incident')
        .controller('ViewIncidentController', ViewIncidentController);
    function ViewIncidentController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, AgencyService, $localStorage, imageUploadService, notificationService, objIncident, IncidentService) {
        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.changed = function () {
            $log.log('Time changed to: ' + $scope.mytime);
        };

        $scope.clear = function () {
            $scope.mytime = null;
        };
        $scope.currentDate = new Date();


        var vm = this;
        vm.ImageUrlPath = HOST_URL.url;
        $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        // vm.Incident = {
        //     AgencyName:''
        // };
        vm.ParticipantIds = {
            StudentInfoId: [],
            OtherStudentInfoId: [],
        };
        vm.AgencyName = localStorage.AgencyName;
        vm.IsVisibleActionButton = false;
        if (localStorage.FamilyId != null || localStorage.FamilyId != undefined)
            vm.IsVisibleActionButton = true;
        vm.status = 'idle';  // idle | uploading | complete
        vm.Title = "Incident Report";
        vm.Incident = {};
        vm.addIncident = addIncident;
        vm.showProgressbar = false;
        // vm.GetPositions = GetPositions;
        // vm.ResetFields = ResetFields();
        vm.queryContacts = queryContacts;
        $scope.id = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            staff: 0,
            name: '',
            PositionId: 0,
            AgencyId: $scope.$storage.agencyId,
            IsTimeClockUser: true
        };
        vm.selectedVegetables = [];

        vm.autocompleteDemoRequireMatch = true;
        vm.readonly = false;
        vm.selectedItem = null;
        vm.selectedItemOther = null;
        vm.searchText = null;
        vm.querySearch = querySearch;
        vm.loadParticipants = loadParticipants();
        vm.selectedParticipants = [];
        vm.numberChips = [];
        vm.numberChips2 = [];
        vm.numberBuffer = '';
        vm.autocompleteDemoRequireMatch = true;
        vm.getParentInformation = getParentInformation;
        vm.remove = remove;
        vm.maxDate = new Date();
        vm.getIncidentData = getIncidentData;
        /**
         * Search for vegetables.
         */
        function querySearch(query) {
            var results = query ? vm.AllParticipant.filter(createFilterFor(query)) : [];
            return results;
        }
        getIncidentData();
        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(participant) {
                return (participant._lowername.indexOf(lowercaseQuery) === 0);
            };

        }

        function remove() {
            vm.ParentName = "";
            vm.PhoneNumber = "";
            vm.ParentId = "";
        }
        function getParentInformation(obj) {
            vm.Incident.AgeOfChild = obj.Age;
            var model = {
                StudentId: obj.studentId
            }
            vm.promise = CommonService.getParentInformationByStudent(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.ParentInfo = null;
                    vm.ParentInfo = eval(response.Content);
                    vm.ParentName = vm.ParentInfo[0].Name;
                    vm.PhoneNumber = vm.ParentInfo[0].PhoneNumber;
                    vm.ParentId = vm.ParentInfo[0].ParentId;
                    vm.FamilyId = vm.ParentInfo[0].FamilyId;
                }
            });
        }
        vm.AllParticipant = [];
        function loadParticipants() {
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllParticipant = null;
                    vm.AllParticipant = eval(response.Content);
                    //    vm.AllParticipant.formap(function (participant) {
                    //         participant._lowername = participant.name.toLowerCase();
                    //         // participant._lowertype = "Brassica".toLowerCase();
                    //     })
                }
            });
            // var veggies = [
            //     {
            //         'name': 'Broccoli',
            //         'type': 'Brassica'
            //     },
            //     {
            //         'name': 'Cabbage',
            //         'type': 'Brassica'
            //     },
            //     {
            //         'name': 'Carrot',
            //         'type': 'Umbelliferous'
            //     },
            //     {
            //         'name': 'Lettuce',
            //         'type': 'Composite'
            //     },
            //     {
            //         'name': 'Spinach',
            //         'type': 'Goosefoot'
            //     }
            // ];


        }

        getParticipants();
        getAllStaff();
        function getParticipants() {
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllParticipant = null;
                    vm.AllParticipant = eval(response.Content);
                }
            });
        };
        function getAllStaff() {
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllStaff(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllStaff = null;
                    vm.AllStaff = eval(response.Content);
                }
            });
        };
        vm.FirstAidAdministered = false;
        vm.DoctorRequired = false;
        function getIncidentData() {
            var model={
                IncidentId:objIncident.incidentId,
                TimeZone:localStorage.TimeZone
            }
            vm.promise = IncidentService.GetIncidentListByIdService(model);
            vm.promise.then(function (response) {
                if (response.incidentData != null) {
                    // vm.IncidentInfo = null;
                    // vm.IncidentInfo = eval(response.incidentData);
                    vm.ParticipantName = response.incidentData.ParticipantName;
                    vm.StaffName = response.incidentData.StaffName;
                    vm.DateOfAccident = response.incidentData.DateOfAccident;
                    vm.TimeOfAccident = response.incidentData.TimeOfAccident;
                    vm.ParentName = response.incidentData.ParentName;
                    vm.AgeOfChild = response.incidentData.AgeOfChild;
                    vm.PlaceOfAccident = response.incidentData.PlaceOfAccident;
                    vm.NatureOfInjury = response.incidentData.NatureOfInjury;
                    vm.OtherTreatment = response.incidentData.OtherTreatment;
                    vm.FirstAidAdministered = response.incidentData.FirstAidAdministered;
                    vm.DoctorRequired = response.incidentData.DoctorRequired;
                    // vm.OtherParticipants = response.incidentData.OtherParticipants;
                    vm.DescriptionOfInjury = response.incidentData.DescriptionOfInjury;
                    vm.ActionTaken = response.incidentData.ActionTaken;
                    vm.PhoneNumber = response.incidentData.PhoneNumber;
                    vm.OtherParticipants = response.incidentData.OtherParticipants;


                }
            });
        }
        function queryContacts($query) {
            var lowercaseQuery = angular.lowercase($query);
            return vm.AllParticipant.filter(function (contact) {
                var lowercaseName = angular.lowercase(contact.name);
                if (lowercaseName.indexOf(lowercaseQuery) !== -1) {
                    return contact;
                }
            });
        }
        // function GetPositions() {
        //     vm.promise = CommonService.getPositions(localStorage.agencyId);
        //     vm.promise.then(function (response) {
        //         if (response.length > 0) {
        //             vm.AllPosition = null;
        //             vm.AllPosition = eval(response);
        //         }
        //     });
        // };
        vm.Incident = {
            ListStudentInfoIds: [],
            ListOtherStudentInfoIds: [],
            TimeOfAccident: new Date(new Date().toDateString() + ' ' + '09:00:00'),
        }

        function addIncident(Obj) {
            if (Obj.$valid) {
                vm.showProgressbar = true;
                vm.Incident.AgencyRegistrationId = $scope.$storage.agencyId;
                vm.Incident.ParentId = vm.ParentId;
                vm.Incident.FamilyId = vm.FamilyId;
                vm.ParticipantIds.StudentInfoId.forEach(function (element) {
                    vm.Incident.ListStudentInfoIds.push({
                        'studentId': element
                            .studentId
                    });
                }, this);
                vm.ParticipantIds.OtherStudentInfoId.forEach(function (element) {
                    vm.Incident.ListOtherStudentInfoIds.push({
                        'studentId': element
                            .studentId
                    });
                }, this);
                var model = {};
                model = vm.Incident;
                model.TimeOfAccident = (moment(model.TimeOfAccident).format('HH:mm:ss'));
                $http.post(HOST_URL.url + '/api/Incident/AddIncident', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController(response.data.ReturnMessage[0]);
                        // ResetFields();
                        $state.go('triangular.incident');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function ResetFields() {
            vm.staff = {};

        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
})();