(function () {
    'use strict';

    angular
        .module('app.incident')
        .controller('ViewNewIncidentController', ViewNewIncidentController);

    /* @ngInject */
    function ViewNewIncidentController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, AgencyService, $localStorage, imageUploadService, notificationService, IncidentService) {

        var vm = this;
        if ($stateParams.objIncident == null || $stateParams.objIncident == undefined) {
            if(localStorage.Portal=='AGENCY')
            $state.go("triangular.incident");
            else
            $state.go("triangular.parentincident");
            return;
        }
        var Id = $stateParams.objIncident;
        vm.getIncidentData = getIncidentData;
        vm.updateIsRead = updateIsRead;
        getIncidentData();
        function getIncidentData() {
                var model={
                IncidentId:Id,
                TimeZone:localStorage.TimeZone
            }
            vm.promise = IncidentService.GetIncidentListByIdService(model);
            vm.promise.then(function (response) {
                if (response.incidentData != null) {
                    // vm.IncidentInfo = null;
                    // vm.IncidentInfo = eval(response.incidentData);
                    vm.ParticipantName = response.incidentData.ParticipantName;
                    vm.StaffName = response.incidentData.StaffName;
                    vm.DateOfAccident = response.incidentData.DateOfAccident;
                    vm.TimeOfAccident = response.incidentData.TimeOfAccident;
                    vm.ParentName = response.incidentData.ParentName;
                    vm.AgeOfChild = response.incidentData.AgeOfChild;
                    vm.PlaceOfAccident = response.incidentData.PlaceOfAccident;
                    vm.NatureOfInjury = response.incidentData.NatureOfInjury;
                    vm.OtherTreatment = response.incidentData.OtherTreatment;
                    //vm.FirstAidAdministered = response.incidentData.FirstAidAdministered;
                    if (response.incidentData.DoctorRequired == true) {
                        vm.DoctorRequired = 'true';
                    }
                    else if (response.incidentData.DoctorRequired == false) {
                        vm.DoctorRequired = 'false';
                    }
                    if (response.incidentData.FirstAidAdministered == true) {
                        vm.FirstAidAdministered = 'true';
                    }
                    else if (response.incidentData.FirstAidAdministered == false) {
                        vm.FirstAidAdministered = 'false';
                    }
                    if (response.incidentData.ParentInformed == true) {
                        vm.ParentInformed = 'true';
                    }
                    else if (response.incidentData.ParentInformed == false) {
                        vm.ParentInformed = 'false';
                    }
                    //vm.DoctorRequired = response.incidentData.DoctorRequired;
                    vm.ParentDescription = response.incidentData.ParentDescription;
                    vm.OtherParticipants = response.incidentData.OtherParticipants;
                    vm.DescriptionOfInjury = response.incidentData.DescriptionOfInjury;
                    vm.ActionTaken = response.incidentData.ActionTaken;
                    vm.PhoneNumber = response.incidentData.PhoneNumber;
                    vm.OtherParticipants = response.incidentData.OtherParticipants;
                    vm.IsRead = response.incidentData.IsRead;



                }
            });
        }
        function updateIsRead(IsRead) {

            var model = {
                IncidentId: Id,
                IsRead: IsRead
            };

            $http.post(HOST_URL.url + '/api/Incident/UpdateIncidentRead', model).then(function (response) {
                if (response.data.ReturnStatus == true) {
                    if(localStorage.Portal=="AGENCY")
                    $state.go('triangular.incident');
                    else
                    $state.go('triangular.parentincident');
                }
                else {
                    vm.showProgressbar = false;
                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                }
            });
        }


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

})();