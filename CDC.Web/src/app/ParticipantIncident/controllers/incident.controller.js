(function () {
    'use strict';

    angular
        .module('app.incident')
        .controller('IncidentPageController', IncidentPageController);


    /* @ngInject */
    function IncidentPageController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, CommonService, AgencyService, filerService, $localStorage, HOST_URL, IncidentService) {
        var vm = this;
        $scope.$storage = localStorage;
        vm.showDeleteIncidentConfirmDialoue=showDeleteIncidentConfirmDialoue;
        //    $scope.$storage = localStorage;
        // if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        localStorage.ParticipantAttendancePage = false;
        vm.IsVisibleActionButton = false;
        if (localStorage.FamilyId != null || localStorage.FamilyId != undefined)
            vm.IsVisibleActionButton = true;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.ImageUrlPath = HOST_URL.url;
        vm.viewDetails = viewDetails;
        vm.close = filerService.close();
        vm.columns = {
            Name: 'Participant',
            ReporterName: 'Reporter',
            DateOfAccident: 'Accident Date',
            PlaceOfAccident: 'Accident Place',
            DateOfReport: 'Report Date',
            ID: 'ID',
            ProfilePic: 'Image'
        };
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.incidentCount = 0;
        $scope.selected = [];
        $scope.isSuperAdmin = true;
       
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StaffInfoId: 0,
            incident: 0,
            ParticipantName: '',
            PositionId: 0,
            AgencyId: localStorage.agencyId,
            FamilyId: localStorage.FamilyId,
            IsTimeClockUser: true,
            TimeZone:localStorage.TimeZone
        };
        // vm.getPositions = getPositions;
        vm.GetincidentList = GetincidentList;
        vm.editincident = editincident;
        vm.reset = reset;
        // vm.detailincidentInfo = detailincidentInfo;
        // getPositions();
        // // getincidentCount();
        GetincidentList();
        // getCategory();
        // getSessions();
        //    function getincidentCount() {
        //             vm.promise = CommonService.getincident($scope.query);
        //             vm.promise.then(function (response) {
        //                 if (response.Content.incidentList.length > 0) {
        //                     localStorage.TimeClockincidentCount=0;
        //                     localStorage.TimeClockincidentCount= eval(response.Content.incidentList.length);
        //                 }
        //             });
        //         };
        function getPositions() {
            vm.promise = CommonService.getPositions($scope.$storage.agencyId);
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.AllPosition = null;
                    vm.AllPosition = eval(response);
                }
            });
        };
        function GetincidentList() {
            $scope.ParticipantName = $scope.query.ParticipantName == '' ? "All" : $scope.query.ParticipantName;
            $scope.StaffName = $scope.query.StaffInfoId == "" ? "All" : $scope.query.StaffInfoId;
            if($scope.StaffName > 0){
                vm.AllStaff.forEach(function(element) {
             if($scope.StaffName==element.ID)
             {
                 $scope.StaffName=element.FirstName +" "+ element.LastName;
             }   
            });
            }
            
            vm.promise = IncidentService.GetincidentListService($scope.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        for (var i = 0; i < response.Content.length; i++) {
                            if (response.Content[i].ImagePath == "") {
                                response.Content[i].ImagePath = "assets/images/avatars/avatar-5.png"

                            }
                        }
                        vm.NoData = false;
                        vm.incidentList = {};
                        vm.incidentList = response.Content;
                        vm.incidentCount = response.TotalRows;


                        $scope.selected = [];
                    }
                    else {
                        vm.NoData = true;
                        vm.incidentList = [];
                        vm.incidentCount = 0;
                        NotificationMessageController('No Incident list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get incident list at the moment. Please try again after some time.');
                }
                vm.close();
            });



        };
         getAllStaff();
            function getAllStaff() {
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllStaff(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllStaff = null;
                    vm.AllStaff = eval(response.Content);
                }
            });
        };

        function viewDetails(incidentId) {
            if(localStorage.Portal=="AGENCY")
            $state.go('triangular.incidentdetails', { objIncident: incidentId });
            else
            $state.go('triangular.parentportalincidentdetails', { objIncident: incidentId });
        }
       
        //    $scope.CompletedEvent = function (scope) {
        //     console.log("Completed Event called");
        // };

        // $scope.ExitEvent = function (scope) {
        //     console.log("Exit Event called");
        // };

        // $scope.ChangeEvent = function (targetElement, scope) {
        //     console.log("Change Event called");
        //     console.log(targetElement);  //The target element
        //     console.log(this);  //The IntroJS object
        // };

        // $scope.BeforeChangeEvent = function (targetElement, scope) {
        //     console.log("Before Change Event called");
        //     console.log(targetElement);
        // };

        // $scope.AfterChangeEvent = function (targetElement, scope) {
        //     console.log("After Change Event called");
        //     console.log(targetElement);
        // };
        // introJs().start();
        // $scope.IntroOptions = {
        // steps: [
        //     {
        //         element: document.querySelector('#step1'),
        //         intro: "<b>Add Family to start.</b>",
        //         position: 'bottom'
        //     },
        //     {
        //         element: document.querySelectorAll('#step2')[0],
        //         intro: "<b>Add participants into family registeration.</b>",
        //         position: 'bottom'
        //     },
        //     {
        //         element: document.querySelector('#step3'),
        //         intro: "<b>Add incident to classes you've registered.</b>",
        //         position: 'bottom'
        //     },
        //     {
        //         element: document.querySelector('#step4'),
        //         intro: "<b>You can see the signed in participants here!</b>",
        //         position: 'bottom'

        //     },
        //     {
        //         element: document.querySelector('#step5'),
        //         intro: "<b>You are all set to go!</b>",
        //         position: 'bottom'

        //     },


        // ],
        //      showStepNumbers: true,
        //      exitOnOverlayClick: true,
        //      exitOnEsc: true,
        //     // nextLabel: '<i class="fa fa-hand-o-right" aria-hidden="true"></i>',
        //     // prevLabel: '<i class="fa fa-hand-o-left" aria-hidden="true"></i>',
        //     skipLabel: 'Exit',
        //     doneLabel: 'Go!'
        // };
        vm.editincident = editincident;
        function editincident(incident) {
            if(localStorage.Portal=="AGENCY")
            $state.go('triangular.updateincident', { objIncidentData: incident });
            else
            $state.go('triangular.parentupdateincident', { objIncidentData: incident });
            
        };
        function showDeleteIncidentConfirmDialoue(event, data) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this Incident Report permanently?')
                //   .textContent('All of this branch data and all "Branch Managers/Delivery Personals" associated with this class  will be deleted.')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteincident(data);
            }, function () {
                $scope.hide();
            });
        }
        function deleteincident(data) {
            vm.promise = IncidentService.DeleteIncidentById(data);
            vm.promise.then(function (response) {
                // alert(response.IsSuccess);
                if (response.IsSuccess == true) {
                    NotificationMessageController('Incident deleted successfully.');
                    GetincidentList();
                }
                else {

                    NotificationMessageController('Unable to get incident list at the moment. Please try again after some time.');
                }
            });
        };
        function setincidentEndDate(DateVal) {
            vm.disabledincidentEndDatePicker = false;
            vm.minincidentenddate = new Date(DateVal);
            $scope.query.classenddate = null;


        };
        function detailincidentInfo($event, incident) {
            incidentService.SetId(incident);
            incident.RedirectFrom = "incidentdetails";

            $state.go("triangular.incidentdetails", { obj: incident });
        };
        function reset() {
            
            $scope.query.StaffInfoId = 0;
            $scope.query.ParticipantName = '';
            GetincidentList();
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }


})();