(function () {
    'use strict';

    angular
        .module('app.incident')
        .controller('AddIncidentController', AddIncidentController);

    /* @ngInject */
    function AddIncidentController($scope, $http, $state, $log, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, AgencyService, $localStorage, imageUploadService, notificationService) {

        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.changed = function () {
            $log.log('Time changed to: ' + $scope.mytime);
        };

        $scope.clear = function () {
            $scope.mytime = null;
        };
        $scope.currentDate = new Date();


        var vm = this;
        vm.ImageUrlPath = HOST_URL.url;
        $scope.$storage = localStorage;
        if (localStorage.agencyId == undefined || null) {
            $state.go('authentication.login');
            notificationService.displaymessage("You must login first.");
            return;
        }
        localStorage.ParticipantAttendancePage = false;
        // vm.Incident = {
        //     AgencyName:''
        // };
        vm.ParticipantIds = {
            StudentInfoId: [],
            OtherStudentInfoId: [],
        };
        vm.AgencyName = localStorage.AgencyName;
        vm.IsVisibleActionButton = false;
        if (localStorage.FamilyId != null || localStorage.FamilyId != undefined)
            vm.IsVisibleActionButton = true;
        vm.status = 'idle';  // idle | uploading | complete
        vm.Title = "Add Incident Information";
        vm.Incident = {
            FirstAidAdministered: true
        };
        vm.addIncident = addIncident;
        vm.showProgressbar = false;
        // vm.GetPositions = GetPositions;
        // vm.ResetFields = ResetFields();
        vm.queryContacts = queryContacts;
        $scope.id = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            staff: 0,
            name: '',
            PositionId: 0,
            AgencyId: $scope.$storage.agencyId,
            IsTimeClockUser: true
        };
        vm.selectedVegetables = [];

        vm.autocompleteDemoRequireMatch = true;
        vm.readonly = false;
        vm.selectedItem = null;
        vm.selectedItemOther = null;
        vm.searchText = null;
        vm.querySearch = querySearch;
        vm.querySearchOther = querySearchOther;
        vm.loadParticipants = loadParticipants();
        vm.selectedParticipants = [];
        vm.numberChips = [];
        vm.numberChips2 = [];
        vm.numberBuffer = '';
        vm.autocompleteDemoRequireMatch = true;
        vm.getParentInformation = getParentInformation;
        vm.remove = remove;
        vm.stateChange = stateChange;
        vm.InvolvedParticipant = InvolvedParticipant;
        vm.maxDate = new Date();
        vm.Incident = {
            ListStudentInfoIds: [],
            ListOtherStudentInfoIds: [],
            ListParentInfoIds: [],
            TimeOfAccident: null,
            DateOfAccident: new Date()
        }

        /**
         * Search for vegetables.
         */
        function querySearch(query) {
            var results = query ? vm.AllParticipant.filter(createFilterFor(query)) : vm.AllParticipant;
            return results;
        }
        function querySearchOther(query) {
            var results = query ? vm.AllParticipantOthers.filter(createFilterForOther(query)) : vm.AllParticipantOthers;
            return results;
        }

        /**
         * Create filter function for a query string
         */
        function searchTextChange(text) {
            $log.info('Text changed to ' + text);
        }

        function selectedItemChange(item) {
            $log.info('Item changed to ' + JSON.stringify(item));
        }
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(participant) {
                return (participant._lowername.indexOf(lowercaseQuery) === 0);
            };

        }
        vm.AllOtherParticipant = [];
        function InvolvedParticipant(obj) {

            var model = {
                agencyId: localStorage.agencyId,
                StudentId: obj.studentId
            }
            vm.promise = CommonService.getAllOtherParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllOtherParticipant = null;
                    vm.AllOtherParticipant = eval(response.Content);

                }
            });


        }
        function createFilterForOther(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(participant) {
                return (participant._lowername.indexOf(lowercaseQuery) === 0);
            };

        }

        function stateChange() {
            if (vm.Incident.ParentInformed == "true") {
                vm.disable = true;
            }
            else if (vm.Incident.ParentInformed == "false") {
                vm.disable = false;
            }

        }

        function remove() {
            vm.ParentName = "";
            vm.PhoneNumber = "";
            vm.ParentId = "";
        }
        function getParentInformation(obj) {

            // if (vm.ParticipantIds.StudentInfoId.length > 1) {
            //     vm.ParticipantIds.StudentInfoId.remove(obj);
            //     NotificationMessageController('More than one participant is not allowed.');
            //     return;
            // }


            vm.Incident.AgeOfChild = obj.Age;
            var model = {
                StudentId: obj.studentId
            }

            vm.promise = CommonService.getParentInformationByStudent(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.ParentInfo = null;
                    vm.ParentInfo = eval(response.Content);
                    vm.ParentName = vm.ParentInfo[0].Name;
                    vm.PhoneNumber = vm.ParentInfo[0].PhoneNumber;
                    vm.ParentId = vm.ParentInfo[0].ParentId;
                    $scope.EmailId = vm.ParentInfo[0].Email;
                    vm.FamilyId = vm.ParentInfo[0].FamilyId;
                }
            });
            vm.AllParticipantOthers = angular.copy(vm.AllParticipant);
            for (var i = 0; i <= vm.AllParticipantOthers.length; i++) {
                if (vm.AllParticipantOthers[i].studentId == obj.studentId) {
                    vm.AllParticipantOthers.splice(i, 1);
                    break;
                }
            }
        }
        vm.AllParticipant = [];
        vm.AllParticipantOri = [];
        vm.AllParticipantOthers = [];
        vm.AllParticipantOthersOri = [];
        function loadParticipants() {
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllParticipant = null;
                    vm.AllParticipant = eval(response.Content);
                    vm.AllParticipantOthers = eval(response.Content);
                }
            });


        }

        //getParticipants();
        getAllStaff();
        function getParticipants() {
            var model = {
                agencyId: localStorage.agencyId,
                Date: new Date()
            }
            vm.promise = CommonService.getAllParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllParticipant = null;
                    vm.AllParticipant = eval(response.Content);
                }
            });
        };
        function getAllStaff() {
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllStaff(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllStaff = null;
                    vm.AllStaff = eval(response.Content);
                }
            });
        };

        function queryContacts($query) {
            var lowercaseQuery = angular.lowercase($query);
            return vm.AllParticipant.filter(function (contact) {
                var lowercaseName = angular.lowercase(contact.name);
                if (lowercaseName.indexOf(lowercaseQuery) !== -1) {
                    return contact;
                }
            });
        }
        function addIncident(Obj) {
            var chechModel = {};
            chechModel.StudentId = vm.selectedItem.studentId;
            chechModel.DateOfAccident = vm.Incident.DateOfAccident;
            $http.post(HOST_URL.url + '/api/common/CheckParticipantAttedance', chechModel).then(function (response) {
                if (response.data.IsSuccess == true) {
                    if (response.data.IsAttendee == true) {
                        if (Obj.$valid) {
                            vm.showProgressbar = true;
                            vm.Incident.AgencyRegistrationId = $scope.$storage.agencyId;
                            vm.Incident.ParentId = vm.ParentId;
                            vm.Incident.FamilyId = vm.FamilyId;
                            vm.selectedItem;
                            vm.ParentInfo.forEach(function (element) {
                                vm.Incident.ListParentInfoIds.push({
                                    'ParentInfoId': element
                                        .ParentId
                                });
                            }, this);
                            vm.Incident.ListStudentInfoIds.push({
                                'studentId': vm.selectedItem.studentId
                            });

                            vm.ParticipantIds.OtherStudentInfoId.forEach(function (element) {
                                vm.Incident.ListOtherStudentInfoIds.push({
                                    'studentId': element
                                        .studentId
                                });
                            }, this);
                            var model = {};
                            model = vm.Incident;
                            // model.DataOfAccident= (moment(model.DateOfAccident).format('MM/DD/YYYY H:mm Z'));
                            model.EmailId = $scope.EmailId;
                            model.TimeOfAccident = (moment(model.TimeOfAccident).format('HH:mm:ss'));
                            model.IsRead = false;
                            model.TimeZone = localStorage.TimeZone;
                            $http.post(HOST_URL.url + '/api/Incident/AddIncident', model).then(function (response) {
                                if (response.data.IsSuccess == true) {
                                    vm.showProgressbar = false;
                                    NotificationMessageController(response.data.ReturnMessage[0]);
                                    // ResetFields();
                                    $state.go('triangular.incident');
                                }
                                else {
                                    vm.showProgressbar = false;
                                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                                }
                            });
                        }
                    }
                    else {
                        NotificationMessageController("Sorry,Participant have no attendance for the day");
                        return;
                    }

                }
                else {
                    NotificationMessageController("Unable to check whether participant is attendee or not for that day");
                }
            });

        }

        function ResetFields() {
            vm.staff = {};

        };


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

})();