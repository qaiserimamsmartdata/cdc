(function () {
    'use strict';

    angular
        .module('app.incident')
        .controller('UpdateIncidentController', UpdateIncidentController);


    /* @ngInject */
    function UpdateIncidentController($filter, notificationService, $scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, StaffService, CommonService, AgencyService, $localStorage, imageUploadService, IncidentService) {
        if ($stateParams.objIncidentData == null || $stateParams.objIncidentData == undefined) {
            if (localStorage.Portal == 'AGENCY')
                $state.go("triangular.incident");
            else
                $state.go("triangular.parentincident");
            return;
        }
        var vm = this;
        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };
        function stateChange() {
            if (vm.Incident.ParentInformed == "true") {
                vm.disable = true;
            }
            else if (vm.Incident.ParentInformed == "false") {
                vm.disable = false;
            }

        }
        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.changed = function () {
            $log.log('Time changed to: ' + $scope.mytime);
        };
        function searchTextChange(text) {
            $log.info('Text changed to ' + text);
        }
        $scope.clear = function () {
            $scope.mytime = null;
        };
        $scope.currentDate = new Date();

        vm.stateChange = stateChange;

        vm.ImageUrlPath = HOST_URL.url;
        $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        var data = $stateParams.objIncidentData;

        // vm.Incident = {
        //     AgencyName:''
        // };
        vm.ParticipantIds = {
            StudentInfoId: [],
            OtherStudentInfoId: [],
        };
        vm.AgencyName = localStorage.AgencyName;
        vm.IsVisibleActionButton = false;
        if (localStorage.FamilyId != null || localStorage.FamilyId != undefined)
            vm.IsVisibleActionButton = true;
        vm.status = 'idle';  // idle | uploading | complete
        vm.Title = "Update Incident Information";
        vm.Incident = {};
        vm.updateIncident = updateIncident;
        vm.showProgressbar = false;
        // vm.GetPositions = GetPositions;
        // vm.ResetFields = ResetFields();
        vm.queryContacts = queryContacts;
        $scope.id = 0;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            staff: 0,
            name: '',
            PositionId: 0,
            AgencyId: $scope.$storage.agencyId,
            IsTimeClockUser: true
        };
        vm.selectedVegetables = [];

        vm.autocompleteDemoRequireMatch = true;
        vm.readonly = false;
        vm.selectedItem = null;
        vm.selectedItemOther = null;
        vm.searchText = null;
        vm.querySearch = querySearch;
        vm.loadParticipants = loadParticipants;
        vm.selectedParticipants = [];
        vm.numberChips = [];
        vm.numberChips2 = [];
        vm.numberBuffer = '';
        vm.autocompleteDemoRequireMatch = true;
        vm.getParentInformation = getParentInformation;
        vm.remove = remove;
        vm.maxDate = new Date();
        /**
         * Search for vegetables.
         */
        function querySearch(query) {
            var results = query ? vm.AllParticipant.filter(createFilterFor(query)) : vm.AllParticipant;
            return results;
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(participant) {
                return (participant._lowername.indexOf(lowercaseQuery) === 0);
            };

        }

        function remove() {
            vm.ParentName = "";
            vm.PhoneNumber = "";
            vm.ParentId = "";
        }
        function getParentInformation(obj) {
            vm.Incident.AgeOfChild = obj.Age;
            var model = {
                StudentId: obj.studentId
            }
            vm.promise = CommonService.getParentInformationByStudent(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.ParentInfo = null;
                    vm.ParentInfo = eval(response.Content);
                    vm.ParentName = vm.ParentInfo[0].Name;
                    vm.PhoneNumber = vm.ParentInfo[0].PhoneNumber;
                    vm.ParentId = vm.ParentInfo[0].ParentId;
                    $scope.EmailId = vm.ParentInfo[0].Email;
                    vm.FamilyId = vm.ParentInfo[0].FamilyId;
                }
            });
        }
        vm.AllParticipant = [];
        function loadParticipants() {
            var model = {
                agencyId: localStorage.agencyId,
                Date: new Date()
            }
            vm.promise = CommonService.getAllParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllParticipant = null;
                    vm.AllParticipant = eval(response.Content);
                    vm.AllParticipantOthers = angular.copy(vm.AllParticipant);
                    vm.selectedItem;
                    for (var i = 0; i <= vm.AllParticipantOthers.length; i++) {
                        if (vm.AllParticipantOthers[i].studentId == vm.selectedItem.studentId) {
                            vm.AllParticipantOthers.splice(i, 1);
                            break;
                        }
                    }

                }
            });
        }
        //getParticipants();
        getAllStaff();
        function getParticipants() {
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllParticipant = null;
                    vm.AllParticipant = eval(response.Content);
                }
            });
        };
        function getAllStaff() {
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllStaff(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllStaff = null;
                    vm.AllStaff = eval(response.Content);
                }
            });
        };

        function queryContacts($query) {
            var lowercaseQuery = angular.lowercase($query);
            return vm.AllParticipant.filter(function (contact) {
                var lowercaseName = angular.lowercase(contact.name);
                if (lowercaseName.indexOf(lowercaseQuery) !== -1) {
                    return contact;
                }
            });
        }
        vm.Incident = {
            ListStudentInfoIds: [],
            ListOtherStudentInfoIds: [],            
            TimeOfAccident: null
        }

        function updateIncident(Obj) {
            var chechModel = {};
            chechModel.StudentId = vm.selectedItem.studentId;
            chechModel.DateOfAccident = vm.Incident.DateOfAccident == null ? null : moment(vm.Incident.DateOfAccident).format('YYYY-MM-DD');
            $http.post(HOST_URL.url + '/api/common/CheckParticipantAttedance', chechModel).then(function (response) {
                if (response.data.IsSuccess == true) {
                    if (response.data.IsAttendee == true) {
                        if (Obj.$valid) {
                            vm.showProgressbar = true;
                            vm.selectedItem;
                            vm.Incident.AgencyRegistrationId = $scope.$storage.agencyId;
                            vm.Incident.ParentId = vm.ParentId;
                            vm.Incident.FamilyId = vm.FamilyId;
                            vm.Incident.ListStudentInfoIds = [];
                            vm.Incident.ListOtherStudentInfoIds = [];
                            vm.Incident.ListParentInfoIds= [];
                            // vm.Incident.DateOfAccident = new Date(vm.Incident.DateOfAccident == null ? null : moment(vm.Incident.DateOfAccident).format('YYYY-MM-DD'));
                            vm.Incident.DateOfAccident = new Date(vm.Incident.DateOfAccident);
                            vm.ParentInfo.forEach(function (element) {
                                vm.Incident.ListParentInfoIds.push({
                                    'ParentInfoId': element
                                        .ParentId
                                });
                            }, this);
                            vm.Incident.ListStudentInfoIds.push({
                                'studentId': vm.selectedItem.studentId
                            });

                            vm.ParticipantIds.OtherStudentInfoId.forEach(function (element) {
                                vm.Incident.ListOtherStudentInfoIds.push({
                                    'studentId': element
                                        .studentId,
                                    'ID': element.ID,
                                    'IncidentId': element.IncidentId
                                });
                            }, this);
                            var model = {};
                            model = vm.Incident;
                            model.EmailId = $scope.EmailId;
                            model.TimeOfAccident = (moment(model.TimeOfAccident).format('HH:mm:ss'));
                            model.IsRead = false;
                            model.TimeZone = localStorage.TimeZone;
                            $http.post(HOST_URL.url + '/api/Incident/AddIncident', model).then(function (response) {
                                if (response.data.IsSuccess == true) {
                                    vm.showProgressbar = false;
                                    // NotificationMessageController(response.data.ReturnMessage[0]);
                                    NotificationMessageController("Incident Information Updated Successfully");
                                    // ResetFields();
                                    $state.go('triangular.incident');
                                }
                                else {
                                    vm.showProgressbar = false;
                                    NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                                }
                            });
                        }
                    }
                    else {
                        NotificationMessageController("Sorry,Participant have no attendance for the day");
                        return;
                    }

                }
                else {
                    NotificationMessageController("Unable to check whether participant is attendee or not for that day");
                }
            });

        }
        vm.AssignData = AssignData;
        AssignData();
        vm.FirstAidAdministered = true;
        vm.querySearchOther = querySearchOther;


        function querySearchOther(query) {
            var results = query ? vm.AllParticipantOthers.filter(createFilterForOther(query)) : vm.AllParticipantOthers;
            return results;
        }
        function createFilterForOther(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(participant) {
                return (participant._lowername.indexOf(lowercaseQuery) === 0);
            };

        }
        function AssignData() {
            vm.Incident = data;

            vm.Incident.StaffInfoId = data.StaffInfoId.toString();
            vm.Incident.DescriptionOfInjury = data.DescriptionOfInjury;
            // vm.Incident.DateOfAccident=(new Date(moment(data.DateOfAccident).format('YYYY-MM-DD')));
            vm.Incident.DateOfAccident = (new Date(data.DateOfAccident));
            // vm.Incident.DoctorRequired = data.DoctorRequired;
            if (data.DoctorRequired == true) {
                vm.Incident.DoctorRequired = 'true';
            }
            else if (data.DoctorRequired == false) {
                vm.Incident.DoctorRequired = 'false';
            }
            if (data.FirstAidAdministered == true) {
                vm.Incident.FirstAidAdministered = 'true';
            }
            else if (data.FirstAidAdministered == false) {
                vm.Incident.FirstAidAdministered = 'false';
            }
            //vm.Incident.FirstAidAdministered = data.FirstAidAdministered;
            vm.Incident.OtherTreatment = data.OtherTreatment;
            vm.Incident.PlaceOfAccident = data.PlaceOfAccident;
            vm.Incident.DateOfAccident = new Date(data.DateOfAccident);
            vm.Incident.TimeOfAccident = data.TimeOfAccident;
            vm.Incident.ActionTaken = data.ActionTaken;
            vm.Incident.AgeOfChild = data.AgeOfChild;
            vm.Incident.NatureOfInjury = data.NatureOfInjury;
            vm.Incident.IsRead = data.IsRead;
            vm.Incident.ParentDescription = data.ParentDescription;
            if (data.ParentInformed == true) {
                vm.Incident.ParentInformed = 'true';
            }
            else if (data.ParentInformed == false) {
                vm.Incident.ParentInformed = 'false';
            }
            // vm.Incident.ParentInformed = data.ParentInformed;
            vm.ParticipantIds.StudentInfoId.push({ 'name': data.IncidentParticipant[0].StudentInfo.FirstName + ' ' + data.IncidentParticipant[0].StudentInfo.LastName, 'studentId': data.IncidentParticipant[0].StudentInfo.ID, 'ID': data.IncidentParticipant[0].ID, 'IncidentId': data.IncidentParticipant[0].IncidentId })
            var model = { 'Age': data.AgeOfChild, 'studentId': data.IncidentParticipant[0].StudentInfo.ID };
            getParentInformation(model);
            data.IncidentOtherParticipant.forEach(function (element) {
                vm.ParticipantIds.OtherStudentInfoId.push({ 'name': element.OtherStudentInfo.FirstName + ' ' + element.OtherStudentInfo.LastName, 'studentId': element.OtherStudentInfo.ID, 'ID': element.ID, 'IncidentId': element.IncidentId });
            }, this);
            loadParticipants();


            vm.selectedItem;
            vm.selectedItem = vm.ParticipantIds.StudentInfoId[0];
            //vm.Incident.DateOfAccident = data.DateOfAccident == null ? '' : (new Date(data.DateOfAccident));
            // vm.Incident.DateOfAccident = data.DateOfAccident == null ? '' : (new Date(moment(data.DateOfAccident).format('MM-DD-YYYY')));
            vm.Incident.TimeOfAccident = new Date(new Date().toDateString() + ' ' + data.TimeOfAccident);
        }


        function ResetFields() {
            vm.staff = {};

        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

})();