   angular
    .module('app')
    .directive('ngIncident', ['$http', 'HOST_URL','$localStorage', function (async, HOST_URL,$localStorage) {
      
        return {
            require: 'ngModel',
            
            link: function (scope, elem, attrs, ctrl) {
                elem.on('change', function (evt) {
                    if (attrs.ngIncident == 'chip') {
                        scope.$apply(function () {
                            var val =elem.val();
                            var data=true;
                           
                           if(val=="")
                           {
                                ctrl.$setValidity('incident',!data);
                           }
                           else
                           {
                               ctrl.$setValidity('incident',data);
                           }
                        
                        });
                    }                   
                });
            }
        }
    }]);