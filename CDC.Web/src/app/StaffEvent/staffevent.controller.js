(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('StaffEventController', StaffEventController);

    /* @ngInject */
    function StaffEventController($scope, $http, $localStorage, HOST_URL) {
        var vm = this;
        $scope.agencyId = localStorage.agencyId;
        localStorage.HostUrl=HOST_URL.url;
            $scope.IsLoaded = false;
        window.uploadDone = function () {
            $scope.IsLoaded = true;
        }
    }
})();