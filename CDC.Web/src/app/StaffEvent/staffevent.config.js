(function() {
    'use strict';

    angular
        .module('app.staffevent')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
          .state('triangular.staffevent', {
                url: '/staffevent',
                templateUrl: 'app/StaffEvent/staffevent.tmpl.html',
                // set the controller to load for this page
                controller: 'StaffEventController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewStaffEvent']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"STAFF",$window);
                    }
                }
            })
        
        ;

        triMenuProvider.addMenu({
            name: 'Events',
            icon: 'fa fa-calendar',
            type: 'link',
            permission: 'viewStaffEvent',
            state: 'triangular.staffevent',
            priority: 7

        });
    }
})();
