(function () {
    'use strict';

    angular
        .module('app.students')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider, $urlRouterProvider) {

        $stateProvider
            .state('triangular.students', {
                url: '/students',
                templateUrl: 'app/students/views/StudentList.tmpl.html',
                // set the controller to load for this page
                controller: 'StudentsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewStudents']
                    }
                }
            })
            .state('triangular.addstudents', {
                url: '/students/add',
                params: {
                    id: 0
                },
                templateUrl: 'app/students/views/addstudents.tmpl.html',
                // set the controller to load for this page
                controller: 'AddStudentsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                }
            })
            .state('triangular.updatestudent', {
                url: '/students/update',
                params: {
                    id: 0
                },
                templateUrl: 'app/students/views/addstudents.tmpl.html',
                // set the controller to load for this page
                controller: 'EditStudentsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                }
            })
            .state('triangular.studentdetails', {
                url: '/participant/details',
                templateUrl: 'app/students/studentdetails/views/enrolllist_details.tmpl.html',
                // set the controller to load for this page
                controller: 'StudentProfileController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                }
            })
            .state('triangular.parentstudentdetails', {
                url: '/parent/participant/details',
                templateUrl: 'app/students/studentdetails/views/enrolllist_details.tmpl.html',
                // set the controller to load for this page
                controller: 'StudentProfileController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                }
            });

        // triMenuProvider.addMenu({
        //     name: 'Participants',
        //     icon: 'md-cyan-theme material-icons zmdi zmdi-account-box',
        //     type: 'dropdown',
        //     priority: 1.1,
        //     children: [
        //         {
        //             name: 'Participant List',
        //             state: 'triangular.students',
        //             type: 'link'
        //         }
        //         // , {
        //         //     name: 'Add Participant',
        //         //     state: 'triangular.addstudents',
        //         //     type: 'link'
        //         // }
        //     ]
        // });
        // triMenuProvider.addMenu({
        //     name: 'Participants',
        //     state: 'triangular.students',
        //     icon: 'md-cyan-theme material-icons zmdi zmdi-account-box',
        //     type: 'link',
        //     permission: 'viewStudents',
        //     priority: 1.1,
        // });
    }
})();
