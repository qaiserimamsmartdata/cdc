(function () {
    'use strict';

    angular
        .module('app.students')
        .controller('EditStudentsController', EditStudentsController);

    /* @ngInject */
    function EditStudentsController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, StudentService) {
        var vm = this;
        vm.updateStudent = updateStudent;
        vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;
        vm.showProgressbar = false;
        $scope.id = $stateParams.id;
        vm.student = {
            Firstname: '',
            Lastname: '',
            Gender: '',
            Dateofbirth: '',
            Fathername: '',
            Mothername: '',
            Email: '',
            ParentsMobile: '',
            Guardianfirstname: '',
            Guardianlastname: '',
            Relation: '',
            Guardianemail: '',
            Guardianmobile: '',
            Address: '',
            Country: '',
            State: '',
            City: '',
            Postalcode: '',
            ClassID: '',
            Room: '',
            Startdate: '',
            Enddate: '',
            AdditionalInformation: ''
        };
        GetAllCountry();
        GetStudentByID();

        function GetStudentByID() {
            if ($stateParams.id == 0) {
                $state.go('triangular.students');
                return false;
            }
            else {
                vm.promise = StudentService.GetStudentbyIdService($stateParams.id);
                vm.promise.then(function (response) {
                    if (response.student != undefined && response.student != null) {
                        vm.student = response.student;
                        vm.student.Dateofbirth = new Date(response.student.Dateofbirth);
                        vm.student.Startdate = new Date(response.student.Startdate);
                        vm.student.Enddate = new Date(response.student.Enddate);
                        vm.student.ParentsMobile = parseFloat(response.student.ParentsMobile);
                        vm.student.Guardianmobile = parseFloat(response.student.Guardianmobile);
                        vm.student.Country = response.student.Country.toString();
                        vm.student.State = response.student.State.toString();
                        vm.student.Postalcode = parseFloat(response.student.Postalcode);
                        GetState();
                        $scope.selected = [];
                    }
                    else {
                        NotificationMessageController('Unable to get participant list at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function updateStudent(childinformationform, parentinformationform, guardianinformationform, contactinformationform, enrollmentinformationform) {
            if (childinformationform.$valid && parentinformationform.$valid) {
                var model = {
                    ID: $stateParams.id,
                    Firstname: childinformationform.Firstname.$modelValue,
                    Lastname: childinformationform.Lastname.$modelValue,
                    Gender: childinformationform.Gender.$modelValue,
                    Dateofbirth: childinformationform.Dateofbirth.$modelValue,
                    Fathername: parentinformationform.Fathername.$modelValue,
                    Mothername: parentinformationform.Mothername.$modelValue,
                    Email: parentinformationform.Email.$modelValue,
                    ParentsMobile: parentinformationform.ParentsMobile.$modelValue,
                    Guardianfirstname: guardianinformationform.Guardianfirstname.$modelValue == null ? '' : guardianinformationform.Guardianfirstname.$modelValue,
                    Guardianlastname: guardianinformationform.Guardianlastname.$modelValue == null ? '' : guardianinformationform.Guardianlastname.$modelValue,
                    Relation: guardianinformationform.Relation.$modelValue,
                    Guardianemail: guardianinformationform.Guardianemail.$modelValue == null ? '' : guardianinformationform.Guardianemail.$modelValue,
                    Guardianmobile: guardianinformationform.Guardianmobile.$modelValue == (null) ? '' : guardianinformationform.Guardianmobile.$modelValue,
                    Address: contactinformationform.Address.$modelValue == null ? '' : contactinformationform.Address.$modelValue,
                    Country: contactinformationform.Country.$modelValue,
                    State: contactinformationform.State.$modelValue,
                    City: contactinformationform.City.$modelValue,
                    Postalcode: contactinformationform.Postalcode.$modelValue,
                    ClassID: enrollmentinformationform.ClassID.$modelValue,
                    Room: enrollmentinformationform.Room.$modelValue,
                    Startdate: enrollmentinformationform.Startdate.$modelValue,
                    Enddate: enrollmentinformationform.Enddate.$modelValue,
                    AdditionalInformation: enrollmentinformationform.AdditionalInformation.$modelValue
                };

                $http.post(HOST_URL.url + '/api/Students/UpdateStudent', model).then(function (response) {

                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        // sessionService.setSession(response.data.Content);
                        NotificationMessageController('Participant updated Successfully.');
                        $state.go('triangular.students');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };

        // Common Services to call Country state and City

        function GetAllCountry() {
            $scope.id = $state.params.id;
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/country',
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllCountries = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        //NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to get country list at the moment.');
            });
        }

        function GetState() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/state',
                data: vm.student.Country
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllStates = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        NotificationMessageController('Unable to retreive state list at the moment.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to retreive state list at the moment.');
            });
        }

        function getCity() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/getstatescity',
                data: vm.student.State
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllCity = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        NotificationMessageController('Unable to retreive city list at the moment.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to retreive city list at the moment.');
            });
        }
    }
})();