(function () {
    'use strict';
    angular
        .module('app.students')
        .controller('AddStudentsController', AddStudentsController);

    /* @ngInject */
    function AddStudentsController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL) {
        var vm = this;
        vm.addStudent = addStudent;
        vm.showProgressbar = false;
        vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;
        vm.getCity = getCity;
        $scope.id = 0;
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate()-365);        
        vm.minBirthDate = new Date(bDate);
        vm.todayData = new Date();
        
        //for min 80years.
        var NewDate = new Date();
        NewDate= NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate)

        GetAllCountry();

        vm.student = {
            firstname: '',
            lastname: '',
            gender: '',
            dateofbirth: '',
            fathername: '',
            mothername: '',
            email: '',
            mobile: '',
            guardianfirstname: '',
            guardianlastname: '',
            relation: '',
            guardianemail: '',
            guardianmobile: '',
            address: '',
            country: '',
            state: '',
            city: '',
            postalcode: '',
            class: '',
            room: '',
            startdate: '',
            enddate: '',
            additionalInformation: ''
        };

        function GetAllCountry() {
            $scope.id = $state.params.id;
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/country',
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllCountries = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        //NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to retreive country list at the moment.');
            });
        }

        function GetState() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/state',
                data: vm.student.Country
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllStates = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        NotificationMessageController('Unable to retreive state list at the moment.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to retreive state list at the moment.');
            });
        }

        function getCity() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/getstatescity',
                data: vm.student.State
            };
            $http(req).then(function successCallback(response) {
                if (response.data.length > 0) {
                    vm.AllCity = eval(response.data);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        NotificationMessageController('Unable to retreive city list at the moment.');
                    }
                }
            }, function errorCallback(response) {
                NotificationMessageController('Unable to retreive city list at the moment.');
            });
        }

        function addStudent(childinformationform, parentinformationform, guardianinformationform, contactinformationform, enrollmentinformationform) {
            if (childinformationform.$valid && parentinformationform.$valid) {
                var model = {
                    Firstname: childinformationform.Firstname.$modelValue,
                    Lastname: childinformationform.Lastname.$modelValue,
                    Gender: childinformationform.Gender.$modelValue,
                    Dateofbirth: childinformationform.Dateofbirth.$modelValue,
                    Fathername: parentinformationform.Fathername.$modelValue,
                    Mothername: parentinformationform.Mothername.$modelValue,
                    Email: parentinformationform.Email.$modelValue,
                    ParentsMobile: parentinformationform.ParentsMobile.$modelValue,
                    Guardianfirstname: guardianinformationform.Guardianfirstname.$modelValue == null ? '' : guardianinformationform.Guardianfirstname.$modelValue,
                    Guardianlastname: guardianinformationform.Guardianlastname.$modelValue == null ? '' : guardianinformationform.Guardianlastname.$modelValue,
                    Relation: guardianinformationform.Relation.$modelValue,
                    Guardianemail: guardianinformationform.Guardianemail.$modelValue == null ? '' : guardianinformationform.Guardianemail.$modelValue,
                    Guardianmobile: guardianinformationform.Guardianmobile.$modelValue == null ? '' : guardianinformationform.Guardianmobile.$modelValue,
                    Address: contactinformationform.Address.$modelValue == null ? '' : contactinformationform.Address.$modelValue,
                    Country: contactinformationform.Country.$modelValue,
                    State: contactinformationform.State.$modelValue,
                    City: contactinformationform.City.$modelValue,
                    Postalcode: contactinformationform.Postalcode.$modelValue,
                    ClassID: enrollmentinformationform.ClassID.$modelValue,
                    Room: enrollmentinformationform.Room.$modelValue,
                    Startdate: enrollmentinformationform.Startdate.$modelValue,
                    Enddate: enrollmentinformationform.Enddate.$modelValue,
                    AdditionalInformation: enrollmentinformationform.AdditionalInformation.$modelValue
                };

                $http.post(HOST_URL.url + '/api/Students/AddStudent', model).then(function (response) {

                    if (response.data == 1) {
                        vm.showProgressbar = false;
                        // sessionService.setSession(response.data.Content);
                        NotificationMessageController('Participant added Successfully.');
                        $state.go('triangular.students');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                    }
                });
            }
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

    // function AddStudentsController2($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL) {
    //     var vm = this;
    //     vm.addStudent = addStudent;
    //     vm.showProgressbar = false;
    //     vm.GetAllCountry = GetAllCountry;
    //     vm.GetState = GetState;
    //     vm.getCity = getCity;

    //     GetAllCountry();


    //     vm.student = {
    //         firstname: '',
    //         lastname: '',
    //         gender: '',
    //         dateofbirth: '',
    //         fathername: '',
    //         mothername: '',
    //         email: '',
    //         mobile: '',
    //         guardianfirstname: '',
    //         guardianlastname: '',
    //         relation: '',
    //         guardianemail: '',
    //         guardianmobile: '',
    //         address: '',
    //         country: '',
    //         state: '',
    //         city: '',
    //         postalcode: '',
    //         class: '',
    //         room: '',
    //         startdate: '',
    //         enddate: '',
    //         additionalInformation: ''
    //     };

    //     function GetAllCountry() {
    //         var req = {
    //             method: 'POST',
    //             url: HOST_URL.url + '/api/Common/country',
    //         };
    //         $http(req).then(function successCallback(response) {
    //             if (response.data.length > 0) {
    //                 vm.AllCountries = eval(response.data);
    //             }
    //             else {
    //                 if (response.data.length == 0) {
    //                     NotificationMessageController('No branch manager found.');
    //                 }
    //                 else {
    //                     NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
    //                 }
    //             }
    //         }, function errorCallback(response) {
    //             NotificationMessageController('Unable to get branch managers at the moment. Please try again after some time.');
    //         });
    //     }

    //     function GetState() {
    //         var req = {
    //             method: 'POST',
    //             url: HOST_URL.url + '/api/Common/state',
    //         };
    //         $http(req).then(function successCallback(response) {
    //             if (response.data.length > 0) {
    //                 vm.AllStates = eval(response.data);
    //             }
    //             else {
    //                 if (response.data.length == 0) {
    //                     NotificationMessageController('No branch manager found.');
    //                 }
    //                 else {
    //                     NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
    //                 }
    //             }
    //         }, function errorCallback(response) {
    //             NotificationMessageController('Unable to get branch managers at the moment. Please try again after some time.');
    //         });
    //     }

    //     function getCity() {
    //         var req = {
    //             method: 'POST',
    //             url: HOST_URL.url + '/api/Common/getstatescity',
    //         };
    //         $http(req).then(function successCallback(response) {
    //             if (response.data.length > 0) {
    //                 vm.AllCity = eval(response.data);
    //             }
    //             else {
    //                 if (response.data.length == 0) {
    //                     NotificationMessageController('No branch manager found.');
    //                 }
    //                 else {
    //                     NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
    //                 }
    //             }
    //         }, function errorCallback(response) {
    //             NotificationMessageController('Unable to get branch managers at the moment. Please try again after some time.');
    //         });
    //     }

    //     function addStudent(registerStudent) {
    //         if (registerStudent.$valid) {
    //             var model = {
    //                 Firstname: registerStudent.firstname.$modelValue,
    //                 Lastname: registerStudent.lastname.$modelValue,
    //                 Gender: registerStudent.gender.$modelValue,
    //                 Dateofbirth: registerStudent.dateofbirth.$modelValue,
    //                 Fathername: registerStudent.fathername.$modelValue,
    //                 Mothername: registerStudent.mothername.$modelValue,
    //                 Email: registerStudent.email.$modelValue,
    //                 ParentsMobile: registerStudent.mobile.$modelValue,
    //                 Guardianfirstname: registerStudent.guardianfirstname.$modelValue,
    //                 Guardianlastname: registerStudent.guardianlastname.$modelValue,
    //                 Relation: registerStudent.relation.$modelValue,
    //                 Guardianemail: registerStudent.guardianemail.$modelValue,
    //                 Guardianmobile: registerStudent.guardianmobile.$modelValue,
    //                 Address: registerStudent.address.$modelValue,
    //                 Country: registerStudent.country.$modelValue,
    //                 State: registerStudent.state.$modelValue,
    //                 City: registerStudent.city.$modelValue,
    //                 Postalcode: registerStudent.postalcode.$modelValue,
    //                 ClassID: registerStudent.class.$modelValue,
    //                 Room: registerStudent.room.$modelValue,
    //                 Startdate: registerStudent.startdate.$modelValue,
    //                 Enddate: registerStudent.enddate.$modelValue,
    //                 AdditionalInformation: registerStudent.additionalInformation.$modelValue
    //             };

    //             $http.post(HOST_URL.url + '/api/Students/AddStudent', model).then(function (response) {

    //                 if (response.data == 1) {
    //                     vm.showProgressbar = false;
    //                     // sessionService.setSession(response.data.Content);
    //                     NotificationMessageController('Student added Successfully !');
    //                     $state.go('triangular.students');
    //                 }
    //                 else {
    //                     vm.showProgressbar = false;
    //                     NotificationMessageController('Unable to add at the moment. Please try again after some time.');
    //                 }
    //             });
    //         }
    //     }

    //     function NotificationMessageController(message) {
    //         $timeout(function () {
    //             $rootScope.$broadcast('newMailNotification');
    //             $mdToast.show({
    //                 template: '<md-toast><span flex>' + message + '</span></md-toast>',
    //                 position: 'bottom right',
    //                 hideDelay: 5000
    //             });
    //         }, 100);
    //     };
    // }
})();