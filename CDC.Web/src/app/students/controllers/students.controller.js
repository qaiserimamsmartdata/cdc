(function () {
    'use strict';

    angular
        .module('app.students')
        .controller('StudentsController', StudentsController);

    /* @ngInject */
    function StudentsController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdSidenav, $log, $mdToast, $rootScope, $q, StudentService, filerService) {
        var vm = this;
        vm.GetStudentList = GetStudentList;
        vm.editStudent = editStudent;
        vm.showDeleteStudentDialoue = showDeleteStudentDialoue;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.columns = {
            Name: 'NAME',
            Class: 'CLASS',
            Room: 'ROOM',
            Gender: 'GENDER',
            BirthDate: 'DATE OF BIRTH',
            Startdate: 'START DATE',
            Enddate: 'END DATE',
            ID: 'ID'
        };

        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.studentCount = 0;
        $scope.selected = [];
        $scope.isSuperAdmin = true;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            studentclass: 0,
            name: ''
        };
        GetStudentList();
        function GetStudentList() {
            vm.promise = StudentService.GetStudentListService($scope.query);
            vm.promise.then(function (response) {
                if (response.studentList.length > 0) {
                    vm.studentList = response.studentList;
                    vm.studentCount = response.studentList.length;
                    $scope.selected = [];
                    vm.NoData = false;
                    
                }
                else {
                    if (response.studentList.length == 0) {
                        vm.NoData = true;
                        vm.studentList = [];
                        vm.studentCount = 0;
                        NotificationMessageController('No Participant list found.');
                    }
                    else {
                        NotificationMessageController('Unable to get participant list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        };

        function editStudent(studentID) {
            $state.go('triangular.updatestudent', { id: studentID });
        }

        function showDeleteStudentDialoue(event, Id) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this participant?')
                .textContent('All of this participant data will be deleted.')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                DeleteStudentById(Id);
            }, function () {
                $scope.hide();
            });
        }

        function DeleteStudentById(Id) {
            vm.promise = StudentService.DeleteStudentbyIdService(Id);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('participant deleted Successfully.');
                    GetStudentList();
                }
                else {
                    NotificationMessageController('Error occured while deleting, Please try again later.');
                }
            });


            // var req = {
            //     method: 'POST',
            //     url: HOST_URL.url + '/api/Students/DeleteStudentById',
            //     data: { StudentId: StudentId },
            //     headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            // };
            // $http(req).then(function successCallback(response) {
            //     if (response.data.IsSuccess == true) {
            //         $scope.selected = [];
            //         GetStudentList();
            //         NotificationMessageController('Deleted Successfully.');
            //     }
            //     else {
            //         NotificationMessageController(response.data.Message);
            //     }
            // }, function errorCallback() {
            //     NotificationMessageController('Unable to delete student at the moment. Please try again after some time.');
            // });
        }
        function detailStudentInfo($event, data) {
            StudentService.SetId(data.ID);
             if(localStorage.Portal=='AGENCY')
            $state.go('triangular.studentdetails', { obj: data });
            else
            $state.go('triangular.parentstudentdetails', { obj: data });
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
})();