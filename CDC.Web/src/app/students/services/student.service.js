﻿(function () {
    'use strict';

    angular
        .module('app.students')
        .factory('StudentService', StudentService);

    StudentService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function StudentService($http, $q, HOST_URL) {

        return {
            GetStudentListService: GetStudentListService,
            GetStudentbyIdService: GetStudentbyIdService,
            DeleteStudentbyIdService: DeleteStudentbyIdService
        };

        ////////////////

        function GetStudentListService(model) {
            var deferred = $q.defer();

            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Students/GetAllStudents', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            })
                .error(function (errResp) {
                    deferred.reject({ message: "Really bad." });
                });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        }

        function GetStudentbyIdService(id) {
            var deferred = $q.defer();

            $http.post(HOST_URL.url + 'api/Students/GetStudentById/', id).success(function (response, status, headers, config) {
                deferred.resolve(response);
            })
                .error(function (errResp) {
                    deferred.reject({ message: "Really bad" });
                });
            return deferred.promise;
        }

        function DeleteStudentbyIdService(StudentId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + 'api/Students/DeleteStudentById/', StudentId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            })
                .error(function (errResp) {
                    deferred.reject({ message: "Really bad" });
                });
            return deferred.promise;
        }
    }
})();