(function () {
    'use strict';

    angular
        .module('app.students')
        .controller('BillingAndFeesDetailsController', BillingAndFeesDetailsController)
        .controller('AddPaymentController', AddPaymentController);
    //For Showing student attendance history details -Tulesh
    function BillingAndFeesDetailsController($scope, $rootScope, $mdDialog, $state, $stateParams, $localStorage, notificationService, filerService, apiService, HOST_URL, CommonService, $filter) {
        var vm = this;
        if ($stateParams.obj == null || $stateParams.obj == undefined) {
            $state.go('triangular.allparticipants');
            return;
        }
        var studentId;
        if ($stateParams.obj.StudentId != null || $stateParams.obj.StudentId != undefined) {
            studentId = $stateParams.obj.StudentId;
        }
        else {
            studentId = $stateParams.obj.Student.ID;
        }

        vm.toggleRight1 = filerService.toggleRight1();
        vm.isOpenRight1 = filerService.isOpenRight1();
        vm.close1 = filerService.close1();
        vm.goToPayment = goToPayment;
        vm.reset = reset;
        //For Family Portal
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || null)
            vm.FamilyId = localStorage.FamilyId;
        vm.IsEnableForStaff = 0;
        if (localStorage.Portal == 'STAFF')
            vm.IsEnableForStaff = 1;
        ///////////
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-ScheduleDate',
            page: 1,
            status: 0,
            FromDate: new Date((new Date()).setMonth((new Date()).getMonth() - 1)),
            ToDate: new Date(),
            ParticipantName: '',
            AgencyId: localStorage.agencyId,
            StudentId: studentId,
            TimeZoneName: localStorage.TimeZone,
            PaymentStatus: null,
            ClassId: 0
        };
        vm.studentPaymentHistoryListCount = 0;
        vm.NoToDoData = false;
        vm.getFeesUnpaidList = getFeesUnpaidList;
        vm.getFeesUnpaidListSuccess = getFeesUnpaidListSuccess;
        vm.failed = failed;
        // vm.setStartDate = setStartDate;
        vm.MaxDate = new Date();
        vm.setToDate = setToDate;
        function setToDate() {
            $scope.query.ToDate = new Date();
        }
        GetClass_Room_Lesson();
        getFeesUnpaidList();
        $rootScope.$on("getFeesUnpaidList", function () {
            getFeesUnpaidList();
        });
        function getFeesUnpaidList() {
            if ($scope.query.ClassId > 0) {
                var ClassData = $filter('filter')(vm.AllClasses, { ID: $scope.query.ClassId });
                $scope.ClassName = ClassData[0].ClassName;
            }
            else {
                $scope.ClassName = "All";
            }
            $scope.ParticipantName = $scope.query.ParticipantName == "" ? "All" : $scope.query.ParticipantName;
            $scope.PaymentStatus = $scope.query.PaymentStatus == null ? "All" : $scope.query.PaymentStatus == 0 ? 'Unpaid' : 'Paid';
            $scope.FromDate = $scope.query.FromDate;
            $scope.ToDate = $scope.query.ToDate;
            vm.promise = apiService.post('/api/StudentSchedule/GetUnpaidHistory', $scope.query,
                getFeesUnpaidListSuccess,
                failed);
        }
        function getFeesUnpaidListSuccess(response) {
            if (response.data.ReturnStatus == true) {
                if (response.data.Content.length > 0) {
                    vm.NoToDoData = false;
                    vm.studentPaymentHistoryList = response.data.Content;
                    vm.ClassName = vm.studentPaymentHistoryList[0].ClassName;
                    vm.StudentId = vm.studentPaymentHistoryList[0].StudentId;
                    vm.Discount = vm.studentPaymentHistoryList[0].PaidFees == null ? 0 : vm.studentPaymentHistoryList[0].PaidFees;
                    vm.Fees = vm.studentPaymentHistoryList[0].ClassInfo.Fees == null ? 0 : vm.studentPaymentHistoryList[0].ClassInfo.Fees;
                    vm.FeeTypeId = vm.studentPaymentHistoryList[0].ClassInfo.FeeTypeId == null ? 0 : vm.studentPaymentHistoryList[0].ClassInfo.FeeTypeId;
                    vm.PaymentStatusList = vm.studentPaymentHistoryList[0].PaymentStatusList;
                    vm.studentPaymentHistoryListCount = response.data.TotalRows;
                }
                else {
                    vm.studentPaymentHistoryList = null;
                    vm.ClassName = '';
                    vm.StudentId = 0;
                    vm.ParticipantName = '';
                    vm.Discount = 0;
                    vm.Fees = 0;
                    vm.FeeTypeId = 0;
                    vm.PaymentStatusList = null;
                    vm.studentPaymentHistoryListCount = 0;
                    notificationService.displaymessage('No family payment history found.');
                    vm.NoToDoData = true;
                }
            }
            vm.close1();
        }
        function failed() {
            notificationService.displaymessage('Unable to get list at the moment. Please try again after some time.');
            vm.studentPaymentHistoryList = null;
            vm.ClassName = '';
            vm.StudentId = 0;
            vm.ParticipantName = '';
            vm.Fees = 0;
            vm.FeeTypeId = 0;
            vm.PaymentStatusList = null;
            vm.close1();

        }
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }
        function setStartDate(DateVal) {
            vm.disabled = false;
            var tempDate = new Date(DateVal);
            vm.attendanceendDate = tempDate;
        };
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function goToPayment(data) {

            $mdDialog.show({
                controller: AddPaymentController,
                controllerAs: 'vm',
                templateUrl: 'app/students/studentdetails/views/details/pages/paymentfees.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                items: { paymentdata: data, ClassName: vm.ClassName, StudentId: vm.StudentId, Fees: vm.Fees, FeeTypeId: vm.FeeTypeId, Discount: vm.Discount }
            });
        }

        function reset() {
            $scope.query.ParticipantName = '';
            $scope.query.PaymentStatus = null;
            $scope.query.FromDate = new Date((new Date()).setMonth((new Date()).getMonth() - 1));
            $scope.query.ToDate = new Date();
            $scope.query.ClassId = 0;
            getFeesUnpaidList();
        }
    }
    function AddPaymentController($scope, $mdDialog, $rootScope, $state, $stateParams, $localStorage, notificationService, filerService, apiService, HOST_URL, CommonService, items) {
        var vm = this;
        vm.cancelClick = cancelClick;
        vm.ClassName = items.ClassName;

        vm.getParents = getParents;
        vm.Fees = items.Fees;
        vm.Discount = items.Discount;
        vm.PaidAmount = items.Fees;
        vm.FeeTypeId = items.FeeTypeId.toString();
        vm.CalculateTotalAmount = CalculateTotalAmount;
        vm.PaymentMode = "Cash";
        getParents();
        CalculateTotalAmount(vm.Discount);
        function CalculateTotalAmount(discAmt) {
            if (discAmt > vm.Fees) {
                vm.Discount = "";
                vm.PaidAmount = "";
                notificationService.displaymessage("Discount cannot be more than Fee amount.")
                return;
            }
            vm.PaidAmount = vm.Fees - discAmt;
        }
        function getParents() {
            var model = {
                ID: items.StudentId
            }
            vm.promise = CommonService.getParentListByStudentId(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.ParentList = null;
                    vm.ParentList = response.Content;
                }
            });
        };
        function cancelClick() {
            $mdDialog.cancel();
        }

        vm.updateFeesDetails = updateFeesDetails;
        vm.updateFeesDetailsSuccess = updateFeesDetailsSuccess;
        vm.failed = failed;
        // // vm.setStartDate = setStartDate;


        // getFeesUnpaidList();

        function updateFeesDetails() {
            items.paymentdata.Fees = vm.Fees;
            items.paymentdata.Discount = vm.Discount;
            items.paymentdata.PaymentMode = vm.PaymentMode;
            items.paymentdata.Discription = vm.Discription;
            items.paymentdata.ParentInfoId = vm.ParentId;
            items.paymentdata.ParentInfoId = vm.FeeTypeId;
            items.paymentdata.PaymentStatus = 1;
            apiService.post('/api/StudentSchedule/UpdateEnrollPaymentInfo', items.paymentdata,
                updateFeesDetailsSuccess,
                failed);
        }
        function updateFeesDetailsSuccess(response) {
            if (response.data.ReturnStatus == true) {
                $rootScope.$emit("getFeesUnpaidList", {});
                notificationService.displaymessage('Updated Successfully.');
                cancelClick();
            }
            else {
                notificationService.displaymessage('Unable to update at the moment..');
            }
        }
        function failed() {
            notificationService.displaymessage('No list found.');
        }
        // function setStartDate(DateVal) {
        //     vm.disabled = false;
        //     var tempDate = new Date(DateVal);
        //     vm.attendanceendDate = tempDate;
        // };
        // $scope.hide = function () {
        //     $mdDialog.hide();
        // };

        // $scope.cancel = function () {
        //     $mdDialog.cancel();
        // };
        // function goToPayment(data) {

        //     $mdDialog.show({
        //         controller: AddPaymentController,
        //         controllerAs: 'vm',
        //         templateUrl: 'app/students/studentdetails/views/details/pages/paymentfees.tmpl.html',
        //         parent: angular.element(document.body),
        //         escToClose: true,
        //         clickOutsideToClose: true,
        //         fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
        //         items: { paymentdata: data }
        //     });
        // }

        // function reset() {
        //     $scope.query.ParticipantName = '';
        //     $scope.query.ClassId = 0;
        //     $scope.query.FromDate = new Date(),
        //         $scope.query.ToDate = new Date(),
        //         getstudentPaymentHistoryList();
        // }
    }
})();