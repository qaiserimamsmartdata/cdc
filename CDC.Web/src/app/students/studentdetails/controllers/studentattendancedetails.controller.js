(function () {
    'use strict';

    angular
        .module('app.students')
        .controller('StudentAttendanceDetailsController', StudentAttendanceDetailsController);
    //For Showing student attendance history details -Tulesh
    function StudentAttendanceDetailsController($scope, $mdDialog, $state, $stateParams, $localStorage,
        notificationService, filerService, apiService, HOST_URL,$filter, CommonService) {
        var vm = this;
        var studentId;
        if ($stateParams.obj == null || $stateParams.obj == undefined) {
            $state.go('triangular.allparticipants');
            return;
        }
        if ($stateParams.obj.StudentId != null || $stateParams.obj.StudentId != undefined) {
            studentId = $stateParams.obj.StudentId;
        }
        else {
            studentId = $stateParams.obj.Student.ID;
        }
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        vm.disabled = true;
        vm.attendancestartDate = new Date();
        vm.ImageUrlPath = HOST_URL.url;
        vm.columns = {
            Image: 'Image',
            StudentName: 'Participant',
            ClassName: 'Class Name',
            AttendanceDate: 'Attendance Date',
            DropedByName: 'Sign In By',
            InTime: 'Sign In Time',
            PickupByName: 'Sign Out By',
            OutTime: 'Sign Out Time'
        };

        $scope.query = {
            filter: '',
            limit: '10',
            order: '-AttendanceDate',
            page: 1,
            status: 0,
            FromDate: new Date(),
            ToDate: new Date(),
            ParticipantName: '',
            AgencyId: localStorage.agencyId,
            StudentId: studentId,
            TimeZoneName: localStorage.TimeZone,
            ClassId: 0
        };
        vm.getStudentAttendanceHistoryList = getStudentAttendanceHistoryList;
        vm.getStudentAttendanceHistoryListSuccess = getStudentAttendanceHistoryListSuccess;
        vm.failed = failed;
        vm.setStartDate = setStartDate;
        vm.setFromDate = setFromDate;
        vm.MaxDate = new Date();
        vm.setToDate = setToDate;
        function setToDate() {
            $scope.query.ToDate = new Date();
        }
        GetClass_Room_Lesson();

        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }

        getStudentAttendanceHistoryList();
        vm.studentattendanceHistoryCount = 0;
        function getStudentAttendanceHistoryList() {
              if ($scope.query.ClassId > 0) {
                var ClassData = $filter('filter')( vm.AllClasses, { ID: $scope.query.ClassId });
                $scope.Class = ClassData[0].ClassName;
            }
            else{
                $scope.Class="All";
            }
            $scope.FromDate = $scope.query.FromDate;
            $scope.ToDate = $scope.query.ToDate;
            apiService.post('/api/StudentAttendance/GetStudentAttendanceHistory', $scope.query,
                getStudentAttendanceHistoryListSuccess,
                failed);
            vm.close();
        }

        function getStudentAttendanceHistoryListSuccess(response) {
            vm.studentattendanceHistoryList = response.data.Content;
            vm.studentattendanceHistoryCount = response.data.TotalRows;

        }
        function failed() {
            notificationService.displaymessage('No participant list found.');
        }
        function setStartDate(DateVal) {
            vm.disabled = false;
            $scope.query.ToDate = undefined;
            vm.attendancestartDate = "";
            var tempDate = new Date(DateVal);
            vm.attendanceendDate = tempDate;
        };
        function setFromDate(DateVal) {
            var tempDate = new Date(DateVal);
            vm.attendancestartDate = tempDate;
        };
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function reset() {
            $scope.query.ParticipantName = '';
            $scope.query.ClassId = 0;
            $scope.query.FromDate = new Date(),
                $scope.query.ToDate = new Date(),
                getStudentAttendanceHistoryList();
        }
    }
})();