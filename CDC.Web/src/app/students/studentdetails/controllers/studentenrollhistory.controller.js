(function () {
    'use strict';

    angular
        .module('app.students')
        .controller('StudentEnrollHistoryController', StudentEnrollHistoryController);
    //For Showing student attendance history details -Tulesh
    function StudentEnrollHistoryController($scope, $mdDialog, $state, $stateParams, $localStorage,
        notificationService, filerService, apiService, ClassService, HOST_URL, CommonService, $filter) {
        var vm = this;
        vm.reset4 = reset4;
        vm.toggleRight4 = filerService.toggleRight4();
        vm.isOpenRight4 = filerService.isOpenRight4();
        vm.close4 = filerService.close4();
        $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        if ($stateParams.obj == null || $stateParams.obj == undefined) {
            $state.go('triangular.allparticipants');
            return;
        }
        var studentId;
        if ($stateParams.obj.StudentId != null || $stateParams.obj.StudentId != undefined) {
            studentId = $stateParams.obj.StudentId;
        }
        else {
            studentId = $stateParams.obj.Student.ID;
        }
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }
        vm.ImageUrlPath = HOST_URL.url;
        vm.columns = {
            // Image: 'Image',
            ClassName: 'Class Name',
            StartDate: 'Start Date',
            Status: 'Status',
            ID: 'ID'
        };
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-ClassName',
            page: 1,
            AgencyId: $scope.$storage.agencyId,
            StudentId: studentId,
            ParticipantName: '',
            ClassId: 0,
            StartDate: null
        };
        function reset4() {
            $scope.query.ClassId = 0;
            GetStudentScheduleList();
        }
        vm.GetStudentScheduleList = GetStudentScheduleList;
        GetStudentScheduleList();
        vm.enrollHistoryListCount = 0;
        function GetStudentScheduleList() {
            if ($scope.query.ClassId > 0) {
                var ClassData = $filter('filter')(vm.AllClasses, { ID: $scope.query.ClassId });
                $scope.Class = ClassData[0].ClassName;
            }
            else {
                $scope.Class = "All";
            }
            vm.promise = ClassService.GetEnrollmentHistory($scope.query);
            vm.promise.then(function (response) {
                if (response.enrollHistory.Content.length > 0) {
                    vm.NoData = false;
                    vm.enrollHistoryList = response.enrollHistory.Content;
                    vm.enrollHistoryListCount = response.enrollHistory.TotalRows;
                    // vm.StudentList.FirstName ="Parag";
                    // vm.FamilyCount = response.FamilyList.TotalRows;
                    $scope.selected = [];

                }
                else {
                    if (response.enrollHistory.Content.length == 0) {
                        vm.NoData = true;
                        vm.enrollHistoryList = [];
                        vm.enrollHistoryListCount = 0;
                        // NotificationMessageController('No Schedule list found.');
                    }
                    else {
                        // NotificationMessageController('Unable to get Schedule list at the moment. Please try again after some time.');
                    }
                }
                vm.close4();
            });
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();