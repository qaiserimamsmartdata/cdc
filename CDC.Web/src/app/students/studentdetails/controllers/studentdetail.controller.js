(function () {
  'use strict';

  angular
    .module('app.students')
    .controller('StudentDetailsController', StudentDetailsController)
    .controller('StudentProfileController', StudentProfileController)


  /* @ngInject */
  function StudentDetailsController($scope, $timeout, $mdToast, $mdDialog, imageUploadService, $stateParams, $localStorage, $state, ClassService, FamilyService, CommonService, HOST_URL, $http, notificationService, apiService, $rootScope) {
    var vm = this;
    var data = $stateParams.obj;
    $scope.participantScheduleCount = 0;
    $scope.futureCount = 0;
    $scope.$storage = localStorage;
    // if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
    //   $state.go('authentication.login');
    //   notificationService.displaymessage("You must login first.");
    //   return;
    // }
    if (localStorage.Portal == "FAMILY") {
      vm.Cancel = 1;
    }

    vm.close = close;
    vm.status = 'idle';  // idle | uploading | complete


    var fileList;
    vm.ImageUrlPath = HOST_URL.url;

    if ($stateParams.obj == null || $stateParams.obj == undefined) {
      $state.go('triangular.allparticipants');
      return;
    }
    else {
      vm.columns = {
        Image: 'Image',
        FirstName: 'First Name',
        StudentName: 'Participant',
        ClassName: 'Class Name',
        StartDate: 'Start Date',
        Room: 'Room',
        ID: 'ID'
      };
      var studentId;
      if ($stateParams.obj.StudentId != null || $stateParams.obj.StudentId != undefined) {
        studentId = $stateParams.obj.StudentId;
      }
      else {
        studentId = $stateParams.obj.Student.ID;
      }
      vm.query = {
        filter: '',
        limit: '10',
        order: '-id',
        page: 1,
        status: 0,
        StudentId: studentId,
        AgencyId: localStorage.agencyId,
        name: '',
        StudentName: ''
      };
      ///For Family Portal
      vm.FamilyId = 0;
      if (localStorage.FamilyId != undefined || null)
        vm.FamilyId = localStorage.FamilyId;
        vm.IsEnableForStaff=0;
        if(localStorage.Portal=='STAFF')
        vm.IsEnableForStaff=1;

      /////////////////////////
      vm.showProgressbarForDisplay = true;
      vm.CancelEnrollment = CancelEnrollment;
      vm.showDeleteFutureStudentConfirmDialoue = showDeleteFutureStudentConfirmDialoue;
      vm.showUpdateFutureStudentDialoue = showUpdateFutureStudentDialoue;
      vm.GetStudentScheduleList = GetStudentScheduleList;
      vm.GetFutureEnrolledStudent = GetFutureEnrolledStudent;
      var bDate = new Date();
      bDate = bDate.setDate(bDate.getDate() - 365);
      vm.dateOfBirth = new Date(bDate);

      //for min 80years.
      var NewDate = new Date();
      NewDate = NewDate.setDate(NewDate.getDate() - 29200);
      vm.NewMinDate = new Date(NewDate)

      GetStudentListByFamily();
      GetFutureEnrolledStudent();
      GetStudentScheduleList();
      getGradeLevel();
      getMembership();

      $rootScope.$on("GetFutureEnrolledStudent", function () {
        GetFutureEnrolledStudent();
      });

      $rootScope.$on("GetStudentListByFamily", function () {
        GetStudentListByFamily();
      });

      function getGradeLevel() {
        vm.promise = CommonService.getGradeLevel(vm.query);
        vm.promise.then(function (response) {
          if (response.Content.length > 0) {
            vm.AllGradeLevel = null;
            vm.AllGradeLevel = eval(response.Content);
          }
        });
      };

      vm.checkRedirect = checkRedirect;
      function checkRedirect() {
        if (data.RedirectFrom == "allparticipants") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.allparticipants');
          else
            $state.go('triangular.parentallparticipants');

        }
        else if (data.RedirectFrom == "enrolledparticipants") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.enrolledparticipants');
          else
            $state.go('triangular.parentenrolledparticipants');
        }
        else if (data.RedirectFrom == "FutureEnrolledParticipants") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.futureenrolledparticipants');
          else
            $state.go('triangular.parentfutureenrolledparticipants');

        }
        else if (data.RedirectFrom == "UnEnrolledParticipants") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.unenrolledparticipants');
          else
            $state.go('triangular.parentunenrolledparticipants');

        }
        else if (data.RedirectFrom == "attendanceHistory") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.attendancehistory');
          else
            $state.go('triangular.parentattendancehistory');

        }
        else if (data.RedirectFrom == "StudentList") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.Familydetails');
          else
            $state.go('triangular.ParentFamilydetails');
        }
        else if (data.RedirectFrom == "enrollList") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.Familydetails');
          else
            $state.go('triangular.ParentFamilydetails');
        }
        else if (data.RedirectFrom == "ParticipantAttendance") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.attendance');
          else
            $state.go('triangular.staffportalattendance');

        }
        else if (data.RedirectFrom == "FailedSignoutParticipant") {
          if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.failedsignouts');
          else
            $state.go('triangular.staffportalattendance');
        }
      };
      vm.uploadImage = uploadImage;
      vm.UploadSuccess = UploadSuccess;
      function uploadImage($files) {
        imageUploadService.uploadImage($files, vm.UploadSuccess)
        $scope.ProfileImage = $files;
        var fileExtension = $scope.ProfileImage[0].name;
        var size = $files[0].size;
        if (size > 2097152) {
          notificationService.displaymessage('Image Size should not exceed 2MB');
          return;

        }
        fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
        if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {
          uploadStarted();

          $timeout(uploadComplete, 2000);
          imageUploadService.uploadImage($files, vm.UploadSuccess)
        } else {
          notificationService.displaymessage('Please select valid image format.');
          //angular.element("input[type='file']").val(null);
        }
      }
      function uploadStarted() {
        vm.status = 'uploading';
      }

      function uploadComplete() {
        vm.status = 'complete';
        var message = 'Image Uploaded Successfully';
        for (var file in fileList) {
          message += fileList[file].name + ' ';
        }
        $mdToast.show({
          template: '<md-toast><span flex>' + message + '</span></md-toast>',
          position: 'bottom right',
          hideDelay: 2000
        });

        $timeout(uploadReset, 2000);
      }

      function uploadReset() {
        vm.status = 'idle';
      }
      function UploadSuccess(data) {
        vm.ProfilePic = data.Content;
        vm.StudentDetails.Student.ImagePath = data.Content;
      }

      function getMembership() {
        vm.promise = CommonService.getMembership(vm.query);
        vm.promise.then(function (response) {
          if (response.Content.length > 0) {
            vm.AllMembership = null;
            vm.AllMembership = eval(response.Content);
          }
        });
      };
      function GetStudentListByFamily() {

        vm.promise = FamilyService.GetStudentDetailsByIdService(vm.query);
        vm.promise.then(function (response) {
          if (response.Content.length > 0) {
            vm.StudentDetails = response.Content[0];
            vm.StudentDetails.Student.Gender = vm.StudentDetails.Student.Gender.toString();
            vm.StudentDetails.Student.MembershipTypeId = vm.StudentDetails.Student.MembershipTypeId.toString();
            vm.StudentDetails.GradeLevelId = vm.StudentDetails.GradeLevelId == null ? '' : vm.StudentDetails.GradeLevelId.toString();
            vm.StudentDetails.Student.DateOfBirth = new Date(vm.StudentDetails.Student.DateOfBirth);
            vm.showProgressbarForDisplay = false;
            vm.updateParticipant = updateParticipant;
            $scope.ParticipantName = vm.StudentDetails.FirstName + " " + vm.StudentDetails.LastName;
            $scope.selected = [];
          }
          else {
            if (response.Content.length == 0) {
              notificationService.displaymessage('No participant detail found.');
              vm.showProgressbarForDisplay = false;
            }
            else {
              notificationService.displaymessage('Unable to get participant detail at the moment. Please try again after some time.');
              vm.showProgressbarForDisplay = false;
            }
          }
        });
      };

      function GetStudentScheduleList() {

        vm.promise = FamilyService.GetStudentScheduleListByIdService(vm.query);
        vm.promise.then(function (response) {
          if (response.enrollList.Content.length > 0) {
            vm.ScheduleList = response.enrollList.Content;
            $scope.participantScheduleCount = response.enrollList.TotalRows;
            // vm.StudentList.FirstName ="Parag";
            // vm.FamilyCount = response.FamilyList.TotalRows;
            $scope.selected = [];

          }
          else {
            if (response.enrollList.Content.length == 0) {
              vm.ScheduleList = [];
              vm.participantScheduleCount = 0;
              //notificationService.displaymessage('No current enrollment list found.');
            }
            else {
              notificationService.displaymessage('Unable to get current enroll list at the moment. Please try again after some time.');
            }
          }

        });
      };

      function GetFutureEnrolledStudent() {

        vm.promise = FamilyService.GetFutureEnrolledStudent(vm.query);
        vm.promise.then(function (response) {
          debugger
          if (response.enrollList.Content.length > 0) {
            vm.FutureScheduleList = response.enrollList.Content;
            $scope.futureCount = response.enrollList.TotalRows;
            // vm.StudentList.FirstName ="Parag";
            // vm.FamilyCount = response.FamilyList.TotalRows;
            $scope.selected = [];

          }
          else {
            if (response.enrollList.Content.length == 0) {
              vm.FutureScheduleList = [];
              vm.participantScheduleCount = 0;
              // notificationService.displaymessage('No future list found.');
            }
            else {
              notificationService.displaymessage('Unable to get future enroll list at the moment. Please try again after some time.');
            }
          }

        });
      };

      function showUpdateFutureStudentDialoue(event, data) {
        $mdDialog.show({
          controller: 'UpdateFutureEnrolledStudentController',
          controllerAs: 'vm',
          templateUrl: 'app/Participants/views/updatefutureenrolledstudent.tmpl.html',
          parent: angular.element(document.body),
          targetEvent: event,
          escToClose: true,
          clickOutsideToClose: true,
          items: { studentdetail: data },
          fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
      };

      function showDeleteFutureStudentConfirmDialoue(event, data) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
          .title('Are you sure to delete this student permanently?')
          //   .textContent('All of this branch data and all "Branch Managers/Delivery Personals" associated with this class  will be deleted.')
          .ariaLabel('Lucky day')
          .targetEvent(event)
          .ok('Delete')
          .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
          DeleteFutureEnrolledStudent(data);
        }, function () {
          $scope.hide();
        });
      };

      function DeleteFutureEnrolledStudent(model) {

        vm.showProgressbar = true;
        vm.promise = FamilyService.DeleteFutureEnrolledStudent(model);
        vm.promise.then(function (response) {
          if (response.StudentInfo.IsSuccess == true) {
            notificationService.displaymessage('Participant deleted Successfully.');
            vm.showProgressbar = false;
            GetFutureEnrolledStudent();
          }
          else {
            notificationService.displaymessage('Error occured while deleting, Please try again later.');
          }
        })
      }

      function updateParticipant(model) {
        // if (studentForm.$valid) {
        vm.showProgressbarForDisplay = true;
        vm.showProgressbar = true;
        vm.ParticipantForms = [];
        vm.ParticipantForms.push(model);
        $http.post(HOST_URL.url + '/api/Family/saveStudentInfo', vm.ParticipantForms).then(function (response) {
          if (response.data.ContactInfo.IsSuccess == true) {
            vm.showProgressbarForDisplay = false;
            notificationService.displaymessage(response.data.ContactInfo.Message);
          }
          else {
            vm.showProgressbarForDisplay = false;
            notificationService.displaymessage('Unable to Update at the moment. Please try again after some time.');
          }
        });
        // }
      }


      function CancelEnrollment(schedule) {

        var confirm = $mdDialog.confirm()
          .title('Are you sure to cancel the enrollment of ' + schedule.StudentFullName + ' for class ' + schedule.ClassName + '?')
          .ariaLabel('Lucky day')
          .ok('Ok')
          .cancel('Cancel');

        $mdDialog.show(confirm).then(function () {

          var EnrollMentId = schedule.ID;
          apiService.post('/api/EnrollClass/DeleteEnrollMent', EnrollMentId,
            CancelEnrollmentSuccess,
            Failed);
        });


      }

      function CancelEnrollmentSuccess(response) {
        if (response.data.CancelenrollList.IsSuccess == true) {
          notificationService.displaymessage('Enrollment cancelled successfully.');
          GetStudentScheduleList();
        }
        else {
          notificationService.displaymessage('Something went wrong.');
        }

      }

      function Failed() {
        notificationService.displaymessage('Something went wrong.')
      }
    }
  }

  function StudentProfileController($scope, $state, FamilyService, $stateParams) {
    var vm = this;
    var studentId;
    var data = $stateParams.obj;
    // if ((localStorage.agencyId == undefined || null) && (localStorage.FamilyId == undefined || null)) {
    //   $state.go('authentication.login');
    //   notificationService.displaymessage("You must login first.");
    //   return;
    // }
    if ($stateParams.obj == null || $stateParams.obj == undefined) {
      if(localStorage.Portal=='AGENCY')
      $state.go('triangular.allparticipants');
      else
      $state.go('triangular.parentallparticipants');
      return;
    }
    if ($stateParams.obj.StudentId != null || $stateParams.obj.StudentId != undefined) {
      studentId = $stateParams.obj.StudentId;
    }
    else {
      studentId = $stateParams.obj.Student.ID;
    }
    vm.query = {
      filter: '',
      limit: '10',
      order: '-id',
      page: 1,
      status: 0,
      StudentId: studentId,
      AgencyId: localStorage.agencyId,
      name: '',
      StudentName: ''
    };

    GetStudentListByFamily();
    vm.checkRedirect = checkRedirect;
    function checkRedirect() {
      if (data.RedirectFrom == "allparticipants") {
         if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.allparticipants');
          else
            $state.go('triangular.parentallparticipants');

      }
      else if (data.RedirectFrom == "enrolledparticipants") {
         if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.enrolledparticipants');
          else
            $state.go('triangular.parentenrolledparticipants');
      }
      else if (data.RedirectFrom == "FutureEnrolledParticipants") {
        if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.futureenrolledparticipants');
          else
            $state.go('triangular.parentfutureenrolledparticipants');

      }
      else if (data.RedirectFrom == "UnEnrolledParticipants") {
        if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.unenrolledparticipants');
          else
            $state.go('triangular.parentunenrolledparticipants');
      }
      else if (data.RedirectFrom == "attendanceHistory") {
        if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.attendancehistory');
          else
            $state.go('triangular.parentattendancehistory');
      }
      else if (data.RedirectFrom == "StudentList") {
        if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.Familydetails');
          else
            $state.go('triangular.ParentFamilydetails');
      }
      else if (data.RedirectFrom == "enrollList") {
        if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.Familydetails');
          else
            $state.go('triangular.ParentFamilydetails');
      }
      else if (data.RedirectFrom == "ParticipantAttendance") {
         if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.attendance');
          else
            $state.go('triangular.staffportalattendance');
      }
      else if (data.RedirectFrom == "FailedSignoutParticipant") {
         if (localStorage.Portal == 'AGENCY')
            $state.go('triangular.failedsignouts');
          else
            $state.go('triangular.staffportalattendance');
      }
    };
    function GetStudentListByFamily() {

      vm.promise = FamilyService.GetStudentDetailsByIdService(vm.query);
      vm.promise.then(function (response) {
        if (response.Content.length > 0) {
          vm.StudentDetails = response.Content[0];
          $scope.ParticipantName = vm.StudentDetails.FirstName + " " + vm.StudentDetails.LastName;
          $scope.selected = [];
        }
        else {
          if (response.Content.length == 0) {
            notificationService.displaymessage('No participant detail found.');
            vm.showProgressbarForDisplay = false;
          }
          else {
            notificationService.displaymessage('Unable to get participant detail at the moment. Please try again after some time.');
            vm.showProgressbarForDisplay = false;
          }
        }
      });
    };
  }
})();

