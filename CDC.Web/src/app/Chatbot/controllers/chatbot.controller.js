// (function () {
//   'use strict';

//   angular
//     .module('app.chatbot')
//     .controller('chatbotController', chatbotController);

//   /* @ngInject */
//   function chatbotController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, StaffService, CommonService, AgencyService, filerService, $sessionStorage, HOST_URL, $filter) {

//     var vm = this;
   
//     var accessToken = "b5864e4127224f4bb36d18ceb27f1b34",
//       baseUrl = "https://api.api.ai/v1/",
//       $speechInput,
//       $recBtn,
//       $answer,
//       $speech,
//       recognition,
//       messageRecording = "Recording...",
//       messageCouldntHear = "I couldn't hear you, could you say that again?",
//       messageInternalError = "Oh no, there has been an internal server error",
//       messageSorry = "I'm sorry, I don't have the answer to that yet.";

//     $(document).ready(function () {
//       $speechInput = $("#speech");
//       $recBtn = $("#rec");

//       $speechInput.keypress(function (event) {
//         if (event.which == 13) {
//           event.preventDefault();
//           send();
//         }
//       });
//       $recBtn.on("click", function (event) {
//         switchRecognition();
//       });
//       $(".debug__btn").on("click", function () {
//         $(this).next().toggleClass("is-active");
//         return false;
//       });
//     });

//     function startRecognition() {
//       recognition = new webkitSpeechRecognition();
//       recognition.continuous = false;
//       recognition.interimResults = false;

//       recognition.onstart = function (event) {
//         respond(messageRecording);
//         updateRec();
//       };
//       recognition.onresult = function (event) {
//         recognition.onend = null;

//         var text = "";
//         for (var i = event.resultIndex; i < event.results.length; ++i) {
//           text += event.results[i][0].transcript;
//         }
//         setInput(text);
//         stopRecognition();
//       };
//       recognition.onend = function () {
//         respond(messageCouldntHear);
//         stopRecognition();
//       };
//       recognition.lang = "en-US";
//       recognition.start();
//     }

//     function stopRecognition() {
//       if (recognition) {
//         recognition.stop();
//         recognition = null;
//       }
//       updateRec();
//     }

//     function switchRecognition() {
//       if (recognition) {
//         stopRecognition();
//       } else {
//         startRecognition();
//       }
//     }

//     function setInput(text) {
//       $speechInput.val(text);
//       send();
//     }

//     function updateRec() {
//       $recBtn.text(recognition ? "Stop" : "Speak");
//     }

//     function send() {
//       var text = $speechInput.val();
//       $.ajax({
//         type: "POST",
//         url: baseUrl + "query",
//         contentType: "application/json; charset=utf-8",
//         dataType: "json",
//         headers: {
//           "Authorization": "Bearer " + accessToken
//         },
//         data: JSON.stringify({ query: text, lang: "en", sessionId: "f7af3355-2303-4233-85c0-3cdfe548c4db" }),

//         success: function (data) {
//           prepareResponse(data);
//         },
//         error: function () {
//           respond(messageInternalError);
//         }
//       });
//     }
    
//     function prepareResponse(val) {
//       $answer = val.result.speech;
     
      
//       var queryWords = $answer.split(" ");
//       $speech=$answer;
//       for (var i = 0; i < queryWords.length; i++) {
//         if (queryWords[i] == "@families") {
//           $speech=$answer.replace("@families"," ");
//           $state.go('triangular.families');
//         }
//         else if(queryWords[i] == "@family_reg"){
//            $speech=$answer.replace("@family_reg"," ");
//           $state.go('triangular.addFamily');
//         }
//         else if(queryWords[i] == "@new_staff"){
//            $speech=$answer.replace("@new_staff"," ");
//           $state.go('triangular.addstaff');
//         }

//       }
//        var debugJSON = JSON.stringify(val, undefined, 2),
//       spokenResponse =$speech;

//       respond(spokenResponse);
//       debugRespond(debugJSON);
//     }

//     function debugRespond(val) {
//       $("#response").text(val);
//     }

//     function respond(val) {
//       if (val == "") {
//         val = messageSorry;
//       }

//       if (val !== messageRecording) {
//         var msg = new SpeechSynthesisUtterance();
//         msg.voiceURI = "native";
//         msg.text = val;
//         msg.lang = "en-US";
//         window.speechSynthesis.speak(msg);
//       }

//       $("#spokenResponse").addClass("is-active").find(".spoken-response__text").html(val);
//     }

//   }
// })();