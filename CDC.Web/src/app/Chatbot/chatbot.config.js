(function () {
    'use strict';

    angular
        .module('app.chatbot')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

          $stateProvider
            .state('triangular.chatbot', {
                url: '/chatbot',
                templateUrl: 'app/Chatbot/views/chatbot.tmpl.html',
                // set the controller to load for this page
               // controller: 'chatbotController',
               // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10 chatbox-bg'
                    },
                    permissions: {
                        only: ['viewChatbot']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            

        // triMenuProvider.addMenu({
        //     name: 'Chat Bot',
        //     icon: 'fa fa-weixin',
        //     type: 'link',
        //     state: 'triangular.chatbot',
        //     permission: 'viewChatbot',
            
             
            
        // });
    }
})();
