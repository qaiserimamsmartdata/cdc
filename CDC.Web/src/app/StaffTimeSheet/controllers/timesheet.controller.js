(function () {
    'use strict';

    angular
        .module('app.stafftimesheet')
        .controller('timeSheetController', timeSheetController);
    function timeSheetController($scope, $mdDialog, $rootScope, $filter, StaffTimeSheetService,apiService, HOST_URL, $state, filerService, notificationService, $localStorage, StaffService) {

        var vm = this;
        // $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        //  if(localStorage.StaffId == undefined || null)
        // {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // } 
        vm.query = {
            page: 1,
            limit: 10,
            FromDate: new Date(),
            ToDate: new Date(),
            TimeZone: '',
            AgencyId: localStorage.agencyId
        };
        localStorage.ParticipantAttendancePage = false;
        var toDate = vm.query.ToDate;
        vm.query.FromDate.setDate(toDate.getDate() - 6);
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.disabled = true;
        vm.StaffId = localStorage.staffId == null ? 0 : localStorage.staffId;
        vm.query.TimeZone = localStorage.TimeZone;
        // vm.title = 'Employee';
        // $scope.showGrid = true;
        // $scope.dateFromValue = null;
        // $scope.dateToValue = null;
          $scope.total_count=0;
        $scope.TimezoneList = null;
        // //$scope.TimezoneListSelect = 'Loading...';


        $scope.openCheckInOrOutDialog = openCheckInOrOutDialog;
        $scope.cancelCheckInOrOutDialog = cancelCheckInOrOutDialog;

        $scope.pageChangeHandler = pageChangeHandler;
        $scope.searchTimeSheetWithDates = searchTimeSheetWithDates;

        $scope.saveCheckInOutData = saveCheckInOutData;
        $scope.saveCheckInOutDataFromTable = saveCheckInOutDataFromTable;
        $scope.checkInOrOutType = null;
        //Get CheckInandCheckOut data
        $scope.getCheckInCheckOutType = getCheckInCheckOutType;
        // $scope.getTimeSheetByTimezone = getTimeSheetByTimezone;
        // //Confirmation Alert
        $scope.clockInConfirmationAlert = clockInConfirmationAlert;
        $scope.clockOutConfirmationAlert = clockOutConfirmationAlert;

        vm.reset = reset;
        vm.setFromDate=setFromDate;
        //vm.setStartDate = setStartDate;
        init();
        function init() {
            apiService.post("api/TimeSheet/TimeZones", null,
                getTimeZoneSuccess,
                Failed);

            function getTimeZoneSuccess(result) {
                vm.TimezoneList = result.data.Response;
                vm.TimezoneListSelect = 'Select Timezone';
                GetLocalTimezoneName();
            }
        }
        vm.Failed = Failed;
        function Failed(result) {
            notificationService.displaymessage('Please try again after some time.');
        }
        vm.setStartDate = setStartDate;
        function setStartDate(DateVal) {
            vm.disabled = false;
            vm.query.ToDate=undefined;
            vm.attendancestartDate=""; 
            var tempDate = new Date(DateVal);
            vm.attendanceendDate = tempDate;
        };
        function openCheckInOrOutDialog() {
            $scope.getCheckInCheckOutType();
            if (angular.isUndefined()) {
                $scope.checkOutTypeList = [{
                    ID: '0',
                    TimeCheckFor: 'Day'
                }];
            }
        }
        function cancelCheckInOrOutDialog() {
            vm.close();
        }

        function pageChangeHandler(pageno) {
           
            vm.query.pageNo = pageno;
            vm.query.StaffId = localStorage.staffId;
            apiService.post('api/timesheet/GetTimesheetWithTimeZone', vm.query, pageChangeHandlerSuccess, Failed)

        }
      
        function pageChangeHandlerSuccess(result) {
           
            if (result.data.Response !== null) {
                $scope.newData = null;
                $scope.timeSheetDetails = angular.copy($scope.newData);
                $scope.timeSheetDetails = result.data.Response.TimeSheets;
                $scope.total_count = (angular.copy(result.data.Response.TotalRecords)==undefined||angular.copy(result.data.Response.TotalRecords)==null)?0:angular.copy(result.data.Response.TotalRecords);
                $scope.total_hours = angular.copy(result.data.Response.TotalHours);
                //$scope.showGrid = false;
            }
        }

        function searchTimeSheetWithDates() {
           
            $scope.FromDate=vm.query.FromDate;
            $scope.ToDate=vm.query.ToDate;
             vm.query.StaffId = localStorage.staffId;
            if (StaffService.Id != null || StaffService.Id != undefined)
                vm.query.StaffId = StaffService.Id;
            apiService.post('api/timesheet/GetTimesheetWithTimeZone', vm.query, searchTimeSheetWithDatesSuccess, Failed)
        }
        function searchTimeSheetWithDatesSuccess(result) {
           
            $scope.showGrid = result.data.Response !== null ? true : false;
            $scope.timeSheetDetails = result.data.Response !== null ? result.data.Response.TimeSheets : null;
            $scope.total_count = result.data.Response !== null ? result.data.Response.TotalRecords : 0;
            $scope.total_hours = result.data.Response !== null ? angular.copy(result.data.Response.TotalHours) : null;
            vm.close();
        }
        searchTimeSheetWithDates();
        function saveCheckInOutData(data) {
            data.StaffID = localStorage.staffId;
            apiService.post('api/TimeSheet/SaveTimeSheet', data, saveCheckInOutDataSuccess, Failed)


        }

        function saveCheckInOutDataSuccess(response) {
            if (response.IsError) {
                notificationService.displaymessage('Please try again after some time.');
            }
            else {
                notificationService.displaymessage('You have been Clocked In.');
                //Need to optimized
                $scope.pageChangeHandler(1);
            }
        }

        function saveCheckInOutDataFromTable(data) {
            // data.StaffID = localStorage.staffId;
            // apiService.post('api/TimeSheet/SaveTimeSheet', data, saveCheckInOutDataFromTableSuccess, Failed)
            data.StaffID = localStorage.staffId;
            vm.promise = StaffTimeSheetService.saveCheckInOutDataFromTable(data);
            vm.promise.then(function (response) {
                notificationService.displaymessage(data.showMessage);
                 //Need to optimized
                $scope.pageChangeHandler(1);

            });
        }
        function saveCheckInOutDataFromTableSuccess(response) {
            if (response.data.IsError) {
                notificationService.displaymessage('Please try again after some time.');
            }
            else {
                notificationService.displaymessage('You have been Clocked In.');
                //Need to optimized
                $scope.pageChangeHandler(1);
            }
        }

        //Funtion to fetch data from dashboardService 
        //for CheckInCheckOut Model Popup
        function getCheckInCheckOutType() {
            apiService.post('api/timesheet/TimeCheckStatus', vm.StaffId, getCheckInCheckOutTypeSuccess, Failed)
        }
        function getCheckInCheckOutTypeSuccess(response) {
            if (response.data.IsError) {
                //Logger.error(response.data.Message, "", "");
            }
            else {
                $scope.checkOutTypeList = response.data.Response.OpenChecks;
            }
        }

        // function getTimeSheetByTimezone(timezone) {
        //     apiService.post('api/timesheet/GetTimesheetWithTimeZone')
        //     timeSheetService.getTimeSheetByTimezone(timezone).then(function (data) {
        //         if (data.data.IsError) {
        //             Logger.error(data.data.Message, "", "");
        //         }
        //         else {
        //             $scope.newData = null;
        //             $scope.timeSheetDetails = angular.copy($scope.newData);
        //             $scope.timeSheetDetails = data.data.Response !== null ? data.data.Response.TimeSheets : null;
        //             $scope.pageSize = 10;
        //             $scope.total_count = data.data.Response !== null ? data.data.Response.TotalRecords : null;
        //             paginationService.setCurrentPage("timeSheetPagination", 1);
        //         }
        //     });
        // }

        function GetLocalTimezoneName() {
            // var n = new Date,
            //     t = n.getFullYear().toString() + "-" + ((n.getMonth() + 1).toString().length === 1 ? "0" + (n.getMonth() + 1).toString() : (n.getMonth() + 1).toString()) + "-" + (n.getDate().toString().length === 1 ? "0" + n.getDate().toString() : n.getDate().toString()) + "T" + (n.getHours().toString().length === 1 ? "0" + n.getHours().toString() : n.getHours().toString()) + ":" + (n.getMinutes().toString().length === 1 ? "0" + n.getMinutes().toString() : n.getMinutes().toString()) + ":" + (n.getSeconds().toString().length === 1 ? "0" + n.getSeconds().toString() : n.getSeconds().toString());
            // $scope.selectedTimeZone = new Date(t + "Z").toString().match(/\(([A-Za-z\s].*)\)/)[1];
            // vm.query.TimeZone = $scope.selectedTimeZone;
            //$scope.pageChangeHandler(1);
        }

        function clockInConfirmationAlert(ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure?')
                .textContent('Do you want to clock in?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Clock In')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                $scope.saveCheckInOutDataFromTable({ 'PurposeToCheckInOrOut': 0, 'showMessage': 'You have been Clocked In' });
                //$scope.status = 'You decided to get rid of your debt.';
            }, function () {
                //$scope.status = 'You decided to keep your debt.';
            });
        }

        function clockOutConfirmationAlert(ev) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure?')
                .textContent('Do you want to clock out?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Clock Out')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                $scope.saveCheckInOutDataFromTable({ 'PurposeToCheckInOrOut': 0, 'showMessage': 'You have been Clocked Out' });
                //$scope.status = 'You decided to get rid of your debt.';
            }, function () {
                //$scope.status = 'You decided to keep your debt.';
            });
        }
        function reset() {
            vm.query.TimeZone = localStorage.TimeZone;
            vm.query.ToDate = new Date();
            var fromDate = new Date();
            fromDate.setDate(vm.query.ToDate.getDate() - 6);
            vm.query.FromDate = fromDate;
            searchTimeSheetWithDates();
        }
        function setFromDate(DateVal) {
            var tempDate = new Date(DateVal);
            vm.attendancestartDate = tempDate;
        };
    }

} ());
