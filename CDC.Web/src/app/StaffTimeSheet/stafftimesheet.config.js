(function () {
    'use strict';
    angular
        .module('app.stafftimesheet')
        .config(moduleConfig);
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.timesheet', {
                url: '/timesheet',
                templateUrl: 'app/StaffTimeSheet/views/timeSheet.tmpl.html',
                // set the controller to load for this page
                controller: 'timeSheetController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewStaffSetting']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"STAFF",$window);
                    }
                }
            });
        triMenuProvider.addMenu({
            name: 'Time Clock',
            icon: 'fa fa-clock-o',
            type: 'link',
            permission: 'viewStaffSetting',
            state: 'triangular.timesheet',
            priority: 1.1,

        });
    }
    /* @ngInject */
})();