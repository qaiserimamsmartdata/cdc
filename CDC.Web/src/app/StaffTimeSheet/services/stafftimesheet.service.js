﻿(function () {
    'use strict';

    angular
        .module('app.stafftimesheet')
        .factory('StaffTimeSheetService', StaffTimeSheetService);

    StaffTimeSheetService.$inject = ['$http', '$q', 'HOST_URL','$rootScope'];

    /* @ngInject */
    function StaffTimeSheetService($http, $q, HOST_URL,$rootScope) {
        return {
            saveCheckInOutDataFromTable: saveCheckInOutDataFromTable
        };
       
       function saveCheckInOutDataFromTable(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/TimeSheet/SaveTimeSheet', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    }
})();