(function () {
    'use strict';

    angular
        .module('app')
        .factory('imageUploadService', imageUploadService);

    imageUploadService.$inject = ['$timeout', '$rootScope', '$mdToast', '$upload','HOST_URL'];

    function imageUploadService($timeout, $rootScope, $mdToast, $upload,HOST_URL) {

        var service = {
            uploadImage: uploadImage
        };

        function uploadImage($files,callback) {
            if ($files !== null && $files.length > 0) {
                var $file = $files;
                $rootScope.upload = $upload.upload({
                    method: 'POST',
                    url: HOST_URL.url + '/api/common/imageUpload',
                    file: $file
                }).success(function (data, status, headers) {
                    // file is downloaded successfully
                    // if (data.IsSuccess == true) {
                    //     $rootScope.ImagePath = data.Content;
                    // }
                    // else {
                    //     $mdToast.show({
                    //         template: '<md-toast><span flex>' + 'Some error occured, please try again.' + '</span></md-toast>',
                    //         position: 'bottom right',
                    //         hideDelay: 5000
                    //     });
                    // }
                    callback(data);
                }).error(function (data, status, headers) {
                    console.log(data);
                });
            }
        }
        return service;
    }
})();