﻿(function () {
    'use strict';

    angular
        .module('app.class')
        .factory('CommonService', CommonService);

    CommonService.$inject = ['$http', '$q', 'HOST_URL', '$localStorage'];

    /* @ngInject */
    function CommonService($http, $q, HOST_URL, $localStorage) {
        return {
            getCategory: getCategory,
            getRooms: getRooms,
            getSessions: getSessions,
            getClassStatus: getClassStatus,
            getLeaveStatusData: getLeaveStatusData,
            getYears: getYears,
            getMonths: getMonths,
            isExistName: isExistName,
            getCountries: getCountries,
            getStates: getStates,
            getCities: getCities,
            getPositions: getPositions,
            getAllOtherParticipants: getAllOtherParticipants,

            getType: getType,
            SetId: SetId,
            getClass: getClass,
            getStaff: getStaff,
            getScheduleInstructors: getScheduleInstructors,
            getInfoSource: getInfoSource,
            getMembership: getMembership,
            getRelationType: getRelationType,
            getGradeLevel: getGradeLevel,
            getParentListByStudentId: getParentListByStudentId,
            getClassList: getClassList,
            getFeesType: getFeesType,
            getPricingPlans: getPricingPlans,
            getAgencyLocations: getAgencyLocations,
            getSMSCarrier: getSMSCarrier,
            getSecurityQuestions: getSecurityQuestions,
            getTimeClockUsersPricingPlans: getTimeClockUsersPricingPlans,
            getEnrolledParticipantsCount: getEnrolledParticipantsCount,
            getPricingPlanCount: getPricingPlanCount,
            getAllParentList: getAllParentList,
            getAllCount: getAllCount,
            getPriorities: getPriorities,
            getSkillMaster: getSkillMaster,
            EmailToGuardians: EmailToGuardians,
            getStaffDashboardAllData: getStaffDashboardAllData,
            getSuperAdminDashboardAllData: getSuperAdminDashboardAllData,
            getAssociationParticipant: getAssociationParticipant,
            getParentDashboardAllData: getParentDashboardAllData,
            GetDateNew: GetDateNew,
            getAllParticipants: getAllParticipants,
            getAllStaff: getAllStaff,
            getParentInformationByStudent: getParentInformationByStudent,
            getDailyStatusCategory: getDailyStatusCategory,
            getPositionNameById: getPositionNameById,
            getParticipantInformation: getParticipantInformation,
            getAssociationParent: getAssociationParent,
            getClassCombo: getClassCombo,
            getYearsForFood: getYearsForFood,
            getMonthsForFood: getMonthsForFood,
            getMealTypeData: getMealTypeData,
            getFoodMasterData: getFoodMasterData,
            getAssociationType: getAssociationType,
            getMealItemData: getMealItemData,
            getMealServeSizeData: getMealServeSizeData,
            getMealServeQuantityData: getMealServeQuantityData,
            getMealServeTypesData: getMealServeTypesData,
            getModeOfConvenience: getModeOfConvenience
        };
        function SetId(Id) {
            this.AgencyId = Id;
        };

        function getYears() {
            var AgeYears = [];
            AgeYears.push(
                {
                    ID: 1,
                    Name: 1 + " " + "Year"
                }
            );
            for (var i = 2; i <= 99; i++) {

                AgeYears.push({
                    ID: i,
                    Name: i + " " + "Years"
                });
            }
            return AgeYears;
        }
        function getMonths() {
            var AgeMonths = [];
            AgeMonths.push(
                {
                    ID: 0,
                    Name: 0 + " " + "Month"
                },
                {
                    ID: 1,
                    Name: 1 + " " + "Month"
                }
            );
            for (var i = 2; i <= 11; i++) {

                AgeMonths.push({
                    ID: i,
                    Name: i + " " + "Months"
                });
            }
            return AgeMonths;
        }
        ////////////////////
        function getInfoSource(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/InfoSource/GetInfoSourceList', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getSecurityQuestions(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/SecurityQuestions/GetSecurityQuestionsList', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        }
        function getMembership(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/MembershipType/GetMembershipTypeList', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getAssociationParticipant(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/ParticipantModule/getAssociationParticipant', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getAssociationType(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/ParticipantModule/getAssociationType', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function getAssociationParent(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/ParticipantModule/getAssociationParents', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getRelationType(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/RelationshipType/GetRelationshipTypeList', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getGradeLevel(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/GradeLevel/GetGradeLevelList', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getCategory(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Category/GetCategory', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getRooms(model) {
            var deferred = $q.defer();
            var model = {
                AgencyId: localStorage.agencyId
            };
            $http.post(HOST_URL.url + '/api/Room/GetRoom', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        // function getCategories(){
        //   var deferred = $q.defer();
        //   $http.post(HOST_URL.url + '/api/Common/GetCategories').success(function (response, status, headers, config) {
        //         deferred.resolve(response);
        //     }).error(function (errResp) {
        //         deferred.reject({ message: "Really bad" });
        //     });
        //     return deferred.promise;
        // }

        function getSessions() {
            var deferred = $q.defer();
            var model = {
                AgencyId: localStorage.agencyId
            };
            $http.post(HOST_URL.url + '/api/Session/GetSession', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function getClassStatus() {
            var deferred = $q.defer();
            var model = {
                filter: '',
                limit: '10',
                order: '-id',
                page: 1,
                status: 0,
                studentclass: 0,
                name: '',
                AgencyId: localStorage.agencyId
            };
            $http.post(HOST_URL.url + '/api/ClassStatus/GetAllClassStatus', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function isExistName(name, apiLink) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + apiLink + '?name=' + name).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function getCountries() {
            var deferred = $q.defer();

            $http.post(HOST_URL.url + '/api/Common/country', null).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getSMSCarrier() {
            var deferred = $q.defer();

            $http.post(HOST_URL.url + '/api/Common/getSMSCarriers', null).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getPositions(agencyId) {
            debugger
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/getPositions', agencyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getStates(CountryId) {
            var deferred = $q.defer();

            $http.post(HOST_URL.url + '/api/Common/state', CountryId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getCities(StateId) {
            var deferred = $q.defer();

            $http.post(HOST_URL.url + '/api/Common/getstatescity', StateId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getType() {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/getType').success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getClass(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetClass_Room_Lesson', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getStaff(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetClass_Room_Lesson', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getScheduleInstructors() {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetStaffSchedule').success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getClassList(agencyId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetClass_Room_LessonByAgency', agencyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getParentListByStudentId(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StudentAttendance/GetParentListByStudentId', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getFeesType(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/getFeesTypes', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getPricingPlans(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/getPricingPlans', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getAgencyLocations(agencyId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/getLocations', agencyId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function EmailToGuardians(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/mailToGuardians', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getTimeClockUsersPricingPlans(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/getTimeClockUsersPricingPlans', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getEnrolledParticipantsCount(model) {
            model.IsEnrolled = true;
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/getAllEnrollList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getPricingPlanCount(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/getPricingPlanCount', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getAllParentList(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetAllParentList', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getAllCount(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/dashboard/getAllCount', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function getSuperAdminDashboardAllData(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/dashboard/getSuperAdminDashboardData', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getStaffDashboardAllData(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/dashboard/getStaffDashboardData', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getParentDashboardAllData(model) {
            var deferred = $q.defer();

            $http.post(HOST_URL.url + '/api/dashboard/getParentLoginDashboardData', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getPriorities(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Priority/GetAllPriority', model)
                .success(function (response, status, headers, config) {
                    deferred.resolve(response);
                })
                .error(function () {
                    deferred.reject({ message: "Really bad" });
                })
            return deferred.promise;
        };

        function getSkillMaster(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/SkillMaster/GetAllSkillMaster', model)
                .success(function (response, status, headers, config) {
                    deferred.resolve(response);
                })
                .error(function () {
                    deferred.reject({ message: "Really bad" });
                })
            return deferred.promise;
        };

        function GetDateNew(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetDateNew', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getAllParticipants(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetAllParticipants', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getAllOtherParticipants(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetAllOtherParticipants', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getAllStaff(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetAllStaff', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getParentInformationByStudent(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetParentInformationByStudent', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getDailyStatusCategory(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetDailyStatusCategory', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getPositionNameById(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetPositionNameById', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getParticipantInformation(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetParticipantInformation', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getClassCombo(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Common/GetClassCombo', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getYearsForFood() {
            var AgeYears = [];
            AgeYears.push(
                {
                    ID: 1,
                    Name: 1 + " " + "Year"
                }
            );
            for (var i = 2; i <= 99; i++) {

                AgeYears.push({
                    ID: i,
                    Name: i + " " + "Years"
                });
            }
            return AgeYears;
        }
        function getMonthsForFood() {
            var AgeMonths = [];
            AgeMonths.push(
                {
                    ID: 0,
                    Name: 0 + " " + "Month"
                },
                {
                    ID: 1,
                    Name: 1 + " " + "Month"
                }
            );
            for (var i = 2; i <= 11; i++) {

                AgeMonths.push({
                    ID: i,
                    Name: i + " " + "Months"
                });
            }
            return AgeMonths;
        }
        function getMealTypeData(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/GetMealType', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getFoodMasterData(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/GetFoodMster', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getMealItemData(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/GetMealItemMaster', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getLeaveStatusData(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/getLeaveStatusData', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getMealServeSizeData(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/GetMealServeSizeMaster', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getMealServeQuantityData(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/GetMealServeQuantityMaster', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getMealServeTypesData(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/GetMealServeTypesMaster', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function getModeOfConvenience(SearchData) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/common/GetModeOfConvenience', SearchData).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
    }
})();