(function () {
    'use strict';

    angular
        .module('app')
        .factory('hostUrlService', hostUrlService);

    hostUrlService.$inject = ['$http', '$q'];

    function hostUrlService($http) {
        return {
            url: 'anonymous'
        };
    }
})();