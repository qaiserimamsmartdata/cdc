(function () {
    'use strict';

    angular
        .module('app')
        .factory('notificationService', notificationService);

        notificationService.$inject = ['$timeout','$rootScope','$mdToast'];
        
        function notificationService($timeout,$rootScope,$mdToast) {

            var service = {
            // displaySuccess: displaySuccess,
            // displayError: displayError,
            // displayWarning: displayWarning,
            // displayInfo: displayInfo
            displaymessage: displaymessage
            };            

             function displaymessage(message) {
                $timeout(function () {
                    $rootScope.$broadcast('newMailNotification');
                    $mdToast.show({
                        template: '<md-toast><span flex>' + message + '</span></md-toast>',
                        position: 'bottom right',
                        hideDelay: 5000
                    });
                }, 100);
             };   
        return service;         
        }
})();