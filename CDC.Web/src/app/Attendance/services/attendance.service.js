﻿(function () {
    'use strict';

    angular
        .module('app.attendance')
        .factory('AttendanceService', AttendanceService);

    AttendanceService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function AttendanceService($http, $q, HOST_URL) {
        return {
            GetScheduledData: GetScheduledData,
            AddAttendance: AddAttendance,
            StaffAttendanceData: StaffAttendanceData,
            SetId: SetId,
            AddStudentPickUpAttendanceKiosk:AddStudentPickUpAttendanceKiosk,
            AddStudentAttendance:AddStudentAttendance,
            AddStudentAttendanceKiosk :AddStudentAttendanceKiosk ,
            AddStudentPickUpAttendance:AddStudentPickUpAttendance
        };

        function SetId(data) {
            this.Id = 0;
            this.StaffName = '';
            this.Id = data.ID;
            this.StaffName = data.FullName;
            this.StaffData = data;
        };
        function GetScheduledData(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StaffAttendance/GetAllStaffAttendance', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function StaffAttendanceData(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StaffAttendance/GetAllStaffAttendance', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function AddAttendance(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StaffAttendance/AddAttendance', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
          function AddStudentAttendance(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StudentAttendance/MarkDropByAttendance', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
          function AddStudentAttendanceKiosk(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StudentAttendance/MarkDropByAttendanceKiosk', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        function AddStudentPickUpAttendance(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StudentAttendance/MarkPickUpByAttendance', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
          function AddStudentPickUpAttendanceKiosk(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StudentAttendance/MarkPickUpByAttendanceKiosk', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
        
    }
})();