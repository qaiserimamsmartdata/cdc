(function () {
    'use strict';

    angular
        .module('app.attendance')
        .controller('staffTimeClockController', staffTimeClockController);

    function staffTimeClockController($scope, $mdDialog, $state, $http, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, filerService, notificationService, apiService, $localStorage, HOST_URL) {
        var vm = this;
         $scope.$storage = localStorage;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.GetStaffList = GetStaffList;
        vm.reset = reset;
        vm.setStartDate = setStartDate;
        vm.disabled = true;
        vm.columns = {
            Date: 'Date',
            DaySignIn: 'Day Sign In',
            LunchSignOut: 'Lunch Sign Out',
            LunchSignIn: 'Lunch Sign In',
            BreakSignOut: 'Break Sign Out',
            BreakSignIn: 'Break Sign In',
            DaySignOut: 'Day Sign Out',
            TotalHours: 'Total Hours'
        };

        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            FromDate: new Date(),
            ToDate: new Date(),
            ParticipantName: '',
            AgencyId: localStorage.agencyId
        };

        function GetStaffList() { }
        function reset() { }
        function setStartDate(DateVal) {
            vm.disabled = false;
            var tempDate = new Date(DateVal);
            vm.attendanceendDate = tempDate;
        };
    }
})();