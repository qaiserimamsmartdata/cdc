(function () {
    'use strict';

    angular
        .module('app.attendance')
        .controller('AttendanceController', AttendanceController)
        .controller('AddAttendanceController', AddAttendanceController)
        .controller('EditAttendanceController', EditAttendanceController);

    /* @ngInject */
    function AttendanceController($scope, $mdDialog, $state, $http, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, CommonService, AgencyService, AttendanceService, filerService, ClassService, notificationService, apiService, $localStorage, HOST_URL) {
        var vm = this;
        vm.showAddAttendaceDialoue = showAddAttendaceDialoue;
        vm.showEditAttendaceDialoue = showEditAttendaceDialoue;
        vm.openAttendancePopup = openAttendancePopup;
        vm.openAttendanceDropMarkPopup = openAttendanceDropMarkPopup;
        vm.openAttendancePickupMarkPopup = openAttendancePickupMarkPopup;
        vm.reset = reset;
        vm.getClassDropdown = getClassDropdown;
        vm.getAttendanceData = getAttendanceData;
        vm.getAttendanceDataSuccess = getAttendanceDataSuccess;
        vm.Failed = Failed;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.ImageUrlPath = HOST_URL.url;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.staffCount = 0;
        vm.CurrentDate = new Date();
        $scope.selected = [];
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            Name: '',
            Date: vm.CurrentDate,
            AgencyId: localStorage.agencyId
        };
        getClassDropdown();
        function getClassDropdown() {
            $scope.query.limit = 100;
            vm.promise = ClassService.GetClassListService($scope.query);
            vm.promise.then(function (response) {
                if (response.classList.length > 0) {
                    vm.classList = response.classList;
                    vm.classCount = response.classList[0].TotalCount;
                    $scope.selected = [];
                }
                else {
                    if (response.classList.length == 0) {
                        vm.classList = [];
                        vm.classCount = 0;
                        notificationService.displaymessage('No Class list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get class list at the moment. Please try again after some time.');
                    }
                }
                vm.close();
            });
        }
        function reset() {
            $scope.query.ClassId = 0;
            $scope.query.Name = '';
            $scope.query.Date = $scope.query.Date != null ? null : '';
            getAttendanceData();
        };
        getAttendanceData();
        function getAttendanceData() {
          
            localStorage.ClassId = $scope.query.ClassId;
            apiService.post('/api/StudentAttendance/GetAllStudentAttendance', $scope.query,
                getAttendanceDataSuccess,
                Failed).then(function (response) {
                    if (response.IsSuccess == true) {
                        if (response.Content.length > 0) {
                            vm.EnrolledStudentList = [];
                            vm.EnrolledStudentList = response.Content;
                            vm.EnrolledStudentListCount = response.TotalRows;
                            $scope.selected = [];
                        }
                        else {
                            vm.EnrolledStudentList = [];
                            vm.EnrolledStudentList = 0;
                            NotificationMessageController('No Participant list found.');
                        }
                    }
                    vm.close();
                });
        }
        function getAttendanceDataSuccess(result) {
            
            vm.EnrolledStudentList = result.data.Content;

        }
        function Failed(result) {
            notificationService.displaymessage('Please try again after some time.');
        }
        function openAttendancePopup(attendanceData) {
            if (attendanceData.InTime == null) {
                openAttendanceDropMarkPopup(attendanceData);
            } else if (attendanceData.OutTime == null) {
                openAttendancePickupMarkPopup(attendanceData)
            } else {
                notificationService.displaymessage('You have already marked pickup.');
            }
        }
        function openAttendanceDropMarkPopup(attendanceData) {
            $mdDialog.show({
                controller: AddAttendanceController,
                controllerAs: 'vm',
                templateUrl: 'app/Attendance/views/markDropInAttendance.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { attendanceData: attendanceData },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function openAttendancePickupMarkPopup(attendanceData) {
            $mdDialog.show({
                controller: AddAttendanceController,
                controllerAs: 'vm',
                templateUrl: 'app/Attendance/views/markPickupAttendance.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { attendanceData: attendanceData },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        function showAddAttendaceDialoue() {
            $mdDialog.show({
                controller: AddAttendanceController,
                controllerAs: 'vm',
                templateUrl: 'app/Attendance/views/attendance.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function showEditAttendaceDialoue(event, attendanceData) {
            $mdDialog.show({
                controller: EditAttendanceController,
                controllerAs: 'vm',
                templateUrl: 'app/Attendance/views/attendance.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { attendanceData: attendanceData },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
    }
    function AddAttendanceController($scope, $mdDialog, $state, $http, $localStorage, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, CommonService, AgencyService, AttendanceService, filerService, ClassService, notificationService, apiService, FamilyService, items, $filter) {
        var vm = this;
        vm.AttendanceData = {
            StaffScheduleId: 0,
            InTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            OutTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            OnLeave: false,
        };
        vm.SaveAttendance = SaveAttendance;
        vm.getParents = getParents;
        vm.cancelClick = cancelClick;
        $scope.$storage = localStorage;
        $scope.attendanceDataDetail = items.attendanceData;
        vm.AttendanceData.ClassId = items.attendanceData.ClassId;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            Name: '',
            Date: '',
            SheduleDate: '',
            ClassId: 0,
            AgencyId: AgencyService.AgencyId
        };

        vm.getAttendanceData = getAttendanceData;
        vm.getAttendanceDataSuccess = getAttendanceDataSuccess;
        vm.Failed = Failed;
        vm.IsParents = 0;
        vm.IsOthers = 0;
        getAttendanceData();
        function getAttendanceData() {
            $scope.query.ClassId = localStorage.ClassId
            apiService.post('/api/StudentAttendance/GetAllStudentAttendance', $scope.query,
                getAttendanceDataSuccess,
                Failed);
        }
        function getAttendanceDataSuccess(result) {
            $scope.EnrolledStudentList = result.data.Content;
        }
        function Failed(result) {
            notificationService.displaymessage('Please try again after some time.');
        }


        function cancelClick() {
            $mdDialog.cancel();
        }
        getParents();
        function getParents() {
            var model = {
                ID: items.attendanceData.StudentId
            }
            vm.promise = CommonService.getParentListByStudentId(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.ParentList = null;
                    vm.ParentList = response.Content;
                }
            });
        };
        vm.getOtherDroppedByData = getOtherDroppedByData;
        function getOtherDroppedByData() {
            vm.IDViewModel = {};
            vm.IDViewModel.id = localStorage.agencyId;
            apiService.post('/api/GeneralDroppedBy/GetGeneralDroppedBy', vm.IDViewModel,
                getOtherDroppedByDataSuccess,
                Failed);
        }
        function getOtherDroppedByDataSuccess(result) {
            vm.OtherDroppedByList = result.data.Content;
        }
        vm.getOtherDroppedByData();
        vm.checkOthers = checkOthers;
        function checkOthers(id) {
            if (id == 0) {
                vm.IsOthers = 1;
            }
        }
        vm.SecurityKeyCheckVM = {
            AgencyId: localStorage.agencyId
        };
        
        function SaveAttendance(model, toggle) {
            model.StudentId = items.attendanceData.StudentId;
            model.AgencyId = localStorage.agencyId;
            if (toggle == 1) {
                if (vm.IsParents == 1) {
                    model.DropedByOtherId = model.DropedByOtherId
                    ///Code for Sending Message edited by Parag balapure
                    var OtherData = $filter('filter')(vm.OtherDroppedByList, { ID: model.DropedByOtherId });
                    model.DropedByOtherName = OtherData[0].Name;
                    model.GuradianEmail = vm.ParentList[0].EmailId;
                    model.GuradianSMSCarrier = vm.ParentList[0].SMSCarrier.CarrierAddress;
                    model.GuradianPhone = vm.ParentList[0].Mobile;
                    model.ParentName = vm.ParentList[0].FullName;
                    model.StudentName = $scope.attendanceDataDetail.StudentName;
                    //////
                } else {
                    model.DropedById = model.ParentId;

                    ///Code for Sending Message edited by Parag balapure
                    var GuardianData = $filter('filter')(vm.ParentList, { ID: model.ParentId });
                    model.GuradianEmail = GuardianData[0].EmailId;
                    model.GuradianSMSCarrier = GuardianData[0].SMSCarrier.CarrierAddress;
                    model.GuradianPhone = GuardianData[0].Mobile;
                    model.ParentName = GuardianData[0].FullName;
                    model.StudentName = $scope.attendanceDataDetail.StudentName;
                    /////
                }

                vm.promise = AttendanceService.AddStudentAttendance(model);
            }
            else {
                model.ID = items.attendanceData.ID;
                if (vm.IsOthers == 0) {
                    model.PickupById = model.ParentId;
                    ///Code for Sending Message edited by Parag balapure
                    var GuardianData = $filter('filter')(vm.ParentList, { ID: model.ParentId });
                    model.GuradianEmail = GuardianData[0].EmailId;
                    model.GuradianSMSCarrier = GuardianData[0].SMSCarrier.CarrierAddress;
                    model.GuradianPhone = GuardianData[0].Mobile;
                    model.ParentName = GuardianData[0].FullName;
                    model.StudentName = $scope.attendanceDataDetail.StudentName;
                    /////
                    vm.promise = AttendanceService.AddStudentPickUpAttendance(model);
                } else {
                    if (model.SecurityKey == items.attendanceData.SecurityKey) {
                        model.PickupByOtherName = model.PickupByOtherName;
                        ///Code for Sending Message edited by Parag balapure
                        model.GuradianEmail = vm.ParentList[0].EmailId;
                        model.GuradianSMSCarrier = vm.ParentList[0].SMSCarrier.CarrierAddress;
                        model.GuradianPhone = vm.ParentList[0].Mobile;
                        model.ParentName = vm.ParentList[0].FullName;
                        model.StudentName = $scope.attendanceDataDetail.StudentName;
                        ///Code for Sending Message edited by Parag balapure
                        vm.promise = AttendanceService.AddStudentPickUpAttendance(model);
                    } else {
                        notificationService.displaymessage('Please enter valid code word.');
                        return;
                    }
                }
            }
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.Message);
                    cancelClick();
                    $state.go('triangular.attendance', {}, { reload: true });
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        function reset() {
            vm.AttendanceData = {
                StaffScheduleId: 0,
                InTime: new Date(1970, 0, 1, 14, 57, 0),
                OutTime: new Date(1970, 0, 1, 14, 57, 0),
                OnLeave: '',
            };
        };
    }
    function EditAttendanceController($scope, $mdDialog, $state, $http, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, CommonService, AgencyService, AttendanceService, filerService, items, notificationService, apiService) {
        var vm = this;
        vm.getInstructors = getInstructors;
        vm.getAttendanceDetailsbyId = getAttendanceDetailsbyId;
        vm.UpdateAttendance = UpdateAttendance;
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };
        vm.AttendanceData = {
            StaffScheduleId: 0,
            InTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            OutTime: new Date(new Date().toDateString() + ' ' + '00:00'),
            OnLeave: '',
        };
        $scope.Id = 0;
        getInstructors();
        function getInstructors() {
            vm.promise = CommonService.getScheduleInstructors();
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllScheduledStaff = null;
                    vm.AllScheduledStaff = eval(response.Content.staffScheduleList);
                }
            });
        }
        function getAttendanceDetailsbyId() {
            vm.AttendanceData.InTime = (moment(items.attendanceData.InTime).format('HH:mm:ss'));
            vm.AttendanceData.OutTime = (moment(items.attendanceData.OutTime).format('HH:mm:ss'));
            vm.AttendanceData.ID = items.attendanceData.ID;
            vm.AttendanceData.StaffScheduleId = items.attendanceData.StaffScheduleId;
            $scope.Id = items.attendanceData.ID;
        }
        function UpdateAttendance(sheduleData) {
            vm.AttendanceData.InTime = (moment(vm.AttendanceData.InTime).format('HH:mm:ss'));
            vm.AttendanceData.OutTime = (moment(vm.AttendanceData.OutTime).format('HH:mm:ss'));
            vm.AttendanceData.AttendanceMarkedId = AgencyService.AgencyId;
            vm.promise = AttendanceService.AddAttendance(vm.AttendanceData);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.ReturnMessage[0]);
                    reset();
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
        function reset() {
            vm.AttendanceData = {
                StaffScheduleId: 0,
                InTime: new Date(1970, 0, 1, 14, 57, 0),
                OutTime: new Date(1970, 0, 1, 14, 57, 0),
                OnLeave: '',
            };
        };
    }
})();