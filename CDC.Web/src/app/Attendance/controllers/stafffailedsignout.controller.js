(function () {
    'use strict';
    angular
        .module('app.attendance')
        .controller('StaffFailedSignoutController', StaffFailedSignoutController)
    function StaffFailedSignoutController($scope, $rootScope, $mdDialog, $state, $stateParams, $http, $localStorage,
        AttendanceService, notificationService, CommonService, filerService, HOST_URL, imageUploadService) {
        var vm = this;
        $scope.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        $scope.todaysDate = new Date();
        vm.detailStaffInfo = detailStaffInfo;
        vm.reset = reset;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.ImageUrlPath = HOST_URL.url;
        vm.GetStaffFailedSignoutList = GetStaffFailedSignoutList;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            Name: '',
            Date: vm.CurrentDate,
            ClassId: 0,
            AgencyId: localStorage.agencyId,
            TimeZoneName: localStorage.TimeZone
        };
        function detailStaffInfo(staff) {
            StaffService.SetId(staff);
            staff.RedirectFrom = "FailedSignoutStaff";
            $state.go("triangular.staffdetails", { obj: staff });
        };
        GetStaffFailedSignoutList();
        $rootScope.$on("GetStaffFailedSignoutList", function () {
            GetStaffFailedSignoutList();
        });
        vm.StaffFailedSignoutCount = 0;
        function GetStaffFailedSignoutList() {
            debugger
            vm.promise = AttendanceService.GetStaffFailedSignoutListService(vm.query);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.StaffFailedSignoutList = null;
                    vm.StaffFailedSignoutCount = 0;
                    vm.StaffFailedSignoutList = response.Content;
                    vm.StaffFailedSignoutCount = response.TotalRows;
                    $scope.selected = [];
                }
                else {
                    if (response.Content.length == 0) {
                        vm.StaffFailedSignoutList = [];
                        vm.StaffFailedSignoutCount = 0;
                        notificationService.displaymessage('No Staff failed signout found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get staff failed signout at the moment. Please try again after some time.');
                    }
                }
            });
        };


        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        function reset() {
            $scope.query.ParentName = '';
            $scope.StaffName = "";
            GetStaffFailedSignoutList();
        };

        function cancelClick() {
            $mdDialog.cancel();
        }
    }

})();