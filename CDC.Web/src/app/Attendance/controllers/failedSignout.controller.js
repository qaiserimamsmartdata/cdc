(function () {
    'use strict';

    angular
        .module('app.attendance')
        .controller('FailedSignoutController', FailedSignoutController);

    /* @ngInject */
    function FailedSignoutController($scope, $mdDialog, $state, $http, $stateParams,StaffService, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, CommonService, AttendanceService, filerService, apiService, $localStorage, notificationService, HOST_URL) {
        var vm = this;
        $scope.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        vm.staffAttendanceData = staffAttendanceData;
        vm.detailStaffInfo=detailStaffInfo;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.toggleRight1 = filerService.toggleRight1();
        vm.isOpenRight1 = filerService.isOpenRight1();
        vm.close1 = filerService.close1();
        vm.ImageUrlPath = HOST_URL.url;
        $scope.todaysDate=new Date();
        vm.CurrentDate = new Date();
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.staffCount = 0;
        $scope.selected = [];
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StaffName: '',
            AttendanceDate: vm.CurrentDate,
            Date: vm.CurrentDate,
            ClassId: 0,
            StaffId:localStorage.staffId,
            AgencyId: localStorage.agencyId,
            TimeZoneName:localStorage.TimeZone
        };
        vm.GetParticipantFailedSignout = GetParticipantFailedSignout;
        vm.GetStaffFailedSignout = GetStaffFailedSignout;
        vm.reset = reset;
        vm.reset1 = reset1;
        vm.detailParticipantInfo = detailParticipantInfo;

        GetParticipantFailedSignout();
        GetStaffFailedSignout();

       function detailParticipantInfo(data) {
            data.RedirectFrom = "FailedSignoutParticipant";
            // ClassService.SetId(data.ID);
             if(localStorage.Portal=='AGENCY')
            $state.go('triangular.studentdetails', { obj: data });
            else
            $state.go('triangular.parentstudentdetails', { obj: data });
        };
         function detailStaffInfo(event,staff) {
             debugger
            StaffService.SetId(staff);
            staff.RedirectFrom = "FailedSignoutStaff";
            if(localStorage.Portal=='AGENCY')
            $state.go("triangular.staffdetails", { obj: staff });
            else
            $state.go("triangular.staffportalstaffdetails", { obj: staff });
        };
        function staffAttendanceData() {
            vm.promise = AttendanceService.StaffAttendanceData();
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.StaffAttendanceList = null;
                    vm.StaffAttendanceList = eval(response);
                }
                vm.close();
            });
        }

        function GetStaffFailedSignout() {
            $scope.StaffName=$scope.query.StaffName==""?"All":$scope.query.StaffName;
            $scope.DisplayDate=$scope.query.AttendanceDate;
            localStorage.ClassId = $scope.query.ClassId;
            apiService.post('/api/StaffAttendance/GetStaffFailedSignout', $scope.query,
                getStaffAttendanceDataSuccess,
                Failed);
            vm.close1();
        }

       vm.showProgressbar=true;
       vm.staffCount=0;
        function getStaffAttendanceDataSuccess(result) {
            debugger
            $scope.StaffList = result.data.Content;
            vm.staffCount = result.data.TotalRows;
            vm.showProgressbar=false;
            if (result.data.TotalRows > 0) {
                vm.NoStaffFailedSignOut = "false";
            }
            else {
                vm.NoStaffFailedSignOut = "true";
            }

        }

        function Failed() {
            notificationService.displaymessage('Unable to retrieve staff failed signout list.');
        }
        function GetParticipantFailedSignout() {
            $scope.ParticipantName=$scope.query.Name==undefined?"All":$scope.query.Name;
            $scope.DisplayDate=$scope.query.Date;
            localStorage.ClassId = $scope.query.ClassId;
            apiService.post('/api/StudentAttendance/GetParticipantFailedSignout', $scope.query,
                getAttendanceDataSuccess,
                Failed);
            vm.close();
        }
        

        vm.showProgressbar=true;
        function getAttendanceDataSuccess(result) {
            $scope.EnrolledStudentList = result.data.Content;
            vm.StudentCount = result.data.TotalRows;
            vm.showProgressbar=false;
            if (result.data.TotalRows > 0) {
                vm.NoParticipantFailedSignOut = false;
            }
            else {
                vm.NoParticipantFailedSignOut = true;
            }
        }
        function Failed(result) {
            notificationService.displaymessage('Please try again after some time.');
        }

        function reset() {
            $scope.query.ClassId = 0;
            $scope.query.Name = undefined;
            $scope.ParticipantName="";
            $scope.StaffName="";
            $scope.query.Date = vm.CurrentDate;
            GetParticipantFailedSignout();
        };

        function reset1() {
            $scope.query.ClassId = 0;
            $scope.query.StaffName = '';
            $scope.query.AttendanceDate = vm.CurrentDate;
            GetStaffFailedSignout();
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };

    }
})();