(function () {
    'use strict';

    angular
        .module('app.attendance')
        .controller('staffAttendanceController', staffAttendanceController);

    function staffAttendanceController($scope, $mdDialog, StaffService, $state, $http, CommonService, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, filerService, notificationService, apiService, $localStorage, HOST_URL) {

        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 1;

        $scope.options = {
            hstep: [1, 2, 3],
            mstep: [1, 5, 10, 15, 25, 30]
        };
        $scope.todaysDate = new Date();
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        // $scope.changed = function () {
        //     $log.log('Time changed to: ' + $scope.mytime);
        // };

        $scope.clear = function () {
            $scope.mytime = null;
        };



        $scope.selected = [];
        var vm = this;
        $scope.$storage = localStorage;
        if (localStorage.agencyId == undefined || null) {
            $state.go('authentication.login');
            notificationService.displaymessage("You must login first.");
            return;
        }
        localStorage.ParticipantAttendancePage = false;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.GetStaffList = GetStaffList;
        vm.ImageUrlPath = HOST_URL.url;
        vm.saveStaffAttendance = saveStaffAttendance;
        vm.saveStaffAttendanceSuccess = saveStaffAttendanceSuccess;
        vm.resetSearch = resetSearch;
        vm.detailStaffInfo = detailStaffInfo;
        vm.columns = {};
        vm.columns = {
            Name: 'Name',
            IsTimeClockUser: 'Time Clock User',
            Date: 'Attendance Date',
            IsPresent: 'On Leave',
            Comment: 'Comment',
            ID: 'ID',
            CheckInTime: 'Check-In Time',
            CheckOutTime: 'Check-Out Time',
            ProfilePic: 'Image'
        };
        vm.AttendanceDate = new Date();
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-StaffName',
            page: 1,
            StaffName: '',
            AttendanceDate: vm.AttendanceDate,
            ClassId: localStorage.ClassId == undefined ? 0 : localStorage.ClassId,
            AgencyId: localStorage.agencyId,
            TimeZoneName: localStorage.TimeZone
        };
        GetStaffList();
        vm.onSwitchChange = onSwitchChange;
        function onSwitchChange(myIndex, myval) {
            vm.staffList[myIndex].Comment = " ";
            vm.staffList[myIndex].InTime = null;
            vm.staffList[myIndex].OutTime = null;
        };
        vm.staffList = {
            OnLeave: false
        };
        function detailStaffInfo(staff) {

            staff.ID = staff.StaffId;
            StaffService.SetId(staff);
            staff.RedirectFrom = "StaffAttendance";
            $state.go("triangular.staffdetails", { obj: staff });
        };
        function GetStaffList() {
            $scope.StaffName = $scope.query.StaffName == "" ? "All" : $scope.query.StaffName;
            $scope.DisplayDate = $scope.query.AttendanceDate;
            apiService.post('/api/StaffAttendance/GetAllStaffAttendance', $scope.query, getStaffListSuccess, Failed)
        }
        function getStaffListSuccess(result) {

            if (result.data.Content.length > 0) {
                vm.NoData = false;
                vm.staffList = result.data.Content;
                vm.staffCount = result.data.TotalRows;
                angular.forEach(vm.staffList, function (value) {
                    if (value.OnLeave == null)
                        value.OnLeave = false;
                    if (value.InTime != null)
                        value.InTime = new Date(new Date().toDateString() + ' ' + value.InTime);
                    if (value.OutTime != null)
                        value.OutTime = new Date(new Date().toDateString() + ' ' + value.OutTime);
                });

            }
            else {
                vm.NoData = true;
                vm.staffList = [];
                vm.staffCount = 0;
                notificationService.displaymessage('No Staff attendance list found.');
            }

            vm.close();
        }
        function Failed() {
            notificationService.displaymessage('Unable to retrieve staff list.');
        }

        function saveStaffAttendance(staff) {

            if (staff.OnLeave == true && (staff.Comment == "" || staff.Comment == " ")) {
                notificationService.displaymessage('Please enter comment.');
                return;
            }
            else if (staff.OnLeave == false && (staff.InTime == "Invalid Date" || staff.InTime == null)) {
                notificationService.displaymessage('Please enter valid CheckInTime.');
                return;
            }
            if (staff.InTime == "Invalid Date" || staff.InTime == null) {
                staff.InTime = null;
            }
            else {
                staff.InTime = (moment(staff.InTime).format('HH:mm:ss'));
                // if(staff.InTime<=new Date())
                // {
                //       notificationService.displaymessage('Start time should be less than or equal current time.');
                //       return;
                // }
            }
            if (staff.OutTime == "Invalid Date" || staff.OutTime == null) {
                staff.OutTime = null;
            }
            else {
                staff.OutTime = (moment(staff.OutTime).format('HH:mm:ss'));
                if ((staff.InTime != "Invalid Date" || staff.InTime != null) && (staff.OutTime != "Invalid Date" || staff.OutTime != null)) {
                    if (staff.OutTime < staff.InTime) {
                        notificationService.displaymessage('End time should be greater than start time.');
                        return;
                    }
                }
            }

            staff.TimeZone = localStorage.TimeZone;
            apiService.post('/api/StaffAttendance/AddAttendance', staff, saveStaffAttendanceSuccess, Failed)
        }
        function saveStaffAttendanceSuccess() {
            notificationService.displaymessage('Attendance marked successsfully');
            GetStaffList();
        }
        function resetSearch() {
            $scope.query.StaffName = '';
            $scope.StaffName = "";
            $scope.query.AttendanceDate = vm.AttendanceDate;
            GetStaffList();
        }

        vm.NavtoStaffDetail = NavtoStaffDetail;
        function NavtoStaffDetail() {

            $state.go('triangular.staffdetails');
        }
    }

})();
