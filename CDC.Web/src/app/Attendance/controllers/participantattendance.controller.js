(function () {
    'use strict';

    angular
        .module('app.attendance')
        .controller('ParticipantAttendanceController', ParticipantAttendanceController)
        .controller('DialogCtrl', DialogCtrl);


    /* @ngInject */
    function ParticipantAttendanceController($scope, $http, $filter, $state, $q, notificationService, apiService, filerService, CommonService, AttendanceService, $localStorage, HOST_URL, $mdDialog) {
        var vm = this;
        localStorage.ParticipantAttendancePage = true;
        $scope.$storage = localStorage;
        if (localStorage.agencyId == undefined || null) {
            $state.go('authentication.login');
            notificationService.displaymessage("You must login first.");
            return;
        }
        vm.resetSearch = resetSearch;
        vm.reset = reset;
        vm.ResetAll = ResetAll;
        vm.getAttendanceData = getAttendanceData;
        vm.getAttendanceDataSuccess = getAttendanceDataSuccess;
        vm.Failed = Failed;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.ImageUrlPath = HOST_URL.url;
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.staffCount = 0;
        vm.CurrentDate = new Date();
        vm.IsParents = 0;
        vm.IsOthers = 0;
        vm.checkOthers = checkOthers;
        $scope.selected = [];
        vm.MaxDate = new Date();
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            Name: '',
            ClassId: 0,
            Date: vm.CurrentDate,
            AgencyId: localStorage.agencyId,
            TimeZoneName: localStorage.TimeZone
        };

        vm.DisplayDate = vm.CurrentDate;
        vm.detailParticipantInfo = detailParticipantInfo;
        GetClassMaster();
        function GetClassMaster() {
            var model = {
                AgencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getClassCombo(model);
            vm.promise.then(function (response) {
                debugger
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }
        function detailParticipantInfo(data) {
            data.RedirectFrom = "ParticipantAttendance";
            if (localStorage.Portal == 'AGENCY')
                $state.go('triangular.studentdetails', { obj: data });
            else
                $state.go('triangular.parentstudentdetails', { obj: data });
        };
        function ResetAll($index) {
            vm.EnrolledStudentList[$index].DropedById = null;
            vm.EnrolledStudentList[$index].DropedByOtherId = null;
            vm.EnrolledStudentList[$index].PickupById = null;
            vm.EnrolledStudentList[$index].CheckSecurityKey = "";
            vm.EnrolledStudentList[$index].PickupByOtherName = "";
        }
        function checkOthers(id, $index) {
            if (id == 0) {
                vm.EnrolledStudentList[$index].IsOthers = 1;
            }
            else
                vm.EnrolledStudentList[$index].IsOthers = 0;
        }
        vm.getOtherDroppedByData = getOtherDroppedByData;
        getOtherDroppedByData();
        function getOtherDroppedByData() {
            vm.IDViewModel = {};
            vm.IDViewModel.id = localStorage.agencyId;
            apiService.post('/api/GeneralDroppedBy/GetGeneralDroppedBy', vm.IDViewModel,
                getOtherDroppedByDataSuccess,
                Failed);
        }
        function getOtherDroppedByDataSuccess(result) {
            vm.OtherDroppedByList = result.data.Content;
        }

        function reset($index) {
            $scope.StaffName = "";
            vm.EnrolledStudentList[$index].IsParents = false;
            vm.EnrolledStudentList[$index].DropedById = 0;
        };

        function resetSearch() {
            $scope.query.Name = '';
            $scope.query.ClassId = 0;
            $scope.query.Date = new Date();
            getAttendanceData();
        }
        vm.EnrolledStudentList = [];
        vm.EnrolledStudentList.ParentList = {};
        vm.EnrolledStudentList = {
            OnLeave: false
        };

        getAttendanceData();
        vm.EnrolledStudentListCount = 0;
        function getAttendanceData() {
            vm.showProgressbar = true;
            $scope.ParticipantName = $scope.query.Name == "" ? "All" : $scope.query.Name;

            var data = null;
            data = $filter('filter')(vm.AllClasses, parseInt($scope.query.ClassId), true);
            $scope.ClassName = data == null || undefined ? "All" : data.length > 0 ? data[0].ClassName : "All";

            vm.DisplayDate = $scope.query.Date;
            localStorage.ClassId = $scope.query.ClassId;
            apiService.post('/api/StudentAttendance/GetAllStudentAttendance', $scope.query,
                getAttendanceDataSuccess,
                Failed);
            vm.close();
        }
        function getAttendanceDataSuccess(result) {

            vm.EnrolledStudentList = result.data.Content;
            vm.EnrolledStudentListCount = result.data.TotalRows;
            // vm.EnrolledStudentList.forEach(function (element) {
            //     var model = {
            //         ID: element.StudentId
            //     }
            //     vm.promise = CommonService.getParentListByStudentId(model);
            //     vm.promise.then(function (response) {
            //         if (response.Content.length > 0) {
            //             vm.ParentList = null;
            //             vm.ParentList = response.Content;
            //             if (element.OnLeave == null)
            //                 element.OnLeave = false;
            //             element.ParentList = vm.ParentList;
            //             if (element.ImagePath == null)
            //                 element.ImagePath = "assets/images/avatars/avatar-5.png";
            //         }
            //     });
            // }, this);
            vm.showProgressbar = false;
        };

        function Failed(result) {
            vm.showProgressbar = false;
            notificationService.displaymessage('Please try again after some time.');
        }
        vm.SaveAttendance = SaveAttendance;
        vm.showProgressbar = true;
        function SaveAttendance(model, toggle, onleave, $index) {


            vm.showProgressbar = true;
            model.TimeZone = localStorage.TimeZone;
            model.AttendanceDate = $scope.query.Date;
            model.AgencyId = localStorage.agencyId;
            var PrimaryGuardianData = $filter('filter')(model.ParentList, { IsPrimary: true });
            var PrimaryParentID = PrimaryGuardianData[0].ID;
            var PrimaryGuradianEmail = PrimaryGuardianData[0].EmailId;
            var PrimaryGuradianSMSCarrier = PrimaryGuardianData[0].SMSCarrier.CarrierAddress;
            var PrimaryGuradianPhone = PrimaryGuardianData[0].Mobile;
            var PrimaryParentName = PrimaryGuardianData[0].FullName;
            var PrimaryDropByRelationName = PrimaryGuardianData[0].RelationName;
            var PrimaryPickedByRelationName = PrimaryGuardianData[0].RelationName;
            var IsParticipantAttendanceMailReceived = PrimaryGuardianData[0].IsParticipantAttendanceMailReceived == null ? false : PrimaryGuardianData[0].IsParticipantAttendanceMailReceived;
            model.IsParticipantAttendanceMailReceived = IsParticipantAttendanceMailReceived;
            ///////////////
            if (toggle == 1) {
                if (model.IsParents == true) {

                    if (model.DropedByOtherId == undefined || model.DropedByOtherId == null) {
                        notificationService.displaymessage('Select  Mode of convenience.');
                        return;
                    }
                    model.DropedByOtherId = model.DropedByOtherId
                    //Code for Sending Message edited by Parag balapure
                    var OtherData = $filter('filter')(vm.OtherDroppedByList, { ID: model.DropedByOtherId });
                    model.DropedByOtherName = OtherData[0].Name;
                    model.GuradianEmail = PrimaryGuradianEmail;
                    model.GuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                    model.GuradianPhone = PrimaryGuradianPhone;
                    model.ParentName = PrimaryParentName;
                    model.DropByRelationName = PrimaryDropByRelationName;

                    //for Primary
                } else {
                    if (onleave == true) {
                        model.DropedById = null;
                        model.GuradianEmail = PrimaryGuradianEmail;
                        model.GuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                        model.GuradianPhone = PrimaryGuradianPhone;
                        model.ParentName = PrimaryParentName;
                        model.DropByRelationName = PrimaryDropByRelationName;
                        vm.EnrolledStudentList[$index].DisableOnLeave = true;
                    }
                    else {
                        if (model.DropedById == undefined || model.DropedById == null) {
                            notificationService.displaymessage('Select Parent.');
                            return;
                        }
                        var GuardianData = $filter('filter')(model.ParentList, { ID: model.DropedById });
                        model.GuradianEmail = GuardianData[0].EmailId;
                        model.GuradianSMSCarrier = GuardianData[0].SMSCarrier.CarrierAddress;
                        model.GuradianPhone = GuardianData[0].Mobile;
                        model.ParentName = GuardianData[0].FullName;
                        model.DropByRelationName = GuardianData[0].RelationName;

                        //for Primary

                        if (PrimaryParentID != model.DropedById) {
                            model.PrimaryGuradianEmail = PrimaryGuradianEmail;
                            model.PrimaryGuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                            model.PrimaryGuradianPhone = PrimaryGuradianPhone;
                            model.PrimaryParentName = PrimaryParentName;
                            model.PrimaryDropByRelationName = PrimaryDropByRelationName;


                        }
                    }
                }
                vm.promise = AttendanceService.AddStudentAttendance(model);
            }
            else {
                if (model.IsOthers == 0) {
                    if (model.PickupById == undefined || model.PickupById == null) {
                        notificationService.displaymessage('Select Parent.');
                        return;
                    }
                    ///Code for Sending Message edited by Parag balapure
                    var GuardianData = $filter('filter')(model.ParentList, { ID: model.PickupById });
                    model.GuradianEmail = GuardianData[0].EmailId;
                    model.GuradianSMSCarrier = GuardianData[0].SMSCarrier.CarrierAddress;
                    model.GuradianPhone = GuardianData[0].Mobile;
                    model.ParentName = GuardianData[0].FullName;
                    model.PickedByRelationName = GuardianData[0].RelationName;
                    //for Primary
                    if (PrimaryParentID != model.PickupById) {
                        model.PrimaryGuradianEmail = PrimaryGuradianEmail;
                        model.PrimaryGuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                        model.PrimaryGuradianPhone = PrimaryGuradianPhone;
                        model.PrimaryParentName = PrimaryParentName;
                        model.PrimaryPickedByRelationName = PrimaryPickedByRelationName;
                    }


                    vm.promise = AttendanceService.AddStudentPickUpAttendance(model);
                } else {
                    if (model.CheckSecurityKey == "") {
                        notificationService.displaymessage('Enter pin number.');
                        return;
                    }
                    if (model.PickupByOtherName == "") {
                        notificationService.displaymessage('Enter person name.');
                        return;
                    }
                    if (model.SecurityKey == model.CheckSecurityKey) {
                        //Code for Sending Message edited by Parag balapure
                        model.GuradianEmail = PrimaryGuradianEmail;
                        model.GuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                        model.GuradianPhone = PrimaryGuradianPhone;
                        model.ParentName = PrimaryParentName;
                        model.PickedByRelationName = PrimaryPickedByRelationName;
                        vm.promise = AttendanceService.AddStudentPickUpAttendance(model);
                    } else {
                        notificationService.displaymessage('Please enter valid pin number.');
                        return;
                    }
                }
            }
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    notificationService.displaymessage(response.Message);
                    // getAttendanceData();
                }
                else {
                    vm.showProgressbar = false;
                    //  getAttendanceData();
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });

        }
        vm.cancelClick = function () {
            $mdDialog.cancel();
        };
        vm.openDropPickupByListDialogue = openDropPickupByListDialogue;
        function openDropPickupByListDialogue(data) {
            //used toggle value 2 for signed out,1 for signed in,
            if(vm.showProgressbar)return;
            var toggle = 2;
            if (data.InTime == null)
                toggle = 1;
            if (data.InTime != null && data.OutTime == null)
                toggle = 0;
            if (toggle == 2)
                return;
            $mdDialog.show({
                controller: DialogCtrl,
                controllerAs: 'vm',
                templateUrl: 'app/Attendance/views/dropPickupList.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                Items: { attendanceData: data, toggle: toggle, convenienceModeList: vm.OtherDroppedByList }
            }).then(function (data) {
                getAttendanceData();
            }, function (err) {
                // console.error(err);
            }).finally(function () {
                // finally block is optional for cleanup
            });
        }
        //  function openDropPickupByListDialogue(attendanceData) {

        //     $mdDialog.show({
        //         controller: DropPickupController,
        //         controllerAs: 'vm',
        //         templateUrl: 'app/familiesDetails/views/addParticipant.tmpl.html',
        //         parent: angular.element(document.body),
        //         escToClose: true,
        //         clickOutsideToClose: true,
        //         fullscreen: $scope.customFullscreen,
        //         data: { attendanceData: attendanceData }// Only for -xs, -sm breakpoints.
        //     });
        // };

    }
    function DialogCtrl($timeout, $q, $scope, $mdDialog, $filter, notificationService, AttendanceService, CommonService, Items) {
        var vm = this;
        vm.toggle = Items.toggle;
        vm.attendanceData = Items.attendanceData;
        vm.convenienceModeList = Items.convenienceModeList;
        vm.getParentList = getParentList;
        getParentList();
        vm.cancelClick = function ($event) {
            $mdDialog.cancel();
        };
        vm.finish = function ($event) {
            $mdDialog.hide();
        };
        vm.showProgressbar=true;
        vm.showSaveProgress=false;
        function getParentList() {
            var model = {
                ID: vm.attendanceData.StudentId
            }
            vm.promiseParentList = CommonService.getParentListByStudentId(model);
            vm.promiseParentList.then(function (response) {
                if (response.Content.length > 0) {
                    if (vm.attendanceData.OnLeave == null)
                        vm.attendanceData.OnLeave = false;
                    vm.attendanceData.ParentList = response.Content;
                    vm.allParentList = vm.attendanceData.ParentList;
                    vm.showProgressbar=false;
                }
            });
        }
        vm.SaveAttendance = SaveAttendance;
        function SaveAttendance(dropByEntity, model, isDoneByOthers, onleave, $index) {
            
           vm.showSaveProgress=true;
            model = Items.attendanceData;
            model.TimeZone = localStorage.TimeZone;
            model.AgencyId = localStorage.agencyId;
            model.IsParents = false;
            var PrimaryGuardianData = $filter('filter')(model.ParentList, { IsPrimary: true });
            var PrimaryParentID = PrimaryGuardianData[0].ID;
            var PrimaryGuradianEmail = PrimaryGuardianData[0].EmailId;
            var PrimaryGuradianSMSCarrier = PrimaryGuardianData[0].SMSCarrier.CarrierAddress;
            var PrimaryGuradianPhone = PrimaryGuardianData[0].Mobile;
            var PrimaryParentName = PrimaryGuardianData[0].FullName;
            var PrimaryDropByRelationName = PrimaryGuardianData[0].RelationName;
            var PrimaryPickedByRelationName = PrimaryGuardianData[0].RelationName;
            var IsParticipantAttendanceMailReceived = PrimaryGuardianData[0].IsParticipantAttendanceMailReceived == null ? false : PrimaryGuardianData[0].IsParticipantAttendanceMailReceived;
            model.IsParticipantAttendanceMailReceived = IsParticipantAttendanceMailReceived;
            ///////////////
            if (vm.toggle == 1) {
                if (isDoneByOthers == true) {
                    model.DropedByOtherId = dropByEntity.ID
                    if (model.DropedByOtherId == undefined || model.DropedByOtherId == null) {
                        notificationService.displaymessage('Select  Mode of convenience.');
                        return;
                    }

                    //Code for Sending Message edited by Parag balapure
                    var OtherData = $filter('filter')(vm.convenienceModeList, { ID: model.DropedByOtherId });
                    model.DropedByOtherName = OtherData[0].Name;
                    model.GuradianEmail = PrimaryGuradianEmail;
                    model.GuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                    model.GuradianPhone = PrimaryGuradianPhone;
                    model.ParentName = PrimaryParentName;
                    model.DropByRelationName = PrimaryDropByRelationName;

                    //for Primary
                } else {
                    if (onleave == true) {
                        model.DropedById = null;
                        model.GuradianEmail = PrimaryGuradianEmail;
                        model.GuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                        model.GuradianPhone = PrimaryGuradianPhone;
                        model.ParentName = PrimaryParentName;
                        model.DropByRelationName = PrimaryDropByRelationName;
                        vm.EnrolledStudentList[$index].DisableOnLeave = true;
                    }
                    else {
                        model.DropedById = dropByEntity.ID;
                        if (model.DropedById == undefined || model.DropedById == null) {
                            notificationService.displaymessage('Select Parent.');
                            return;
                        }
                        var GuardianData = $filter('filter')(model.ParentList, { ID: model.DropedById });
                        model.GuradianEmail = GuardianData[0].EmailId;
                        model.GuradianSMSCarrier = GuardianData[0].SMSCarrier.CarrierAddress;
                        model.GuradianPhone = GuardianData[0].Mobile;
                        model.ParentName = GuardianData[0].FullName;
                        model.DropByRelationName = GuardianData[0].RelationName;

                        //for Primary

                        if (PrimaryParentID != model.DropedById) {
                            model.PrimaryGuradianEmail = PrimaryGuradianEmail;
                            model.PrimaryGuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                            model.PrimaryGuradianPhone = PrimaryGuradianPhone;
                            model.PrimaryParentName = PrimaryParentName;
                            model.PrimaryDropByRelationName = PrimaryDropByRelationName;


                        }
                    }
                }
                vm.promise = AttendanceService.AddStudentAttendance(model);
            }
            else {

                if (isDoneByOthers == false) {
                    model.PickupById = dropByEntity.ID;
                    if (model.PickupById == undefined || model.PickupById == null) {
                        notificationService.displaymessage('Select Parent.');
                        return;
                    }
                    ///Code for Sending Message edited by Parag balapure

                    var GuardianData = $filter('filter')(model.ParentList, { ID: model.PickupById });
                    model.GuradianEmail = GuardianData[0].EmailId;
                    model.GuradianSMSCarrier = GuardianData[0].SMSCarrier.CarrierAddress;
                    model.GuradianPhone = GuardianData[0].Mobile;
                    model.ParentName = GuardianData[0].FullName;
                    model.PickedByRelationName = GuardianData[0].RelationName;
                    //for Primary
                    if (PrimaryParentID != model.PickupById) {
                        model.PrimaryGuradianEmail = PrimaryGuradianEmail;
                        model.PrimaryGuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                        model.PrimaryGuradianPhone = PrimaryGuradianPhone;
                        model.PrimaryParentName = PrimaryParentName;
                        model.PrimaryPickedByRelationName = PrimaryPickedByRelationName;
                    }


                    vm.promise = AttendanceService.AddStudentPickUpAttendance(model);
                } else {
                    if (model.CheckSecurityKey == "" || model.CheckSecurityKey == undefined) {
                        notificationService.displaymessage('Enter pin number.');
                        vm.showSaveProgress=false;
                        return;
                    }
                    if (model.PickupByOtherName == "" || model.PickupByOtherName == undefined) {
                        notificationService.displaymessage('Enter person name.');
                        vm.showSaveProgress=false;
                        return;
                    }
                    if (model.SecurityKey == model.CheckSecurityKey) {
                        //Code for Sending Message edited by Parag balapure
                        model.GuradianEmail = PrimaryGuradianEmail;
                        model.GuradianSMSCarrier = PrimaryGuradianSMSCarrier;
                        model.GuradianPhone = PrimaryGuradianPhone;
                        model.ParentName = PrimaryParentName;
                        model.PickedByRelationName = PrimaryPickedByRelationName;
                        vm.promise = AttendanceService.AddStudentPickUpAttendance(model);
                    } else {

                        notificationService.displaymessage('Please enter valid pin number.');
                          vm.showSaveProgress=false;
                        return;
                    }
                }
            }
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                      vm.showSaveProgress=false;
                    notificationService.displaymessage(response.Message);
                    vm.finish();
                }
                else {
                   vm.showSaveProgress=false;
                    vm.cancelClick();
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });

        }
    }
})();