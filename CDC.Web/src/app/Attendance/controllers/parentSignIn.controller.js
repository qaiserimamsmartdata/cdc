(function () {
    'use strict';

    angular
        .module('app.attendance')
        .controller('ParentSignInController', ParentSignInController);
        


    /* @ngInject */
    function ParentSignInController($scope, $http, $filter, $state, $q, notificationService, apiService, filerService, CommonService, AttendanceService, $localStorage, HOST_URL, $mdDialog) {
       var vm=this;
       vm.CurrentDate = new Date();
       vm.LoginLogo = "assets/images/logo.svg";
       vm.close = filerService.close();
         $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            Name: '',
            ClassId: 0,
            Date: vm.CurrentDate,
            AgencyId: localStorage.agencyId,
            TimeZoneName: localStorage.TimeZone,
            PhoneNumber:null,
            PinNumber:null
        };
        $scope.IsDisabled=false;
        vm.SignIn=SignIn;
        vm.redirectToDashBoard=redirectToDashBoard;
        
         
        function SignIn() {
            $scope.IsDisabled=true;
            apiService.post('/api/StudentAttendance/CheckParentSignIn', $scope.query,
                getAttendanceDataSuccess,
                Failed);
        };
        function getAttendanceDataSuccess(result)
        {
            $scope.IsDisabled=false;
            if(result.data.ReturnStatus==false)
            {
                notificationService.displaymessage("Invalid Phone Number or Pin Number")
            }
            else if(result.data.ReturnStatus==true)
            {
                 $state.go('authentication.kioskAttendanceParentMark', {obj:$scope.query});
                 $localStorage.attendanceData=$scope.query;
                 vm.showProgressbar = false;
            }
        }
        function Failed(result) {
            $scope.IsDisabled=false;
            vm.showProgressbar = false;
            notificationService.displaymessage('Please try again after some time.');
        }
        function redirectToDashBoard()
        {
            $state.go("authentication.login");
        }
    }
   
})();