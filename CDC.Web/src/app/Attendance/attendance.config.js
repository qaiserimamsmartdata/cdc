(function () {
    'use strict';

    angular
        .module('app.attendance')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
    
            .state('triangular.attendance', {
                url: '/attendance/mark',
                templateUrl: 'app/Attendance/views/markattendancenew.tmpl.html',
                // set the controller to load for this page
                controller: 'KioskParticipantAttendanceController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewAttendance']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
             .state('triangular.attendanceParentMark', {
                url: '/participantattendance/mark',
                templateUrl: 'app/Attendance/views/kioskparticipantattendance.tmpl.html',
                // set the controller to load for this page
                controller: 'ParticipantAttendanceController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewParticipantAttendance']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            // .state('triangular.kioskParentSignIn', {
            //     url: '/kiosk/attendance/parentSignIn',
            //     templateUrl: 'app/Attendance/views/kioskparentSignIn.tmpl.html',
            //     // set the controller to load for this page
            //     controller: 'ParentSignInController',
            //     controllerAs: 'vm',
            //     // layout-column class added to make footer move to
            //     // bottom of the page on short pages
            //     data: {
            //         layout: {
            //             contentClass: 'layout-column overlay-10'
            //         },
            //         permissions: {
            //             only: ['viewKioskParticipantAttendance']
            //         }
            //     },
            //     resolve: {
            //         data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
            //             return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
            //         }
            //     }
            // })
            //  .state('triangular.kioskAttendanceParentMark', {
            //     url: '/kiosk/attendance/mark',
            //     templateUrl: 'app/Attendance/views/kioskparticipantattendancelist.tmpl.html',
            //     // set the controller to load for this page
            //     controller: 'KioskParticipantAttendanceController',
            //     controllerAs: 'vm',
            //     // layout-column class added to make footer move to
            //     // bottom of the page on short pages
            //      params: {
            //         obj: null
            //     },
            //     data: {
            //         layout: {
            //             contentClass: 'layout-column overlay-10'
            //         },
            //         permissions: {
            //             only: ['viewKioskParticipantAttendance']
            //         }
            //     },
            //     resolve: {
            //         data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
            //             return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
            //         }
            //     }
            // })
            .state('triangular.staffportalattendance', {
                url: '/participants/attendance/mark',
                templateUrl: 'app/Attendance/views/markattendancenew.tmpl.html',
                // set the controller to load for this page
                controller: 'AttendanceControllerNew',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewAttendance']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "STAFF", $window);
                    }
                }
            })
            .state('triangular.applyleave', {
                url: '/staff/ApplyLeave',
                templateUrl: 'app/Staff/views/staffleave.tmpl.html',
                // set the controller to load for this page
                controller: 'staffLeaveController',
                controllerAs: 'vm',
                
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewLeave']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "STAFF", $window);
                    }
                }
            })
            
            .state('triangular.staffattendance', {
                url: '/attendance/staffattendance',
                templateUrl: 'app/Attendance/views/staffAttendance.tmpl.html',
                // set the controller to load for this page
                controller: 'staffAttendanceController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages

                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.failedsignouts', {
                url: '/attendance/failedsignouts',
                templateUrl: 'app/Attendance/views/failed_signouts.tmpl.html',
                // set the controller to load for this page
                controller: 'FailedSignoutController',
                // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.staffportalfailedsignouts', {
                url: '/failedsignouts',
                templateUrl: 'app/Attendance/views/failed_signouts.tmpl.html',
                // set the controller to load for this page
                controller: 'FailedSignoutController',
                // controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "STAFF", $window);
                    }
                }
            });

        triMenuProvider.addMenu({
            name: 'Attendance',
            icon: 'fa fa-sign-in',
            type: 'dropdown',
            permission: 'viewAttendance',
            priority: 7,
            children: [{
                name: 'Participant Attendance',
                state: 'triangular.attendance',
                type: 'link',
                permission: 'viewAgencyParticipantAttendance'
            },
                {
                    name: 'Participant Attendance',
                    state: 'triangular.staffportalattendance',
                    type: 'link',
                    permission: 'viewStaffPortalAttendance'
                },
                {
                    name: 'Staff Attendance',
                    state: 'triangular.staffattendance',
                    type: 'link',
                    permission: 'viewStaffAttendance'
                },
                {
                    name: 'Failed Signouts',
                    state: 'triangular.failedsignouts',
                    type: 'link',
                    permission: 'viewAgencyFailedSignouts'
                }
                ,
                {
                    name: 'Failed Signouts',
                    state: 'triangular.staffportalfailedsignouts',
                    type: 'link',
                    permission: 'viewStaffPortalFailedSignouts'
                },
                {
                    name: 'Apply Leave',
                    state: 'triangular.applyleave',
                    permission: 'viewLeave',
                    type: 'link',

                },
                {
                    name: 'Kiosk',
                    state: 'authentication.parentKiosklogin',
                    permission: 'viewKioskParticipantAttendance',
                    type: 'link',
                }
                ]
        });
    }
})();
