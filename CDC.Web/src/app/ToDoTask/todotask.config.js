
(function () {
    'use strict';

    angular
        .module('app.todotask')
        .config(moduleConfig);

    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider

            .state('triangular.todo', {
                url: '/todo',
                templateUrl: 'app/ToDoTask/views/todo.tmpl.html',
                //templateUrl: 'app/ToDoTask/taskHtml.html',
                controller: 'ToDoController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewToDoTask']
                    }
                }
            })

            ;

        triMenuProvider.addMenu({
            name: 'To do',
            icon: 'fa fa-bars',
            type: 'dropdown',
            priority: 10,
            permission: 'viewToDoTask',
            children: [{
                name: 'To do List',
                state: 'triangular.todo',
                type: 'link'
            }
            ]
        });

    }
})();
