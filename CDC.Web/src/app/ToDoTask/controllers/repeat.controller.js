(function () {
    'use strict';
    angular
        .module('app.todotask')
        .controller('RepeatController', RepeatController);
    function RepeatController($scope) {
        $scope.number = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize;
    }
})();