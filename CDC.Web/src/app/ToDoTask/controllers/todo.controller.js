
(function () {
    'use strict';

    angular
        .module('app.todotask')
      
        .controller('ToDoController', ToDoController)
        .controller('AddToDoController', AddToDoController)
        .controller('UpdateToDoController', UpdateToDoController);



    /* @ngInject */
    function ToDoController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        ToDoService, CommonService, filerService,$filter) {



        var vm = this;
        vm.$storage = localStorage;
        localStorage.ParticipantAttendancePage = false;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.columns = {
            Name: 'Name',
            ID: 'ID',
            Priority: 'Priority',
            DueDate: 'Due Date',
            Description: 'Description',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.toDoCount = 0;
        vm.selected = [];
        //vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            Name: '',
            PriorityId: 0,
            DueDate: undefined,
            AgencyId: localStorage.agencyId,
            Description: '',
        };

        // $scope.number = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize;

        vm.GetToDoList = GetToDoList;
        vm.showAddToDoDialoue = showAddToDoDialoue;
        vm.showUpdateToDoDialoue = showUpdateToDoDialoue;
        vm.showDeleteToDoConfirmDialoue = showDeleteToDoConfirmDialoue;
        vm.reset = reset;
        GetToDoList();

        vm.getPriorities = getPriorities;
        getPriorities();
        function getPriorities() {
            vm.addQuery = {
                filter: '',
                limit: '10',
                order: '-id',
                page: 1,
                StatusId: 0,
                Name: '',
                AgencyId: localStorage.agencyId,
            };
            vm.AllPriorities = null;
            vm.promise = CommonService.getPriorities(vm.addQuery);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllPriorities = response.Content;
                    //console.log(response.Content);
                }
            });
        }

        $rootScope.$on("GetToDoList", function () {
            GetToDoList();
        });

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
$rootScope.AgencyId=1;
        function GetToDoList() {
            //console.log(vm.$storage);
            //console.log(localStorage);
            $scope.Todo=vm.query.Name==""?"All":vm.query.Name;
            if(vm.query.PriorityId>0){
                var PriorityData=$filter('filter')(vm.AllPriorities, { ID: vm.query.PriorityId});
                $scope.priority=PriorityData[0].Name;
            }
            else{
                $scope.priority="All";
            }
            $scope.DueDate=vm.query.DueDate==undefined?"All":vm.query.DueDate;
            $scope.Priority=vm.query.PriorityId
            vm.promise = ToDoService.GetToDoListService(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.toDoList = {};
                        vm.toDoList = response.Content;
                        vm.toDoCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.toDoList = [];
                        vm.toDoCount = 0;
                        NotificationMessageController('No To do list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get To do list at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };


        function showAddToDoDialoue(event, data) {
            $mdDialog.show({
                controller: AddToDoController,
                controllerAs: 'vm',
                templateUrl: 'app/ToDoTask/views/addtodo.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            })
        };

        function showUpdateToDoDialoue(event, data) {
            $mdDialog.show({
                controller: UpdateToDoController,
                controllerAs: 'vm',
                templateUrl: 'app/ToDoTask/views/updatetodo.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: event,
                escToClose: true,
                clickOutsideToClose: true,
                items: { toDodetail: data },
                fullscreen: $scope.customFullscreen
            })
        };

        function showDeleteToDoConfirmDialoue(event, data) {
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this To do permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteToDo(data);
            }, function () {
                $scope.hide();
            });
        };

        function deleteToDo(data) {
            vm.promise = ToDoService.DeleteToDoById(data);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    NotificationMessageController('To do deleted successfully.')
                    GetToDoList();
                }
                else {
                    NotificationMessageController('Unable to get Todo list at the moment. Please try again after some time.');
                }
            });
        };


        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.Name = '';
            vm.query.PriorityId = 0,
                vm.query.DueDate = undefined,
                GetToDoList();
        };



    }
    //New Controller For Add
    function AddToDoController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        ToDoService, CommonService, HOST_URL) {
        var vm = this;
        vm.toDo = {
            Name: '',
            ID: 0,
            Priority: 0,
            DueDate: new Date(),
            Description: '',
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        vm.addToDo = addToDo;
        vm.cancelClick = cancelClick;

        vm.getPriorities = getPriorities;
        getPriorities();

        vm.todayDate = new Date();


        localStorage.ParticipantAttendancePage = false;
        function addToDo(addToDo) {
            if (addToDo.$valid) {
                var model = {
                    ID: vm.toDo.ID,
                    Name: vm.toDo.Name,
                    PriorityId: vm.toDo.PriorityId,
                    AgencyId: localStorage.agencyId,
                    DueDate: vm.toDo.DueDate,
                    Description: vm.toDo.Description,
                };

                $http.post(HOST_URL.url + '/api/ToDo/AddUpdateToDo', model).then(function (response) {

                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('To do Added Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetToDoList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function getPriorities() {
            vm.addQuery = {
                filter: '',
                limit: '10',
                order: '-id',
                page: 1,
                StatusId: 0,
                Name: '',
                AgencyId: localStorage.agencyId,
            };
            vm.AllPriorities = null;
            vm.promise = CommonService.getPriorities(vm.addQuery);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllPriorities = response.Content;
                    //console.log(response.Content);
                }
            });
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }


    //New Controller For Update
    function UpdateToDoController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        ToDoService, CommonService, HOST_URL, items) {
        var vm = this;
        vm.toDo = {
            Name: '',
            ID: 0,
            Priority: 0,
            DueDate: new Date(),
            Description: '',
        };
        $scope.hide = function () {
            $mdDialog.hide();
            $mdDialog.destroy();
            $mdDialog.cancel();
        };

        vm.getPriorities = getPriorities;
        getPriorities();

        GetToDoDetailbyId();

        function GetToDoDetailbyId() {
            vm.toDo.Name = items.toDodetail.Name;
            vm.toDo.ID = items.toDodetail.ID;
            vm.toDo.Priority = items.toDodetail.Priority.ID.toString();
            vm.toDo.DueDate = items.toDodetail.DueDate == null ? new Date() : (new Date(items.toDodetail.DueDate));
            vm.toDo.Description = items.toDodetail.Description;
        }
        vm.updateToDo = updateToDo;
        vm.cancelClick = cancelClick;



        function updateToDo(updateToDo) {
            if (updateToDo.$valid) {
                var model = {
                    ID: vm.toDo.ID,
                    Name: vm.toDo.Name,
                    PriorityId: vm.toDo.Priority,

                    DueDate: vm.toDo.DueDate,
                    Description: vm.toDo.Description,
                    AgencyId: localStorage.agencyId,
                    IsDeleted: false,
                };

                $http.post(HOST_URL.url + '/api/ToDo/AddUpdateToDo', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('To do updated Successfully.');
                        cancelClick();
                        $rootScope.$emit("GetToDoList", {});
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $mdDialog.hide();
                    }
                });
            }
        }

        function getPriorities() {
            vm.addQuery = {
                filter: '',
                limit: '10',
                order: '-id',
                page: 1,
                StatusId: 0,
                Name: '',
                AgencyId: localStorage.agencyId,
            };
            vm.AllPriorities = null;
            vm.promise = CommonService.getPriorities(vm.addQuery);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllPriorities = response.Content;
                    //console.log(response.Content);
                }
            });
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }

})();