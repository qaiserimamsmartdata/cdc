﻿(function () {
    'use strict';

    angular
        .module('app.todotask')
        .factory('ToDoService', ToDoService);

    ToDoService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function ToDoService($http, $q, HOST_URL) {
        return {
            GetToDoListService:GetToDoListService,
            DeleteToDoById:DeleteToDoById,
            SetId: SetId
        };

    function SetId(data) {
            this.Id=0;
            this.Name='';
            this.Id = data.ID;
            this.Name = data.Name;
            this.ToDoData=data;
        };

    function GetToDoListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/ToDo/GetAllToDo', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    function DeleteToDoById(ToDoId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/ToDo/DeleteToDo?toDoId=' + ToDoId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


    }
})();