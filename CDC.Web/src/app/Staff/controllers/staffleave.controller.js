(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('staffLeaveController', staffLeaveController)
        .controller('ApplyLeaveController', ApplyLeaveController);

    function staffLeaveController($scope, $mdDialog, $state, $http, $stateParams,CommonService ,$mdEditDialog, $timeout, $mdToast, $rootScope, $q, filerService, notificationService, apiService, $localStorage, HOST_URL, StaffService) {
        var vm = this;
        // $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        vm.DisableApplyLeave = 0;
        if (localStorage.staffId != undefined || null)
            vm.DisableApplyLeave = 1;

        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        //$scope.AgencyId=localStorage.agencyId;
        $scope.Title = localStorage.staffId == undefined ? "Leave Record Details" : "Applied Leave details";
        vm.close = filerService.close();
        vm.setClassEndDate = setClassEndDate;
        vm.MaxDate = new Date();
        vm.reset = reset;
        $scope.isShow = true;
        if (JSON.parse(localStorage.getItem('roles'))[0] == 'STAFF') {
            $scope.isShow = false;
        }
        //vm.setStartDate = setStartDate;
        vm.disabled = true;
        vm.columns = {
            Name: 'Name',
            FromDate: 'From Date',
            ToDate: 'To Date',
            Status: 'Leave Status',
            Remark: 'Remark',
            Action: 'Action'
        };

        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            status: 0,
            LeaveStartDate: new Date(),
            LeaveEndDate: new Date(),
            StaffName: '',
            AgencyId: localStorage.agencyId,
            StaffId: localStorage.staffId
        };
        vm.ApproveLeave = ApproveLeave;
        vm.DeclineLeave = DeclineLeave;
        vm.openApplyLeaveModel = openApplyLeaveModel;
        vm.GetStaffLeaveList = GetStaffLeaveList;
        getLeaveStatusData();
        var myDate = new Date();
        vm.query.LeaveStartDate = new Date(myDate);
        vm.query.LeaveStartDate.setMonth(myDate.getMonth() - 1);
        vm.query.LeaveEndDate = new Date(myDate);
        vm.query.LeaveEndDate.setMonth(myDate.getMonth() + 1);
        // var toDate =vm.query.LeaveEndDate;
        // vm.query.LeaveStartDate.setDate(toDate.getDate() - 6);
        //vm.GetStatusList = GetStatusList;
        // function GetStatusList() {
        //     var IDViewModel = { id: localStorage.agencyId };
        //     apiService.post('api/Status/GetStatus', null, GetStatusListSuccess, Failed)
        // }
        // function GetStatusListSuccess(result) {
        //     vm.StatusList = result.data.Content;
        //     //notificationService.displaymessage('Staff List r.');
        // }
        //GetStatusList();
        function GetStaffLeaveList() {
            $scope.StaffName = vm.query.StaffName == "" ? "All" : vm.query.StaffName;
            if ($scope.LeaveStatusId == 1) {
                $scope.LeaveStatus = "Pending";
            }
            else if ($scope.LeaveStatusId == 2) {
                $scope.LeaveStatus = "On Leave";
            }
            else if ($scope.LeaveStatusId == 3) {
                $scope.LeaveStatus = "Declined";
            }
            else if ($scope.LeaveStatusId == 4) {
                $scope.LeaveStatus = "Approved";
            }
            else {
                $scope.LeaveStatus = "All";
            }
            if (vm.query.LeaveStartDate == undefined && vm.query.LeaveEndDate == undefined) {
                $scope.isFilterShow = true;
                $scope.Period = "All";
            }
            else {
                $scope.isFilterShow = false;
                $scope.fromDate = vm.query.LeaveStartDate;
                $scope.toDate = vm.query.LeaveEndDate;
            }
            apiService.post('api/Leave/GetAllLeave', vm.query, GetStaffLeaveListSuccess, Failed)
        }
        function GetStaffLeaveListSuccess(result) {
            if (result.data.Content.length > 0) {
                vm.NoData = false;
                vm.StaffLeaveList = result.data.Content;
                vm.staffLeaveCount = result.data.TotalRows;
            }
            else {
                vm.NoData = true;
                vm.StaffLeaveList = [];
                vm.staffLeaveCount = 0;
                notificationService.displaymessage('No Staff leave list found.');
            }
            vm.close();
            //notificationService.displaymessage('Staff List r.');
        }
        function getLeaveStatusData() {
            vm.AllLeaveStatus = null;// Clear previously loaded state list
            var myPromise = CommonService.getLeaveStatusData({ AgencyId: localStorage.agencyId });
            myPromise.then(function (resolve) {
                vm.AllLeaveStatus = null;
                vm.AllLeaveStatus = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function Failed(result) {
            vm.NoData = true;
            notificationService.displaymessage('No Staff leave list found.');
        }
        GetStaffLeaveList();
        function ApproveLeave(LeaveVm) {
            LeaveVm.StatusId = 4;
            LeaveVm.AgencyId = localStorage.agencyId;
            LeaveVm.TimeZone = localStorage.TimeZone;
            LeaveVm.ProfileName = localStorage.profileName;
            apiService.post('api/Leave/UpdateLeave', LeaveVm, GetStaffLeaveList, Failed)
        }
        function DeclineLeave(LeaveVm) {
            LeaveVm.StatusId = 3;
            apiService.post('api/Leave/UpdateLeave', LeaveVm, GetStaffLeaveList, Failed)
        }
        function reset() {
              vm.query.LeaveStatusId = 0;
            vm.query.StaffName = '';
            vm.query.LeaveEndDate = null;
            vm.query.LeaveStartDate = null;
          var myDate = new Date();
            vm.query.LeaveStartDate = new Date(myDate);
            vm.query.LeaveStartDate.setMonth(myDate.getMonth()-1);
            vm.query.LeaveEndDate = new Date(myDate);
            vm.query.LeaveEndDate.setMonth(myDate.getMonth()+1);
            // vm.query.LeaveStartDate = vm.query.LeaveStartDate != null ? null : '';
            // vm.query.LeaveEndDate = vm.query.LeaveEndDate != null ? null : '';
            vm.disabled = true;
            GetStaffLeaveList();
        }

        // function setStartDate(DateVal) {
        //     vm.disabled = false;
        //     var tempDate = new Date(DateVal);
        //     vm.attendanceendDate = tempDate;
        // };

        function setClassEndDate(DateVal) {
            vm.query.LeaveEndDate = DateVal;
        };

        function openApplyLeaveModel() {
            $mdDialog.show({
                controller: ApplyLeaveController,
                controllerAs: 'vm',
                templateUrl: 'app/Staff/views/applyLeaveModel.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                //items: { attendanceData: attendanceData },
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        }

        vm.DetailStaffInfo = detailStaffInfo;

        function detailStaffInfo($event, staff) {
            StaffService.SetId(staff);
            staff.RedirectFrom = "LeaveRecord";
            if(localStorage.Portal=='AGENCY')
            $state.go("triangular.staffdetails", { obj: staff });
            else
            $state.go("triangular.staffportalstaffdetails", { obj: staff });
        };
    }

    function ApplyLeaveController($scope, $mdDialog, $state, $http, $stateParams, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, filerService, notificationService, apiService, $localStorage, HOST_URL) {
        var vm = this;
        vm.GetStaffList = GetStaffList;
        vm.ApplyLeaveVm = {
            LeaveToDate: null,
            LeaveFromDate: new Date()
        };
        vm.currentData = new Date();
        vm.setLeaveEndDate = setLeaveEndDate;
        vm.cancelClick = cancelClick;
        vm.ApplyLeave = ApplyLeave;
        function setLeaveEndDate(dateVal) {
            vm.ApplyLeaveVm.LeaveToDate=null;
            // vm.ApplyLeaveVm.LeaveToDate = dateVal;
            // var tempDate = new Date(DateVal);
            // vm.leaveenddate = tempDate;
        };
        vm.setToDate = setToDate;
        function setToDate() {
            vm.ApplyLeaveVm.LeaveToDate = new Date();
        }
        function GetStaffList() {
            var IDViewModel = { id: localStorage.agencyId, StaffId: localStorage.staffId };
            apiService.post('api/common/GetStaffList', IDViewModel, GetStaffListSuccess, Failed)
        }
        function GetStaffListSuccess(result) {
            $scope.StaffName = result.data.Content[0].Name;
            vm.StaffList = result.data.Content;
            //notificationService.displaymessage('Staff leave record not found.');
        }
        function Failed(result) {
            //notificationService.displaymessage('Please try again after some time.');
        }
        function cancelClick() {
            $mdDialog.cancel();
        }
        function ApplyLeave() {
            vm.ApplyLeaveVm.StaffId = localStorage.staffId;
            vm.ApplyLeaveVm.AgencyId = localStorage.agencyId;
            vm.ApplyLeaveVm.TimeZone = localStorage.TimeZone;
            vm.ApplyLeaveVm.ProfileName = localStorage.profileName;

            apiService.post('api/Leave/ApplyStaffLeave', vm.ApplyLeaveVm, ApplyLeaveSuccess, Failed)
        }
        function ApplyLeaveSuccess(result) {
            vm.StaffList = result.data.Content;
            cancelClick();
            $state.go('triangular.portalstaffleave', {}, { reload: true });
        }
        GetStaffList();
    }
})();