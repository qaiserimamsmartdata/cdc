(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('UpdateStaffController', UpdateStaffController);


    /* @ngInject */
    function UpdateStaffController($filter, notificationService, $scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, StaffService, CommonService, AgencyService, $localStorage, imageUploadService) {
        var vm = this;
        $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        vm.Title = "Update staff information";
        vm.status = 'idle';  // idle | uploading | complete
        var fileList;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            staff: 0,
            name: '',
            PositionId: 0,
            AgencyId: $scope.$storage.agencyId,
            IsTimeClockUser: true
        };
        var StaffId = StaffService.Id;
        if ($scope.$storage.staffId == null || $scope.$storage.staffId == undefined)
            $state.go('triangular.staff');
        else
            $scope.id = $scope.$storage.staffId;
        vm.showProgressbar = false;
        vm.disabled = true;
        vm.ImageUrlPath = HOST_URL.url;
        // vm.GetAllCountry = GetAllCountry;
        vm.getPositions = getPositions;
        vm.GetState = GetState;
        vm.query = {
            AgencyId: localStorage.agencyId
        };
        // vm.GetCity = GetCity;
        GetState(1);
        getPositions();
        // GetAllCountry();
       
        GetStaffList();


        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        vm.CheckTimeClockUser = CheckTimeClockUser;
        getSMSCarrier();
        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        }
         var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);

//for min 80years.
        var NewDate = new Date();
        NewDate= NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);

        function uploadImage($files) {
            //imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image size should not exceed 2MB');
                return;

            }
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {
                uploadStarted();

                $timeout(uploadComplete, 2000);
                imageUploadService.uploadImage($files, vm.UploadSuccess)
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ProfilePicComplete = HOST_URL.url + data.Content;
        }
        function uploadStarted() {
            vm.status = 'uploading';
        }

        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image uploaded successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 2000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }

        // function GetAllCountry() {
        //     vm.AllCountries = null; // Clear previously loaded state list
        //     // $scope.CountryTextToShow = "Please Wait..."; // this will show until load states from database
        //     var myPromise = CommonService.getCountries();

        //     myPromise.then(function (resolve) {
        //         vm.AllCountries = resolve;
        //         $scope.CountryTextToShow = "--Select Country--";
        //     }, function (reject) {

        //     });

        // };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        // function GetCity(stateId) {
        //     //Load State
        //     vm.Cities = null;
        //     var myPromise = CommonService.getCities(stateId);
        //     // wait until the promise return resolve or eject
        //     //"then" has 2 functions (resolveFunction, rejectFunction)
        //     myPromise.then(function (resolve) {
        //         vm.Cities = resolve;
        //         $scope.CityTextToShow = "--Select Cities--";
        //     }, function (reject) {

        //     });

        // };
        function getPositions() {
            vm.promise = CommonService.getPositions(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.AllPosition = null;
                    vm.AllPosition = eval(response);
                }
            });
        };
        var temp = '';
         vm.selectedLocation=[];
        function GetStaffList() {
            vm.promise = StaffService.GetStaffListByIdService($scope.$storage.staffId);
            vm.promise.then(function (response) {
                if (response.staffList.ID > 0) {
                    vm.staff = response.staffList;
                     getLocations();
                    // response.staffList.StaffLocationInfoList.forEach(function(element) {
                    //     // var temp=element.AgencyLocationInfoId;
                    //   vm.selectedLocation.push(element.AgencyLocationInfoId);  
                    // }, this);
                    //   vm.selectedLocation.push(response.staffList.StaffLocationInfoList[0].AgencyLocationInfoId.toString());
                    // vm.selectedLocation.push(response.staffList.StaffLocationInfoList[0]);
                    temp = vm.staff.IsTimeClockUser;
                    vm.staff.Certification = vm.staff.Certification.toString();
                    vm.staff.StateId = vm.staff.StateId.toString();
                    vm.staff.Password = vm.staff.Password.toString();
                    vm.staff.SMSCarrierId = vm.staff.SMSCarrierId == null ? null : vm.staff.SMSCarrierId.toString();
                    vm.staff.confirmpassword = vm.staff.Password.toString();
                    vm.staff.PositionId = vm.staff.PositionId.toString();
                    // vm.staff.AgencyLocationInfoId = vm.staff.AgencyLocationInfoId.toString();
                    vm.staff.DateOfBirth = new Date(vm.staff.DateOfBirth);
                    vm.staff.DateHired = new Date(vm.staff.DateHired);
                    vm.staff.PhoneNumber = parseFloat(vm.staff.PhoneNumber);
                    AddStaff.$invalid = true;
                    vm.ProfilePic = vm.staff.ImagePath;
                    vm.ProfilePicComplete = HOST_URL.url + vm.staff.ImagePath;
                    $scope.selected = [];
                }
                else {
                    if (response.staffList.ID == 0) {
                        vm.staff = [];
                        vm.staffCount = 0;
                        NotificationMessageController('Invalid found.');
                    }
                    else {
                        NotificationMessageController('Unable to get staff list at the moment. Please try again after some time.');
                    }
                }
            });

        };

        function getLocations() {
            vm.promise = CommonService.getAgencyLocations(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllLocations = null;
                    vm.AllLocations = eval(response.Content);
                     vm.staff.StaffLocationInfoList.forEach(function(element) {
                        // var temp=element.AgencyLocationInfoId;
                      vm.selectedLocation.push(element.AgencyLocationInfoId);  
                    }, this);
                    //  vm.selectedLocation.push(vm.AllLocations[0].ID);
                }
            });

        }
        vm.updateStaff = updateStaff;
        function updateStaff(AddStaff) {
            if (AddStaff.$valid) {

                vm.showProgressbar = true;
                var roleModel = {};

                roleModel.RoleName = "Staff";

                vm.staff.ImagePath = vm.ProfilePic == undefined ? roleModel.ImagePath : vm.ProfilePic;
                var model = {};
                model = vm.staff;
                 model.StaffLocationList = vm.selectedLocation.map(function (elm) {
                        return {AgencyId:localStorage.agencyId,AgencyLocationInfoId: elm};
                    });
                $http.post(HOST_URL.url + '/api/Staff/UpdateStaff', model).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        vm.showProgressbar = false;
                        NotificationMessageController('Staff updated successfully.');
                        $state.go('triangular.staff');
                    }
                    else {
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to update at the moment. Please try again after some time.');
                        $state.go('triangular.classlist');
                    }
                });
            }
        }

        vm.cancelUpdate = cancelUpdate;
        function cancelUpdate() {
            $state.go('triangular.classlist');
        };

        function getCategory() {
            vm.promise = CommonService.getCategory(vm.query);
            vm.promise.then(function (response) {
                if (response.category.length > 0) {
                    vm.AllCategories = null;
                    vm.AllCategories = eval(response.category);
                }
            });

        };

        function getRooms() {
            vm.promise = CommonService.getRooms();
            vm.promise.then(function (response) {
                if (response.room.length > 0) {
                    vm.AllRooms = null;
                    vm.AllRooms = eval(response.room);
                }
            });

        };
        function getSessions() {
            vm.promise = CommonService.getSessions();
            vm.promise.then(function (response) {
                if (response.session.length > 0) {
                    vm.AllSessions = null;
                    vm.AllSessions = eval(response.session);
                }
            });

        };
        function CheckTimeClockUser(IsTimeClockUser) {
            if (temp == true) return;
            if (IsTimeClockUser == true) {
                vm.promise = CommonService.getStaff($scope.query);
                vm.promise.then(function (response) {
                    if (response.Content.staffList.length > 0) {
                        localStorage.TimeClockStaffCount = 0;
                        localStorage.TimeClockStaffCount = eval(response.Content.staffList.length);
                        if (localStorage.TimeClockStaffCount == localStorage.TotalTimeClockUsers) {

                            var confirm = $mdDialog.confirm()
                                .title('Sorry,timeclock users have reached maximum limit.')
                                .textContent('Would you like to upgrade plan?')
                                .ariaLabel('Lucky day')
                                .ok('Please do it.')
                                .cancel('Cancel');

                            $mdDialog.show(confirm).then(function () {
                                vm.staff.IsTimeClockUser = false;
                                $state.go('TimeClockUsersPrice.timeclockusersplanpricing');
                            }, function () {
                                vm.staff.IsTimeClockUser = false;
                            });
                            //     var confirm = $mdDialog.confirm()
                            //         .title('Sorry,TimeClockUsers hasve reached maximum limit.')
                            //         .textContent('Would you like to upgrade plan?.')
                            //         .ariaLabel('Lucky day')
                            //         .targetEvent(event)
                            //         .ok('Ok')
                            //         .cancel('Cancel');
                            //     $mdDialog.show(confirm).then(function () {
                            //         alert("Yes")
                            //     }, function () {
                            //         $scope.hide();

                            //         alert("No");
                            //     });
                            // }

                        }
                    }
                });

            }
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

})();