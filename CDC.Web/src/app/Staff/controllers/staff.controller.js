(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('StaffPageController', StaffPageController);

    /* @ngInject */
    function StaffPageController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, StaffService, CommonService, AgencyService, filerService, $localStorage, HOST_URL, $filter) {
        var vm = this;
        $scope.$storage = localStorage;
        //    $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        localStorage.ParticipantAttendancePage = false;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.ImageUrlPath = HOST_URL.url;
        vm.close = filerService.close();
        vm.columns = {
            Name: 'Name',
            PhoneNumber: 'Phone',
            Email: 'Email',
            Gender: 'Gender',
            DateHired: 'Date Hired',
            Position: 'Position',
            IsTimeClockUser: 'Time clock user',
            Status: 'Status',
            ID: 'ID',
            ProfilePic: 'Image'
        };
        $scope.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.staffCount = 0;
        $scope.selected = [];
        $scope.isSuperAdmin = true;
        $scope.Position = "All";
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            staff: 0,
            name: '',
            PositionId: 0,
            AgencyId: $scope.$storage.agencyId,
            IsTimeClockUser: true
        };
        vm.getPositions = getPositions;
        vm.GetStaffList = GetStaffList;
        vm.editStaff = editStaff;
        vm.showDeleteStaffConfirmDialoue = showDeleteStaffConfirmDialoue;
        vm.reset = reset;
        vm.detailStaffInfo = detailStaffInfo;
        getPositions();
        // getStaffCount();
        GetStaffList();
        // getCategory();
        // getSessions();
        //    function getStaffCount() {
        //             vm.promise = CommonService.getStaff($scope.query);
        //             vm.promise.then(function (response) {
        //                 if (response.Content.staffList.length > 0) {
        //                     localStorage.TimeClockStaffCount=0;
        //                     localStorage.TimeClockStaffCount= eval(response.Content.staffList.length);
        //                 }
        //             });
        //         };
        function getPositions() {
            vm.promise = CommonService.getPositions($scope.$storage.agencyId);
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.AllPosition = null;
                    vm.AllPosition = eval(response);
                }
            });
        };
        function GetStaffList() {
            $scope.Staff = $scope.query.StaffName == undefined || $scope.query.StaffName == "" ? "All" : $scope.query.StaffName;
            // if ($scope.query.PositionId > 0) {
            //     var model = {
            //         AgencyId: localStorage.agencyId,
            //         PositionId: $scope.query.PositionId
            //     }
            //     vm.promise = CommonService.getPositionNameById(model);
            //     vm.promise.then(function (response) {
            //         $scope.Position = response.Content.Name;
            //     })
            // }
            if ($scope.query.PositionId > 0) {
                var PositionData = $filter('filter')(vm.AllPosition, { ID: $scope.query.PositionId });
                $scope.Position = PositionData[0].Name;
            }

            vm.promise = StaffService.GetStaffListService($scope.query);
            vm.promise.then(function (response) {

                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoStaffData = false;
                        for (var i = 0; i < response.Content.length; i++) {
                            response.Content[i].DateHiredString = new Date(response.Content[i].DateHiredString);
                            if (response.Content[i].ImagePath == "") {
                                response.Content[i].ImagePath = "assets/images/avatars/avatar-5.png"

                            }
                        }
                        vm.staffList = {};
                        vm.staffList = response.Content;
                        vm.staffCount = response.TotalRows;


                        $scope.selected = [];
                    }
                    else {
                        vm.NoStaffData = true;
                        vm.staffList = [];
                        vm.staffCount = 0;
                        NotificationMessageController('No Staff list found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get staff list at the moment. Please try again after some time.');
                }
                vm.close();
            });



        };
        //    $scope.CompletedEvent = function (scope) {
        //     console.log("Completed Event called");
        // };

        // $scope.ExitEvent = function (scope) {
        //     console.log("Exit Event called");
        // };

        // $scope.ChangeEvent = function (targetElement, scope) {
        //     console.log("Change Event called");
        //     console.log(targetElement);  //The target element
        //     console.log(this);  //The IntroJS object
        // };

        // $scope.BeforeChangeEvent = function (targetElement, scope) {
        //     console.log("Before Change Event called");
        //     console.log(targetElement);
        // };

        // $scope.AfterChangeEvent = function (targetElement, scope) {
        //     console.log("After Change Event called");
        //     console.log(targetElement);
        // };
        // introJs().start();
        // $scope.IntroOptions = {
        // steps: [
        //     {
        //         element: document.querySelector('#step1'),
        //         intro: "<b>Add Family to start.</b>",
        //         position: 'bottom'
        //     },
        //     {
        //         element: document.querySelectorAll('#step2')[0],
        //         intro: "<b>Add participants into family registeration.</b>",
        //         position: 'bottom'
        //     },
        //     {
        //         element: document.querySelector('#step3'),
        //         intro: "<b>Add staff to classes you've registered.</b>",
        //         position: 'bottom'
        //     },
        //     {
        //         element: document.querySelector('#step4'),
        //         intro: "<b>You can see the signed in participants here!</b>",
        //         position: 'bottom'

        //     },
        //     {
        //         element: document.querySelector('#step5'),
        //         intro: "<b>You are all set to go!</b>",
        //         position: 'bottom'

        //     },


        // ],
        //      showStepNumbers: true,
        //      exitOnOverlayClick: true,
        //      exitOnEsc: true,
        //     // nextLabel: '<i class="fa fa-hand-o-right" aria-hidden="true"></i>',
        //     // prevLabel: '<i class="fa fa-hand-o-left" aria-hidden="true"></i>',
        //     skipLabel: 'Exit',
        //     doneLabel: 'Go!'
        // };

        function editStaff($event, data) {
            $scope.$storage.staffId = data.ID;
            $scope.$storage.staffName = data.FullName;
            // StaffService.SetId(data);
            $state.go('triangular.updatestaff');
        };
        function showDeleteStaffConfirmDialoue(event, data) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this staff permanently?')
                //   .textContent('All of this branch data and all "Branch Managers/Delivery Personals" associated with this class  will be deleted.')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteStaff(data);
            }, function () {
                $scope.hide();
            });
        }
        function deleteStaff(data) {
            vm.promise = StaffService.DeleteStaffById(data);
            vm.promise.then(function (response) {
                // alert(response.IsSuccess);
                if (response.IsSuccess == true) {
                    NotificationMessageController('Staff deleted successfully.');
                    GetStaffList();
                }
                else {

                    NotificationMessageController('Unable to get staff list at the moment. Please try again after some time.');
                }
            });
        };
        function setStaffEndDate(DateVal) {
            vm.disabledStaffEndDatePicker = false;
            vm.minstaffenddate = new Date(DateVal);
            $scope.query.classenddate = null;


        };
        function detailStaffInfo($event, staff) {
            StaffService.SetId(staff);
            staff.RedirectFrom = "staffdetails";

            $state.go("triangular.staffdetails", { obj: staff });
        };
        function reset() {
            $scope.query.PositionId = 0;
            $scope.Position = "All";
            $scope.query.StatusId = 0;
            $scope.query.StaffName = '';
            $scope.Staff = undefined;
            GetStaffList();
        };
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };
    }
})();