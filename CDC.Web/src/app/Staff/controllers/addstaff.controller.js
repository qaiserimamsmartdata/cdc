(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('AddStaffController', AddStaffController);

    /* @ngInject */
    function AddStaffController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, AgencyService, $localStorage, imageUploadService, notificationService) {
        var vm = this;
        vm.ImageUrlPath = HOST_URL.url;
        $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        localStorage.ParticipantAttendancePage = false;
        vm.status = 'idle';  // idle | uploading | complete
        vm.upload = upload;

        var fileList;
        vm.Title = "Add Staff Information";
        //  $scope.$storage = localStorage;
        vm.dateOfBirth = new Date();
        vm.staff = {};
        vm.addStaff = addStaff;
        vm.showProgressbar = false;
        // vm.GetAllCountry = GetAllCountry;
        vm.GetState = GetState;
        // vm.GetCity = GetCity;
        vm.GetPositions = GetPositions;
        // vm.ResetFields = ResetFields();
        $scope.id = 0;
        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.dateOfBirth = new Date(bDate);

        //for min 80years.
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);

        // vm.minBirthDate = new Date(bDate);
        // vm.todayData = new Date();
        // GetAllCountry();
        $scope.selectedUser = { id: 1, AgencyLocationInfoId: 'Main' };
        vm.CheckTimeClockUser = CheckTimeClockUser;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            staff: 0,
            name: '',
            PositionId: 0,
            AgencyId: $scope.$storage.agencyId,
            IsTimeClockUser: true
        };
        getLocations();
        GetState(1);
        GetPositions();
        vm.ProfilePicComplete = "assets/images/avatars/avatar-5.png";
        // vm.agency = {};
        // function GetAllCountry() {
        //     vm.AllCountries = null; // Clear previously loaded state list
        //     // $scope.CountryTextToShow = "Please Wait..."; // this will show until load states from database
        //     var myPromise = CommonService.getCountries();

        //     myPromise.then(function (resolve) {
        //         vm.AllCountries = null;
        //         vm.AllCountries = resolve;
        //         $scope.CountryTextToShow = "--Select Country--";
        //     }, function (reject) {

        //     });

        // };
        // function getStaffCount() {

        // };

        vm.uploadImage = uploadImage;
        vm.UploadSuccess = UploadSuccess;
        function upload($files) {
            if ($files !== null && $files.length > 0) {
                fileList = $files;

                uploadStarted();

                $timeout(uploadComplete, 2000);
            }
        }

        function uploadStarted() {
            vm.status = 'uploading';
        }
        getSMSCarrier();
        function getSMSCarrier() {
            vm.AllSMSCarrier = null; // Clear previously loaded state list
            var myPromise = CommonService.getSMSCarrier();

            myPromise.then(function (resolve) {
                vm.AllSMSCarrier = null;
                vm.AllSMSCarrier = eval(resolve.Content);
            }, function (reject) {

            });

        };
        function uploadComplete() {
            vm.status = 'complete';
            var message = 'Image uploaded successfully';
            for (var file in fileList) {
                message += fileList[file].name + ' ';
            }
            $mdToast.show({
                template: '<md-toast><span flex>' + message + '</span></md-toast>',
                position: 'bottom right',
                hideDelay: 5000
            });

            $timeout(uploadReset, 2000);
        }

        function uploadReset() {
            vm.status = 'idle';
        }
        function uploadImage($files) {
            var size = $files[0].size;
            if (size > 2097152) {
                notificationService.displaymessage('Image size should not exceed 2MB');
                return;

            }
            uploadStarted();

            $timeout(uploadComplete, 2000);
            imageUploadService.uploadImage($files, vm.UploadSuccess)
            $scope.ProfileImage = $files;
            var fileExtension = $scope.ProfileImage[0].name;
            fileExtension = fileExtension.substr(fileExtension.lastIndexOf('.') + 1).toLowerCase();
            if (fileExtension == "jpg" || fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpeg" || fileExtension == "bmp") {

                imageUploadService.uploadImage($files, vm.UploadSuccess)
            } else {
                notificationService.displaymessage('Please select valid image format.');
                //angular.element("input[type='file']").val(null);
            }
        }
        function UploadSuccess(data) {
            vm.ProfilePic = data.Content;
            vm.ProfilePicComplete = HOST_URL.url + data.Content;
        }



        function CheckTimeClockUser(IsTimeClockUser) {
            if (IsTimeClockUser == true) {
                vm.promise = CommonService.getStaff($scope.query);
                vm.promise.then(function (response) {
                    if (response.Content.staffList.length > 0) {
                        localStorage.TimeClockStaffCount = 0;
                        localStorage.TimeClockStaffCount = eval(response.Content.staffList.length);
                        // if (localStorage.TimeClockStaffCount == localStorage.TimeClockUsers + localStorage.AgencyTimeClockUsers) {
                        if (localStorage.TimeClockStaffCount == localStorage.TotalTimeClockUsers) {
                            var confirm = $mdDialog.confirm()
                                .title('Sorry,timeclock users have reached maximum limit.')
                                .textContent('Would you like to upgrade plan?')
                                .ariaLabel('Lucky day')
                                .ok('Please do it.')
                                .cancel('Cancel');

                            $mdDialog.show(confirm).then(function () {
                                vm.staff.IsTimeClockUser = false;
                                $state.go('TimeClockUsersPrice.timeclockusersplanpricing');
                            }, function () {
                                vm.staff.IsTimeClockUser = false;
                            });
                            //     var confirm = $mdDialog.confirm()
                            //         .title('Sorry,TimeClockUsers hasve reached maximum limit.')
                            //         .textContent('Would you like to upgrade plan?.')
                            //         .ariaLabel('Lucky day')
                            //         .targetEvent(event)
                            //         .ok('Ok')
                            //         .cancel('Cancel');
                            //     $mdDialog.show(confirm).then(function () {
                            //         alert("Yes")
                            //     }, function () {
                            //         $scope.hide();

                            //         alert("No");
                            //     });
                            // }

                        }
                    }
                });

            }
        }
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.States = null;
                vm.States = resolve;
                //  vm.staff.StateId = "1".toString();
                // $scope.StateTextToShow = "--United State--";
            }, function (reject) {

            });

        };

        // function GetCity(stateId) {
        //     //Load State
        //     vm.Cities = null;
        //     var myPromise = CommonService.getCities(stateId);
        //     // wait until the promise return resolve or eject
        //     //"then" has 2 functions (resolveFunction, rejectFunction)
        //     myPromise.then(function (resolve) {
        //         vm.Cities = resolve;
        //         $scope.CityTextToShow = "--Select Cities--";
        //     }, function (reject) {

        //     });

        // };
        function GetPositions() {
            vm.promise = CommonService.getPositions(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.length > 0) {
                    vm.AllPosition = null;
                    vm.AllPosition = eval(response);
                }
            });
        };
        function addStaff(StaffObj) {
            if (StaffObj.$valid) {
                vm.showProgressbar = true;
                var roleModel = {};
                roleModel.ImagePath = "assets/images/avatars/avatar-5.png"
                roleModel.RoleName = "Staff"
                vm.staff.AgencyRegistrationId = $scope.$storage.agencyId;
                vm.staff.ImagePath = vm.ProfilePic == undefined ? roleModel.ImagePath : vm.ProfilePic;
                vm.staff.AgencyName = localStorage.agencyName;
                var model = {};
                model = vm.staff;
                $http.post(HOST_URL.url + '/api/User/GetRoleIdByRoleName', roleModel).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        model.RoleId = response.data.Content.ID;
                        AddStaff(model);
                    }
                })

                function AddStaff(model) {
                    // model.StaffLocationList = vm.selectedUser;
                    model.StaffLocationList = vm.selectedLocation.map(function (elm) {
                        return { AgencyId: localStorage.agencyId, AgencyLocationInfoId: elm };
                    });
                    $http.post(HOST_URL.url + '/api/Staff/AddStaff', model).then(function (response) {
                        if (response.data.IsSuccess == true) {
                            vm.showProgressbar = false;
                            NotificationMessageController(response.data.ReturnMessage[0]);
                            // ResetFields();
                            $state.go('triangular.staff');
                        }
                        else {
                            vm.showProgressbar = false;
                            NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                        }
                    });
                }
            }
        }
        function ResetFields() {
            vm.staff = {};

        };

        vm.selectedLocation = [];
        function getLocations() {
            vm.promise = CommonService.getAgencyLocations(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.AllLocations = null;
                    vm.AllLocations = eval(response.Content);
                    vm.selectedLocation.push(vm.AllLocations[0].ID);
                }
            });

        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }

})();