(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('StaffInnerDetailController', StaffInnerDetailController);

    /* @ngInject */
    function StaffInnerDetailController($scope, $mdDialog, $state, StaffService, HOST_URL, AgencyService, $localStorage, StaffScheduleService,notificationService) {
        var vm = this;
        vm.ImageUrlPath = HOST_URL.url;
        // $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        // TimeClockUser();

        // function TimeClockUser() {
        //     if (StaffService.StaffData.IsTimeClockUser == null)
        //         vm.IsTimeClockUser = "No";
        //     else
        //         vm.IsTimeClockUser = "Yes";

        // }
        // $scope.$storage = localStorage;
        if (StaffService.Id == undefined && StaffService.Id == null)
            $state.go('triangular.staff');
        else {
            vm.ImagePath = StaffService.StaffData.ImagePath;

            vm.FullName = StaffService.StaffData.FullName;
            vm.DateOfBirthString = new Date(StaffService.StaffData.DateOfBirthString);
            vm.GenderName = StaffService.StaffData.GenderName;
            vm.PositionName = StaffService.StaffData.PositionName;
            vm.PhoneNumber = StaffService.StaffData.PhoneNumber;
            vm.Email = StaffService.StaffData.Email;
            vm.DateHiredString = new Date(StaffService.StaffData.DateHiredString);
            vm.Address = StaffService.StaffData.Address;
            vm.CountryName = StaffService.StaffData.CountryName;
            vm.StateName = StaffService.StaffData.StateName;
            vm.CityName = StaffService.StaffData.CityName;
            vm.PostalCode = StaffService.StaffData.PostalCode;
            vm.StaffData = StaffService.StaffData.StaffScheduleList;
            if (StaffService.StaffData.IsTimeClockUser == null)
                vm.IsTimeClockUser = "No";
            else
                vm.IsTimeClockUser = "Yes";


            var model = { AgencyId: localStorage.agencyId, StaffId: StaffService.Id };
            vm.promise = StaffScheduleService.GetStaffScheduleListService(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.NoData = false;
                    vm.scheduleList = response.Content;
                    vm.scheduleCount = response.TotalRows;
                }
                else {
                    vm.NoData = true;
                    vm.scheduleList = [];
                    vm.scheduleCount = 0;
                }
            });

        }
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();