(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('profiledetailController', profiledetailController);

    /* @ngInject */
    function profiledetailController($scope, $stateParams, notificationService, $mdDialog, $state, StaffService, $localStorage) {
        var vm = this;
        var data = $stateParams.obj;
        // console.log(data);
        // $scope.$storage = localStorage;
        //$scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        if (StaffService.Id == undefined && StaffService.Id == null)
            $state.go('triangular.staff');
        else
            vm.staffname = StaffService.StaffName;

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        vm.checkRedirect = checkRedirect;
        function checkRedirect() {
            if (data.RedirectFrom == "staffdetails") {
                $state.go('triangular.staff');
            }
            else if (data.RedirectFrom == "StaffAttendance") {
                $state.go('triangular.staffattendance');
            }
            else if (data.RedirectFrom == "LeaveRecord") {
                if(localStorage.Portal=='AGENCY')
                $state.go('triangular.staffleave');
                else
                $state.go('triangular.portalstaffleave');
            }
            else if (data.RedirectFrom == "FailedSignoutStaff") {
                if(localStorage.Portal=='AGENCY')
                $state.go('triangular.failedsignouts');
                else
                $state.go('triangular.staffportalfailedsignouts');
            }
        }

    }
})();