(function () {
    'use strict';

    angular
        .module('app.staff')
        .factory('StaffScheduleService', StaffScheduleService);

    StaffScheduleService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function StaffScheduleService($http, $q, HOST_URL) {
        return {
            GetStaffScheduleListService: GetStaffScheduleListService,
            DeleteStaffScheduleById: DeleteStaffScheduleById,
            GetEventListService:GetEventListService,
            DeleteEventById:DeleteEventById,

            SetId: SetId
        };

        function SetId(data) {
            this.Id = 0;
            this.StaffName = '';
            this.Id = data.ID;
            this.StaffName = data.FullName;
            this.StaffData = data;
        };

        function GetStaffScheduleListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StaffSchedule/GetAllStaffSchedule', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function DeleteStaffScheduleById(staffScheduleId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/StaffSchedule/DeleteScheduleStaff?staffScheduleId=' + staffScheduleId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };


        function GetEventListService(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Event/GetAllEvent', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

        function DeleteEventById(eventId) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/Event/DeleteEventById?eventId=' + eventId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };

    }
})();