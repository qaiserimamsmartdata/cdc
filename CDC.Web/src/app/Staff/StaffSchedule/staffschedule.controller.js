(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('StaffScheduleController', StaffScheduleController);

    /* @ngInject */
    function StaffScheduleController($scope, $http, $localStorage, HOST_URL,$window) {
        var vm = this;
        $scope.agencyId = localStorage.agencyId;
        localStorage.HostUrl=HOST_URL.url;
         $scope.IsLoaded = false;
        window.loadStaffSchedule = function () {
            $scope.IsLoaded = true;
        }
    }
})();