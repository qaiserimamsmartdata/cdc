(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('EventDialogController', EventDialogController);

    /* @ngInject */
    function EventDialogController($scope, notificationService, $mdDialog, $filter, triTheming, dialogData, event, edit) {

        $scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 1;
        $scope.options = {
            hstep: [1, 2, 3],
            mstep: [1, 5, 10, 15, 25, 30]
        };
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };
        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };
        $scope.changed = function () {
            // $log.log('Time changed to: ' + $scope.mytime);
        };
        $scope.clear = function () {
            $scope.mytime = null;
        };

        $scope.IsValidTime = true;

        $scope.ValidateTime = function () {
            var startDate = new Date(vm.start);
            var endDate = new Date(vm.end);
            if (startDate.getDate() == endDate.getDate()) {
                var start = new Date(vm.startTime);
                var end = new Date(vm.endTime);
                var startTime = start.getTime();
                var endTime = end.getTime();
                if (start.getTime() < end.getTime()) {
                    $scope.IsValidTime = false;
                }
                else {
                    $scope.IsValidTime = true;
                }
            }
        }

        var vm = this;
        vm.todayDate = new Date();
        vm.setEndDate = setEndDate;
        function setEndDate(DateVal) {
            vm.end = null;
            var tempDate = new Date(DateVal);
            tempDate.setDate(tempDate.getDate() + 0);
            vm.end = tempDate;
        };
        vm.cancelClick = cancelClick;
        vm.colors = [];
        vm.colorChanged = colorChanged;
        vm.deleteClick = deleteClick;
        vm.allDayChanged = allDayChanged;
        vm.dialogData = dialogData;
        vm.edit = edit;
        vm.event = event;
        vm.okClick = okClick;
        vm.selectedColor = null;
        // create start and end date of event
        vm.start = event.start.toDate();
        vm.startTime = convertMomentToTime(event.start);

        if (event.end !== null) {
            vm.end = event.end.toDate();
            vm.endTime = convertMomentToTime(event.end);
        }

        ////////////////

        function colorChanged() {
            vm.event.backgroundColor = vm.selectedColor.backgroundColor;
            vm.event.borderColor = vm.selectedColor.backgroundColor;
            vm.event.textColor = vm.selectedColor.textColor;
            vm.event.palette = vm.selectedColor.palette;
        }

        function okClick() {
            vm.event.start = updateEventDateTime(vm.start, vm.startTime);
            if (vm.event.end !== null) {
                vm.event.end = updateEventDateTime(vm.end, vm.endTime);
            }
            $mdDialog.hide(vm.event);
        }

        function cancelClick() {
            $mdDialog.cancel();
        }

        function deleteClick() {
            vm.event.deleteMe = true;
            notificationService.displaymessage("Schedule deleted successfully.");
            $mdDialog.hide(vm.event);
        }

        function allDayChanged() {
            // if all day turned on and event already saved we need to create a new date
            if (vm.event.allDay === false && vm.event.end === null) {
                vm.event.end = moment(vm.event.start);
                vm.event.end.endOf('day');
                vm.end = vm.event.end.toDate();
                vm.endTime = convertMomentToTime(vm.event.end);
            }
        }

        function convertMomentToTime(moment) {
            // return {
            //     hour: moment.hour(),
            //     minute: moment.minute()
            // };

            var now = moment;
            return now;
        }

        function updateEventDateTime(date, time) {
            // var newDate = moment(date);
            // newDate.hour(time.hour);
            // newDate.minute(time.minute);
            // return newDate;

            var mydate = moment(date);
            var mytime = moment(time).format('HH:mm');
            var values = mytime.split(":");
            var value1 = values[0];
            var value2 = values[1];
            mydate.set({ hour: value1, minute: value2, second: 0, millisecond: 0 })
            return mydate;
        }

        function ConvertToISO(newdate) {
            var mydate = moment(newdate);
            var hhh = newdate.getUTCHours();
            var mmm = newdate.getUTCMinutes();
            mydate.set({ hour: hhh, minute: mmm, second: 0, millisecond: 0 })
            return mydate;
        }

        function createDateSelectOptions() {
            // create options for time select boxes (this will be removed in favor of mdDatetime picker when it becomes available)
            vm.dateSelectOptions = {
                hours: [],
                minutes: []
            };
            // hours
            for (var hour = 0; hour <= 23; hour++) {
                vm.dateSelectOptions.hours.push(hour);
            }
            // minutes
            for (var minute = 0; minute <= 59; minute++) {
                vm.dateSelectOptions.minutes.push(minute);
            }
        }

        // init
        createDateSelectOptions();

        // create colors
        angular.forEach(triTheming.palettes, function (palette, index) {
            var color = {
                name: index.replace(/-/g, ' '),
                palette: index,
                backgroundColor: triTheming.rgba(palette['500'].value),
                textColor: triTheming.rgba(palette['500'].contrast)
            };

            vm.colors.push(color);

            if (index === vm.event.palette) {
                vm.selectedColor = color;
                vm.colorChanged();
            }
        });
    }
})();
