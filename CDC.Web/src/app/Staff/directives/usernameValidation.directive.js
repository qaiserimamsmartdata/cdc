angular
    .module('app')
    .directive('ngUnique', ['$http', 'HOST_URL','$localStorage', function (async, HOST_URL,$localStorage) {
      
        return {
            require: 'ngModel',
            
            link: function (scope, elem, attrs, ctrl) {
                elem.on('blur', function (evt) {
                    if (attrs.ngUnique == 'staffusername') {
                        scope.$apply(function () {
                            var val = elem.val();
                            
                            var req = { "name": val, "agencyId": localStorage.agencyId, "staffId":localStorage.staffId} 
                            var ajaxConfiguration = { method: 'POST', url: HOST_URL.url + 'api/User/isexiststaffusername', data: req };
                            async(ajaxConfiguration)
                                .success(function (data, status, headers, config) {
                                    ctrl.$setValidity('unique', !data);
                                });
                        });
                    }                   
                });
            }
        }
    }]);