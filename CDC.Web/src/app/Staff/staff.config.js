(function () {
    'use strict';

    angular
        .module('app.staff')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.staff', {
                url: '/staff',
                templateUrl: 'app/Staff/views/staff.tmpl.html',
                // set the controller to load for this page
                controller: 'StaffPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewStaff']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.addstaff', {
                url: '/staff/add',
                templateUrl: 'app/Staff/views/addstaff.tmpl.html',
                // set the controller to load for this page
                controller: 'AddStaffController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column '
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.updatestaff', {
                url: '/staff/update',
                templateUrl: 'app/Staff/views/addstaff.tmpl.html',
                // set the controller to load for this page
                controller: 'UpdateStaffController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.staffdetails', {
                url: '/staff/details',
                templateUrl: 'app/Staff/Staff-Details/views/profile_detail.tmpl.html',
                // set the controller to load for this page
                controller: 'profiledetailController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        //contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.staffportalstaffdetails', {
                url: '/staff/profile',
                templateUrl: 'app/Staff/Staff-Details/views/profile_detail.tmpl.html',
                // set the controller to load for this page
                controller: 'profiledetailController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        //contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "STAFF", $window);
                    }
                }
            })
            .state('triangular.staffscheduler', {
                url: '/staffschedule',
                templateUrl: 'app/Staff/StaffSchedule/staffschedule.tmpl.html',
                // set the controller to load for this page
                controller: 'StaffScheduleController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.staffleave', {
                url: '/staff/leave',
                templateUrl: 'app/Staff/views/staffleave.tmpl.html',
                // set the controller to load for this page
                controller: 'staffLeaveController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            })
            .state('triangular.portalstaffleave', {
                url: '/leave',
                templateUrl: 'app/Staff/views/staffleave.tmpl.html',
                // set the controller to load for this page
                controller: 'staffLeaveController',
                controllerAs: 'vm',
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "STAFF", $window);
                    }
                }
            });

        triMenuProvider.addMenu({
            name: 'Staff',
            icon: 'fa fa-male',
            id: 'step2',
            type: 'dropdown',
            priority: 6,
            permission: 'viewStaff',
            children: [{
                name: 'Staff List',
                state: 'triangular.staff',
                priority: 1,
                type: 'link'
            }
                // , {
                //     name: 'Add Staff',
                //     state: 'triangular.addstaff',
                //     type: 'link'
                // }
                ,
                // {
                //     name: 'Schedule',
                //     state: 'triangular.schedule',
                //     type: 'link'
                // },
                // {
                //     name: 'Schedule',
                //     state: 'triangular.calendar',
                //     type: 'link'
                // },
                {
                    name: 'Staff Schedule',
                    state: 'triangular.staffscheduler',
                    priority: 2,
                    type: 'link'
                },
                //  {
                //     name: 'Events',
                //     state: 'triangular.kendo',
                //     priority: 3,
                //     type: 'link'
                // },

                {
                    name: 'Leave Record',
                    state: 'triangular.staffleave',
                    priority: 4,
                    type: 'link',
                    permission: 'viewStaffLeaves'
                },
                {
                    name: 'Leave Record',
                    state: 'triangular.portalstaffleave',
                    priority: 4,
                    type: 'link',
                    permission: 'viewPortalLeaves'
                }]
        });
    }
})();
