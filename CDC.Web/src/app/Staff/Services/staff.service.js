﻿(function () {
    'use strict';

    angular
        .module('app.staff')
        .factory('StaffService', StaffService);

    StaffService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function StaffService($http, $q, HOST_URL) {
        return {
            GetStaffListService: GetStaffListService,
            GetStaffListByIdService: GetStaffListByIdService,
            DeleteStaffById: DeleteStaffById,
            SetId: SetId
        };
 
        function SetId(data) {
           
            this.Id=0;
            this.StaffName='';
            this.Id = data.ID;
            this.StaffName = data.FullName;
            this.StaffData=data;
        };
        function GetStaffListService(model) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Staff/GetAllStaff', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
        function GetStaffListByIdService(StaffId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Staff/GetStaffById?staffId=' + StaffId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
        function DeleteStaffById(StaffId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/Staff/DeleteStaff?staffId=' + StaffId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };

    }
})();