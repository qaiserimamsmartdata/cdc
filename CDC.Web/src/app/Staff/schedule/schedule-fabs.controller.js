(function() {
    'use strict';

    angular
        .module('app.staff')
        .controller('ScheduleFabController', ScheduleFabController);

    /* @ngInject */
    function ScheduleFabController($rootScope) {
        var vm = this;
        vm.addSchedule = addSchedule;

        ////////////////

        function addSchedule($event) {
            $rootScope.$broadcast('addSchedule', $event);
        }
    }
})();