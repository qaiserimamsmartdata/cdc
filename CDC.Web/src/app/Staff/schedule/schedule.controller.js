(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('ScheduleController', ScheduleController);

    /* @ngInject */
    function ScheduleController($scope, $http, $rootScope, notificationService, $mdDialog, $mdToast, $filter, $element, triTheming, triLayout, uiCalendarConfig, HOST_URL, StaffScheduleService, $localStorage) {
        var vm = this;
        $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }
        vm.addSchedule = addSchedule;
        vm.AllRooms = [];
        vm.AllClasses = [];
        vm.AllLessons = [];
        vm.AllStaffs = [];
        vm.scheduleId = 0;
        vm.scheduleList = [];
        vm.calendarOptions = {
            contentHeight: 'auto',
            selectable: true,
            editable: true,
            header: false,
            viewRender: function (view) {
                // change day
                //vm.currentDay = view.calendar.getDate();
                vm.currentDay = moment();

                vm.currentView = view.name;
                // update toolbar with new day for month name
                $rootScope.$broadcast('calendar-changeday', vm.currentDay);
                // update background image for month
                triLayout.layout.contentClass = 'calendar-background-image background-overlay-static overlay-gradient-10 calendar-background-month-' + vm.currentDay.month();
            },
            dayClick: function (date, jsEvent, view) { //eslint-disable-line
                vm.currentDay = date;
            },
            eventClick: function (calEvent, jsEvent, view) { //eslint-disable-line
                $mdDialog.show({
                    controller: 'ScheduleDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'app/Staff/schedule/event-dialog.tmpl.html',
                    targetEvent: jsEvent,
                    focusOnOpen: false,
                    //fullscreen: $scope.customFullscreen ,
                    locals: {
                        dialogData: {
                            title: 'Edit Schedule',
                            confirmButtonText: 'Save',
                            rooms: vm.AllRooms,
                            classes: vm.AllClasses,
                            lessons: vm.AllLessons,
                            staffs: vm.AllStaffs
                        },
                        event: calEvent,
                        edit: true
                    }
                })
                    .then(function (event) {
                        if (angular.isDefined(event.deleteMe) && event.deleteMe === true) {
                            vm.promise = StaffScheduleService.DeleteStaffScheduleById(event.scheduleId);
                            vm.promise.then(function (response) {
                                if (response.IsSuccess == true) {
                                    uiCalendarConfig.calendars['triangular-calendar'].fullCalendar('removeEvents', event._id);
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content($filter('triTranslate')('Schedule Deleted'))
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );
                                }
                                else {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content($filter('triTranslate')('Error while deleting schedule, please try again later.'))
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );
                                }
                            });

                        }
                        else {
                            var model = {
                                ID: event.scheduleId,
                                Title: event.title,
                                ClassId: event.class,
                                RoomId: event.room,
                                StaffId: event.staff,
                                Date: event.start.toDate(),
                                Time: convertMomentToTime(event.start),
                                EndDate: event.end.toDate(),
                                EndTime: convertMomentToTime(event.end),
                                LessonPlanId: event.lesson,
                                IsDeleted: false
                            };
                            $http.post(HOST_URL.url + '/api/StaffSchedule/UpdateScheduleStaff', model).then(function (response) {
                                if (response.data.IsSuccess == true) {
                                    uiCalendarConfig.calendars['triangular-calendar'].fullCalendar('updateEvent', event);
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content($filter('triTranslate')('Schedule Updated'))
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );
                                }
                                else {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content($filter('triTranslate')("Error in updating schedule, Please try again later."))
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );
                                }
                            });
                        }
                    });
            }
        };

        vm.viewFormats = {
            'month': 'MMMM YYYY',
            'agendaWeek': 'w',
            'agendaDay': 'Do MMMM YYYY'
        };

        vm.eventSources = [{
            events: []
        }];

        function addSchedule(event, $event) {
            var inAnHour = moment(vm.currentDay).add(1, 'h');
            $mdDialog.show({
                controller: 'ScheduleDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/Staff/schedule/event-dialog.tmpl.html',
                targetEvent: $event,
                focusOnOpen: false,
                // fullscreen: $scope.customFullscreen ,
                locals: {
                    dialogData: {
                        title: 'Add Schedule',
                        confirmButtonText: 'Add',
                        rooms: vm.AllRooms,
                        classes: vm.AllClasses,
                        lessons: vm.AllLessons,
                        staffs: vm.AllStaffs
                    },
                    event: {
                        scheduleId: 0,
                        title: $filter('triTranslate')('New Schedule'),
                        //allDay: false,
                        start: vm.currentDay,
                        end: vm.currentDay,
                        room: 0,
                        class: 0,
                        lesson: 0,
                        staff: 0,
                        stick: true
                    },
                    edit: false
                }
            })
                .then(function (event) {
                    var model = {
                        Title: event.title,
                        ClassId: event.class,
                        RoomId: event.room,
                        StaffId: event.staff,
                        Date: event.start.toDate(),
                        Time: convertMomentToTime(event.start),
                        EndDate: event.end.toDate(),
                        EndTime: convertMomentToTime(event.end),
                        LessonPlanId: event.lesson
                    };
                    $http.post(HOST_URL.url + '/api/StaffSchedule/AddSchedule', model).then(function (response) {
                        if (response.data.IsSuccess == true) {
                            event.scheduleId = response.data.Id;
                            vm.eventSources[0].events.push(event);
                            $mdToast.show(
                                $mdToast.simple()
                                    .content($filter('triTranslate')('Schedule Created'))
                                    .position('bottom right')
                                    .hideDelay(2000)
                            );
                        }
                        else {
                            $mdToast.show(
                                $mdToast.simple()
                                    .content($filter('triTranslate')('Error! Adding schedule failed, Please try again later'))
                                    .position('bottom right')
                                    .hideDelay(2000)
                            );
                        }
                    });
                });
        }

        function createRandomEvents(number, startDate, endDate) {
            var model = { AgencyId: localStorage.agencyId };
            vm.promise = StaffScheduleService.GetStaffScheduleListService(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.scheduleList = response.Content;
                    vm.scheduleCount = response.TotalRows;
                    $scope.selected = [];
                    // for(var i=0; i< vm.scheduleList.length;i++){
                    //    var d = new Date();
                    //     d.setTime(1332403882588);
                    //    vm.scheduleList[i].Date=vm.scheduleList[i].Date;
                    // }
                    AddSchedulePosts(vm.scheduleList);
                }
                else {
                    vm.scheduleList = [];
                    vm.scheduleCount = 0;
                }
            });
        }
        function AddSchedulePosts(scheduleList) {
            // var eventNames = ['Pick up the kids', 'Remember the milk', 'Meeting with Morris', 'Car service', 'Go Surfing', 'Party at Christos house', 'Beer Oclock', 'Festival tickets', 'Laundry!', 'Haircut appointment', 'Walk the dog', 'Dentist :(', 'Board meeting', 'Go fishing'];
            // var locationNames = ['London', 'New York', 'Paris', 'Burnley'];
            for (var x = 0; x < scheduleList.length; x++) {
                //var randomMonthDate = randomDate(startDate, endDate);
                //var inAnHour = moment(randomMonthDate).add(1, 'h');
                // var randomEvent = Math.floor(Math.random() * (eventNames.length - 0));
                //var randomLocation = Math.floor(Math.random() * (locationNames.length - 0));
                var randomPalette = pickRandomProperty(triTheming.palettes);
                vm.eventSources[0].events.push({
                    title: scheduleList[x].Title,
                    allDay: false,
                    start: scheduleList[x].Date,
                    end: scheduleList[x].EndDate,
   
                    //end: inAnHour,
                    scheduleId: scheduleList[x].ID,
                    room: scheduleList[x].RoomId.toString(),
                    class: scheduleList[x].ClassId.toString(),
                    lesson: scheduleList[x].LessonPlanId.toString(),
                    staff: scheduleList[x].StaffId.toString(),
                    stick: true,
                    //description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, fugiat! Libero ut in nam cum architecto error magnam, quidem beatae deleniti, facilis perspiciatis modi unde nostrum ea explicabo a adipisci!',
                    //location: locationNames[randomLocation],
                    backgroundColor: triTheming.rgba(triTheming.palettes[randomPalette]['500'].value),
                    borderColor: triTheming.rgba(triTheming.palettes[randomPalette]['500'].value),
                    textColor: triTheming.rgba(triTheming.palettes[randomPalette]['500'].contrast),
                    palette: randomPalette
                });
            }
        }

        // listeners

        $scope.$on('addSchedule', addSchedule);

        // create 10 random events for the month
        createRandomEvents(100, moment().startOf('year'), moment().endOf('year'));

        function randomDate(start, end) {
            var startNumber = start.toDate().getTime();
            var endNumber = end.toDate().getTime();
            var randomTime = Math.random() * (endNumber - startNumber) + startNumber;
            return moment(randomTime);
        }

        function pickRandomProperty(obj) {
            var result;
            var count = 0;
            for (var prop in obj) {
                if (Math.random() < 1 / ++count) {
                    result = prop;
                }
            }
            return result;
        }

        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            var req = {
                method: 'POST',
                url: HOST_URL.url + '/api/Common/GetClass_Room_Lesson',
                data: {
                    AgencyId: localStorage.agencyId,
                    IsTimeClockUser: true
                }
            };
            $http(req).then(function successCallback(response) {
                if (response.data.IsSuccess) {
                    vm.AllRooms = eval(response.data.Content.roomList);
                    vm.AllClasses = eval(response.data.Content.classList);
                    vm.AllLessons = eval(response.data.Content.lessonPlanList);
                    vm.AllStaffs = eval(response.data.Content.staffList);
                }
                else {
                    if (response.data.length == 0) {
                        //NotificationMessageController('No branch manager found.');
                    }
                    else {
                        //NotificationMessageController('Unable to retreive branch managers  at the moment. Please try again after some time.');
                    }
                }
            }, function errorCallback(response) {
                //NotificationMessageController('Unable to retreive country list at the moment.');
            });
        }

        function convertMomentToTime(moment) {
            return moment.hour() + ':' + moment.minute() + ':00';
            // return {
            //     hour: moment.hour(),
            //     minute: moment.minute()
            // };
        }
    }
})();
