﻿(function () {
    'use strict';

    angular
        .module('app.reports')
        .factory('AgencyPaymentHistoryService', AgencyPaymentHistoryService);

    AgencyPaymentHistoryService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function AgencyPaymentHistoryService($http, $q, HOST_URL) {
        return {
            GetAgencyPaymentHistory:GetAgencyPaymentHistory            
        };

    function GetAgencyPaymentHistory(model) {
            var deferred = $q.defer();
            $http.post(HOST_URL.url + '/api/PaymentHistory/GetAllAgencyPaymentHistory', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
        };
    }
})();