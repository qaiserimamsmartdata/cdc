(function () {
    'use strict';

    angular
        .module('app.reports')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('triangular.AgencyPaymentHistory', {
                url: '/reports/paymenthistory/Organization',
                templateUrl: 'app/Reports/views/paymenthistory.tmpl.html',
                // set the controller to load for this page
                controller: 'AgencyPaymentHistoryController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewReport']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            .state('triangular.FamilyPaymentHistory', {
                url: '/reports/paymenthistory/family',
                templateUrl: 'app/Reports/views/reportbillingfees.tmpl.html',
                // set the controller to load for this page
                controller: 'ReportBillingAndFeesDetailsController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewReport']
                    }
                }
            });

        triMenuProvider.addMenu({
            name: 'Reports',
            icon: 'fa fa-tree',
            type: 'dropdown',
            permission: 'viewReport',
            priority: 12,
            children: [ {
                    name: 'Payment History',
                    //state: 'triangular.superadmin',
                    type: 'dropdown',
                    children: [{
                        name: 'Organization',
                        state: 'triangular.AgencyPaymentHistory',
                        type: 'link'
                    }, {
                            name: 'Family',
                            state: 'triangular.FamilyPaymentHistory',
                            type: 'link'
                        },
                    ]
                }]
        });
    }
})();
