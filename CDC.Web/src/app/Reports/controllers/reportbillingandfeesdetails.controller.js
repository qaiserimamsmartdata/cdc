(function () {
    'use strict';

    angular
        .module('app.reports')
        .controller('ReportBillingAndFeesDetailsController', ReportBillingAndFeesDetailsController)
        .controller('ReportAddPaymentController', ReportAddPaymentController);
    //For Showing student attendance history details -Tulesh
    function ReportBillingAndFeesDetailsController($scope, $rootScope, $mdDialog, $state, $stateParams, $localStorage, notificationService, filerService, apiService, HOST_URL, CommonService, $window, $filter) {
        var vm = this;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.goToPayment = goToPayment;
        vm.printDiv = printDiv;
        function printDiv(printable) {

            var printContents = document.getElementById(printable).innerHTML;
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();
        }
        vm.reset = reset;
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-ScheduleDate',
            page: 1,
            status: 0,
            FromDate: new Date((new Date()).setMonth((new Date()).getMonth() - 1)),
            ToDate: new Date(),
            ParticipantName: '',
            AgencyId: localStorage.agencyId,
            StudentId: 0,
            TimeZoneName: localStorage.TimeZone,
            PaymentStatus: null,
            ClassId: 0
        };
        vm.studentPaymentHistoryListCount = 0;
        vm.NoToDoData = false;
        vm.getFeesUnpaidList = getFeesUnpaidList;
        vm.getFeesUnpaidListSuccess = getFeesUnpaidListSuccess;
        vm.failed = failed;
        vm.printClick = printClick;
        vm.MaxDate = new Date();
        vm.setToDate = setToDate;
        function setToDate() {
            $scope.query.ToDate = new Date();
        }
        GetClass_Room_Lesson();
        getFeesUnpaidList();
        $rootScope.$on("getFeesUnpaidList", function () {
            getFeesUnpaidList();
        });
        function getFeesUnpaidList() {
            if ($scope.query.ClassId > 0) {
                var ClassData = $filter('filter')(vm.AllClasses, { ID: $scope.query.ClassId });
                $scope.ClassName = ClassData[0].ClassName;
            }
            else {
                $scope.ClassName = "All";
            }
            $scope.ParticipantName = $scope.query.ParticipantName == "" ? "All" : $scope.query.ParticipantName;
            $scope.PaymentStatus = $scope.query.PaymentStatus == null ? "All" : $scope.query.PaymentStatus == 0 ? 'Unpaid' : 'Paid';
            $scope.FromDate = $scope.query.FromDate;
            $scope.ToDate = $scope.query.ToDate;
            vm.promise = apiService.post('/api/StudentSchedule/GetUnpaidHistory', $scope.query,
                getFeesUnpaidListSuccess,
                failed);
        }
        function getFeesUnpaidListSuccess(response) {
            if (response.data.ReturnStatus == true) {
                if (response.data.Content.length > 0) {
                    vm.NoToDoData = false;
                    vm.studentPaymentHistoryList = response.data.Content;
                    vm.ClassName = vm.studentPaymentHistoryList[0].ClassName;
                    vm.StudentId = vm.studentPaymentHistoryList[0].StudentId;
                    vm.ParticipantName = vm.studentPaymentHistoryList[0].StudentInfo.FirstName + ' ' + vm.studentPaymentHistoryList[0].StudentInfo.LastName;
                    vm.Fees = vm.studentPaymentHistoryList[0].ClassInfo.Fees == null ? 0 : vm.studentPaymentHistoryList[0].ClassInfo.Fees;
                    vm.FeeTypeId = vm.studentPaymentHistoryList[0].ClassInfo.FeeTypeId == null ? 0 : vm.studentPaymentHistoryList[0].ClassInfo.FeeTypeId;
                    vm.PaymentStatusList = vm.studentPaymentHistoryList.PaymentStatusList;
                    vm.studentPaymentHistoryListCount = response.data.TotalRows;

                }

                else {
                    vm.studentPaymentHistoryList = null;
                    vm.ClassName = '';
                    vm.StudentId = 0;
                    vm.ParticipantName = '';
                    vm.Fees = 0;
                    vm.FeeTypeId = 0;
                    vm.PaymentStatusList = null;
                    vm.studentPaymentHistoryListCount = 0;
                    notificationService.displaymessage('No family payment history found.');
                    vm.NoToDoData = true;
                }
            }
            vm.close();
        }
        GetClass_Room_Lesson();
        function GetClass_Room_Lesson() {
            vm.promise = CommonService.getClassList(localStorage.agencyId);
            vm.promise.then(function (response) {
                if (response.Content.classList.length > 0) {
                    vm.AllClasses = null;
                    vm.AllClasses = eval(response.Content.classList);
                }
            });
        }
        function failed() {
            notificationService.displaymessage('Unable to get list at the moment. Please try again after some time.');
            vm.studentPaymentHistoryList = null;
            vm.ClassName = '';
            vm.StudentId = 0;
            vm.ParticipantName = '';
            vm.Fees = 0;
            vm.FeeTypeId = 0;
            vm.PaymentStatusList = null;
            vm.close();
        }
        function setStartDate(DateVal) {
            vm.disabled = false;
            var tempDate = new Date(DateVal);
            vm.attendanceendDate = tempDate;
        };
        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        function goToPayment(data) {

            $mdDialog.show({
                controller: AddPaymentController,
                controllerAs: 'vm',
                templateUrl: 'app/students/studentdetails/views/details/pages/paymentfees.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                items: { paymentdata: data, ClassName: vm.ClassName, StudentId: vm.StudentId, Fees: vm.Fees, FeeTypeId: vm.FeeTypeId }
            });
        }

        function reset() {
            $scope.query.ParticipantName = '';
            $scope.query.PaymentStatus = null;
            $scope.query.FromDate = new Date((new Date()).setMonth((new Date()).getMonth() - 1));
            $scope.query.ToDate = new Date();
            $scope.query.ClassId = 0;
            getFeesUnpaidList();
        }
        function printClick() {
            $window.print();
        }
    }
    function ReportAddPaymentController($scope, $mdDialog, $rootScope, $state, $stateParams, $localStorage, notificationService, filerService, apiService, HOST_URL, CommonService, items) {
        var vm = this;
        vm.cancelClick = cancelClick;
        vm.ClassName = items.ClassName;

        vm.getParents = getParents;
        vm.Fees = items.Fees;
        vm.Discount = items.Fees;
        vm.PaidAmount = items.Fees;
        vm.FeeTypeId = items.FeeTypeId;
        getParents();
        function getParents() {
            var model = {
                ID: items.StudentId
            }
            vm.promise = CommonService.getParentListByStudentId(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.ParentList = null;
                    vm.ParentList = response.Content;
                }
            });
        };
        function cancelClick() {
            $mdDialog.cancel();
        }

        vm.updateFeesDetails = updateFeesDetails;
        vm.updateFeesDetailsSuccess = updateFeesDetailsSuccess;
        vm.failed = failed;
        function updateFeesDetails() {
            items.paymentdata.Fees = vm.Fees;
            items.paymentdata.Discount = vm.Discount;
            items.paymentdata.PaymentMode = vm.PaymentMode;
            items.paymentdata.Discription = vm.Discription;
            items.paymentdata.ParentInfoId = vm.ParentId;
            items.paymentdata.ParentInfoId = vm.FeeTypeId;
            items.paymentdata.PaymentStatus = 1;
            apiService.post('/api/StudentSchedule/UpdateEnrollPaymentInfo', items.paymentdata,
                updateFeesDetailsSuccess,
                failed);
        }
        function updateFeesDetailsSuccess(response) {
            if (response.data.ReturnStatus == true) {
                $rootScope.$emit("getFeesUnpaidList", {});
                notificationService.displaymessage('Updated Successfully.');
                cancelClick();
            }
            else {
                notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
            }
        }
        function failed() {
            NotificationMessageController('Unable to update at the moment. Please try again after some time.');
        }
     
    }
})();