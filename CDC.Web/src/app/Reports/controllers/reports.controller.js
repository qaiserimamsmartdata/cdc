(function() {
    'use strict';

    angular
        .module('app.reports')
        .controller('ReportsPageController', ReportsPageController);

    /* @ngInject */
    function ReportsPageController() {
        var vm = this;
        vm.testData = ['triangular', 'is', 'great'];
    }
})();