
(function () {
    'use strict';

    angular
        .module('app.reports')
        .controller('AgencyPaymentHistoryController', AgencyPaymentHistoryController);

    /* @ngInject */
    function AgencyPaymentHistoryController($scope, $http,  $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, $localStorage,
        notificationService, AgencyPaymentHistoryService, filerService, $window) {
        var vm = this;
        // $scope.$storage = localStorage;
        //   $scope.$storage = localStorage;
        // if (localStorage.agencyId == undefined || null) {
        //     $state.go('authentication.login');
        //     notificationService.displaymessage("You must login first.");
        //     return;
        // }

        vm.printDiv = printDiv;
        function printDiv(printable) {

             var printContents = document.getElementById(printable).innerHTML;
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();
        }
        vm.$storage = localStorage;

        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        //vm.printClick = printClick;
        vm.columns = {
            AgencyName: 'Organization',
            PhoneNumber: 'Phone Number',
            Email: 'Email',
            PlanName: 'Plan',
            PlanAmount: 'Amount',
            CreatedDate: 'Date',
            StartDate: 'Start Date',
            ExpiryDate: 'Expiry Date',
            EventType: 'Payment',
            ID: 'ID',
        };
        vm.Pageno = 0;
        vm.skip = 0;
        vm.take = 0;
        vm.agencyCount = 0;
        vm.selected = [];
        vm.isSuperAdmin = true;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-CreatedDate',
            page: 1,
            StatusId: 0,
            agency: 0,
            AgencyName: '',
            PlanName: '',
            PositionId: 0,
            AgencyId: localStorage.agencyId,
            FromDate:new Date((new Date()).setMonth((new Date()).getMonth() - 1)),
            ToDate:new Date(),
            PaymentStatus:'',
            TimeZone:localStorage.TimeZone
        };
        vm.GetAgencyPaymeHistoryList = GetAgencyPaymeHistoryList;
        vm.reset = reset;
        vm.MaxDate = new Date();
        vm.setToDate = setToDate;
        function setToDate() {
            vm.query.ToDate = new Date();
        }
        GetAgencyPaymeHistoryList();

        function GetAgencyPaymeHistoryList() {
            $scope.Plan = vm.query.PlanName == "" ? "All" : vm.query.PlanName;
            $scope.PaymentStatus = vm.query.PaymentStatus == "" ? "All" : vm.query.PaymentStatus=="invoice.payment_succeeded"?"Succeeded":"Failed";
            vm.promise = AgencyPaymentHistoryService.GetAgencyPaymentHistory(vm.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    if (response.Content.length > 0) {
                        vm.NoToDoData = false;
                        vm.agencyPaymentList = {};
                        vm.agencyPaymentList = response.Content;
                        vm.agencyPaymentListCount = response.TotalRows;
                        vm.selected = [];
                    }
                    else {
                        vm.NoToDoData = true;
                        vm.agencyPaymentList = [];
                        vm.agencyPaymentListCount = 0;
                        NotificationMessageController('No payment history found.');
                    }
                }
                else {
                    NotificationMessageController('Unable to get payment history at the moment. Please try again after some time.');
                }
                vm.close();
            });
        };

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);

        };

        function reset() {
            vm.query.AgencyName = '';
            vm.query.FromDate =new Date((new Date()).setMonth((new Date()).getMonth() - 1));
            vm.query.ToDate = new Date();
            vm.query.Email = '';
            vm.query.PaymentStatus = '';
            vm.query.PlanName='';
            GetAgencyPaymeHistoryList();
        };
         vm.printToCart=printToCart;
        function printToCart(printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
      }
    }
})();