(function () {
    'use strict';

    angular
        .module('app.timeclockusersplanpricing')
        .controller('timeclockusersplanpricingController', timeclockusersplanpricingController);

    /* @ngInject */
    function timeclockusersplanpricingController($state, triSettings, $http, HOST_URL, planrateService, notificationService, CommonService, $localStorage, $filter) {
        var vm = this;
        vm.registerurl = HOST_URL.url + '#/agency';
        localStorage.PricePlan = null;
        vm.GotoAgencyRegistration = GotoAgencyRegistration;
        vm.GetTimeClockUsersPlanPricing = GetTimeClockUsersPlanPricing;
        vm.GetSelectedPlan = GetSelectedPlan;
        vm.UpdateAgencyTimeClockUsers = UpdateAgencyTimeClockUsers;
        vm.showProgressbar = true;
        GetTimeClockUsersPlanPricing();
        vm.model = {};
        vm.model.TimeClockUsersPlanId = localStorage.TimeClockUsersPlanId;
        vm.TimeClockUsersPlanId = localStorage.TimeClockUsersPlanId;
        vm.TotalTimeClockUsers = 0;
        function GetTimeClockUsersPlanPricing() {
            vm.promise = CommonService.getTimeClockUsersPricingPlans(null);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.TimeClockUsersPlanPricing = null;
                    vm.TimeClockUsersPlanPricing = eval(response.Content);
                    if (localStorage.TimeClockUsersPlanId != null || localStorage.TimeClockUsersPlanId != undefined) {
                        var FilteredData = $filter('filter')(vm.TimeClockUsersPlanPricing, { ID: localStorage.TimeClockUsersPlanId });
                        vm.selectedPlan = {
                            plan: FilteredData[0].Name
                        };
                        vm.planPrice = {
                            plan: FilteredData[0].Name
                        };
                        vm.priceText = {
                            text: ''
                        };
                        vm.registerFor = {
                            text: FilteredData[0].MaxNumberOfTimeClockUsers
                        };
                        vm.TimeClockUsersPlanId = FilteredData[0].ID;
                        vm.TotalTimeClockUsers = FilteredData[0].MaxNumberOfTimeClockUsers;
                        localStorage.StripePlanId = FilteredData[0].StripePlanId;
                    }
                    else {
                        vm.selectedPlan = {
                            plan: vm.TimeClockUsersPlanPricing[0].Name
                        };
                        vm.planPrice = {
                            plan: vm.TimeClockUsersPlanPricing[0].Name
                        };
                        vm.priceText = {
                            text: ''
                        };
                        vm.registerFor = {
                            text: vm.TimeClockUsersPlanPricing[0].MaxNumberOfTimeClockUsers
                        };
                        vm.TimeClockUsersPlanId = vm.TimeClockUsersPlanPricing[0].ID;
                        vm.TotalTimeClockUsers = vm.TimeClockUsersPlanPricing[0].MaxNumberOfTimeClockUsers;
                        localStorage.StripePlanId = vm.TimeClockUsersPlanPricing[0].StripePlanId;
                    }
                    vm.showProgressbar = false;
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('No Plans found.');
                }
            });
        };
        function GetSelectedPlan() {
            vm.TimeClockUsersPlanPricing.forEach(function (element) {
                if (element.Name == vm.selectedPlan.plan) {


                    vm.planPrice = {
                        plan: element.Name
                    };
                    vm.priceText = {
                        text: ''
                    };
                    vm.registerFor = {
                        text: element.MaxNumberOfTimeClockUsers
                    };
                    localStorage.setItem('PricePlan',JSON.stringify(element));
                    localStorage.StripePlanId = element.StripePlanId;
                    vm.TimeClockUsersPlanId = element.ID;
                    vm.TotalTimeClockUsers = element.MaxNumberOfTimeClockUsers;
                }
            }, this);
        }
        function UpdateAgencyTimeClockUsers(model) {
            vm.showProgressbar = true;
            vm.model.ID = localStorage.agencyId;
            vm.model.TimeClockUsersPlanId = vm.TimeClockUsersPlanId;
            vm.model.StripePlanId = localStorage.StripePlanId;
            vm.model.StripeUserId = localStorage.StripeUserId;
            vm.model.StripeTimeClockSubscriptionId = localStorage.StripeTimeClockSubscriptionId;
            ////for timeclockusers count
            vm.model.TimeClockStaffCount = localStorage.TimeClockStaffCount;
            vm.model.AgencyTimeClockUsers = localStorage.AgencyTimeClockUsers;
            vm.model.TimeClockPlanUsers = vm.TotalTimeClockUsers;
            //////////////////////////////////////////////////////////////////////////
            $http.post(HOST_URL.url + '/api/AgencyRegistration/UpdateAgencyByTimeClockUsers', vm.model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.ReturnMessage[0]);
                    localStorage.TotalTimeClockUsers = response.data.Content.TotalTimeClockUsers;
                    localStorage.TimeClockPlanUsers = vm.model.TimeClockPlanUsers;
                    localStorage.StripeTimeClockSubscriptionId = response.data.Content.StripeTimeClockSubscriptionId;
                    localStorage.TimeClockUsersPlanId = null;
                    localStorage.TimeClockUsersPlanId = response.data.Content.TimeClockUsersPlanId;
                    $state.go('triangular.dashboard');
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.ReturnMessage[0]);
                }
            });
        }
        function GotoAgencyRegistration() {
            if (vm.selectedPlan.plan != "" && vm.selectedPlan.plan != null) {
                if (localStorage.PricePlan == null || localStorage.PricePlan == undefined) {
                    localStorage.setItem('PricePlan',JSON.stringify(vm.TimeClockUsersPlanPricing[0]));
                }
                // planrateService.setplanRate(vm.selectedPlan.plan);
                $state.go('Register.agency');
            }
            else {
                notificationService.displaymessage('Please choose a plan.');
            }
        }
    }
})();