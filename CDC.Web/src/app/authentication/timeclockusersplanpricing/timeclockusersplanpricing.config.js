(function() {
    'use strict';

    angular
        .module('app.timeclockusersplanpricing')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
		 $stateProvider
        .state('TimeClockUsersPrice', {
            abstract: true,
            views: {
                'root': {
                    templateUrl: 'app/authentication/timeclockusersplanpricing/layouts/timeclockusersplanpricing_layout.tmpl.html'
                }
            },
            data: {
                permissions: {
                    only: ['viewAuthentication']
                }
            }
        })
        .state('TimeClockUsersPrice.timeclockusersplanpricing', {
            url: '/timeclockusersplanpricing',
            templateUrl: 'app/authentication/timeclockusersplanpricing/views/timeclockusersplanpricing.tmpl.html',
            // set the controller to load for this page
            controller: 'timeclockusersplanpricingController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
    }
})();
