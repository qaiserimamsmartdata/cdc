(function () {
    'use strict';

    angular
        .module('app.authentication')
        .controller('ForgotController', ForgotController);

    /* @ngInject */
    function ForgotController($scope,$localStorage, $state, $mdToast, $filter, $http, triSettings, HOST_URL, notificationService) {
        var vm = this;
        vm.triSettings = triSettings;
        vm.ResetLogo = "assets/images/logo.svg";
        vm.user = {
            EmailId: ''
        };
        vm.forgotPassword = forgotPassword;
        function forgotPassword() {
            localStorage.ParentEmail=vm.user.EmailId;
            $http.post(HOST_URL.url + '/api/User/ForgotPassword', vm.user).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.Message);
                    $state.go('authentication.login');
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.Message);

                }
            });
        }
    }
})();
