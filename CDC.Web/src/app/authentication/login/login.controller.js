(function () {
    'use strict';

    angular
        .module('app.authentication')
        .controller('LoginController', LoginController);

    /* @ngInject */
    function LoginController($scope, $http, $state, $stateParams, $mdDialog, $localStorage, notificationService
        , $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService,
        triSettings, AgencyService, UserService, authenticationService, $location, $window, FamilyService) {
        var vm = this;
        vm.ImageUrl = HOST_URL.url + 'Homepage/Home.html';
        vm.loginClick = loginClick;
        vm.showProgressbar = false;
        vm.LoginLogo = "assets/images/logo.svg";
        localStorage.clear();
        localStorage.HostUrl = HOST_URL.url;

        vm.socialLogins = [{
            icon: 'fa fa-twitter',
            color: '#5bc0de',
            url: '#'
        }, {
                icon: 'fa fa-facebook',
                color: '#337ab7',
                url: '#'
            }, {
                icon: 'fa fa-google-plus',
                color: '#e05d6f',
                url: '#'
            }, {
                icon: 'fa fa-linkedin',
                color: '#337ab7',
                url: '#'
            }];
        vm.triSettings = triSettings;
        vm.user = {
            email: ''.toString(),
            password: ''.toString()
        };

        GetLocalTimezoneName();
        function GetLocalTimezoneName() {
            var n = new Date,
                t = n.getFullYear().toString() + "-" + ((n.getMonth() + 1).toString().length === 1 ? "0" + (n.getMonth() + 1).toString() : (n.getMonth() + 1).toString()) + "-" + (n.getDate().toString().length === 1 ? "0" + n.getDate().toString() : n.getDate().toString()) + "T" + (n.getHours().toString().length === 1 ? "0" + n.getHours().toString() : n.getHours().toString()) + ":" + (n.getMinutes().toString().length === 1 ? "0" + n.getMinutes().toString() : n.getMinutes().toString()) + ":" + (n.getSeconds().toString().length === 1 ? "0" + n.getSeconds().toString() : n.getSeconds().toString());
            vm.selectedTimeZone = new Date(t + "Z").toString().match(/\(([A-Za-z\s].*)\)/)[1];
            vm.TimeZone = vm.selectedTimeZone;
        }
        vm.Url = $location.$$absUrl;
        vm.redirectPricing = 0;
        vm.redirectToHomePage = redirectToHomePage;
        function redirectToHomePage() {
            $window.location.href = vm.ImageUrl;
        }
        if (vm.Url == 'http://betaparent.pinwheelcare.com/login')
            vm.redirectPricing = 1;
        function loginClick(login) {
            if (login.$valid) {
                vm.showProgressbar = true;
                var model = {
                    UserName: login.email.$modelValue,
                    Password: login.password.$modelValue,
                    Url: vm.Url
                };
                delete $http.defaults.headers.common.Authorization;
                $http.post(HOST_URL.url + '/api/User/GetToken', model).then(function (response) {
                    if (response.data.IsSuccess == true && response.data.Content != null) {
                        if (response.data.Content.TimeZone == null || response.data.Content.TimeZone == undefined)
                            localStorage.TimeZone = vm.TimeZone;
                        else
                            localStorage.TimeZone = response.data.Content.TimeZone;
                        vm.showProgressbar = false;

                        var roleModel = {};
                        vm.currentUser = {
                            displayName: '',
                            avatar: 'assets/images/avatars/avatar-5.png'
                        };
                        localStorage.Token = response.data.Token;
                        console.log("Token is:" + response.data.Token);
                        roleModel.ID = response.data.Content.RoleId;
                        localStorage.RoleId = roleModel.ID;
                        roleModel.RoleName = response.data.Content.RoleName;
                        localStorage.UserId = response.data.Content.UserId;
                        localStorage.EmailId = response.data.Content.EmailId;
                        var rolesLst = [];
                        rolesLst.push(roleModel.RoleName);
                        localStorage.setItem('roles', JSON.stringify(rolesLst));

                        if (roleModel.RoleName == "AGENCY") {
                            localStorage.MaxNumberOfParticipants = null;
                            localStorage.PlanId = null;
                            localStorage.PlanId = response.data.Content.PricingPlan.ID;
                            localStorage.StripeUserId = response.data.Content.StripeUserId;
                            localStorage.StripeTimeClockSubscriptionId = response.data.Content.StripeTimeClockSubscriptionId;
                            localStorage.Image = "assets/images/avatars/avatar-5.png";
                            localStorage.setItem('PricingPlan', JSON.stringify(response.data.Content.PricingPlan));
                            localStorage.MaxNumberOfParticipants = response.data.Content.PricingPlan.MaxNumberOfParticipants;
                            localStorage.AgencyTimeClockUsers = response.data.Content.PricingPlan.TimeClockUsers;
                            if (response.data.Content.TimeClockUsersPlan != null) {
                                localStorage.TimeClockPlanUsers = response.data.Content.TimeClockUsersPlan.MaxNumberOfTimeClockUsers;
                                localStorage.TotalTimeClockUsers = response.data.Content.TotalTimeClockUsers;
                            }
                            else {
                                localStorage.TimeClockPlanUsers = 0;
                                localStorage.TotalTimeClockUsers = 0;
                            }
                            vm.currentUser.displayName = response.data.Content.AgencyName;
                            localStorage.profileName = response.data.Content.AgencyName;
                            localStorage.displayName = vm.currentUser;
                            AgencyService.SetId(response.data.Content.ID, response.data.Content.AgencyName);
                            localStorage.agencyId = response.data.Content.ID;
                            localStorage.AgencyName = response.data.Content.AgencyName;
                            localStorage.agencyName = response.data.Content.AgencyName;
                            localStorage.Portal = 'AGENCY';
                            localStorage.DifferPortal = 'AGENCY';
                            //////////////////for local storage///////////////////////////////  
                            //////For Checking Maxmimum Enrolled Participants
                            vm.query = {
                                AgencyId: localStorage.agencyId,

                            }
                            vm.promise = CommonService.getEnrolledParticipantsCount(vm.query);

                            vm.promise.then(function (responseForEnrolledParticipants) {
                                if (responseForEnrolledParticipants.IsSuccess) {
                                    localStorage.TotalEnrolledParticipants = responseForEnrolledParticipants.TotalEnrolledParticipants;

                                    ////////////////////////////////////
                                    ///For Trial Period Expires

                                    if (response.data.Content.IsTrial == true) {
                                        var days = response.data.trialDays;
                                        if (days <= 0) {
                                            notificationService.displaymessage('You trial period expired,you can not login.');
                                            return;
                                        }

                                    }
                                    localStorage.DashboardagencyId = localStorage.agencyId;
                                    notificationService.displaymessage('You have logged in successfully.');
                                    //////////////////////////////////////
                                    ///For checking first time login then jump to usersetting
                                    var IsLoggedFirstTime = response.data.Content.IsLoggedFirstTime;
                                    if (response.data.Content.IsLoggedFirstTime == false) {
                                        vm.promise = AgencyService.SetIsLoggedFirstTime(response.data.Content.ID)
                                        vm.promise.then(function (response) {
                                            if (response.IsSuccess == true) {
                                                $state.go('triangular.dashboard', { obj: IsLoggedFirstTime });
                                            }
                                        });
                                    }
                                    else {
                                        $state.go('triangular.dashboard', { obj: IsLoggedFirstTime });
                                    }

                                }
                            });

                            return true;
                        }
                        else if (roleModel.RoleName == "STAFF") {
                            vm.currentUser.displayName = response.data.Content.FullName;
                            localStorage.profileName = response.data.Content.FullName;
                            localStorage.agencyId = response.data.Content.AgencyRegistrationId;
                            localStorage.TimeZone = response.data.Content.TimeZone;
                            localStorage.displayName = vm.currentUser;
                            localStorage.Image = response.data.Content.ImagePath;
                            localStorage.staffId = response.data.Content.ID;
                            localStorage.Portal = "STAFF";
                            localStorage.DifferPortal = 'STAFF';
                            $state.go('triangular.staffdashboard')
                            return true;
                        }
                        else if (roleModel.RoleName == "SuperAdmin") {
                            vm.currentUser.displayName = "Sumedh";
                            localStorage.profileName = "Sumedh";
                            localStorage.displayName = vm.currentUser;
                            localStorage.SuperAdminId = response.data.Content.ID;
                            localStorage.Portal = "SuperAdmin";
                            localStorage.DifferPortal = 'SuperAdmin';
                            localStorage.Image = "assets/images/avatars/avatar-5.png";
                            $state.go('triangular.superadmindashboard')
                            return true;
                        }
                        else if (roleModel.RoleName == "FAMILY") {
                            vm.currentUser.displayName = response.data.Content.FullName;
                            localStorage.profileName = response.data.Content.FullName;
                            localStorage.agencyId = response.data.Content.AgencyId;
                            localStorage.PrimaryParentEmail = response.data.Content.EmailId;
                            localStorage.displayName = vm.currentUser;
                            localStorage.FamilyId = response.data.Content.ID;
                            localStorage.Image = response.data.Content.ImagePath;   
                            localStorage.Portal = "FAMILY";
                            localStorage.DifferPortal = 'FAMILY';
                            var IsLoggedFirstTimeParent = response.data.Content.IsLoggedFirstTime;
                            if (IsLoggedFirstTimeParent == false) {
                                var EmailId = localStorage.PrimaryParentEmail;
                                var ParentID= response.data.Content.ID;
                                localStorage.clear();
                                localStorage.ParentID = ParentID;
                                localStorage.PrimaryParentEmail = EmailId;
                                $location.path('/family/resetPassword')
                            }
                            else {
                                $state.go('triangular.parentportaldashboard')
                                return true;
                            }

                        }

                    }
                    else {
                        vm.showProgressbar = false;
                        notificationService.displaymessage('Invalid email or password.');
                        return false;
                    }
                });

            }
        };
    }
})();
