(function() {
    'use strict';

    angular
        .module('app.authentication', ['app.category','app.examples.dashboards','app.pricing','app.agencyregistration','app.timeclockusersplanpricing','app.pricingupdate']);
})();