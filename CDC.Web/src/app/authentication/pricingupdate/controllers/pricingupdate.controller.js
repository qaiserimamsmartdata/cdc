(function () {
    'use strict';

    angular
        .module('app.pricingupdate')
        .controller('pricingupdateController', pricingupdateController);

    /* @ngInject */
    function pricingupdateController($state, triSettings, HOST_URL, planrateService, notificationService, CommonService, $localStorage, $filter, $http) {
        var vm = this;
        vm.registerurl = HOST_URL.url + '#/agency';
          vm.Url = HOST_URL.url + 'Homepage/Home.html';
        localStorage.PricePlan = null;
        vm.model = {};
        vm.model.TimeClockStaffCount = localStorage.TimeClockStaffCount;
        vm.model.PricingPlanId = localStorage.PlanId;
        vm.GotoAgencyRegistration = GotoAgencyRegistration;
        vm.GetPricingPlans = GetPricingPlans;
        vm.GetSelectedPlan = GetSelectedPlan;
        vm.UpdateAgencyPricingPlan = UpdateAgencyPricingPlan;
        vm.showProgressbar = true;
        GetPricingPlans();
        function GetPricingPlans() {
            vm.promise = CommonService.getPricingPlans({ IsFromUpdate: true });
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.PricingPlans = null;
                    vm.PricingPlans = eval(response.Content);
                    var FilteredData = $filter('filter')(vm.PricingPlans, { ID: localStorage.PlanId });
                    vm.selectedPlan = {
                        plan: FilteredData[0].Name
                    };
                    vm.planPrice = {
                        plan: FilteredData[0].Name
                    };
                    vm.priceText = {
                        text: ''
                    };
                    vm.registerFor = {
                        text: FilteredData[0].MinNumberOfParticipants + "-" + FilteredData[0].MaxNumberOfParticipants
                    };

                    vm.showProgressbar = false;
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('No Plans found.');
                }
            });
        };
        function GetSelectedPlan() {
            vm.PricingPlans.forEach(function (element) {
                if (element.Name == vm.selectedPlan.plan) {
                    vm.planPrice = {
                        plan: element.Name
                    };
                    vm.priceText = {
                        text: ''
                    };
                    vm.registerFor = {
                        text: element.MinNumberOfParticipants + "-" + element.MaxNumberOfParticipants
                    };
                    localStorage.setItem('PricePlan',JSON.stringify(element));
                    vm.model.PricingPlanId = element.ID;
                }
            }, this);
        }
        function UpdateAgencyPricingPlan(model) {
            vm.showProgressbar = true;
            vm.model.ID = localStorage.agencyId;
            var FilteredData = $filter('filter')(vm.PricingPlans, { ID: vm.model.PricingPlanId });
            vm.model.TotalRequireEnrollParticipants = FilteredData[0].MaxNumberOfParticipants;
            ////for timeclockusers count
            vm.model.TimeClockStaffCount = localStorage.TimeClockStaffCount;
            vm.model.AgencyTimeClockUsers = FilteredData[0].TimeClockUsers;
            vm.model.TimeClockPlanUsers = localStorage.TimeClockPlanUsers;
            //////////////////////////////////////////////////////////////////////////
            $http.post(HOST_URL.url + '/api/AgencyRegistration/UpdateAgencyPricingPlan', vm.model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.ReturnMessage[0]);
                    localStorage.TotalTimeClockUsers = response.data.Content.TotalTimeClockUsers;
                    localStorage.MaxNumberOfParticipants = null;
                    localStorage.PlanId = null;
                    localStorage.PlanId = response.data.Content.PricingPlan.ID;
                    localStorage.MaxNumberOfParticipants = response.data.Content.PricingPlan.MaxNumberOfParticipants;

                    //////for AgencyTimeClockUsers count
                    localStorage.AgencyTimeClockUsers = vm.model.AgencyTimeClockUsers;
                    /////////////////////////////////

                    $state.go('triangular.dashboard');
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to update at the moment. Please try again after some time.');
                }
            });
        }
        function GotoAgencyRegistration() {
            if (vm.selectedPlan.plan != "" && vm.selectedPlan.plan != null) {
                if (localStorage.PricePlan == null || localStorage.PricePlan == undefined) {
                    localStorage.setItem('PricePlan',JSON.stringify(vm.PricingPlans[0]));                  
                }
                // planrateService.setplanRate(vm.selectedPlan.plan);
                $state.go('Register.agency');
            }
            else {
                notificationService.displaymessage('Please choose a plan.');
            }
        }
    }
})();