(function() {
    'use strict';

    angular
        .module('app.pricingupdate')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
		 $stateProvider
        .state('PriceUpdate', {
            abstract: true,
            views: {
                'root': {
                    templateUrl: 'app/authentication/pricingupdate/layouts/pricing_layout.tmpl.html'
                }
            },
            data: {
                permissions: {
                    only: ['viewAuthentication']
                }
            }
        })
        .state('PriceUpdate.pricing', {
            url: '/updatepricing',
            templateUrl: 'app/authentication/pricingupdate/views/pricingupdate.tmpl.html',
            // set the controller to load for this page
            controller: 'pricingupdateController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
    }
})();
