(function () {
    'use strict';

    angular
        .module('app.authentication')
        .controller('ResetPasswordController', ResetPasswordController);

    /* @ngInject */
    function ResetPasswordController($scope, $localStorage, $state, $stateParams, $mdToast, $filter, $http, triSettings, HOST_URL, notificationService) {
        var vm = this;
        var vm = this;
        vm.triSettings = triSettings;
        vm.user = {};
        var GUID = $stateParams.Guid;
        var splitGuid = GUID.split("&");
        vm.user.Guid = splitGuid[0];
        vm.user.EmailId = splitGuid[1];

        vm.ResetPassword = ResetPassword;
        ////////////////
        function ResetPassword() {
            $http.post(HOST_URL.url + '/api/User/ResetPassword', vm.user).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.Message);
                    $state.go('authentication.login');
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.Message);
                }
            });
        }
    }
})();