(function () {
    'use strict';

    angular
        .module('app.authentication')
        .controller('SignupController', SignupController);

    /* @ngInject */
    function SignupController($filter, $scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService, triSettings, authenticationService) {
        var vm = this;
        vm.triSettings = triSettings;
        vm.signupClick = signupClick;
        vm.showProgressbar = false;
        vm.user = {
            name: '',
            email: '',
            password: '',
            confirm: ''
        };
        $scope.submitText = "sign up";
        function signupClick(agencyObj) {
            if (agencyObj.$valid) {
                $scope.submitText = "processing";
                vm.showProgressbar = true;
                var model = {
                    AgencyName: agencyObj.name.$modelValue,
                    Email: agencyObj.email.$modelValue,
                    Password: agencyObj.password.$modelValue,
                    IsLoggedFirstTime: false
                };
                vm.promise = authenticationService.agencySignupService(model);
                vm.promise.then(function (response) {
                    if (response == 1) {
                        vm.showProgressbar = false;
                        $scope.submitText = "sign up";
                        NotificationMessageController('Agency is registered Successfully.');
                        NotificationMessageController('Login information has been sent to your email.');
                        $state.go("authentication.login");
                    }
                    else {
                        vm.showProgressbar = false;
                        $scope.submitText = "sign up";
                        vm.showProgressbar = false;
                        NotificationMessageController('Unable to register at the moment. Please try again after some time.');
                    }
                });
            }
        }
        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
    }
})();
