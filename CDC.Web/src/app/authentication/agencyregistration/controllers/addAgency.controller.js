(function () {
    'use strict';
    angular
        .module('app.agencyregistration')
        .controller('AddAgencyController', AddAgencyController);

    /* @ngInject */
    function AddAgencyController($scope, $http, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, CommonService, planrateService, apiService, $localStorage) {
        if (localStorage.getItem('PricePlan') == "null" || localStorage.getItem('PricePlan') == undefined) {
            $state.go("authentication.login");
            return;
        }
        var vm = this;
        vm.addAgency = addAgency;
        vm.showProgressbar = false;
        vm.assignEmailToLocation = assignEmailToLocation;
        vm.GetAllCountry = GetAllCountry;
        vm.stateChange = stateChange;
        vm.GetState = GetState;
        vm.disable = true;
        if (JSON.parse(localStorage.getItem('PricePlan')).Price > 0)
            vm.planRate = JSON.parse(localStorage.getItem('PricePlan')).Name + "[$" + JSON.parse(localStorage.getItem('PricePlan')).Price + "/month]";
        else
            vm.planRate = JSON.parse(localStorage.getItem('PricePlan')).Name;
        vm.GotoPricing = GotoPricing;
        vm.OnCheck = OnCheck;
        $scope.id = 0;

        var year = new Date().getFullYear();
        var range = [];
        range.push(year);
        for (var i = 1; i < 11; i++) {
            range.push(year + i);
        }
        $scope.years = range;


        var bDate = new Date();
        bDate = bDate.setDate(bDate.getDate() - 365);
        vm.minBirthDate = new Date(bDate);

        //for min 80years.
        var NewDate = new Date();
        NewDate = NewDate.setDate(NewDate.getDate() - 29200);
        vm.NewMinDate = new Date(NewDate);

        vm.todayData = new Date();
        GetAllCountry();
        vm.agency = {};
        vm.Failed = Failed;
        init();
        function init() {
            apiService.post("api/TimeSheet/TimeZones", null,
                getTimeZoneSuccess,
                Failed);

            function getTimeZoneSuccess(result) {
                vm.TimezoneList = result.data.Response;
                vm.TimezoneListSelect = 'Select Timezone';
            }
        }
        function Failed(result) {
            notificationService.displaymessage('Please try again after some time.');
        }
        function GetAllCountry() {
            vm.AllCountries = null; // Clear previously loaded state list
            var myPromise = CommonService.getCountries();
            myPromise.then(function (resolve) {
                vm.AllCountries = resolve;
                vm.agency.CountryId = "1".toString();
                GetState(vm.agency.CountryId);
                $scope.CountryTextToShow = "--Select Country--";
            }, function (reject) {

            });

        };
        function GetState(country) {
            //Load State
            vm.States = null;
            var myPromise = CommonService.getStates(country);
            // wait until the promise return resolve or eject
            //"then" has 2 functions (resolveFunction, rejectFunction)
            myPromise.then(function (resolve) {
                vm.States = resolve;
                $scope.StateTextToShow = "--Select States--";
            }, function (reject) {

            });

        };
        function GotoPricing() {
            if (vm.planRate != "")
                planrateService.setplanRate(vm.planRate);
            $state.go('Price.pricing');
        };
      
        function OnCheck() {
            if ($scope.IsChecked == true) {
                vm.agency.CardBillingAddress = vm.agency.Address;
                vm.agency.CardCountryId = vm.agency.CountryId;
                vm.agency.CardStateId = vm.agency.StateId;
                vm.agency.CardCityName = vm.agency.CityName;
                vm.agency.CardPostalCode = vm.agency.PostalCode
            }
            else {
                vm.agency.CardBillingAddress = '';
                vm.agency.CardCountryId = '';
                vm.agency.CardStateId = '';
                vm.agency.CardCityName = '';
                vm.agency.CardPostalCode = '';

            }

        };

        function stateChange() {
            vm.disable = vm.agency.Acceptance == true ? false : true;
        }
        vm.placeholder = "____-____-____-____";
        vm.pattern = "9999-9999-9999-9999";
        vm.CVV="3";
        vm.CVVmax="3";
        
        vm.checkStatus=checkStatus;
        function checkStatus() {
            vm.agency.CardNumber = '';
            vm.agency.CVV='';
            if (vm.agency.CardType != 'American Express') {
                vm.pattern = "9999-9999-9999-9999";
                vm.placeholder = "____-____-____-____";
                vm.CVV="3";
                vm.CVVmax="3";
            }
            else {
                 vm.pattern = "9999-999999-99999";
                 vm.placeholder = "____-______-_____";
                vm.CVV="4";
                vm.CVVmax="4";
            }
        }
        function addAgency(accountForm, addressForm, billingForm, paymentForm) {
            debugger
            vm.disable = true;
            if (accountForm.$valid && addressForm.$valid && billingForm.$valid && paymentForm.$valid) {

                vm.showProgressbar = true;
                GetLocalTimezoneName();
                function GetLocalTimezoneName() {
                    var n = new Date,
                        t = n.getFullYear().toString() + "-" + ((n.getMonth() + 1).toString().length === 1 ? "0" + (n.getMonth() + 1).toString() : (n.getMonth() + 1).toString()) + "-" + (n.getDate().toString().length === 1 ? "0" + n.getDate().toString() : n.getDate().toString()) + "T" + (n.getHours().toString().length === 1 ? "0" + n.getHours().toString() : n.getHours().toString()) + ":" + (n.getMinutes().toString().length === 1 ? "0" + n.getMinutes().toString() : n.getMinutes().toString()) + ":" + (n.getSeconds().toString().length === 1 ? "0" + n.getSeconds().toString() : n.getSeconds().toString());
                    vm.selectedTimeZone = new Date(t + "Z").toString().match(/\(([A-Za-z\s].*)\)/)[1];
                }
                var roleModel = {};
                roleModel.RoleName = "Agency"
                var model = {};
                model = vm.agency;
                model.IsExistingAccount = false;
                //Get User roleId by role name
                $http.post(HOST_URL.url + '/api/User/GetRoleIdByRoleName', roleModel).then(function (response) {
                    if (response.data.IsSuccess == true) {
                        model.RoleId = response.data.Content.ID;
                        AddAgency(model);
                    }
                })
                function AddAgency(model) {
                    model.TimeZone = vm.TimeZone;
                    model.PricingPlanId = JSON.parse(localStorage.getItem('PricePlan')).ID;
                    model.TotalRequireEnrollParticipants = JSON.parse(localStorage.getItem('PricePlan')).MaxNumberOfParticipants;
                    model.TotalTimeClockUsers=JSON.parse(localStorage.getItem('PricePlan')).TimeClockUsers;
                    $http.post(HOST_URL.url + '/api/AgencyRegistration/AddAgency', model).then(function (response) {
                        if (response.data.IsSuccess == true) {
                            vm.showProgressbar = false;
                            if ($stateParams.Redirection == 'Super') {
                                $state.go("triangular.superadmindashboard", {}, { reload: true });
                            }
                            else {
                                $state.go('authentication.login');
                            }
                            NotificationMessageController(response.data.ReturnMessage[0]);
                        }
                        else {
                            vm.showProgressbar = false;
                            if (response.data.ValidationErrors.TokenError != null || response.data.ValidationErrors.TokenError != undefined)
                                NotificationMessageController(response.data.ValidationErrors.TokenError);
                            else
                                NotificationMessageController('Unable to add at the moment. Please try again after some time.');
                        }
                    });
                }


            }
        }

        function NotificationMessageController(message) {
            $timeout(function () {
                $rootScope.$broadcast('newMailNotification');
                $mdToast.show({
                    template: '<md-toast><span flex>' + message + '</span></md-toast>',
                    position: 'bottom right',
                    hideDelay: 5000
                });
            }, 100);
        };
        function assignEmailToLocation(email) {
            vm.agency.LocationEmailId = email;
        }
    }
})();