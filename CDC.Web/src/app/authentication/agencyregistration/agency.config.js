(function () {
    'use strict';

    angular
        .module('app.agencyregistration')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
        $stateProvider
            .state('Register', {
                abstract: true,
                views: {
                    'root': {
                        templateUrl: 'app/authentication/agencyregistration/layouts/agency_layout.tmpl.html'
                    }
                },
                data: {
                    permissions: {
                        only: ['viewAuthentication']
                    }
                }
            })
            .state('Register.agency', {
                url: '/agency',
                templateUrl: 'app/authentication/agencyregistration/views/agency_registration.tmpl.html',
                // set the controller to load for this page
                controller: 'AddAgencyController',
                controllerAs: 'vm',
                params: {
                    Redirection: null
                },
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve: {
                    data: function ($q, $http, $state, $location, $localStorage, $window) {
                        var deferred = $q.defer();
                        if (localStorage.getItem('PricePlan') != "null" && localStorage.getItem('PricePlan') != undefined&&localStorage.getItem('PricePlan')!=null)
                            deferred.resolve();
                        else
                            $location.path('/login');
                        return deferred.promise;

                    }
                }
            })
    }
})();
