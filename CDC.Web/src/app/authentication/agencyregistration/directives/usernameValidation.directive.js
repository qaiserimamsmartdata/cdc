angular
    .module('app')
    .directive('ngUnique', ['$http', 'HOST_URL', function (async, HOST_URL) {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                elem.on('blur', function (evt) {
                    if (attrs.ngUnique == 'username') {
                        scope.$apply(function () {
                            var val = elem.val();
                            var req = { "name": val }
                            var ajaxConfiguration = { method: 'POST', url: HOST_URL.url + 'api/User/isexistname', data: req };
                            async(ajaxConfiguration)
                                .success(function (data, status, headers, config) {
                                    ctrl.$setValidity('unique', !data);
                                });
                        });
                    }                   
                });
            }
        }
    }]);