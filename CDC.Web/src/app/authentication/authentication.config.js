(function () {
    'use strict';

    angular
        .module('app.authentication')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
            .state('authentication', {
                abstract: true,
                views: {
                    'root': {
                        templateUrl: 'app/authentication/layouts/authentication.tmpl.html'
                    }
                },
                data: {
                    permissions: {
                        only: ['viewAuthentication']
                    }
                }
            })
            .state('authentication.login', {
                url: '/login',
                templateUrl: 'app/authentication/login/login.tmpl.html',
                controller: 'LoginController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
                resolve: {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"LOGIN",$window);
                    }
                }

            })
            .state('authentication.parentlogin', {
                url: '/parent/login',
                templateUrl: 'app/authentication/login/login.tmpl.html',
                controller: 'LoginController',
                controllerAs: 'vm',
                params: {
                    obj: null
                },
               
            })
            .state('authentication.signup', {
                url: '/signup',
                templateUrl: 'app/authentication/signup/signup.tmpl.html',
                controller: 'SignupController',
                controllerAs: 'vm'
            })
            .state('authentication.lock', {
                url: '/lock',
                templateUrl: 'app/authentication/lock/lock.tmpl.html',
                controller: 'LockController',
                controllerAs: 'vm'
            })
            .state('authentication.forgot', {
                url: '/forgot',
                templateUrl: 'app/authentication/forgot/forgot.tmpl.html',
                controller: 'ForgotController',
                controllerAs: 'vm'
            })
               .state('authentication.ParentPassword', {
                url: '/family/resetPassword',
                templateUrl: 'app/authentication/ParentPassword/parentpassword.tmpl.html',
                // set the controller to load for this page
                controller: 'ParentPasswordController',
                controllerAs: 'vm',
               
            })
            .state('authentication.reset', {
                url: '/reset/:Guid',
                templateUrl: 'app/authentication/profile/resetpassword.html',
                controller: 'ResetPasswordController',
                controllerAs: 'vm'
            })
            .state('triangular.profile', {
                url: '/profile',
                templateUrl: 'app/authentication/profile/profile.tmpl.html',
                controller: 'ProfileController',
                controllerAs: 'vm'
            })
            .state('authentication.parentKiosklogin', {
                url: '/kiosk',
                  templateUrl: 'app/Attendance/views/kioskparentSignIn.tmpl.html',
               controller: 'ParentSignInController',
                controllerAs: 'vm',
                 data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewKioskParticipantAttendance']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
               
            })
             .state('authentication.kioskAttendanceParentMark', {
                url: '/kiosk/attendance/mark',
                templateUrl: 'app/Attendance/views/kioskparticipantattendancelist.tmpl.html',
                // set the controller to load for this page
                controller: 'KioskParticipantAttendanceController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                 params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewKioskParticipantAttendance']
                    }
                },
                resolve: {
                    data: function (apiService, $q, $http, $state, $location, $localStorage, $window) {
                        return apiService.checkLoginOnce($q, $http, $state, $location, $localStorage, "AGENCY", $window);
                    }
                }
            });
    }
})();
