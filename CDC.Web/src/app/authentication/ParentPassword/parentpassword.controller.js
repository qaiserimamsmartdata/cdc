(function () {
    'use strict';

    angular
        .module('app.authentication')
        .controller('ParentPasswordController', ParentPasswordController);

    /* @ngInject */
    function ParentPasswordController($scope, $http, $state, $stateParams, $mdDialog, $localStorage, notificationService
        , $mdEditDialog, $timeout, $mdToast, $rootScope, $q, HOST_URL, ClassService, CommonService,
        triSettings, AgencyService, UserService, authenticationService, $location, $window, FamilyService) {
        var vm = this;
        vm.LoginLogo = "assets/images/logo.svg";
        vm.loginClick = loginClick;
        function loginClick(model) {
            model.EmailId = localStorage.PrimaryParentEmail;
            $http.post(HOST_URL.url + '/api/User/ResetParentPassword', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.Message);
                    var ParentID = localStorage.ParentID;
                    localStorage.clear();
                    vm.promise = FamilyService.SetIsLoggedFirstTime(ParentID);
                    vm.promise.then(function (response) {
                        console.log(response);
                        if (response.IsSuccess == true) {
                             $location.path('/login')
                        }
                        else {
                            notificationService.displaymessage("Something went wrong.");
                        }
                    });
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.Message);

                }
            });
        }
    }
})();
