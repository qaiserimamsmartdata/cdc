(function() {
    'use strict';

    angular
        .module('app.pricing')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
		 $stateProvider
        .state('Price', {
            abstract: true,
            views: {
                'root': {
                    templateUrl: 'app/authentication/pricing/layouts/pricing_layout.tmpl.html'
                }
            },
            data: {
                permissions: {
                    only: ['viewAuthentication']
                }
            }
        })
        .state('Price.pricing', {
            url: '/pricing',
            templateUrl: 'app/authentication/pricing/views/pricing.tmpl.html',
            // set the controller to load for this page
            controller: 'pricingController',
            controllerAs: 'vm',
              params: {
                    Redirection: null
                },
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
    }
})();
