(function () {
    'use strict';

    angular
        .module('app.pricing')
        .controller('pricingController', pricingController);

    /* @ngInject */
    function pricingController($state, triSettings, HOST_URL, planrateService, notificationService, $window, CommonService, $localStorage, $stateParams) {
        var vm = this;
        vm.registerurl = HOST_URL.url + '#/agency';
        vm.Url = HOST_URL.url + 'Homepage/Home.html';
        localStorage.setItem('PricePlan', null);
        vm.HomePageRediect = HomePageRediect;
        function HomePageRediect() {
            $window.location.href = HOST_URL.url + 'Homepage/Home.html';
        }
        vm.GotoAgencyRegistration = GotoAgencyRegistration;
        vm.GetPricingPlans = GetPricingPlans;
        vm.GetSelectedPlan = GetSelectedPlan;

        GetPricingPlans();
        vm.showProgressbar = true;
        function GetPricingPlans() {
            vm.promise = CommonService.getPricingPlans(null);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.PricingPlans = null;
                    vm.PricingPlans = eval(response.Content);
                    vm.selectedPlan = {
                        plan: vm.PricingPlans[0].Name
                    };
                    vm.planPrice = {
                        plan: vm.PricingPlans[0].Name
                    };
                    vm.priceText = {
                        text: ''
                    };
                    vm.registerFor = {
                        text: vm.PricingPlans[0].MinNumberOfParticipants + "-" + vm.PricingPlans[0].MaxNumberOfParticipants
                    };
                    vm.showProgressbar = false;
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('No Plans found.');
                }
            });
        };
        function GetSelectedPlan() {
            vm.PricingPlans.forEach(function (element) {
                if (element.Name == vm.selectedPlan.plan) {
                    vm.planPrice = {
                        plan: element.Name
                    };
                    vm.priceText = {
                        text: ''
                    };
                    vm.registerFor = {
                        text: element.MinNumberOfParticipants + "-" + element.MaxNumberOfParticipants
                    };
                    localStorage.setItem('PricePlan', JSON.stringify(element));
                }
            }, this);
        }

        function GotoAgencyRegistration() {
            if (vm.selectedPlan.plan != "" && vm.selectedPlan.plan != null) {
                if (localStorage.PricePlan == "null" || localStorage.PricePlan == undefined || localStorage.PricePlan == null) {
                    localStorage.setItem('PricePlan', JSON.stringify(vm.PricingPlans[0]));
                }
                $state.go('Register.agency', { Redirection: $stateParams.Redirection });
            }
            else {
                notificationService.displaymessage('Please choose a plan.');
            }
        }
    }
})();