(function () {
    'use strict';

    angular
        .module('app.authentication')
        .factory('authInterceptorService', authInterceptorService);

    authInterceptorService.$inject = ['$q', '$location', 'HOST_URL', '$localStorage'];

    /* @ngInject */
    function authInterceptorService($q,$location, HOST_URL, $localStorage) {
        return {
            request: _request,
            responseError: _responseError
        };
        function _request(config) {
            config.headers = config.headers || {};
              var authData = localStorage.getItem('Token');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData;
            }
            return config;
        }
        function _responseError(rejection) {
            if (rejection.status === 401) {
                localStorage.clear();
                $location.path('/login');
            }
            return $q.reject(rejection);
        }
    }
})();