(function () {
    'use strict';

    angular
        .module('app.dailystatus')
        .controller('DailyStatusPageController', DailyStatusPageController);
    /* @ngInject */
    function DailyStatusPageController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, DailyStatusService, CommonService, AgencyService, filerService, $localStorage, HOST_URL) {
        var vm = this;
        vm.selectedItem = null;
        vm.searchText = null;
        vm.querySearch = querySearch;
        vm.Date = new Date();
        var FamilyId=0;
        if(localStorage.FamilyId!=undefined||localStorage.FamilyId!=null)
        FamilyId=localStorage.FamilyId;
        function querySearch(query,dailystatus) {
            var results = query ? vm.AllParticipant.filter(createFilterFor(query)) : vm.AllParticipant;
            var deferred = $q.defer();
            dailystatus.autocompleteField.$error=true;
            dailystatus.autocompleteField.$touched=true;
            $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
            return deferred.promise;
            
        }
        // vm.showDeleteClassConfirmDialoue = showDeleteClassConfirmDialoue;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.getDailyStatusCategory = getDailyStatusCategory;
        getDailyStatusCategory();
        function getDailyStatusCategory() {
            var model = {};
            vm.promise = CommonService.getDailyStatusCategory(model);
            vm.promise.then(function (response) {
                // alert(response.IsSuccess);
                if (response.IsSuccess == true) {
                    vm.DailyStatusCatgory = response.Content;
                }
                else {

                    notificationService.displaymessage('Unable to get daily status category at the moment. Please try again after some time.');
                }
            });
        }

        // function showDeleteClassConfirmDialoue(data) {

        //     // Appending dialog to document.body to cover sidenav in docs app
        //     var confirm = $mdDialog.confirm()
        //         .title('Are you sure to delete this record permanently?')
        //         //   .textContent('All of this branch data and all "Branch Managers/Delivery Personals" associated with this class  will be deleted.')
        //         .ariaLabel('Lucky day')
        //         .targetEvent(event)
        //         .ok('Delete')
        //         .cancel('Cancel');
        //     $mdDialog.show(confirm).then(function () {
        //         deleteDailyStatus(data);
        //     }, function () {
        //         // $scope.hide();
        //     });
        // }
        function deleteDailyStatus(data) {
            vm.promise = DailyStatusService.DeletedailystatusById(data);
            vm.promise.then(function (response1) {
                // alert(response.IsSuccess);
                if (response1.IsSuccess == true) {
                    notificationService.displaymessage('Daily Status record deleted successfully.');
                    GetDailyStatusList();
                }
                else {

                    notificationService.displaymessage('Unable to get delete at the moment. Please try again after some time.');
                }
            });
        }
        vm.loadParticipants = loadParticipants;
        loadParticipants();
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            name: '',
            AgencyId: localStorage.agencyId,
            purposeid: 0,
            FamilyId:FamilyId,
            FromDate: new Date(),
            ToDate: new Date()
        };
        vm.dailyStatusCount = 0;
        vm.GetDailyStatusList = GetDailyStatusList;

        GetDailyStatusList();
        $rootScope.$on("GetDailyStatusList", function () {
            GetDailyStatusList();
        });
        function GetDailyStatusList() {
            vm.promise = DailyStatusService.GetAllDailyStatusParticipants($scope.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.NoClassData = false;
                    vm.dailyStatusList = response.Content;
                    vm.dailyStatusCount = response.TotalRows;
                    $scope.selected = [];
                }
                else {
                    if (response.classList.length == 0) {
                        vm.NoClassData = true;
                        vm.classList = [];
                        vm.classCount = 0;
                        notificationService.displaymessage('No Class list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to get class list at the moment. Please try again after some time.');
                    }
                }

            });
        };
        vm.loadParticipantOnDateChange = loadParticipantOnDateChange;
        function loadParticipantOnDateChange() {
            loadParticipants();
        }
        function loadParticipants() {
            vm.selectedItem = null;
            var model = {
                agencyId: localStorage.agencyId,
                Date: vm.Date
            }
            vm.promise = CommonService.getAllParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllParticipant = null;
                    vm.AllParticipant = eval(response.Content);
                    vm.AllParticipant.forEach(function (element) {
                        element.value = element.name.toLowerCase();
                        element.display = element.name.toLowerCase();
                    }, this);
                }
                else {
                    vm.AllParticipant = null;
                }
            });
        }

        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };
        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;



        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.changed = function () {
            $log.log('Time changed to: ' + $scope.mytime);
        };

        $scope.clear = function () {
            $scope.mytime = null;
        };
        vm.reset = reset;
        function reset() {
            $scope.query.purposeid = 0;
            $scope.query.ParticipantName = '';
            $scope.query.FromDate = new Date();
            $scope.query.ToDate = new Date();

            GetDailyStatusList();
        };
        $scope.currentDate = new Date();
        vm.ShowDailogueDailyStatusAdd = ShowDailogueDailyStatusAdd;
        vm.showParticipantDailyStatusDetailDialogue = showParticipantDailyStatusDetailDialogue;

        function showParticipantDailyStatusDetailDialogue(participantId) {
            $mdDialog.show({
                controller: DailyStatusDetailPageController,
                controllerAs: 'vm',
                templateUrl: 'app/DailyStatus/views/dailyStatusDetail.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                items: { participantId: participantId }
            });
        }

        function ShowDailogueDailyStatusAdd(PurposeId, PurposeName) {
            $mdDialog.show({
                controller: AddDailyStatusPageController,
                controllerAs: 'vm',
                templateUrl: 'app/DailyStatus/views/adddailystatus.tmpl.html',
                parent: angular.element(document.body),
                escToClose: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                items: { participant: vm.selectedItem, PurposeId: PurposeId, PurposeName: PurposeName, Date: vm.Date }
            });
        }

        // vm.ShowDialogueDailyStatusUpdate = ShowDialogueDailyStatusUpdate;
        // function ShowDialogueDailyStatusUpdate(DailyStatusData) {
        //     $mdDialog.show({
        //         controller: UpdateDailyStatusPageController,
        //         controllerAs: 'vm',
        //         templateUrl: 'app/DailyStatus/views/adddailystatus.tmpl.html',
        //         parent: angular.element(document.body),
        //         escToClose: true,
        //         clickOutsideToClose: true,
        //         fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
        //         items: { DailyStatusData: DailyStatusData }
        //     });
        // }
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(participant) {
                return (participant.value.indexOf(lowercaseQuery) === 0);
            };

        }
    }
    function AddDailyStatusPageController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, DailyStatusService, CommonService, AgencyService, filerService, $localStorage, HOST_URL, items) {

        var kk = items.participant;
        var vm = this;
        vm.DailyStatusId = 0;
        vm.ParticipantName = items.participant.name;
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };
        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;



        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.changed = function () {
            $log.log('Time changed to: ' + $scope.mytime);
        };

        $scope.clear = function () {
            $scope.mytime = null;
        };
        $scope.currentDate = new Date();
        vm.PurposeName = items.PurposeName;
        vm.addDailyStatus = addDailyStatus;
        vm.cancelClick = cancelClick;
        function cancelClick() {
            $mdDialog.cancel();
        }
        function addDailyStatus(model) {
            model.DailyStatusCategoryId = items.PurposeId;
            model.StudentInfoId = items.participant.studentId;
            model.AgencyInfoId = localStorage.agencyId;
            model.Date = items.Date;
            model.StartTime = model.StartTime == null ? model.StartTime : (moment(model.StartTime).format('HH:mm:ss'));
            model.EndTime = model.EndTime == null ? model.EndTime : (moment(model.EndTime).format('HH:mm:ss'));
            if(model.StartTime<=model.EndTime)
            {
                 notificationService.displaymessage('End Time should be greater than Start Time.');
                 return;
            }
            $http.post(HOST_URL.url + '/api/DailyStatus/AddDailyStatus', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.ReturnMessage[0]);
                    cancelClick();
                    $rootScope.$emit("GetDailyStatusList", {});
                    // $state.go('triangular.dailystatus');
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
    }
 
})();