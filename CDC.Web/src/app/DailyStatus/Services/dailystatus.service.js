﻿(function () {
    'use strict';

    angular
        .module('app.dailystatus')
        .factory('DailyStatusService', DailyStatusService);

    DailyStatusService.$inject = ['$http', '$q', 'HOST_URL'];

    /* @ngInject */
    function DailyStatusService($http, $q, HOST_URL) {
        return {
            GetAllDailyStatusParticipants: GetAllDailyStatusParticipants,
            GetdailystatusListByIdService: GetdailystatusListByIdService,
            DeletedailystatusById: DeletedailystatusById,
            GetAllDailyStatus:GetAllDailyStatus,
            SetId: SetId
        };
 
        function SetId(data) {
           
            this.Id=0;
            this.dailystatusName='';
            this.Id = data.ID;
            this.dailystatusName = data.FullName;
            this.dailystatusData=data;
        };
        function GetAllDailyStatusParticipants(model) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/DailyStatus/GetAllDailyStatusParticipants', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
         function GetAllDailyStatus(model) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/DailyStatus/GetAllDailyStatus', model).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
        function GetdailystatusListByIdService(dailystatusId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/dailystatus/GetdailystatusById?dailystatusId=' + dailystatusId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };
        function DeletedailystatusById(dailystatusId) {
            var deferred = $q.defer();
            //var data = { take: query.limit, currentPage: query.page, filter: query.filter, order: query.order, SearchFields: query.SearchFields };
            $http.post(HOST_URL.url + '/api/DailyStatus/DeleteDailyStatus?dailystatusId=' + dailystatusId).success(function (response, status, headers, config) {
                deferred.resolve(response);
            }).error(function (errResp) {
                deferred.reject({ message: "Really bad" });
            });
            return deferred.promise;
            //return $http.post('http://localhost:10959/api/Student/GetAllStudents')
            //success(function (data) {
            //    return data;
            //});
        };

    }
})();