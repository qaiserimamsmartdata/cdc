(function () {
    'use strict';

    angular
        .module('app.dailystatus')
        .controller('DailyStatusPageController', DailyStatusPageController);
    /* @ngInject */
    function DailyStatusPageController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, DailyStatusService, CommonService, AgencyService, filerService, $localStorage, HOST_URL) {
        var vm = this;
        vm.FamilyId = 0;
        if (localStorage.FamilyId != undefined || localStorage.FamilyId != null) {
            vm.FamilyId = localStorage.FamilyId;
        }
        vm.selectedItem = null;
        vm.searchText = null;
        vm.querySearch = querySearch;
        vm.Date = new Date();
        localStorage.ParticipantAttendancePage = false;

        function querySearch(query) {
            var results = query ? vm.AllParticipant.filter(createFilterFor(query)) : vm.AllParticipant;
            var deferred = $q.defer();
            $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
            return deferred.promise;
        }
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.getDailyStatusCategory = getDailyStatusCategory;
        vm.setClassName = setClassName;
        vm.setFromDate = setFromDate;
        vm.setToDate = setToDate;
        function setToDate() {
            $scope.query.ToDate = new Date();
        }
        vm.ClassName = "None";
        function setClassName() {
            vm.ClassName = vm.selectedItem == null ? '' : vm.selectedItem.ClassName;
        }
        getDailyStatusCategory();
        function getDailyStatusCategory() {
            var model = {};
            vm.promise = CommonService.getDailyStatusCategory(model);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.DailyStatusCatgory = response.Content;
                }
                else {

                    notificationService.displaymessage('Unable to get daily status category at the moment. Please try again after some time.');
                }
            });
        }
        function deleteDailyStatus(data) {
            vm.promise = DailyStatusService.DeletedailystatusById(data);
            vm.promise.then(function (response1) {
                // alert(response.IsSuccess);
                if (response1.IsSuccess == true) {
                    notificationService.displaymessage('Daily Status record deleted successfully.');
                    GetDailyStatusList();
                }
                else {

                    notificationService.displaymessage('Unable to get delete at the moment. Please try again after some time.');
                }
            });
        }
        vm.loadParticipants = loadParticipants;
        loadParticipants();
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1,
            StatusId: 0,
            name: '',
            AgencyId: localStorage.agencyId,
            purposeid: 0,
            FamilyId: vm.FamilyId,
            FromDate: new Date(),
            ToDate: new Date()
        };
        vm.FromDisplayDate = $scope.query.FromDate;
        vm.ToDisplayDate = $scope.query.ToDate;
        vm.dailyStatusCount = 0;
        vm.GetDailyStatusList = GetDailyStatusList;
        vm.dsrMinDate = new Date();
        GetDailyStatusList();
        $rootScope.$on("GetDailyStatusList", function () {
            GetDailyStatusList();
        });
        function GetDailyStatusList() {
            $scope.Participant = $scope.query.ParticipantName == undefined ? "All" : $scope.query.ParticipantName;
            $scope.FromDate = $scope.query.FromDate;
            $scope.ToDate = $scope.query.ToDate;
            vm.promise = DailyStatusService.GetAllDailyStatusParticipants($scope.query);
            vm.promise.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.NoClassData = false;
                    vm.dailyStatusList = response.Content;
                    vm.dailyStatusCount = response.TotalRows;
                    vm.FromDisplayDate = $scope.query.FromDate;
                    vm.ToDisplayDate = $scope.query.ToDate;
                    $scope.selected = [];
                }
                else {
                    if (response.classList.length == 0) {
                        vm.NoClassData = true;
                        vm.classList = [];
                        vm.classCount = 0;
                        notificationService.displaymessage('No daily status list found.');
                    }
                    else {
                        notificationService.displaymessage('Unable to daily status list at the moment. Please try again after some time.');
                    }
                }
                vm.close()
            });
        };
        vm.loadParticipantOnDateChange = loadParticipantOnDateChange;
        function loadParticipantOnDateChange() {
            loadParticipants();
        }
        function loadParticipants() {
            vm.selectedItem = null;
            var model = {
                agencyId: localStorage.agencyId
            }
            vm.promise = CommonService.getAllParticipants(model);
            vm.promise.then(function (response) {
                if (response.Content.length > 0) {
                    vm.AllParticipant = null;
                    vm.AllParticipant = eval(response.Content);
                    vm.AllParticipant.forEach(function (element) {
                        element.value = element.name.toLowerCase();
                        element.display = element.name.toLowerCase();
                    }, this);
                }
                else {
                    vm.AllParticipant = null;
                }
            });
        }

        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };
        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;



        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.changed = function () {
            $log.log('Time changed to: ' + $scope.mytime);
        };

        $scope.clear = function () {
            $scope.mytime = null;
        };
        vm.reset = reset;
        function reset() {
            $scope.query.purposeid = 0;
            $scope.query.ParticipantName = undefined;
            $scope.Participant = "";
            $scope.query.FromDate = new Date();
            $scope.query.ToDate = new Date();

            GetDailyStatusList();
        };
        $scope.currentDate = new Date();
        vm.ShowDailogueDailyStatusAdd = ShowDailogueDailyStatusAdd;
        vm.showParticipantDailyStatusDetailDialogue = showParticipantDailyStatusDetailDialogue;

        function showParticipantDailyStatusDetailDialogue(studentInfoObj) {
            var dataObj = {
                participantId: studentInfoObj.ID,
                ParticipantName: studentInfoObj.FirstName + ' ' + studentInfoObj.LastName,
                FromDate: $scope.query.FromDate,
                ToDate: $scope.query.ToDate
            };
           if(localStorage.Portal=='AGENCY')
            $state.go("triangular.dailyStatusDetails", { obj: dataObj });
            else
            $state.go("triangular.parentportaldailyStatusDetails", { obj: dataObj });
        }

        function ShowDailogueDailyStatusAdd(PurposeId, PurposeName) {
            ///check that participant was attendee for that day or not
            var chechModel = {};
            vm.PurposeName = PurposeName;
            chechModel.StudentId = vm.selectedItem.studentId;
            chechModel.DateOfAccident = vm.Date == null ? null : moment(vm.Date).format('MM-DD-YYYY');
            $http.post(HOST_URL.url + '/api/common/CheckParticipantAttedance', chechModel).then(function (response) {
                if (response.data.IsSuccess == true) {
                    if (response.data.IsAttendee == true) {
                        if (PurposeId == 4) {
                            $mdDialog.show({
                                controller: AddDailyStatusPageController,
                                controllerAs: 'vm',
                                templateUrl: 'app/DailyStatus/views/adddailystatuscomment.tmpl.html',
                                parent: angular.element(document.body),
                                escToClose: true,
                                clickOutsideToClose: true,
                                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                                items: { participant: vm.selectedItem, PurposeId: PurposeId, PurposeName: PurposeName, Date: vm.Date }
                            });
                        }
                        else {
                            $mdDialog.show({
                                controller: AddDailyStatusPageController,
                                controllerAs: 'vm',
                                templateUrl: 'app/DailyStatus/views/adddailystatus.tmpl.html',
                                parent: angular.element(document.body),
                                escToClose: true,
                                clickOutsideToClose: true,
                                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                                items: { participant: vm.selectedItem, PurposeId: PurposeId, PurposeName: PurposeName, Date: vm.Date }
                            });
                        }
                    }
                    else {
                        notificationService.displaymessage("Sorry,Participant have no attendance for the day");
                        return;
                    }
                }
                else {
                    notificationService.displaymessage("Unable to check whether participant is attendee or not for that day");
                }
            });

        }

        function setFromDate(DateVal) {
            var tempDate = new Date(DateVal);
            vm.dsrMinDate = tempDate;
        };
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(participant) {
                return (participant.value.indexOf(lowercaseQuery) === 0);
            };

        }
    }
    function AddDailyStatusPageController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, DailyStatusService, CommonService, AgencyService, filerService, $localStorage, HOST_URL, items) {

        var kk = items.participant;
        var vm = this;
        vm.DailyStatusId = 0;
        vm.ParticipantName = items.participant.name;
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };
        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;



        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.changed = function () {
            $log.log('Time changed to: ' + $scope.mytime);
        };

        $scope.clear = function () {
            $scope.mytime = null;
        };
        $scope.currentDate = new Date();
        vm.PurposeName = items.PurposeName;
        vm.addDailyStatus = addDailyStatus;
        vm.cancelClick = cancelClick;
        function cancelClick() {
            $mdDialog.cancel();
        }
        function addDailyStatus(model) {
            model.DailyStatusCategoryId = items.PurposeId;
            model.StudentInfoId = items.participant.studentId;
            model.AgencyInfoId = localStorage.agencyId;
            model.Date = items.Date;
            model.StartTime = model.StartTime == null ? model.StartTime : (moment(model.StartTime).format('HH:mm:ss'));
            model.EndTime = model.EndTime == null ? model.EndTime : (moment(model.EndTime).format('HH:mm:ss'));
            model.TimeZone = localStorage.TimeZone;
            if (model.EndTime <= model.StartTime) {
                notificationService.displaymessage('End Time should be Greater than Start Time.');
                model.EndTime = null;
                model.StartTime = null;
                return false;
            }
            $http.post(HOST_URL.url + '/api/DailyStatus/AddDailyStatus', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.ReturnMessage[0]);
                    cancelClick();
                    $rootScope.$emit("GetDailyStatusList", {});
                    
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
    }

})();