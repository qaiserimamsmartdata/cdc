(function () {
    'use strict';

    angular
        .module('app.dailystatus')
        .controller('DailyStatusDetailPageController', DailyStatusDetailPageController)
        .controller('UpdateDailyStatusPageController', UpdateDailyStatusPageController);
    /* @ngInject */

    function DailyStatusDetailPageController($scope, $window, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, DailyStatusService, CommonService, AgencyService, filerService, $localStorage, HOST_URL, $filter) {
        var vm = this;

        if ($stateParams.obj == undefined || $stateParams.obj == null) {
            if(localStorage.Portal=='AGENCY')
            $state.go("triangular.dailystatus");
            else
            $state.go("triangular.parentdailystatus");
            return;
        }
        vm.FamilyId = 0;
        var ParticipantId = $stateParams.obj.participantId;
        var FromDate = $stateParams.obj.FromDate;
        var ToDate = $stateParams.obj.ToDate;
        vm.dsrMaxDate = new Date();
        vm.ParticipantName = $stateParams.obj.ParticipantName;
        vm.redirectToList = redirectToList;
        vm.FromDisplayDate = $stateParams.obj.FromDate;
        vm.ToDisplayDate = $stateParams.obj.ToDate;
        if (localStorage.FamilyId != undefined || localStorage.FamilyId != null) {
            vm.FamilyId = localStorage.FamilyId;
        }
        localStorage.ParticipantAttendancePage = false;
        vm.printClick = printClick;
        vm.toggleRight = filerService.toggleRight();
        vm.isOpenRight = filerService.isOpenRight();
        vm.close = filerService.close();
        vm.reset = reset;
        vm.setToDate = setToDate;
        function setToDate() {
            $scope.query.ToDate = new Date();
        }
        $scope.query = {
            filter: '',
            limit: '10',
            order: '-Date',
            page: 1,
            StatusId: 0,
            name: '',
            AgencyId: localStorage.agencyId,
            purposeid: 0,
            ParticipantId: ParticipantId,
            FromDate: FromDate,
            ToDate: ToDate,
            TimeZone: localStorage.TimeZone
        };
        vm.GetDailyStatusDetail = GetDailyStatusDetail;
        GetDailyStatusDetail();
        $rootScope.$on("GetDailyStatusDetail", function (event, thisdata, IsFromUpdate) {
            GetDailyStatusDetail(thisdata, IsFromUpdate);
        });
        function reset() {
            $scope.query.purposeid = 0;
            $scope.query.FromDate = FromDate;
            $scope.query.ToDate = ToDate;
            GetDailyStatusDetail();
        }
        vm.getDailyStatusCategory = getDailyStatusCategory;
        function redirectToList() {
            if (localStorage.Portal == 'AGENCY')
                $state.go('triangular.dailystatus');
            else
                $state.go('triangular.parentdailystatus');

        }
        getDailyStatusCategory();
        function getDailyStatusCategory() {
            var model = {};
            vm.promise2 = CommonService.getDailyStatusCategory(model);
            vm.promise2.then(function (response) {
                if (response.IsSuccess == true) {
                    vm.DailyStatusCatgory = response.Content;
                }
                else {

                    notificationService.displaymessage('Unable to get daily status category at the moment. Please try again after some time.');
                }
            });
        }
        vm.dailyStatusCount = 0;
        function GetDailyStatusDetail(thisdata, IsFromUpdate) {
            var purpose = '';
            if ($scope.query.purposeid > 0) {
                var PurposeData = $filter('filter')(vm.DailyStatusCatgory, { ID: parseInt($scope.query.purposeid) }, true);
                purpose = PurposeData[0].Name;
            }
            vm.purpose = purpose == '' ? "All" : purpose;
            if ((thisdata != undefined || thisdata != null) && (IsFromUpdate != undefined && IsFromUpdate == true)) {
                vm.FromDate = thisdata.UpdateFromDate;
                vm.ToDate = thisdata.UpdateToDate;
            }
            else {
                vm.FromDate = $scope.query.FromDate;
                vm.ToDate = $scope.query.ToDate;
            }
            vm.promise1 = DailyStatusService.GetAllDailyStatus($scope.query);
            vm.promise1.then(function (response) {
                if (response.Content.length > 0) {
                    vm.dailyStatusDetail = null;
                    vm.dailyStatusDetail = eval(response.Content);
                    vm.dailyStatusCount = response.TotalRows;
                    $scope.selected = [];
                }
                else {
                    vm.dailyStatusDetail = null;
                    vm.dailyStatusCount = 0;
                    notificationService.displaymessage('No Daily Status Details found.');
                }
                vm.close();
            });
        }
        vm.cancelClick = cancelClick;
        function cancelClick() {
            $mdDialog.cancel();
        }
        vm.ShowDialogueDailyStatusUpdate = ShowDialogueDailyStatusUpdate;
        function ShowDialogueDailyStatusUpdate(DailyStatusData) {
            if (DailyStatusData.DailyStatusCategoryId == 4) {
                $mdDialog.show({
                    controller: UpdateDailyStatusPageController,
                    controllerAs: 'vm',
                    templateUrl: 'app/DailyStatus/views/adddailystatuscomment.tmpl.html',
                    parent: angular.element(document.body),
                    escToClose: true,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                    items: { DailyStatusData: DailyStatusData },
                    preserveScope: true,
                    autoWrap: true,
                    skipHide: true,
                });
            }
            else {
                $mdDialog.show({
                    controller: UpdateDailyStatusPageController,
                    controllerAs: 'vm',
                    templateUrl: 'app/DailyStatus/views/adddailystatus.tmpl.html',
                    parent: angular.element(document.body),
                    escToClose: true,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                    items: { DailyStatusData: DailyStatusData, FromDate: $scope.query.FromDate, ToDate: $scope.query.ToDate },
                    preserveScope: true,
                    autoWrap: true,
                    skipHide: true,
                });
            }
        }
        vm.showDeleteClassConfirmDialoue = showDeleteClassConfirmDialoue;
        function showDeleteClassConfirmDialoue(data) {

            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this record permanently?')
                .ariaLabel('Lucky day')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                deleteDailyStatus(data);
            }, function () {
            });
        }
        function deleteDailyStatus(data) {
            vm.promise = DailyStatusService.DeletedailystatusById(data);
            vm.promise.then(function (response1) {
                if (response1.IsSuccess == true) {
                    notificationService.displaymessage('Daily Status record deleted successfully.');
                    GetDailyStatusDetail();
                }
                else {

                    notificationService.displaymessage('Unable to get delete at the moment. Please try again after some time.');
                }
            });
        }
        function printClick() {
            $window.print();
        }

    }
    function UpdateDailyStatusPageController($scope, $http, notificationService, $state, $stateParams, $mdDialog, $mdEditDialog, $timeout, $mdToast, $rootScope, $q, DailyStatusService, CommonService, AgencyService, filerService, $localStorage, HOST_URL, items) {


        var vm = this;
        vm.DailyStatusId = 0;
        var DailyStatusData = items.DailyStatusData;
        vm.DailyStatusId = DailyStatusData.ID;
        vm.DailyStatus = {};
        AssignData();
        function AssignData() {
            vm.PurposeName = DailyStatusData.DailyStatusCategory.Name;
            vm.DailyStatus.StartTime = DailyStatusData.StartTime == null ? null : (new Date(new Date().toDateString() + ' ' + DailyStatusData.StartTime));
            vm.DailyStatus.EndTime = DailyStatusData.EndTime == null ? null : (new Date(new Date().toDateString() + ' ' + DailyStatusData.EndTime));          
            vm.DailyStatus.Comments = DailyStatusData.Comments;
            vm.ParticipantName = vm.StudentInfoId = DailyStatusData.StudentInfo.FirstName + ' ' + DailyStatusData.StudentInfo.LastName;
        }


        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };
        $scope.mytime = new Date();

        $scope.hstep = 1;
        $scope.mstep = 15;



        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        $scope.update = function () {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.mytime = d;
        };

        $scope.changed = function () {
            $log.log('Time changed to: ' + $scope.mytime);
        };

        $scope.clear = function () {
            $scope.mytime = null;
        };

        vm.updateDailyStatus = updateDailyStatus;
        vm.cancelClick = cancelClick;
        function cancelClick() {
            $mdDialog.cancel();
        }
        function updateDailyStatus(model) {
            model.DailyStatusCategoryId = DailyStatusData.DailyStatusCategory.ID;
            model.StudentInfoId = DailyStatusData.StudentInfo.ID;
            model.AgencyInfoId = localStorage.agencyId;
            model.Date = items.Date;
            model.StartTime = model.StartTime == null ? null : (moment(model.StartTime).format('HH:mm:ss'));
            model.EndTime = model.EndTime == null ? null : (moment(model.EndTime).format('HH:mm:ss'));
            model.ID = DailyStatusData.ID;
            model.IsDeleted = DailyStatusData.IsDeleted;
            model.Date = DailyStatusData.Date;
            model.TimeZone = localStorage.TimeZone;
            var IsFromUpdate = true;
            $http.post(HOST_URL.url + '/api/DailyStatus/UpdateDailyStatus', model).then(function (response) {
                if (response.data.IsSuccess == true) {
                    vm.showProgressbar = false;
                    notificationService.displaymessage(response.data.ReturnMessage[0]);
                    $rootScope.$emit("GetDailyStatusDetail", { UpdateFromDate: items.FromDate, UpdateToDate: items.ToDate }, IsFromUpdate);
                    cancelClick();
                }
                else {
                    vm.showProgressbar = false;
                    notificationService.displaymessage('Unable to add at the moment. Please try again after some time.');
                }
            });
        }
    }
})();