(function () {
    'use strict';

    angular
        .module('app.dailystatus')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.dailystatus', {
                url: '/dailyStatus',
                templateUrl: 'app/DailyStatus/views/dailystatus.tmpl.html',
                // set the controller to load for this page
                controller: 'DailyStatusPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewdailystatus']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
             .state('triangular.parentdailystatus', {
                url: '/participants/dailyStatus',
                templateUrl: 'app/DailyStatus/views/dailystatus.tmpl.html',
                // set the controller to load for this page
                controller: 'DailyStatusPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column overlay-10'
                    },
                    permissions: {
                        only: ['viewdailystatus']
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"FAMILY",$window);
                    }
                }
            })
            .state('triangular.adddailystatus', {
                url: '/dailystatus/add',
                templateUrl: 'app/DailyStatus/views/adddailystatus.tmpl.html',
                // set the controller to load for this page
                controller: 'AdddailystatusController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
            
            .state('triangular.updatedailystatus', {
                url: '/dailystatus/update',
                templateUrl: 'app/DailyStatus/views/adddailystatus.tmpl.html',
                // set the controller to load for this page
                controller: 'UpdatedailystatusController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
              .state('triangular.dailyStatusDetails', {
                url: '/dailystatus/details',
                templateUrl: 'app/DailyStatus/views/dailyStatusDetail.tmpl.html',
                // set the controller to load for this page
                controller: 'DailyStatusDetailPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                 params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column '
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"AGENCY",$window);
                    }
                }
            })
             .state('triangular.parentportaldailyStatusDetails', {
                url: '/participants/dailystatus/details',
                templateUrl: 'app/DailyStatus/views/dailyStatusDetail.tmpl.html',
                // set the controller to load for this page
                controller: 'DailyStatusDetailPageController',
                controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                 params: {
                    obj: null
                },
                data: {
                    layout: {
                        contentClass: 'layout-column '
                    }
                },
                resolve:  {
                    data: function (apiService,$q, $http,$state,$location,$localStorage,$window) {
                        return apiService.checkLoginOnce($q, $http,$state,$location,$localStorage,"FAMILY",$window);
                    }
                }
            });
            
          

        triMenuProvider.addMenu({
            name: 'Daily Status',
            icon: 'fa fa-newspaper-o',
            id: 'step2',
            type: 'dropdown',
            priority: 11,
            permission: 'viewdailystatus',
            children: [{
                name: 'Daily Status',
                state: 'triangular.dailystatus',
                type: 'link',
                permission: 'viewAgencydailystatus'
            },
            {
                name: 'Daily Status',
                state: 'triangular.parentdailystatus',
                type: 'link',
                permission: 'viewParentdailystatus'
            }
                // , {
                //     name: 'Add dailystatus',
                //     state: 'triangular.adddailystatus',
                //     type: 'link'
                // }
                , 
                // {
                //     name: 'Schedule',
                //     state: 'triangular.schedule',
                //     type: 'link'
                // },
                // {
                //     name: 'Schedule',
                //     state: 'triangular.calendar',
                //     type: 'link'
                // },
             
                ]
        });
    }
})();
