(function () {
    'use strict';
    angular
        .module('app.staffsetting')
        .config(moduleConfig);
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider

            .state('triangular.staffsetting', {
                url: '/staffsetting',
                templateUrl: 'app/StaffSetting/Views/updatestaffsetting.tmpl.html',
                // set the controller to load for this page
                controller: 'UpdateStaffSettingController',
                 controllerAs: 'vm',
                // layout-column class added to make footer move to
                // bottom of the page on short pages
                data: {
                    layout: {
                        contentClass: 'layout-column'
                    },
                    permissions: {
                        only: ['viewStaffSetting']
                    }
                }
            });
        triMenuProvider.addMenu({
            name: 'Staff Settings',
            icon: 'fa fa-cog',
            type: 'link',
            permission: 'viewStaffSetting',
            state: 'triangular.staffsetting',
            priority: 1.1,

        });
    }
    /* @ngInject */
})();