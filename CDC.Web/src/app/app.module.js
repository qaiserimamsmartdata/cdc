(function () {
    'use strict';

    angular
        .module('app', [
            'ui.router',
            'triangular',
            'ngAnimate', 'ngCookies', 'ngSanitize', 'AngularPrint', 'ngMessages', 'ngMaterial', 'ngStorage',
            'googlechart', 'chart.js', 'linkify', 'ui.calendar', 'angularMoment', 'textAngular', 'uiGmapgoogle-maps', 'hljs', 'md.data.table', 'ngFileUpload', 'ui.mask',
            // 'seed-module',
            // uncomment above to activate the example seed module
            //'app.translate',
            // only need one language?  if you want to turn off translations
            // comment out or remove the 'app.translate', line above
            //'app.permission', By Qaiser Imam
            // dont need permissions?  if you want to turn off permissions
            // comment out or remove the 'app.permission', line above
            // also remove 'permission' from the first line of dependencies
            // https://github.com/Narzerus/angular-permission see here for why
            //'app.examples','app.students',
            'app.familyevent', 'app.staffevent', 'app.agencyevent', 'app.enrolledparticipants', 'app.class', 'app.staff', 'app.attendance', 'app.events', 'app.reports', 'app.store', 'app.skills', 'app.staffskill', 'app.staffschedule',
            'app.category', 'app.room', 'app.session', 'app.startup', 'app.authentication', 'angularFileUpload', 'app.Position', 'app.stafftimesheet',
            'app.paytype', 'app.family', 'app.mealSchedular', 'app.studentSchedule', 'app.messages', 'app.dropdownfamily', 'app.dropdownclass', 'app.dropdownstudent', 'app.students', 'app.permission', 'app.dropdownstaff', 'app.staffsetting', 'app.superadmin', 'app.dashboard', 'staffdashboard', 'superadmindashboard', 'app.todotask', 'app.chatbot', 'ui.bootstrap', 'app.parentportaldashboard', 'app.incident', 'app.dailystatus', 'app.dropdownparticipantattendance'
        ])
        // set a constant for the API we are connecting to
        .constant('API_CONFIG', {
            // 'url': 'http://triangular-api.oxygenna.com/'
        })
        .value('badge', 5)



        .constant('HOST_URL', {
            'url': 'http://localhost:51824/'
            ///MainServer
            //'url': 'http://http://172.10.200.15:3000/'
            // Staging
            //   'url': 'http://108.168.203.227:9006/'
            ////Qc Tester
            //  'url': 'https://108.168.203.227:9030/' 
            //beta Server
            // 'url': 'http://beta.pinwheelcare.com/'
            //beta parent Server
            // 'url': 'http://betaparent.pinwheelcare.com/'
            //beta main Server
            //  'url': 'http://132.148.130.30/plesk-site-preview/beta.pinwheelcare.com/132.148.130.30/'
        })

        .config(['$stateProvider', '$locationProvider', function ($stateProvider, $locationProvider) {
            $locationProvider.html5Mode(true);
        }]);
         angular.module('app').config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    });
})();

