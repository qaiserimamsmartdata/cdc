﻿using CDC.Entities.Association;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Masters
{
   public class ParticipantAssociationMasterData
    {
        public static void GetParticipantAssociationMasterData(CDCContext context, long agencyid)
        {
            context.ParticipantAssociationMasterSet.AddOrUpdate(
                new NewParticipantAssociationMaster { AssociationTypeName = "Sibling", AgencyId = agencyid, IsDeleted = false },
                new NewParticipantAssociationMaster { AssociationTypeName = "Cousin", AgencyId = agencyid, IsDeleted = false },
                new NewParticipantAssociationMaster { AssociationTypeName = "Family Friend", AgencyId = agencyid, IsDeleted = false },
                new NewParticipantAssociationMaster { AssociationTypeName = "Other Relation", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
