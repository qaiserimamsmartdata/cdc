﻿using CDC.Entities.Tools.Class;
using CDC.Entities.Tools.Student;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class FeesTypeData
    {
        public static void GetFeesTypeData (CDCContext context, long agencyid)
        {
            context.FeesTypeSet.AddOrUpdate(
                new FeeType { Name = "Monthly", AgencyId = agencyid,Days=30, IsDeleted = false },
                new FeeType { Name = "Weekly", AgencyId = agencyid,Days=7,IsDeleted = false },
                new FeeType { Name = "Biweekly", AgencyId = agencyid, Days = 14, IsDeleted = false },
                new FeeType { Name = "One Time", AgencyId = agencyid, Days = 1, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
