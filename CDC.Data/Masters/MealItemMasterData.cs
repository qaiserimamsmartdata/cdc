﻿using System.Data.Entity.Migrations;
using CDC.Entities;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Tools.MealServingType;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Cities Master Data
    /// </summary>
    public static class MealItemMasterData
    {
        public static void GetMealItemMasterData(CDCContext context, long agencyid)
        {
            context.MealItemMasterInfoSet.AddOrUpdate(
                new MealItemMaster { Name = "Milk", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Vegetable", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Fruit", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "yogurt", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Cheese", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Bread slice", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Meat", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Fish", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Egg", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Cooked dry beans", AgencyId = agencyid, IsDeleted = false },
                new MealItemMaster { Name = "Cooked dry peas", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}