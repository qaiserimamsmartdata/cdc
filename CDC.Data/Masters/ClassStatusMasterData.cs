﻿using CDC.Entities.Tools.Category;
using CDC.Entities.Tools.Class;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class ClassStatusMasterData
    {
        public static void GetClassStatus(CDCContext context, long agencyid)
        {
            context.ClassStatusSet.AddOrUpdate(
                new ClassStatus { Name = "Active", AgencyId = agencyid, IsDeleted = false },
                new ClassStatus { Name = "InActive", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
