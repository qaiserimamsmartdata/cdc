﻿using CDC.Entities.Tools.Student;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Masters
{
    public static class GeneralDroppedByData
    {
        public static void GetGeneralDroppedByData(CDCContext context, long agencyid)
        {
            context.GeneralDroppedBySet.AddOrUpdate(
                new GeneralDroppedBy { Name = "By Bus", AgencyId = agencyid, IsDeleted = false },
                new GeneralDroppedBy { Name = "By Cab", AgencyId = agencyid, IsDeleted = false },
                new GeneralDroppedBy { Name = "Self", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
