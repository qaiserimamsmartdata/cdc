﻿using CDC.Entities.Tools.Family;
using CDC.Entities.Tools.Staff;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Masters
{
    public static class DesignationMasterData
    {
        public static void GetDesignationMasterData(CDCContext context, long agencyid)
        {
            context.PositionSet.AddOrUpdate(
                new Position {Name = "Teacher", AgencyId = agencyid, IsDeleted = false },
                new Position { Name = "Instructor", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
