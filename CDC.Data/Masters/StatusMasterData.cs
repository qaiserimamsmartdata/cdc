﻿using CDC.Entities.Tools.Staff;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Masters
{
    public static class StatusMasterData
    {
        public static void GetStatusMasterData(CDCContext context)
        {
            context.StatusSet.AddOrUpdate(m => m.Name,
                new Status { Name = "Pending", AgencyId = 0, IsDeleted = false },
                new Status { Name = "OnLeave", AgencyId = 0, IsDeleted = false },
                new Status { Name = "Declined", AgencyId = 0, IsDeleted = false },
                new Status { Name = "Approved", AgencyId = 0, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
