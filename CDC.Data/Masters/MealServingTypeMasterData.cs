﻿using System.Data.Entity.Migrations;
using CDC.Entities;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Tools.MealServingType;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Cities Master Data
    /// </summary>
    public static class MealServingTypeMasterData
    {
        public static void GetMealServingTypeMasterData(CDCContext context)
        {
            context.MealServingTypeInfoSet.AddOrUpdate(m => m.Name,
                new MealServeType { Name = "cl", IsDeleted = false },
                new MealServeType { Name = "cups", IsDeleted = false },
                    new MealServeType { Name = "flozes", IsDeleted = false },
                new MealServeType { Name = "grams", IsDeleted = false },
                    new MealServeType { Name = "kgs", IsDeleted = false },
                    new MealServeType { Name = "lbs", IsDeleted = false },
                new MealServeType { Name = "litres", IsDeleted = false },
                    new MealServeType { Name = "mls", IsDeleted = false },
                new MealServeType { Name = "oz", IsDeleted = false },
                    new MealServeType { Name = "pints", IsDeleted = false },
                    new MealServeType { Name = "quats", IsDeleted = false },
                    new MealServeType { Name = "servings", IsDeleted = false },
                new MealServeType { Name = "tbsp", IsDeleted = false },
                    new MealServeType { Name = "tsps", IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}