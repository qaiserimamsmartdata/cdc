﻿using CDC.Entities.Tools.Class;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class SessionData
    {
        public static void GetSession(CDCContext context, long agencyid)
        {
            context.SessionSet.AddOrUpdate(
                new Session { Name = "Mar 2016 - Feb 2017", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
