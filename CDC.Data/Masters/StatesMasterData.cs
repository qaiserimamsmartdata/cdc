﻿using System;
using System.Data.Entity.Migrations;
using CDC.Entities;

namespace CDC.Data.Masters
{
    /// <summary>
    ///     The States Master Data
    /// </summary>
    internal static class StatesMasterData
    {
        public static void GetStates(CDCContext context)
        {
            context.StateSet.AddOrUpdate(m => m.StateName,
                new State { StateName = "Alaska", StateCode = "AK", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Arizona", StateCode = "AZ", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Arkansas", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "California", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Colorado", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Connecticut", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Delaware", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "District of Columbia", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Florida", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Georgia", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Hawaii", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Idaho", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Illinois", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Indiana", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Iowa", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Kansas", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Kentucky", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Louisiana", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Maine", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Maryland", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Massachusetts", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Michigan", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Minnesota", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Mississippi", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Missouri", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Montana", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Nebraska", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Nevada", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "New Hampshire", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "New Jersey", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "New Mexico", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "New York", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "North Carolina", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "North Dakota", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Ohio", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Oklahoma", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Oregon", StateCode = "1", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Pennsylvania", StateCode = "PS", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Puerto Rico", StateCode = "PR", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Rhode Island", StateCode = "RI", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "South Carolina", StateCode = "SC", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "South Dakota", StateCode = "SD", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Tennessee", StateCode = "TN", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Texas", StateCode = "TX", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Utah", StateCode = "UT", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Vermont", StateCode = "VT", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Virginia", StateCode = "VA", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Washington", StateCode = "WA", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "West Virginia", StateCode = "WV", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Wisconsin", StateCode = "WI", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow },
                new State { StateName = "Wyoming", StateCode = "WY", CountryId = 1, IsDeleted = false, CreatedOn = DateTime.UtcNow }
                //,
                //new State
                //{
                //    StateName = "Maharashtra ",
                //    StateCode = "3",
                //    CountryId = 2,
                //    IsDeleted = false,
                //    CreatedOn = DateTime.UtcNow
                //},
                //new State
                //{
                //    StateName = "Bihar ",
                //    StateCode = "4",
                //    CountryId = 2,
                //    IsDeleted = false,
                //    CreatedOn = DateTime.UtcNow
                //}

                );
            context.SaveChanges();
        }
    }
}