﻿using CDC.Entities.Family;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Masters
{
    public static class SecurityQuestionsMasterData
    {
        public static void GetQuestions(CDCContext context)
        {
            context.SecurityQuestionsSet.AddOrUpdate(m=>m.SecurityQuestion,
               
                new SecurityQuestions { SecurityQuestion = "What was your favorite place to visit as a child?", ID = 1, IsDeleted = false },
                new SecurityQuestions { SecurityQuestion = "Who is your favorite actor, musician, or artist?", ID = 2, IsDeleted = false },
                new SecurityQuestions { SecurityQuestion = "What is the name of your first school?", ID =3, IsDeleted = false },
                new SecurityQuestions { SecurityQuestion = "What is your favorite sports team?", ID = 4, IsDeleted = false },
                new SecurityQuestions { SecurityQuestion = "What was the make of your first car?", ID = 5, IsDeleted = false },
                new SecurityQuestions { SecurityQuestion = "What’s your Mother’s maiden name?", ID = 6, IsDeleted = false },
                new SecurityQuestions { SecurityQuestion = "Where is your favorite place to vacation?", ID = 7, IsDeleted = false },
                new SecurityQuestions { SecurityQuestion = "What Is your favorite book?", ID = 8, IsDeleted = false }

                //,
                //new Country {CountryName = "India", CountryCode = "2", IsDeleted = false, CreatedOn = DateTime.UtcNow}
                );
            context.SaveChanges();
        }
    }
}
