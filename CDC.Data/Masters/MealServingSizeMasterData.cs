﻿using System.Data.Entity.Migrations;
using CDC.Entities;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Tools.MealServingType;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Cities Master Data
    /// </summary>
    public static class MealServingSizeMasterData
    {
        public static void GetMealServingSizeMasterData(CDCContext context)
        {
            context.MealServingSizeInfoSet.AddOrUpdate(m => m.Name,
                new MealServeSize { Name = "0", IsDeleted = false },
               new MealServeSize { Name = "1", IsDeleted = false },
               new MealServeSize { Name = "2", IsDeleted = false },
                   new MealServeSize { Name = "3", IsDeleted = false },
               new MealServeSize { Name = "4", IsDeleted = false },
                   new MealServeSize { Name = "5", IsDeleted = false },
                   new MealServeSize { Name = "6", IsDeleted = false },
               new MealServeSize { Name = "7", IsDeleted = false },
                   new MealServeSize { Name = "8", IsDeleted = false },
               new MealServeSize { Name = "9", IsDeleted = false },
                   new MealServeSize { Name = "10", IsDeleted = false },
                   new MealServeSize { Name = "11", IsDeleted = false },
                   new MealServeSize { Name = "12", IsDeleted = false },
               new MealServeSize { Name = "13", IsDeleted = false },
                   new MealServeSize { Name = "14", IsDeleted = false },
                   new MealServeSize { Name = "15", IsDeleted = false },
               new MealServeSize { Name = "16", IsDeleted = false },
                   new MealServeSize { Name = "17", IsDeleted = false },
               new MealServeSize { Name = "18", IsDeleted = false },
                   new MealServeSize { Name = "19", IsDeleted = false },
                   new MealServeSize { Name = "20", IsDeleted = false }
               );
            context.SaveChanges();
        }
    }
}