﻿using CDC.Entities.Tools.Family;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class GradeLevelData
    {
        public static void GetGradeLevel(CDCContext context,long agencyid)
        {
            context.GradeLevelSet.AddOrUpdate(
                new GradeLevel { Name = "Pre-K", AgencyId= agencyid, IsDeleted=false },
                new GradeLevel { Name = "Kindergarten", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 1", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 2", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 3", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 4", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 5", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 6", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 7", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 8", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 9", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 10", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 11", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Grades 12", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Freshman", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Sophomore", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Junior", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Senior", AgencyId = agencyid, IsDeleted = false },
                new GradeLevel { Name = "Graduate School", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
