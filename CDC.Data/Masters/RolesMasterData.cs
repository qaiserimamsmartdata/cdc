﻿using System.Data.Entity.Migrations;
using CDC.Entities.User;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Countries Master Data 
    /// </summary>
    public static class RolesMasterData
    {
        public static void GetRoles(CDCContext context)
        {
            context.RolesSet.AddOrUpdate(m => m.RoleName,
                new Roles
                {
                    RoleName = "Agency",
                    IsDeleted = false,

                },
                new Roles { RoleName = "Staff", IsDeleted = false },
            new Roles { RoleName = "SuperAdmin", IsDeleted = false },
            new Roles { RoleName = "Family", IsDeleted = false }
                );

            context.SaveChanges();
        }
    }
}