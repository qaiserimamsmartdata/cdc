﻿using CDC.Entities.Tools.FoodManagement;
using CDC.Entities.Tools.Staff;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Masters
{
    public static class MealTypeMasterData
    {
        public static void GetMealMasterData(CDCContext context)
        {
            context.MealTypeSet.AddOrUpdate(m => m.Name,
                new MealType { Name = "Breakfast", AgencyId = 0, IsDeleted = false },
                new MealType { Name = "Lunch", AgencyId = 0, IsDeleted = false },
                new MealType { Name = "Snacks", AgencyId = 0, IsDeleted = false },
                new MealType { Name = "Dinner", AgencyId = 0, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
