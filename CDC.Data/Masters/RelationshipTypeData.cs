﻿using CDC.Entities.Tools.Family;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class RelationshipTypeData
    {
        public static void GetRelationshipType(CDCContext context, long agencyid)
        {
            context.RelationTypeSet.AddOrUpdate(
                new RelationType { Name = "Father", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Mother", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Aunt", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Uncle", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Brother", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Sister", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Step-Mother", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Step-Father", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Godmother", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Godfather", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Grandmother", AgencyId = agencyid, IsDeleted = false },
                new RelationType { Name = "Grandfather", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
