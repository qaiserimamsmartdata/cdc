﻿using System.Data.Entity.Migrations;
using CDC.Entities;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Tools.MealServingType;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Cities Master Data
    /// </summary>
    public static class MealServingQuantityMasterData
    {
        public static void GetMealServingQuantityMasterData(CDCContext context)
        {
            context.MealServingQuantityInfoSet.AddOrUpdate(m => m.Name,
                new MealServeQuantity { Name = "0", IsDeleted = false },
               new MealServeQuantity { Name = "1/10", IsDeleted = false },
               new MealServeQuantity { Name = "1/8", IsDeleted = false },
                   new MealServeQuantity { Name = "1/7", IsDeleted = false },
               new MealServeQuantity { Name = "1/6", IsDeleted = false },
                   new MealServeQuantity{ Name = "1/5", IsDeleted = false },
                   new MealServeQuantity{ Name = "1/4", IsDeleted = false },
               new MealServeQuantity{ Name = "1/3", IsDeleted = false },
                   new MealServeQuantity{ Name = "1/2", IsDeleted = false },
               new MealServeQuantity{ Name = "2/3", IsDeleted = false },
                   new MealServeQuantity{ Name = "3/4", IsDeleted = false },
                   new MealServeQuantity{ Name = "7/8", IsDeleted = false }
               );
            context.SaveChanges();
        }
    }
}