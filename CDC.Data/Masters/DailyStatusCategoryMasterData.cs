﻿using CDC.Entities.DailyStatus;
using CDC.Entities.Tools.Staff;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Masters
{
    public static class DailyStatusCategoryMasterData
    {
        public static void GetDailyStatusCategoryMasterData(CDCContext context)
        {
            context.DailyStatusCategorySet.AddOrUpdate(m => m.Name,
                new DailyStatusCategory { Name = "Eat", AgencyId = 0, IsDeleted = false },
                new DailyStatusCategory { Name = "Sleep", AgencyId = 0, IsDeleted = false },
                new DailyStatusCategory { Name = "Toilet", AgencyId = 0, IsDeleted = false },
                   new DailyStatusCategory { Name = "General", AgencyId = 0, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
