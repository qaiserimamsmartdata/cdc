﻿using System.Data.Entity.Migrations;
using CDC.Entities;
using CDC.Entities.Pricing;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Cities Master Data
    /// </summary>
    public static class PricingPlanMasterData
    {
        public static void GetPricingPlans(CDCContext context)
        {
            context.PricingPlanSet.AddOrUpdate(m => m.Name,
                new PricingPlan{Name = "Free Plan", Price = 0,MinNumberOfParticipants=0,MaxNumberOfParticipants=5, IsDeleted = false,TimeClockUsers=1},
                new PricingPlan { Name = "Basic Plan", Price = 29, MinNumberOfParticipants = 6, MaxNumberOfParticipants = 29, IsDeleted = false, TimeClockUsers =  2},
                new PricingPlan { Name = "Bronze Plan", Price = 49, MinNumberOfParticipants = 30, MaxNumberOfParticipants = 69, IsDeleted = false, TimeClockUsers = 4 },
                new PricingPlan { Name = "Silver Plan", Price = 69, MinNumberOfParticipants = 70, MaxNumberOfParticipants = 99, IsDeleted = false, TimeClockUsers = 6 },
                new PricingPlan { Name = "Gold Plan", Price = 99, MinNumberOfParticipants = 100, MaxNumberOfParticipants = 149, IsDeleted = false, TimeClockUsers = 8},
                new PricingPlan { Name = "Platinum Plan", Price = 119, MinNumberOfParticipants = 150, MaxNumberOfParticipants = 199, IsDeleted = false, TimeClockUsers = 10},
                new PricingPlan { Name = "Max Plan", Price = 189, MinNumberOfParticipants = 300, MaxNumberOfParticipants = 499, IsDeleted = false, TimeClockUsers = 12 },
                new PricingPlan { Name = "Super Plan", Price = 500, MinNumberOfParticipants = 500, MaxNumberOfParticipants = 1000, IsDeleted = false, TimeClockUsers = 14 }
                );
            context.SaveChanges();
        }
    }
}