﻿using CDC.Entities.Skills;
using System.Data.Entity.Migrations;


namespace CDC.Data.Masters
{
    public static class SkillMasterData
    {
        public static void GetSkillMaster(CDCContext context,long agencyId)
        {
            context.SkillMasterSet.AddOrUpdate( new SkillMaster {Skill="Ballet",AgencyId=agencyId, IsDeleted = false });
            context.SkillMasterSet.AddOrUpdate(new SkillMaster { Skill = "Hip Hop", AgencyId = agencyId, IsDeleted = false });
            context.SkillMasterSet.AddOrUpdate(new SkillMaster { Skill = "Jazz", AgencyId = agencyId, IsDeleted = false });
            context.SkillMasterSet.AddOrUpdate(new SkillMaster { Skill = "Pointe", AgencyId = agencyId, IsDeleted = false });
            context.SkillMasterSet.AddOrUpdate(new SkillMaster { Skill = "Tap", AgencyId = agencyId, IsDeleted = false });
            context.SkillMasterSet.AddOrUpdate(new SkillMaster { Skill = "Tumbling", AgencyId = agencyId, IsDeleted = false });
            context.SaveChanges();
        }
    }
}
