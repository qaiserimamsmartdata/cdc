﻿using System.Data.Entity.Migrations;
using CDC.Entities;
using CDC.Entities.Pricing;
using CDC.Entities.TimeClockUsers;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Cities Master Data
    /// </summary>
    public static class TimeClockUsersPlanMasterData
    {
        public static void GetTimeClockUsersPlans(CDCContext context)
        {
            context.TimeClockUsersPlanSet.AddOrUpdate(m => m.Name,
                new TimeClockUsersPlan { Name = "Basic", Price = 10, MaxNumberOfTimeClockUsers= 1, IsDeleted = false },
                new TimeClockUsersPlan { Name = "Standard", Price = 15, MaxNumberOfTimeClockUsers = 2, IsDeleted = false },
                new TimeClockUsersPlan { Name = "Pro", Price = 20, MaxNumberOfTimeClockUsers = 4, IsDeleted = false },
                new TimeClockUsersPlan { Name = "Enterprise", Price = 25, MaxNumberOfTimeClockUsers = 6, IsDeleted = false },
                new TimeClockUsersPlan { Name = "Max", Price = 30, MaxNumberOfTimeClockUsers = 8, IsDeleted = false },
                new TimeClockUsersPlan { Name = "Super", Price = 35, MaxNumberOfTimeClockUsers = 10, IsDeleted = false },
                new TimeClockUsersPlan { Name = "Advance", Price = 40, MaxNumberOfTimeClockUsers = 12, IsDeleted = false },
                new TimeClockUsersPlan { Name = "Believer", Price = 50, MaxNumberOfTimeClockUsers = 20, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}