﻿using System;
using System.Data.Entity.Migrations;
using CDC.Entities;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Countries Master Data 
    /// </summary>
    public static class CountriesMasterData
    {
        public static void GetCountries(CDCContext context)
        {
            context.CountrySet.AddOrUpdate(m => m.CountryName,
                new Country
                {
                    CountryName = "United States",
                    CountryCode = "1",
                    IsDeleted = false,
                    CreatedOn = DateTime.UtcNow
                }
                //,
                //new Country {CountryName = "India", CountryCode = "2", IsDeleted = false, CreatedOn = DateTime.UtcNow}
                );
            context.SaveChanges();
        }
    }
}