﻿using CDC.Entities.Tools.Category;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class CategoryData
    {
        public static void GetCategory(CDCContext context, long agencyid)
        {
            context.CategorySet.AddOrUpdate(
                new Category { Name = "Infant/New Born", AgencyId = agencyid, IsDeleted = false },
                new Category { Name = "Toddler", AgencyId = agencyid, IsDeleted = false },
                new Category { Name = "Elementary School Age", AgencyId = agencyid, IsDeleted = false },
                new Category { Name = "Middle School Age", AgencyId = agencyid, IsDeleted = false },
                new Category { Name = "High School Age", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
