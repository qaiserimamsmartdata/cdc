﻿using CDC.Entities.ToDoTask;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Masters
{
    public static class PriorityMasterData
    {
        public static void GetPriorityMasterData(CDCContext context,long agencyid)
        {
            context.PrioritySet.AddOrUpdate(
                 //new Priority { ID = 0, Name = "High", AgencyId = agencyid, IsDeleted = false },
                 //new Priority { ID = 1, Name = "Medium", AgencyId = agencyid, IsDeleted = false },
                 //new Priority { ID = 2, Name = "Low", AgencyId = agencyid, IsDeleted = false }
                 new Priority { Name = "High", AgencyId = agencyid, IsDeleted = false },
                new Priority { Name = "Medium", AgencyId = agencyid, IsDeleted = false },
                new Priority {  Name = "Low", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
