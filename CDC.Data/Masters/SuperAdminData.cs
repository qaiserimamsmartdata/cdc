﻿using CDC.Entities.Tools.Category;
using CDC.Entities.User;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class SuperAdminData
    {
        public static void GetSuperAdminData(CDCContext context)
        {
            context.UsersSet.AddOrUpdate(m => m.EmailId,
                new Users { EmailId = "pinwheel.trinity@gmail.com", Password = "l5fpr8r9qBvkBoxtnWr2YA==", IsDeleted = false,RoleId=3 }
                );
            context.SaveChanges();
        }
    }
}
