﻿using CDC.Entities.Tools.Family;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class MembershipTypeData
    {
        public static void GetMembershipType(CDCContext context, long agencyid)
        {
            context.MembershipTypeSet.AddOrUpdate(
                new MembershipType { Name = "Monthly", AgencyId = agencyid, IsDeleted = false },
                new MembershipType { Name = "Weekly", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
