﻿using CDC.Entities.Tools.Class;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class RoomData
    {
        public static void GetRoom(CDCContext context, long agencyid,long agencyLocationId)
        {
            context.RoomSet.AddOrUpdate(
                new Room { Name = "Room 1", AgencyId = agencyid,AgencyLocationInfoId= agencyLocationId,IsDeleted = false },
                new Room { Name = "Room 2", AgencyId = agencyid, AgencyLocationInfoId = agencyLocationId,IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
