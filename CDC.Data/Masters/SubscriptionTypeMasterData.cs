﻿using System.Data.Entity.Migrations;
using CDC.Entities;
using CDC.Entities.AgencyRegistration;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Cities Master Data
    /// </summary>
    public static class SubscriptionTypeMasterData
    {
        public static void GetSubscriptionType(CDCContext context)
        {
            context.AgencySubscriptionTypeSet.AddOrUpdate(m => m.TypeName,
                new AgencySubscriptionType { TypeName = "AgencySubscription", IsDeleted = false},
                new AgencySubscriptionType { TypeName = "TimeClockUserSubscription",  IsDeleted = false}
                );
            context.SaveChanges();
        }
    }
}