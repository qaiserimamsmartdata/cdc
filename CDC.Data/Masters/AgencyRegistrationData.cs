﻿using CDC.Entities.AgencyRegistration;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class AgencyRegistrationData
    {
        public static void GetAgencies(CDCContext context)
        {
            context.AgencyRegistrationInfoSet.AddOrUpdate(
                new AgencyRegistrationInfo { AgencyName = "Rockford Agency", OwnerFirstName = "Yatish",OwnerLastName= "Raj",  ContactPersonFirstName = "Yatish",ContactPersonLastName= "Raj",Email = "yatishraaz@yopmail.com", Password= "9RONBjDLswv1fBHu1HggzA==",IsExistingAccount= false,IsDeleted=false }
                );
            context.SaveChanges();
        }
    }
}
