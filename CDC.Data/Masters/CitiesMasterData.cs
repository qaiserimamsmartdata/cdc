﻿using System.Data.Entity.Migrations;
using CDC.Entities;

namespace CDC.Data.Masters
{
    /// <summary>
    /// The Cities Master Data
    /// </summary>
    public static class CitiesMasterData
    {
        public static void GetCities(CDCContext context)
        {
            context.CitySet.AddOrUpdate(m => m.name,
                new City {name = "Sacramento", StateId = 1, IsDeleted = false},
                new City {name = "Tallahassee", StateId = 2, IsDeleted = false},
                new City {name = "Nagpur", StateId = 3, IsDeleted = false},
                new City {name = "Jamshedpur", StateId = 4, IsDeleted = false}
                );
            context.SaveChanges();
        }
    }
}