﻿using CDC.Entities.Tools.Family;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class InfoSourceData
    {
        public static void GetInfoSource(CDCContext context, long agencyid)
        {
            context.InfoSourceSet.AddOrUpdate(
                new InfoSource { Name = "Walk-in", AgencyId = agencyid, IsDeleted = false },
                new InfoSource { Name = "Magazine/Newspaper Ad", AgencyId = agencyid, IsDeleted = false },
                new InfoSource { Name = "Word of Mouth", AgencyId = agencyid, IsDeleted = false },
                new InfoSource { Name = "Google Search", AgencyId = agencyid, IsDeleted = false },
                new InfoSource { Name = "School", AgencyId = agencyid, IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
