﻿using CDC.Entities.SMSCarrier;
using CDC.Entities.Tools.Category;
using System.Data.Entity.Migrations;

namespace CDC.Data.Masters
{
    public static class SMSCarrierData
    {
        public static void GetSMSCarrier(CDCContext context)
        {
            context.SMSCarrierSet.AddOrUpdate(m => m.Carrier,
                new SMSCarrier { Carrier= "Alltel", CarrierAddress = "message.alltel.com", IsDeleted = false },
                new SMSCarrier { Carrier = "AT&T", CarrierAddress = "txt.att.net", IsDeleted = false },
                new SMSCarrier { Carrier = "Boost", CarrierAddress = "myboostmobile.com", IsDeleted = false },
                new SMSCarrier { Carrier = "Cingular", CarrierAddress = "cingularme.com", IsDeleted = false },
                new SMSCarrier { Carrier = "Cricket", CarrierAddress = "mms.mycricket.com", IsDeleted = false },
                new SMSCarrier { Carrier = "N/A", CarrierAddress = "tmomail.net", IsDeleted = false },
                new SMSCarrier { Carrier = "Sprint", CarrierAddress = "messaging.sprintpcs.com", IsDeleted = false },
                new SMSCarrier { Carrier = "Nextel", CarrierAddress = "messaging.nextel.com", IsDeleted = false },
                new SMSCarrier { Carrier = "Suncom", CarrierAddress = "tms.suncom.com", IsDeleted = false },
                new SMSCarrier { Carrier = "T-Mobile", CarrierAddress = "tmomail.net", IsDeleted = false },
                new SMSCarrier { Carrier = "Verizon", CarrierAddress = "vtext.com", IsDeleted = false },
                new SMSCarrier { Carrier = "Virgin", CarrierAddress = "vmobl.com", IsDeleted = false },
                new SMSCarrier { Carrier = "U.S. Cellular", CarrierAddress = "email.uscc.net", IsDeleted = false },
                new SMSCarrier { Carrier = "Metro PCS", CarrierAddress = "mymetropcs.com", IsDeleted = false }
                );
            context.SaveChanges();
        }
    }
}
