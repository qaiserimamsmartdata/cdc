﻿using System.Data.Entity;
using CDC.Data.Masters;

namespace CDC.Data
{
    /// <summary>
    /// The Child day care Master Data Class
    /// </summary>
    /// <seealso>
    ///     <cref>System.Data.Entity.DropCreateDatabaseAlways{CDC.Data.CDCContext}</cref>
    /// </seealso>
    public class CDCMasterData : DropCreateDatabaseAlways<CDCContext>
    {
        protected override void Seed(CDCContext context)
        {
            CountriesMasterData.GetCountries(context);
            StatesMasterData.GetStates(context);
            CitiesMasterData.GetCities(context);
        }
    }
}