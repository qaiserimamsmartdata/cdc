﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDC.Entities.ToDoTask;


namespace CDC.Data.Configurations.ToDoTask
{
    public class ToDoConfiguration : EntityBaseConfiguration<ToDo>
    {
        public ToDoConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
