﻿using CDC.Entities.ToDoTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.ToDoTask
{
    public class PriorityConfiguration : EntityBaseConfiguration<Priority>
    {
        public PriorityConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
