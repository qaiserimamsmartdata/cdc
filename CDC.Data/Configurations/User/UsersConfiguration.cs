﻿namespace CDC.Data.Configurations.User
{
    /// <summary>
    /// The users Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Users}" />
    public class UsersConfiguration : EntityBaseConfiguration<Entities.User.Users>
    {
        public UsersConfiguration()
        {
            Property(u => u.EmailId).IsRequired();
            Property(u => u.Password).IsRequired();
            Property(u => u.RoleId).IsRequired();
        }
    }
}
