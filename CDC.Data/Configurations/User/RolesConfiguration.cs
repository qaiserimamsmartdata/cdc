﻿namespace CDC.Data.Configurations.User
{
    /// <summary>
    /// The roles Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Roles}" />
    public class RolesConfiguration : EntityBaseConfiguration<Entities.User.Roles>
    {
        public RolesConfiguration()
        {
            Property(u => u.RoleName).IsRequired();
        }
    }
}
