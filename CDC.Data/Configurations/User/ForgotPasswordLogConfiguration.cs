﻿using CDC.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.User
{
    public class ForgotPasswordLogConfiguration : EntityBaseConfiguration<ForgotPasswordLog>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ForgotPasswordLogConfiguration"/> class.
        /// </summary>
        public ForgotPasswordLogConfiguration()
        {
            Property(x => x.ID).HasColumnName(@"ID").IsRequired().HasColumnType("bigint");
            Property(x => x.UserId).HasColumnName(@"UserId").IsRequired().HasColumnType("bigint");
            Property(x => x.Guid).HasColumnName(@"Guid").IsRequired().HasColumnType("uniqueidentifier");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
