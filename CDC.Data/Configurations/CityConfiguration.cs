﻿using CDC.Entities;

namespace CDC.Data.Configurations
{
    /// <summary>
    /// The City Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{City}" />
    public class CityConfiguration : EntityBaseConfiguration<City>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CityConfiguration"/> class.
        /// </summary>
        public CityConfiguration()
        {
            Property(x => x.ID).HasColumnName(@"CityId").IsRequired().HasColumnType("bigint");
            Property(x => x.StateId).HasColumnName(@"StateId").IsOptional().HasColumnType("bigint");
            Property(x => x.name).HasColumnName(@"name").IsRequired().HasColumnType("nvarchar").HasMaxLength(500);
        }
    }
}