﻿using CDC.Entities.Common;

namespace CDC.Data.Configurations.Common
{
    public class ContactInfoMapConfiguration : EntityBaseConfiguration<ContactInfoMap>
    {
        public ContactInfoMapConfiguration()
        {
            Property(u => u.FamilyId).IsOptional().HasColumnType("bigint");
            Property(u => u.StaffId).IsOptional().HasColumnType("bigint");
            Property(u => u.ContactId).IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
