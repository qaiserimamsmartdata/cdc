﻿using CDC.Entities.Common;

namespace CDC.Data.Configurations.Common
{
    public class ImageLogConfiguration : EntityBaseConfiguration<ImageLog>
    {
        public ImageLogConfiguration()
        {
            Property(u => u.ImageName).IsOptional().HasColumnType("varchar").HasMaxLength(200);
            Property(u => u.ImageUrl).IsOptional().HasColumnType("varchar").HasMaxLength(200);
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
