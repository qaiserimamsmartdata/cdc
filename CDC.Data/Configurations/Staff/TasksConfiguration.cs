﻿using CDC.Entities.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Staff
{
    public class TasksConfiguration : EntityBaseConfiguration<Tasks>
    {
        public TasksConfiguration()
        {
            Property(u => u.Title).IsRequired();
        }
    }
}
