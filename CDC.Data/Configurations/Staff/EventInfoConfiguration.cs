﻿using CDC.Entities.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Staff
{
    public class EventInfoConfiguration : EntityBaseConfiguration<EventInfo>
    {

        public EventInfoConfiguration()
        {
            Property(u => u.Title).IsRequired();
        }
    }
}
