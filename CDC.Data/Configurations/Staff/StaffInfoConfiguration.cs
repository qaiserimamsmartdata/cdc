﻿namespace CDC.Data.Configurations.Staff
{
    public class StaffInfoConfiguration : EntityBaseConfiguration<Entities.Staff.StaffInfo>
    {
        public StaffInfoConfiguration()
        {
            Property(u => u.FirstName).IsRequired().HasMaxLength(100);
            Property(u => u.LastName).IsRequired().HasMaxLength(100);
            Property(u => u.Gender).IsRequired();
            Property(u => u.PositionId);
            Property(u => u.Email).IsRequired().HasMaxLength(100);
            Property(u => u.Address).HasMaxLength(200);
            Property(u => u.CountryId).IsOptional();
            Property(u => u.StateId).IsRequired();
            Property(u => u.CityName).IsRequired();
            Property(u => u.PostalCode).HasMaxLength(6);
            //Mobile
            Property(u => u.PhoneNumber).IsRequired().HasMaxLength(15);
            Property(u => u.HomePhone).IsOptional().HasMaxLength(15);
            Property(u => u.Apartment).IsOptional().HasMaxLength(50);
        }
    }
}
