﻿namespace CDC.Data.Configurations.Staff
{
    public class LeaveInfoConfiguration : EntityBaseConfiguration<Entities.Staff.LeaveInfo>
    {
        public LeaveInfoConfiguration()
        {
            Property(u => u.StaffId).IsRequired().HasColumnType("bigint");
            Property(u => u.StatusId).IsRequired().HasColumnType("bigint");
            Property(u => u.LeaveFromDate).IsRequired().HasColumnType("datetime2");
            Property(u => u.LeaveToDate).IsOptional().HasColumnType("datetime2");
            Property(u => u.Remarks).HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.ActionDate).IsOptional().HasColumnType("datetime2");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
