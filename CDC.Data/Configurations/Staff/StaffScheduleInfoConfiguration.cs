﻿namespace CDC.Data.Configurations.Staff
{
    public class StaffScheduleInfoConfiguration : EntityBaseConfiguration<Entities.Staff.StaffScheduleInfo>
    {
        public StaffScheduleInfoConfiguration()
        {
            Property(u => u.Title).IsRequired();
            Property(u => u.StaffId).IsRequired();
            Property(u => u.ClassId).IsRequired();
            //Property(u => u.RoomId).IsRequired();
            Property(u => u.Date).IsRequired();
            Property(u => u.Time).IsRequired();
            Property(u => u.LessonPlanId).IsRequired();
        }
    }
}
