﻿using CDC.Entities.Tools.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Tools.Student
{
    class FeeTypeConfiguration : EntityBaseConfiguration<FeeType>
    {
        public FeeTypeConfiguration()
        {
            Property(u => u.Name).IsRequired();
            Property(u => u.Days).IsRequired();
        }
    }
}
