﻿using CDC.Entities.Tools.Student;

namespace CDC.Data.Configurations.Tools.Student
{
    public class DropReasonConfiguration : EntityBaseConfiguration<DropReason>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DropReasonConfiguration"/> class.
        /// </summary>
        public DropReasonConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
