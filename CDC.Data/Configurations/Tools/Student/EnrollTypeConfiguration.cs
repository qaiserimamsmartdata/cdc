﻿using CDC.Entities.Tools.Student;

namespace CDC.Data.Configurations.Tools.Student
{
    class EnrollTypeConfiguration : EntityBaseConfiguration<EnrollType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnrollTypeConfiguration"/> class.
        /// </summary>
        public EnrollTypeConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
