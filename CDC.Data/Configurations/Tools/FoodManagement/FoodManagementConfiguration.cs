﻿using CDC.Entities.Tools.FoodManagement;
using CDC.Entities.Tools.ManageHoliday;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Tools.FoodManagement
{
    public class FoodManagementConfiguration : EntityBaseConfiguration<FoodManagementMaster>
    {
        public FoodManagementConfiguration()
        {
            Property(u => u.MealItemId).IsRequired();
            //Property(u => u.MealTypeId).IsRequired();
        }
    }
}
