﻿namespace CDC.Data.Configurations.Tools.Category
{
    /// <summary>
    /// The Category Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Category}" />
    public class CategoryConfiguration : EntityBaseConfiguration<Entities.Tools.Category.Category>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryConfiguration"/> class.
        /// </summary>
        public CategoryConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}