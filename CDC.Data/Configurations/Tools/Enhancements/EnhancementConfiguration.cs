﻿using CDC.Entities.Tools.Enhancements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Tools.Enhancements
{
    public class EnhancementConfiguration : EntityBaseConfiguration<Enhancement>
    {
        public EnhancementConfiguration()
        {
            Property(u => u.Title).IsRequired();
        }
    }
}
