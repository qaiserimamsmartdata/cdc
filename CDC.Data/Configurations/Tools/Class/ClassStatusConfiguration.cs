﻿using CDC.Entities.Tools.Class;

namespace CDC.Data.Configurations.Tools.Class
{
    /// <summary>
    /// The class status Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{ClassStatus}" />
    public class ClassStatusConfiguration: EntityBaseConfiguration<ClassStatus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClassStatusConfiguration"/> class.
        /// </summary>
        public ClassStatusConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
