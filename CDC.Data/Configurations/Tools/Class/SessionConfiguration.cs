﻿using CDC.Entities.Tools.Class;

namespace CDC.Data.Configurations.Tools.Class
{
    /// <summary>
    /// The Session Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Session}" />
    public class SessionConfiguration : EntityBaseConfiguration<Session>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SessionConfiguration"/> class.
        /// </summary>
        public SessionConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}