﻿using CDC.Entities.Tools.Class;

namespace CDC.Data.Configurations.Tools.Class
{
    /// <summary>
    /// The Room Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Room}" />
    public class RoomConfiguration : EntityBaseConfiguration<Room>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoomConfiguration"/> class.
        /// </summary>
        public RoomConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}