﻿using CDC.Entities.Tools.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Tools.Class
{
    /// <summary>
    /// The Status Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Status}" />
    public class StatusConfugration: EntityBaseConfiguration<Status>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusConfiguration"/> class.
        /// </summary>
        public StatusConfugration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
