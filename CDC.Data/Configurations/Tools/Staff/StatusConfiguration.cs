﻿using CDC.Entities.Tools.Staff;

namespace CDC.Data.Configurations.Tools.Staff
{
    /// <summary>
    /// The Designation Configuration staff
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Status}" />
    public class StatusConfiguration : EntityBaseConfiguration<Status>
    {
        public StatusConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
