﻿using CDC.Entities.Tools.Staff;

namespace CDC.Data.Configurations.Tools.Staff
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso>
    ///     <cref>CDC.Data.Configurations.EntityBaseConfiguration{CDC.Entities.Tools.Staff.PayType}</cref>
    /// </seealso>
    public class PayTypeConfiguration : EntityBaseConfiguration<PayType>
    {
        public PayTypeConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
