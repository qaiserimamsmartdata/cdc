﻿using CDC.Entities.Tools.Staff;

namespace CDC.Data.Configurations.Tools.Staff
{
    /// <summary>
    /// The Designation Configuration staff
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Designation}" />
    public class PositionConfiguration : EntityBaseConfiguration<Position>
    {
        public PositionConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
