﻿using CDC.Entities.Tools.Staff;

namespace CDC.Data.Configurations.Tools.Staff
{
    public class LeaveTypeConfiguration : EntityBaseConfiguration<LeaveType>
    {
        public LeaveTypeConfiguration()
        {
            Property(u => u.Name).IsRequired();
        }
    }
}
