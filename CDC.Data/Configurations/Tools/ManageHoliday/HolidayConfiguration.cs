﻿using CDC.Entities.Tools.ManageHoliday;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Tools.ManageHoliday
{
    public class HolidayConfiguration: EntityBaseConfiguration<Holiday>
    {
        public HolidayConfiguration()
        {
            Property(u => u.Title).IsRequired();
        }
    }
}
