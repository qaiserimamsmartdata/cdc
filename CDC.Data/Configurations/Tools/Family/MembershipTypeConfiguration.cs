﻿using CDC.Entities.Tools.Family;

namespace CDC.Data.Configurations.Tools.Family
{
    public class MembershipTypeConfiguration : EntityBaseConfiguration<MembershipType>
    {
        public MembershipTypeConfiguration()
        {
            Property(u => u.Name).IsRequired().HasMaxLength(500).HasColumnType("nvarchar");
        }
    }
}
