﻿using CDC.Entities.Tools.Family;

namespace CDC.Data.Configurations.Tools.Family
{
    public class InfoSourceConfiguration : EntityBaseConfiguration<InfoSource>
    {
        public InfoSourceConfiguration()
        {
            Property(u => u.Name).IsRequired().HasMaxLength(500).HasColumnType("nvarchar");
        }
    }
}
