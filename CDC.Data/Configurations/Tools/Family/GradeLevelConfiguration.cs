﻿using CDC.Entities.Tools.Family;

namespace CDC.Data.Configurations.Tools.Family
{
    public class GradeLevelConfiguration : EntityBaseConfiguration<GradeLevel>
    {
        public GradeLevelConfiguration()
        {
            Property(u => u.Name).IsRequired().HasMaxLength(500).HasColumnType("nvarchar");
        }
    }
}
