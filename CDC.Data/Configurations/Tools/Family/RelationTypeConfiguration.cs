﻿using CDC.Entities.Tools.Family;

namespace CDC.Data.Configurations.Tools.Family
{
    public class RelationTypeConfiguration : EntityBaseConfiguration<RelationType>
    {
        public RelationTypeConfiguration()
        {
            Property(u => u.Name).IsRequired().HasMaxLength(500).HasColumnType("nvarchar");
        }
    }
}
