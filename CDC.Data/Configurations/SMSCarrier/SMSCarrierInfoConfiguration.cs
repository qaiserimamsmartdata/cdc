﻿namespace CDC.Data.Configurations.SMSCarrier
{
    public class SMSCarrierInfoConfiguration : EntityBaseConfiguration<Entities.SMSCarrier.SMSCarrier>
    {
        public SMSCarrierInfoConfiguration()
        {
            Property(u => u.Carrier).HasMaxLength(100);
            Property(u => u.CarrierAddress).HasMaxLength(200);
        }
    }
}
