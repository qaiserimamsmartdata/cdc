﻿using CDC.Entities.Schedule;

namespace CDC.Data.Configurations.Schedule
{
    /// <summary>
    /// The Student Schedule Configuration Class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{StudentSchedule}" />
    public class StudentScheduleConfiguration : EntityBaseConfiguration<StudentSchedule>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentScheduleConfiguration"/> class.
        /// </summary>
        public StudentScheduleConfiguration()
        {
            Property(u => u.StudentId);
            Property(u => u.CategoryId);
            Property(u => u.ClassId);
            Property(u => u.RoomId);
            Property(u => u.StartDate).IsRequired();
            Property(u => u.EndDate).IsRequired();
            
        }
    }
}