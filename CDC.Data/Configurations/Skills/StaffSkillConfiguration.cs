﻿using CDC.Entities.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Skills
{
    public class StaffSkillConfiguration :EntityBaseConfiguration<StaffSkill>
    {
        public StaffSkillConfiguration()
        {
            Property(u => u.Description).IsRequired();
        }
    }
}
