﻿namespace CDC.Data.Configurations.Skills
{
    public class SkillConfiguration : EntityBaseConfiguration<Entities.Skills.Skills>
    {
        public SkillConfiguration()
        {
            Property(u => u.CategoryId).IsRequired();
            Property(u => u.Skill).IsRequired().HasMaxLength(100);
            Property(u => u.DaysRequired).IsRequired();
            Property(u => u.ClassesRequired).IsRequired();
        }
    }
}