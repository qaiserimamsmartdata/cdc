﻿using CDC.Entities.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDC.Data.Configurations.Skills
{
    public class SkillMasterConfiguration : EntityBaseConfiguration<SkillMaster>
    {
        public SkillMasterConfiguration()
        {
            Property(u => u.Skill).IsRequired().HasMaxLength(100);
        }
    }
}
