﻿using CDC.Entities.Attendance;

namespace CDC.Data.Configurations.Attendance
{
    public class StudentAttendanceConfiguration : EntityBaseConfiguration<StudentAttendanceInfo>
    {
        public StudentAttendanceConfiguration()
        {
            Property(u => u.InTime).HasPrecision(0).IsOptional();
            Property(u => u.OutTime).HasPrecision(0).IsOptional();
            Property(u => u.AttendanceDate).HasColumnType("datetime2");
            Property(u => u.DropedById).IsOptional().HasColumnType("bigint");
            Property(u => u.DropedByOtherId).IsOptional().HasColumnType("bigint");
            Property(u => u.PickupById).IsOptional().HasColumnType("bigint");
            Property(u => u.PickupByOtherName).IsOptional().HasMaxLength(50).HasColumnType("nvarchar");
            Property(x => x.OnLeave).HasColumnName(@"OnLeave").IsRequired().HasColumnType("bit");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.DisableOnLeave).HasColumnName(@"DisableOnLeave").IsOptional().HasColumnType("bit");
        }
    }
}
