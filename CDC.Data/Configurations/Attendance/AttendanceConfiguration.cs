﻿namespace CDC.Data.Configurations.Attendance
{
    public class AttendanceConfiguration : EntityBaseConfiguration<Entities.Attendance.AttendanceInfo>
    {
        public AttendanceConfiguration()
        {
            Property(u => u.InTime).HasPrecision(0).IsOptional();
            Property(u => u.OutTime).HasPrecision(0).IsOptional();
            Property(u => u.Comment).HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.AttendanceDate).HasColumnType("datetime2");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
