﻿using CDC.Entities;

namespace CDC.Data.Configurations
{
    /// <summary>
    /// The State Configuration Class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{State}" />
    public class StateConfiguration : EntityBaseConfiguration<State>
    {
        public StateConfiguration()
        {
            Property(x => x.ID).HasColumnName(@"StateId").IsRequired().HasColumnType("bigint");
            Property(x => x.CountryId).HasColumnName(@"CountryId").IsRequired().HasColumnType("bigint");
            Property(x => x.StateName)
                .HasColumnName(@"StateName")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(100);
        }
    }
}