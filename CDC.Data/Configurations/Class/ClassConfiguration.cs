﻿using CDC.Entities.Class;

namespace CDC.Data.Configurations.Class
{
    /// <summary>
    /// The Class information Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{ClassInfo}" />
    public class ClassConfiguration : EntityBaseConfiguration<ClassInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClassConfiguration"/> class.
        /// </summary>
        public ClassConfiguration()
        {
            Property(u => u.ClassName).IsRequired().HasMaxLength(100);
            Property(u => u.CategoryId).IsRequired();
            Property(u => u.ClassStatusId).IsRequired();
            Property(u => u.EnrollCapacity).IsRequired();
            Property(u => u.SessionId).IsOptional();
            Property(u => u.RoomId).IsOptional();
            Property(u => u.MinAgeFrom).IsRequired();
            Property(u => u.MinAgeTo).IsRequired();
            Property(u => u.MaxAgeFrom).IsRequired();
            Property(u => u.MaxAgeTo).IsRequired();
            Property(u => u.AgeCutOffDate).IsOptional();
            Property(u => u.RegistrationStartDate).IsOptional();
            Property(u => u.ClassStartDate).IsOptional();
            Property(u => u.ClassEndDate).IsOptional();
            Property(u => u.OnGoing).IsOptional();
        }
    }
}