﻿using CDC.Entities.Class;

namespace CDC.Data.Configurations.Class
{
    public class WaitListConfiguration : EntityBaseConfiguration<WaitListMap>
    {
        public WaitListConfiguration()
        {
            Property(u => u.ClassId).IsRequired().HasColumnType("bigint");
            Property(u => u.StudentId).IsRequired().HasColumnType("bigint");
            Property(u => u.Notes).IsOptional().HasMaxLength(500).HasColumnType("nvarchar");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
