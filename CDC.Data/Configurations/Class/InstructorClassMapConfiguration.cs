﻿using CDC.Entities.Class;

namespace CDC.Data.Configurations.Class
{
    public class InstructorClassMapConfiguration : EntityBaseConfiguration<InstructorClassMap>
    {
        public InstructorClassMapConfiguration()
        {
            Property(x => x.ClassId).IsRequired();
            Property(x => x.StaffId).IsRequired();
        }
    }
}
