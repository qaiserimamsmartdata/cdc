﻿using CDC.Entities.Class;

namespace CDC.Data.Configurations.Class
{
   
    public class LessonPlanConfiguration : EntityBaseConfiguration<LessonPlan>
    {
        public LessonPlanConfiguration()
        {
            Property(u => u.Name).IsRequired();
            Property(u => u.ClassId).IsOptional();
        }
    }
}