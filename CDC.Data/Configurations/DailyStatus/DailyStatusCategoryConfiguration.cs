﻿using CDC.Entities.Class;
using CDC.Entities.DailyStatus;

namespace CDC.Data.Configurations.DailyStatus
{
    /// <summary>
    /// The Class information Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{DailyStatusCategory}" />
    public class DailyStatusCategoryConfiguration : EntityBaseConfiguration<DailyStatusCategory>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClassConfiguration"/> class.
        /// </summary>
        public DailyStatusCategoryConfiguration()
        {
            Property(u => u.Name).IsRequired().HasMaxLength(100);
            Property(u => u.AgencyId).IsOptional();
        }
    }
}