﻿using System.Data.Entity.ModelConfiguration;
using CDC.Entities;

namespace CDC.Data.Configurations
{
    /// <summary>
    /// The Entity Base Configuration Class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="System.Data.Entity.ModelConfiguration.EntityTypeConfiguration{T}" />
    public class EntityBaseConfiguration<T> : EntityTypeConfiguration<T> where T : class, IEntityBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityBaseConfiguration{T}"/> class.
        /// </summary>
        protected EntityBaseConfiguration()
        {
            HasKey(e => e.ID);
            Property(x => x.IsDeleted).HasColumnName(@"IsDeleted").IsOptional().HasColumnType("bit");
        }
    }
}