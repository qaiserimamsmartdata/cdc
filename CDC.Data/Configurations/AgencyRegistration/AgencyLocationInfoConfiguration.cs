﻿using CDC.Entities.AgencyRegistration;

namespace CDC.Data.Configurations.AgencyRegistration
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso>
    ///     <cref>CDC.Data.Configurations.EntityBaseConfiguration{CDC.Entities.AgencyRegistration.AgencyLocationInfo}</cref>
    /// </seealso>
    public class AgencyLocationInfoConfiguration : EntityBaseConfiguration<AgencyLocationInfo>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="AgencyRegistrationInfoConfiguration"/> class.
        /// </summary>
        public AgencyLocationInfoConfiguration()
        {
            Property(u => u.LocationName).IsRequired().HasMaxLength(100);
            Property(u => u.ContactLastName).IsOptional().HasMaxLength(100);
            Property(u => u.ContactFirstName).IsOptional().HasMaxLength(100);
            Property(u => u.Description).IsOptional().HasMaxLength(200);
            Property(u => u.LocationPhoneNumber).IsOptional().HasMaxLength(15);
            Property(u => u.LocationEmailId).IsOptional().HasMaxLength(200);
            Property(u => u.Address).IsOptional().HasMaxLength(200);
            Property(u => u.CountryId).IsRequired();
            Property(u => u.StateId).IsRequired();
            Property(u => u.CityName).IsOptional().HasMaxLength(100);
            Property(u => u.PostalCode).IsOptional().HasMaxLength(6);            
        }
    }
}