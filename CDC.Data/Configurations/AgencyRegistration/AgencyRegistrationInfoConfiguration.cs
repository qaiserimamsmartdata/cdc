﻿using CDC.Entities.AgencyRegistration;

namespace CDC.Data.Configurations.AgencyRegistration
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{AgencyInfo}" />
    public class AgencyRegistrationInfoConfiguration : EntityBaseConfiguration<AgencyRegistrationInfo>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="AgencyRegistrationInfoConfiguration"/> class.
        /// </summary>
        public AgencyRegistrationInfoConfiguration()
        {
            //Property(u => u.AgencyName).IsRequired().HasMaxLength(100);
            //Property(u => u.OwnerFirstName).IsRequired().HasMaxLength(100);
            //Property(u => u.OwnerLastName).IsRequired().HasMaxLength(100);
            //Property(u => u.ContactPersonFirstName).IsRequired().HasMaxLength(100);
            //Property(u => u.ContactPersonLastName).IsRequired().HasMaxLength(100);
            //Property(u => u.Email).HasMaxLength(100);
            //Property(u => u.Password).IsRequired().HasMaxLength(50);
            Property(u => u.TrialEnd).IsOptional();
            Property(u => u.TrialStart).IsOptional();
            //Property(u => u.TotalTimeClockUsers).IsOptional();
        }
    }
}