﻿using CDC.Entities.AgencyRegistration;

namespace CDC.Data.Configurations.AgencyRegistration
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso>
    ///     <cref>CDC.Data.Configurations.EntityBaseConfiguration{CDC.Entities.AgencyRegistration.AgencyContactInfo}</cref>
    /// </seealso>
    public class AgencyContactInfoConfiguration : EntityBaseConfiguration<AgencyContactInfo>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="AgencyRegistrationInfoConfiguration"/> class.
        /// </summary>
        public AgencyContactInfoConfiguration()
        {
            Property(u => u.Address).IsRequired().HasMaxLength(200);
            Property(u => u.CountryId).IsRequired();
            Property(u => u.StateId).IsRequired();
            Property(u => u.CityName).HasMaxLength(100);
            Property(u => u.PostalCode).HasMaxLength(6);
            Property(u => u.PhoneNumber).HasMaxLength(15);
        }
    }
}