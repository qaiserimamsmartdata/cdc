﻿using CDC.Entities.AgencyRegistration;

namespace CDC.Data.Configurations.AgencyRegistration
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso>
    ///     <cref>CDC.Data.Configurations.EntityBaseConfiguration{CDC.Entities.AgencyRegistration.AgencyContactInfo}</cref>
    /// </seealso>
    public class AgencyTimeZoneConfiguration : EntityBaseConfiguration<AgencyTimeZone>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="AgencyRegistrationInfoConfiguration"/> class.
        /// </summary>
        public AgencyTimeZoneConfiguration()
        {
            Property(u => u.TimeZone).IsRequired();
            
        }
    }
}