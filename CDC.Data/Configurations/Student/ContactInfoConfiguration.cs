﻿using CDC.Entities.Students;

namespace CDC.Data.Configurations.Student
{
    /// <summary>
    /// The Contact Information Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{ContactInfo}" />
    public class ContactInfoConfiguration : EntityBaseConfiguration<ContactInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContactInfoConfiguration"/> class.
        /// </summary>
        public ContactInfoConfiguration()
        {
            Property(u => u.Address).IsRequired().HasMaxLength(200);
            Property(u => u.CountryId).IsRequired();
            Property(u => u.StateId).IsRequired();
            Property(u => u.City).IsRequired();
            Property(u => u.PostalCode).IsRequired().HasMaxLength(6);
        }
    }
}