﻿using CDC.Entities.Students;

namespace CDC.Data.Configurations.Student
{
    /// <summary>
    /// The Class Enrollment information configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{EnrollmentInfo}" />
    public class EnrollmentInfoConfiguration : EntityBaseConfiguration<EnrollmentInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnrollmentInfoConfiguration"/> class.
        /// </summary>
        public EnrollmentInfoConfiguration()
        {
            Property(u => u.ClassId).IsRequired();
            Property(u => u.RoomId).IsRequired();
            Property(u => u.StartDate).IsRequired();
            Property(u => u.EndDate).IsRequired();
            Property(u => u.AdditionalInfo).HasMaxLength(200);
        }
    }
}