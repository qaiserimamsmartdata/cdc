﻿using CDC.Entities.Students;

namespace CDC.Data.Configurations.Student
{
    /// <summary>
    /// The Guardian info configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{GuardianInfo}" />
    public class GuardianInfoConfiguration : EntityBaseConfiguration<GuardianInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GuardianInfoConfiguration"/> class.
        /// </summary>
        public GuardianInfoConfiguration()
        {
            Property(u => u.LastName).IsRequired().HasMaxLength(100);
            Property(u => u.FirstName).IsRequired().HasMaxLength(100);
            Property(u => u.RelationId).IsRequired();
            Property(u => u.Mobile).IsRequired().HasMaxLength(15);
            Property(u => u.EmailId).IsRequired().HasMaxLength(100);
        }
    }
}