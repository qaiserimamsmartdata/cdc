﻿using CDC.Entities.Students;

namespace CDC.Data.Configurations.Student
{
    /// <summary>
    /// The Parent information configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{ParentInfo}" />
    public class ParentInfoConfiguration : EntityBaseConfiguration<ParentInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParentInfoConfiguration"/> class.
        /// </summary>
        public ParentInfoConfiguration()
        {
            Property(u => u.FatherName).IsRequired().HasMaxLength(100);
            Property(u => u.MotherName).IsRequired().HasMaxLength(100);
            Property(u => u.Mobile).IsRequired().HasMaxLength(15);
            Property(u => u.EmailId).IsRequired().HasMaxLength(100);
        }
    }
}