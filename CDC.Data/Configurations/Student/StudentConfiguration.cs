﻿namespace CDC.Data.Configurations.Student
{
    /// <summary>
    /// The Student Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Student}" />
    public class StudentConfiguration : EntityBaseConfiguration<Entities.Students.Student>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentConfiguration"/> class.
        /// </summary>
        public StudentConfiguration()
        {
            Property(u => u.FirstName).IsRequired().HasMaxLength(100);
            Property(u => u.LastName).IsRequired().HasMaxLength(100);
            Property(u => u.Gender).IsRequired();
        }
    }
}
