﻿using CDC.Entities.Family;

namespace CDC.Data.Configurations.Family
{
    public class StudentDetailConfiguration : EntityBaseConfiguration<StudentDetail>
    {
        public StudentDetailConfiguration()
        {
            Property(u => u.PhoneNumber).IsOptional().HasMaxLength(15).HasColumnType("varchar");
            Property(u => u.Email).IsOptional().HasMaxLength(100).HasColumnType("varchar");
            Property(u => u.SchoolName).IsOptional().HasMaxLength(100).HasColumnType("varchar");
            Property(u => u.Transportation).IsOptional().HasMaxLength(200).HasColumnType("varchar");
            Property(u => u.Disabilities).IsOptional().HasMaxLength(200).HasColumnType("varchar");
            Property(u => u.Allergies).IsOptional().HasMaxLength(200).HasColumnType("varchar");
            Property(u => u.Medications).IsOptional().HasMaxLength(200).HasColumnType("varchar");
            Property(u => u.Allergies).IsOptional().HasMaxLength(200).HasColumnType("varchar");
            Property(u => u.PrimaryDoctor).IsOptional().HasMaxLength(200).HasColumnType("varchar");
            Property(u => u.GradeLevelId).IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
