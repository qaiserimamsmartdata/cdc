﻿using CDC.Entities.Family;

namespace CDC.Data.Configurations.Family
{
    public class ContactInfoConfiguration : EntityBaseConfiguration<ContactInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContactInfoConfiguration"/> class.
        /// </summary>
        public ContactInfoConfiguration()
        {            
            Property(u => u.Address).IsOptional().HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.CountryId).IsOptional().HasColumnType("bigint");
            Property(u => u.StateId).IsOptional().HasColumnType("bigint");
            Property(u => u.CityName).IsOptional().HasMaxLength(100).HasColumnType("nvarchar");
            Property(u => u.PostalCode).IsOptional().HasMaxLength(6).HasColumnType("nvarchar");
            Property(u => u.EmergencyFirstName).IsOptional().HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.EmergencyLastName).IsOptional().HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.EmergencyPhoneNumber).IsOptional().HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.RelationshipToParticipantId).IsOptional().HasColumnType("bigint");
            Property(u => u.InsuranceCarrier).IsOptional().HasMaxLength(200).HasColumnType("nvarchar");
            Property(u => u.InsurancePolicyNumber).IsOptional().HasMaxLength(50).HasColumnType("nvarchar");
            Property(u => u.PreferredHospitalName).IsOptional().HasMaxLength(50).HasColumnType("nvarchar");
            Property(u => u.FamilyDoctorName).IsOptional().HasMaxLength(100).HasColumnType("nvarchar");
            Property(u => u.FamilyDoctorPhoneNumber).IsOptional().HasMaxLength(15).HasColumnType("nvarchar");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}