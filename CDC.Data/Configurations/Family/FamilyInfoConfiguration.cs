﻿using CDC.Entities.Family;

namespace CDC.Data.Configurations.Family
{
    public class FamilyInfoConfiguration : EntityBaseConfiguration<FamilyInfo>
    {
        public FamilyInfoConfiguration()
        {
            Property(u => u.AgencyId).IsRequired().HasColumnType("bigint");
            Property(u => u.FamilyName).IsRequired().HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.SecurityKey).IsRequired().HasMaxLength(50).HasColumnType("nvarchar");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
