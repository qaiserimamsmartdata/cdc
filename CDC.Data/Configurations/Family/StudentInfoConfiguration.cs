﻿using CDC.Entities.Family;

namespace CDC.Data.Configurations.Family
{
    public class StudentInfoConfiguration : EntityBaseConfiguration<StudentInfo>
    {
        public StudentInfoConfiguration()
        {
            Property(u => u.FirstName).IsRequired().HasMaxLength(100).HasColumnType("nvarchar");
            Property(u => u.LastName).IsRequired().HasMaxLength(100).HasColumnType("nvarchar");
            Property(u => u.Gender).IsRequired().HasColumnType("int");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
