﻿using CDC.Entities.Family;

namespace CDC.Data.Configurations.Family
{
    public class ParentInfoConfiguration : EntityBaseConfiguration<ParentInfo>
    {
        public ParentInfoConfiguration()
        {
            Property(u => u.FamilyId).IsRequired().HasColumnType("bigint");
            Property(u => u.FirstName).IsOptional().HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.LastName).IsOptional().HasMaxLength(500).HasColumnType("nvarchar");
            Property(u => u.Mobile).IsOptional().HasMaxLength(15).HasColumnType("nvarchar");
            Property(u => u.HomePhone).IsOptional().HasMaxLength(15).HasColumnType("nvarchar");
            Property(u => u.EmailId).IsOptional().HasMaxLength(100).HasColumnType("nvarchar");
            Property(u => u.RelationTypeId).IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
            Property(u => u.SMSCarrierId).IsOptional().HasColumnType("bigint");
            Property(u => u.IsEventMailReceived).IsOptional();
            Property(u => u.IsParticipantAttendanceMailReceived).IsOptional();
        }
    }
}