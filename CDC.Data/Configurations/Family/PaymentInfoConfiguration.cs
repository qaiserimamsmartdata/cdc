﻿using CDC.Entities.Family;

namespace CDC.Data.Configurations.Family
{
    public class PaymentInfoConfiguration : EntityBaseConfiguration<PaymentInfo>
    {
        public PaymentInfoConfiguration()
        {
            Property(u => u.FamilyId).IsOptional().HasColumnType("bigint");
            Property(u => u.CardName).IsOptional().HasMaxLength(100).HasColumnType("nvarchar");         
            Property(u => u.CardType).IsOptional().HasColumnType("int");
            Property(u => u.CardNumber).IsOptional().HasColumnType("bigint");
            Property(x => x.CardExpiryMonth).IsOptional().HasColumnType("int");
            Property(x => x.CardExpiryYear).IsOptional().HasColumnType("int");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}