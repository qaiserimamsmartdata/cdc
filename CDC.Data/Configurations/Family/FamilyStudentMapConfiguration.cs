﻿using CDC.Entities.Family;

namespace CDC.Data.Configurations.Family
{
    public class FamilyStudentMapConfiguration : EntityBaseConfiguration<FamilyStudentMap>
    {
        public FamilyStudentMapConfiguration()
        {
            Property(u => u.FamilyId).IsRequired().HasColumnType("bigint");
            Property(u => u.StudentId).IsRequired().HasColumnType("bigint");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
