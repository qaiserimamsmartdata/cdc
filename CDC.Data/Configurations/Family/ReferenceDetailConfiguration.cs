﻿using CDC.Entities.Family;

namespace CDC.Data.Configurations.Family
{
    public class ReferenceDetailConfiguration : EntityBaseConfiguration<ReferenceDetail>
    {
        public ReferenceDetailConfiguration()
        {
            Property(u => u.FamilyId).IsOptional().HasColumnType("bigint");
            Property(u => u.InfoSourceId).IsRequired().HasColumnType("bigint");
            Property(u => u.ReferedBy).IsOptional().HasMaxLength(500).HasColumnType("nvarchar");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").IsOptional().HasColumnType("bigint");
            Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").IsOptional().HasColumnType("datetime2");
            Property(x => x.ModifiedDate).HasColumnName(@"ModifiedDate").IsOptional().HasColumnType("datetime2");
        }
    }
}
