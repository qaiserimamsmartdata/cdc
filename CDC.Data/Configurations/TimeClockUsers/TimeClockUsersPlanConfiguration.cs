﻿using CDC.Entities;
using CDC.Entities.Pricing;
using CDC.Entities.TimeClockUsers;

namespace CDC.Data.Configurations.TimeClockUsers
{
    /// <summary>
    /// The Pricing Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{TimeClockUsers}" /> 
    public class TimeClockUsersPlanConfiguration : EntityBaseConfiguration<TimeClockUsersPlan>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PricingConfiguration"/> class.
        /// </summary>
        public TimeClockUsersPlanConfiguration()
        {
            Property(x => x.Name).IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.Price).IsOptional().HasColumnType("decimal").HasPrecision(8, 2);
            Property(x => x.MaxNumberOfTimeClockUsers).IsOptional().HasColumnType("int");
        }
    }
}