﻿using CDC.Entities.Agency;

namespace CDC.Data.Configurations.Agency
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{AgencyInfo}" />
    public class AgencyInfoConfiguration : EntityBaseConfiguration<AgencyInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AgencyInfoConfiguration"/> class.
        /// </summary>
        public AgencyInfoConfiguration()
        {
            Property(u => u.AgencyName).IsRequired().HasMaxLength(100);
            Property(u => u.ContactPerson).IsRequired().HasMaxLength(100);
            Property(u => u.Email).IsRequired().HasMaxLength(100); 
            Property(u => u.Address).HasMaxLength(200);
            Property(u => u.CountryId).IsRequired();
            Property(u => u.StateId).IsRequired();
            Property(u => u.CityId).IsRequired();
            Property(u => u.PostalCode).HasMaxLength(6);
            Property(u => u.PhoneNumber).IsRequired().HasMaxLength(15);            
        }
    }
}