﻿using CDC.Entities;
using CDC.Entities.Pricing;

namespace CDC.Data.Configurations.Pricing
{
    /// <summary>
    /// The Pricing Configuration class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{PricingPlan}" />
    public class PricingPlanConfiguration : EntityBaseConfiguration<PricingPlan>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PricingConfiguration"/> class.
        /// </summary>
        public PricingPlanConfiguration()
        {
            Property(x => x.Name).IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.Price).IsOptional().HasColumnType("decimal").HasPrecision(8, 2);
            Property(x => x.MinNumberOfParticipants).IsOptional().HasColumnType("int");
            Property(x => x.MaxNumberOfParticipants).IsOptional().HasColumnType("int");
            Property(x => x.TimeClockUsers).IsOptional().HasColumnType("int");
        }
    }
}