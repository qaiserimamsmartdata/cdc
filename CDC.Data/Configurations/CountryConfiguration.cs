﻿using CDC.Entities;

namespace CDC.Data.Configurations
{
    /// <summary>
    /// The Country Configuration Class
    /// </summary>
    /// <seealso cref="CDC.Data.Configurations.EntityBaseConfiguration{Country}" />
    public class CountryConfiguration : EntityBaseConfiguration<Country>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CountryConfiguration"/> class.
        /// </summary>
        public CountryConfiguration()
        {
            Property(x => x.ID).HasColumnName(@"CountryId").IsRequired().HasColumnType("bigint");
            Property(x => x.CountryName)
                .HasColumnName(@"CountryName")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(500);
            Property(x => x.CountryCode)
                .HasColumnName(@"CountryCode")
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(50);
        }
    }
}