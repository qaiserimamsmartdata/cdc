﻿namespace CDC.Data.Infrastructure
{
    /// <summary>
    /// The DB Factory Class
    /// </summary>
    /// <seealso cref="CDC.Data.Infrastructure.Disposable" />
    /// <seealso cref="CDC.Data.Infrastructure.IDbFactory" />
    public class DbFactory : Disposable, IDbFactory
    {
        private CDCContext dbContext;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns></returns>
        public CDCContext Init()
        {
            return dbContext ?? (dbContext = new CDCContext());
        }

        protected override void DisposeCore()
        {
            dbContext?.Dispose();
        }
    }
}