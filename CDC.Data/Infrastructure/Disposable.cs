﻿using System;

namespace CDC.Data.Infrastructure
{
    /// <summary>
    /// The Disposable Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public class Disposable : IDisposable
    {
        private bool isDisposed;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="Disposable"/> class.
        /// </summary>
        ~Disposable()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                DisposeCore();
            }

            isDisposed = true;
        }

        // Overrides this to dispose custom objects
        protected virtual void DisposeCore()
        {
        }
    }
}