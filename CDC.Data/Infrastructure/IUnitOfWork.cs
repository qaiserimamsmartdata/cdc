﻿namespace CDC.Data.Infrastructure
{
    /// <summary>
    /// 
    /// </summary>The UnitOfWork interface
    public interface IUnitOfWork
    {
        void Commit();
    }
}