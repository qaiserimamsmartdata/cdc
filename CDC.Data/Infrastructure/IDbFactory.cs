﻿using System;

namespace CDC.Data.Infrastructure
{
    /// <summary>
    /// The db factory interface
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IDbFactory : IDisposable
    {
        CDCContext Init();
    }
}