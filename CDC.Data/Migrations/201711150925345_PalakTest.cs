namespace CDC.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PalakTest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IncidentParentParticipantMapping",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        IncidentId = c.Long(),
                        ParentInfoId = c.Long(),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.tblParentInfo", t => t.ParentInfoId)
                .ForeignKey("dbo.Incident", t => t.IncidentId)
                .Index(t => t.IncidentId)
                .Index(t => t.ParentInfoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IncidentParentParticipantMapping", "IncidentId", "dbo.Incident");
            DropForeignKey("dbo.IncidentParentParticipantMapping", "ParentInfoId", "dbo.tblParentInfo");
            DropIndex("dbo.IncidentParentParticipantMapping", new[] { "ParentInfoId" });
            DropIndex("dbo.IncidentParentParticipantMapping", new[] { "IncidentId" });
            DropTable("dbo.IncidentParentParticipantMapping");
        }
    }
}
