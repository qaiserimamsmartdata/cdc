namespace CDC.Data.Migrations
{
    using Masters;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CDC.Data.CDCContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            
        }

        protected override void Seed(CDC.Data.CDCContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            CountriesMasterData.GetCountries(context);
            StatesMasterData.GetStates(context);
            //CitiesMasterData.GetCities(context);
            RolesMasterData.GetRoles(context);
            StatusMasterData.GetStatusMasterData(context);
           // PricingPlanMasterData.GetPricingPlans(context);
            SMSCarrierData.GetSMSCarrier(context);
           // TimeClockUsersPlanMasterData.GetTimeClockUsersPlans(context);
            SuperAdminData.GetSuperAdminData(context);
            DailyStatusCategoryMasterData.GetDailyStatusCategoryMasterData(context);
            SecurityQuestionsMasterData.GetQuestions(context);
            MealTypeMasterData.GetMealMasterData(context);
            SubscriptionTypeMasterData.GetSubscriptionType(context);
            MealServingSizeMasterData.GetMealServingSizeMasterData(context);
            MealServingQuantityMasterData.GetMealServingQuantityMasterData(context);
            MealServingTypeMasterData.GetMealServingTypeMasterData(context);
        }
    }
}
