namespace CDC.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Betatest : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.IncidentOtherParticipant", newName: "IncidentOtherParticipantMapping");
            RenameTable(name: "dbo.IncidentParticipant", newName: "IncidentParticipantMapping");
            DropForeignKey("dbo.AgencyRegistrationInfo", "PricingPlanId", "dbo.PricingPlan");
            DropIndex("dbo.AgencyRegistrationInfo", new[] { "PricingPlanId" });
            CreateTable(
                "dbo.IncidentParentParticipantMapping",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        IncidentId = c.Long(),
                        ParentInfoId = c.Long(),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.tblParentInfo", t => t.ParentInfoId)
                .ForeignKey("dbo.Incident", t => t.IncidentId)
                .Index(t => t.IncidentId)
                .Index(t => t.ParentInfoId);
            
            AddColumn("dbo.tblParentInfo", "TimeZone", c => c.String());
            AddColumn("dbo.Tasks", "IsRecurring", c => c.Boolean());
            AddColumn("dbo.Tasks", "EndsOn", c => c.String());
            AlterColumn("dbo.AgencyRegistrationInfo", "PricingPlanId", c => c.Long());
            AlterColumn("dbo.AgencyLocationInfo", "LocationPhoneNumber", c => c.String(maxLength: 15));
            AlterColumn("dbo.AgencyLocationInfo", "Address", c => c.String(maxLength: 200));
            CreateIndex("dbo.AgencyRegistrationInfo", "PricingPlanId");
            AddForeignKey("dbo.AgencyRegistrationInfo", "PricingPlanId", "dbo.PricingPlan", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AgencyRegistrationInfo", "PricingPlanId", "dbo.PricingPlan");
            DropForeignKey("dbo.IncidentParentParticipantMapping", "IncidentId", "dbo.Incident");
            DropForeignKey("dbo.IncidentParentParticipantMapping", "ParentInfoId", "dbo.tblParentInfo");
            DropIndex("dbo.IncidentParentParticipantMapping", new[] { "ParentInfoId" });
            DropIndex("dbo.IncidentParentParticipantMapping", new[] { "IncidentId" });
            DropIndex("dbo.AgencyRegistrationInfo", new[] { "PricingPlanId" });
            AlterColumn("dbo.AgencyLocationInfo", "Address", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.AgencyLocationInfo", "LocationPhoneNumber", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.AgencyRegistrationInfo", "PricingPlanId", c => c.Long(nullable: false));
            DropColumn("dbo.Tasks", "EndsOn");
            DropColumn("dbo.Tasks", "IsRecurring");
            DropColumn("dbo.tblParentInfo", "TimeZone");
            DropTable("dbo.IncidentParentParticipantMapping");
            CreateIndex("dbo.AgencyRegistrationInfo", "PricingPlanId");
            AddForeignKey("dbo.AgencyRegistrationInfo", "PricingPlanId", "dbo.PricingPlan", "ID", cascadeDelete: true);
            RenameTable(name: "dbo.IncidentParticipantMapping", newName: "IncidentParticipant");
            RenameTable(name: "dbo.IncidentOtherParticipantMapping", newName: "IncidentOtherParticipant");
        }
    }
}
