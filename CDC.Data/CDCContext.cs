﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using CDC.Data.Configurations;
using CDC.Data.Configurations.Attendance;
using CDC.Data.Configurations.Class;
using CDC.Data.Configurations.Common;
using CDC.Data.Configurations.Family;
using CDC.Data.Configurations.Schedule;
using CDC.Data.Configurations.Skills;
using CDC.Data.Configurations.Staff;
using CDC.Data.Configurations.Student;
using CDC.Data.Configurations.Tools.Category;
using CDC.Data.Configurations.Tools.Class;
using CDC.Data.Configurations.Tools.Family;
using CDC.Data.Configurations.Tools.Staff;
using CDC.Data.Configurations.Tools.Student;
using CDC.Data.Configurations.User;
using CDC.Data.Models;
using CDC.Entities;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Attendance;
using CDC.Entities.Class;
using CDC.Entities.Common;
using CDC.Entities.Family;
using CDC.Entities.Schedule;
using CDC.Entities.Skills;
using CDC.Entities.Staff;
using CDC.Entities.Students;
using CDC.Entities.Tools.Category;
using CDC.Entities.Tools.Class;
using CDC.Entities.Tools.Family;
using CDC.Entities.Tools.Staff;
using CDC.Entities.Tools.Student;
using CDC.Entities.User;
using Microsoft.AspNet.Identity.EntityFramework;
using CDC.Data.Configurations.Pricing;
using CDC.Entities.Pricing;
using CDC.Data.Configurations.AgencyRegistration;
using CDC.Data.Configurations.SMSCarrier;
using CDC.Entities.SMSCarrier;
using CDC.Data.Configurations.TimeClockUsers;
using CDC.Entities.TimeClockUsers;
using CDC.Entities.ToDoTask;
using CDC.Data.Configurations.ToDoTask;
using CDC.Entities.Tools.ManageHoliday;
using CDC.Data.Configurations.Tools.ManageHoliday;
using CDC.Entities.Tools.Enhancements;
using CDC.Entities.Messages;
using CDC.Entities.ParticipantIncident;
using CDC.Data.Configurations.DailyStatus;
using CDC.Entities.DailyStatus;
using CDC.Data.Configurations.Tools.FoodManagement;
using CDC.Entities.Tools.FoodManagement;
using CDC.Entities.MealSchedular;
using CDC.Entities.Tools.MealServingType;
using CDC.Entities.PaymentHistory;
using CDC.Entities.PaymentHistoryForPlans;
using CDC.Entities.Tools.Attendance;
using CDC.Entities.ExceptionLogs;
using CDC.Entities.Association;
using CDC.Entities.NewParent;
using CDC.Entities.NewParticipant;

//using CDC.Data.Configurations.Tools.Student;
//using CDC.Entities.Tools.Student;

namespace CDC.Data
{
    /// <summary>
    ///     The on-line child day care context
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    public class CDCContext : IdentityDbContext<ApplicationUser>
    {
        public CDCContext()
            : base("DefaultConnection", false)
        {
        }

        #region Static Method

        public static CDCContext Create()
        {
            return new CDCContext();
        }

        #endregion

        public virtual void Commit()
        {
            SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new AgencyLocationInfoConfiguration());
            modelBuilder.Configurations.Add(new CountryConfiguration());
            modelBuilder.Configurations.Add(new StateConfiguration());
            modelBuilder.Configurations.Add(new CityConfiguration());
            //modelBuilder.Configurations.Add(new StudentConfiguration());
            modelBuilder.Configurations.Add(new ParentInfoConfiguration());
            modelBuilder.Configurations.Add(new PaymentInfoConfiguration());
            modelBuilder.Configurations.Add(new ContactInfoConfiguration());
            //modelBuilder.Configurations.Add(new GuardianInfoConfiguration());
            modelBuilder.Configurations.Add(new EnrollmentInfoConfiguration());
            modelBuilder.Configurations.Add(new SkillConfiguration());
            modelBuilder.Configurations.Add(new ClassConfiguration());
            modelBuilder.Configurations.Add(new StudentScheduleConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new RoomConfiguration());
            modelBuilder.Configurations.Add(new SessionConfiguration());
            modelBuilder.Configurations.Add(new LessonPlanConfiguration());
            //modelBuilder.Configurations.Add(new AgencyInfoConfiguration());
            modelBuilder.Configurations.Add(new StaffInfoConfiguration());
            modelBuilder.Configurations.Add(new PositionConfiguration());
            modelBuilder.Configurations.Add(new StatusConfiguration());
            modelBuilder.Configurations.Add(new LeaveInfoConfiguration());
            modelBuilder.Configurations.Add(new LeaveTypeConfiguration());
            modelBuilder.Configurations.Add(new StaffScheduleInfoConfiguration());
            modelBuilder.Configurations.Add(new AttendanceConfiguration());
            modelBuilder.Configurations.Add(new StudentAttendanceConfiguration());
            modelBuilder.Configurations.Add(new PayTypeConfiguration());
            modelBuilder.Configurations.Add(new InstructorClassMapConfiguration());
            modelBuilder.Configurations.Add(new WaitListConfiguration());
            modelBuilder.Configurations.Add(new FamilyInfoConfiguration());
            modelBuilder.Configurations.Add(new ReferenceDetailConfiguration());
            modelBuilder.Configurations.Add(new FamilyStudentMapConfiguration());
            modelBuilder.Configurations.Add(new ContactInfoMapConfiguration());
            modelBuilder.Configurations.Add(new InfoSourceConfiguration());
            modelBuilder.Configurations.Add(new RelationTypeConfiguration());
            modelBuilder.Configurations.Add(new MembershipTypeConfiguration());
            modelBuilder.Configurations.Add(new StudentInfoConfiguration());
            modelBuilder.Configurations.Add(new StudentDetailConfiguration());
            modelBuilder.Configurations.Add(new GradeLevelConfiguration());
            modelBuilder.Configurations.Add(new ClassStatusConfiguration());
            modelBuilder.Configurations.Add(new DropReasonConfiguration());
            modelBuilder.Configurations.Add(new EnrollTypeConfiguration());
            modelBuilder.Configurations.Add(new UsersConfiguration());
            modelBuilder.Configurations.Add(new RolesConfiguration());
            modelBuilder.Configurations.Add(new GeneralDroppedByConfiguration());
            modelBuilder.Configurations.Add(new FeeTypeConfiguration());
            modelBuilder.Configurations.Add(new PricingPlanConfiguration());
            modelBuilder.Configurations.Add(new ForgotPasswordLogConfiguration());
            //modelBuilder.Configurations.Add(new ImageLogConfiguration());
            modelBuilder.Configurations.Add(new TimesheetConfiguration());
            modelBuilder.Configurations.Add(new SMSCarrierInfoConfiguration());
            modelBuilder.Configurations.Add(new TimeClockUsersPlanConfiguration());

            modelBuilder.Configurations.Add(new ToDoConfiguration());
            modelBuilder.Configurations.Add(new PriorityConfiguration());
            modelBuilder.Configurations.Add(new AgencyTimeZoneConfiguration());
            modelBuilder.Configurations.Add(new HolidayConfiguration());
            modelBuilder.Configurations.Add(new SkillMasterConfiguration());
            modelBuilder.Configurations.Add(new StaffSkillConfiguration());
            modelBuilder.Configurations.Add(new DailyStatusCategoryConfiguration());
            modelBuilder.Configurations.Add(new FoodManagementConfiguration());
            modelBuilder.Configurations.Add(new AgencyRegistrationInfoConfiguration());
            modelBuilder.Entity<tblParticipantInfo>();
            //modelBuilder.Configurations.Add(new EventInfoConfiguration());
            //modelBuilder.Configurations.Add(new TasksConfiguration());
            // modelBuilder.Configurations.Add(new TaskConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        #region Entity Sets

        public IDbSet<IdentityUser> IdentityUser { get; set; }
        public IDbSet<Country> CountrySet { get; set; }
        public IDbSet<State> StateSet { get; set; }
        public IDbSet<City> CitySet { get; set; }
        public IDbSet<SecurityQuestions> SecurityQuestionsSet { get; set; }
        //public IDbSet<Student> StudentSet { get; set; }
        public IDbSet<ParentInfo> ParentInfoSet { get; set; }
        public IDbSet<PaymentInfo> PaymentInfoSet { get; set; }
        public IDbSet<ContactInfo> ContactInfoSet { get; set; }
        //public IDbSet<GuardianInfo> GuardianInfoSet { get; set; }
        public IDbSet<EnrollmentInfo> EnrollmentInfoSet { get; set; }
        public IDbSet<Skills> SkillSet { get; set; }
        public IDbSet<ClassInfo> ClassSet { get; set; }
        public IDbSet<StudentSchedule> StudentScheduleSet { get; set; }
        public IDbSet<Category> CategorySet { get; set; }
        public IDbSet<Room> RoomSet { get; set; }
        public IDbSet<Session> SessionSet { get; set; }
        //public IDbSet<AgencyInfo> AgencySet { get; set; }
        public IDbSet<LessonPlan> LessonPlanSet { get; set; }
        public IDbSet<StaffInfo> StaffInfoSet { get; set; }
        public IDbSet<Position> PositionSet { get; set; }

        public IDbSet<Status> StatusSet { get; set; }
        public IDbSet<LeaveInfo> LeaveInfoSet { get; set; }
        public IDbSet<LeaveType> LeaveTypeSet { get; set; }
        public IDbSet<AgencyRegistrationInfo> AgencyRegistrationInfoSet { get; set; }
        public IDbSet<AgencyPaymentInfo> AgencyPaymentInfoSet { get; set; }
        public IDbSet<AgencyContactInfo> AgencyContactInfoSet { get; set; }
        public IDbSet<AgencyBillingInfo> AgencyBillingInfoSet { get; set; }
        public IDbSet<AgencyLocationInfo> AgencyLocationInfoSet { get; set; }
        public IDbSet<StaffScheduleInfo> StaffScheduleInfoSet { get; set; }
        public IDbSet<AttendanceInfo> AttendanceInfoSet { get; set; }
        public IDbSet<StudentAttendanceInfo> StudentAttendanceInfoSet { get; set; }
        public IDbSet<PayType> PayTypeSet { get; set; }
        public IDbSet<InstructorClassMap> InstructorClassMapSet { get; set; }
        public IDbSet<WaitListMap> WaitListMapSet { get; set; }
        public IDbSet<FamilyInfo> FamilyInfoSet { get; set; }
        public IDbSet<FamilyStudentMap> FamilyStudentMapSet { get; set; }
        public IDbSet<ReferenceDetail> ReferenceDetailSet { get; set; }
        public IDbSet<ContactInfoMap> ContactInfoMapSet { get; set; }
        public IDbSet<InfoSource> InfoSourceSet { get; set; }
        public IDbSet<RelationType> RelationTypeSet { get; set; }
        public IDbSet<MembershipType> MembershipTypeSet { get; set; }
        public IDbSet<StudentInfo> StudentInfoSet { get; set; }
        public IDbSet<StudentDetail> StudentDetailSet { get; set; }
        public IDbSet<GradeLevel> GradeLevelSet { get; set; }
        public IDbSet<ClassStatus> ClassStatusSet { get; set; }
        public IDbSet<DropReason> DropReasonSet { get; set; }
        public IDbSet<EnrollType> EnrollTypeSet { get; set; }
        public IDbSet<Users> UsersSet { get; set; }
        public IDbSet<Roles> RolesSet { get; set; }
        public IDbSet<FeeType> FeesTypeSet { get; set; }
        //public IDbSet<ImageLog> ImageLogSet { get; set; }
        public IDbSet<GeneralDroppedBy> GeneralDroppedBySet { get; set; }
        public IDbSet<PricingPlan> PricingPlanSet { get; set; }
        public IDbSet<tblParentInfo> NewParentInfoSet { get; set; }
        public IDbSet<tblParentParticipantMapping> NewParentParticipantMappingSet { get; set; }
        public IDbSet<tblParticipantInfo> NewParticipantInfoSet { get; set; }
        public IDbSet<tblParticipantAssociatedParticipantMap> NewParticipantAssociatedParticipantMapSet { get; set; }

        public IDbSet<Messages> messageSet { get; set; }
        public IDbSet<MessageTo> toSet { get; set; }
        public IDbSet<MessageFrom> fromSet { get; set; }
        public IDbSet<MessageCC> ccSet { get; set; }
        public IDbSet<MessageContacts> contactsSet { get; set; }

        public IDbSet<ForgotPasswordLog> ForgotPasswordLogSet { get; set; }
        public IDbSet<Timesheet> TimesheetSet { get; set; }
        public IDbSet<SMSCarrier> SMSCarrierSet { get; set; }
        public IDbSet<TimeClockUsersPlan> TimeClockUsersPlanSet { get; set; }

        public IDbSet<Priority> PrioritySet { get; set; }
        public IDbSet<ToDo> ToDoSet { get; set; }
        public IDbSet<AgencyTimeZone> AgencyTimeZoneSet { get; set; }

        public IDbSet<Holiday> HolidaySet { get; set; }

        public IDbSet<Enhancement> EnhancementSet { get; set; }

        public IDbSet<SkillMaster> SkillMasterSet { get; set; }
        public IDbSet<StaffSkill> StaffSkillSet { get; set; }
        public IDbSet<StaffLocationInfo> StaffLocationSet { get; set; }
        public IDbSet<Incident> IncidentSet { get; set; }

        public IDbSet<Tasks> TasksSet { get; set; }
        public IDbSet<DailyStatusCategory> DailyStatusCategorySet { get; set; }
        public IDbSet<DailyStatus> DailyStatusSet { get; set; }
        public IDbSet<FoodManagementMaster> FoodManagementMasterSet { get; set; }
        public IDbSet<MealType> MealTypeSet { get; set; }
        public IDbSet<FoodManagementMealPattern> FoodManagementMealPatternSet { get; set; }
        public IDbSet<NewParticipantAssociationMaster> ParticipantAssociationMasterSet { get; set; }
        public IDbSet<FoodManagementMealPatternItems> FoodManagementMealPatternItemsSet { get; set; }
        public IDbSet<AgencySubscriptionType> AgencySubscriptionTypeSet { get; set; }
        public IDbSet<MealSchedule> MealScheduleSet { get; set; }
        public IDbSet<MealScheduleItemsInfo> MealScheduleItemsInfoSet { get; set; }
        public IDbSet<MealServeSize> MealServingSizeInfoSet { get; set; }
        public IDbSet<MealServeQuantity> MealServingQuantityInfoSet { get; set; }
        public IDbSet<MealServeType> MealServingTypeInfoSet { get; set; }
        public IDbSet<MealItemMaster> MealItemMasterInfoSet { get; set; }
        public IDbSet<PaymentHistoryInfo> PaymentHistoryInfoSet { get; set; }
        public IDbSet<PricingPlanAssociations> PricingPlanAssociationsInfoSet { get; set; }
        public IDbSet<PaymentHistoryInfoForPlans> PaymentHistoryInfoForPlansSet { get; set; }
        public IDbSet<ModeOfConvenience> ModeOfConvenienceSet { get; set; }
        public IDbSet<ExceptionLogInfo> ExceptionLogInfoSet { get; set; }
        public IDbSet<TasksClassMapping> TasksClassMappingSet { get; set; }
        public IDbSet<IncidentParentParticipantMapping> IncidentParentParticipantMappingSet { get; set; }
        #endregion
    }
}