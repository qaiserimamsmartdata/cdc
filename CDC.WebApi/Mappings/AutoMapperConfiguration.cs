﻿using AutoMapper;

namespace CDC.WebApi.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.CreateMap<string, string>().ConvertUsing<NullStringConverter>();
            });
        }
    }
}