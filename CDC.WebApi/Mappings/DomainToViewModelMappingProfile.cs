﻿using System;
using AutoMapper;
using CDC.Entities.Class;
using CDC.Entities.Tools.Class;
using CDC.ViewModel;
using CDC.ViewModel.Class;
using CDC.ViewModel.Tools.Class;
using CDC.Entities.Staff;
using CDC.ViewModel.Staff;
using CDC.Entities.Tools.Staff;
using CDC.ViewModel.Tools.Staff;
using CDC.Entities;
using CDC.ViewModel.Schedule;
using CDC.Entities.Schedule;
using CDC.ViewModel.AgencyRegistration;
using CDC.Entities.AgencyRegistration;
using CDC.Entities.Students;
using CDC.ViewModel.Attendance;
using CDC.Entities.Attendance;
using CDC.Entities.Family;
using CDC.ViewModel.Family;
using CDC.Entities.Tools.Family;
using CDC.ViewModel.Tools.Family;
using CDC.Entities.Common;
using CDC.ViewModel.Tools.Student;
using CDC.Entities.Tools.Student;
using CDC.ViewModel.User;
using CDC.Entities.User;
using CDC.Entities.Pricing;
using CDC.ViewModel.Location;
using CDC.Entities.SMSCarrier;
using CDC.Entities.TimeClockUsers;
using CDC.ViewModel.SuperAdmin;
using CDC.Entities.ToDoTask;
using CDC.ViewModel.ToDoTask;
using CDC.Entities.Tools.Category;
using CDC.ViewModel.Tools.Category;

using CDC.ViewModel.Tools.ManageHoliday;
using CDC.Entities.Tools.ManageHoliday;
using CDC.Entities.Tools.Enhancements;
using CDC.ViewModel.Tools.Enhancements;
using CDC.Entities.ParticipantEnrollmentPayment;
using CDC.Entities.Skills;
using CDC.ViewModel.Skill;

using CDC.ViewModel.messages;
using CDC.Entities.Messages;
using CDC.ViewModel.ParticipantEnrollmentPayment;
using CDC.ViewModel.ParticipantIncident;
using CDC.Entities.ParticipantIncident;
using CDC.ViewModel.DailyStatus;
using CDC.Entities.DailyStatus;
using CDC.ViewModel.Tools.FoodManagementMaster;
using CDC.Entities.Tools.FoodManagement;
using CDC.ViewModel.MealSchedular;
using CDC.Entities.MealSchedular;
using CDC.Entities.Tools.MealServingType;
using CDC.ViewModel.Tools.MealServingType;
using CDC.Entities.PaymentHistory;
using CDC.ViewModel.PaymentHistory;
using CDC.Entities.PaymentHistoryForPlans;
using CDC.ViewModel.PaymentHistoryForPlans;
using CDC.ViewModel.Tools.Attendance;
using CDC.Entities.Tools.Attendance;
using CDC.Entities.NewParticipant;
using CDC.ViewModel.NewParticipant;
using CDC.Entities.NewParent;
using CDC.ViewModel.NewParent;
using CDC.Entities.Association;
using CDC.ViewModel.NewAssociation;

namespace CDC.WebApi.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        [Obsolete("Create a constructor and configure inside of your profile\'s constructor instead. Will be removed in 6.0")]
        protected override void Configure()
        {
            //CreateMap<Customer, CustomerViewModel>()
            //    .ForMember(vm => vm.CityName, map => map.MapFrom(m => m.City.CityName));

            CreateMap<ClassInfo, ClassViewModel>();
            CreateMap<ClassViewModel, ClassInfo>()
               .ForMember(vm => vm.ClassName, map => map.MapFrom(m => m.ClassName));

            CreateMap<Category, CategoryViewModel>();
            CreateMap<CategoryViewModel, Category>();

            CreateMap<Room, RoomViewModel>();
            CreateMap<RoomViewModel, Room>();

            CreateMap<Session, SessionViewModel>();
            CreateMap<SessionViewModel, Session>();

            CreateMap<LessonPlan, LessonPlanViewModel>();

            CreateMap<LessonPlanViewModel, LessonPlan>();
            //CreateMap<AgencyInfo, AgencyViewModel>();

            //CreateMap<AgencyViewModel, AgencyInfo>();

            CreateMap<Messages, messageViewModel>()
                .ForMember(vm => vm.id, map => map.MapFrom(m => m.ID.ToString()));
            CreateMap<messageViewModel, Messages>()
                .ForMember(vm => vm.ID, map => map.MapFrom(m => m.id));

            CreateMap<MessageCC, ccViewModel>();
            CreateMap<ccViewModel, MessageCC>();
            CreateMap<MessageTo, toViewModel>();
            CreateMap<toViewModel, MessageTo>();
            CreateMap<MessageFrom, fromViewModel>();
            CreateMap<fromViewModel, MessageFrom>();

            CreateMap<MessageContacts, contactsViewModel>();
            CreateMap<contactsViewModel, MessageContacts>();

            CreateMap<AgencyPaymentInfo,AgencyPaymentViewModel>();
            CreateMap<AgencyPaymentViewModel, AgencyPaymentInfo>();


            CreateMap<StaffInfo, StaffViewModel>()
              .ForMember(vm => vm.PositionName, map => map.MapFrom(m => m.Position.Name))
              //.ForMember(vm => vm.StateName, map => map.MapFrom(m => m.StateName))
              .ForMember(vm => vm.StatusName, map => map.MapFrom(m => m.Status.Name))
              .ForMember(vm => vm.FullName, map => map.MapFrom(m => m.FirstName + " " + m.LastName))


              .ForMember(vm => vm.DateHiredString, map => map.MapFrom(m => m.DateHired.Value.ToShortDateString()))

              .ForMember(vm => vm.GenderName, map => map.MapFrom(m => m.Gender.ToString()))
              .ForMember(vm => vm.DateOfBirthString, map => map.MapFrom(m => m.DateOfBirth.Value.ToShortDateString()));
            //.ForMember(vm => vm.StateName, map => map.MapFrom(m => m.City.stat))
            //.ForMember(vm => vm.CountryName, map => map.MapFrom(m => m.City.State.Country.CountryName));
            //.ForMember(vm => vm.DateOfJoiningString, map => map.MapFrom(m => m.DateOfJoining.Value.ToShortDateString()))
            // .ForMember(vm => vm.DateHiredString, map => map.MapFrom(m => m.DateHired.Value.ToShortDateString()));



            CreateMap<StaffViewModel, StaffInfo>();
            //.ForMember(vm => vmD, map => map.MapFrom(m => m.SMSCarrier));
            //  .ForMember(vm => vm.StateName, map => map.MapFrom(m => m.StateName));
            //   .ForMember(vm => vm.DateHired.Value.ToShortDateString(), map => map.MapFrom(m => m.DateOfBirthString));




            CreateMap<Position, PositionViewModel>();
            CreateMap<PositionViewModel, Position>();

            CreateMap<Status, StatusViewModel>();
            CreateMap<StatusViewModel, Status>();

            CreateMap<City, CityViewModel>();
            CreateMap<CityViewModel, City>();
            CreateMap<tblParticipantInfo,tblParticipantInfoViewModel>();
            CreateMap<tblParticipantInfoViewModel, tblParticipantInfo>();
            CreateMap<tblParentInfo, tblParentInfoViewModel>();
            CreateMap<tblParentInfoViewModel, tblParentInfo>();
            CreateMap<NewParticipantAssociationMaster, tblParticipantAssociationMasterViewModel>();
            CreateMap<tblParticipantAssociationMasterViewModel, NewParticipantAssociationMaster>();
            CreateMap<tblParentParticipantMapping, tblParentParticipantMappingViewModel>();
            CreateMap<tblParentParticipantMappingViewModel,tblParentParticipantMapping>();
            CreateMap<tblParticipantAssociatedParticipantMap, tblParticipantAssociatedParticipantViewModel>();
            CreateMap<tblParticipantAssociatedParticipantViewModel, tblParticipantAssociatedParticipantMap>();

            CreateMap<StudentScheduleViewModel, StudentSchedule>();
            CreateMap<StudentSchedule, StudentScheduleViewModel>()
                .ForMember(vm => vm.ClassName, map => map.MapFrom(m => m.ClassInfo.ClassName))
                 .ForMember(vm => vm.ImagePath, map => map.MapFrom(m => m.StudentInfo.ImagePath));


            CreateMap<LeaveInfo, LeaveViewModel>();
            CreateMap<LeaveViewModel, LeaveInfo>();

            CreateMap<AgencyRegistrationViewModel, AgencyRegistrationInfo>();
            CreateMap<AgencyRegistrationInfo, AgencyRegistrationViewModel>();

            CreateMap<AgencyRegistrationViewModel, AgencyContactInfo>();
            CreateMap<AgencyContactInfo, AgencyRegistrationViewModel>();
            CreateMap<AgencyRegistrationViewModel, AgencyLocationInfo>();
            CreateMap<AgencyLocationInfo, AgencyRegistrationViewModel>();
            CreateMap<AgencyRegistrationViewModel, AgencyBillingInfo>();
            CreateMap<AgencyBillingInfo, AgencyRegistrationViewModel>();
            CreateMap<AgencyRegistrationViewModel, AgencyPaymentInfo>();
            CreateMap<AgencyPaymentInfo, AgencyRegistrationViewModel>();

            CreateMap<LeaveType, LeaveTypeViewModel>();
            CreateMap<LeaveTypeViewModel, LeaveType>();

            CreateMap<EnrollListViewModel, EnrollmentInfo>();
            CreateMap<EnrollmentInfo, EnrollListViewModel>()
                .ForMember(vm => vm.StudentFullName, map => map.MapFrom(m => m.Student.FirstName + ' ' + m.Student.LastName))
                .ForMember(vm => vm.ImagePath, map => map.MapFrom(m => m.Student.ImagePath))
                .ForMember(vm => vm.Gender, map => map.MapFrom(m => m.Student.Gender))
                .ForMember(vm => vm.FirstName, map => map.MapFrom(m => m.Student.FirstName))
                .ForMember(vm => vm.LastName, map => map.MapFrom(m => m.Student.LastName))
                .ForMember(vm => vm.DateOfBirth, map => map.MapFrom(m => m.Student.DateOfBirth));

            CreateMap<StudentSchedule, EnrollListViewModel>()
                .ForMember(vm => vm.StudentFullName, map => map.MapFrom(m => m.StudentInfo.FirstName + ' ' + m.StudentInfo.LastName))
                .ForMember(vm => vm.ImagePath, map => map.MapFrom(m => m.StudentInfo.ImagePath))
                .ForMember(vm => vm.Gender, map => map.MapFrom(m => m.StudentInfo.Gender))
                .ForMember(vm => vm.StudentId, map => map.MapFrom(m => m.StudentInfo.ID))
                .ForMember(vm => vm.DateOfBirth, map => map.MapFrom(m => m.StudentInfo.DateOfBirth))
                .ForMember(vm => vm.ClassName, map => map.MapFrom(m => m.ClassInfo.ClassName))
                .ForMember(vm => vm.RoomName, map => map.MapFrom(m => m.ClassInfo.Room.Name))
                .ForMember(vm => vm.CategoryName, map => map.MapFrom(m => m.ClassInfo.Category.Name))
                .ForMember(vm => vm.SessionName, map => map.MapFrom(m => m.ClassInfo.Session.Name))
                .ForMember(vm => vm.FirstName, map => map.MapFrom(m => m.StudentInfo.FirstName))
                .ForMember(vm => vm.LastName, map => map.MapFrom(m => m.StudentInfo.LastName))
                 .ForMember(vm => vm.EnrollStatus, map => map.MapFrom(m => m.IsEnrolled ? "Enrolled" : "Cancelled"));

            CreateMap<EnrollListViewModel, StudentSchedule>()
               .ForMember(vm => vm.ID, map => map.MapFrom(m => m.ID))
               .ForMember(vm => vm.RoomId, map => map.MapFrom(m => m.RoomId))
               .ForMember(vm => vm.StudentId, map => map.MapFrom(m => m.StudentId))
               .ForMember(vm => vm.StartDate, map => map.MapFrom(m => m.Startdate))
               .ForMember(vm => vm.EndDate, map => map.MapFrom(m => m.Enddate))
               .ForMember(vm => vm.ClassId, map => map.MapFrom(m => m.ClassId));

            CreateMap<StaffScheduleInfo, StaffScheduleViewModel>();
            // CreateMap<StaffScheduleInfo, StaffScheduleViewModel>().MaxDepth(5);

            CreateMap<StaffScheduleViewModel, StaffScheduleInfo>();


            CreateMap<AttendanceInfo, AttendanceViewModel>();
            //.ForMember(vm => vm.StaffName, map => map.MapFrom(m => m.Staff.FirstName + " " + m.Staff.LastName))
            //.ForMember(vm => vm.StringCreatedDate, map => map.MapFrom(m => m.CreatedDate.Value.ToShortDateString()))
            //.ForMember(vm => vm.Createdday, map => map.MapFrom(m => m.CreatedDate.Value.DayOfWeek.ToString()))
            //.ForMember(vm => vm.Staff, map => map.MapFrom(m => m.Staff))
            //.ForMember(vm => vm.StaffId, map => map.MapFrom(m => m.StaffId));



            CreateMap<AttendanceViewModel, AttendanceInfo>();

            CreateMap<StaffScheduleInfo, AttendanceViewModel>()
                .ForMember(vm => vm.StaffName, map => map.MapFrom(m => m.Staff.FirstName + " " + m.Staff.LastName))
                .ForMember(vm => vm.ClassName, map => map.MapFrom(m => m.Class.ClassName))
                .ForMember(vm => vm.RoomName, map => map.MapFrom(m => m.Room.Name))
            .ForMember(vm => vm.GenderName, map => map.MapFrom(m => m.Staff.Gender.ToString()))
            .ForMember(vm => vm.DOB, map => map.MapFrom(m => m.Staff.DateOfBirth.Value.ToShortDateString()));



            CreateMap<StudentAttendanceInfo, StudentAttendanceViewModel>()
                .ForMember(vm => vm.StudentName, map => map.MapFrom(m => m.Student.FirstName + " " + m.Student.LastName)) //changed
                .ForMember(vm => vm.ImagePath, map => map.MapFrom(m => m.Student.ImagePath)) //changed
                                                                                             //.ForMember(vm => vm.ClassName, map => map.MapFrom(m => m.StudentSchedule.ClassInfo.ClassName)) //changed
                .ForMember(vm => vm.ParentName, map => map.MapFrom(m => m.DropedBy.FirstName + " " + m.DropedBy.LastName));
            CreateMap<StudentAttendanceViewModel, StudentAttendanceInfo>();

            CreateMap<PayType, PayTypeViewModel>();
            CreateMap<PayTypeViewModel, PayType>();

            CreateMap<StudentSchedule, StudentAttendanceViewModel>()
                .ForMember(vm => vm.StudentId, map => map.MapFrom(m => m.StudentId))
                .ForMember(vm => vm.StudentName, map => map.MapFrom(m => m.StudentInfo.FirstName + " " + m.StudentInfo.LastName))
                .ForMember(vm => vm.ImagePath, map => map.MapFrom(m => m.StudentInfo.ImagePath))
                .ForMember(vm => vm.ClassName, map => map.MapFrom(m => m.ClassInfo.ClassName))
                .ForMember(vm => vm.StudentScheduleId, map => map.MapFrom(m => m.ID));
            CreateMap<StudentAttendanceViewModel, StudentSchedule>();
            //CreateMap<Student, StudentViewModel>();
            //CreateMap<StudentViewModel, Student>();
            CreateMap<WaitListMap, WaitListMapViewModel>();
            CreateMap<WaitListMapViewModel, WaitListMap>();
            CreateMap<InstructorClassMapViewModel, InstructorClassMap>();
            CreateMap<InstructorClassMap, InstructorClassMapViewModel>()
                .ForMember(vm => vm.InstructorName, map => map.MapFrom(m => m.Staff.FirstName + " " + m.Staff.LastName))
                .ForMember(vm => vm.ClassName, map => map.MapFrom(m => m.Class.ClassName))
                .ForMember(vm => vm.Email, map => map.MapFrom(m => m.Staff.Email))
                .ForMember(vm => vm.DesignationName, map => map.MapFrom(m => m.Staff.Position.Name));

            CreateMap<State, StateViewModel>();

            CreateMap<StateViewModel, State>();
            CreateMap<Country, CountryViewModel>();
            CreateMap<CountryViewModel, Country>();

            CreateMap<ParentInfo, ParentInfoViewModel>()
             .ForMember(vm => vm.RelationName, map => map.MapFrom(m => m.RelationType.Name))
             .ForMember(vm => vm.FullName, map => map.MapFrom(m => m.FirstName + " " + m.LastName))
             .ForMember(vm => vm.ImagePath, map => map.MapFrom(m => m.ImagePath));

            CreateMap<ParentInfoViewModel, ParentInfo>();

            CreateMap<ContactInfo, ContactInfoViewModel>();

            CreateMap<ContactInfoViewModel, ContactInfo>();
            CreateMap<ContactInfoMap, ContactInfoMapViewModel>()
                .ForMember(vm => vm.PhoneCode, map => map.MapFrom(m => m.Contact.Country.PhoneCode.ToString()));
            CreateMap<ContactInfoMapViewModel, ContactInfoMap>();
            CreateMap<FamilyInfo, FamilyInfoViewModel>();

            CreateMap<FamilyInfoViewModel, FamilyInfo>();

            CreateMap<ReferenceDetail, ReferenceDetailViewModel>();
            CreateMap<ReferenceDetailViewModel, ReferenceDetail>();
            CreateMap<RelationType, RelationTypeViewModel>();
            CreateMap<RelationTypeViewModel, RelationType>();
            CreateMap<InfoSource, InfoSourceViewModel>();
            CreateMap<InfoSourceViewModel, InfoSource>();
            CreateMap<MembershipType, MembershipTypeViewModel>();
            CreateMap<MembershipTypeViewModel, MembershipType>();
            CreateMap<StudentDetail, StudentDetailViewModel>()
            .ForMember(vm => vm.FirstName, map => map.MapFrom(m => m.Student.FirstName))
            .ForMember(vm => vm.LastName, map => map.MapFrom(m => m.Student.LastName));
            CreateMap<StudentDetailViewModel, StudentDetail>();
            CreateMap<StudentInfo, StudentInfoViewModel>();



            CreateMap<StudentInfoViewModel, StudentInfo>();
            CreateMap<GradeLevel, GradeLevelViewModel>();
            CreateMap<GradeLevelViewModel, GradeLevel>();
            CreateMap<FamilyInfo, FamilyDetailViewModel>();

            CreateMap<GradeLevelViewModel, GradeLevel>();
            CreateMap<FamilyStudentMap, FamilyStudentMapViewModel>();
            CreateMap<FamilyStudentMapViewModel, FamilyStudentMap>();
            CreateMap<ClassStatusViewModel, ClassStatus>();
            CreateMap<ClassStatus, ClassStatusViewModel>();

            CreateMap<DropReasonViewModel, DropReason>();
            CreateMap<DropReason, DropReasonViewModel>();

            CreateMap<EnrollTypeViewModel, EnrollType>();
            CreateMap<EnrollType, EnrollTypeViewModel>();

            CreateMap<UserViewModel, Users>();
            CreateMap<Users, UserViewModel>()
            .ForMember(vm => vm.RoleName, map => map.MapFrom(m => m.Role.RoleName));

            CreateMap<RolesViewModel, Roles>();
            CreateMap<Roles, RolesViewModel>();


            CreateMap<StaffViewModel, Users>();
            CreateMap<PaymentInfo, PaymentInfoViewModel>();


            CreateMap<PaymentInfoViewModel, PaymentInfo>()
                .ForMember(vm => vm.CardNumber, map => map.MapFrom(m => Convert.ToInt64(m.CardNumber)));

            CreateMap<GeneralDroppedBy, GeneralDroppedByViewModel>();
            CreateMap<GeneralDroppedByViewModel, GeneralDroppedBy>();
            CreateMap<PaymentInfoViewModel, PaymentInfo>();
            CreateMap<StaffViewModel, Users>();
            CreateMap<FeeType, FeeTypeViewModel>();
            CreateMap<FeeTypeViewModel, FeeType>();

            CreateMap<StaffInfo, AttendanceViewModel>()
            .ForMember(vm => vm.FullName, map => map.MapFrom(m => m.FirstName + ' ' + m.LastName))
            .ForMember(vm => vm.StaffId, map => map.MapFrom(m => m.ID))
            .ForMember(vm => vm.ImagePath, map => map.MapFrom(m => m.ImagePath))
             .ForMember(vm => vm.GenderName, map => map.MapFrom(m => m.Gender))
              .ForMember(vm => vm.PositionName, map => map.MapFrom(m => m.Position.Name))
             .ForMember(vm => vm.PostalCode, map => map.MapFrom(m => m.PostalCode))
             .ForMember(vm => vm.DateOfBirthString, map => map.MapFrom(m => m.DateOfBirth.Value.ToShortDateString()))
             .ForMember(vm => vm.DateHiredString, map => map.MapFrom(m => m.DateHired.Value.ToShortDateString()));
            CreateMap<EnrollListViewModel, StudentInfo>();
            CreateMap<StudentInfo, EnrollListViewModel>();
            CreateMap<PricingPlanViewModel, PricingPlan>();
            CreateMap<PricingPlan, PricingPlanViewModel>();
            CreateMap<ForgotPasswordLog, ForgotPasswordLogViewModel>();
            CreateMap<ForgotPasswordLogViewModel, ForgotPasswordLog>();
            CreateMap<Timesheet, TimesheetModel>();
            CreateMap<TimesheetModel, Timesheet>();
            //CreateMap<EnrollListViewModel, StudentSchedule>();
            //CreateMap<StudentSchedule, EnrollListViewModel>();
            CreateMap<LocationViewModel, AgencyLocationInfo>();
            CreateMap<AgencyLocationInfo, LocationViewModel>();
            CreateMap<SMSCarrier, SMSCarrierViewModel>();
            CreateMap<SMSCarrierViewModel, SMSCarrier>();
            CreateMap<TimeClockUsersPlan, TimeClockUsersViewModel>();
            CreateMap<TimeClockUsersViewModel, TimeClockUsersPlan>();


            CreateMap<UserViewModel, ForgotPasswordLogViewModel>();
            CreateMap<AgencyRegistrationInfo, AgencyListViewModel>();

            CreateMap<ToDo, ToDoViewModel>();
            CreateMap<ToDoViewModel, ToDo>();

            CreateMap<Priority, PriorityViewModel>();
            CreateMap<PriorityViewModel, Priority>();

            CreateMap<AgencyRegistrationViewModel, AgencyTimeZone>();


            CreateMap<Holiday, HolidayViewModel>();
            CreateMap<HolidayViewModel, Holiday>();

            CreateMap<Enhancement, EnhancementViewModel>().ReverseMap();

            CreateMap<SkillMaster, SkillMasterViewModel>().ReverseMap();
            CreateMap<StaffSkill, StaffSkillViewModel>().ReverseMap();
            CreateMap<ParticipantEnrollPaymentViewModel, ParticipantEnrollPayment>();
            CreateMap<ParticipantEnrollPayment, ParticipantEnrollPaymentViewModel>();
            CreateMap<StaffLocationInfo, StaffLocationInfoViewModel>();
            CreateMap<StaffLocationInfoViewModel, StaffLocationInfo>();

            CreateMap<AgencyLocationInfoViewModel, AgencyLocationInfo>();
            CreateMap<AgencyLocationInfo, AgencyLocationInfoViewModel>();

            CreateMap<AgencyLocationInfo, AgencyLocationInfoViewModel>();
            CreateMap<IncidentViewModel, Incident>();

            CreateMap<Incident, IncidentViewModel>();


            CreateMap<IncidentParticipantViewModel, IncidentParticipantMapping>()
            .ForMember(vm => vm.StudentInfoId, map => map.MapFrom(m => m.studentId));
            CreateMap<IncidentParticipantMapping, IncidentParticipantViewModel>();


            CreateMap<IncidentOtherParticipantViewModel, IncidentOtherParticipantMapping>()
                .ForMember(vm => vm.OtherStudentInfoId, map => map.MapFrom(m => m.studentId));

            CreateMap<IncidentOtherParticipantMapping, IncidentOtherParticipantViewModel>();


            CreateMap<Tasks, TasksViewModel>()
                      .ForMember(vm => vm.TaskID, map => map.MapFrom(m => m.ID));
            CreateMap<TasksViewModel, Tasks>()
                 .ForMember(vm => vm.ID, map => map.MapFrom(m => m.TaskID));

            CreateMap<Tasks, ViewModel.Staff.TasksViewModelCopy>()
              .ForMember(vm => vm.TaskID, map => map.MapFrom(m => m.ID));
            CreateMap<ViewModel.Staff.TasksViewModelCopy, Tasks>()
                 .ForMember(vm => vm.ID, map => map.MapFrom(m => m.TaskID));

            CreateMap<DailyStatusViewModel, DailyStatus>();
            CreateMap<DailyStatus, DailyStatusViewModel>();
            CreateMap<DailyStatusCategoryViewModel, DailyStatusCategory>();
            CreateMap<DailyStatusCategory, DailyStatusCategoryViewModel>();
            CreateMap<FamilyInfo, FamilyInfoViewModel>();
            CreateMap<FamilyInfoViewModel, FamilyInfo>();
            CreateMap<MealTypeViewModel, MealType>();
            CreateMap<MealType, MealTypeViewModel>();

            CreateMap<FoodManagementMasterViewModel, FoodManagementMaster>();
            CreateMap<FoodManagementMaster, FoodManagementMasterViewModel>();

            CreateMap<FoodManagementMealPatternViewModel, FoodManagementMealPattern>();
            CreateMap<FoodManagementMealPattern, FoodManagementMealPatternViewModel>();

            CreateMap<FoodManagementMealPatternItemsViewModel, FoodManagementMealPatternItems>();
            CreateMap<FoodManagementMealPatternItems, FoodManagementMealPatternItemsViewModel>();
            CreateMap<MealSchedule, MealSchedularViewModel>()
                 .ForMember(vm => vm.MealSheduleID, map => map.MapFrom(m => m.ID));

            CreateMap<MealSchedularViewModel, MealSchedule>()
                  .ForMember(vm => vm.ID, map => map.MapFrom(m => m.MealSheduleID));

            CreateMap<MealScheduleItemsInfoViewModel, MealScheduleItemsInfo>();
            CreateMap<MealScheduleItemsInfo, MealScheduleItemsInfoViewModel>();

            CreateMap<MealSchedule, ViewModel.MealSchedular.TasksViewModelCopy>()
                 .ForMember(vm => vm.MealSheduleID, map => map.MapFrom(m => m.ID));
            CreateMap<ViewModel.MealSchedular.TasksViewModelCopy,MealSchedule>()
                 .ForMember(vm => vm.ID, map => map.MapFrom(m => m.MealSheduleID));


            CreateMap<MealServeSize, MealServeSizeViewModel>();
            CreateMap<MealServeSizeViewModel, MealServeSize>();

            CreateMap<MealServeQuantity, MealServeQuantityViewModel>();
            CreateMap<MealServeQuantityViewModel, MealServeQuantity>();

            CreateMap<MealServeType, MealServeTypeViewModel>();
            CreateMap<MealServeTypeViewModel, MealServeType>();

            CreateMap<MealItemMaster, MealItemViewModel>();
            CreateMap<MealItemViewModel, MealItemMaster>();

            CreateMap<PaymentHistoryInfo, PaymentHistoryInfoViewModel>();
            CreateMap<PaymentHistoryInfoViewModel, PaymentHistoryInfo>();

            CreateMap<PricingPlanAssociations, PricingPlanAssociationsViewModel>();
            CreateMap<PricingPlanAssociationsViewModel, PricingPlanAssociations>();
            CreateMap<PaymentHistoryInfoViewModel, PaymentHistoryInfo>();
            CreateMap<PaymentHistoryInfo, PaymentHistoryInfoViewModel>();
            CreateMap<PaymentHistoryInfoForPlansViewModel, PaymentHistoryInfoForPlans>();
            CreateMap<PaymentHistoryInfoForPlans,PaymentHistoryInfoForPlansViewModel>();
            CreateMap<ModeOfConvenience, ModeOfConvenienceViewModel>();
            CreateMap<ModeOfConvenienceViewModel,ModeOfConvenience>();
            CreateMap<IncidentParentParticipantMapping, IncidentParentParticipantMappingViewModel>();
            CreateMap<IncidentParentParticipantMappingViewModel, IncidentParentParticipantMapping>();
        }
    }
    public class NullStringConverter : ITypeConverter<string, string>
    {
        public string Convert(string source, ResolutionContext context)
        {
            return source ?? string.Empty;
        }

        public string Convert(string source, string destination, ResolutionContext context)
        {
            return source ?? string.Empty;
        }
    }
}