﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CDC.WebApi.Startup))]

namespace CDC.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
