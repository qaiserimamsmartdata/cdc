﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDC.WebApi.Models
{
    public sealed class ResponseModel<T>
    {
        private T _t;

        /// <summary>
        /// Gets or sets property to hold the response data.
        /// </summary>
        public T Response
        {
            get { return _t; }
            set { _t = value; }
        }
        /// <summary>
        /// Gets the true if any error occurs else will get value false.
        /// </summary>
        public bool IsError
        {
            get { return _t == null ? true : false; }
        }
        /// <summary>
        /// Gets or sets property to hold the error message.
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Gets or sets property to hold the response code.
        /// </summary>
        public int ResponseCode { get; set; }
        /// <summary>
        /// Gets or sets property to hold the response description.
        /// </summary>
        public string ResponseDescription { get; set; }
        /// <summary>
        /// Gets or sets property to hold the sub status code of response.
        /// </summary>
        public int SubStatusCode { get; set; }
    }
}