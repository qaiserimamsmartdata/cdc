﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDC.WebApi.Email.EmailTemplate
{
    public class TestEmailTemplate
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsPremiumUser { get; set; }
    }
}