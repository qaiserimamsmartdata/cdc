﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace CDC.WebApi.Email
{
    public class Email
    {
        public Email()
        {
            StrCc = new List<string>();
            StrBcc = new List<string>();
        }

        /// <summary>
        ///     This function will send the email
        /// </summary>
        /// <returns></returns>

        //public void SendEmailToAdminOnly()
        //{

        //    try
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

        //        //===========
        //        var client = GetSmtpClient();
        //        var mail = new MailMessage { From = new MailAddress(MailFrom) };
        //        mail.From = new MailAddress(MailFrom);
        //        var objPermissionsDb = new PermissionsDB();

        //        foreach (var bcc in StrBcc)
        //        {
        //            mail.Bcc.Add(objPermissionsDb.GetDecryptedData(bcc));
        //        }

        //        //mail.Priority = MailPriority.Normal;
        //        mail.Subject = MailSubject;
        //        mail.Body = MailMessage;
        //        mail.IsBodyHtml = true;
        //        client.Send(mail);
        //    }
        //    catch (Exception ex)
        //    {
        //        var errorLog = new ErrorLog
        //        {
        //            Error = ex + "-------->DateTime: " + DateTime.UtcNow,
        //            PageName = "Email.cs",
        //            ActionName = "SendMail()",
        //            UserId = 0
        //        };
        //        WriteErrorToDatabase.SaveErrorLog(errorLog);
        //    }
        //    finally
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = null;
        //    }

        //}

        //public void SendEmailAfterSave()
        //{

        //    try
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

        //        //===========
        //        var client = GetSmtpClient();
        //        var mail = new MailMessage { From = new MailAddress(MailFrom) };
        //        mail.To.Add(MailTo);
        //        mail.From = new MailAddress(MailFrom);
        //        var objPermissionsDb = new PermissionsDB();

        //        foreach (var bcc in StrBcc)
        //        {
        //            mail.Bcc.Add(objPermissionsDb.GetDecryptedData(bcc));
        //        }

        //        //mail.Priority = MailPriority.Normal;
        //        mail.Subject = MailSubject;
        //        mail.Body = MailMessage;
        //        mail.IsBodyHtml = true;
        //        client.Send(mail);
        //    }
        //    catch (Exception ex)
        //    {
        //        var errorLog = new ErrorLog
        //        {
        //            Error = ex + "-------->DateTime: " + DateTime.UtcNow,
        //            PageName = "Email.cs",
        //            ActionName = "SendMail()",
        //            UserId = 0
        //        };
        //        WriteErrorToDatabase.SaveErrorLog(errorLog);
        //    }
        //    finally
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = null;
        //    }

        //}
        public void SendEmail()
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

                //===========
                var client = GetSmtpClient();
                var mail = new MailMessage {From = new MailAddress(MailFrom)};
                mail.To.Add(MailTo);
                mail.From = new MailAddress(MailFrom);

                mail.Subject = MailSubject;
                mail.Body = MailMessage;
                mail.IsBodyHtml = true;
                client.Send(mail);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        //public void SendEmailAttachement()
        //{
        //    try
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
        //    }
        //    catch (Exception ex)
        //    {
        //        var errorLog = new ErrorLog
        //        {
        //            Error = ex + "-------->DateTime: " + DateTime.UtcNow,
        //            PageName = "Email.cs",
        //            ActionName = "SendEmailAttachement()",
        //            UserId = 0
        //        };
        //        WriteErrorToDatabase.SaveErrorLog(errorLog);
        //    }
        //    finally
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = null;
        //    }
        //    var client = GetSmtpClient();

        //    var mail = new MailMessage { From = new MailAddress(MailFrom) };
        //    mail.To.Add(MailTo);
        //    var objPermissionsDb = new PermissionsDB();

        //    foreach (var cc in StrCc)
        //    {
        //        mail.CC.Add(objPermissionsDb.GetDecryptedData(cc));
        //    }

        //    foreach (var bcc in StrBcc)
        //    {
        //        mail.Bcc.Add(objPermissionsDb.GetDecryptedData(bcc));
        //    }

        //    mail.Priority = MailPriority.Normal;
        //    mail.Attachments.Add(new Attachment(SendMailAttachment, AttachmentName));
        //    mail.Subject = MailSubject;
        //    mail.Body = MailMessage;
        //    mail.IsBodyHtml = true;
        //    client.Send(mail);

        //}

        //public void SendEmailAttachementPath()
        //{
        //    try
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
        //    }
        //    catch (Exception ex)
        //    {
        //        var errorLog = new ErrorLog
        //        {
        //            Error = ex + "-------->DateTime: " + DateTime.UtcNow,
        //            PageName = "Email.cs",
        //            ActionName = "SendEmailAttachementPath()",
        //            UserId = 0
        //        };
        //        WriteErrorToDatabase.SaveErrorLog(errorLog);
        //    }
        //    finally
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = null;
        //    }

        //    var client = GetSmtpClient();

        //    using (var objMailMessage = new MailMessage(MailFrom, MailTo, MailSubject, MailMessage))
        //    {
        //        using (var a = new Attachment(MailAttachmentPath))
        //        {
        //            objMailMessage.Attachments.Add(a);
        //        }
        //        var objPermissionsDb = new PermissionsDB();

        //        foreach (var bcc in StrBcc)
        //        {
        //            objMailMessage.Bcc.Add(objPermissionsDb.GetDecryptedData(bcc));
        //        }

        //        objMailMessage.IsBodyHtml = true;
        //        objMailMessage.Priority = MailPriority.Normal;

        //        client.Send(objMailMessage);
        //    }
        //}

        private SmtpClient GetSmtpClient()
        {
            return new SmtpClient
            {
                Host = ConfigurationManager.AppSettings["MailRelayServer"].ToString(CultureInfo.InvariantCulture),
                UseDefaultCredentials = false,
                EnableSsl = false,
                Credentials =
                    new NetworkCredential(
                        ConfigurationManager.AppSettings["EmailMailFrom"].ToString(CultureInfo.CurrentCulture),
                        ConfigurationManager.AppSettings["CredentialsPassword"].ToString(CultureInfo.CurrentCulture)),
                Port = Convert.ToInt32(ConfigurationManager.AppSettings["MailServerPort"])
            };
        }

        #region Properties

        public string MailTo { get; set; }
        public string MailFrom { get; set; }
        public string MailSubject { get; set; }
        public string MailMessage { get; set; }
        public string MailAttachmentPath { get; set; }
        public string AttachmentName { get; set; }
        public List<string> StrCc { get; set; }
        public List<string> StrBcc { get; set; }
        public Stream SendMailAttachment { get; set; }

        #endregion
    }
}