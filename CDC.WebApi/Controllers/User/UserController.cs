﻿using CDC.Services;
using CDC.Services.Abstract;
using CDC.Services.Abstract.RazorEngine;
using CDC.Services.Abstract.User;
using CDC.ViewModel;
using CDC.ViewModel.Account;
using CDC.ViewModel.Common;
using CDC.ViewModel.Family;
using CDC.ViewModel.User;
using CDC.WebApi.Encryption;
using System;
using System.Configuration;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.User
{
    [Authorize]
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private readonly IUserService _iuserService;
        private readonly ICommonService _icommonservice;
        private readonly IRazorService _iRazorService;
        public UserController(IUserService iuserService, ICommonService icommonservice, IRazorService iRazorService)
        {
            _iuserService = iuserService;
            _icommonservice = icommonservice;
            _iRazorService = iRazorService;

        }
  [AllowAnonymous]
        [HttpPost]
        [Route("isexistname")]
        public bool IsExistName([FromBody] Test test)
        {

            return _iuserService.IsExistUserName(test.Name.Trim());
        }
        
        public class FileO
        {
            public long FileSize { get; set; }
        }
  
        [HttpPost]
        [Route("FileSize")]
        public bool FileSize([FromBody] FileO file)
        {
            return _icommonservice.FileSize(file.FileSize);
        }
        public class Test
        {
            public string Name { get; set; }
            public long AgencyId { get; set; }

        }

     
        [HttpPost]
        [Route("isexiststaffusername")]
        public bool IsExistStaffUsername([FromBody] StaffTest test)
        {

            return _icommonservice.IsExistStaffUsername(test.Name.Trim(),test.AgencyId,test.StaffId);
        }

        [HttpPost]
        [Route("isexistmobileNumber")]
        public bool IsExistMobileNumber([FromBody] Test test)
        {

            return _icommonservice.IsExistMobileNumber(test.Name.Trim(),test.AgencyId);
        }

        [HttpPost]
        [Route("isexistparentusername")]
        public bool IsExistParentUsername([FromBody] StaffTest test)
        {

            return _icommonservice.IsExistParentUsername(test.Name.Trim());
        }
    
        [HttpPost]
        [Route("isexistsecuritycode")]
        public bool IsExistSecurityCode([FromBody] Test test)
        {

            return _icommonservice.IsExistSecurityCode(test.Name.Trim());
            
        }
        public class StaffTest
        {
            public string Name { get; set; }
            public long AgencyId { get; set; }

            public long StaffId { get; set; }

        }

        [HttpPost]
        [Route("EnrolledCapacity")]
        public bool EnrolledCapacity(IDViewModel model)
        {

            return _iuserService.EnrolledCapacity(model);

        }
        //[AllowAnonymous]
        //[Route("validUsername")]
        //[HttpGet]
        //public async Task<IHttpActionResult> Get(string userName)
        //{
        //    try
        //    {
        //        if (userName == null)
        //        {
        //            return BadRequest("Invalid userName");
        //        }

        //        var result = await IsUsernameExists(userName);
        //        if (result)
        //        {
        //            return BadRequest("Username Exists");
        //        }
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest("An error ocuured.");
        //    }

        //}
        [AllowAnonymous]
        [HttpPost]
        [Route("GetToken")]
        public ResponseViewModel GetToken(LoginViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                if (ModelState.IsValid)
                {
                
                    string parentUrl = ConfigurationManager.AppSettings["ParentUrl"];

                    model.Password = EncryptDecrypt.GetEncryptedData(model.Password.Trim());
                    model.UserName = model.UserName.Trim();
                    response = _iuserService.FindUser(model.UserName, model.Password);

                    //This is only for parent login
                    if (model.Url.ToLower() == parentUrl.ToLower() && response.IsSuccess && response.Content != null)
                    {
                        
                        ParentInfoViewModel obj = (ParentInfoViewModel)response.Content;
                        if (obj.RoleName != "FAMILY")
                            response.Content = null;
                    }
                }
              
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Something went wrong.";
            }
            return response;
        }

        [Authorize]
        [HttpPost]
        [Route("GetUserDetails")]
        public ResponseViewModel GetUserDetails(LoginViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                if (ModelState.IsValid)
                {
                    string parentUrl = ConfigurationManager.AppSettings["ParentUrl"];
                    model.Password = EncryptDecrypt.GetEncryptedData(model.Password.Trim());
                    model.UserName = model.UserName.Trim();
                    response = _iuserService.FindUser(model.UserName, model.Password);
                    //This is only for parent login
                    if (model.Url.ToLower() == parentUrl.ToLower() && response.IsSuccess && response.Content!=null)
                    {
                        ParentInfoViewModel obj = (ParentInfoViewModel)response.Content;
                        if(obj.RoleName != "FAMILY")
                        response.Content = null;
                    }                    
                }
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Something went wrong.";
            }
            return response;
        }

        /// <summary>
        ///  Get user roleId by role name
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("GetRoleIdByRoleName")]
        public ResponseViewModel GetRoleIdByRoleName([FromBody] RolesViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                if (!string.IsNullOrEmpty(model.RoleName))
                    response = _iuserService.GetUserRoleId(model.RoleName);
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide role name";
                }

            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Something went wrong";
            }
            return response;
        }
        [Authorize]
        [HttpPost]
        [Route("GetRoleNameByRoleId")]
        public ResponseViewModel GetRoleNameByRoleId([FromBody] RolesViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                if (model.ID != 0)
                    response = _iuserService.GetUserRoleName(model.ID);
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide role id";
                }

            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Something went wrong";
            }
            return response;
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("ForgotPassword")]
        public ResponseViewModel ForgotPassword(HttpRequestMessage request, [FromBody]UserViewModel model)
        {
            var responseViewModel = new ResponseViewModel();
            try
            {
                if (ModelState.IsValid)
                {
                    Guid guid = Guid.NewGuid();
                    string Email = EncryptDecrypt.GetEncryptedData(model.EmailId.Trim());
                    string baseUrl = request.Headers.Referrer.AbsoluteUri.Replace("/forgot", "") + "/reset" + "/" + guid+"&"+model.EmailId;
                    Uri currentUrl = HttpContext.Current.Request.Url;
                    var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
                    string MailSubject = AppConstants.ForgotPassword;
                    var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.ForgotPassword, _icommonservice.GetMd5Hash(CDCEmailTemplates.ForgotPassword), new
                    {
                        Url = url,
                        usernameVal = model.EmailId,
                        resetPasswordLink = baseUrl,
                        TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                        Logo = url + ConfigurationManager.AppSettings["Logo"]
                    });

                        ForgotPasswordLogViewModel forgotPwd = new ForgotPasswordLogViewModel();
                        forgotPwd.Guid = guid;
                        forgotPwd.EmailId = model.EmailId;
                        ResponseInformation responseInfo;
                        _iuserService.AddForgotPasswordLog(forgotPwd, out responseInfo);
                        
                        if (responseInfo.ReturnStatus)
                        {

                            responseViewModel.Message = "Reset Password link has been sent to your Email ID successfully.";
                            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
                                    _icommonservice.SendEmail(model.EmailId, MailSubject, result);
                        
                        }
                        else
                        {
                            responseViewModel.Message = "Email does not exist.";
                            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
                        }
                }

                return responseViewModel;

            }
            catch (Exception)
            {
                responseViewModel.IsSuccess = false;
                responseViewModel.Message = "Something went wrong.";
                return responseViewModel;
            }
        }
        /// <summary>
        ///  Reset parent password on first login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("ResetParentPassword")]
        public ResponseViewModel ResetParentPassword(UserViewModel model)
        {

            var response = new ResponseViewModel();
            try
            {
                if(ModelState.IsValid)
                {
                    model.RoleId = 4;
                    model.Password = EncryptDecrypt.GetEncryptedData(model.Password.Trim());
                    response = _iuserService.ResetParentPassword(model);
                    response.IsSuccess = true;

                }
                
            }
            catch
            {
                response.IsSuccess = false;
                response.Message = "Something went wrong.";
            }
            return response;
        }

            [AllowAnonymous]
            [HttpPost]
            [Route("ResetPassword")]
        public ResponseViewModel ResetPassword(UserViewModel model)
        {

            var response = new ResponseViewModel();
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            var decryptPassword = model.Password;
            try
            {
                if (ModelState.IsValid)
                {
                    model.Guid = model.Guid;
                    model.Password = EncryptDecrypt.GetEncryptedData(model.Password.Trim());
                    response = _iuserService.ResetPassword(model);
                    if (response.IsSuccess)
                    {
                        string MailSubject = AppConstants.ResetPassword;
                        var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.ResetPasswordSuccess, _icommonservice.GetMd5Hash(CDCEmailTemplates.ResetPasswordSuccess), new
                        {
                            Url = url,
                            usernameVal = model.EmailId,
                            passwordVal = decryptPassword,
                            TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                            Logo = url + ConfigurationManager.AppSettings["Logo"]
                        });
                        _icommonservice.SendEmail(model.EmailId, MailSubject, result);
                    }
                }
            }
            catch (Exception)
            {
                response.IsSuccess = false;
                response.Message = "Something went wrong.";
            }
            return response;
        }
        
       
    }
}
