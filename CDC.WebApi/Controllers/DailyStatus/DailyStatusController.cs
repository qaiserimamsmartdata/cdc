﻿using CDC.Services.Abstract.DailyStatus;
using CDC.ViewModel.Common;
using CDC.ViewModel.DailyStatus;
using CDC.WebApi.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.DailyStatus
{
    
    [Authorize]
    [RoutePrefix("api/DailyStatus")]
    public class DailyStatusController : ApiController
    {
        private readonly IDailyStatusService _iDailyStatusService;

        public DailyStatusController(IDailyStatusService iDailyStatusService)
        {
            _iDailyStatusService = iDailyStatusService;
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("GetDailyStatusById")]        
        
        public HttpResponseMessage GetDailyStatusById(long DailyStatusId)
        {
            var dailyStatusList = _iDailyStatusService.GetDailyStatusById(DailyStatusId);
            var resultData = new { DailyStatusList = dailyStatusList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("GetAllDailyStatus")]
        public HttpResponseMessage GetAllDailyStatus([FromBody] SearchDailyStatusViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iDailyStatusService.GetAllDailyStatus(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("GetAllDailyStatusParticipants")]
        public HttpResponseMessage GetAllDailyStatusParticipants([FromBody] SearchDailyStatusViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iDailyStatusService.GetAllDailyStatusParticipants(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("AddDailyStatus")]
        public HttpResponseMessage AddDailyStatus([FromBody] DailyStatusViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation responseInfo;
            _iDailyStatusService.AddDailyStatus(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("UpdateDailyStatus")]
        public HttpResponseMessage UpdateDailyStatus(DailyStatusViewModel model)
        {
            ResponseInformation responseInfo;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            _iDailyStatusService.UpdateDailyStatus(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;

            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
                return response;
            }
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteDailyStatus")]
        public HttpResponseMessage DeleteDailyStatusById(long DailyStatusId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iDailyStatusService.DeleteDailyStatusById(DailyStatusId,out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
