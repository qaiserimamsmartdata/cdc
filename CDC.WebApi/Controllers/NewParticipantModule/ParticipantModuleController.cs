﻿using CDC.Entities.NewParent;
using CDC.Services;
using CDC.Services.Abstract;
using CDC.Services.Abstract.NewParticipantModule;
using CDC.Services.Abstract.RazorEngine;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.NewParent;
using CDC.ViewModel.NewParticipant;
using CDC.ViewModel.ParentPortal;
using CDC.WebApi.Encryption;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.NewParticipantModule
{
   
    [RoutePrefix("api/ParticipantModule")]
    public class ParticipantModuleController : ApiController
    {
        private readonly IParticipantModuleService _participantModuleService;
        private readonly IRazorService _iRazorService;
        private readonly ICommonService _icommonservice;

        public ParticipantModuleController(IParticipantModuleService participantModuleService, IRazorService iRazorService, ICommonService icommonservice)
        {
            _participantModuleService = participantModuleService;
            _iRazorService = iRazorService;
            _icommonservice = icommonservice;
        }
        [HttpPost]
        [Route("AddParticipantModule")]
        public HttpResponseMessage AddParticipantModule([FromBody] tblParticipantInfoViewModel model)
        {
            var responseData = _participantModuleService.RegisterParticipant(model);

            if(responseData.IsSuccess)
            {
                if(model.Radio == "newParent")
                {
                    Uri currentUrl = HttpContext.Current.Request.Url;
                    var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
                    string mailSubject = AppConstants.PrimaryParentRegistered;
                    var portalInfoLst = (List<ParentPortalViewModel>)(responseData.Content);
                    foreach (var item in portalInfoLst)
                    {
                        item.PortalPassword = EncryptDecrypt.GetDecryptedData(item.PortalPassword);
                        var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.PrimaryParentRegister, _icommonservice.GetMd5Hash(CDCEmailTemplates.PrimaryParentRegister), new
                        {
                            Url = url,
                            usernameVal = item.PortalEmail, 
                            passwordVal = item.PortalPassword,
                            TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                            Logo = url + ConfigurationManager.AppSettings["Logo"]
                        });
                        _icommonservice.SendEmail(item.PortalEmail, mailSubject, result);
                    }
                    
                }
                else if(model.Radio=="existing")
                {
                    var parentParticipant = (tblParentParticipantMapping)(responseData.Content);
                    List<long?> lstIds = new List<long?>();
                    lstIds.Add(parentParticipant.NewParticipantInfoID);
                    IDViewModel idVmModel = new IDViewModel
                    {
                        participantIds = lstIds
                    };
                    var parentList = _icommonservice.GetParentList(idVmModel);
                    var parentInfo = (List<tblParentInfoViewModel>)(parentList.Content);
                    foreach (var item in parentInfo)
                    {
                        Uri currentUrl = HttpContext.Current.Request.Url;
                        var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
                        string mailSubject = AppConstants.AssociatedParent;
                        //var portalInfoLst = (List<ParentPortalViewModel>)(responseData.Content);
                        //foreach (var item1 in portalInfoLst)
                        //{
                            //item1.PortalPassword = EncryptDecrypt.GetDecryptedData(item1.PortalPassword);
                            var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.AssociatedParent, _icommonservice.GetMd5Hash(CDCEmailTemplates.AssociatedParent), new
                            {
                                Url = url,
                                usernameVal = parentParticipant.NewParticipantInfo.FullName,
                                //passwordVal = item1.PortalPassword,
                                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                                Logo = url + ConfigurationManager.AppSettings["Logo"]
                            });
                            _icommonservice.SendEmail(item.EmailId, mailSubject, result);
                        //}
                    }
                  
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        [HttpPost]
        [Route("DeleteParticipant")]
        public HttpResponseMessage DeleteParticipant([FromBody] tblParticipantInfoViewModel model)
        {
            var resultData = _participantModuleService.DeleteParticipant(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData,"application/json");
        }
        [HttpPost]
        [Route("getAssociationType")]
        public HttpResponseMessage getAssociationType([FromBody]SearchFieldsViewModel model)
        {
            var resultData = _participantModuleService.getAssociationType(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("getAssociationParticipant")]
        public HttpResponseMessage getAssociationParticipant([FromBody]SearchFieldsViewModel model)
        {
            var resultData = _participantModuleService.getAssociationParticipant(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("getAssociationParents")]
        public HttpResponseMessage getAssociationParents(List<tblParentParticipantMappingViewModel> model)
        {
            var resultData = _participantModuleService.getAssociationParents(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("GetAllParticipant")]
        public HttpResponseMessage GetAllParticipant([FromBody] SearchParticipantViewModel model)
        {
            ResponseViewModel responseViewModel;
            _participantModuleService.GetAllParticipant(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [HttpPost]
        [Route("ParticipantParentDetails")]
        public HttpResponseMessage ParticipantParentDetails([FromBody] tblParticipantInfoViewModel model)
        {
           var resultData = _participantModuleService.ParticipantParentDetails(model);
           return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }


    }
}
