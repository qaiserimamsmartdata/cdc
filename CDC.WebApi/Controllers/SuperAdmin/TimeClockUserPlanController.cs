﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.ViewModel.Common;
using CDC.Services.Abstract;
using CDC.Services.Abstract.SuperAdmin;
using CDC.Entities.TimeClockUsers;
using Stripe;
using System;
using System.Configuration;

namespace CDC.WebApi.Controllers.SuperAdmin
{

    [Authorize(Roles = "SuperAdmin")]
    [RoutePrefix("api/TimeClockUserPlan")]
    public class TimeClockUserPlanController : ApiController
    {
        private readonly ITimeClockUserPlanService _iTimeClockUserPlanService;
        private readonly ICommonService _icommonservice;



        public TimeClockUserPlanController(ITimeClockUserPlanService iTimeClockUserPlanService,
                             ICommonService icommonservice)
        {
            _iTimeClockUserPlanService = iTimeClockUserPlanService;
            _icommonservice = icommonservice;
        }


        /// <summary>
        /// Gets all TimeClockUser Plan.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllTimeClockUserPlan")]
        public HttpResponseMessage GetAllTimeClockUserPlan([FromBody] SearchTimeClockUserPlanViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iTimeClockUserPlanService.GetAllTimeClockUserPlan(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        private static StripePlan CreateStripePlan(TimeClockUsersViewModel plan)
        {
            //setup plan
            StripePlanCreateOptions planOption = new StripePlanCreateOptions();
            planOption.Id = "TimeClock_" + plan.Name;
            planOption.Interval = "month";
            planOption.Currency = "usd";
            planOption.Amount = Int32.Parse(plan.Price.ToString()) * 100;
            planOption.Name = "TimeClock_"+plan.Name;

            //Create plan
            StripePlanService planSVc = new StripePlanService(ConfigurationManager.AppSettings["StripePrivateKey"]);
            return planSVc.Create(planOption);
        }


        [HttpPost]
        [Route("AddUpdateTimeClockUserPlan")]
        public HttpResponseMessage AddUpdateTimeClockUserPlan([FromBody] TimeClockUsersViewModel model)
        {
            try
            {
                StripePlan stripePlan;
                if (model.ID > 0 && !string.IsNullOrEmpty(model.StripePlanId))
                {
                    stripePlan = UpdateStripePlan(model);
                    if (stripePlan != null)
                    {
                        model.StripePlanId = stripePlan.Id;
                    }
                }
                else
                {
                    stripePlan = CreateStripePlan(model);
                    if (stripePlan != null)
                    {
                        model.StripePlanId = stripePlan.Id;
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            var response = new ResponseViewModel();
            var returnData = _iTimeClockUserPlanService.AddUpdateTimeClockUserPlan(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already a plan with same name.Please add another.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        private static StripePlan UpdateStripePlan(TimeClockUsersViewModel plan)
        {
            //update plan
            StripePlanUpdateOptions planOption = new StripePlanUpdateOptions {Name = "TimeClock_" + plan.Name};
            StripePlanService planSVc = new StripePlanService(ConfigurationManager.AppSettings["StripePrivateKey"]);
            return planSVc.Update(plan.StripePlanId, planOption);
        }
        /// <summary>
        ///     Deletes TimeClockUserPlan by identifier.
        /// </summary>
        /// <param name="timeclockuserplanId">The TimeClockUserPlan identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteTimeClockUserPlan")]
        public HttpResponseMessage DeleteTimeClockUserPlan(long timeclockuserplanId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iTimeClockUserPlanService.DeleteTimeClockUserById(timeclockuserplanId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

    }
}
