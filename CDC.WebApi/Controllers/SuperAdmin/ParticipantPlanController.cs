﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.ViewModel.Common;
using CDC.Services.Abstract.SuperAdmin;
using CDC.Entities.Pricing;
using Stripe;
using System.Configuration;
using System;

namespace CDC.WebApi.Controllers.SuperAdmin
{
    [Authorize(Roles = "SuperAdmin")]
    [RoutePrefix("api/ParticipantPlan")]
    public class ParticipantPlanController : ApiController
    {
        private readonly IParticipantPlanService _iParticipantPlanService;

        public ParticipantPlanController(IParticipantPlanService iparticipantplanservice)
        {
            _iParticipantPlanService = iparticipantplanservice;
        }

        /// <summary>
        /// Gets all ParticipantPlan.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
    
        [HttpPost]
        [Route("GetAllParticipantPlan")]
        public HttpResponseMessage GetAllParticipantPlan([FromBody] SearchParticipantPlanViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iParticipantPlanService.GetAllParticipantPlan(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }




        /// <summary>
        ///     Deletes ParticipantPlan by identifier.
        /// </summary>
        /// <param name="participantplanId">The ParticipantPlan identifier.</param>
        /// <returns></returns>
       
        [HttpPost]
        [Route("DeleteParticipantPlan")]
        public HttpResponseMessage DeleteParticipantPlanById(long participantplanId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iParticipantPlanService.DeleteParticipantPlanById(participantplanId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

        
        [HttpPost]
        [Route("AddUpdateParticipantPlan")]
        public HttpResponseMessage AddUpdateParticipantPlan([FromBody] PricingPlanViewModel model)
        {
            var response = new ResponseViewModel();
            try
            {
                StripePlan stripePlan;
                if (model.ID > 0)
                {
                    stripePlan = UpdateStripePlan(model);
                    if (stripePlan != null)
                    {
                        model.StripePlanId = stripePlan.Id;
                    }
                }
                else
                {
                    stripePlan = CreateStripePlan(model);
                    if (stripePlan != null)
                    {
                        model.StripePlanId = stripePlan.Id;
                    }
                }
                var returnData = _iParticipantPlanService.AddUpdateParticipantPlan(model);

                response.IsSuccess = returnData == 1;
                if (returnData == 3)
                {
                    response.IsSuccess = false;
                    response.Message = "ERROR! There is already a plan with same name.Please add another.";
                }
                else if (returnData == 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Error! Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        private static StripePlan CreateStripePlan(PricingPlanViewModel plan)
        {
            //setup plan
            StripePlanCreateOptions planOption = new StripePlanCreateOptions();
            if (plan.IsTrial==null?false: plan.IsTrial.Value)
            {
                planOption.Id = "Agency_" + plan.Name;
                planOption.TrialPeriodDays = 1;
                planOption.Interval = "day";
                //planOption.TrialPeriodDays = 30;
                //planOption.Interval = "month";
                planOption.Amount = 0;
                planOption.Currency = "usd";
                planOption.Name = plan.Name;
            }
            else
            {
                planOption.Id = "Agency_" + plan.Name;
                planOption.Interval = "month";
                planOption.Currency = "usd";
                planOption.Amount = Int32.Parse(plan.Price.ToString()) * 100;
                planOption.Name = plan.Name;
            }
            //Create plan
            StripePlanService planSVc = new StripePlanService(ConfigurationManager.AppSettings["StripePrivateKey"]);
            return planSVc.Create(planOption);
        }
        private static StripePlan UpdateStripePlan(PricingPlanViewModel plan)
        {
            //update plan
            StripePlanUpdateOptions planOption = new StripePlanUpdateOptions {Name = plan.Name};
            StripePlanService planSVc = new StripePlanService(ConfigurationManager.AppSettings["StripePrivateKey"]);
            return planSVc.Update(plan.StripePlanId,planOption);
        }

        /* private StripePlan UpdateStripePlan(SubscriptionPlan plan, string planId)
         {
             StripePlanService strpPlanSvc = new StripePlanService(ConfigurationManager.AppSettings["MS_WebHookReceiverSecret_Stripe"]);
             StripePlanUpdateOptions updatePlan = new StripePlanUpdateOptions();
             updatePlan.Name = plan.PlanName;
             return strpPlanSvc.Update(planId, updatePlan);
         }*/
    }
}
