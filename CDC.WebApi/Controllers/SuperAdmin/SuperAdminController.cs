﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.ViewModel.Common;
using CDC.Services.Abstract.SuperAdmin;

namespace CDC.WebApi.Controllers.SuperAdmin
{
    [Authorize(Roles = "SuperAdmin")]
    [RoutePrefix("api/SuperAdmin")]
    public class SuperAdminController : ApiController
    {
        private readonly ISuperAdminService _iSuperAdminService;

        public SuperAdminController(ISuperAdminService isuperadminservice)
        {
            _iSuperAdminService = isuperadminservice;
        }


        /// <summary>
        /// Gets all Agency.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllAgency")]
        public HttpResponseMessage GetAllAgency([FromBody] SearchAgencyViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iSuperAdminService.GetAllAgency(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        /// <summary>
        ///     Deletes Agency by identifier.
        /// </summary>
        /// <param name="agencyId">The Agency identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteAgency")]
        public HttpResponseMessage DeleteAgencyById(long agencyId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iSuperAdminService.DeleteAgencyById(agencyId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
