﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.ViewModel.Common;
using CDC.Services.Abstract;
using CDC.Services.Abstract.SuperAdmin;

namespace CDC.WebApi.Controllers.SuperAdmin
{
    [Authorize(Roles = "SuperAdmin,Agency")]
    [RoutePrefix("api/PaymentHistory")]
    public class PaymentHistoryController : ApiController
    {
        private readonly IPaymentHistoryService _iPaymentHistoryService;
        private readonly ICommonService _icommonservice;

        public PaymentHistoryController(IPaymentHistoryService iPaymentHistoryService,
                             ICommonService icommonservice)
        {
            _iPaymentHistoryService = iPaymentHistoryService;
            _icommonservice = icommonservice;
        }


        /// <summary>
        /// Gets all Agency Payment History.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllAgencyPaymentHistory")]
        public HttpResponseMessage GetAllAgencyPaymentHistory([FromBody] SearchPaymentHistoryViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iPaymentHistoryService.GetAllAgencyPaymentHistory(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

    }
}
