﻿using CDC.Data;
using CDC.ViewModel.SuperAdmin;
using Microsoft.AspNet.WebHooks;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
//using Microsoft.AspNet.WebHooks;
namespace CDC.WebApi.Controllers.SuperAdmin
{
    public class StripeWebHookHandler : WebHookHandler  //private readonly IMembershipService _memberShipService;
    {
        public StripeWebHookHandler()
        {
           this.Receiver = StripeWebHookReceiver.ReceiverName;
         
        }

        public override Task ExecuteAsync(string generator, WebHookHandlerContext context)
        {
            //UserPlanMapping mapData = new UserPlanMapping();
            try
            {
                // 'https://stripe.com/docs/webhooks'
                Microsoft.AspNet.WebHooks.StripeEvent entry = context.GetDataOrDefault<Microsoft.AspNet.WebHooks.StripeEvent>();
                if (entry != null)
                {
                    var Data = JsonConvert.SerializeObject(entry.Data);
                    var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<StripeEventDataViewModel>(Data);
                    if (obj != null && obj.@object.status == "active")
                    {
                        using (CDCContext db = new CDCContext())
                        {
                            var OrgData = db.AgencyRegistrationInfoSet.Where(a => a.StripeUserId == obj.@object.customer).FirstOrDefault();
                            if (OrgData != null)
                            {
                                var planDetail = db.UserPlanMapping.Where(a => a.OrganizationID == OrgData.ID).ToList().LastOrDefault();
                                if (planDetail != null)
                                {
                                    planDetail.IsFinal = false;

                                    db.SaveChanges();
                                }
                                mapData.CreatedBy = OrgData.ID;
                                mapData.OrganizationID = OrgData.ID;
                                mapData.IsSelected = true;
                                //mapData.CreatedDate = DateTime.UtcNow;
                                mapData.CreatedDate = UnixTimeStampToDateTime(obj.@object.current_period_start);
                                mapData.PlanExpiryDate = UnixTimeStampToDateTime(obj.@object.current_period_end);
                                mapData.IsDeleted = false;
                                mapData.IsFinal = true;
                                mapData.StripeSubscriptionId = obj.@object.id;
                                var planData = db.SubscriptionPlans.Where(a => a.StripePlanId == obj.@object.plan.id).ToList().FirstOrDefault();
                                if (planData != null)
                                {
                                    mapData.PlanAmount = planData.PlanAmount;
                                    mapData.PlanId = planData.ID;
                                    mapData.PlanName = planData.PlanName;
                                }
                                //db.UserPlanMapping.Add(mapData);
                                //db.SaveChanges();
                            }
                        }
                        // status = _superAdminService.SaveStripeEventata(obj);
                    }

                }

                Trace.WriteLine(entry.ToString());



                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}