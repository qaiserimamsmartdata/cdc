﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Dashboard;
using CDC.ViewModel.Common;

namespace CDC.WebApi.Controllers.Dashboard
{
  [Authorize]
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        private readonly ICommonService _icommonservive;
        private readonly IDashboardService _idashboardservive;
        private readonly IStaffDashboardService _istaffdashboardservive;
        private readonly ISuperAdminDashboardService _iSuperAdminDashboardService;
        private readonly IParentDashboardService _iParentDashboardService;

        public DashboardController(ICommonService iCommonservice, IDashboardService idashboardservive,IStaffDashboardService istaffdashboardservive,ISuperAdminDashboardService iSuperAdminDashboardService,IParentDashboardService iParentDashboardService)
        {
            _icommonservive = iCommonservice;
            _idashboardservive = idashboardservive;
            _istaffdashboardservive = istaffdashboardservive;
            _iSuperAdminDashboardService = iSuperAdminDashboardService;
            _iParentDashboardService = iParentDashboardService;
        }

        [HttpPost]
        [Route("getPricingPlanCount")]
        public HttpResponseMessage GetPricingPlanCount(SearchFieldsViewModel model)
        {
            var response = _icommonservive.GetPricingPlanCount(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("getAllCount")]
        public HttpResponseMessage getAllCount([FromBody]IDViewModel model)
        {
            ResponseViewModel responseViewModel;
            _idashboardservive.GetAllCountsById(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Staff")]
        [HttpPost]
        [Route("getStaffDashboardData")]
        public HttpResponseMessage getStaffDashboardData([FromBody]IDViewModel model)

        {
            ResponseViewModel responseViewModel;
            _istaffdashboardservive.GetAllCountsById(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost]
        [Route("getSuperAdminDashboardData")]
        public HttpResponseMessage getSuperAdminDashboardData([FromBody]IDViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iSuperAdminDashboardService.GetAllCountsById(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Family")]
        [HttpPost]
        [Route("getParentLoginDashboardData")]
        public HttpResponseMessage getParentLoginDashboardData([FromBody]IDViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iParentDashboardService.GetAllCountsById(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
    }
}