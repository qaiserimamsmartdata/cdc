﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.ToDoTask;
using CDC.ViewModel.Common;
using CDC.ViewModel.ToDoTask;

namespace CDC.WebApi.Controllers.ToDoTask
{

    [Authorize(Roles ="Agency")]
    [RoutePrefix("api/ToDo")]
    public class ToDoController : ApiController
    {
        private readonly IToDoService _iToDoService;

        public ToDoController(IToDoService iToDoService)
        {
            _iToDoService = iToDoService;
        }


        /// <summary>
        /// Gets all ToDo
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllToDo")]
        public HttpResponseMessage GetAllToDo([FromBody] SearchToDoViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iToDoService.GetAllToDo(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


        [HttpPost]
        [Route("AddUpdateToDo")]
        public HttpResponseMessage AddUpdateToDo([FromBody] ToDoViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iToDoService.AddUpdateToDo(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already a plan with same name.Please add another.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }



        /// <summary>
        ///     Deletes ToDo by identifier.
        /// </summary>
        /// <param name="toDoId">The toDoId identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteToDo")]
        public HttpResponseMessage DeleteToDo(long toDoId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iToDoService.DeleteToDoById(toDoId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
