﻿using CDC.Services.Abstract.ToDoTask;
using CDC.ViewModel.Common;
using CDC.ViewModel.ToDoTask;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.ToDoTask
{

    [Authorize]
    [RoutePrefix("api/Priority")]
    public class PriorityController : ApiController
    {

        private readonly IPriorityService _ipriorityservice;
        public PriorityController(IPriorityService ipriorityservice)
        {
            _ipriorityservice = ipriorityservice;
        }



        /// <summary>
        /// Add/Update Priority
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddUpdatePriority")]
        public HttpResponseMessage AddUpdatePriority([FromBody] PriorityViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _ipriorityservice.AddUpdatePriority(model);
            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }



        /// <summary>
        /// Gets all Priority
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllPriority")]
        public HttpResponseMessage GetAllPriority([FromBody] SearchPriorityViewModel model)
        {
            ResponseViewModel responseViewModel;
            _ipriorityservice.GetAllPriority(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        /// <summary>
        /// Delete Priority
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeletePriorityById")]
        public HttpResponseMessage DeletePriorityById([FromBody]PriorityViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _ipriorityservice.DeletePriority(model);
            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

    }
}
