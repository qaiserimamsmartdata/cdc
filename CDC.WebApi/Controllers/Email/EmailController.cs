﻿using CDC.Services;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Email;
using CDC.Services.Abstract.RazorEngine;
using CDC.ViewModel.Common;
using CDC.ViewModel.messages;
using CDC.WebApi.Helpers;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Email
{
    /// <summary>
    /// The Email Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [Authorize]
    [RoutePrefix("api/Email")]
    public class EmailController : ApiController
    {
        private readonly IEmailService _iemailservice;
        private readonly ICommonService _icommonservice;
        private readonly IRazorService _iRazorService;
        public EmailController(IEmailService iemailservice, ICommonService icommonservice, IRazorService iRazorService)
        {
            _iemailservice = iemailservice;
            _icommonservice = icommonservice;
            _iRazorService = iRazorService;
        }
        [HttpPost]
        [Route("Inbox")]
        public HttpResponseMessage GetAllMail([FromBody]SearchMessageViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iemailservice.GetAllMail(out responseViewModel, model);
            //var a = Json(responseViewModel.Content);
            var c = Json(new[] { responseViewModel.Content });
            var d = JsonConvert.SerializeObject(responseViewModel.Content);

            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel.Content, "application/json");
        }

        [HttpPost]
        [Route("Sent")]
        public HttpResponseMessage GetAllSentMail([FromBody]SearchMessageViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iemailservice.GetAllSentMail(out responseViewModel, model);
            //var a = Json(responseViewModel.Content);
            var c = Json(new[] { responseViewModel.Content });
            var d = JsonConvert.SerializeObject(responseViewModel.Content);

            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel.Content, "application/json");
        }

        [HttpPost]
        [Route("Contacts")]
        public HttpResponseMessage GetAllContacts([FromBody]SearchMessageViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iemailservice.GetAllContacts(out responseViewModel, model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel.Content, "application/json");
        }



        [HttpPost]
        [Route("GetAgencyByFamilyId")]
        public HttpResponseMessage GetAgencyByFamilyId([FromBody]SearchUserViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                return Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            }

            _iemailservice.GetAgencyByFamilyId(out responseViewModel, model);

            if (responseViewModel.IsSuccess == true)
            {
                return Request.CreateResponse(HttpStatusCode.OK, responseViewModel.Content);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel.Content);
        }

        [HttpPost]
        [Route("ComposeMessage")]
        public HttpResponseMessage ComposeMessage([FromBody]messageViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                return Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            }
            ResponseViewModel responseInfo;
            _iemailservice.ComposeMessage(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            string mailSubject = "Message from " + model.@from.name + ": " + model.subject;
            string helloMsg = "";
            foreach (var item in model.to)
            {
                helloMsg = "Hello " + item.name + ",";
            }
            var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.ComposeMessage, _icommonservice.GetMd5Hash(CDCEmailTemplates.ComposeMessage), new
            {
                Url = url,
                content = model.content,
                Regards = "Regards,<br/>" + model.@from.name,
                Hello = helloMsg,
                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                Logo = url + ConfigurationManager.AppSettings["Logo"]
            });

            foreach (var item in model.to)
            {

                _icommonservice.SendEmail(item.email, mailSubject, result);
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;

        }

        [HttpPost]
        [Route("DeleteInboxMessage")]
        public HttpResponseMessage DeleteInboxMessage([FromBody]messageViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                return Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            }
            ResponseInformation responseInfo;
            _iemailservice.DeleteInboxMessage(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }

        [HttpPost]
        [Route("DeleteSentMessage")]
        public HttpResponseMessage DeleteSentMessage([FromBody]messageViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                return Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            }
            ResponseInformation responseInfo;
            _iemailservice.DeleteSentMessage(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }

        [HttpPost]
        [Route("updateUnreadCount")]
        public HttpResponseMessage updateUnreadCount([FromBody]messageViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                return Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            }
            ResponseInformation responseInfo;
            _iemailservice.updateUnreadCount(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }

        [HttpPost]
        [Route("GetUnreadCount")]
        public HttpResponseMessage GetUnreadCount([FromBody]SearchMessageViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iemailservice.GetUnreadCount(out responseViewModel, model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel.Content, "application/json");
        }

    }
}