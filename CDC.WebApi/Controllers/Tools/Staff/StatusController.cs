﻿using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Staff
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/Status")]
    public class StatusController : ApiController
    {
        private readonly IStatusServices _istatusservice;
        public StatusController(IStatusServices istatusservice)
        {
            _istatusservice = istatusservice;
        }
        [HttpPost]
        [Route("AddUpdateStatus")]
        public HttpResponseMessage AddUpdateStatus([FromBody] StatusViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _istatusservice.AddUpdateStatus(model);
            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpGet]
        [Route("GetStatus")]
        public HttpResponseMessage GetStatus()
        {
            var status = _istatusservice.GetStatus();
            var resultData = new { status = status.Select(m => new { m.ID, m.Name }) };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("DeleteStatusById")]
        public HttpResponseMessage DeleteStatusById(long statusId)
        {
            var response = new ResponseViewModel();

            var returnData = _istatusservice.DeleteStatusById(statusId);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
    }
}
