﻿using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;
using CDC.WebApi.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Staff
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/PayType")]
    public class PayTypeController : ApiController
    {
        private readonly IPayTypeService _iPayTypeService;
        public PayTypeController(IPayTypeService iPayTypeService)
        {
            _iPayTypeService = iPayTypeService;
        }
        [HttpPost]
        [Route("AddUpdatePayType")]
        public HttpResponseMessage AddUpdatePayType([FromBody] PayTypeViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation transaction;
            _iPayTypeService.AddUpdatePayType(model, out transaction);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = transaction.ReturnStatus;
            responseViewModel.ReturnMessage = transaction.ReturnMessage;
            responseViewModel.ValidationErrors = transaction.ValidationErrors;
            responseViewModel.Id = transaction.ID;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }
        [HttpGet]
        [Route("GetPayType")]
        public HttpResponseMessage GetPayType()
        {
            ResponseViewModel responceViewModel;
            _iPayTypeService.GetPayType(out responceViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responceViewModel, "application/json");

        }
        [HttpPost]
        [Route("DeletePayTypeById")]
        public HttpResponseMessage DeletePayTypeById(long payTypeId)
        {
            ResponseInformation transaction;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iPayTypeService.DeletePayTypeById(payTypeId, out transaction);
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
