﻿using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Staff
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/LeaveType")]
    public class LeaveTypeController : ApiController
    {
        private readonly ILeaveTypeService _ileavetypeservice;
        public LeaveTypeController(ILeaveTypeService ileavetypeservice)
        {
            _ileavetypeservice = ileavetypeservice;
        }
        [HttpPost]
        [Route("AddUpdateLeaveType")]
        public HttpResponseMessage AddUpdateLeaveType([FromBody] LeaveTypeViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _ileavetypeservice.AddUpdateLeaveType(model);
            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpGet]
        [Route("GetLeaveType")]
        public HttpResponseMessage GetLeaveType()
        {
            var leaveType = _ileavetypeservice.GetLeaveType();
            var resultData = new { leaveType = leaveType.Select(m => new { m.ID, m.Name }) };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("DeleteLeaveTypeById")]
        public HttpResponseMessage DeleteLeaveTypeById(long leaveTypeId)
        {
            var response = new ResponseViewModel();

            var returnData = _ileavetypeservice.DeleteLeaveTypeById(leaveTypeId);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
    }
}
