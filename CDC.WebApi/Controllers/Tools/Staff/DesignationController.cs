﻿using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Staff;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Staff
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/Designation")]
    public class DesignationController : ApiController
    {
        private readonly IDesignationService _idesignationservice;
        public DesignationController(IDesignationService idesignationservice)
        {
            _idesignationservice = idesignationservice;
        }
        [HttpPost]
        [Route("AddUpdateDesignation")]
        public HttpResponseMessage AddUpdateDesignation([FromBody] PositionViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _idesignationservice.AddUpdateDesignation(model);
            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpPost]
        [Route("GetDesignation")]
        public HttpResponseMessage GetDesignation([FromBody]SearchFieldsViewModel model)
        {

            var designation = _idesignationservice.GetDesignation(model);
            var resultData = new { designation = designation.Select(m => new { m.ID, m.Name,m.AgencyId }) };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }


        /// <summary>
        /// Gets all Position
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllDesignation")]
        public HttpResponseMessage GetAllDesignation([FromBody] SearchPositionViewModel model)
        {
            ResponseViewModel responseViewModel;
            _idesignationservice.GetAllDesignation(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


        [HttpPost]
        [Route("DeleteDesignationById")]
        public HttpResponseMessage DeleteDesignationById([FromBody]PositionViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _idesignationservice.DeleteDesignationById(model);
            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
  
    }
}
