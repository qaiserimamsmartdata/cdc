﻿using CDC.Services.Abstract.Tools.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Attendance;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Attendance
{

    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/ModeOfConvenience")]
    public class ModeOfConvenienceController : ApiController
    {
        private readonly IModeOfConvenienceService _imodeOfConvenienceservice;
        public ModeOfConvenienceController(IModeOfConvenienceService imodeOfConvenienceservice)
        {
            _imodeOfConvenienceservice = imodeOfConvenienceservice;
        }
        [HttpPost]
        [Route("AddUpdateModeOfConvenience")]
        public HttpResponseMessage AddUpdatemodeOfConvenience([FromBody] ModeOfConvenienceViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _imodeOfConvenienceservice.AddUpdateModeOfConvenience(model);
            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpPost]
        [Route("GetModeOfConvenience")]
        public HttpResponseMessage GetmodeOfConvenience([FromBody]SearchFieldsViewModel model)
        {

            var modeOfConvenience = _imodeOfConvenienceservice.GetModeOfConvenience(model);
            var resultData = new { modeOfConvenience = modeOfConvenience.Select(m => new { m.ID, m.Name,m.AgencyId }) };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }


        /// <summary>
        /// Gets all 
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllModeOfConvenience")]
        public HttpResponseMessage GetAllmodeOfConvenience([FromBody] SearchModeOfConvenienceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _imodeOfConvenienceservice.GetAllModeOfConvenience(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


        [HttpPost]
        [Route("DeleteModeOfConvenienceById")]
        public HttpResponseMessage DeletemodeOfConvenienceById([FromBody]ModeOfConvenienceViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _imodeOfConvenienceservice.DeleteModeOfConvenienceById(model);
            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
  
    }
}
