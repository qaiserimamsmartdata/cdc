﻿using CDC.Services.Abstract.Tools.Student;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Student
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/DropReason")]
    public class DropReasonController : ApiController
    {
        private readonly IDropReasonService _idropreasonservice;

        public DropReasonController(IDropReasonService idropreasonservice)
        {
            _idropreasonservice = idropreasonservice;
        }

        [HttpPost]
        [Route("AddUpdateDropReason")]
        public HttpResponseMessage AddUpdateDropReason([FromBody] DropReasonViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _idropreasonservice.AddUpdateDropReason(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already a drop reason with same name.Please add another one.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetDropReason")]
        public HttpResponseMessage GetDropReason([FromBody] SearchFieldsViewModel model)
        {
            var dropReason = _idropreasonservice.GetDropReason(model);
            var resultData = new { dropReason = dropReason.Select(m => new { m.ID, m.Name }) };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("DeleteDropReasonById")]
        public HttpResponseMessage DeleteDropReasonById([FromBody]DropReasonViewModel model)
        {
            var response = new ResponseViewModel();

            var returnData = _idropreasonservice.DeleteDropReasonById(model);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
    }
}
