﻿using CDC.Services.Abstract.Tools.Student;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Student
{
    [Authorize]
    [RoutePrefix("api/GeneralDroppedBy")]
    public class GeneralDroppedByController : ApiController
    {
        private readonly IGeneralDroppedByService _iGeneralDroppedByService;
        public GeneralDroppedByController(IGeneralDroppedByService iGeneralDroppedByService)
        {
            _iGeneralDroppedByService = iGeneralDroppedByService;
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("GetGeneralDroppedBy")]
        public HttpResponseMessage GetGeneralDroppedBy([FromBody]IDViewModel model)
        {
            var responseData = _iGeneralDroppedByService.GetGeneralDroppedBy(model.ID);
            return Request.CreateResponse(HttpStatusCode.OK, responseData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("AddUpdateGeneralDroppedBy")]
        public HttpResponseMessage AddUpdateGeneralDroppedBy([FromBody] GeneralDroppedByViewModel model)
        {
            var responseData = _iGeneralDroppedByService.AddUpdateGeneralDroppedBy(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteGeneralDroppedBy")]
        public HttpResponseMessage DeleteGeneralDroppedBy([FromBody] GeneralDroppedByViewModel model)
        {
            var responseData = _iGeneralDroppedByService.DeleteGeneralDroppedBy(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("GetAllGeneralDroppedBy")]
        public HttpResponseMessage GetAllGeneralDroppedBy([FromBody] SearchGeneralDroppedByViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iGeneralDroppedByService.GetAll(model,out  responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
    }
}
