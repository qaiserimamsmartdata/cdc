﻿using CDC.Services.Abstract.Tools.Student;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Student
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/EnrollType")]
    public class EnrollTypeController : ApiController
    {
        private readonly IEnrollTypeService _ienrolltypeservice;

        public EnrollTypeController(IEnrollTypeService ienrolltypeservice)
        {
            _ienrolltypeservice = ienrolltypeservice;
        }

        [HttpPost]
        [Route("AddUpdateEnrollType")]
        public HttpResponseMessage AddUpdateEnrollType([FromBody] EnrollTypeViewModel model)
        {  
            var response = new ResponseViewModel();
            var returnData = _ienrolltypeservice.AddUpdateEnrollType(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already a enroll type with same name.Please add another one.";
            }
            else if(returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetEnrollType")]
        public HttpResponseMessage GetEnrollType([FromBody] EnrollTypeViewModel model)
        {
            var enrollType = _ienrolltypeservice.GetEnrollType(model);
            var resultData = new { enrollType = enrollType.Select(m => new { m.ID, m.Name }) };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("DeleteEnrollTypeById")]
        public HttpResponseMessage DeleteEnrollTypeById([FromBody]EnrollTypeViewModel model)
        {
            var response = new ResponseViewModel();

            var returnData = _ienrolltypeservice.DeleteEnrollTypeById(model);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
    }
}
