﻿using CDC.Services.Abstract;
using CDC.Services.Abstract.Tools.Student;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Student;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Student
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/Tools/Student")]
    public class FeeTypeController : ApiController
    {
        private readonly IFeeTypeService _feetypeservice;
        ResponseViewModel responseViewModel = new ResponseViewModel();

        public FeeTypeController(IFeeTypeService feetypeservice, ICommonService icommonservice)
        {
            _feetypeservice = feetypeservice;
        }

        [HttpPost]
        [Route("GetFeeTypeById")]
        public HttpResponseMessage GetFeeTypeById(long Id)
        {
            var feeTypeList = _feetypeservice.GetFeeTypeById(Id);
            var resultData = new { FeeTypeList = feeTypeList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("GetAllFeeTypes")]
        public HttpResponseMessage GetAllFeeTypes([FromBody] FeeTypeViewModel model)
        {
            model.AgencyId = 1;
            _feetypeservice.GetAllFeeTypes(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        [HttpPost]
        [Route("AddUpdateFeeTypes")]
        public HttpResponseMessage AddUpdateFeeTypes([FromBody] FeeTypeViewModel model)
        {
            var eventRegister = _feetypeservice.AddUpdateFeeTypes(model);
            if (eventRegister.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, eventRegister, "application/json");
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, eventRegister, "application/json");
        }

        [HttpPost]
        [Route("DeleteFeeTypes")]
        public HttpResponseMessage DeleteFeeTypes(long Id, out ResponseInformation responseInfo)
        {
            _feetypeservice.DeleteFeeTypes(Id, out responseInfo);
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }
    }
}