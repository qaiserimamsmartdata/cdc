﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.Tools.Category;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Category;

namespace CDC.WebApi.Controllers.Tools.Category
{
    /// <summary>
    ///     The Category Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    //[Authorize(Roles = "Agency")]
    [RoutePrefix("api/Category")]
    public class CategoryController : ApiController
    {
        private readonly ICategoryService _icategoryservice;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryController" /> class.
        /// </summary>
        /// <param name="icategoryservice">The Category service.</param>
        public CategoryController(ICategoryService icategoryservice)
        {
            _icategoryservice = icategoryservice;
        }

        /// <summary>
        ///     Adds the category.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddCategory")]
        public HttpResponseMessage AddCategory([FromBody] CategoryViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _icategoryservice.AddCategory(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "Category already exists.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        ///     Gets the category.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCategory")]
        public HttpResponseMessage GetCategory(SearchFieldsViewModel model)

        {
            var category = _icategoryservice.GetCategory(model);
            var resultData = new {category = category.Select(m => new {m.ID, m.Name})};
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        /// <summary>
        ///     Updates the category.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateCategory")]
        public HttpResponseMessage UpdateCategory(CategoryViewModel model)
        {
            var response = new ResponseViewModel();

            var returnData = _icategoryservice.UpdateCategory(model, model.ID);

            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "Category is already exist!";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        ///     Deletes the category by identifier.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteCategoryById")]
        public HttpResponseMessage DeleteCategoryById(long categoryId)
        {
            var response = new ResponseViewModel();

            var returnData = _icategoryservice.DeleteCategoryById(categoryId);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        /// Gets all Category
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllCategory")]

        public HttpResponseMessage GetAllCategory([FromBody] SearchCategoryListViewModel model)
        {
            ResponseViewModel responseViewModel;
            _icategoryservice.GetAllCategory(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

    }
}