﻿using CDC.Services.Abstract.Tools.Enhancements;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Enhancements;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Enhancements
{
    [Authorize(Roles = "SuperAdmin")]
    [RoutePrefix("api/Enhancement")]
    public class EnhancementController : ApiController
    {
        private readonly IEnhancementService _iEnhancementService;

        public EnhancementController(IEnhancementService iEnhancementService)
        {
            _iEnhancementService = iEnhancementService;
        }

        /// <summary>
        /// Gets all Enhancement
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllEnhancement")]
        public HttpResponseMessage GetAllEnhancement([FromBody] SearchEnhancementViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iEnhancementService.GetAllEnhancement(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


        [HttpPost]
        [Route("AddUpdateEnhancement")]
        public HttpResponseMessage AddUpdateEnhancement([FromBody] EnhancementViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iEnhancementService.AddUpdateEnhancement(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already with same name.Please add another.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        ///     Deletes Enhancement by identifier.
        /// </summary>
        /// <param name="enhancementId">The enhancementId identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteEnhancement")]
        public HttpResponseMessage DeleteEnhancement(long enhancementId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iEnhancementService.DeleteEnhancementById(enhancementId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
                return response;
            }
        }


    }
}
