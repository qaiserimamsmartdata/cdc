﻿using CDC.Services.Abstract.Tools.ManageHoliday;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.ManageHoliday;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.ManageHoliday
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/Holiday")]
    public class HolidayController : ApiController
    {
        private readonly IHolidayService _iHolidayService;

        public HolidayController(IHolidayService iHolidayService)
        {
            _iHolidayService = iHolidayService;
        }


        /// <summary>
        /// Gets all Holiday
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllHoliday")]
        public HttpResponseMessage GetAllHoliday([FromBody] SearchHolidayViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iHolidayService.GetAllHoliday(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


        [HttpPost]
        [Route("AddUpdateHoliday")]
        public HttpResponseMessage AddUpdateHoliday([FromBody] HolidayViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iHolidayService.AddUpdateHoliday(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already with same name.Please add another.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }



        /// <summary>
        ///     Deletes Holiday by identifier.
        /// </summary>
        /// <param name="holidayId">The holidayId identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteHoliday")]
        public HttpResponseMessage DeleteHoliday(long holidayId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iHolidayService.DeleteHolidayById(holidayId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
                return response;
            }
        }


    }
}
