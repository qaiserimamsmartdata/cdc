﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.Tools.FoodManagementMaster;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.FoodManagementMaster;
using CDC.WebApi.Helpers;

namespace CDC.WebApi.Controllers.Tools.FoodManagement
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/foodManagementMaster")]
    public class FoodManagementController : ApiController
    {
        private readonly IFoodManagementMasterService _iFoodManagementMasterService;

        public FoodManagementController(IFoodManagementMasterService iFoodManagementMasterService)
        {
            _iFoodManagementMasterService = iFoodManagementMasterService;
        }
        [HttpPost]
        [Route("GetFoodMasterById")]
        public HttpResponseMessage GetFoodMasterById(long foodMasterfId)
        {
            var foodMasterList = _iFoodManagementMasterService.GetFoodMasterById(foodMasterfId);
            var resultData = foodMasterList;
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("GetAllFoodManagementMaster")]
        public HttpResponseMessage GetAllFoodManagementMaster([FromBody] SearchFoodMasterViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iFoodManagementMasterService.GetAllFoodManagementMaster(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }



        [HttpPost]
        [Route("AddFoodMaster")]
        public HttpResponseMessage AddFoodMaster([FromBody] FoodManagementMasterViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation responseInfo;
            _iFoodManagementMasterService.AddFoodMaster(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            else
            {

                var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
                return response;
            }

        }



        [HttpPost]
        [Route("UpdateFoodMaster")]
        public HttpResponseMessage UpdateFoodMaster(FoodManagementMasterViewModel model)
        {
            ResponseInformation responseInfo;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            _iFoodManagementMasterService.UpdateFoodMaster(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;

            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
                return response;
            }
        }

        [HttpPost]
        [Route("DeleteFoodMasterById")]
        public HttpResponseMessage DeleteFoodMasterById(long foodMasterId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iFoodManagementMasterService.DeleteFoodMasterById(foodMasterId,out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
                return response;
            }
        }
    }
}
