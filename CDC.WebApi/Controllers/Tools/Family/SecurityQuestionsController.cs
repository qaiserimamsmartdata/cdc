﻿using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Family
{
    [Authorize]
    [RoutePrefix("api/SecurityQuestions")]
    public class SecurityQuestionsController : ApiController
    {
        private readonly ISecurityQuestionsService _iquestionstypeservice;
        public SecurityQuestionsController(ISecurityQuestionsService iquestionstypeservice)
        {
            _iquestionstypeservice = iquestionstypeservice;
        }
        
        
        [HttpPost]
        [Route("GetSecurityQuestionsList")]
        public HttpResponseMessage GetSecurityQuestionsList([FromBody]SearchFieldsViewModel SearchData)
        {
            var resultData = _iquestionstypeservice.GetSecurityQuestionsList(SearchData);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
    }
}
