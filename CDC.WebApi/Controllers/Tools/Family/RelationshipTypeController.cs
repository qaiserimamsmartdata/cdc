﻿using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Family
{
    [Authorize]
    [RoutePrefix("api/RelationshipType")]
    public class RelationshipTypeController : ApiController
    {

        private readonly IRelationshipTypeService _irelationshiptypeservice;

        public RelationshipTypeController(IRelationshipTypeService irelationshiptypeservice)
        {
            _irelationshiptypeservice = irelationshiptypeservice;
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("AddUpdateRelationshipType")]
        public HttpResponseMessage AddUpdateRelationshipType([FromBody]RelationTypeViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _irelationshiptypeservice.AddUpdateRelationshipType(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "Relationship type is already exist!";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteRelationshipType")]
        public HttpResponseMessage DeleteRelationshipType([FromBody]RelationTypeViewModel model)
        {
            var resultData = _irelationshiptypeservice.DeleteRelationshipType(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency,Family")]
        [HttpPost]
        [Route("GetRelationshipTypeList")]
        public HttpResponseMessage GetRelationshipTypeList([FromBody]SearchFieldsViewModel model)
        {
            var resultData = _irelationshiptypeservice.GetRelationshipTypeList(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        /// <summary>
        /// Gets all RelationshipType
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("GetAllRelationshipType")]
        public HttpResponseMessage GetAllRelationshipType([FromBody] SearchRelationTypeListViewModel model)
        {
            ResponseViewModel responseViewModel;
            _irelationshiptypeservice.GetAllRelationshipType(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

    }
}
