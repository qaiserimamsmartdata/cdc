﻿using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Family
{
    [Authorize]
    [RoutePrefix("api/InfoSource")]
    public class InfoSourceController : ApiController
    {
        private readonly IInfoSourceService _iinfosourceservice;

        /// <summary>
        ///     Initializes a new instance of the <see cref="InfoSourceController" /> class.
        /// </summary>
        /// <param name="iinfosourceservice">The info source service.</param>
        public InfoSourceController(IInfoSourceService iinfosourceservice)
        {
            _iinfosourceservice = iinfosourceservice;
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("AddUpdateInfoSource")]
        public HttpResponseMessage AddUpdateInfoSource([FromBody]InfoSourceViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iinfosourceservice.AddUpdateInfoSource(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "Info source is already exist!";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteInfoSource")]
        public HttpResponseMessage DeleteInfoSource([FromBody]InfoSourceViewModel model)
        {
            var resultData = _iinfosourceservice.DeleteInfoSource(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency,Family")]
        [HttpPost]
        [Route("GetInfoSourceList")]
        public HttpResponseMessage GetInfoSourceList([FromBody]SearchFieldsViewModel model)
        {
            var resultData = _iinfosourceservice.GetInfoSourceList(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        /// <summary>
        /// Gets all InfoSource
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("GetAllInfoSource")]
        public HttpResponseMessage GetAllInfoSource([FromBody] SearchInfoSourceListViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iinfosourceservice.GetAllInfoSource(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
    }
}
