﻿using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Family
{
    [Authorize]
    [RoutePrefix("api/MembershipType")]
    public class MembershipTypeController : ApiController
    {
        private readonly IMembershipTypeService _imembershiptypeservice;

        public MembershipTypeController(IMembershipTypeService imembershiptypeservice)
        {
            _imembershiptypeservice = imembershiptypeservice;
        }
        [Authorize(Roles ="Agency")]
        [HttpPost]
        [Route("AddUpdateMembershipType")]
        public HttpResponseMessage AddUpdateMembershipType([FromBody]MembershipTypeViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _imembershiptypeservice.AddUpdateMembershipType(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "Membership type is already exist!";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteMembershipType")]
        public HttpResponseMessage DeleteMembershipType([FromBody]MembershipTypeViewModel model)
        {
            var resultData = _imembershiptypeservice.DeleteMembershipType(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency, Family")]
        [HttpPost]
        [Route("GetMembershipTypeList")]
        public HttpResponseMessage GetMembershipTypeList([FromBody]SearchFieldsViewModel model)
        {
            var resultData = _imembershiptypeservice.GetMembershipTypeList(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        /// <summary>
        /// Gets all MembershipType
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("GetAllMembershipType")]
        public HttpResponseMessage GetAllMembershipType([FromBody] SearchMembershipTypeListViewModel model)
        {
            ResponseViewModel responseViewModel;
            _imembershiptypeservice.GetAllMembershipType(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
    }
}
