﻿using CDC.Services.Abstract.Tools.Family;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Family;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Family
{
    [Authorize]
    [RoutePrefix("api/GradeLevel")]
    public class GradeLevelController : ApiController
    {
        private readonly IGradeLevelService _igradelevelservice;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GradeLevelController" /> class.
        /// </summary>
        /// <param name="igradelevelservice">The Grade Level service.</param>
        public GradeLevelController(IGradeLevelService igradelevelservice)
        {
            _igradelevelservice = igradelevelservice;
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("AddUpdateGradeLevel")]
        public HttpResponseMessage AddUpdateGradeLevel([FromBody]GradeLevelViewModel model)
        {  
            var response = new ResponseViewModel();
            var returnData = _igradelevelservice.AddUpdateGradeLevel(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "Grade level is already exist!";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteGradeLevel")]
        public HttpResponseMessage DeleteGradeLevel([FromBody]GradeLevelViewModel model)
        {
            var resultData = _igradelevelservice.DeleteGradeLevel(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency,Family")]
        [HttpPost]
        [Route("GetGradeLevelList")]
        public HttpResponseMessage GetGradeLevelList([FromBody]SearchFieldsViewModel model)
        {
            var resultData = _igradelevelservice.GetGradeLevelList(model);
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        /// <summary>
        /// Gets all GradeLevel
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("GetAllGradeLevel")]
        public HttpResponseMessage GetAllGradeLevel([FromBody] SearchGradeLevelListViewModel model)
        {
            ResponseViewModel responseViewModel;
            _igradelevelservice.GetAllGradeLevel(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

    }
}
