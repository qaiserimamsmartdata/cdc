﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.Tools.Class;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Class;

namespace CDC.WebApi.Controllers.Tools.Class
{
    //[Authorize(Roles = "Agency")]
    [RoutePrefix("api/Room")]
    public class RoomController : ApiController
    {
        private readonly IRoomService _iroomservice;

        public RoomController(IRoomService iroomservice)
        {
            _iroomservice = iroomservice;
        }

        [HttpPost]
        [Route("AddUpdateRoom")]
        public HttpResponseMessage AddUpdateRoom([FromBody] RoomViewModel model)
       {   
            var response = new ResponseViewModel();
            var returnData = _iroomservice.AddUpdateRoom(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already a room with same name.Please add another.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetRoom")]
        public HttpResponseMessage GetRoom([FromBody] RoomViewModel model)
        {
            var room = _iroomservice.GetRoom(model);
            var resultData = new {room = room.Select(m => new {m.ID, m.Name,m.AgencyLocationInfoId})};
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("DeleteRoomById")]
        public HttpResponseMessage DeleteRoomById([FromBody]RoomViewModel model)
        {
            var response = new ResponseViewModel();

            var returnData = _iroomservice.DeleteRoomById(model);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        /// Gets all Room   
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllRoom")]
        public HttpResponseMessage GetAllRoom([FromBody] SearchRoomListViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iroomservice.GetAllRoom(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


    }
}