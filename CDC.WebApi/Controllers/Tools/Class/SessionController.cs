﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.Tools.Class;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Class;

namespace CDC.WebApi.Controllers.Tools.Class
{
    [Authorize(Roles = "Agency")]
    [RoutePrefix("api/Session")]
    public class SessionController : ApiController
    {
        private readonly ISessionService _isessionservice;

        public SessionController(ISessionService isessionservice)
        {
            _isessionservice = isessionservice;
        }

        [HttpPost]
        [Route("AddUpdateSession")]
        public HttpResponseMessage AddUpdateSession([FromBody] SessionViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _isessionservice.AddUpdateSession(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already a session with same duration.Please add another one.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetSession")]
        public HttpResponseMessage GetSession([FromBody] SearchFieldsViewModel model)
        {
            var session = _isessionservice.GetSession(model);
            var resultData = new {session = session.Select(m => new {m.ID, m.Name})};
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("DeleteSessionById")]
        public HttpResponseMessage DeleteSessionById([FromBody] SessionViewModel model)
        {
            var response = new ResponseViewModel();

            var returnData = _isessionservice.DeleteSessionById(model);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        /// Gets all Session
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllSession")]
        public HttpResponseMessage GetAllSession([FromBody] SearchSessionListViewModel model)
        {
            ResponseViewModel responseViewModel;
            _isessionservice.GetAllSession(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

    }
}