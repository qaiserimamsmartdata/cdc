﻿using CDC.Services.Abstract.Tools.Class;
using CDC.ViewModel.Common;
using CDC.ViewModel.Tools.Class;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Tools.Class
{
    //[Authorize(Roles = "Agency")]
    [RoutePrefix("api/ClassStatus")]
    public class ClassStatusController : ApiController
    {
        private readonly IClassStatusService _iclassstatusservice;

        public ClassStatusController(IClassStatusService iclassstatusservice)
        {
            _iclassstatusservice = iclassstatusservice;
        }

        [HttpPost]
        [Route("AddUpdateClassStatus")]
        public HttpResponseMessage AddUpdateClassStatus([FromBody] ClassStatusViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iclassstatusservice.AddUpdateClassStatus(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already a class status with same name.Please add another one.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetClassStatus")]
        public HttpResponseMessage GetClassStatus([FromBody] SearchFieldsViewModel model)
        {
            var classStatus = _iclassstatusservice.GetClassStatus(model);
            var resultData = new { classStatus = classStatus.Select(m => new { m.ID, m.Name }) };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("DeleteClassStatusById")]
        public HttpResponseMessage DeleteClassStatusById([FromBody]ClassStatusViewModel model)
        {
            var response = new ResponseViewModel();

            var returnData = _iclassstatusservice.DeleteClassStatusById(model);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        /// Gets all ClassStatus
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllClassStatus")]
        public HttpResponseMessage GetAllClassStatus([FromBody] SearchClassStatusListViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iclassstatusservice.GetAllClassStatus(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
    }
}
