﻿using CDC.Services;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Attendance;
using CDC.Services.Abstract.DailyStatus;
using CDC.Services.Abstract.RazorEngine;
using CDC.ViewModel;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Common;
using CDC.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Attendance
{
    [Authorize]
    [RoutePrefix("api/StudentAttendance")]
    public class StudentAttendanceController : ApiController
    {
        private readonly IStudentAttendanceService _istudentattendanceservice;
        private readonly ICommonService _iCommonservice;
        private readonly IDailyStatusService _idailyStatusService;
        private readonly IRazorService _iRazorService;
        public StudentAttendanceController(IStudentAttendanceService istudentattendanceservice, ICommonService iCommonservice, IDailyStatusService idailyStatusService, IRazorService iRazorService)
        {
            _istudentattendanceservice = istudentattendanceservice;
            _iCommonservice = iCommonservice;
            _idailyStatusService = idailyStatusService;
            _iRazorService = iRazorService;
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("GetAllStundentSchedule")]
        public HttpResponseMessage GetAllStundentSchedule([FromBody] SearchStudentAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _istudentattendanceservice.GetStudentScheduleList(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("GetAllStudentAttendance")]
        public HttpResponseMessage GetAllStudentAttendance([FromBody] SearchStudentAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _istudentattendanceservice.GetStudentAttendanceList(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("GetAllStudentAttendanceTest")]
        public HttpResponseMessage GetAllStudentAttendanceTest([FromBody] SearchStudentAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _istudentattendanceservice.GetStudentAttendanceListTest(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("AddStudentAttendance")]
        public HttpResponseMessage AddStudentAttendance([FromBody] StudentAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation responseInfo;
            _istudentattendanceservice.AddStudentAttendance(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("UpdateStudentAttendance")]
        public HttpResponseMessage UpdateStudentAttendance(StudentAttendanceViewModel model)
        {
            ResponseInformation responseInfo;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }

            _istudentattendanceservice.UpdateStudentAttendance(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;

            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("DeleteStudentAttendanceById")]
        public HttpResponseMessage DeleteStudentAttendanceById(long studentAttendanceId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _istudentattendanceservice.DeleteStudentAttendanceById(studentAttendanceId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("GetParentListByStudentId")]
        public HttpResponseMessage GetParentListByStudentId([FromBody]IDViewModel model)
        {
            var responseViewModel = _istudentattendanceservice.GetParentListByStudentId(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("MarkDropByAttendanceKiosk")]
        public HttpResponseMessage MarkDropByAttendanceKiosk([FromBody]List<StudentAttendanceViewModel> model)
        {
            //var checkMarkedAttendanceViewModel = _istudentattendanceservice.CheckAttendanceForClassKiosk(model);
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            var responseViewModel = _istudentattendanceservice.MarkDropByAttendanceKiosk(model);
            foreach (var item in model)
            {
                if (item.SignInChecked = true && item.SignOutChecked == false)
                {
                    var currentTime = _iCommonservice.ConvertTimeFromUTC(item.AttendanceDate, DateTime.UtcNow.TimeOfDay, item.TimeZone);
                    TimeSpan timespan = new TimeSpan(currentTime.Value.Ticks);
                    DateTime time = DateTime.Today.Add(timespan);
                    string displayTime = time.ToString("hh:mm tt");
                    var template = CDCEmailTemplates.ParticipantSignInSignOut;
                    if (item.IsParticipantAttendanceMailReceived != null && item.IsParticipantAttendanceMailReceived.Value)
                    {
                        string smsMessage = "";
                        if (item.DropedById == null && item.DropedByOtherId == null)
                            smsMessage = "Hi " + item.ParentName + " (" + item.DropByRelationName + ")" + ", <br/>" + item.StudentName + " is on leave";
                        if (item.DropedByOtherId > 0)
                            smsMessage = "Hi " + item.ParentName + "(" + item.DropByRelationName + ")" + ", <br/>" + item.StudentName + " has been dropped by " + item.DropedByOtherName;
                        if (item.DropedById > 0)
                            smsMessage = "Hi " + item.ParentName + " (" + item.DropByRelationName + ")" + ", <br/>" + item.StudentName + " has been dropped by you";
                        smsMessage = smsMessage + " " + "at" + " " + displayTime + ".";
                        string smsSubject = "PinWheel Participant Attendance";
                        string smsAddress = item.GuradianPhone + "@" + item.GuradianSMSCarrier;
                        string mailAddress = item.GuradianEmail;
                        _iCommonservice.SendEmail(smsAddress, smsSubject, smsMessage);

                        var resultTmplt = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                        {
                            Url = url,
                            TextMessage = smsMessage,
                            TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                            Logo = url + ConfigurationManager.AppSettings["Logo"]
                        });
                        _iCommonservice.SendEmail(mailAddress, smsSubject, resultTmplt);
                    }
                    if (item.IsParticipantAttendanceMailReceived != null && (!string.IsNullOrEmpty(item.PrimaryGuradianEmail) && (item.IsParticipantAttendanceMailReceived.Value)))
                    {
                        #region ForPrimary
                        string primarySmsMessage = "";
                        if (item.DropedById == null && item.DropedByOtherId == null)
                            primarySmsMessage = "Hi " + item.PrimaryParentName + " (" + item.PrimaryDropByRelationName + ")" + ", <br/>" + item.StudentName + " is on leave";
                        if (item.DropedByOtherId > 0)
                            primarySmsMessage = "Hi " + item.PrimaryParentName + "(" + item.PrimaryDropByRelationName + ")" + ", <br/>" + item.StudentName + " has been dropped by " + item.DropedByOtherName;
                        if (item.DropedById > 0)
                            primarySmsMessage = "Hi " + item.PrimaryParentName + " (" + item.PrimaryDropByRelationName + ")" + ", <br/>" + item.StudentName + " has been dropped by " + item.ParentName + " (" + item.DropByRelationName + ") ";


                        primarySmsMessage = primarySmsMessage + " " + "at" + " " + displayTime + ".";
                        string PrimarysmsSubject = "PinWheel Participant Attendance";
                        string primarysmsAddress = item.PrimaryGuradianPhone + "@" + item.PrimaryGuradianSMSCarrier;
                        string primarymailAddress = item.PrimaryGuradianEmail;
                        _iCommonservice.SendEmail(primarysmsAddress, PrimarysmsSubject, primarySmsMessage);

                        var resultTmplt = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                        {
                            Url = url,
                            TextMessage = primarySmsMessage,
                            TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                            Logo = url + ConfigurationManager.AppSettings["Logo"]
                        });
                        _iCommonservice.SendEmail(primarymailAddress, PrimarysmsSubject, resultTmplt);
                    }
                }
                else if (item.SignOutChecked == true && item.SignInChecked == true)
                {
                    var currentTime = _iCommonservice.ConvertTimeFromUTC(item.AttendanceDate, DateTime.UtcNow.TimeOfDay, item.TimeZone);
                    TimeSpan timespan = new TimeSpan(currentTime.Value.Ticks);
                    DateTime time = DateTime.Today.Add(timespan);
                    string displayTime = time.ToString("hh:mm tt");
                    currentUrl = HttpContext.Current.Request.Url;
                    url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
                    var template = CDCEmailTemplates.ParticipantSignInSignOut;
                    if (item.IsParticipantAttendanceMailReceived != null && item.IsParticipantAttendanceMailReceived.Value)
                    {

                        string smsMessage = "";
                        if (!string.IsNullOrEmpty(item.PickupByOtherName))
                            smsMessage = "Hi " + item.ParentName + " (" + item.PickedByRelationName + ")" + ", <br/>" + item.StudentName + " has been picked by " + item.PickupByOtherName;
                        if (item.PickupById > 0)
                            smsMessage = "Hi " + item.ParentName + " (" + item.PickedByRelationName + ")" + ", <br/>" + item.StudentName + " has been picked by you";
                        smsMessage = smsMessage.Replace("@Url", url);


                        smsMessage = smsMessage + " " + "at" + " " + displayTime + ".";

                        string smsSubject = "PinWheel Participant Attendance";
                        string smsAddress = item.GuradianPhone + "@" + item.GuradianSMSCarrier;
                        string mailAddress = item.GuradianEmail;
                        _iCommonservice.SendEmail(smsAddress, smsSubject, smsMessage);

                        var result = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                        {
                            Url = url,
                            TextMessage = smsMessage,
                            TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                            Logo = url + ConfigurationManager.AppSettings["Logo"]
                        });

                        _iCommonservice.SendEmail(mailAddress, smsSubject, result);
                    }
                }
            }
            #endregion
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");


        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("MarkDropByAttendance")]
        public HttpResponseMessage MarkDropByAttendance([FromBody] StudentAttendanceViewModel model)
        {
            var checkMarkedAttendanceViewModel = _istudentattendanceservice.CheckAttendanceForClass(model);
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            if (checkMarkedAttendanceViewModel.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, checkMarkedAttendanceViewModel, "application/json");
            }

            var responseViewModel = _istudentattendanceservice.MarkDropByAttendance(model);

            var currentTime = _iCommonservice.ConvertTimeFromUTC(model.AttendanceDate, DateTime.UtcNow.TimeOfDay, model.TimeZone);
            TimeSpan timespan = new TimeSpan(currentTime.Value.Ticks);
            DateTime time = DateTime.Today.Add(timespan);
            string displayTime = time.ToString("hh:mm tt");
            var template = CDCEmailTemplates.ParticipantSignInSignOut;
            if (model.IsParticipantAttendanceMailReceived != null && model.IsParticipantAttendanceMailReceived.Value)
            {
                string smsMessage = "";
                if (model.DropedById == null && model.DropedByOtherId == null)
                    smsMessage = "Hi " + model.ParentName + " (" + model.DropByRelationName + ")" + ", <br/>" + model.StudentName + " is on leave";
                if (model.DropedByOtherId > 0)
                    smsMessage = "Hi " + model.ParentName + "(" + model.DropByRelationName + ")" + ", <br/>" + model.StudentName + " has been dropped by " + model.DropedByOtherName;
                if (model.DropedById > 0)
                    smsMessage = "Hi " + model.ParentName + " (" + model.DropByRelationName + ")" + ", <br/>" + model.StudentName + " has been dropped by you";



                smsMessage = smsMessage + " " + "at" + " " + displayTime + ".";
                string smsSubject = "PinWheel Participant Attendance";
                string smsAddress = model.GuradianPhone + "@" + model.GuradianSMSCarrier;
                string mailAddress = model.GuradianEmail;
                _iCommonservice.SendEmail(smsAddress, smsSubject, smsMessage);

                var resultTmplt = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                {
                    Url = url,
                    TextMessage = smsMessage,
                    TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo = url + ConfigurationManager.AppSettings["Logo"]
                });
                _iCommonservice.SendEmail(mailAddress, smsSubject, resultTmplt);
            }
            if (model.IsParticipantAttendanceMailReceived != null && (!string.IsNullOrEmpty(model.PrimaryGuradianEmail) && (model.IsParticipantAttendanceMailReceived.Value)))
            {
                #region ForPrimary
                string primarySmsMessage = "";
                if (model.DropedById == null && model.DropedByOtherId == null)
                    primarySmsMessage = "Hi " + model.PrimaryParentName + " (" + model.PrimaryDropByRelationName + ")" + ", <br/>" + model.StudentName + " is on leave";
                if (model.DropedByOtherId > 0)
                    primarySmsMessage = "Hi " + model.PrimaryParentName + "(" + model.PrimaryDropByRelationName + ")" + ", <br/>" + model.StudentName + " has been dropped by " + model.DropedByOtherName;
                if (model.DropedById > 0)
                    primarySmsMessage = "Hi " + model.PrimaryParentName + " (" + model.PrimaryDropByRelationName + ")" + ", <br/>" + model.StudentName + " has been dropped by " + model.ParentName + " (" + model.DropByRelationName + ") ";


                primarySmsMessage = primarySmsMessage + " " + "at" + " " + displayTime + ".";
                string PrimarysmsSubject = "PinWheel Participant Attendance";
                string primarysmsAddress = model.PrimaryGuradianPhone + "@" + model.PrimaryGuradianSMSCarrier;
                string primarymailAddress = model.PrimaryGuradianEmail;
                _iCommonservice.SendEmail(primarysmsAddress, PrimarysmsSubject, primarySmsMessage);

                var resultTmplt = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                {
                    Url = url,
                    TextMessage = primarySmsMessage,
                    TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo = url + ConfigurationManager.AppSettings["Logo"]
                });
                _iCommonservice.SendEmail(primarymailAddress, PrimarysmsSubject, resultTmplt);
                #endregion
            }
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("MarkPickUpByAttendance")]
        public HttpResponseMessage MarkPickUpByAttendance([FromBody]StudentAttendanceViewModel model)
        {
            var responseViewModel = _istudentattendanceservice.MarkPickUpByAttendance(model);
            var currentTime = _iCommonservice.ConvertTimeFromUTC(model.AttendanceDate, DateTime.UtcNow.TimeOfDay, model.TimeZone);
            TimeSpan timespan = new TimeSpan(currentTime.Value.Ticks);
            DateTime time = DateTime.Today.Add(timespan);
            string displayTime = time.ToString("hh:mm tt");
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            var template = CDCEmailTemplates.ParticipantSignInSignOut;
            if (model.IsParticipantAttendanceMailReceived != null && model.IsParticipantAttendanceMailReceived.Value)
            {

                string smsMessage = "";
                if (!string.IsNullOrEmpty(model.PickupByOtherName))
                    smsMessage = "Hi " + model.ParentName + " (" + model.PickedByRelationName + ")" + ", <br/>" + model.StudentName + " has been picked by " + model.PickupByOtherName;
                if (model.PickupById > 0)
                    smsMessage = "Hi " + model.ParentName + " (" + model.PickedByRelationName + ")" + ", <br/>" + model.StudentName + " has been picked by you";
                smsMessage = smsMessage.Replace("@Url", url);


                smsMessage = smsMessage + " " + "at" + " " + displayTime + ".";

                string smsSubject = "PinWheel Participant Attendance";
                string smsAddress = model.GuradianPhone + "@" + model.GuradianSMSCarrier;
                string mailAddress = model.GuradianEmail;
                _iCommonservice.SendEmail(smsAddress, smsSubject, smsMessage);

                var result = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                {
                    Url = url,
                    TextMessage = smsMessage,
                    TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo = url + ConfigurationManager.AppSettings["Logo"]
                });

                _iCommonservice.SendEmail(mailAddress, smsSubject, result);
            }
            /////For Daily Status report mail to primary parent
            var dailyStatusData = _idailyStatusService.DailyStatusEmail(model.StudentId.Value, model.AttendanceDate, model.TimeZone);

            string MailSubject = AppConstants.ParticipantDailyStatus;
            var templateDsr = CDCEmailTemplates.DailyStatusReport;

            StringBuilder sb = new StringBuilder();


            sb.AppendLine("<tr>");
            sb.AppendFormat("<td style=''><b>Category</b></td>");
            sb.AppendFormat("<td><b>Start Time</b></td>");
            sb.AppendFormat("<td><b>End Time</b></td>");
            sb.AppendFormat("<td style='width:70px;'><b>Comments</b></td>");
            sb.AppendLine("</tr>");

            foreach (var item in dailyStatusData.DSR_eat)
            {
                sb.AppendLine("<tr>");
                sb.AppendFormat("<td>Eat</td>");
                sb.AppendFormat("<td>{0}</td>", item.StartTime);
                sb.AppendFormat("<td>{0}</td>", item.EndTime);
                sb.AppendFormat("<td style='width:150px;'>{0}</td>", item.Comments);
                sb.AppendLine("</tr>");

            }
            foreach (var item in dailyStatusData.DSR_sleep)
            {
                sb.AppendLine("<tr>");
                sb.AppendFormat("<td>Sleep</td>");
                sb.AppendFormat("<td>{0}</td>", item.StartTime);
                sb.AppendFormat("<td>{0}</td>", item.EndTime);
                sb.AppendFormat("<td  style='width:200px;'>{0}</td>", item.Comments);
                sb.AppendLine("</tr>");

            }
            foreach (var item in dailyStatusData.DSR_toilet)
            {
                sb.AppendLine("<tr>");
                sb.AppendFormat("<td>Toilet</td>");
                sb.AppendFormat("<td>{0}</td>", item.StartTime);
                sb.AppendFormat("<td>{0}</td>", item.EndTime);
                sb.AppendFormat("<td  style='width:180px;'>{0}</td>", item.Comments);
                sb.AppendLine("</tr>");

            }
            foreach (var item in dailyStatusData.DSR_Comment)
            {
                sb.AppendLine("<tr>");
                sb.AppendFormat("<td>General</td>");
                sb.AppendFormat("<td>--</td>");
                sb.AppendFormat("<td>--</td>");
                sb.AppendFormat("<td  style='width:110px;'>{0}</td>", item.Comment);
                sb.AppendLine("</tr>");

            }

            string date = model.AttendanceDate.ToShortDateString();
            var resultTmplt = _iRazorService.GetRazorTemplate(templateDsr, _iCommonservice.GetMd5Hash(templateDsr), new
            {
                Url = url,
                Name = dailyStatusData.ParticipantName,
                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                Logo = url + ConfigurationManager.AppSettings["Logo"],
                Date = date,
                Participantname = Convert.ToString(sb)
            });
            _iCommonservice.SendEmail(dailyStatusData.familyEmail, MailSubject, Convert.ToString(resultTmplt));
            /////////////////////////////////////////////////////////////////////////////////////
            #region For Primary
            if (model.IsParticipantAttendanceMailReceived != null && (!string.IsNullOrEmpty(model.PrimaryGuradianEmail) && (model.IsParticipantAttendanceMailReceived.Value)))
            {


                string primarySmsMessage = "";
                if (!string.IsNullOrEmpty(model.PickupByOtherName))
                    primarySmsMessage = "Hi " + model.PrimaryParentName + " (" + model.PrimaryPickedByRelationName + ")" + ", <br/>" + model.StudentName + " has been picked by " + model.PickupByOtherName;
                if (model.PickupById > 0)
                    primarySmsMessage = "Hi " + model.PrimaryParentName + " (" + model.PrimaryPickedByRelationName + ")" + ", <br/>" + model.StudentName + " has been picked by " + model.ParentName + " (" + model.PickedByRelationName + ") ";
                primarySmsMessage = primarySmsMessage.Replace("@Url", url);
                primarySmsMessage = primarySmsMessage + " " + "at" + " " + displayTime + ".";
                string PrimarysmsSubject = "PinWheel Participant Attendance";
                string primarysmsAddress = model.PrimaryGuradianPhone + "@" + model.PrimaryGuradianSMSCarrier;
                string primarymailAddress = model.PrimaryGuradianEmail;

                _iCommonservice.SendEmail(primarysmsAddress, PrimarysmsSubject, primarySmsMessage);

                var resultTmpltAtt = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                {
                    Url = url,
                    TextMessage = primarySmsMessage,
                    TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo = url + ConfigurationManager.AppSettings["Logo"]
                });

                _iCommonservice.SendEmail(primarymailAddress, PrimarysmsSubject, resultTmpltAtt);
                #endregion

            }

            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("MarkPickUpByAttendanceKiosk")]
        public HttpResponseMessage MarkPickUpByAttendanceKiosk([FromBody]List<StudentAttendanceViewModel> model)
        {

            var responseViewModel = _istudentattendanceservice.MarkPickUpByAttendanceKiosk(model);
            foreach (var item in model)
            {
                var currentTime = _iCommonservice.ConvertTimeFromUTC(item.AttendanceDate, DateTime.UtcNow.TimeOfDay, item.TimeZone);
                TimeSpan timespan = new TimeSpan(currentTime.Value.Ticks);
                DateTime time = DateTime.Today.Add(timespan);
                string displayTime = time.ToString("hh:mm tt");
                Uri currentUrl = HttpContext.Current.Request.Url;
                var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
                var template = CDCEmailTemplates.ParticipantSignInSignOut;
                if (item.IsParticipantAttendanceMailReceived != null && item.IsParticipantAttendanceMailReceived.Value)
                {

                    string smsMessage = "";
                    if (!string.IsNullOrEmpty(item.PickupByOtherName))
                        smsMessage = "Hi " + item.ParentName + " (" + item.PickedByRelationName + ")" + ", <br/>" + item.StudentName + " has been picked by " + item.PickupByOtherName;
                    if (item.PickupById > 0)
                        smsMessage = "Hi " + item.ParentName + " (" + item.PickedByRelationName + ")" + ", <br/>" + item.StudentName + " has been picked by you";
                    smsMessage = smsMessage.Replace("@Url", url);


                    smsMessage = smsMessage + " " + "at" + " " + displayTime + ".";

                    string smsSubject = "PinWheel Participant Attendance";
                    string smsAddress = item.GuradianPhone + "@" + item.GuradianSMSCarrier;
                    string mailAddress = item.GuradianEmail;
                    _iCommonservice.SendEmail(smsAddress, smsSubject, smsMessage);

                    var result = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                    {
                        Url = url,
                        TextMessage = smsMessage,
                        TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                        Logo = url + ConfigurationManager.AppSettings["Logo"]
                    });

                    _iCommonservice.SendEmail(mailAddress, smsSubject, result);
                }
                /////For Daily Status report mail to primary parent
                var dailyStatusData = _idailyStatusService.DailyStatusEmail(item.StudentId.Value, item.AttendanceDate, item.TimeZone);

                string MailSubject = AppConstants.ParticipantDailyStatus;
                var templateDsr = CDCEmailTemplates.DailyStatusReport;

                StringBuilder sb = new StringBuilder();


                sb.AppendLine("<tr>");
                sb.AppendFormat("<td style=''><b>Category</b></td>");
                sb.AppendFormat("<td><b>Start Time</b></td>");
                sb.AppendFormat("<td><b>End Time</b></td>");
                sb.AppendFormat("<td style='width:70px;'><b>Comments</b></td>");
                sb.AppendLine("</tr>");

                foreach (var item2 in dailyStatusData.DSR_eat)
                {
                    sb.AppendLine("<tr>");
                    sb.AppendFormat("<td>Eat</td>");
                    sb.AppendFormat("<td>{0}</td>", item2.StartTime);
                    sb.AppendFormat("<td>{0}</td>", item2.EndTime);
                    sb.AppendFormat("<td style='width:150px;'>{0}</td>", item2.Comments);
                    sb.AppendLine("</tr>");

                }
                foreach (var item2 in dailyStatusData.DSR_sleep)
                {
                    sb.AppendLine("<tr>");
                    sb.AppendFormat("<td>Sleep</td>");
                    sb.AppendFormat("<td>{0}</td>", item2.StartTime);
                    sb.AppendFormat("<td>{0}</td>", item2.EndTime);
                    sb.AppendFormat("<td  style='width:200px;'>{0}</td>", item2.Comments);
                    sb.AppendLine("</tr>");

                }
                foreach (var item2 in dailyStatusData.DSR_toilet)
                {
                    sb.AppendLine("<tr>");
                    sb.AppendFormat("<td>Toilet</td>");
                    sb.AppendFormat("<td>{0}</td>", item2.StartTime);
                    sb.AppendFormat("<td>{0}</td>", item2.EndTime);
                    sb.AppendFormat("<td  style='width:180px;'>{0}</td>", item2.Comments);
                    sb.AppendLine("</tr>");

                }
                foreach (var item2 in dailyStatusData.DSR_Comment)
                {
                    sb.AppendLine("<tr>");
                    sb.AppendFormat("<td>General</td>");
                    sb.AppendFormat("<td>--</td>");
                    sb.AppendFormat("<td>--</td>");
                    sb.AppendFormat("<td  style='width:110px;'>{0}</td>", item2.Comment);
                    sb.AppendLine("</tr>");

                }

                string date = item.AttendanceDate.ToShortDateString();
                var resultTmplt = _iRazorService.GetRazorTemplate(templateDsr, _iCommonservice.GetMd5Hash(templateDsr), new
                {
                    Url = url,
                    Name = dailyStatusData.ParticipantName,
                    TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo = url + ConfigurationManager.AppSettings["Logo"],
                    Date = date,
                    Participantname = Convert.ToString(sb)
                });
                _iCommonservice.SendEmail(dailyStatusData.familyEmail, MailSubject, Convert.ToString(resultTmplt));
                /////////////////////////////////////////////////////////////////////////////////////
                #region For Primary
                if (item.IsParticipantAttendanceMailReceived != null && (!string.IsNullOrEmpty(item.PrimaryGuradianEmail) && (item.IsParticipantAttendanceMailReceived.Value)))
                {


                    string primarySmsMessage = "";
                    if (!string.IsNullOrEmpty(item.PickupByOtherName))
                        primarySmsMessage = "Hi " + item.PrimaryParentName + " (" + item.PrimaryPickedByRelationName + ")" + ", <br/>" + item.StudentName + " has been picked by " + item.PickupByOtherName;
                    if (item.PickupById > 0)
                        primarySmsMessage = "Hi " + item.PrimaryParentName + " (" + item.PrimaryPickedByRelationName + ")" + ", <br/>" + item.StudentName + " has been picked by " + item.ParentName + " (" + item.PickedByRelationName + ") ";
                    primarySmsMessage = primarySmsMessage.Replace("@Url", url);
                    primarySmsMessage = primarySmsMessage + " " + "at" + " " + displayTime + ".";
                    string PrimarysmsSubject = "PinWheel Participant Attendance";
                    string primarysmsAddress = item.PrimaryGuradianPhone + "@" + item.PrimaryGuradianSMSCarrier;
                    string primarymailAddress = item.PrimaryGuradianEmail;

                    _iCommonservice.SendEmail(primarysmsAddress, PrimarysmsSubject, primarySmsMessage);

                    var resultTmpltAtt = _iRazorService.GetRazorTemplate(template, _iCommonservice.GetMd5Hash(template), new
                    {
                        Url = url,
                        TextMessage = primarySmsMessage,
                        TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                        Logo = url + ConfigurationManager.AppSettings["Logo"]
                    });

                    _iCommonservice.SendEmail(primarymailAddress, PrimarysmsSubject, resultTmpltAtt);
                }
                #endregion

            }

            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        [Authorize(Roles = "Agency,Staff,Family")]
        [HttpPost]
        [Route("GetAttendanceHistory")]
        public HttpResponseMessage GetAttendanceHistory([FromBody]SearchAttendanceHistoryViewModel model)
        {
            var responseViewModel = _istudentattendanceservice.GetAttendanceHistory(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency,Staff,Family")]
        [HttpPost]

        [Route("GetStudentAttendanceHistory")]
        public HttpResponseMessage GetStudentAttendanceHistory([FromBody]SearchStudentAttendanceHistoryViewModel model)
        {
            var responseViewModel = _istudentattendanceservice.GetStudentAttendanceHistory(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("GetParticipantFailedSignout")]
        public HttpResponseMessage GetParticipantFailedSignout([FromBody] SearchStudentAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _istudentattendanceservice.GetStudentFailedSignOutList(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");

        }

        //added for kiosk
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("GetStudentAttendanceListForKiosk")]
        public HttpResponseMessage GetStudentAttendanceListForKiosk([FromBody] SearchStudentAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _istudentattendanceservice.GetStudentAttendanceListForKiosk(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("CheckParentSignIn")]
        public HttpResponseMessage CheckParentSignIn([FromBody] SearchStudentAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _istudentattendanceservice.CheckParentSignIn(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
    }
}
