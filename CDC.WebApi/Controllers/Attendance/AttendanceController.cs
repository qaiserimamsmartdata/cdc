﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.Attendance;
using CDC.ViewModel.Attendance;
using CDC.ViewModel.Common;
using CDC.WebApi.Helpers;
using CDC.ViewModel.Staff;
using System;
using CDC.Services.Abstract.Staff;

namespace CDC.WebApi.Controllers.Attendance
{
    [Authorize]
    [RoutePrefix("api/StaffAttendance")]
    public class AttendanceController : ApiController
    {
        private readonly IAttendanceService _iattendanceservice;
        private readonly ITimeSheetService _timeSheetService;
        public AttendanceController(IAttendanceService iattendanceservice, ITimeSheetService timeSheetService)
        {
            _iattendanceservice = iattendanceservice;
            _timeSheetService = timeSheetService;
        }

        /// <summary>
        /// Gets all staff schedule.Required Scheduled Staff for Attendance Mark which will be stored in Attendance.
        /// </summary>
        /// <param name="model">
        ///     The model.
        ///     The model.
        /// </param>
        /// <returns></returns>
        /// <summary>
        /// Gets all staff attendance.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllStaffAttendance")]
        public HttpResponseMessage GetAllStaffAttendance([FromBody] SearchStaffAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iattendanceservice.GetStaffAttendanceList(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        /// <summary>
        /// Adds the attendance.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddAttendance")]
        public HttpResponseMessage AddAttendance([FromBody] AttendanceViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                return Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            }

            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(model.TimeZone);
            DateTime? attDate = TimeZoneInfo.ConvertTimeFromUtc(model.AttendanceDate.Value, tz);

            var responseData = _iattendanceservice.AddAttendance(model);

            if (responseData.IsSuccess)
            {

                //Update TimeSheet Table On Add Attendance
                if (model.ID == 0)
                {
                    // Condition(1.InTime Enter Or 2.InTime-OutTime Enter)
                    if (model.OnLeave == false)
                    { 
                        TimeCheckModel value = new TimeCheckModel();
                    string ipAddress = "";
                        if (model.StaffId != null) value.StaffID = (int)model.StaffId;
                        value.TimesheetID = 0;
                    value.PurposeToCheckInOrOut = Entities.Enums.InOutPurpose.Day;
                    value.CheckOnDateTime = DateTime.UtcNow;

                    if (model.OutTime == null)
                    {
                        TimeSpan span = new TimeSpan(0, 0, 0, 0, 0);
                        model.OutTime = span;
                    }
                    _timeSheetService.SaveCheckFirstTimeFromStaffAttendance(value, attDate.Value, model.InTime.Value, model.OutTime.Value, ipAddress, model.TimeZone);
                }
                }
                return Request.CreateResponse(HttpStatusCode.OK, responseData);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, responseData);
        }
        /// <summary>
        /// Updates the staff attendance.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateStaffAttendance")]
        public HttpResponseMessage UpdateStaffAttendance(AttendanceViewModel model)
        {
            ResponseInformation responseInfo;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }

            _iattendanceservice.UpdateStaffAttendance(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;

            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
        /// <summary>
        /// Deletes the staff attendance by identifier.
        /// </summary>
        /// <param name="staffId">The staff identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteStaffAttendanceById")]
        public HttpResponseMessage DeleteStaffAttendanceById(long staffId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iattendanceservice.DeleteStaffAttendanceById(staffId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

        [HttpPost]
        [Route("GetStaffFailedSignout")]
        public HttpResponseMessage GetStaffFailedSignout([FromBody] SearchStaffAttendanceViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iattendanceservice.GetStaffFailedSignOutList(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");

        }

    }
}
