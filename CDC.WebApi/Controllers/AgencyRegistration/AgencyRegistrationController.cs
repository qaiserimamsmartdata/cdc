﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CDC.ViewModel.Common;
using CDC.WebApi.Encryption;
using CDC.Services.Abstract.AgencyRegistration;
using CDC.ViewModel.AgencyRegistration;
using CDC.WebApi.Helpers;
using CDC.ViewModel;
using CDC.Services.Abstract;
using Stripe;
using System.Globalization;
using System.Linq;
using CDC.Services.Abstract.SuperAdmin;
using Newtonsoft.Json;
using CDC.ViewModel.SuperAdmin;
using CDC.Entities.Pricing;
using CDC.ViewModel.PaymentHistoryForPlans;
using CDC.Services.Abstract.PaymentHistoryForPlans;
using CDC.Entities.TimeClockUsers;
using CDC.Services;
using CDC.Services.Abstract.RazorEngine;

namespace CDC.WebApi.Controllers.AgencyRegistration
{
    /// <summary>
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [Authorize]
    [RoutePrefix("api/AgencyRegistration")]
    public class AgencyRegistrationController : ApiController
    {
        private readonly IAgencyRegistrationService _iagencyRegistrationService;
        private readonly IParticipantPlanService _iParticipantPlanService;
        private readonly ITimeClockUserPlanService _iTimeClockUserPlanService;
        private readonly IPaymentHistoryInfoForPlanService _iPaymentHisoryPlanInfoService;
        private readonly ICommonService _icommonservice;
        private readonly IRazorService _iRazorService;
        public AgencyRegistrationController(IAgencyRegistrationService iagencyservice, 
            ICommonService icommonservice, IParticipantPlanService iparticipantplanservice,IPaymentHistoryInfoForPlanService iPaymentHistoryInfoForPlanService, ITimeClockUserPlanService iTimeClockUserPlanService, IRazorService iRazorService)
        {
            _iagencyRegistrationService = iagencyservice;
            _icommonservice = icommonservice;
            _iParticipantPlanService = iparticipantplanservice;
            _iPaymentHisoryPlanInfoService = iPaymentHistoryInfoForPlanService;
            _iTimeClockUserPlanService = iTimeClockUserPlanService;
            _iRazorService = iRazorService;
        }
       
        [HttpPost]
        [Route("SetIsLoggedFirstTime")]
        public HttpResponseMessage SetIsLoggedFirstTime(long agencyId)
        {
            var responseObj = _iagencyRegistrationService.SetIsLoggedFirstTime(agencyId);
            return Request.CreateResponse(HttpStatusCode.OK, responseObj, "application/json");
        }


        [HttpPost]
        [Route("GetAgencyById")]
        public HttpResponseMessage GetAgencyById(long agencyId)
        {
            var agencyList = _iagencyRegistrationService.GetAgencyById(agencyId);
            var resultData = new { agencyList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("GetPaymentById")]
        public HttpResponseMessage GetPaymentById(long agencyId)
        {
            var agencyList = _iagencyRegistrationService.GetPaymentById(agencyId);
            var resultData = new { agencyList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }



        /// <summary>
        ///     Adds the agency.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("AddAgency")]
        public HttpResponseMessage AddAgency([FromBody]AgencyRegistrationViewModel model)
        {
            var responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                var enumerableErr = errors as IList<object> ?? errors.Cast<object>().ToList();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(enumerableErr);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(enumerableErr);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            var password = model.Password;

            model.Password = EncryptDecrypt.GetEncryptedData(model.Password);


            ResponseInformation transaction;
            #region Start Payment process
            //responseViewModel = PaymentProcessStart(model);
            HttpResponseMessage response;
            //if (responseViewModel.IsSuccess == false)
            //{
            //    response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            //    return response;
            //}

            #endregion
            ///////For Each subscription plan  name and amount which will be saved in paymenthistoryinfo from webhook
            //var phPlansmodel = GetPricingPlanInfo(model, responseViewModel);
            //ResponseInformation res;
            //_iPaymentHisoryPlanInfoService.AddPaymentHistoryInfoForPlans(phPlansmodel, out res);
            //if (res.ReturnStatus == false)
            //{
            //    var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
            //    return badresponse;
            //}
            //////////////////////////////////////////////////////////////////////////////////////

            _iagencyRegistrationService.AddAgency(model, out transaction);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = transaction.ReturnStatus;
            responseViewModel.ReturnMessage = transaction.ReturnMessage;
            responseViewModel.ValidationErrors = transaction.ValidationErrors;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var tt = HttpContext.Current.Request.Url;
            var url = tt.Scheme + Uri.SchemeDelimiter + tt.Host + ":" + tt.Port;

            const string mailSubject = AppConstants.RequestForCDCAgencyLogin;
            var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.LoginDetails, _icommonservice.GetMd5Hash(CDCEmailTemplates.LoginDetails), new
            {
                Url = url,
                username = model.Email, password,
                UserRole = AppConstants.Organization,
                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                Logo = url + ConfigurationManager.AppSettings["Logo"]
            });
            _icommonservice.SendEmail(model.Email, mailSubject, result);

            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            const string mailSubject1 = AppConstants.AgencyRegForAdmin;

            var resultTmplt = _iRazorService.GetRazorTemplate(CDCEmailTemplates.SuperAdmin, _icommonservice.GetMd5Hash(CDCEmailTemplates.SuperAdmin), new
            {
                Url = url,
                name = model.AgencyName,
                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                Logo = url + ConfigurationManager.AppSettings["Logo"]
            });
            _icommonservice.SendEmail("pinwheel.trinity@gmail.com", mailSubject1, resultTmplt);
            response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }

        private static PaymentHistoryInfoForPlansViewModel GetPricingPlanInfo(AgencyRegistrationViewModel model,
            ResponseViewModel responseViewModel)
        {
            var phPlansmodel = new PaymentHistoryInfoForPlansViewModel
            {
                StripePlanId = responseViewModel.StripePlanId,
                CustomerId = model.StripeUserId,
                StripeSubscriptionId = model.StripeSubscriptionId
            };
            if (responseViewModel.PlanId != null) phPlansmodel.PlanId = responseViewModel.PlanId.Value;
            phPlansmodel.EventId = "";
            phPlansmodel.CreatedBy = model.ID;
            phPlansmodel.OrganizationID = model.ID;
            phPlansmodel.PlanName = responseViewModel.PlanName;
            if (responseViewModel.PlanAmount != null) phPlansmodel.PlanAmount = responseViewModel.PlanAmount.Value;
            phPlansmodel.CreatedDate = DateTime.UtcNow;
            phPlansmodel.StartDate = DateTime.UtcNow;
            return phPlansmodel;
        }

        [Route("UpdateAgency")]
        [HttpPost]
        public HttpResponseMessage UpdateAgency([FromBody] AgencyRegistrationViewModel model)
        {
            ResponseInformation transaction;

            var responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                var enumerable = errors as IList<object> ?? errors.Cast<object>().ToList();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(enumerable);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(enumerable);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }

            

            _iagencyRegistrationService.UpdateAgency(model, out transaction);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;

            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

        [Route("UpdatePayment")]
        [HttpPost]
        public HttpResponseMessage UpdatePayment([FromBody] AgencyPaymentViewModel model)
        {
            ResponseInformation transaction;

            var responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                var enumerableErr = errors as object[] ?? errors.Cast<object>().ToArray();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(enumerableErr);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(enumerableErr);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var errMsg = UpdateCardDetails(model);
            if (errMsg != "Updated")
            {
                responceViewModel.ReturnMessage.Add(errMsg);
                responceViewModel.IsSuccess = false;
               // responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.OK, responceViewModel);
                return badresponse;
            }
                _iagencyRegistrationService.UpdatePayment(model, out transaction);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;

            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    
        [Route("UpdateAgencyByTimeClockUsers")]
        [HttpPost]
        public HttpResponseMessage UpdateAgencyByTimeClockUsers([FromBody] AgencyRegistrationViewModel model)
        {
            ResponseInformation transaction;
            var responceViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                var enumerableErr = errors as IList<object> ?? errors.Cast<object>().ToList();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(enumerableErr);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(enumerableErr);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            #region Start Payment process
            responceViewModel = PaymentProcessStartForTimeClock(model);
            HttpResponseMessage response;
            if (responceViewModel.IsSuccess == false)
            {
                response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
                return response;
            }
            var phPlansmodel = GatTimeClockPlanInfo(model);
            ResponseInformation res;
            _iPaymentHisoryPlanInfoService.AddPaymentHistoryInfoForPlans(phPlansmodel, out res);
            if (res.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }

            #endregion


            _iagencyRegistrationService.UpdateAgencyByTimeClockUsers(model, out transaction);
            model.TotalTimeClockUsers = transaction.TotalTimeClockUsers;
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

        private PaymentHistoryInfoForPlansViewModel GatTimeClockPlanInfo(AgencyRegistrationViewModel model)
        {
            var stripePlanData =
                (TimeClockUsersPlan) GetStripeTimeClockPlan(model.TimeClockUsersPlanId ?? 0).Content;
            var phPlansmodel = new PaymentHistoryInfoForPlansViewModel
            {
                StripePlanId = model.StripePlanId,
                CustomerId = model.StripeUserId,
                StripeSubscriptionId = model.StripeTimeClockSubscriptionId,
                PlanId = model.TimeClockUsersPlanId ?? 0,
                EventId = "",
                CreatedBy = model.ID,
                OrganizationID = model.ID,
                PlanName = stripePlanData.Name,
                PlanAmount = stripePlanData.Price,
                CreatedDate = DateTime.UtcNow,
                StartDate = DateTime.UtcNow
            };
            return phPlansmodel;
        }

        [Route("UpdateAgencyPricingPlan")]
        [HttpPost]
        public HttpResponseMessage UpdateAgencyPricingPlan([FromBody] AgencyRegistrationViewModel model)
        {
            ResponseInformation transaction;
            var resp = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                var enumerableErr = errors as IList<object> ?? errors.Cast<object>().ToList();
                resp.ReturnMessage = ModelStateHelper.ReturnErrorMessages(enumerableErr);
                resp.ValidationErrors = ModelStateHelper.ReturnValidationErrors(enumerableErr);
                resp.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, resp);
                return badresponse;
            }

            #region Cancel old plan and add new by Parag Balapure

            var stripeSubscriptionData = GetStripeSubscriptionId(model.ID);
            var stripePlanData= (PricingPlan)GetStripePlan(model.PricingPlanId).Content;
            if (!string.IsNullOrEmpty(stripeSubscriptionData.StripeUserId))
            {
                try
                {
                    CancelSubscription(stripeSubscriptionData.StripeUserId, stripeSubscriptionData.StripeSubscriptionId);
                }
                catch (Exception)
                {
                    // ignored
                }
                StripeSubscription setSubObject = SetSubscription(stripeSubscriptionData.StripeUserId, stripePlanData.StripePlanId);
                if (setSubObject.Status == "active")
                {
                    model.StripeSubscriptionId = setSubObject.Id;

                    var phPlansmodel = new PaymentHistoryInfoForPlansViewModel
                    {
                        StripePlanId = stripePlanData.StripePlanId,
                        CustomerId = stripeSubscriptionData.StripeUserId,
                        StripeSubscriptionId = stripeSubscriptionData.StripeSubscriptionId,
                        PlanId = stripePlanData.ID,
                        EventId = "",
                        CreatedBy = model.ID,
                        OrganizationID = model.ID,
                        PlanName = stripePlanData.Name,
                        PlanAmount = stripePlanData.Price,
                        CreatedDate = DateTime.UtcNow,
                        StartDate = DateTime.UtcNow
                    };
                    ResponseInformation res;
                    _iPaymentHisoryPlanInfoService.AddPaymentHistoryInfoForPlans(phPlansmodel, out res);
                    if (res.ReturnStatus == false)
                    {
                        var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, resp);
                        return badresponse;
                    }
                }
            }
            else
            {
                //handle error
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, resp);
                return badresponse;

            }
             
            #endregion


            _iagencyRegistrationService.UpdateAgencyPricingPlan(model, out transaction);
            model.TotalTimeClockUsers = transaction.TotalTimeClockUsers;
            resp.Content = model;
            resp.IsSuccess = transaction.ReturnStatus;
            resp.ReturnMessage = transaction.ReturnMessage;
            resp.ValidationErrors = transaction.ValidationErrors;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, resp);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, resp);
            return response;
        }

        #region PaymentMethods by Parag Balapure
        public string UpdateCardDetails(AgencyPaymentViewModel model)
        {
            string cardUpdatedStatus;
            try
            {


                var subCusSvc = new StripeCustomerService
                {
                    ApiKey = ConfigurationManager.AppSettings["StripePrivateKey"]
                };
                var newOption = new StripeCustomerUpdateOptions
                {
                    SourceCard = new SourceCard
                    {
                        Number = model.CardNumber,
                        Name = model.CardName,
                        ExpirationMonth = model.CardExpiryMonth.ToString(),
                        ExpirationYear = model.CardExpiryYear.ToString(),
                        Cvc = model.CVV,
                        Description = "Update my new cards"
                    }
                };
                subCusSvc.Update(model.StripeUserId, newOption);

                cardUpdatedStatus = "Updated";
            }
            catch (Exception ex)
            {
                cardUpdatedStatus = ex.Message;
            }
            return cardUpdatedStatus;
        }
    private ResponseViewModel PaymentProcessStart(AgencyRegistrationViewModel model)
        {
            ResponseViewModel responseViewModel;
            GetToken(model,out responseViewModel);
            model.StripeToken = responseViewModel.StripeToken;
            if (model.StripeToken != null)
            {
                var currentCustomer = CreateCustomer(model);
                model.StripeUserId = currentCustomer.Id;
                //Add code to clean previous subscription
                var stripePlanData = (PricingPlan)GetStripePlan(model.PricingPlanId).Content;
                var subsc = SetSubscription(model.StripeUserId, stripePlanData.StripePlanId, stripePlanData.IsTrial ?? false);
                if (subsc.Status == "active")
                {
                    model.StripeSubscriptionId = subsc.Id;
                    model.IsTrial = false;
                    model.TrialEnd = null;
                    model.TrialStart = null;
                    responseViewModel.PlanAmount = stripePlanData.Price;
                    responseViewModel.PlanName = stripePlanData.Name;
                    responseViewModel.PlanId = stripePlanData.ID;
                    responseViewModel.StripePlanId = stripePlanData.StripePlanId;
                    responseViewModel.IsSuccess = true;
                    return responseViewModel;
                }
                else
                {
                    if (subsc.Status == "trialing")

                    {
                        model.StripeSubscriptionId = subsc.Id;
                        model.IsTrial = true;
                        if (subsc.TrialEnd != null) model.TrialEnd = _icommonservice.ConvertToUTC(subsc.TrialEnd.Value);
                        if (subsc.TrialStart != null)
                            model.TrialStart =_icommonservice.ConvertToUTC(subsc.TrialStart.Value);
                        responseViewModel.PlanAmount = stripePlanData.Price;
                        responseViewModel.PlanName = stripePlanData.Name;
                        responseViewModel.PlanId = stripePlanData.ID;
                        responseViewModel.StripePlanId = stripePlanData.StripePlanId;
                        responseViewModel.IsSuccess = true;
                        return responseViewModel;
                    }
                    responseViewModel.IsSuccess = false;
                    return responseViewModel;
                }
            }
        responseViewModel.IsSuccess = false;
        return responseViewModel;
        //Handle Token Creation Failure
        }


        private static ResponseViewModel PaymentProcessStartForTimeClock(AgencyRegistrationViewModel model)
        {
            var responseViewModel = new ResponseViewModel();
            try
            {
                if (model.StripeUserId != null)
                {
                    //Add code to add timeclockuser subscription
                    StripeSubscription subsc = SetSubscription(model.StripeUserId, model.StripePlanId);
                    if (subsc.Status == "active")
                    {
                        if (!string.IsNullOrEmpty(model.StripeTimeClockSubscriptionId))
                        CancelSubscription(model.StripeUserId, model.StripeTimeClockSubscriptionId);

                        model.StripeTimeClockSubscriptionId = subsc.Id;
                        responseViewModel.IsSuccess = true;
                        return responseViewModel;
                    }
                    responseViewModel.IsSuccess = false;
                    return responseViewModel;
                }
                responseViewModel.IsSuccess = false;
                return responseViewModel;
                //Handle Token Creation Failure
            }
            catch (Exception ex)
            {
                responseViewModel.IsSuccess = false;
                responseViewModel.ReturnMessage.Add(ex.Message);
                return responseViewModel;
            }
        }
        private static void GetToken(AgencyRegistrationViewModel model,out ResponseViewModel responseViewModel)
        {
            responseViewModel = new ResponseViewModel();
            try
            {
                var myToken = new StripeTokenCreateOptions
                {
                    Card = new StripeCreditCardOptions
                    {
                        Number = model.CardNumber,
                        ExpirationYear = model.CardExpiryYear.ToString(),
                        ExpirationMonth = model.CardExpiryMonth.ToString(),
                        Cvc = model.CVV.ToString(CultureInfo.CurrentCulture),
                        Name = model.CardName
                    }
                };
                var tokenService = new StripeTokenService(ConfigurationManager.AppSettings["StripePrivateKey"]);
                var stripeToken = tokenService.Create(myToken);
                responseViewModel.StripeToken= stripeToken.Id;
            }
            catch (StripeException ex)
            {
                responseViewModel.ValidationErrors.Add("TokenError", ex.Message);
                //throw ex;
                //Catch Token Failure Reason
            }
        }
        private static StripeCustomer CreateCustomer(AgencyRegistrationViewModel model)
        {
            var customerService = new StripeCustomerService(ConfigurationManager.AppSettings["StripePrivateKey"]);
            var myCustomer = new StripeCustomerCreateOptions
            {
                Email = model.BillingEmail,
                Description = model.AgencyName,
                SourceToken = model.StripeToken
            };
            return customerService.Create(myCustomer);
        }
        private ResponseViewModel GetStripePlan(long planId)
        {
            //Get plan from database
            ResponseViewModel responseViewModel;
            _iParticipantPlanService.getPricingPlan(planId, out responseViewModel);

            //Get plan from stripe account
            //StripePlanService planSVc = new StripePlanService(ConfigurationManager.AppSettings["StripeApiPrivateKey"]);
            return responseViewModel;
        }
        private ResponseViewModel GetStripeTimeClockPlan(long planId)
        {
            //Get plan from database
            ResponseViewModel responseViewModel;
            _iTimeClockUserPlanService.getTimeClockPlan(planId, out responseViewModel);

            //Get plan from stripe account
            //StripePlanService planSVc = new StripePlanService(ConfigurationManager.AppSettings["StripeApiPrivateKey"]);
            return responseViewModel;
        }
        private StripeDataViewModel GetStripeSubscriptionId(long cusId)
        {
            //Get plan from database
            ResponseViewModel responseViewModel;
            _iagencyRegistrationService.getSubscriptionPlan(cusId, out responseViewModel);
            var c = responseViewModel.Content;
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var sessionVal = serializer.Serialize(c);
            var tt = JsonConvert.DeserializeObject<StripeDataViewModel>(sessionVal);
            return tt;
        }
        
        private static StripeSubscription SetSubscription(string cutId, string planId,bool isTrial=false)
        {
            var subscriptionSvc = new StripeSubscriptionService(ConfigurationManager.AppSettings["StripePrivateKey"]);
            var options = new StripeSubscriptionCreateOptions()
            {
                //TrialPeriodDays = 30
                TrialPeriodDays = 1
                //TrialEnd = DateTime.UtcNow.AddDays(1)
            };
            var subscription = isTrial ? subscriptionSvc.Create(cutId, planId, options) : subscriptionSvc.Create(cutId, planId);
            return subscription;
        }
        private static void CancelSubscription(string customerId, string subscriptionId)
        {
            StripeSubscriptionService subscriptionSvc = new StripeSubscriptionService(ConfigurationManager.AppSettings["StripePrivateKey"]);
            subscriptionSvc.Cancel(customerId, subscriptionId);
        }
        #endregion


    }
}