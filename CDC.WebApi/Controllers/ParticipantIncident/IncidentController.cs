﻿using CDC.Services;
using CDC.Services.Abstract;
using CDC.Services.Abstract.ParticipantIncident;
using CDC.Services.Abstract.RazorEngine;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.NewParent;
using CDC.ViewModel.ParticipantIncident;
using CDC.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.ParticipantIncident
{
    [Authorize]
    [RoutePrefix("api/Incident")]
    public class IncidentController : ApiController
    {
        private readonly IIncidentService _iIncidentservice;
        private readonly ICommonService _icommonservice;
        private readonly IRazorService _iRazorService;
        public IncidentController(IIncidentService iIncidentService, ICommonService icommonservice, IRazorService iRazorService)
        {
            _iIncidentservice = iIncidentService;
            _icommonservice = icommonservice;
            _iRazorService = iRazorService;
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("GetIncidentById")]
        public HttpResponseMessage GetIncidentById(IDViewModel model)
        {
            var incidentData = _iIncidentservice.GetIncidentById(model);
            var resultData = new { incidentData };

            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("GetAllIncident")]
        public HttpResponseMessage GetAllIncident([FromBody] SearchIncidentViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iIncidentservice.GetAllIncident(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        /// <summary>
        /// Adds the incident.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("AddIncident")]
        public HttpResponseMessage AddIncident([FromBody] IncidentViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation responseInfo;
            _iIncidentservice.AddIncident(model, out responseInfo);
            responseViewModel.Content = model;

            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            string MailSubject = AppConstants.ParentIncident;
            string template = CDCEmailTemplates.IncidentReport;
            model.TimeOfAccident = _icommonservice.ConvertTimeFromUTC(model.DateOfAccident.Value, model.TimeOfAccident.Value, model.TimeZone);


            TimeSpan timespan = new TimeSpan(model.TimeOfAccident.Value.Ticks);
            DateTime time = DateTime.Today.Add(timespan);
            string displayTime = time.ToString("hh:mm tt");

            #region SendMailToParents
            List<long?> ids = new List<long?>();
            model.ListStudentInfoIds.ForEach(x =>
            {
                ids.Add(x.studentId.Value);
            });
            model.ListOtherStudentInfoIds.ForEach(x =>
            {
                ids.Add(x.studentId.Value);
            });
            IDViewModel idVm = new IDViewModel();
            idVm.participantIds = ids;
            var parentInfoList = (List<tblParentInfoViewModel>)_icommonservice.GetParentList(idVm).Content;
            foreach (var item in parentInfoList)
            {
                var result = _iRazorService.GetRazorTemplate(template, _icommonservice.GetMd5Hash(template), new
                {
                    Url = url,
                    Age = model.AgeOfChild,
                    Informed = model.ParentInformed == true ? "Yes" : "No",
                    Participantname = model.ParticipantName,
                    ParentName = model.ParentName == null,
                    ParentDescription = model.ParentDescription == null,
                    Place = model.PlaceOfAccident,
                    Date = Convert.ToString(model.DateOfAccident.Value.ToShortDateString()),
                    Time = displayTime,
                    Nature = model.NatureOfInjury,
                    model.OtherTreatment,
                    Involvedparticipant = model.OtherParticipants == null,
                    Descripition = model.DescriptionOfInjury,
                    Action = model.ActionTaken,
                    ReportedBy = model.ReporterName,
                    TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo = url + ConfigurationManager.AppSettings["Logo"]
                });
                _icommonservice.SendEmail(item.EmailId, MailSubject, result);
            }
            #endregion
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }
        [Authorize(Roles = "Agency,Family")]
        [HttpPost]
        [Route("UpdateIncidentRead")]
        public HttpResponseMessage UpdateIncidentRead([FromBody] IncidentIdViewModel model)
        {
            ResponseInformation responseViewModel;
            _iIncidentservice.UpdateIncidentById(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteIncident")]
        public HttpResponseMessage DeleteIncidentById(long incidentId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iIncidentservice.DeleteIncidentById(incidentId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }

}

