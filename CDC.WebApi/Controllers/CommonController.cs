﻿using System.Collections.Generic;
using System.Web.Http;
using CDC.Services.Abstract;
using CDC.ViewModel;
using CDC.ViewModel.Tools.Staff;
using System.Net.Http;
using System.Net;
using CDC.ViewModel.Common;
using System.Web;
using System;
using CDC.ViewModel.Tools.Student;
using CDC.Entities.Pricing;
using System.Configuration;
using System.Linq;
using CDC.Entities.TimeClockUsers;
using CDC.Services;
using CDC.Services.Abstract.RazorEngine;

namespace CDC.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    //[Authorize]
    [RoutePrefix("api/common")]
    public class CommonController : ApiController
    {
        private readonly ICommonService _icommonservive;
        private readonly IRazorService _iRazorService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonController"/> class.
        /// </summary>
        /// <param name="iCommonservice">The iCommonservice.</param>
        /// <param name="iRazorService"></param>
        public CommonController(ICommonService iCommonservice, IRazorService iRazorService)
        {
            _icommonservive = iCommonservice;
            _iRazorService = iRazorService;
        }
        /// <summary>
        /// Gets all country.
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("country")]
        public List<CountryViewModel> GetAllCountry()
        {
            return _icommonservive.GetCountryList();
        }
        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <param name="countryId">The country identifier.</param>
        
        [AllowAnonymous]
        [HttpPost]
        [Route("state")]
        //[System.Web.Mvc.OutputCache(Duration = 86400, VaryByParam = "countryId")]
        public List<StateViewModel> GetState([FromBody] long countryId)
        {
            return _icommonservive.GetStateByCountryId(countryId);
        }

        /// <summary>
        /// Gets the city.
        /// </summary>
        /// <param name="stateId">The state identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("getstatescity")]
        //[System.Web.Mvc.OutputCache(Duration = 86400, VaryByParam = "stateId")]
        public List<CityViewModel> GetCity([FromBody] long stateId)
        {
            return _icommonservive.GetCityByCountryId(stateId);
        }

        /// <summary>
        /// Gets the designation.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getPositions")]
        public List<PositionViewModel> GetPositions([FromBody]long agencyId)
        {
            return _icommonservive.GetPositions(agencyId);
        }
        /// <summary>
        /// Determines whether [is exist name] [the specified test].
        /// </summary>
        /// <param name="test">The test.</param>
        /// <returns>
        ///   <c>true</c> if [is exist name] [the specified test]; otherwise, <c>false</c>.
        /// </returns>
        [HttpPost]
        [Route("isexistname")]
        public bool IsExistName([FromBody] Test test)
        {
            return _icommonservive.IsExistClassName(test.Name.Trim(), test.classId, test.AgencyId);
        }

        [HttpPost]
        [Route("isexistTitle")]
        public bool IsExistTitle([FromBody] Test test)
        {
            return _icommonservive.IsExistTitle(test.Name.Trim(), test.AgencyId);
        }

        public bool IsExistEmailId([FromBody] EmailExist obj)
        {
            return _icommonservive.IsExistEmailId(obj.Name);
        }
        public class EmailExist
        {
            public string Name { get; set; }
            // public long AgencyId { get; set; }

        }

        public class Test
        {
            public string Name { get; set; }

            public long classId { get; set; }

            public long AgencyId { get; set; }
        }

        [HttpPost]
        [Route("GetClass_Room_Lesson")]
        public HttpResponseMessage GetClass_Room_Lesson(SearchFieldsViewModel SearchVM)
        {
            var response = _icommonservive.GetClass_Room_Lesson(SearchVM);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetStaffSchedule")]
        public HttpResponseMessage GetStaffSchedule()
        {
            var response = _icommonservive.GetStaffSchedule();
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetClass_Room_LessonByAgency")]
        public HttpResponseMessage GetClass_Room_LessonByAgency([FromBody] long agencyId)
        {
            var response = _icommonservive.GetClass_Room_LessonByAgency(agencyId);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("imageUpload")]
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            var response = new ResponseViewModel();
            var httpRequest = HttpContext.Current.Request;
            try
            {
                var postedFile = httpRequest.Files[0];
                var FolderPath = "Images/ProfilePic/";
                var filename = Guid.NewGuid() + ".jpg";
                if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("~/" + FolderPath)))
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + FolderPath));
                var filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + FolderPath + filename);
                if (filePath != null) postedFile.SaveAs(filePath);
                response.Content = FolderPath + filename;
                response.IsSuccess = true;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpPost]
        [Route("getFeesTypes")]
        public HttpResponseMessage GetFeesTypes(FeeTypeViewModel feeTypeViewModel)
        {
            var response = _icommonservive.GetFeesTypes(feeTypeViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetStaffList")]
        public HttpResponseMessage GetStaffList(IDViewModel model)
        {
            var response = _icommonservive.GetStaffList(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("getPricingPlans")]
        public HttpResponseMessage GetPricingPlans(PricingPlanViewModel pricingPlanViewModel)
        {
            var response = _icommonservive.GetPricingPlans(pricingPlanViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpPost]
        [Route("getLocations")]
        public HttpResponseMessage GetLocations(IDViewModel model)
        {
            var response = _icommonservive.GetLocations(model.AgencyId);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpPost]
        [Route("getSMSCarriers")]
        public HttpResponseMessage GetSMSCarriers([FromBody]long agencyId)
        {
            var response = _icommonservive.GetSMSCarrier();
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles ="Agency")]
        [HttpPost]
        [Route("mailToGuardians")]
        public HttpResponseMessage MailToGuardians([FromBody]EmailDataViewModel model)
        {
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url= currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            var mailTemplate = CDCEmailTemplates.MailToGuardianTemplate;
            foreach (var item in model.GuardianInfo)
            {
                string message1 = "Hi, " + item.FirstName + " " + item.LastName + " . " + "</br>This is an emergency message about ";
                var participants = model.Regards.Aggregate("", (current, participant) => current + (participant.FirstName + " " + participant.LastName + ","));
                if (participants.Length > 0)
                    participants = participants.Substring(0, participants.Length - 1);
                message1 += participants + " " + model.Messages + "</br><p>Regards </br>" + model.AgencyName + "</p>";

                var result= _iRazorService.GetRazorTemplate(mailTemplate, _icommonservive.GetMd5Hash(mailTemplate),new
                {
                    Url = url, item.FirstName, item.LastName,
                    Message = model.Messages,
                    Participant = participants, model.AgencyName,
                    TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo = url + ConfigurationManager.AppSettings["Logo"]
                });
                _icommonservive.SendEmail(item.EmailId, model.Subject, result);
                // For SMS
                _icommonservive.SendEmail(item.Mobile + "@" + item.SMSCarrier.CarrierAddress, model.Subject, message1);
            }
            var resultData = "OK";
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("getTimeClockUsersPricingPlans")]
        public HttpResponseMessage GetTimeClockUsersPricingPlans(TimeClockUsersViewModel timeClockUsersPlanViewModel)
        {
            var response = _icommonservive.GetTimeClockUsersPricingPlans(timeClockUsersPlanViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpPost]
        [Route("getAllEnrollList")]
        public HttpResponseMessage GetAllEnrollList(SearchStudentScheduleViewModel model)
        {
            var response = _icommonservive.GetAllEnrollList(model);

            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("getPricingPlanCount")]
        public HttpResponseMessage GetPricingPlanCount(SearchFieldsViewModel model)
        {
            var response = _icommonservive.GetPricingPlanCount(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [HttpPost]
        [Route("GetAllParentList")]
        public HttpResponseMessage GetAllParentList([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.GetAllParentList(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [HttpPost]
        [Route("GetDateNew")]
        public HttpResponseMessage GetDateNew([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.ConvertFromUTC(DateTime.UtcNow, model.TimeZone);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        [HttpPost]
        [Route("GetDeleteMessage")]
        public HttpResponseMessage  CheckDeleteClass([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.CheckDeleteClass(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        [HttpPost]
        [Route("GetParticipantDeleteMessage")]
        public HttpResponseMessage CheckParticipantDeleteClass(SearchStudentScheduleViewModel model)
        {
            var responseViewModel = _icommonservive.CheckParticipantDeleteClass(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


        [HttpPost]
        [Route("GetAllParticipants")]
        public HttpResponseMessage GetAllParticipants([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.GetAllParticipants(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [HttpPost]
        [Route("GetAllOtherParticipants")]
        public HttpResponseMessage GetAllOtherParticipants([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.GetAllOtherParticipants(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        
        [HttpPost]
        [Route("GetAllStaff")]
        public HttpResponseMessage GetAllStaff([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.GetAllStaff(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [HttpPost]
        [Route("GetParentInformationByStudent")]
        public HttpResponseMessage GetParentInformationByStudent([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.GetParentInformation(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [HttpPost]
        [Route("GetDailyStatusCategory")]
        public HttpResponseMessage GetDailyStatusCategory([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.GetDailyStatusCategory(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [HttpPost]
        [Route("GetPositionNameById")]
        public HttpResponseMessage GetPositionNameById([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.GetPositionNameById(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [HttpPost]
        [Route("GetParticipantInformation")]
        public HttpResponseMessage GetParticipantInformation([FromBody]IDViewModel model)
        {
            var responseViewModel = _icommonservive.GetParticipantInformation(model);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }
        [HttpPost]
        [Route("GetClassCombo")]
        public HttpResponseMessage GetClassCombo(SearchFieldsViewModel SearchVM)
        {
            var response = _icommonservive.GetClassCombo(SearchVM);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpPost]
        [Route("GetMealType")]
        public HttpResponseMessage GetMealType(SearchFieldsViewModel SearchVM)
        {
            var response = _icommonservive.GetMealType(SearchVM);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [HttpPost]
        [Route("GetFoodMster")]
        public HttpResponseMessage GetFoodMster(SearchFieldsViewModel SearchVM)
        {
            var response = _icommonservive.GetFoodMaster(SearchVM);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
      
        [AcceptVerbs("POST", "GET")]
        [Route("GetFoodItems")]
        public HttpResponseMessage GetFoodItems(string AgencyId,string MealTypeId)
        {
            var response = _icommonservive.GetFoodItems(Convert.ToInt32(AgencyId), Convert.ToInt32(MealTypeId));
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

    
        [AcceptVerbs("POST")]
        [Route("GetMealItemMaster")]
        public HttpResponseMessage GetMealItemMaster(SearchFieldsViewModel model)
        {
            var response = _icommonservive.GetMealItemMaster(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
       
        [AcceptVerbs("POST")]
        [Route("getLeaveStatusData")]
        public HttpResponseMessage getLeaveStatusData(SearchFieldsViewModel model)
        {
            var response = _icommonservive.getLeaveStatusData(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
    
        [AcceptVerbs("POST")]
        [Route("GetMealServeSizeMaster")]
        public HttpResponseMessage GetMealServeSizeMaster(SearchFieldsViewModel model)
        {
            var response = _icommonservive.GetMealServeSize(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
      
        [AcceptVerbs("POST")]
        [Route("GetMealServeQuantityMaster")]
        public HttpResponseMessage GetMealServeQuantityMaster(SearchFieldsViewModel model)
        {
            var response = _icommonservive.GetMealServeQuantity(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
      
        [AcceptVerbs("POST")]
        [Route("GetMealServeTypesMaster")]
        public HttpResponseMessage GetMealServeTypesMaster(SearchFieldsViewModel model)
        {
            var response = _icommonservive.GetMealServeTypes(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
     
        [AcceptVerbs("POST")]
        [Route("CheckParticipantAttedance")]
        public HttpResponseMessage CheckParticipantAttedance(SearchFieldsViewModelForParticipants model)
        {
            var response = _icommonservive.CheckParticipantAttendance(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
      
        [AcceptVerbs("POST")]
        [Route("GetModeOfConvinience")]
        public HttpResponseMessage GetModeOfConvinience(SearchFieldsViewModel model)
        {
            var response = _icommonservive.GetModeOfConvinience(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        
    }
}