﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.Schedule;
using CDC.ViewModel.Common;
using CDC.ViewModel.Schedule;
using CDC.WebApi.Helpers;
using CDC.Services.Abstract.ParticipantEnrollmentPayment;
using CDC.Entities.Enums;
using CDC.ViewModel.ParticipantEnrollmentPayment;

namespace CDC.WebApi.Controllers.Schedule
{
    /// <summary>
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [Authorize]
    [RoutePrefix("api/StudentSchedule")]
    public class StudentScheduleController : ApiController
    {
        private readonly IStudentScheduleService _istudentscheduleservice;
        private readonly IParticipantEnrollmentPaymentService _iparticipantEnrollPaymentservice;

        public StudentScheduleController(IStudentScheduleService istudentscheduleservice, IParticipantEnrollmentPaymentService iparticipantEnrollPaymentservice)
        {
            _istudentscheduleservice = istudentscheduleservice;
            _iparticipantEnrollPaymentservice = iparticipantEnrollPaymentservice;
        }

        /// <summary>
        /// Gets the student schedules.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("GetStudentSchedules")]
        public HttpResponseMessage GetStudentSchedules(StudentScheduleViewModelNew model)
        {
            ResponseInformation transaction;
            ResponseViewModel responseViewModel = new ResponseViewModel();
            var studentScheduleList = _istudentscheduleservice.GetAllStudentSchedule(model, out transaction);
            responseViewModel.Content = studentScheduleList;
            responseViewModel.ReturnStatus = transaction.ReturnStatus;
            responseViewModel.ReturnMessage = transaction.ReturnMessage;

            if (transaction.ReturnStatus)
            {
                var response = Request.CreateResponse(HttpStatusCode.OK, responseViewModel);
                return response;
            }

            var badResponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
            return badResponse;
        }
        /// <summary>
        /// Adds the schedule.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("AddSchedule")]
        public HttpResponseMessage AddSchedule([FromBody] StudentScheduleViewModel model)
        {
           
            ResponseInformation transaction;
            ResponseViewModel responseViewModel = new ResponseViewModel();
            //responseViewModel.IsSuccess = true; // dummy entry
            //responseViewModel.Id = 1;
            //var response2 = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            //return response2;

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            _istudentscheduleservice.AddStudentSchedule(model, out transaction);
            model.ID = transaction.ID;
            responseViewModel.Content = model;
            responseViewModel.ReturnStatus = transaction.ReturnStatus;
            responseViewModel.IsSuccess = transaction.ReturnStatus;
            responseViewModel.ReturnMessage = transaction.ReturnMessage;
            responseViewModel.ValidationErrors = transaction.ValidationErrors;
            responseViewModel.IsExist = transaction.IsExist;
            responseViewModel.IsEligible = transaction.IsEligible;

            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            if (!transaction.IsExist)
            {
                if(transaction.IsEligible==true)
                {
                    ParticipantEnrollPaymentViewModel viewModel = new ParticipantEnrollPaymentViewModel();
                    viewModel.AgencyId = model.AgencyId;
                    viewModel.PaymentStatus = (int)EnumPaymentStatus.Unpaid;
                    viewModel.ScheduleId = model.ID;
                    viewModel.scheduleDate = model.StartDate;
                    ResponseInformation responseInfo;
                    _iparticipantEnrollPaymentservice.AddPayInfo(viewModel, out responseInfo);
                }
            }

            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }

        /// <summary>
        /// Updates the schedule.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [Route("UpdateSchedule")]
        [HttpPost]
        public HttpResponseMessage UpdateSchedule([FromBody] StudentScheduleViewModel model)
        {
            ResponseInformation transaction;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            model.IsDeleted = false; //dummy entry
            model.StudentId = 1; // dummy entry
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }

            _istudentscheduleservice.UpdateStudentSchedule(model, out transaction);
            responceViewModel.Content = model;
            responceViewModel.ReturnStatus = transaction.ReturnStatus;
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;

            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
        /// <summary>
        /// Deletes the schedule.
        /// </summary>
        /// <param name="scheduleId">The schedule identifier.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [Route("DeleteSchedule")]
        [HttpPost]
        public HttpResponseMessage DeleteSchedule(long scheduleId)
        {
            ResponseInformation transaction;
            ResponseViewModel responceViewModel = new ResponseViewModel();
            _istudentscheduleservice.DeleteStudentScheduleById(scheduleId, out transaction);
            responceViewModel.ReturnStatus = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
        [Authorize(Roles = "Agency,Family")]
        [Route("GetUnpaidHistory")]
        [HttpPost]
        public HttpResponseMessage GetUnpaidHistory(SearchStudentScheduleViewModel model)
        {
            ResponseInformation transaction;
            ResponseViewModel responceViewModel = new ResponseViewModel();
           var unPaidHistory= _istudentscheduleservice.GetUnPaidHistory(model, out transaction);
            responceViewModel.ReturnStatus = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;
            responceViewModel.TotalRows = transaction.TotalRows;
            responceViewModel.Content = unPaidHistory;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("UpdateEnrollPaymentInfo")]
        public HttpResponseMessage UpdateEnrollPaymentInfo(ParticipantEnrollPaymentViewModel model)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            _iparticipantEnrollPaymentservice.UpdatePayInfo(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}