﻿using CDC.Services.Abstract;
using CDC.Services.Abstract.Location;
using CDC.ViewModel.Common;
using CDC.ViewModel.Location;
using CDC.WebApi.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Location
{
    [Authorize]
    [RoutePrefix("api/Location")]
    public class LocationController : ApiController
    {
        private readonly ILocationService _iLocationservice;

        /// <summary>
        ///  Initializes a new instance of the <see cref="LocationController" /> class.
        /// </summary>
        /// <param name="iLocationService"></param>
        /// <param name="icommonservice"></param>
        public LocationController(ILocationService iLocationService, ICommonService icommonservice)
        {
            _iLocationservice = iLocationService;
        }
        [HttpPost]
        [Route("GetLocationById")]
        public HttpResponseMessage GetLocationById(long LocationId)
        {
            var locationList = _iLocationservice.GetLocationById(LocationId);
            var resultData = new { LocationList = locationList };
          
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        /// <summary>
        /// Gets all classes.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllLocation")]
        public HttpResponseMessage GetAllLocation([FromBody] SearchLocationViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iLocationservice.GetAllLocation(model,out responseViewModel);        
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        /// <summary>
        /// Adds the Location.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddLocation")]
        public HttpResponseMessage AddLocation([FromBody] LocationViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation responseInfo;
            _iLocationservice.AddLocation(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
                var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
                return response;
        }

        [HttpPost]
        [Route("UpdateLocation")]
        public HttpResponseMessage UpdateLocation(LocationViewModel model)
        {
            ResponseInformation responseInfo;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            _iLocationservice.UpdateLocation(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }


        /// <summary>
        /// Add/Update the Location.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddUpdateLocation")]
        public HttpResponseMessage AddUpdateLocation([FromBody]LocationViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iLocationservice.AddUpdateLocation(model);
            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        /// <summary>
        ///     Deletes the Location by identifier.
        /// </summary>
        /// <param name="LocationId">The Location identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteLocation")]
        public HttpResponseMessage DeleteLocationById(long LocationId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iLocationservice.DeleteLocationById(LocationId,out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
