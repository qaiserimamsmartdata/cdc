﻿using CDC.Services.Abstract.Skills;
using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Skills
{
    [Authorize]
    [RoutePrefix("api/SkillMaster")]
    public class SkillMasterController : ApiController
    {
        private readonly ISkillMasterService _iskillmasterservice;
        public SkillMasterController(ISkillMasterService iskillmasterservice)
        {
            _iskillmasterservice = iskillmasterservice;
        }

        /// <summary>
        /// Add/Update SkillMaster
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddUpdateSkillMaster")]
        public HttpResponseMessage AddUpdateSkillMaster([FromBody] SkillMasterViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iskillmasterservice.AddUpdateSkillMaster(model);
            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }



        /// <summary>
        /// Gets all SkillMaster
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllSkillMaster")]
        public HttpResponseMessage GetAllSkillMaster([FromBody] SearchSkillMasterViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iskillmasterservice.GetAllSkillMaster(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        /// <summary>
        /// Delete SkillMaster
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteSkillMasterById")]
        public HttpResponseMessage DeleteSkillMasterById([FromBody]SkillMasterViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iskillmasterservice.DeleteSkillMaster(model);
            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }


    }
}
