﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.Skills;
using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;

namespace CDC.WebApi.Controllers.Skills
{
    /// <summary>
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [Authorize]
    [RoutePrefix("api/Skills")]
    public class SkillsController : ApiController
    {
        private readonly ISkillService _iskillService;

        public SkillsController(ISkillService iskillservive)
        {
            _iskillService = iskillservive;
        }

        /// <summary>
        ///     Adds the skill.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddSkill")]
        public int AddSkill([FromBody] SkillViewModel model)
        {
            return _iskillService.AddSkill(model);
        }

        /// <summary>
        ///     Gets the skill.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetSkill")]
        public HttpResponseMessage GetSkill([FromBody] SearchSkillViewModel model)
        {
            int count;
            var skill = _iskillService.GetSkills(model, out count);
            var resultData =
                new
                {
                    skill =
                        skill.Select(
                            m =>
                                new
                                {
                                    m.ID,
                                    m.ClassesRequired,
                                    m.DaysRequired,
                                    m.Details,
                                    skill = m.Skill,
                                    m.SubSkill,
                                    categoryId = m.CategoryId,
                                    TotalCount = count
                                })
                };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        /// <summary>
        ///     Updates the skill.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSkill")]
        public HttpResponseMessage UpdateSkill(SkillViewModel model)
        {
            var response = new ResponseViewModel();

            var returnData = _iskillService.UpdateSkill(model, model.ID);

            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        ///     Deletes the skill by identifier.
        /// </summary>
        /// <param name="skillId">The skill identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteSkillById")]
        public HttpResponseMessage DeleteSkillById(long skillId)
        {
            var response = new ResponseViewModel();

            var returnData = _iskillService.DeleteSkillById(skillId);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        ///     Gets the skill by identifier.
        /// </summary>
        /// <param name="skillId">The skill identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSkillById")]
        public HttpResponseMessage GetSkillById(long skillId)
        {
            var skill = _iskillService.GetSkillById(skillId);
            var resultData =
                new
                {
                    skill =
                        new
                        {
                            skill.ID,
                            skill.ClassesRequired,
                            skill.DaysRequired,
                            skill.Details,
                            skill = skill.Skill,
                            skill.SubSkill,
                            categoryId = skill.CategoryId
                        }
                };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
    }
}