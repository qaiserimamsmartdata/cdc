﻿using CDC.Services.Abstract.Skills;
using CDC.ViewModel.Common;
using CDC.ViewModel.Skill;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Skills
{
    [Authorize(Roles = "Staff,Agency")]
    [RoutePrefix("api/StaffSkill")]
    public class StaffSkillController : ApiController
    {

        private readonly IStaffSkillService _iStaffSkillService;

        public StaffSkillController(IStaffSkillService iStaffSkillService)
        {
            _iStaffSkillService = iStaffSkillService;
        }


        /// <summary>
        /// Gets all StaffSkill
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
       
        [HttpPost]
        [Route("GetAllStaffSkill")]
        public HttpResponseMessage GetAllStaffSkill([FromBody] SearchStaffSkillViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iStaffSkillService.GetAllStaffSkill(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }

        [HttpPost]
        [Route("AddUpdateStaffSkill")]
        public HttpResponseMessage AddUpdateStaffSkill([FromBody] StaffSkillViewModel model)
        {
            var response = new ResponseViewModel();
            var returnData = _iStaffSkillService.AddUpdateStaffSkill(model);
            response.IsSuccess = returnData == 1;
            if (returnData == 3)
            {
                response.IsSuccess = false;
                response.Message = "ERROR! There is already a plan with same name.Please add another.";
            }
            else if (returnData == 0)
            {
                response.IsSuccess = false;
                response.Message = "Error! Something went wrong.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }



        /// <summary>
        ///     Deletes StaffSkill by identifier.
        /// </summary>
        /// <param name="staffskillId">The StaffSkillId identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteStaffSkill")]
        public HttpResponseMessage DeleteStaffSkill(long staffskillId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iStaffSkillService.DeleteStaffSkillById(staffskillId, out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
                return response;
            }
        }

    }
}
