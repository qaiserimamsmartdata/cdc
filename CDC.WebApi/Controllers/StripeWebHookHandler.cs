﻿using AT.Net.Service;
using CDC.Data;
using CDC.Entities.PaymentHistory;
using CDC.Services;
using CDC.ViewModel.PaymentHistory;
using Microsoft.AspNet.WebHooks;
using Newtonsoft.Json;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CDC.Entities.ExceptionLogs;

namespace CDC.WebApi.Controllers
{
    public class StripeWebHookHandler : WebHookHandler
    {
        
        public StripeWebHookHandler()
        {
            Receiver = StripeWebHookReceiver.ReceiverName;
        }

        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        public override Task ExecuteAsync(string generator, WebHookHandlerContext context)
        {
            StripeEvent entry = context.GetDataOrDefault<StripeEvent>();
            PaymentHistoryInfo mapData = new PaymentHistoryInfo();
            // 'https://stripe.com/docs/webhooks'
            if (entry != null)
            {
                var data = JsonConvert.SerializeObject(entry.Data);
                var obj = JsonConvert.DeserializeObject<StripeEventDataViewModel>(data);


                if (obj != null)
                {
                    using (CDCContext db = new CDCContext())
                    {
                        if (entry.EventType == "customer.subscription.created" || entry.EventType == "invoice.payment_succeeded" || entry.EventType == "invoice.payment_failed" || entry.EventType == "customer.subscription.updated"|| entry.EventType == "charge.succeeded")

                        {
                              
                            var orgData = db.AgencyRegistrationInfoSet.FirstOrDefault(a => a.StripeUserId == obj.@object.customer);
                            if (orgData != null)
                            {
                                var planDetail = db.PaymentHistoryInfoSet.Where(a => a.OrganizationID == orgData.ID).ToList().LastOrDefault();
                                if (planDetail != null)
                                {
                                    planDetail.IsFinal = false;
                                    db.SaveChanges();
                                }
                                mapData.CreatedBy = orgData.ID;
                                mapData.OrganizationID = orgData.ID;
                                mapData.OrganizationName = orgData.AgencyName;
                                mapData.CreatedDate = entry.Created;
                                mapData.IsDeleted = false;
                                mapData.IsFinal = true;
                                mapData.StripeSubscriptionId = obj.@object.id;

                                mapData.EventType = entry.EventType;
                                mapData.EventId = entry.Id;
                                // mapData.CreatedDate = entry.Created;
                                mapData.LiveMode = entry.LiveMode;
                                mapData.JsonObject = entry.Object;


                                ////Getting Information about plans (not getting plan id in case of payment success and failed)
                                if (entry.EventType == "customer.subscription.created")
                                {
                                }
                                //var planData = db.PricingPlanSet.Where(a => a.StripePlanId == StripPlanId).ToList().FirstOrDefault();
                                //if (planData != null)
                                //{
                                //    mapData.PlanAmount = planData.Price;
                                //    mapData.PlanId = planData.ID;
                                //    mapData.PlanName = planData.Name;
                                //}
                                //var TimeClockplanData = db.TimeClockUsersPlanSet.Where(a => a.StripePlanId == StripeTimeCLockPlanId).ToList().FirstOrDefault();
                                //if (TimeClockplanData != null)
                                //{
                                //    mapData.PlanAmount = TimeClockplanData.Price;
                                //    mapData.PlanId = TimeClockplanData.ID;
                                //    mapData.PlanName = TimeClockplanData.Name;
                                //}

                                var planData = db.PaymentHistoryInfoForPlansSet.Where(a => a.CustomerId == obj.@object.customer).OrderByDescending(m=>m.ID).ToList();
                                if (planData.Count>0)
                                {
                                    mapData.PlanAmount = planData.FirstOrDefault().PlanAmount;
                                    mapData.PlanId = planData.FirstOrDefault().PlanId;
                                    mapData.PlanName = planData.FirstOrDefault().PlanName;
                                }
                                ///////////////////////////////////////////////////////////

                                var emailid = orgData.AgencyBillingInfos.FirstOrDefault().BillingEmail;
                                var PhoneNumber = orgData.AgencyContactInfos.FirstOrDefault().PhoneNumber;
                                var TimeZone = orgData.AgencyTimeZones.FirstOrDefault().TimeZone;
                                mapData.EmailAddress = emailid;

                                mapData.PhoneNumber = PhoneNumber;
                                mapData.balance_transaction = obj.@object.balance_transaction;
                                mapData.CustomerId = obj.@object.customer;
                                switch (entry.EventType)
                                {
                                    case "customer.subscription.created":
                                        if (obj.@object.status == "active")
                                        {
                                            mapData.StartDate = UnixTimeStampToDateTime(obj.@object.current_period_start);
                                            mapData.PlanExpiryDate = UnixTimeStampToDateTime(obj.@object.current_period_end);
                                        }
                                        break;
                                    case "invoice.payment_succeeded":
                                        mapData.StartDate = DateTime.UtcNow;
                                        mapData.PlanExpiryDate = DateTime.UtcNow;
                                            
                                        ////for superadmin mails
                                        //Thread superEmailThrd = new Thread(delegate ()
                                        //{
                                        //    SendEmail("pinwheel.trinity@gmail.com", OrgData.AgencyName, "Transaction has been done successfully.");
                                        //});
                                        //superEmailThrd.Start();

                                        ////for agency mails
                                        //Thread agencyEmailThrd = new Thread(delegate ()
                                        //{
                                        //    SendEmail(emailid, OrgData.AgencyName, "Transaction has been done successfully");
                                        //});
                                        //agencyEmailThrd.Start();
                                        break;
                                    case "charge.succeeded":
                                        mapData.StartDate = DateTime.UtcNow;
                                        mapData.PlanExpiryDate = DateTime.UtcNow;
                                            
                                        //for superadmin mails
                                        SendEmail("pinwheel.trinity@gmail.com", orgData.AgencyName,mapData.PlanName,mapData.PlanAmount,mapData.balance_transaction,mapData.CreatedDate,TimeZone,"Transaction has been done successfully.");

                                        //for agency mails
                                        SendEmail(emailid, orgData.AgencyName, mapData.PlanName, mapData.PlanAmount, mapData.balance_transaction, mapData.CreatedDate,TimeZone, "Transaction has been done successfully");
                                        break;
                                    case "invoice.payment_failed":
                                        mapData.StartDate = DateTime.UtcNow;
                                        mapData.PlanExpiryDate = DateTime.UtcNow;

                                        //for superadmin mails
                                        SendEmail("pinwheel.trinity@gmail.com", orgData.AgencyName, mapData.PlanName, mapData.PlanAmount, mapData.balance_transaction, mapData.CreatedDate, TimeZone, "Transaction has been declined.");
                                        break;
                                    //for agency mails
                                        SendEmail(emailid, orgData.AgencyName, mapData.PlanName, mapData.PlanAmount, mapData.balance_transaction, mapData.CreatedDate, TimeZone, "Transaction has been declined.");
                                        break;
                                }
                                    db.PaymentHistoryInfoSet.Add(mapData);
                                    db.SaveChanges();
                                
                            }
                        }
                    }
                    // status = _superAdminService.SaveStripeEventata(obj);

                    switch (entry.EventType)
                    {
                        //    ////Edited by parag
                        case "charge.succeeded":
                            break;
                        //    case "invoice.created":
                        //        break;
                        //    case "customer.subscription.created":
                        //        break;
                        //    case "customer.source.created":
                        //        break;
                        //    case "invoice.payment_succeeded":
                        //        break;
                        //    //case "charge.refunded":
                        //    //    // do work
                        //    //    break;
                        //    case "customer.subscription.updated":
                        //        break;
                        //    //case "customer.subscription.deleted":
                        //    //case "customer.subscription.created":
                        //    //    // do work
                        //    //    break;
                        //    case "invoice.payment_failed":
                        //        break;
                    }
                }
            }
            //Trace.WriteLine(entry.ToString());                
            return Task.FromResult(true);
        }
        public void SendEmail(string mailTo, string AgencyName,string PlanName,decimal PlanAmount,string balance_transaction,DateTime? CreatedDate,string timezone,string Data)
        {
           
            if (HttpContext.Current == null)
                return;
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            try
            {
                    
                    var resultTmplt = Engine.Razor.RunCompile(CDCEmailTemplates.InvoicePaymentSuccess, GetMd5Hash(CDCEmailTemplates.InvoicePaymentSuccess), null, new
                    {
                        name= AgencyName, Url = url,
                    planname= PlanName,
                    transactionid= balance_transaction,
                    amount= "$" + PlanAmount.ToString(),
                    date=ConvertFromUTC(CreatedDate.Value, timezone).ToString(), Data,

                    TopLogo=url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo=url + ConfigurationManager.AppSettings["Logo"]
                });
                    ////for beta
                    List<string> toMails = new List<string>();
                    toMails.Clear();
                    toMails.Add(mailTo);
                    AT.Net.Service.Email smtp = new AT.Net.Service.Email();
                    smtp.Body = resultTmplt;
                    smtp.Subject = "Invoice";
                    smtp.To = toMails;
                    smtp.Format = BodyFormat.HTML;
                    smtp.Send(true);
            
            }
            catch (Exception)
            {
                // ignored
            }
        }
        public string GetMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (byte t in hash)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }
        public DateTime? ConvertFromUTC(DateTime value, string timeZone = null)
        {
            TimeZoneInfo tz;
            if (string.IsNullOrEmpty(timeZone))
            {
                return DateTime.FromFileTime(value.ToFileTimeUtc());
            }
            if (timeZone == "Eastern Daylight Time")
            {
                string displayName = "(GMT-05:00) Eastern Standard Time";
                string standardName = "Eastern Standard Time";
                TimeSpan offset = new TimeSpan(04, 00, 00);
                tz = TimeZoneInfo.CreateCustomTimeZone(standardName, offset, displayName, standardName);
            }
            else
            {
                tz = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            }
            return TimeZoneInfo.ConvertTimeFromUtc(value, tz);
        }

    }
}