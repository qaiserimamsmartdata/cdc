﻿using CDC.Services;
using CDC.Services.Abstract;
using CDC.Services.Abstract.Family;
using CDC.Services.Abstract.RazorEngine;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.Family;
using CDC.WebApi.Encryption;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Family
{
    [Authorize]
    [RoutePrefix("api/Family")]
    public class FamilyController : ApiController
    {
        private readonly IFamilyService _ifamilyService;
        private readonly ICommonService _icommonservice;
        private readonly IPasswordService _ipasswordservice;
        private readonly IRazorService _iRazorService;
        public FamilyController(IFamilyService ifamilyService, ICommonService icommonservice, IPasswordService ipasswordservice, IRazorService iRazorService)
        {
            _ifamilyService = ifamilyService;
            _icommonservice = icommonservice;
            _ipasswordservice = ipasswordservice;
            _iRazorService = iRazorService;
        }
        
        [HttpPost]
        [Route("AddFamily")]
        public HttpResponseMessage RegisterFamily([FromBody]FamilyRegisterViewModel model)
        {

            ////Code is for Family Portal 
            var portalUserEmail = model.ParentInfo.Find(m => m.IsPrimary == true).EmailId;
            if (!string.IsNullOrEmpty(portalUserEmail))
            {
                model.PortalInfo.PortalEmail = portalUserEmail;
                var parentPassword = _ipasswordservice.Generate();
                model.PortalInfo.PortalPassword = EncryptDecrypt.GetEncryptedData(parentPassword);
                Uri currentUrl = HttpContext.Current.Request.Url;
                var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
                string mailSubject = AppConstants.PrimaryParentRegistered;
                var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.PrimaryParentRegister, _icommonservice.GetMd5Hash(CDCEmailTemplates.PrimaryParentRegister), new
                {
                    Url = url,
                    usernameVal = model.PortalInfo.PortalEmail,
                    passwordVal = parentPassword,
                    TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                    Logo = url + ConfigurationManager.AppSettings["Logo"]
                });
                _icommonservice.SendEmail(model.PortalInfo.PortalEmail, mailSubject, result);
            }
            var familyRegister = _ifamilyService.RegisterFamily(model);
            if (familyRegister.IsSuccess)
            {
                Uri currentUrl = HttpContext.Current.Request.Url;
                var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;

                string mailSubject = AppConstants.RequestForCDCParentLogin;
                foreach (var item in model.ParentInfo)
                {
                    SendMails(CDCEmailTemplates.NewParentRegistration,url,item,mailSubject, model.FamilyInfo.SecurityKey);
                }
            }
            var resultData = new { FamilyRegister = familyRegister };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency,Family")]
        [HttpPost]
        [Route("updateFamilyName")]
        public HttpResponseMessage UpdateFamilyName([FromBody]FamilyRegisterViewModel model)
        {
            var response = _ifamilyService.AddFamilyInfo(model.FamilyInfo);
            if(response.IsSuccess)
            response.Message = "Family Name is Updated Successfully";
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("getFamilyInfo")]
        public HttpResponseMessage GetFamilyInfo([FromBody]SearchFamilyViewModel model)
        {
            var familyRegister = _ifamilyService.GetFamilyInfo(model);
            var resultData = new { FamilyRegister = familyRegister };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("SetIsLoggedFirstTime")]
        public HttpResponseMessage SetIsLoggedFirstTime(long ParentID)
        {
            var responseObj = _ifamilyService.SetIsLoggedFirstTime(ParentID);
            return Request.CreateResponse(HttpStatusCode.OK, responseObj, "application/json");
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("getFamilyContactInfo")]
        public HttpResponseMessage GetFamilyContactInfo([FromBody]SearchFamilyViewModel model)
        {
            var contactInfo = _ifamilyService.GetFamilyContactInfo(model);
            var resultData = new { ContactInfo = contactInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("getFamilyStudentList")]
        public HttpResponseMessage GetFamilyStudentList([FromBody]SearchFamilyViewModel model)
        {
            var contactInfo = _ifamilyService.GetFamilyStudentList(model);
            var resultData = new { ContactInfo = contactInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("getFamilyParentList")]
        public HttpResponseMessage getFamilyParentList([FromBody]SearchFamilyViewModel model)
        {
            var contactInfo = _ifamilyService.GetFamilyParentList(model);
            var resultData = new { ContactInfo = contactInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("saveStudentInfo")]
        public HttpResponseMessage SaveStudentInfo([FromBody]List<StudentDetailViewModel> model)
        {
            var contactInfo = _ifamilyService.SaveStudentDetail(model);
            var resultData = new { ContactInfo = contactInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency,Family")]
        [HttpPost]
        [Route("saveParentInfo")]
        public HttpResponseMessage SaveParentInfo([FromBody]List<ParentInfoViewModel> model)
        {
            var contactInfo = _ifamilyService.SaveParentInfo(model);
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            if (contactInfo.IsSuccess)
            {
                if (model[0].ID == 0)
                {
                    string mailSubject = AppConstants.RequestForCDCParentLogin;
                    var template = CDCEmailTemplates.NewParentRegistration;
                    foreach (var item in model)
                    {
                        SendMails(template, url, item, mailSubject,"");
                    }
                }


            }
            var resultData = new { ContactInfo = contactInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        private void SendMails(string template, string url, ParentInfoViewModel item,string mailSubject,string securityCode)
        {
            var result = _iRazorService.GetRazorTemplate(template, _icommonservice.GetMd5Hash(template), new
            {
                Url = url,
                welcomeVal = "Welcome " + item.FirstName + " " + item.LastName + ",",
                Congo = "Congratulations you have been registered to PinWheel Care.",
                SecurityCode=string.IsNullOrEmpty(securityCode)?item.SecurityCode: securityCode,
                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                Logo = url + ConfigurationManager.AppSettings["Logo"]
            });
            _icommonservice.SendEmail(item.EmailId, mailSubject, result);

            string smsMessage = "Hi Welcome to PinWheel Care";
            string smsSubject = "Welcome to PinWheel Care";
            //string smsAddress = item.Mobile + "@txt.att.net";
            string smsAddress;
            if (item.ID > 0)
                smsAddress = item.Mobile + "@" + item.SMSCarrier.CarrierAddress;
            else
                smsAddress = item.Mobile + "@" + item.SMSCarrierEmailAddress;

            //string smsAddress = 3369700741 + "@msg.fi.google.com";
            _icommonservice.SendEmail(smsAddress, smsSubject, smsMessage);
        }

        [Authorize(Roles = "Agency,Family")]
        [HttpPost]
        [Route("addContact")]
        public HttpResponseMessage AddContact([FromBody]FamilyRegisterViewModel model)
        {
            var contactInfo = _ifamilyService.AddParentAndContactInfo(model);
            var resultData = new { ContactInfo = contactInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("deleteStudentInfo")]
        public HttpResponseMessage DeleteStudentInfo([FromBody]IDViewModel model)
        {
            var studentInfo = _ifamilyService.DeleteStudentInfo(model.ID);
            var resultData = new { StudentInfo = studentInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("deleteParentInfo")]
        public HttpResponseMessage DeleteParentInfo([FromBody]ParentInfoViewModel model)
        {
            var parentInfo = _ifamilyService.DeleteParentInfo(model);
            var resultData = new { ParentInfo = parentInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        
        [HttpPost]
        [Route("getFamilyStudentDetails")]
        public HttpResponseMessage GetFamilyStudentDetails([FromBody]SearchFamilyViewModel model)
        {
         
            var response = _ifamilyService.GetFamilyStudentDetails(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Agency,Family")]
        [HttpPost]
        [Route("savePaymentInfo")]
        public HttpResponseMessage SavePaymentInfo([FromBody]PaymentInfoViewModel model)
        {
            var response = _ifamilyService.SavePaymentInfo(model);
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
    }
}
