﻿using CDC.ViewModel.Common;
using CDC.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Http.Cors;
using CDC.Services.Abstract.MealSchedular;
using CDC.ViewModel.MealSchedular;

namespace CDC.WebApi.Controllers.MealSchedular
{
    [Authorize(Roles ="Agency")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/MealSchedule")]
    public class MealSchedularController : ApiController
    {
        private readonly IMealScheduleService _iMealSchedularsservice;
    
        public MealSchedularController(
          
            IMealScheduleService iMealSchedularsservice)
        {
        
            _iMealSchedularsservice = iMealSchedularsservice;
        }

      
      
        [AcceptVerbs("POST", "GET")]
        [Route("GetAll")]
        public HttpResponseMessage GetAll(string AgencyID)
        {
            string[] words = AgencyID.Split('~');
            string agencyId = words[0];
            string timeZone = words[1];
            string taskType = words[2];
            string staffId = words[3];
            string familyId = words[4];
            SearchTasksViewModel model = new SearchTasksViewModel();
            ResponseViewModel responceViewModel;
            if (string.IsNullOrEmpty(agencyId))
            {
                model.AgencyId = 0;
            }
            else
            {
                model.AgencyId = Convert.ToInt32(agencyId);

            }
            if (staffId == "null")
            {
                model.StaffId = 0;
            }
            else
            {
                model.StaffId = Convert.ToInt32(staffId);
            }
            if (familyId == "null")
            {
                model.FamilyId = 0;
            }
            else
            {
                model.FamilyId = Convert.ToInt32(familyId);
            }

            model.order = "AgencyId";
            if (string.IsNullOrEmpty(timeZone))
            {
                model.TimeZone = "India Standard Time";
            }
            else
            {
                model.TimeZone = timeZone;
            }
            model.TaskType = taskType;

            _iMealSchedularsservice.GetAllTasks(model, out responceViewModel);

            var a = responceViewModel.Content;
            return Request.CreateResponse(HttpStatusCode.OK, a, "application/json");
        }

     
        [AcceptVerbs("POST", "GET")]
        [Route("GetAllByClass")]
        public HttpResponseMessage GetAllByClass(string ClassId)
        {
            SearchTasksViewModel model = new SearchTasksViewModel();
            ResponseViewModel responceViewModel;
            if (string.IsNullOrEmpty(ClassId))
            {
                model.AgencyId = 0;
            }
            else
            {
                model.ClassId= Convert.ToInt32(ClassId);

            }

            model.order = "AgencyId";

            _iMealSchedularsservice.GetAllTasks(model, out responceViewModel);

            var result = responceViewModel.Content;
            return Request.CreateResponse(HttpStatusCode.OK, result, "application/json");
        }
      

    
        [HttpPost]
        [Route("AddMealShedule")]
        public HttpResponseMessage AddMealShedule(RootObject models)
        {

            ResponseViewModel responseViewModel = new ResponseViewModel();
            HttpResponseMessage badresponse;
            if (models != null)
            {
                char[] charsToTrim = { '[', ']' };
                var m = models.models;
                string modelTrimmed = m.Trim(charsToTrim);
                // Deserialize
                var serializedTaskViewModel = new JavaScriptSerializer().Deserialize<MealSchedularViewModel>(modelTrimmed);
                var serializedItemsTaskViewModel = new JavaScriptSerializer().Deserialize<List<MealScheduleItemsInfoViewModel>>(models.data);
                serializedTaskViewModel.ItemList = serializedItemsTaskViewModel.Where(k=>k.IsChecked).ToList();

                ResponseInformation transaction;
                _iMealSchedularsservice.AddTasks(serializedTaskViewModel, out transaction);
                responseViewModel.Content = serializedTaskViewModel;
                responseViewModel.IsSuccess = transaction.ReturnStatus;
                responseViewModel.ReturnMessage = transaction.ReturnMessage;
                responseViewModel.ValidationErrors = transaction.ValidationErrors;
                responseViewModel.Id = transaction.ID;
                if (transaction.ReturnStatus == false)
                {
                    badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                    return badresponse;
                }
                SearchTasksViewModel model = new SearchTasksViewModel();
                ResponseViewModel responceViewModel;
                _iMealSchedularsservice.GetAllTasks(model, out responceViewModel);
                var result = responceViewModel.Content;
                return Request.CreateResponse(HttpStatusCode.OK, result, "application/json");
            }
            var errors = ModelState.Errors();
            responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
            responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
            responseViewModel.ReturnStatus = false;
            badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
            return badresponse;
        }
    
        [HttpPost]
        [Route("UpdateMealShedule")]
        public HttpResponseMessage UpdateMealShedule(RootObject models)
        {

            ResponseViewModel responseViewModel = new ResponseViewModel();
            HttpResponseMessage badresponse;
            if (models != null)
            {
                char[] charsToTrim = { '[', ']' };
                var m = models.models;
                string modelTrimmed = m.Trim(charsToTrim);
                // Deserialize
                var serializedTaskViewModel = new JavaScriptSerializer().Deserialize<MealSchedularViewModel>(modelTrimmed);
                var serializedItemsTaskViewModel = new JavaScriptSerializer().Deserialize<List<MealScheduleItemsInfoViewModel>>(models.data);
                serializedTaskViewModel.ItemList = serializedItemsTaskViewModel.Where(k => k.IsChecked).ToList();

                ResponseInformation transaction;
                _iMealSchedularsservice.UpdateTasks(serializedTaskViewModel, out transaction);
                responseViewModel.Content = serializedTaskViewModel;
                responseViewModel.IsSuccess = transaction.ReturnStatus;
                responseViewModel.ReturnMessage = transaction.ReturnMessage;
                responseViewModel.ValidationErrors = transaction.ValidationErrors;
                responseViewModel.Id = transaction.ID;
                if (transaction.ReturnStatus == false)
                {
                    badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                    return badresponse;
                }
                SearchTasksViewModel model = new SearchTasksViewModel();
                ResponseViewModel responceViewModel;
                _iMealSchedularsservice.GetAllTasks(model, out responceViewModel);
                var result = responceViewModel.Content;
                return Request.CreateResponse(HttpStatusCode.OK, result, "application/json");
            }
            var errors = ModelState.Errors();
            responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
            responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
            responseViewModel.ReturnStatus = false;
            badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
            return badresponse;
        }
    
     
        [AcceptVerbs("POST", "GET")]
        [Route("DeleteMealScheduleById")]
        public HttpResponseMessage DeleteMealScheduleById(RootObject models)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            HttpResponseMessage badresponse;
            if (models != null)
            {
                char[] charsToTrim = { '[', ']' };
                var m = models.models;
                string modelTrimmed = m.Trim(charsToTrim);
                // Deserialize
                MealSchedularViewModel serializedViewModel = new JavaScriptSerializer().Deserialize<MealSchedularViewModel>(modelTrimmed);
                ResponseInformation transaction;
                _iMealSchedularsservice.DeleteMealScheduleById(serializedViewModel, out transaction);
                responseViewModel.Content = serializedViewModel;
                responseViewModel.IsSuccess = transaction.ReturnStatus;
                responseViewModel.ReturnMessage = transaction.ReturnMessage;
                responseViewModel.ValidationErrors = transaction.ValidationErrors;
                responseViewModel.Id = transaction.ID;
                if (transaction.ReturnStatus == false)
                {
                    badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                    return badresponse;
                }
                SearchTasksViewModel model = new SearchTasksViewModel();
                ResponseViewModel responceViewModel;
                _iMealSchedularsservice.GetAllTasks(model, out responceViewModel);
                var a = responceViewModel.Content;
                return Request.CreateResponse(HttpStatusCode.OK, a, "application/json");
            }
            var errors = ModelState.Errors();
            responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
            responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
            responseViewModel.ReturnStatus = false;
            badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
            return badresponse;
        }
    }
}