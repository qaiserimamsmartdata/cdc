﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using CDC.WebApi.Helpers;

namespace CDC.WebApi.Controllers.Classes
{
    /// <summary>
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    //[Authorize(Roles = "Agency")]
    
    public class ClassController : ApiController
    {
        private readonly IClassService _iclassservive;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ClassController" /> class.
        /// </summary>
        /// <param name="iclassservice">The class service.</param>
        public ClassController(IClassService iclassservice)
        {
            _iclassservive = iclassservice;
        }


        /// <summary>
        ///     Adds the class.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Add([FromBody] ClassViewModel model)
        {
            ResponseInformation responseInfo = new ResponseInformation();
                if (!ModelState.IsValid)
                {
                    var errors = ModelState.Errors();
                    responseInfo.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                    responseInfo.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                    responseInfo.ReturnStatus = false;
                    var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseInfo);
                    return badresponse;
                }

                _iclassservive.Add(model, out responseInfo);
                if (responseInfo.ReturnStatus == false)
                {
                    var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseInfo);
                    return badresponse;
                }
                else
                {
                    var response = Request.CreateResponse(HttpStatusCode.Created, responseInfo);
                    return response;
                }
            }


        /// <summary>
        ///     Gets the class by identifier.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetById(IDViewModel model)
        {
            var classList = _iclassservive.GetById(model.ClassId, model.TimeZone);
            var resultData = new {classList};
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        /// <summary>
        ///     Gets all classes.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage All([FromBody] SearchClassViewModel model)
        {
            ResponseInformation resp;
            _iclassservive.All(model, out resp);
            if (resp.ReturnStatus)
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, resp);
                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.OK, resp, "application/json");
                return response;
            }
        }


        /// <summary>
        ///     Updates the class.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Update(ClassViewModel model)
        {
            ResponseInformation responseInfo = new ResponseInformation();
            HttpResponseMessage badresponse;
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseInfo.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseInfo.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseInfo.ReturnStatus = false;
                badresponse = Request.CreateResponse<ResponseInformation>(HttpStatusCode.BadRequest, responseInfo);
                return badresponse;
            }
            _iclassservive.Update(model, out responseInfo);
            if (responseInfo.ReturnStatus)
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, responseInfo);
                return response;
            }
            badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseInfo);
            return badresponse;
        }

        /// <summary>
        ///     Deletes the class by identifier.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Delete(long id)
        {
            ResponseInformation responseInfo;

            _iclassservive.Delete(id,out responseInfo);
            if (responseInfo.ReturnStatus)
            {
                var response = Request.CreateResponse(HttpStatusCode.Created, responseInfo);
                return response;
            }
            var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseInfo);
            return badresponse;
        }
    }
}