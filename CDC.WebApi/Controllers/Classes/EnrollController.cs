﻿using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Classes
{
    [Authorize]
    [RoutePrefix("api/EnrollClass")]
    public class EnrollController : ApiController
    {
        private readonly IEnrollService _iEnrollservive;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ClassController" /> class.
        /// </summary>
        /// <param name="iEnrollservive"></param>
        public EnrollController(IEnrollService iEnrollservive)
        {
            _iEnrollservive = iEnrollservive;
        }
        [Authorize(Roles ="Family,Agency,Staff")]
        [HttpPost]
        [Route("GetCurrentEnrollList")]
        public HttpResponseMessage GetCurrentEnrollList(SearchStudentScheduleViewModel model)
         {
            var enrollList = _iEnrollservive.GetEnrollList(model);
            var resultData = new { enrollList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Family,Agency,Staff")]
        [HttpPost]
        [Route("GetFutureEnrollList")]
        public HttpResponseMessage GetFutureEnrollList(SearchStudentScheduleViewModel model)
        {

            var enrollList = _iEnrollservive.GetFutureEnrollList(model);
            var resultData = new { enrollList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("GetParticipantWaitingListByClassId")]
        public HttpResponseMessage GetParticipantWaitingListByClassId(SearchStudentScheduleViewModel model)
        {

            var enrollList = _iEnrollservive.GetParticipantWaitingListByClassId(model);
            var resultData = new { enrollList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("UpdateFutureEnrolledStudent")]
        public HttpResponseMessage UpdateFutureEnrolledStudent(EnrollListViewModel model)
        {
            var response = _iEnrollservive.UpdateFutureEnrollList(model);
            var resultData = new { response };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteFutureEnrolledStudent")]
        public HttpResponseMessage DeleteFutureEnrolledStudent(EnrollListViewModel model)
        {
            var studentInfo = _iEnrollservive.DeleteFutureEnrolledStudent(model);
            var resultData = new { StudentInfo = studentInfo };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Family,Agency")]
        [HttpPost]
        [Route("GetUnEnrolledStudentList")]
        public HttpResponseMessage GetUnEnrolledStudentList(SearchStudentScheduleViewModel model)
        {
            var unenrollList = _iEnrollservive.GetUnEnrolledStudentList(model);
            var resultData = new { unenrollList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteEnrollMent")]
        public HttpResponseMessage CancelEnrollMent([FromBody]long EnrollMentId)
        {
            var cancelenrollList = _iEnrollservive.CancelEnrollMent(EnrollMentId);
            var resultData = new { CancelenrollList = cancelenrollList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [Authorize(Roles = "Agency,Family,Staff")]
        [HttpPost]
        [Route("GetEnrollmentHistory")]
        public HttpResponseMessage GetEnrollmentHistory(SearchStudentScheduleViewModel model)
        {
            var enrollHistory = _iEnrollservive.GetEnrollHistory(model);
            var resultData = new { enrollHistory };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        
    }
}
