﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;

namespace CDC.WebApi.Controllers.Classes
{
    /// <summary>
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [Authorize]
    [RoutePrefix("api/LessonPlan")]
    public class LessonPlanController : ApiController
    {
        private readonly ILessonPlanService _ilessonPlanservive;

        public LessonPlanController(ILessonPlanService ilessonPlanservive)
        {
            _ilessonPlanservive = ilessonPlanservive;
        }

        /// <summary>
        ///     Adds the update lesson plan.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddUpdateLessonPlan")]
        public HttpResponseMessage AddUpdateLessonPlan([FromBody] LessonPlanViewModel model)
        {
            //var photos = await photoManager.Add(Request);
            var response = new ResponseViewModel();
            var returnData = _ilessonPlanservive.AddUpdateLessonPlan(model);
            response.IsSuccess = returnData == 1;
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        /// <summary>
        ///     Gets the lesson plan.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetLessonPlan")]
        public HttpResponseMessage GetLessonPlan([FromBody] SearchLessonPlanViewModel model)
        {
            int count;
            var lessonPlan = _ilessonPlanservive.GetLessonPlan(model, out count);
            var resultData =
                new
                {
                    lessonPlan =
                        lessonPlan.Select(
                            m =>
                                new
                                {
                                    m.ID, m.Name,
                                    Date = m.Date.Value.ToShortDateString(),
                                    m.Theme,
                                    m.Order,
                                    m.SpecialInstructions,
                                    m.Description,
                                    m.UploadFile,
                                    TotalCount = count
                                })
                };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        /// <summary>
        ///     Deletes the lesson plan by identifier.
        /// </summary>
        /// <param name="lessonPlanId">The lesson plan identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteLessonPlanById")]
        public HttpResponseMessage DeleteLessonPlanById(long lessonPlanId)
        {
            var response = new ResponseViewModel();

            var returnData = _ilessonPlanservive.DeleteLessonPlanById(lessonPlanId);

            if (returnData == 2)
            {
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Unable to delete at the moment. Please try again after some time.";
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }


        [HttpPost]
        [Route("uploadAttachment")]
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            var response = new ResponseViewModel();
            var httpRequest = HttpContext.Current.Request;
            try
            {
                var postedFile = httpRequest.Files[0];
                var FolderPath = "Images/Attachment/";
                if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("~/" + FolderPath)))
                System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + FolderPath));
                var filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + FolderPath + postedFile.FileName);
                if (filePath != null) postedFile.SaveAs(filePath);
                response.IsSuccess = true;
            }
            catch (Exception)
            {
                response.IsSuccess = false;
            }
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
    }
}