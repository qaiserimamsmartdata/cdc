﻿using CDC.Entities.Class;
using CDC.Services.Abstract.Class;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Classes
{
    [Authorize]
    [RoutePrefix("api/WaitList")]
    public class WaitListController : ApiController
    {
        private readonly IWaitListService _iWaitListservive;
        public WaitListController(IWaitListService iWaitListservive)
        {
            _iWaitListservive = iWaitListservive;
        }

        [HttpPost]
        [Route("MapWaitList")]
        public HttpResponseMessage MapWaitList([FromBody] WaitListMap model)
        {
            var waitList = _iWaitListservive.MapWaitList(model);
            var resultData = new { WaitList = waitList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("GetStudentWaitList")]
        public HttpResponseMessage GetStudentWaitList([FromBody] WaitListMap model)
        {
            var waitList = _iWaitListservive.GetWaitListStudentList(model);
            var resultData = new { WaitList = waitList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("deleteMappedWaitList")]
        public HttpResponseMessage DeleteMappedWaitList([FromBody] WaitListMap model)
        {
            var waitList = _iWaitListservive.DeleteMappedWaitList(model);
            var resultData = new { WaitList = waitList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("saveWaitListNotes")]
        public HttpResponseMessage SaveWaitListNotes([FromBody] WaitListMap model)
        {
            var waitList = _iWaitListservive.SaveWaitListNotes(model);
            var resultData = new { WaitList = waitList };
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
    }
}
