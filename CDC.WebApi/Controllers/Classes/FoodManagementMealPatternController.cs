﻿using CDC.Services.Abstract;
using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using CDC.WebApi.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Classes
{
    [Authorize]
    [RoutePrefix("api/foodManagementMealPattern")]
    public class FoodManagementMealPatternController : ApiController
    {
        private readonly IFoodManagementMealPatternService _iFoodManagementMealPatternService;
        private readonly ICommonService _icommonservice;

        public FoodManagementMealPatternController(IFoodManagementMealPatternService iFoodManagementMealPatternService, ICommonService icommonservice)
        {
            _iFoodManagementMealPatternService = iFoodManagementMealPatternService;
            _icommonservice = icommonservice;
        }
        [HttpPost]
        [Route("GetFoodMealPatternById")]
        public HttpResponseMessage GetFoodMealPatternById(long foodMealPatternfId)
        {
            var foodMealPatternList = _iFoodManagementMealPatternService.GetFoodMealPatternById(foodMealPatternfId);
            var resultData = foodMealPatternList;
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        [HttpPost]
        [Route("GetAllFoodManagementMealPattern")]
        public HttpResponseMessage GetAllFoodManagementMealPattern([FromBody] SearchFoodMealPatternViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iFoodManagementMealPatternService.GetAllFoodManagementMealPattern(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }



        [HttpPost]
        [Route("AddFoodMealPattern")]
        public HttpResponseMessage AddFoodMealPattern([FromBody] FoodManagementMealPatternViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation responseInfo;
            _iFoodManagementMealPatternService.AddFoodMealPattern(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }



        [HttpPost]
        [Route("UpdateFoodMealPattern")]
        public HttpResponseMessage UpdateFoodMealPattern(FoodManagementMealPatternViewModel model)

        {
            ResponseInformation responseInfo;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            _iFoodManagementMealPatternService.UpdateFoodMealPattern(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;

            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

        [HttpPost]
        [Route("DeleteFoodMealPatternById")]
        public HttpResponseMessage DeleteFoodMealPatternById(long foodMealPatternfId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _iFoodManagementMealPatternService.DeleteFoodMealPatternById(foodMealPatternfId,out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
