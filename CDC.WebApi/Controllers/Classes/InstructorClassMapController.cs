﻿using CDC.Services.Abstract.Class;
using CDC.ViewModel.Class;
using CDC.ViewModel.Common;
using CDC.WebApi.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Classes
{
    [Authorize]
    [RoutePrefix("api/InstructorClassMap")]
    public class InstructorClassMapController : ApiController
    {
        private readonly IInstructorsMappingService _iInstructorsMappingservice;

        public InstructorClassMapController(IInstructorsMappingService iInstructorsMappingservice)
        {
            _iInstructorsMappingservice = iInstructorsMappingservice;
        }
        /// <summary>
        /// Gets the instructor class map by identifier.
        /// </summary>
        /// <param name="instructorId">The instructor identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetInstructorClassMapById")]
        public HttpResponseMessage GetInstructorClassMapById(long instructorId)
        {
            var instructorClassMapList = _iInstructorsMappingservice.GetInstructorClassMapById(instructorId);
            var resultData = new { instructorClassMapList };

            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }

        [HttpPost]
        [Route("GetAllInstructorClassMap")]
        public HttpResponseMessage GetAllInstructorClassMap([FromBody] SearchInstructorViewModel model)
        {
            ResponseViewModel responseViewModel;
            _iInstructorsMappingservice.GetAllInstructorClassMap(model, out responseViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


        /// <summary>
        /// Adds the instructor class map.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddInstructorClassMap")]
        public HttpResponseMessage AddInstructorClassMap([FromBody]InstructorClassMapViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation responseInfo;
            _iInstructorsMappingservice.AddInstructorClassMap(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }

        /// <summary>
        /// Updates the instructor class map.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateInstructorClassMap")]
        public HttpResponseMessage UpdateInstructorClassMap(InstructorClassMapViewModel model)
        {
            ResponseInformation responseInfo;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }

            _iInstructorsMappingservice.UpdateInstructorClassMap(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;

            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

        /// <summary>
        /// Deletes the instructor class map by identifier.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteInstructorClassMapById")]
        public HttpResponseMessage DeleteInstructorClassMapById(InstructorClassMapViewModel model)
        {

            var responseData = _iInstructorsMappingservice.DeleteInstructorClassMapById(model);
            if (responseData.IsSuccess == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseData);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseData);
            return response;
        }
    }
}
