﻿using CDC.Services.Abstract;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using CDC.WebApi.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Staff
{
    [Authorize]
    [RoutePrefix("api/TimeSheet")]
    public class TimesheetController : ApiController
    {
        private readonly ITimeSheetService _timeSheetService;
        public TimesheetController(ITimeSheetService timeSheetService, ICommonService commonService)
        {
            _timeSheetService = timeSheetService;
        }
        [Authorize(Roles = "Staff")]
        [HttpPost]
        public HttpResponseMessage GetTimesheetsByStaffId([FromBody]SearchTimesheetViewModel model)
        {
            string errorMessage = string.Empty;
            TimesheetViewModel responseData = _timeSheetService.GetTimesheetsByStaffId(model);
            ResponseModel<TimesheetViewModel> response = new ResponseModel<TimesheetViewModel>()
            {
                Response = responseData,
                Message = errorMessage,
                ResponseCode = HttpContext.Current.Response.StatusCode,
                ResponseDescription = HttpContext.Current.Response.StatusDescription,
                SubStatusCode = HttpContext.Current.Response.SubStatusCode
            };
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Staff")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetTimesheetsByStaffIdAndTimeZone([FromBody]SearchTimesheetViewModel model)
        {
            string errorMessage = string.Empty;
            TimesheetViewModel responseData = _timeSheetService.GetTimesheetsByStaffIdAndTimeZone(model);
            ResponseModel<TimesheetViewModel> response = new ResponseModel<TimesheetViewModel>()
            {
                Response = responseData,
                Message = errorMessage,
                ResponseCode = HttpContext.Current.Response.StatusCode,
                ResponseDescription = HttpContext.Current.Response.StatusDescription,
                SubStatusCode = HttpContext.Current.Response.SubStatusCode
            };
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Staff")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetTimesheetsWithoutTimeZone([FromBody]SearchTimesheetViewModel model)
        {
            string errorMessage = string.Empty;
            TimesheetViewModel responseData = _timeSheetService.GetTimesheetsWithoutTimeZone(model);
            ResponseModel<TimesheetViewModel> response = new ResponseModel<TimesheetViewModel>()
            {
                Response = responseData,
                Message = errorMessage,
                ResponseCode = HttpContext.Current.Response.StatusCode,
                ResponseDescription = HttpContext.Current.Response.StatusDescription,
                SubStatusCode = HttpContext.Current.Response.SubStatusCode
            };
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles ="Agency,Staff")]
        // GET: api/Time-sheet
        [HttpPost]
        public HttpResponseMessage GetTimesheetWithTimeZone([FromBody]SearchTimesheetViewModel model)
        {
            string errorMessage = string.Empty;
            TimesheetViewModel responseData = _timeSheetService.GetTimesheetWithTimeZone(model);
            ResponseModel<TimesheetViewModel> response = new ResponseModel<TimesheetViewModel>()
            {
                Response = responseData,
                Message = errorMessage,
                ResponseCode = HttpContext.Current.Response.StatusCode,
                ResponseDescription = HttpContext.Current.Response.StatusDescription,
                SubStatusCode = HttpContext.Current.Response.SubStatusCode
            };
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Staff")]
        //POST: api/Time-sheet
        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveTimeSheet(TimeCheckModel value)
        {
      
            string errorMessage = string.Empty;
            string ipAddress = HttpContext.Current.Request.UserHostAddress;
            string timezoneName = TimeZone.CurrentTimeZone.StandardName;

            value.CheckOnDateTime = DateTime.UtcNow;
            var currentCheckStatus =  _timeSheetService.GetCurrentTimeCheck(value.StaffID);
            value.TimesheetID = currentCheckStatus == null ? 0 : currentCheckStatus.TimesheetID;

            if (value.PurposeToCheckInOrOut == Entities.Enums.InOutPurpose.Day
                && (currentCheckStatus == null || (currentCheckStatus.PurposeToCheckInOrOut == Entities.Enums.InOutPurpose.Day
                && currentCheckStatus.TimeChecksForInOrOut == Entities.Enums.TimeCheckInOrOut.Out)))
            {
                value.PurposeToCheckInOrOut = Entities.Enums.InOutPurpose.Day;
                value.TimeChecksForInOrOut = Entities.Enums.TimeCheckInOrOut.In;
                value.TimesheetID = 0;
       
            }
            else if (value.PurposeToCheckInOrOut == Entities.Enums.InOutPurpose.Day
                && currentCheckStatus.PurposeToCheckInOrOut == Entities.Enums.InOutPurpose.Day
                && currentCheckStatus.TimeChecksForInOrOut == Entities.Enums.TimeCheckInOrOut.In)
            {
                value.PurposeToCheckInOrOut = Entities.Enums.InOutPurpose.Day;
                value.TimeChecksForInOrOut = Entities.Enums.TimeCheckInOrOut.Out;
            
            }
            else
            {
                value.TimeChecksForInOrOut = Entities.Enums.TimeCheckInOrOut.In;
                if ((value.PurposeToCheckInOrOut == Entities.Enums.InOutPurpose.Lunch
                    && currentCheckStatus.PurposeToCheckInOrOut != Entities.Enums.InOutPurpose.Lunch)
                    || (value.PurposeToCheckInOrOut == Entities.Enums.InOutPurpose.Break
                    && currentCheckStatus.PurposeToCheckInOrOut != Entities.Enums.InOutPurpose.Break))
                {
                    value.TimeChecksForInOrOut = Entities.Enums.TimeCheckInOrOut.Out;
                    value.TimesheetID = 0;
                }
            }

            var data = _timeSheetService.SaveCheckTime(value, ipAddress, timezoneName);

            ResponseModel<TimesheetModel> response = new ResponseModel<TimesheetModel>()
            {
                Response = data,
                Message = errorMessage,
                ResponseCode = HttpContext.Current.Response.StatusCode,
                ResponseDescription = HttpContext.Current.Response.StatusDescription,
                SubStatusCode = HttpContext.Current.Response.SubStatusCode,

            };
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }
        [Authorize(Roles = "Staff")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("TimeCheckStatus")]
        public HttpResponseMessage TimeCheckStatus(int staffID)
        {
            string errorMessage = string.Empty;
            TimeCheckModel responseData = _timeSheetService.GetCurrentTimeCheck(staffID);
            ResponseModel<TimeCheckModel> response = new ResponseModel<TimeCheckModel>()
            {
                Response = responseData,
                Message = errorMessage,
                ResponseCode = HttpContext.Current.Response.StatusCode,
                ResponseDescription = HttpContext.Current.Response.StatusDescription,
                SubStatusCode = HttpContext.Current.Response.SubStatusCode
            };
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("TimeZones")]
        public HttpResponseMessage TimeZones()
        {
            var timeZones = _timeSheetService.GetTimezones();
            ResponseModel<object> response = new ResponseModel<object>()
            {
                Response = timeZones,
                Message = string.Empty,
                ResponseCode = HttpContext.Current.Response.StatusCode,
                ResponseDescription = HttpContext.Current.Response.StatusDescription,
                SubStatusCode = HttpContext.Current.Response.SubStatusCode
            };
            return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
        }

    }
}