﻿using CDC.Services;
using CDC.Services.Abstract;
using CDC.Services.Abstract.RazorEngine;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using CDC.WebApi.Encryption;
using CDC.WebApi.Helpers;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Staff
{
   [Authorize]
    [RoutePrefix("api/Staff")]
    public class StaffController : ApiController
    {
        private readonly IStaffService _istaffservice;
        private readonly ICommonService _icommonservice;
        private readonly IRazorService _iRazorService;

       /// <summary>
       ///  Initializes a new instance of the <see cref="StaffController" /> class.
       /// </summary>
       /// <param name="iStaffService"></param>
       /// <param name="icommonservice"></param>
       /// <param name="iRazorService"></param>
       public StaffController(IStaffService iStaffService, ICommonService icommonservice, IRazorService iRazorService)
        {
            _istaffservice = iStaffService;
            _icommonservice = icommonservice;
            _iRazorService = iRazorService;
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("GetStaffById")]
        public HttpResponseMessage GetStaffById(long staffId)
        {
            var staffList = _istaffservice.GetStaffById(staffId);
            staffList.Password = EncryptDecrypt.GetDecryptedData(staffList.Password);
            var resultData = new { staffList };
          
            return Request.CreateResponse(HttpStatusCode.OK, resultData, "application/json");
        }
        /// <summary>
        /// Gets all classes.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("GetAllStaff")]
        public HttpResponseMessage GetAllStaff([FromBody] SearchStaffViewModel model)
        {
            ResponseViewModel responseViewModel;
            _istaffservice.GetAllStaff(model,out responseViewModel);        
            return Request.CreateResponse(HttpStatusCode.OK, responseViewModel, "application/json");
        }


        /// <summary>
        /// Adds the staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("AddStaff")]
        public HttpResponseMessage AddStaff([FromBody] StaffViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            var password = model.Password;
            model.Password = EncryptDecrypt.GetEncryptedData(model.Password);
            ResponseInformation responseInfo;
            _istaffservice.AddStaff(model, out responseInfo);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = responseInfo.ReturnStatus;
            responseViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responseViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            string MailSubject = AppConstants.RequestForCDCStaffLogin;
            var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.NewRegistration, _icommonservice.GetMd5Hash(CDCEmailTemplates.NewRegistration), new
            {
                Url = url,
                usernameVal = model.Email,
                passwordVal = password,
                UserRole = AppConstants.Staff,
                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                Logo = url + ConfigurationManager.AppSettings["Logo"]
            });
            _icommonservice.SendEmail(model.Email, MailSubject, result);

            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            string MailSubject1 = AppConstants.StaffSuperAdmin;

            var resultTmplt = _iRazorService.GetRazorTemplate(CDCEmailTemplates.StaffSuperAdmin, _icommonservice.GetMd5Hash(CDCEmailTemplates.StaffSuperAdmin), new
            {
                Url = url,
                name = model.FirstName,
                agencyname = model.AgencyName,
                UserRole = AppConstants.Staff,
                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                Logo = url + ConfigurationManager.AppSettings["Logo"]
            });

            _icommonservice.SendEmail("pinwheel.trinity@gmail.com", MailSubject1, resultTmplt);

            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }
        [Authorize(Roles = "Agency,Staff")]
        [HttpPost]
        [Route("UpdateStaff")]
        public HttpResponseMessage UpdateStaff(StaffViewModel model)
        {
            ResponseInformation responseInfo;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            model.Password = EncryptDecrypt.GetEncryptedData(model.Password);
            _istaffservice.UpdateStaff(model, out responseInfo);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;

            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
        /// <summary>
        ///     Deletes the staff by identifier.
        /// </summary>
        /// <param name="staffId">The staff identifier.</param>
        /// <returns></returns>
        [Authorize(Roles = "Agency")]
        [HttpPost]
        [Route("DeleteStaff")]
        public HttpResponseMessage DeleteStaffById(long staffId)
        {
            ResponseInformation responseInfo;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _istaffservice.DeleteStaffById(staffId,out responseInfo);
            responceViewModel.IsSuccess = responseInfo.ReturnStatus;
            responceViewModel.ReturnMessage = responseInfo.ReturnMessage;
            responceViewModel.ValidationErrors = responseInfo.ValidationErrors;
            if (responseInfo.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
