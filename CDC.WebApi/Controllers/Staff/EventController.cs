﻿using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using CDC.WebApi.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Staff
{
    [Authorize]
    [RoutePrefix("api/Event")]
    public class EventController : ApiController
    {
        private readonly IEventService _ieventservice;
        public EventController(IEventService ieventservice)
        {
            _ieventservice = ieventservice;
        }

        [HttpPost]
        [Route("GetAllEvent")]
        public HttpResponseMessage GetAllEvent(SearchEventViewModel model)
        {
            ResponseViewModel responceViewModel;
            _ieventservice.GetAllEvent(model, out responceViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responceViewModel, "application/json");
        }


        [HttpPost]
        [Route("AddEvent")]
        public HttpResponseMessage AddEvent([FromBody] EventViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation transaction;
            _ieventservice.AddEvent(model, out transaction);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = transaction.ReturnStatus;
            responseViewModel.ReturnMessage = transaction.ReturnMessage;
            responseViewModel.ValidationErrors = transaction.ValidationErrors;
            responseViewModel.Id = transaction.ID;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }


        [HttpPost]
        [Route("UpdateEvent")]
        public HttpResponseMessage UpdateEvent(EventViewModel model)
        {
            ResponseInformation transaction;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }

            _ieventservice.UpdateEvent(model, out transaction);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;

            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

        [HttpPost]
        [Route("DeleteEventById")]
        public HttpResponseMessage DeleteEventById(long eventId)
        {
            ResponseInformation transaction;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _ieventservice.DeleteEventById(eventId, out transaction);
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
