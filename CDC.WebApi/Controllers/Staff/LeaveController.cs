﻿using CDC.Services;
using CDC.Services.Abstract;
using CDC.Services.Abstract.RazorEngine;
using CDC.Services.Abstract.Staff;
using CDC.ViewModel;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using CDC.WebApi.Helpers;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Staff
{
    [Authorize]
    [RoutePrefix("api/Leave")]
    public class LeaveController : ApiController
    {
        private readonly ILeaveServices _ileaveservice;
        private readonly ICommonService _icommonservice;
        private readonly IRazorService _iRazorService;
        public LeaveController(ILeaveServices ileaveservice, ICommonService icommonservice,IRazorService iRazorService)
        {
            _ileaveservice = ileaveservice;
            _icommonservice = icommonservice;
            _iRazorService = iRazorService;
        }
        /// <summary>
        /// Gets all leave.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles ="Agency,Staff")]
        [HttpPost]
        [Route("GetAllLeave")]
        public HttpResponseMessage GetAllLeave([FromBody] SearchLeaveViewModel model)
        {
            ResponseViewModel responseInfo;
            _ileaveservice.GetAllLeave(model, out responseInfo);
            return Request.CreateResponse(HttpStatusCode.OK, responseInfo, "application/json");
        }
        /// <summary>
        /// Adds the leave.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Staff")]
        [HttpPost]
        [Route("ApplyStaffLeave")]
        public HttpResponseMessage AddLeave([FromBody] LeaveViewModel model)
        {
            ResponseInformation transaction;
            _ileaveservice.AddLeave(model, out transaction);
            return Request.CreateResponse(HttpStatusCode.OK, transaction, "application/json");
        }
        /// <summary>
        /// Updates the leave.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [Authorize(Roles = "Staff,Agency")]
        [HttpPost]
        [Route("UpdateLeave")]
        public HttpResponseMessage UpdateLeave(LeaveViewModel model)
        {
            ResponseInformation transaction;
            ResponseViewModel responceViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            _ileaveservice.UpdateLeaveDetail(model, out transaction);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;
            Uri currentUrl = HttpContext.Current.Request.Url;
            var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;

            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            string mailSubject = AppConstants.LeaveRequestStatus;
            var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.LeaveRequest, _icommonservice.GetMd5Hash(CDCEmailTemplates.LeaveRequest), new
            {
                Url = url,
                status = model.StatusId == 4 ? "approved" : "denied",
                TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                Logo = url + ConfigurationManager.AppSettings["Logo"]
            });
            _icommonservice.SendEmail(model.Staff.Email, mailSubject, result);
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }

        /// <summary>
        /// Deletes the leave by identifier.
        /// Use it only when incharge cancel or decline the leave.
        /// </summary>
        /// <param name="leaveId">The leave identifier.</param>
        /// <returns></returns>
        [Authorize(Roles = "Staff")]
        [HttpPost]
        [Route("DeleteLeave")]
        public HttpResponseMessage DeleteLeaveById(long leaveId)
        {
            ResponseInformation transaction;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _ileaveservice.DeleteLeaveById(leaveId, out transaction);
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
