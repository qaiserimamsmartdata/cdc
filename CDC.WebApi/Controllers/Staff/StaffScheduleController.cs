﻿using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using CDC.WebApi.Helpers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CDC.WebApi.Controllers.Staff
{
    [Authorize]
    [RoutePrefix("api/StaffSchedule")]
    public class StaffScheduleController : ApiController
    {
        private readonly IStaffScheduleService _istaffscheduleservice;
        public StaffScheduleController(IStaffScheduleService istaffscheduleservice)
        {
            _istaffscheduleservice = istaffscheduleservice;
        }
        [HttpPost]
        [Route("GetAllStaffSchedule")]
        public HttpResponseMessage GetAllStaffSchedule(SearchStaffScheduleViewModel model)
        {
            ResponseViewModel responceViewModel;
            _istaffscheduleservice.GetAllSchedule(model,out responceViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responceViewModel, "application/json");
        }
        /// <summary>
        /// Gets all staff schedule.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAll")]
        public HttpResponseMessage GetAll([FromBody] SearchStaffScheduleViewModel model)
        {
            ResponseViewModel responceViewModel;
            _istaffscheduleservice.GetAllStaffSchedule(model, out responceViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responceViewModel, "application/json");
        }

        [HttpPost]
        [Route("GetAllScheduleStaffWise")]
        public HttpResponseMessage GetAllScheduleStaffWise(SearchStaffScheduleViewModel model)
        {
            ResponseViewModel responceViewModel;
            _istaffscheduleservice.GetAllScheduleStaffWise(model, out responceViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responceViewModel, "application/json");
        }
        /// <summary>
        /// Adds the schedule staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddSchedule")]
        public HttpResponseMessage AddSchedule([FromBody] StaffScheduleViewModel model)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responseViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }

            ResponseInformation transaction;
            _istaffscheduleservice.AddScheduleStaff(model, out transaction);
            responseViewModel.Content = model;
            responseViewModel.IsSuccess = transaction.ReturnStatus;
            responseViewModel.ReturnMessage = transaction.ReturnMessage;
            responseViewModel.ValidationErrors = transaction.ValidationErrors;
            responseViewModel.Id = transaction.ID;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responseViewModel);
            return response;
        }
        /// <summary>
        /// Updates the schedule staff.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateScheduleStaff")]
        public HttpResponseMessage UpdateScheduleStaff(StaffScheduleViewModel model)
        {
            ResponseInformation transaction;

            ResponseViewModel responceViewModel = new ResponseViewModel();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Errors();
                responceViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
                responceViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
                responceViewModel.ReturnStatus = false;
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }

            _istaffscheduleservice.UpdateScheduleStaff(model, out transaction);
            responceViewModel.Content = model;
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;

            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
        /// <summary>
        /// Deletes the schedule staff by identifier.
        /// </summary>
        /// <param name="staffScheduleId">The staff schedule identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteScheduleStaff")]
        public HttpResponseMessage DeleteScheduleStaffById(long staffScheduleId)
        {
            ResponseInformation transaction;
            ResponseViewModel responceViewModel = new ResponseViewModel();

            _istaffscheduleservice.DeleteStaffScheduleById(staffScheduleId, out transaction);
            responceViewModel.IsSuccess = transaction.ReturnStatus;
            responceViewModel.ReturnMessage = transaction.ReturnMessage;
            responceViewModel.ValidationErrors = transaction.ValidationErrors;
            if (transaction.ReturnStatus == false)
            {
                var badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responceViewModel);
                return badresponse;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, responceViewModel);
            return response;
        }
    }
}
