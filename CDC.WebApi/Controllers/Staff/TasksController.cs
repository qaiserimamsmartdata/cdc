﻿using CDC.Services.Abstract.Staff;
using CDC.ViewModel.Common;
using CDC.ViewModel.Staff;
using CDC.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Web.Script.Serialization;
using CDC.Entities.Staff;
using CDC.Data.Repositories;
using CDC.Data.Infrastructure;
using System.Web.Http.Cors;
using CDC.Services.Abstract;
using CDC.ViewModel;
using System.Web;
using System.IO;
using CDC.Services.Abstract.Class;
using CDC.Entities.Class;
using System.Threading;
using System.Configuration;
using CDC.Services.Abstract.RazorEngine;
using CDC.Services;
using CDC.ViewModel.NewParent;

namespace CDC.WebApi.Controllers.Staff
{
    [Authorize(Roles ="Agency,Family,Staff")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Tasks")]
    public class TasksController : ApiController
    {
        private readonly ITasksService _itasksservice;
        private readonly ICommonService _icommonservice;
        private readonly IEntityBaseRepository<ClassInfo> _classInfoRepository;
        private readonly IRazorService _iRazorService;
        public TasksController(
            ITasksService itasksservice, ICommonService icommonservice, IEntityBaseRepository<ClassInfo> classInfoRepository, IRazorService iRazorService)
        {
            _itasksservice = itasksservice;
            _icommonservice = icommonservice;
            _classInfoRepository = classInfoRepository;
            _iRazorService = iRazorService;
        }
        [AcceptVerbs("POST", "GET")]
        [Route("GetAllStaffs")]
        public HttpResponseMessage GetAllStaffs(string AgencyID)
        {
            string[] words = AgencyID.Split('~');
            string agencyId = words[0];
            string timeZone = words[1];
            SearchTasksViewModel model = new SearchTasksViewModel();
            ResponseViewModel responceViewModel;
            if (string.IsNullOrEmpty(agencyId))
            {
                model.AgencyId = 0;
            }
            else
            {
                model.AgencyId = Convert.ToInt32(agencyId);
            }
            model.order = "FirstName";
            if (string.IsNullOrEmpty(timeZone))
            {
                model.TimeZone = "India Standard Time";
            }
            else
            {
                model.TimeZone = timeZone;
            }


            _itasksservice.GetAllStaffs(model, out responceViewModel);

            var a = responceViewModel.Content;

            var b = Json(a);
            var c = Json(new[] { a });
            return Request.CreateResponse(HttpStatusCode.OK, a, "application/json");
        }
        [AcceptVerbs("POST", "GET")]
        [Route("GetAllClass")]
        public HttpResponseMessage GetAllClass(string AgencyID)
        {
            string[] words = AgencyID.Split('~');
            string agencyId = words[0];
            string timeZone = words[1];
            SearchTasksViewModel model = new SearchTasksViewModel();
            ResponseViewModel responceViewModel;
            if (string.IsNullOrEmpty(agencyId))
            {
                model.AgencyId = 0;
            }
            else
            {
                model.AgencyId = Convert.ToInt32(agencyId);
            }
            model.order = "ClassName";
            if (string.IsNullOrEmpty(timeZone))
            {
                model.TimeZone = "India Standard Time";
            }
            else
            {
                model.TimeZone = timeZone;
            }
            _itasksservice.GetAllClass(model, out responceViewModel);
            var result = responceViewModel.Content;
            return Request.CreateResponse(HttpStatusCode.OK, result, "application/json");
        }
        [AcceptVerbs("POST", "GET")]
        [Route("GetAll")]
        public HttpResponseMessage GetAll(string AgencyID)
                     {
            string[] words = AgencyID.Split('~');
            string agencyId = words[0];
            string timeZone = words[1];
            string taskType = words[2];
            string staffId =  words[3];
            string familyId = words[4];
            SearchTasksViewModel model = new SearchTasksViewModel();
            ResponseViewModel responceViewModel;
            if (string.IsNullOrEmpty(agencyId))
            {
                model.AgencyId = 0;
            }
            else
            {
                model.AgencyId = Convert.ToInt32(agencyId);
              
            }
            if(staffId=="null")
            {
                model.StaffId = 0;
            }
            else
            {
                model.StaffId = Convert.ToInt32(staffId);
            }
            if (familyId == "null")
            {
                model.FamilyId = 0;
            }
            else
            {
                model.FamilyId = Convert.ToInt32(familyId);
            }

            model.order = "AgencyId";
            if (string.IsNullOrEmpty(timeZone))
            {
                model.TimeZone = "India Standard Time";
            }
            else
            {
                model.TimeZone = timeZone;
            }
            model.TaskType = taskType;

            _itasksservice.GetAllTasks(model, out responceViewModel);

            var result = responceViewModel.Content;
            return Request.CreateResponse(HttpStatusCode.OK, result, "application/json");
        }
        [HttpPost]
        [Route("GetAllTasks")]
        public HttpResponseMessage GetAllTasks(SearchTasksViewModel model)
        {
            ResponseViewModel responceViewModel;
            _itasksservice.GetAllTasks(model, out responceViewModel);
            return Request.CreateResponse(HttpStatusCode.OK, responceViewModel, "application/json");
        }
        [HttpPost]
        [Route("AddTasks")]
        public HttpResponseMessage AddTasks(RootObject models)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            HttpResponseMessage badresponse;
            if (models != null)
            {
                char[] charsToTrim = { '[', ']' };
                var m = models.models;
                string modelTrimmed = m.Trim(charsToTrim);
                // Deserialize
                TasksViewModel serializedTaskViewModel = null;
                try
                {
                    serializedTaskViewModel = new JavaScriptSerializer().Deserialize<TasksViewModel>(modelTrimmed);
                }
                catch (Exception ex)
                {

                    throw;
                }

                ResponseInformation transaction;
                _itasksservice.AddTasks(serializedTaskViewModel, out transaction);

                /////////For seding mail to parents 

                //////////////////////////////////////

                responseViewModel.Content = serializedTaskViewModel;
                responseViewModel.IsSuccess = transaction.ReturnStatus;
                responseViewModel.ReturnMessage = transaction.ReturnMessage;
                responseViewModel.ValidationErrors = transaction.ValidationErrors;
                responseViewModel.Id = transaction.ID;
                if (transaction.ReturnStatus == false)
                {
                    badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                    return badresponse;
                }
                #region For Sending Mails To Parents related to classes     
                if (serializedTaskViewModel.IsSendEmailChecked != null && serializedTaskViewModel.IsSendEmailChecked.Value)
                {
                    string MailSubject = AppConstants.NewEventCreated;
                    string classNames = string.Empty;
                    List<long> classIds = new List<long>();
                    if (serializedTaskViewModel.ClassIdJson != null && serializedTaskViewModel.ClassIdJson.Count > 0)
                    {
                        serializedTaskViewModel.ClassIdJson.ForEach(x =>
                        {
                            classIds.Add(x.Id);
                        });

                        classNames = String.Join(",", _classInfoRepository.FindBy(l => classIds.Contains(l.ID)).Select(n => n.ClassName).ToList());
                    }

                    Uri currentUrl = HttpContext.Current.Request.Url;
                    var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
                    var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.EventMail, _icommonservice.GetMd5Hash(CDCEmailTemplates.EventMail), new
                    {
                        Url = url,
                        Title = serializedTaskViewModel.Title,
                        Class = classNames,
                        EventDesc = serializedTaskViewModel.Description,
                        Start = Convert.ToString(_icommonservice.ConvertFromUTC(serializedTaskViewModel.Start, serializedTaskViewModel.TimeZone)),
                        End = Convert.ToString(_icommonservice.ConvertFromUTC(serializedTaskViewModel.End, serializedTaskViewModel.TimeZone)),
                        TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                        Logo = url + ConfigurationManager.AppSettings["Logo"]
                    });
                    IDViewModel idVm = new IDViewModel();
                    idVm.classIds = classIds;
                    idVm.AgencyId = serializedTaskViewModel.AgencyId.Value;
                    var emailIdParents = (List<tblParentInfoViewModel>)_icommonservice.GetParentList(idVm).Content;
                    foreach (var item in emailIdParents)
                    {
                        //if (item.IsParticipantAttendanceMailReceived != null && item.IsParticipantAttendanceMailReceived.Value)
                        //{
                        _icommonservice.SendEmail(item.EmailId, MailSubject, result);
                        //}
                    }
                    #endregion


                }
                ResponseViewModel responceViewModel = new ResponseViewModel();
                var a = responceViewModel.Content;
                return Request.CreateResponse(HttpStatusCode.OK, a, "application/json");
            }
            var errors = ModelState.Errors();
            responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
            responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
            responseViewModel.ReturnStatus = false;
            badresponse = Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            return badresponse;
        }


        [HttpPost]
        [Route("UpdateTasks")]
        public HttpResponseMessage UpdateTasks(RootObject models)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            HttpResponseMessage badresponse;
            if (models != null)
            {
                char[] charsToTrim = { '[', ']' };
                var rootModel = models.models;
                string modelTrimmed = rootModel.Trim(charsToTrim);
                // Deserialize
                TasksViewModel serializedTaskViewModel = new JavaScriptSerializer().Deserialize<TasksViewModel>(modelTrimmed);
                ResponseInformation transaction;
                _itasksservice.UpdateTasks(serializedTaskViewModel, out transaction);

                /////////////////////////////////////////
                responseViewModel.Content = serializedTaskViewModel;
                responseViewModel.IsSuccess = transaction.ReturnStatus;
                responseViewModel.ReturnMessage = transaction.ReturnMessage;
                responseViewModel.ValidationErrors = transaction.ValidationErrors;
                responseViewModel.Id = transaction.ID;
                if (transaction.ReturnStatus == false)
                {
                    badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                    return badresponse;
                }
                #region For Sending Mails To Parents related to classes     
                if (serializedTaskViewModel.IsSendEmailChecked != null && serializedTaskViewModel.IsSendEmailChecked.Value)
                {

                    string MailSubject = AppConstants.NewEventCreated;
                    string classNames = string.Empty;
                    List<long> classIds = new List<long>();
                    if (serializedTaskViewModel.ClassIdJson != null && serializedTaskViewModel.ClassIdJson.Count > 0)
                    {
                        serializedTaskViewModel.ClassIdJson.ForEach(x =>
                        {
                            classIds.Add(x.Id);
                        });

                        classNames = String.Join(",", _classInfoRepository.FindBy(l => classIds.Contains(l.ID)).Select(n => n.ClassName).ToList());
                    }
                    Uri currentUrl = HttpContext.Current.Request.Url;
                    var url = currentUrl.Scheme + Uri.SchemeDelimiter + currentUrl.Host + ":" + currentUrl.Port;
                    var result = _iRazorService.GetRazorTemplate(CDCEmailTemplates.EventMail, _icommonservice.GetMd5Hash(CDCEmailTemplates.EventMail), new
                    {
                        Url = url,
                        Title = serializedTaskViewModel.Title,
                        Class = classNames,
                        EventDesc = serializedTaskViewModel.Description,
                        Start = Convert.ToString(_icommonservice.ConvertFromUTC(serializedTaskViewModel.Start, serializedTaskViewModel.TimeZone)),
                        End = Convert.ToString(_icommonservice.ConvertFromUTC(serializedTaskViewModel.End, serializedTaskViewModel.TimeZone)),
                        TopLogo = url + ConfigurationManager.AppSettings["TopLogo"],
                        Logo = url + ConfigurationManager.AppSettings["Logo"]
                    });
                    IDViewModel idVm = new IDViewModel();
                    idVm.classIds = classIds;
                    idVm.AgencyId = serializedTaskViewModel.AgencyId.Value;
                    var emailIdParents = (List<tblParentInfoViewModel>)_icommonservice.GetParentList(idVm).Content;
                    foreach (var item in emailIdParents)
                    {
                        //if (item.IsParticipantAttendanceMailReceived != null && item.IsParticipantAttendanceMailReceived.Value)
                        //{
                        _icommonservice.SendEmail(item.EmailId, MailSubject, result);
                        //}
                    }
                    #endregion
                  
                }
                ResponseViewModel responceViewModel = new ResponseViewModel();
                var a = responceViewModel.Content;
                return Request.CreateResponse(HttpStatusCode.OK, a, "application/json");         
            }
            var errors = ModelState.Errors();
            responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
            responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
            responseViewModel.ReturnStatus = false;
            badresponse = Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            return badresponse;

        }
      
        [AcceptVerbs("POST", "GET")]
        [Route("DeleteTasksById")]
        public HttpResponseMessage DeleteTasksById(RootObject models)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            HttpResponseMessage badresponse;
            if (models != null)
            {
                char[] charsToTrim = { '[', ']' };
                var m = models.models;
                string modelTrimmed = m.ToString().Trim(charsToTrim);
                // Deserialize
                TasksViewModel serializedTaskViewModel = new JavaScriptSerializer().Deserialize<TasksViewModel>(modelTrimmed);

                ResponseInformation transaction;
                _itasksservice.DeleteTasksById(serializedTaskViewModel, out transaction);
                responseViewModel.Content = serializedTaskViewModel;
                responseViewModel.IsSuccess = transaction.ReturnStatus;
                responseViewModel.ReturnMessage = transaction.ReturnMessage;
                responseViewModel.ValidationErrors = transaction.ValidationErrors;
                responseViewModel.Id = transaction.ID;
                if (transaction.ReturnStatus == false)
                {
                    badresponse = Request.CreateResponse(HttpStatusCode.BadRequest, responseViewModel);
                    return badresponse;
                }
                SearchTasksViewModel model = new SearchTasksViewModel();
                ResponseViewModel responceViewModel;
                _itasksservice.GetAllTasks(model, out responceViewModel);
                var result = responceViewModel.Content;
                return Request.CreateResponse(HttpStatusCode.OK, result, "application/json");
            }
            var errors = ModelState.Errors();
            responseViewModel.ReturnMessage = ModelStateHelper.ReturnErrorMessages(errors);
            responseViewModel.ValidationErrors = ModelStateHelper.ReturnValidationErrors(errors);
            responseViewModel.ReturnStatus = false;
            badresponse = Request.CreateResponse<ResponseViewModel>(HttpStatusCode.BadRequest, responseViewModel);
            return badresponse;
        }
    }
}