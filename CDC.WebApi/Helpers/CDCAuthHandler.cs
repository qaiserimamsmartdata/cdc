﻿using CDC.ViewModel.Common;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
namespace CDC.WebApi.Helpers
{

    public class CDCAuthHandler : DelegatingHandler
    {
        IEnumerable<string> _authHeaderValues;
        //public MembershipContext ValidateToken(string Token)
        //{
        //    var membershipCtx = new MembershipContext();
        //    //byte[] data = Convert.FromBase64String(Token);
        //    //DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
        //    //if (when < DateTime.UtcNow.AddDays(-30))
        //    //{
        //    //    return membershipCtx;
        //    //}
        //    var user = _userRepository.FindBy(x => x.Token == Token).FirstOrDefault();
        //    if (user != null && DateTime.UtcNow < user.TokenExpiryDate)
        //    {
        //        var userRoles = GetUserRoles(user.Username);
        //        membershipCtx.User = user;

        //        var identity = new GenericIdentity(user.ID.ToString());
        //        membershipCtx.Principal = new GenericPrincipal(
        //            identity,
        //            userRoles.Select(x => x.Name).ToArray());
        //    }
        //    return membershipCtx;
        //}
      
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                request.Headers.TryGetValues("Authorization", out _authHeaderValues);
                if(_authHeaderValues==null)
                request.Headers.TryGetValues("api_key", out _authHeaderValues);
                if (_authHeaderValues == null)
                    return base.SendAsync(request, cancellationToken); // cross fingers

                var tokens = _authHeaderValues.FirstOrDefault();
                tokens = tokens.Replace("Bearer", "").Trim();
                if (!string.IsNullOrEmpty(tokens))
                {
                    //byte[] data = Convert.FromBase64String(tokens);
                    //string decodedString = Encoding.UTF8.GetString(data);
                    //string[] tokensValues = decodedString.Split(':');
                    var membershipService = request.GetMembershipService();

                    ResponseViewModel membershipCtx = membershipService.ValidateToken(tokens);
                    if (membershipCtx.user != null)
                    {
                        IPrincipal principal = membershipCtx.principal;
                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;
                    }
                    else // Unauthorized access - wrong crededentials
                    {
                        var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                        var tsc = new TaskCompletionSource<HttpResponseMessage>();
                        tsc.SetResult(response);
                        return tsc.Task;
                    }
                }
                else
                {
                    var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(response);
                    return tsc.Task;
                }
                 return base.SendAsync(request, cancellationToken);
            }
            catch
            {
                var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(response);
                return tsc.Task;
            }
        }

    }
}