﻿using System.Data.Entity;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using CDC.Data;
using CDC.Data.Infrastructure;
using CDC.Data.Models;
using CDC.Data.Repositories;
using CDC.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CDC.Services.Services.Staff;
using CDC.Services.Abstract.Staff;
using CDC.Services.Services.Tools.Staff;
using CDC.Services.Abstract.Tools.Staff;
using CDC.Services.Services.Family;
using CDC.Services.Abstract.Family;

namespace CDC.WebApi
{
    /// <summary>
    ///     The AutoFac API Config Class
    /// </summary>
    public class AutofacWebapiConfig
    {
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
            config.InitializeReceiveStripeWebHooks();
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            // EF ParkContext
            builder.RegisterType<CDCContext>().As<DbContext>().SingleInstance();

            builder.Register(c => new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new CDCContext())))
                .As<UserManager<ApplicationUser>>().SingleInstance();

            builder.RegisterType<DbFactory>()
                .As<IDbFactory>()
                .InstancePerRequest();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerRequest();

            builder.RegisterGeneric(typeof (EntityBaseRepository<>))
                .As(typeof (IEntityBaseRepository<>))
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof (CommonService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces().InstancePerRequest();

            builder.RegisterType<LeaveServices>()
                .As<ILeaveServices>()
                .InstancePerRequest();

            builder.RegisterType<StatusService>()
               .As<IStatusServices>()
               .InstancePerRequest();
            builder.RegisterType<PasswordService>()
              .As<IPasswordService>()
              .InstancePerRequest();
            
            //builder.RegisterType<CommonService>()
            //    .As<ICommonService>()
            //    .InstancePerRequest();
            //builder.RegisterType<IUserService>()
            //    .As<IUserService>()
            //    .InstancePerRequest();

            //builder.RegisterType<StudentService>()
            //    .As<IStudentService>()
            //    .InstancePerRequest();
            //builder.RegisterType<ClassService>()
            //    .As<IClassService>()
            //    .InstancePerRequest();

            //builder.RegisterType<SkillService>()
            //    .As<ISkillService>()
            //    .InstancePerRequest();

            //builder.RegisterType<CategoryService>()
            //    .As<ICategoryService>()
            //    .InstancePerRequest();

            //builder.RegisterType<StudentScheduleService>()
            //    .As<IStudentScheduleService>()
            //    .InstancePerRequest();

            //builder.RegisterType<RoomService>()
            //    .As<IRoomService>()
            //    .InstancePerRequest();

            //builder.RegisterType<SessionService>()
            //    .As<ISessionService>()
            //    .InstancePerRequest();

            //builder.RegisterType<LessonPlanService>()
            //    .As<ILessonPlanService>()
            //    .InstancePerRequest();

            //builder.RegisterType<AgencyService>()
            //    .As<IAgencyService>()
            //    .InstancePerRequest();
            //builder.RegisterType<StaffService>()
            //    .As<IStaffService>()
            //    .InstancePerRequest();

            //builder.RegisterType<DesignationService>()
            //    .As<IDesignationService>()
            //    .InstancePerRequest();

            //builder.RegisterType<StatusService>()
            //    .As<IStatusServices>()
            //    .InstancePerRequest();

            //builder.RegisterType<LeaveServices>()
            //    .As<ILeaveServices>()
            //    .InstancePerRequest();

            //builder.RegisterType<LeaveTypeService>()
            //    .As<ILeaveTypeService>()
            //    .InstancePerRequest();

            //builder.RegisterType<EnrollService>()
            //    .As<IEnrollService>()
            //    .InstancePerRequest();

            //builder.RegisterType<AgencyRegistrationService>()
            //    .As<IAgencyRegistrationService>()
            //    .InstancePerRequest();

            // builder.RegisterType<StaffScheduleService>()
            //.As<IStaffScheduleService>()
            //.InstancePerRequest();

            #region Database Initialization

            //builder.RegisterType<CDCMasterData>()
            //    .As<IDatabaseInitializer<CDCContext>>()
            //    .SingleInstance();

            #endregion

            Container = builder.Build();

            return Container;
        }
    }
}