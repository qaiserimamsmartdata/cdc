Imports App_Code.CSCode
Imports HRSNetSCOREBusinessObjects
Imports App_Code.VBCode
Imports NetscoreBusinessObjects
Imports System.Data
Imports Scripting
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Collections.Generic
Imports CSATApiWS
Imports NetScore.Entities
Imports Microsoft.LearningComponents.Storage
Imports System.Collections.ObjectModel
Imports System.Web.Script.Serialization
Imports System.Web.Services



Partial Class Admin_ActivityPreview
    Inherits App_Code.VBCode.WebFormBase

    Dim _selectedLeftNavItem As String = String.Empty
    Dim ceactivity_obj As CEActivitiesObjects.CEActivity
    Dim customActivityType As NetScore.Entities.CustomActivityType
    Dim CurrentPageName As String
    Dim bShowParentLink As Boolean = False
    Dim IsPrestestExist As Boolean = False
    Dim EvalId As Integer
    Dim bHasForum As Boolean = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings("HasForum"))


    Private Property Mode() As String
        Get
            Dim tempMode As String
            If Session("Mode") Is Nothing Then
                tempMode = "new"
            Else
                tempMode = Session("Mode")
            End If
            Return tempMode
        End Get
        Set(ByVal value As String)
            Session("Mode") = value
        End Set

    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("Login") <> True AndAlso Request.Form("keepsession") & "" <> "Y" Then
            Response.Redirect("login.aspx")
        ElseIf Request.Form("keepsession") = "Y" Then
            Session("Login") = True
            Session("adminUserID") = Request.Form("adminUserID")
            Session("adminUsername") = Request.Form("adminUsername")

            Try
                Session("superAdmin") = CType(Request.Form("superAdmin"), Boolean)
            Catch ex As Exception

            End Try

            Try
                Session("clientAdmin") = CType(Request.Form("clientAdmin"), Boolean)
            Catch ex As Exception

            End Try

            Session("TasksAllowed") = Request.Form("TasksAllowed")
        End If

        UserID = 0
        If UserPageCode & "" = "" Then
            UserPageCode = "AO"
        End If

        Try
            If Not IsPostBack Then
                App_Code.CSCode.AdminUserSettings.InsertAdminAudit(New App_Code.CSCode.AdminAudit("Viewed activity preview - activityId=" & ProductID.ToString(), "View activity preview"))
            End If

            If Page.IsPostBack Then
                If CurrentPageCode = "AO" Then
                    Dim _courseManager = New NetScore.BAL.CourseManager
                    Dim overviewmoveupid As String = Request.Form("overviewmoveupid")
                    If (overviewmoveupid + "" <> "") Then
                        _courseManager.MoveOverviewUp(overviewmoveupid, ProductID)
                    End If

                    Dim overviewmovedownid As String = Request.Form("overviewmovedownid")
                    If (overviewmovedownid + "" <> "") Then
                        _courseManager.MoveOverviewDown(overviewmovedownid, ProductID)
                    End If


                End If

                Dim operation As String = String.Empty
                Dim description As String = String.Empty
                Dim activityId As String = String.Empty


                If CurrentPageCode = "TC" Or CurrentPageCode = "TEL" Or CurrentPageCode = "MAC" Or CurrentPageCode = "RESCLIB" Then
                    Dim _courseManager = New NetScore.BAL.CourseManager
                    Dim moveupid As String = Request.Form("moveupid")
                    If (moveupid + "" <> "") Then
                        activityId = moveupid
                        _courseManager.MoveContentUp(Convert.ToInt16(moveupid))
                        operation = "Moved up content of activity (ActivityId=" & activityId
                        description = "Move Up content of activity"
                        'Me.RadGrid1.Rebind()
                    End If

                    Dim movedownid As String = Request.Form("movedownid")
                    If (movedownid + "" <> "") Then
                        activityId = movedownid
                        _courseManager.MoveContentDown(Convert.ToInt16(movedownid))
                        operation = "Moved down content of activity (ActivityId=" & activityId
                        description = "Move down content of activity"
                        'Me.RadGrid1.Rebind()
                    End If

                    Dim deleteid As String = Request.Form("deleteid")
                    If (deleteid + "" <> "") Then
                        activityId = deleteid
                        _courseManager.DeleteContent(Convert.ToInt16(deleteid))
                        operation = "Deleted content of activity (ActivityId=" & activityId
                        description = "Deleted content of activity"
                    End If


                End If

                App_Code.CSCode.AdminUserSettings.InsertAdminAudit(New App_Code.CSCode.AdminAudit(description, operation))

            End If

            ceactivity_obj = New CEActivitiesObjects.CEActivity(ProductID)

            customActivityType = Utility.GetCustomActivityTypes(ceactivity_obj.CustomActivityTypeID)

            If ceactivity_obj.ParentID <> ceactivity_obj.CEActivityID And ceactivity_obj.ParentID > 0 Then
                Dim parent_obj = New CEActivitiesObjects.CEActivity(ceactivity_obj.ParentID)

                Dim parentCustomType As NetScore.Entities.CustomActivityType = GetCustomActivityTypes(parent_obj.CustomActivityTypeID)
                If parentCustomType.FunctionalActivityType.ActivityCode = "GROUP" Then
                    bShowParentLink = True
                End If

            End If


            If ceactivity_obj.IsMainEntryForSymposia Then
                IsSymposia = True
            End If

            GetCourseDetails()

            Dim IsAttendence As Boolean = False
            If customActivityType.FunctionalActivityType.ActivityCode = "LIVEEVENT" Then
                IsAttendence = True
            End If
            'Dim sql As String
            'sql = "select CEOfferingID from NS_CEActivity_Credits where CEActivityID = '" & ProductID & "'"
            'Dim dm As IDataManager
            'dm = New DataManager
            'If dm.GetSimpleValue(sql) = "1" Then
            '    IsAttendence = True
            'End If
            'dm = Nothing

            If IsAttendence Then
                If True Then 'eligible to view the product

                    Me.pnlRegisterCourse.Visible = False
                    pnlCourseDetails.Visible = True
                    GetCourseWorkflow()
                Else    'did not purchase the product yet, need badgeid



                End If
            End If

            If Not IsAttendence Then

                If True Then 'eligible to view the product


                    Me.pnlRegisterCourse.Visible = False
                    pnlCourseDetails.Visible = True
                    GetCourseWorkflow()
                Else 'did not purchase the product yet


                End If

            End If


        Catch ex As Exception
            Master.ErrorLabel = ex.Message
        Finally

        End Try


    End Sub

    Private Function getUserCertType(ByVal UserID As String) As String
        'Dim certTypeID As String = ""
        'Dim sql As String
        'Dim dm As IDataManager

        'dm = New DataManager
        'sql = "select certificateTypeID from NS_Users where UserID = '" & UserID & "'"
        'Dim r As String
        'r = dm.GetSimpleValue(sql)
        'If r & "" <> "" Then
        '    certTypeID = r
        'End If

        'Return certTypeID
        Return "N/A"
    End Function

    Private Function getParticipationCertTypeID() As String
        Dim certTypeID As String = ""
        Dim sql As String
        Dim dm As IDataManager

        dm = New DataManager
        sql = "select certificateTypeID from NS_certificateTypes where upper(description) = 'PARTICIPATION'"
        Dim r As String
        r = dm.GetSimpleValue(sql)
        If r & "" <> "" Then
            certTypeID = r
        End If

        Return certTypeID
    End Function

    Private Function getActivityCertTypes(ByVal productID As String) As ArrayList
        Dim certTypeID_A As New ArrayList
        Dim sql As String
        Dim dm As IDataManager

        dm = New DataManager
        sql = "select certificateTypeID from NS_CEActivity_Certificates where CourseID = '" & productID & "'"
        Dim dr As IDataReader
        dr = dm.GetDataReader(sql)
        If Not dr Is Nothing Then
            Do While dr.Read
                certTypeID_A.Add(CStr(dr(0) & ""))
            Loop

            dr.Close()
        End If

        Return certTypeID_A
    End Function


    Private Sub GetCourseWorkflow()
        Dim WM As IWorkFlowManager
        Try
            WM = CreateWorkFlowManager()


            dsActivitySteps = WM.GetCourseSteps(UserID, ProductID, customActivityType.CustomActivityTypeID, UserPageCode)
            For Each table As DataRow In dsActivitySteps.Tables(0).Rows
                If table.ItemArray(3) = "Pre Test" Then
                    IsPrestestExist = True
                End If
            Next

            _selectedLeftNavItem = String.Empty
            Dim IsEligibleToViewPage As Boolean = BuildNavigationForPreview(dsActivitySteps, CurrentPageCode, UserPageCode, ProductID, Me.lblCourseNavigation, Me.lblPrevLink, Me.lblNextLink, _selectedLeftNavItem)

            DisplayContent()
        Finally
            WM = Nothing
        End Try
    End Sub

    Public Function BuildNavigationForPreview(ByVal DS As DataSet, ByVal CurrentPageCode As String, ByVal UserPageCode As String, ByVal ProductID As Integer, ByVal NavLabel As Label, ByVal PrevLabel As Label, ByVal NextLabel As Label, ByRef currentNavName As String) As Boolean
        Dim rowcount As Integer = DS.Tables(0).Rows.Count
        Dim NavString As String = ""
        Dim NextLink As String = ""
        Dim PrevLink As String = ""
        Dim isEligibleToViewPage As Boolean = False

        Dim ceactivity_obj As CEActivitiesObjects.CEActivity
        ceactivity_obj = New CEActivitiesObjects.CEActivity(ProductID)
        Dim maxcredits As Double = ceactivity_obj.MaxCredits
        'Dim customActivityType As NetScore.Entities.CustomActivityType = Utility.GetCustomActivityTypes(ceactivity_obj.CustomActivityTypeID)

        Dim alert_msg As String
        alert_msg = "STOP! IMPORTANT NOTICE!\n\n"
        alert_msg &= "By clicking on the OK button, you will be locking in your credits for the ENTIRE conference. You will no longer be able to add credits, claim credits,  or change your record in any way for the remainder of the conference.\n\n"
        alert_msg &= "Click on Cancel to leave your credits untouched, and return to the portal to claim them at the conclusion of the conference."

        If rowcount > 0 Then
            NavString = "<div class=""step-list""><ul>"
            Dim counter As Integer = 0
            Dim CurrentStepOrder As Integer = 0
            Dim UserStepOrder As Integer = 0
            Dim NextStepPageCode As String = ""

            For counter = 0 To rowcount - 1
                Dim StepOrder As Integer = DS.Tables(0).Rows(counter).Item("StepOrder")
                Dim StepName As String = DS.Tables(0).Rows(counter).Item("StepName")
                Dim UserStatus As String = "1" 'DS.Tables(0).Rows(counter).Item("UserStatus")
                Dim StepCode As String = DS.Tables(0).Rows(counter).Item("StepCode")
                Dim IsEligible As Boolean = True 'DS.Tables(0).Rows(counter).Item("IsEligible")
                Dim StepURL As String = "ActivityPreview.aspx" 'DS.Tables(0).Rows(counter).Item("StepURL")

                Dim PrevStepID As Integer = DS.Tables(0).Rows(counter).Item("PrevStepID")
                Dim PrevURL As String = "ActivityPreview.aspx" 'DS.Tables(0).Rows(counter).Item("PrevURL")
                Dim PrevCode As String = DS.Tables(0).Rows(counter).Item("PrevCode")

                Dim NextStepID As Integer = DS.Tables(0).Rows(counter).Item("NextStepID")
                Dim NextURL As String = "ActivityPreview.aspx" 'DS.Tables(0).Rows(counter).Item("NextURL")
                Dim NextCode As String = DS.Tables(0).Rows(counter).Item("NextCode")


                If UCase(StepCode) = UCase(UserPageCode) Then
                    UserStepOrder = StepOrder
                End If

                If UCase(StepCode) = UCase(CurrentPageCode) Then
                    CurrentStepOrder = StepOrder
                    If IsEligible Then
                        isEligibleToViewPage = True
                    End If

                    If NextStepID <> 0 Then
                        NextLink = NextURL & "?pagecode=" & NextCode & "&ProductID=" & ProductID
                        NextStepPageCode = NextCode
                    Else
                        NextLink = "activitypreview.aspx?pagecode=AO&ProductID=" & ProductID
                    End If

                    If PrevStepID <> 0 Then
                        PrevLink = PrevURL & "?pagecode=" & PrevCode & "&ProductID=" & ProductID
                    End If
                Else
                    If CurrentStepOrder > 0 And CurrentStepOrder = counter Then 'one step forward
                        If Not IsEligible Then
                            If customActivityType.FunctionalActivityType.ActivityCode = "TELECONFERENCE" AndAlso UCase(CurrentPageCode) = "CI" Then
                                Dim ActivityPhaseID = GetPhaseActivityID(ProductID)
                                If ActivityPhaseID <> "5" AndAlso ActivityPhaseID <> "1" Then
                                    NextLink = Replace(NextLink, "?pagecode=TEL&", "?pagecode=EV&")
                                Else
                                    NextLink = ""
                                End If
                            Else
                                NextLink = ""
                            End If

                            'NextLink = ""
                        End If
                    End If

                End If

                If ceactivity_obj.CEOfferingTypeDescription = "symposia" And StepName = "Webinar" Then
                    StepName = "Presentation"
                End If



                StepName = "<span class=""stnum"">Step " & StepOrder & ":</span> " & StepName
                If IsEligible Then
                    If UCase(StepCode) = UCase(CurrentPageCode) Then
                        StepURL = StepURL & "?pagecode=" & StepCode & "&ProductID=" & ProductID

                        NavString &= "<li>"
                        NavString &= "<a href=""" & StepURL & """ class=""current"">" & StepName & "</a></li>"

                        ' Torsten 2-11-2008: set current stepname so we can return to ProductHomepage and display it as headline in the content area
                        currentNavName = DS.Tables(0).Rows(counter).Item("StepName")

                    Else
                        StepURL = StepURL & "?pagecode=" & StepCode & "&ProductID=" & ProductID
                        If UserStepOrder = 0 Or (UserStepOrder > 0 And CurrentStepOrder = 0) Then
                            NavString &= "<li class=""complete"">"
                        Else
                            NavString &= "<li>"
                        End If

                        If UCase(StepCode) = "CERT" And UserPageCode = "SS" Then
                            If False Then
                                NavString &= "<a href=# onclick=""alert('You cannot claim more than " & maxcredits & " credits. Please adjust you selection.')"" >" & StepName & "</a></li>"
                            Else
                                NavString &= "<a href=# onclick=""if(confirm('" & alert_msg & "')){document.location='" & StepURL & "';return false;}"" >" & StepName & "</a></li>"
                            End If

                        Else
                            NavString &= "<a href=""" & StepURL & """>" & StepName & "</a></li>"
                        End If

                    End If
                Else
                    NavString &= "<li class=""disabled"">"
                    NavString &= "<span class=""nonlink"">" & StepName & "</li>"
                End If


            Next
            NavString &= "</ul></div>"
            NavLabel.Text = NavString

            If PrevLink <> "" Then
                PrevLabel.Text = "<a href=""" & PrevLink & """ class=""prevlink"">Prev</a>"
            End If
            If NextLink <> "" Then
                If NextStepPageCode = "CERT" And UserPageCode = "SS" Then
                    'NextLabel.Text = "<a href=# onclick=""if(confirm('You will not be allowed to change your claimed credits after this step. Click OK to proceed.')){document.location='" & NextLink & "';return false;}"" class=""nextlink"">Next</a>"

                    If False Then
                        NextLabel.Text = "<a href=# onclick=""alert('You cannot claim more than " & maxcredits & " credits. Please adjust you selection.')"" class=""nextlink"">Print Certificate for Entire Conference</a>"

                    Else
                        NextLabel.Text = "<a href=# onclick=""if(confirm('" & alert_msg & "')){document.location='" & NextLink & "';return false;}"" class=""nextlink"">Print Certificate for Entire Conference</a>"
                        NextLabel.Text &= "<a href=# class=""nextlink"" onclick=""javascript:return false;"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Print Transcript</a>"
                    End If
                Else
                    NextLabel.Text = "<a href=""" & NextLink & """ class=""nextlink"">Next</a>"
                End If

            End If

        End If
        Return isEligibleToViewPage
    End Function

    Private Sub DisplayContent()

        Dim edit_url As String = ""
        Dim add_url As String = ""

        Dim preHTML As String ' string that will serve as a prefix to the content; typically used for title
        Dim preHTML1 As String

        ' TODO: this code can be eliminated when the Step is driven by the database
        preHTML = "<h4>" & _selectedLeftNavItem
        CurrentPageName = _selectedLeftNavItem
        ''If GetActivityTypeDescription(ProductID) = "symposia" And _selectedLeftNavItem = "Webinar" Then
        ''    preHTML = "<h4>Presentation"
        ''End If

        build_instruction(UCase(CurrentPageCode))

        Try
            Select Case (CurrentPageCode)


                Case "AO" 'Activity Overview
                    edit_url = "CEActivityOverview.aspx?preview=Y&ActivityID=" & ProductID
                    edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=1000, height=750, scrollbars=1, status=1, resizable=1');"" class=""submit"">Edit</button>"
                    preHTML &= edit_url & "</h4>"


                    preHTML &= "<input type=""hidden"" name=""overviewmoveupid"" id=""overviewmoveupid"" value="""" />"
                    preHTML &= "<input type=""hidden"" name=""overviewmovedownid"" id=""overviewmovedownid"" value=""""  />"


                    Me.lblContent.Text = String.Empty
                    GetOverview(Me.lblContent, preHTML, String.Empty)
                    Me.ltl_purchased_text.Text = GetTextForOverview(ProductID, True)



                Case "CI" 'CME Information
                    edit_url = "CEActivityCMEInfo.aspx?preview=Y&ActivityID=" & ProductID
                    edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=1000, height=750, scrollbars=1, status=1, resizable=1');"" class=""submit"">Edit</button>"
                    preHTML &= edit_url & "</h4>"

                    GetCMEInfo(Me.lblContent, preHTML, String.Empty)
                    ' Torsten 2-9-2008: introduces TEL for Teleconferences
                    Me.ltl_purchased_text.Text = GetTextForOverview(ProductID, True)

                Case "TEL"
                    edit_url = "CEActivityContentTeleconference.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID="
                    edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=900, height=900, scrollbars=1, status=1, resizable=1');"" class=""submit"">Edit</button>"
                    preHTML &= edit_url & "&nbsp;"

                    preHTML &= "<input type=""hidden"" name=""moveupid"" id=""moveupid"" value="""" />"
                    preHTML &= "<input type=""hidden"" name=""movedownid"" id=""movedownid"" value=""""  />"
                    preHTML &= "<input type=""hidden"" name=""deleteid"" id=""deleteid"" value=""""  />"
                    Me.GetTeleconferenceContent(Me.lblContent, preHTML, String.Empty)

                    ' GetPresentationContent(Me.lblContent, preHTML, String.Empty)
                Case "TC" 'Presentations and Post Activity Assessment

                    edit_url = "CEActivityDescription.aspx?Preview=Y&ActivityID=" & ProductID
                    edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add/Edit Content Description</button>"

                    preHTML &= edit_url & "</h4>"
                    preHTML &= "<input type=""hidden"" name=""moveupid"" id=""moveupid"" value="""" />"
                    preHTML &= "<input type=""hidden"" name=""movedownid"" id=""movedownid"" value=""""  />"
                    preHTML &= "<input type=""hidden"" name=""deleteid"" id=""deleteid"" value=""""  />"

                    GetPresentationContent(Me.lblContent, preHTML, String.Empty)
                Case "RESCLIB" 'Presentations and Post Activity Assessment

                    edit_url = "CEActivityDescription.aspx?PageCode=RESCLIB&Preview=Y&ActivityID=" & ProductID
                    edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add/Edit Content Description</button>"

                    preHTML &= edit_url & "</h4>"
                    preHTML &= "<input type=""hidden"" name=""moveupid"" id=""moveupid"" value="""" />"
                    preHTML &= "<input type=""hidden"" name=""movedownid"" id=""movedownid"" value=""""  />"
                    preHTML &= "<input type=""hidden"" name=""deleteid"" id=""deleteid"" value=""""  />"

                    GetPresentationContentByPageCode(Me.lblContent, "RESCLIB", preHTML, String.Empty)
                Case "SLIDES" 'Presentations and Post Activity Assessment

                    edit_url = "CEActivityDescription.aspx?preview=Y&ActivityID=" & ProductID & "&PageCode=SLIDES&ContentID="
                    edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add/Edit Content Description</button>"
                    preHTML &= edit_url & "</h4>"
                    preHTML &= "<input type=""hidden"" name=""moveupid"" id=""moveupid"" value="""" />"
                    preHTML &= "<input type=""hidden"" name=""movedownid"" id=""movedownid"" value=""""  />"
                    preHTML &= "<input type=""hidden"" name=""deleteid"" id=""deleteid"" value=""""  />"

                    GetPresentationContentByPageCode(Me.lblContent, "SLIDES", preHTML, String.Empty)
                Case "VCC" 'Presentations and Post Activity Assessment
                    BuildSessionGrid()
                    'edit_url = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&PageCode=VCC&ContentID="
                    'edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add New Content</button>"
                    ' preHTML &= edit_url & "</h4>"
                    'preHTML &= "<input type=""hidden"" name=""moveupid"" id=""moveupid"" value="""" />"
                    'preHTML &= "<input type=""hidden"" name=""movedownid"" id=""movedownid"" value=""""  />"
                    'preHTML &= "<input type=""hidden"" name=""deleteid"" id=""deleteid"" value=""""  />"

                    'GetPresentationContent(Me.lblContent, preHTML, String.Empty)
                Case "MAC"
                    edit_url = "CEActivityDescription.aspx?Preview=Y&ActivityID=" & ProductID
                    edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add/Edit Content Description</button>"

                    preHTML &= edit_url & "</h4>"
                    preHTML &= "<input type=""hidden"" name=""moveupid"" id=""moveupid"" value="""" />"
                    preHTML &= "<input type=""hidden"" name=""movedownid"" id=""movedownid"" value=""""  />"
                    preHTML &= "<input type=""hidden"" name=""deleteid"" id=""deleteid"" value=""""  />"

                    GetMeetingContent(Me.lblContent, preHTML, String.Empty)
                Case "PPAA"
                    If Not IsPostBack Then
                        GetPostTest()
                    End If

                Case "Verify"
                    'If Not IsPostBack Then
                    GetPostTestForVerify()
                    'End If
                Case "PRETEST"
                    Me.GetPretTest()

                Case "SC"

                    'Build session grid here.
                    BuildSessionGrid()
                Case "AL"
                    BuildActivityGrid()
                Case "SS"
                    'BuildSummaryGrid()
                Case "EV"
                    'If Not IsPostBack Then
                    '    Me.Mode = "new" 'Sen added
                    '    GetEvalQuestionnaire()
                    'Else
                    '    SaveEvalQuestionnaire()
                    '    Me.Mode = "retake"
                    'End If
                    GetEvalQuestionnaire()  'for preview
                Case "PRESUR"
                    GetEvalQuestionnaire(4)
                Case "AFTERSUR"
                    GetEvalQuestionnaire(5)
                Case "INFO" 'Presentations and Post Activity Assessment

                    'edit_url = "CollectUserRequiredInfo.aspx?ActivityID=" & ProductID
                    'edit_url = "&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add/Edit Required Fields</button>"
                    'preHTML &= edit_url & "</h4>"

                    GetUserActivityRequiredInfo(Me.lblContent, preHTML, String.Empty)
                Case "CERT"
                    GetCertificateLinks(lblContent, preHTML, String.Empty)
                Case "FORUM" 'Presentations and Post Activity Assessment
                    GetForumContent(Me.lblContent, preHTML, String.Empty)
            End Select

        Catch ex As Exception
            Master.ErrorLabel = "Error in DisplayContent. " & ex.Message
        End Try
    End Sub

    Private Sub build_instruction(ByVal pagecode As String)
        Dim instrn As String = ""
        'Dim customActivityType As NetScore.Entities.CustomActivityType = GetCustomActivityTypeByProductID(ProductID)

        Select Case UCase(pagecode)
            Case "AO" 'Activity Overview
                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"
                instrn &= "<li>Select the [Edit] button next to the Activity Overview to edit the Overview.</li>"
                instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"

            Case "CI" 'CME Information
                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"
                instrn &= "<li>Select the [Edit] button next to the CE Information to edit the CE Information.</li>"
                instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"

            Case "TEL"
                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"
                instrn &= "<li>Select the [Edit] button next to the Teleconference to edit the teleconference information.</li>"
                instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"

            Case "TC" 'Presentations and Post Activity Assessment
                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"

                If customActivityType.FunctionalActivityType.ActivityCode = "SYMPOSIA" Then
                    instrn &= "<li>Select the [Edit] button next to the Presentation to edit the webinar information.</li>"
                Else
                    instrn &= "<li>Select the [Edit] button next to the Webinar to edit the webinar information.</li>"
                End If


                instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"


            Case "PPAA"

                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"
                instrn &= "<li>Select the [Edit Post Test] button above the Post Test to edit the post test.</li>"
                instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"

            Case "SC"

                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"
                If IsSymposia Then
                    instrn &= "<li>Select the [Add Module] button to add a new module to the symposium.</li>"
                    instrn &= "<li>Click the title in the module list to edit module information.</li>"
                Else
                    instrn &= "<li>Select the [Add Session] button to add a new session to the live event.</li>"
                    instrn &= "<li>Click the session title in the session list to edit session information.</li>"
                End If


                instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"

            Case "SS"
                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"
                'instrn &= "<li>Select the Edit Evaluation button next to the Evaluation to edit the evaluation.</li>"
                instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"


            Case "EV"
                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"
                instrn &= "<li>Select the [Edit Evaluation] button next to the Evaluation to edit the evaluation.</li>"
                instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"


            Case "CERT"
                instrn &= "<li>Select the [Edit] button next to the activity title to edit the general information about the activity.</li>"
                instrn &= "<li>Select the [Edit Certificate] button to select the certifications available for this activity and to choose the certificate template.</li>"
                'instrn &= "<li>Select the Edit Evaluation button next to the Evaluation to edit the evaluation.</li>"
                'instrn &= "<li>Once you have edited all information on this tab, you may select the next tab on the left hand side.</li>"


        End Select

        Me.lit_instr.Text = instrn
    End Sub

    Dim customID As String
    Dim confID As String
    Dim sessionCode As String
    Dim session_ceactivityid As String

    Private Sub BuildSessionGrid()
        Me.lblContent.Visible = False
        Me.pnl_sessions_grid.Visible = True

        Dim sql As String
        sql = "select customID from NS_CEActivities where CEActivityID = '" & ProductID & "'"
        Dim dr As IDataReader
        Dim idata As IDataManager
        idata = New DataManager
        dr = idata.GetDataReader(sql)
        If Not dr Is Nothing Then
            If dr.Read Then
                customID = dr(0)
            End If
            dr.Close()
        End If

        If customID & "" <> "" Then
            confID = customID.Split("_")(0)
            sessionCode = customID.Split("_")(1)

            Dim eventcode As String = ""
            sql = "select WSProductID from NS_WS_ProductMapping where activitycustomid = '" & customID & "'"
            eventcode = idata.GetSimpleValue(sql)


            sql = "select cs.*, ce.ceactivityid from custom_sessions cs, ns_ceactivities ce where  ce.customid = cast(cs.ConferenceID as varchar(50)) + '_' + cs.SessionCode and cs.ConferenceID='" & confID & "' and cs.sessioncode <> '" & sessionCode & "'"
            '"Go" is clicked or criteria ssearch has been used like paging rebind. append criterias to sql
            If ViewState("criteria_search") = "Y" Or Request.Form("ctl00$ContentPlaceHolder1$imgSubmit_search.x") & "" <> "" Or Request.Form("ctl00$ContentPlaceHolder1$imgSubmit_search.y") & "" <> "" Then
                Dim app_sql As String = ""

                If Trim(Me.txt_sessionname.Text & "") <> "" Then
                    If app_sql <> "" Then
                        app_sql &= " and "
                    End If
                    app_sql &= "sessionname like '%" & Trim(Me.txt_sessionname.Text & "") & "%'"
                End If

                If Trim(Me.txt_sessioncode.Text & "") <> "" Then
                    If app_sql <> "" Then
                        app_sql &= " and "
                    End If
                    app_sql &= "sessioncode like '%" & Trim(Me.txt_sessioncode.Text & "") & "%'"
                End If

                If Me.ddl_sessiontype.SelectedValue <> "" Then
                    If app_sql <> "" Then
                        app_sql &= " and "
                    End If

                    If Me.ddl_sessiontype.SelectedValue = "ws" Then
                        app_sql &= "sessiontype = 'Workshop'"
                    End If

                    If Me.ddl_sessiontype.SelectedValue = "nonws" Then
                        app_sql &= "sessiontype <> 'Workshop'"
                    End If
                End If

                If Not Me.txt_SessionDate.IsEmpty Then
                    If app_sql <> "" Then
                        app_sql &= " and "
                    End If

                    app_sql &= "sessiondate = '" & Me.txt_SessionDate.SelectedDate.ToString & "'"
                End If

                If Me.ddl_certtype.SelectedValue <> "" Then
                    If app_sql <> "" Then
                        app_sql &= " and "
                    End If

                    app_sql &= "EXISTS (select * from NS_CEActivity_Certificates where certificatetypeid = " & Me.ddl_certtype.SelectedValue & " and courseid = (select CEActivityID from NS_CEActivities where customid = CAST(conferenceid as varchar) + '_' + sessioncode) )"
                End If

                If app_sql <> "" Then
                    sql &= " and " & app_sql
                    ViewState("criteria_search") = "Y"
                End If
            End If

            Dim dv As DataView
            dv = idata.GetDataView(sql)
            Me.sessions_grid.DataSource = dv
            Me.sessions_grid.DataBind()

            dv.Dispose()

            Me.ltl_claim_session.Text = "<h4>Claim Credits</h4><p>You will need to complete an evaluation and select the hours you have attended for each session. To do so, please click on the <strong>Session Name</strong> in the table below. Once you have claimed credits for that session you will be returned to this table and can continue to claim credits for all sessions.</p><p class=""morepad""><strong>You should not proceed to the next step until you have claimed credits for the entire meeting. You will only be able to print your meeting certificate once with the total number of credits claimed for the full meeting.</strong></p>"
        End If

        idata = Nothing

        If ceactivity_obj.IsMainEntryForSymposia Then
            Me.pnl_search_criteria.Visible = False

            Me.sessions_grid.Columns(0).HeaderText = "Module Name"

            Me.sessions_grid.Columns(1).Visible = False
            Me.sessions_grid.Columns(2).Visible = False
            Me.sessions_grid.Columns(5).Visible = False
            Me.sessions_grid.Columns(6).Visible = False
            Me.ltl_claim_session.Text = "<h4>" & CurrentPageName & "</h4>"
        End If
        If CurrentPageCode = "VCC" Then
            Me.sessions_grid.Columns(1).Visible = True
            Me.sessions_grid.Columns(4).Visible = False
        End If
        If Me.ceactivity_obj.getParameter("SessionSearchCriteriaShowSessionType") = "N" Then
            Me.tr_sessiontype.Visible = False
        End If

        If Me.ceactivity_obj.getParameter("SessionSearchCriteriaShowCertificateType") = "N" Then
            Me.tr_sessioncertificatetype.Visible = False
        End If
    End Sub

    Private Sub BuildActivityGrid()
        Dim idata As IDataManager
        idata = New DataManager
        If customActivityType.CustomActivityTypeCode.ToUpper() = "ONLINE PARENT" Then
            pnlWorkFlow.Visible = True
            btnForum.Visible = bHasForum
            ltlActivityDescription.Text = "<p>Click on each event to complete the event and claim credits. You may log into and out of this activity as often as necessary to complete all events. </p>"
        Else
            ltlActivityDescription.Text = "<p>Click on each event to complete the event and claim credits. You may log into and out of this activity as often as necessary to complete all events. If you prefer to only complete some events, you can do so and print your certificate at any time. Every time you complete a new event you can re print your certificate to reflect all the activities you have completed so far.</p>"
        End If

        Dim sql As String
        pnl_activity_grid.Visible = True


        customID = ceactivity_obj.CustomID

        If customID & "" <> "" Then
            confID = customID.Split("_")(0)
            sessionCode = customID.Split("_")(1)
            sql = "select CE.*, Case When CO.ReleaseDate is null then CA.DateOfEvent Else CO.ReleaseDate End as EventDate " & _
            " from NS_CEActivities CE WITH (NOLOCK) " & _
            " Left join NS_CEOfferings_Online CO WITH (NOLOCK) On CE.CEActivityID = CO.CEActivityID " & _
            " Left join NS_CEOfferings_Attendance CA WITH (NOLOCK) On CE.CEActivityID = CA.CEActivityID " & _
            " where CE.ParentID='" & ProductID & "' and CE.parentid <> CE.ceactivityid order by SortOrder, EventDate"

            Dim dv As DataView
            dv = idata.GetDataView(sql)
            Me.activity_grid.DataSource = dv
            Me.activity_grid.DataBind()
            activity_grid.HeaderRow.TableSection = TableRowSection.TableHeader
            dv.Dispose()
        End If


        Me.activity_grid.AllowPaging = False
        Me.activity_grid.AllowSorting = False

        'Me.activity_grid.PagerStyle.Visible = False

        Me.ltl_claim_activity.Text = "<h2>" & _selectedLeftNavItem & "</h2>"

        Dim ActivityAnnoucement As String = ceactivity_obj.getDescriptionBySecionTitle("AL").SectionContent
        If ActivityAnnoucement <> "" Then
            ltlAnnouncement.Text = "<p>" & ActivityAnnoucement & "</p>"
        End If
        If Not IsPostBack Then
            Dim ChildAccessflag As Integer
            Dim sqlQuery As String = " select ChildActivitesCompletionFlag from ns_ceactivities where ceactivityid =" & ProductID
            ChildAccessflag = CType(idata.GetSimpleValue(sqlQuery), Integer)
            If (ChildAccessflag = 1) Then
                RBLChildActivitiesCompletionFlag.SelectedValue = "Yes"
            ElseIf ChildAccessflag = 2 Then
                RBLChildActivitiesCompletionFlag.SelectedValue = "No"
            ElseIf ChildAccessflag = 0 Then
                If customActivityType.CustomActivityTypeCode.ToUpper() = "ONLINE PARENT" Then
                    RBLChildActivitiesCompletionFlag.SelectedValue = "No"
                Else
                    RBLChildActivitiesCompletionFlag.SelectedValue = "Yes"
                End If
            End If
        End If
        idata = Nothing

    End Sub

    Public Function getActivityLink(ByVal description As String, ByVal activityid As String) As String
        Dim result As String = ""
        result &= "<a href=""ActivityPreview.aspx?ProductID=" & activityid & """>"
        result &= description
        result &= "</a>"

        Return result
    End Function

    Dim show_total As Boolean = False

    Public IsSymposia As Boolean = False

    'Private Sub BuildSummaryGrid()
    '    show_total = True
    '    Me.sessions_grid.ShowFooter = True

    '    Me.lblContent.Visible = False
    '    Me.pnl_sessions_grid.Visible = True

    '    Dim sql As String
    '    sql = "select customID from NS_CEActivities where CEActivityID = '" & ProductID & "'"
    '    Dim dr As IDataReader
    '    Dim idata As IDataManager
    '    idata = New DataManager
    '    dr = idata.GetDataReader(sql)
    '    If Not dr Is Nothing Then
    '        If dr.Read Then
    '            customID = dr(0)
    '        End If
    '        dr.Close()
    '    End If

    '    If customID & "" <> "" Then
    '        confID = customID.Split("_")(0)
    '        sessionCode = customID.Split("_")(1)

    '        sql = "select c.ConferenceID, c.SessionName, c.SessionType, c.NofCredits, c.SessionCode, c.SessionDate, c.StartTime, c.EndTime, c.sessionstatus from custom_sessions c, NS_CEOfferings_User_Attendance u where c.ConferenceID='" & confID & "' and c.sessioncode <> '" & sessionCode & "'"
    '        sql &= " and u.userid = '" & UserID & "'"
    '        sql &= " and u.sessioncode = c.sessioncode"
    '        sql &= " and CAST(c.conferenceID as varchar(50)) + '_' + CAST(c.sessionCode as varchar(50)) = (select customID from NS_CEActivities where CEActivityID = u.CEActivityID) "
    '        Dim dv As DataView
    '        dv = idata.GetDataView(sql)
    '        Me.sessions_grid.DataSource = dv
    '        Me.sessions_grid.DataBind()

    '        dv.Dispose()
    '        Dim ss_text As String = "<h4>Credits Summary</h4><h5 class=""morepad"">Please review each sessionís credits carefully. Once you click &quot;Next&quot;, you will not be able to change the number of credits you claimed per session. You should not proceed to the next step until you have claimed credits for the entire meeting. You will only be able to print your meeting certificate once with the total number of credits claimed for the full meeting.</h5>"
    '        ss_text &= "<ul class=""lobj"">"
    '        ss_text &= "<li>To remove a session in total, click on the &quot;Remove&quot; button under the session name.</li>"
    '        ss_text &= "<li>To change the  number of credits claimed for a particular session, click on that session name which will return you to the &quot;Claim Credits&quot; page where you can change your selection.</li>"
    '        ss_text &= "<li>When your &quot;Credit Summary&quot; is final, click ""Print Certificate for Entire Conference"" or click on the ""Print Certificate"" tab to print your certificate for the entire meeting.</li>"
    '        ss_text &= "</ul>"
    '        Me.ltl_session_summary.Text = ss_text
    '    End If

    '    idata = Nothing
    'End Sub

    'Private Sub GetOverviewForUnpurchased(ByVal l As Literal)
    '    Dim CM As ICourseManager
    '    Try
    '        CM = CreateCourseManager()

    '        Dim ds As New DataSet
    '        ds = CM.GetOverviewContentForCourse(ProductID, UserID)

    '        Dim Result As String = ""
    '        Dim RowCount As Integer = ds.Tables(0).Rows.Count
    '        Dim RowCounter As Integer = 0

    '        If RowCount > 0 Then
    '            For RowCounter = 0 To RowCount - 1
    '                'Dim SectionTitle As String = ds.Tables(0).Rows(RowCounter).Item("SectionTitle")
    '                Dim SectionContent As String = ds.Tables(0).Rows(RowCounter).Item("SectionContent")
    '                'Result &= "<div class='subheader'>" & SectionTitle & "</div><div class='cmecontent'>" & SectionContent & "</div>"
    '                Result &= "<div class='cmecontent'>" & SectionContent & "</div>"
    '            Next
    '            ' Torsten 2-9-2008: removed Over header since it shows Overview twice
    '            'l.Text = "<div class='header'>Overview</div>" & Result
    '            'substitutions
    '            Result = Utility.InjectPlaceholderText(Result, "[SpeakerBioLinks]", ProductID)

    '            l.Text = Result
    '        Else
    '            l.Text = "No Overview content available."
    '        End If

    '    Finally
    '        CM = Nothing
    '    End Try

    'End Sub

    Private Sub GetOverview(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager
        Try
            CM = CreateCourseManager()

            Dim ds As New DataSet
            ds = CM.GetOverviewContentForCourse(ProductID, UserID)

            Dim Result As String = preHTML
            Dim RowCount As Integer = ds.Tables(0).Rows.Count
            Dim RowCounter As Integer = 0

            If RowCount > 0 Then
                For RowCounter = 0 To RowCount - 1
                    Dim SectionTitle As String = ds.Tables(0).Rows(RowCounter).Item("SectionTitle")
                    Dim SectionContent As String = ds.Tables(0).Rows(RowCounter).Item("SectionContent")
                    Result &= "<h5>" & SectionTitle & "</h5>"
                    Result &= SectionContent
                    Dim moveUp_url As String = ""
                    Dim moveDown_url As String = ""
                    If RowCounter > 0 Then
                        moveUp_url = OverviewMoveUp(SectionTitle)
                        moveUp_url = "<div class=""moveuplink"">&nbsp;&nbsp;<button onclick=""" & moveUp_url & """ class=""submit"">Up</button></div>"
                    End If

                    If RowCounter < RowCount - 1 Then
                        moveDown_url = OverviewMoveDown(SectionTitle)
                        moveDown_url = "<div class=""movedownlink"">&nbsp;&nbsp;<button onclick=""" & moveDown_url & """ class=""submit"">Down</button></div>"
                    End If
                    Result = Result & moveUp_url & moveDown_url
                Next
                ' Torsten 2-9-2008: removed Over header since it shows Overview twice
                'l.Text = "<div class='header'>Overview</div>" & Result
                Result = Utility.InjectPlaceholderText(Result, "[SpeakerNames]", ProductID)
                Result = Utility.InjectPlaceholderText(Result, "[SpeakerBioLinks]", ProductID)
                Result = Utility.InjectPlaceholderText(Result, "[SpeakerDisclosures]", ProductID)
                l.Text = Result & postHTML
            Else
                l.Text = Result & "<div class='cmecontent'><h4>No Overview content available.</h4></div>" & postHTML
            End If

        Finally
            CM = Nothing
        End Try

    End Sub
    Private Sub GetCMEInfo(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager
        Try
            CM = CreateCourseManager()

            Dim ds As New DataSet
            ds = CM.GetCMEContentForCourse(ProductID, UserID)

            Dim Result As String = preHTML
            Dim RowCount As Integer = ds.Tables(0).Rows.Count
            Dim RowCounter As Integer = 0

            If RowCount > 0 Then
                For RowCounter = 0 To RowCount - 1
                    'Dim SectionTitle As String = ds.Tables(0).Rows(RowCounter).Item("SectionTitle")
                    Dim SectionContent As String = ds.Tables(0).Rows(RowCounter).Item("SectionContent")
                    'Result &= "<div class='subheader'>" & SectionTitle & "</div><div class='cmecontent'>" & SectionContent & "</div>"
                    Result &= "<div class='cmecontent'>" & SectionContent & "</div>"
                Next
                ' Torsten 2-9-2008: removed Over header since it shows CME Information twice
                'l.Text = "<div class='header'>CME Information</div>" & Result
                Result = Utility.InjectPlaceholderText(Result, "[SpeakerNames]", ProductID)
                Result = Utility.InjectPlaceholderText(Result, "[SpeakerBioLinks]", ProductID)
                Result = Utility.InjectPlaceholderText(Result, "[SpeakerDisclosures]", ProductID)
                l.Text = Result & postHTML
            Else
                l.Text = Result & "<div class='cmecontent'><h4>No information available.</h4></div>" & postHTML
            End If

        Finally
            CM = Nothing
        End Try
    End Sub

    ' Torsten 2-9-2008: need to deal with Teleconferences separately as they display instructions for the conference
    Private Sub GetTeleconferenceContent(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager

        Try
            CM = CreateCourseManager()

            Dim ds As New DataSet
            ds = CM.GetTeleconferenceContent(ProductID)

            Dim Result As String = preHTML
            Dim RowCount As Integer = ds.Tables(0).Rows.Count
            Dim RowCounter As Integer = 0

            If RowCount > 0 Then

                Dim sDate As String
                Dim sTime As String
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("ReleaseDate")) Then
                    sDate = DateTime.Parse(ds.Tables(0).Rows(0).Item("ReleaseDate")).ToString("M/d/yyyy")
                    sTime = DateTime.Parse(ds.Tables(0).Rows(0).Item("ReleaseDate")).ToString("h:mm tt")
                Else
                    sDate = "N/A"
                    sTime = "N/A"
                End If

                Dim sDuration As String
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("DurationInSeconds")) Then
                    sDuration = GetTimeStringFromSeconds(ds.Tables(0).Rows(0).Item("DurationInSeconds"), True)
                Else
                    sDuration = "N/A"
                End If

                Dim sHeaderImage As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("HeaderImage"))
                Dim sDescriptionHTML As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("DescriptionHTML"))
                Dim sPhoneTollFreeUS As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("PhoneTollFreeUS"))
                Dim sPhoneTollFreeCanada As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("PhoneTollFreeCanada"))
                Dim sPhoneToll As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("PhoneToll"))
                Dim sHelpTollFree As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("HelpTollFree"))
                Dim sHelpToll As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("HelpToll"))
                Dim sPasscode As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("PassCode"))
                Dim sURL As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("URL"))
                Dim sURLText As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("URLText"))
                Dim sAttachmentHTML As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("AttachmentHTML"))
                Dim sRegKey As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("RegKey"))
                Dim sRegURL As String = Utility.SafeString(ds.Tables(0).Rows(0).Item("RegURL"))
                Dim add_url As String
                add_url = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&PageCode=WebinarContent&ContentID="
                add_url = "&nbsp;&nbsp;<button onclick=""window.open('" & add_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add New Content</button>"
                Result &= add_url
                If sHeaderImage <> "" Then
                    Result &= "<div class='subheader'><img src=""images/" & sHeaderImage & """ class=""headerimage"" " & "</div>"
                End If

                Result &= "<div class='teleconference_content'>" & sDescriptionHTML & "</div>"
                If sURL <> "" Then
                    Result &= "<br/><p><a href=""" & sURL & """ class='purchaselink' target='teleconference'>View Webinar</a></p>"
                End If

                Result &= "<ul class='lobj'>"
                Result &= "<li>" & "Date: " & sDate & "</li>"
                Result &= "<li>" & "Time: " & sTime & " Eastern Time</li>"
                Result &= "<li>" & "Duration: " & sDuration & "</li><ul><br/>"
                If sPhoneTollFreeUS <> "" Then
                    Result &= "<h5>" & "To hear the audio for this program, dial:" & "</h5><p>" & sPhoneTollFreeUS & " <strong>(Toll Free)</strong><br/>Access Code: " & sPasscode & "</p><p>" & sPhoneToll & " <strong>(Alternative dial-in number; long distance charges may apply)</strong><br/>Access Code: " & sPasscode & "</p>"
                End If
                If sHelpToll <> "" Then
                    Result &= "<p>" & "For Help during the webinar call:" & "<br/>" & sHelpToll & " </p>"
                End If


                Result &= ""

                Result &= "<div class='teleconference_content'>" & sAttachmentHTML & "</div>"

                Result &= GetTeleconferenceActivityContent(preHTML, postHTML)


                l.Text = Result
            Else

                l.Text = Result & "<div class='teleconference_content'><p>No Teleconference information available.</p></div>" & postHTML
            End If

        Finally
            CM = Nothing
        End Try

    End Sub


    Private Function GetTeleconferenceActivityContent(ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager
        Dim Result As String = ""
        Try
            CM = CreateCourseManager()

            '''''''''Added for add content 
            Dim dsContent As New DataSet
            dsContent = CM.GetTeleconferenceAddedContent(ProductID)
            Dim RowCountContent As Integer = dsContent.Tables(0).Rows.Count
            Dim RowCounterContent As Integer = 0

            Dim displayTestLink As Boolean
            Dim PresentationComplete As Boolean
            Dim SpeakerName As String = ""
            Dim hasTest As Boolean = False

            If RowCountContent > 0 Then


                Dim dv As DataView = dsContent.Tables(0).DefaultView
                dv.Sort = "ContentOrder ASC"

                For RowCounterContent = 0 To RowCountContent - 1

                    Dim contentID As Integer = dv(RowCounterContent).Item("ID")

                    Dim edit_url As String = String.Empty
                    Dim delete_url As String = String.Empty
                    Dim moveUp_url As String = String.Empty
                    Dim moveDown_url As String = String.Empty

                    edit_url = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID=" & contentID
                    edit_url = "<div class=""editlink"">&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Edit</button></div>"

                    delete_url = getDeleteUrl(contentID, "Delete Content?")
                    delete_url = "<div class=""deletelink"">&nbsp;&nbsp;<button onclick=""" & delete_url & """ class=""submit"">Delete</button></div>"

                    If RowCounterContent > 0 Then
                        moveUp_url = getMoveUp(contentID)
                        moveUp_url = "<div class=""moveuplink"">&nbsp;&nbsp;<button onclick=""" & moveUp_url & """ class=""submit"">Up</button></div>"
                    End If

                    If RowCounterContent < RowCountContent - 1 Then
                        moveDown_url = getMoveDown(contentID)
                        moveDown_url = "<div class=""movedownlink"">&nbsp;&nbsp;<button onclick=""" & moveDown_url & """ class=""submit"">Down</button></div>"
                    End If


                    Result = Result & "<div class=""presitem"">"
                    Result = Result & "<div class=""preslink"">"
                    Result = Result & edit_url & delete_url & moveUp_url & moveDown_url
                    displayTestLink = False
                    PresentationComplete = False



                    Dim speakerRoles As List(Of NetScore.Entities.ContentRole) = NetScore.BAL.CourseManager.GetContentRolesWithSpeakers(contentID)
                    Dim speakerDiv = String.Empty
                    For Each contentRole As NetScore.Entities.ContentRole In speakerRoles
                        speakerDiv &= "<p class=""spk"">" & contentRole.RoleName & ":"
                        speakerDiv &= "<ul class=""spklist"">"
                        Dim count As Integer = 0
                        For Each speaker As NetScore.Entities.Speaker In contentRole.Speakers
                            count = count + 1
                            speakerDiv &= "<li><a href=""speaker.aspx?SpeakerID=" & speaker.SpeakerID & """  class=""popupwindow"" rel=""windowBio"">" & speaker.FullName & "</a>" & IIf(contentRole.Speakers.Count > count, "; ", String.Empty) & "</li>"
                        Next
                        speakerDiv &= "</p>"
                    Next
                    speakerDiv &= "</ul>"



                    Dim webnar_icon As String = ""

                    If customActivityType.FunctionalActivityType.ActivityCode = "WEBINAR" Then

                        webnar_icon = "id=""webinarlink"""
                    End If
                    Dim ContentURL As String = Utility.SafeString(dv(RowCounterContent).Item("ContentURL"))
                    Result = Result & "<p>"
                    If (dv(RowCounterContent).Item("ContentTitle") & "").ToString.EndsWith("$SCORM") Then
                        If (Utility.SafeString(dv(RowCounterContent).Item("ContentPlayerREL")) = "windowVimeoPlayer") Then
                            Dim sAbsoluteURL = ConfigurationManager.AppSettings("AbsoluteURL")
                            Dim sPlayerURL As String = sAbsoluteURL.Replace("https", "http") & "/admin/vimeoplayer.aspx?playerurl=" & ContentURL
                            Result = Result & "<a " & webnar_icon & " href=""" & sPlayerURL & """   class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounterContent).Item("ContentPlayerREL")) & """ >" & dv(RowCounterContent).Item("ContentTitle") & "</a></p>"
                        Else
                            Result = Result & dv(RowCounterContent).Item("ContentTitle") & "</p>"
                        End If
                    Else

                        If CurrentPageCode = "MAC" Then
                            Result = Result & "<a " & webnar_icon & " href=""" & ContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounterContent).Item("ContentPlayerREL")) & """ >" & dv(RowCounterContent).Item("ContentTitle") & "</a></p>"
                        ElseIf (Utility.SafeString(dv(RowCounterContent).Item("ContentPlayerREL")) = "windowVimeoPlayer") Then
                            Dim sAbsoluteURL = ConfigurationManager.AppSettings("AbsoluteURL")
                            Dim sPlayerURL As String = sAbsoluteURL.Replace("https", "http") & "/admin/vimeoplayer.aspx?playerurl=" & ContentURL
                            Result = Result & "<a " & webnar_icon & " href=""" & sPlayerURL & """   class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounterContent).Item("ContentPlayerREL")) & """ >" & dv(RowCounterContent).Item("ContentTitle") & "</a></p>"
                        ElseIf (Utility.SafeString(dv(RowCounterContent).Item("ContentPlayerREL")) = "windowArticulatePlayer") Then
                            Result = Result & "<a " & webnar_icon & " href=""" & ContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounterContent).Item("ContentPlayerREL")) & """ >" & dv(RowCounterContent).Item("ContentTitle") & "</a></p>"
                        Else
                            If ContentURL.IndexOf("?") > 0 Then
                                ContentURL = ContentURL & "&ProductId=" & ProductID & "&IsAdmin=Y"""
                            Else
                                ContentURL = ContentURL & "?ProductId=" & ProductID & "&IsAdmin=Y"""
                            End If
                            ContentURL &= "&adminID=" & Session("adminUserID")
                            Result = Result & "<a " & webnar_icon & " href=""" & ContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounterContent).Item("ContentPlayerREL")) & """ >" & dv(RowCounterContent).Item("ContentTitle") & "</a></p>"
                        End If
                    End If

                    If Utility.SafeString(dv(RowCounterContent).Item("ContentURL2")) <> String.Empty Then
                        Dim sContent2String As String
                        ' set up for handling different cases based on REL content - for now just PDF
                        If Utility.SafeString(dv(RowCounterContent).Item("ContentPlayer2REL")) = "windowPDFViewer" Then
                            sContent2String = "PDF version"
                        End If
                        ' <p><a href="#" id="pdflink">Pdf link</a></p>
                        Result = Result & "<p><a id=""pdflink"" href=""../Users/" & Utility.SafeString(dv(RowCounterContent).Item("ContentURL2")) & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounterContent).Item("ContentPlayer2REL")) & """ >" & sContent2String & "</a></p>"
                    End If


                    Result &= speakerDiv

                    Result = Result & "</div> <!-- end preslink -->"


                    Result = Result & "</div> <!-- end pressitem -->"
                Next
                ' End Torsten 11-21-2009

            End If

            Dim sPopUpBlockerWarning As String = ""
            Result = sPopUpBlockerWarning & Result & postHTML
            ''''''''''''''''''''''''''''''''''''''    
            Return Result
        Finally
            CM = Nothing
        End Try
    End Function

    Private Sub GetPresentationContent(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager
        Try
            CM = CreateCourseManager()

            Dim ds As New DataSet
            ds = CM.GetCourseContent(ProductID, UserID)

            Dim Result As String = String.Empty

            Dim RowCount As Integer = ds.Tables(0).Rows.Count
            Dim RowCounter As Integer = 0
            Dim displayTestLink As Boolean
            Dim PresentationComplete As Boolean
            Dim SpeakerName As String = ""

            Dim hasTest As Boolean = False ' ASM currently does not have tests but will in the future - we may want to put those on a separate tab.

            If RowCount > 0 Then

                ' Torsten 11-21-2009: apply ContentOrder
                Dim dv As DataView = ds.Tables(0).DefaultView
                dv.Sort = "ContentOrder ASC"
                Dim ActivityContentDescription As String = ""
                If Not IsDBNull(ds.Tables(0).Rows(0)("ActivityContentDescription")) Then
                    ActivityContentDescription = ds.Tables(0).Rows(0)("ActivityContentDescription")
                End If

                Dim sPopUpBlockerWarning As String = ""

                Dim contentEditURL As String = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID="
                Result = Result & "<div class=""content-desc"">" & ActivityContentDescription & "</div>" & sPopUpBlockerWarning & " <button onclick=""window.open('" & contentEditURL & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add New Content</button>"


                For RowCounter = 0 To RowCount - 1

                    Dim contentID As Integer = dv(RowCounter).Item("ID")

                    Dim contentTitle As String = (If(TypeOf dv(RowCounter)("ContentTitle") Is DBNull, String.Empty, Convert.ToString(dv(RowCounter)("ContentTitle"))))
                    Dim contentURL As String = (If(TypeOf dv(RowCounter)("ContentURL") Is DBNull, String.Empty, Convert.ToString(dv(RowCounter)("ContentURL"))))

                    Dim edit_url As String = String.Empty
                    Dim delete_url As String = String.Empty
                    Dim moveUp_url As String = String.Empty
                    Dim moveDown_url As String = String.Empty

                    edit_url = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID=" & contentID
                    edit_url = "<div class=""editlink"">&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Edit</button></div>"

                    delete_url = getDeleteUrl(contentID, "Delete Content?")
                    delete_url = "<div class=""deletelink"">&nbsp;&nbsp;<button onclick=""" & delete_url & """ class=""submit"">Delete</button></div>"

                    If RowCounter > 0 Then
                        moveUp_url = getMoveUp(contentID)
                        moveUp_url = "<div class=""moveuplink"">&nbsp;&nbsp;<button onclick=""" & moveUp_url & """ class=""submit"">Up</button></div>"
                    End If

                    If RowCounter < RowCount - 1 Then
                        moveDown_url = getMoveDown(contentID)
                        moveDown_url = "<div class=""movedownlink"">&nbsp;&nbsp;<button onclick=""" & moveDown_url & """ class=""submit"">Down</button></div>"
                    End If


                    Result = Result & "<div class=""presitem"">"
                    Result = Result & "<div class=""preslink"">"
                    Result = Result & edit_url & delete_url & moveUp_url & moveDown_url
                    displayTestLink = False
                    PresentationComplete = False

                    If (String.IsNullOrEmpty(contentTitle)) OrElse (String.IsNullOrEmpty(contentURL)) Then
                        Master.ErrorLabel += "Content Title / Content URL is 'empty'. Please verify the item number - '" & Convert.ToString(RowCounter + 1) & "'." & vbCr & vbLf
                        Continue For
                    End If

                    Dim speakerRoles As List(Of NetScore.Entities.ContentRole) = NetScore.BAL.CourseManager.GetContentRolesWithSpeakers(contentID)
                    Dim speakerDiv = String.Empty
                    For Each contentRole As NetScore.Entities.ContentRole In speakerRoles
                        speakerDiv &= "<p class=""spk"">" & contentRole.RoleName & ":"
                        speakerDiv &= "<ul class=""spklist"">"
                        Dim count As Integer = 0
                        For Each speaker As NetScore.Entities.Speaker In contentRole.Speakers
                            count = count + 1
                            'speakerDiv &= "<li><a href=""speaker.aspx?SpeakerID=" & speaker.SpeakerID & """  class=""popupwindow"" rel=""windowBio"">" & speaker.FullName & "</a>" & IIf(contentRole.Speakers.Count > count, "; ", String.Empty) & "</li>"
                            speakerDiv &= "<li>" & speaker.FullName & IIf(contentRole.Speakers.Count > count, "; ", String.Empty) & "</li>"
                        Next
                        speakerDiv &= "</p>"
                    Next
                    speakerDiv &= "</ul>"



                    Dim webnar_icon As String = ""
                    'Dim customActivityType As NetScore.Entities.CustomActivityType = GetCustomActivityTypeByProductID(ProductID)
                    If customActivityType.FunctionalActivityType.ActivityCode = "WEBINAR" Then
                        ' TODO: can we paramterize this id somehow?
                        webnar_icon = "id=""webinarlink"""
                    End If
                    '<p><a href="#">Pres title</a></p>
                    Result = Result & "<p>"
                    'Dim ContentTitle As String = dv(RowCounter).Item("ContentTitle")
                    If Utility.SafeString(dv(RowCounter).Item("ContentMetaData")) <> String.Empty Then
                        contentTitle &= " (" & dv(RowCounter).Item("ContentMetaData") & ")"
                    End If
                    If (contentTitle & "").ToString.EndsWith("$SCORM") Then

                        Session("adminscorm") = 1
                        Dim ch As DotNetSCORM.LearningAPI.ControlHelper = New DotNetSCORM.LearningAPI.ControlHelper()
                        Dim job As LearningStoreJob = ch.LStore.CreateJob()
                        ch.RequestCurrentUserInfo(job)
                        ch.RequestMyTrainingDUP(job, Nothing)
                        Dim results As ReadOnlyCollection(Of Object) = job.Execute()

                        Dim dt As DataTable = results(1)

                        Dim api_result As String = ch.GetMyTrainingResultsDUP(dt, dv(RowCounter).Item("ContentURL") & "", ProductID)
                        'Dim StgArray As string[]
                        Dim spoints As String = ""
                        Dim attemptstatustring As String = ""

                        Try
                            Dim StgArray() As String = Split(api_result, "^^^^")

                            'spoints = Split(api_result, "^^^^")(1)
                            spoints = StgArray(1)
                            attemptstatustring = StgArray(2)
                        Catch ex As Exception

                        End Try

                        If spoints <> "" Then
                            spoints &= "% Completed"
                            spoints = "(" & spoints & ")"
                        End If

                        'If (attemptstatustring.ToLower() = "completed") Then
                        '    'If the package is completed and the user is still in the content state,
                        '    'then save the content as viewed so the step can be complete and the user is eligible for the next step
                        '    'of the activity. 
                        '    'The session variable is set and checked so the code runs only once and is not run everytime the page refreshes
                        '    If CurrentPageCode = currentUserPageCode And Session("SavedContentFor_" & ProductID & "_" & contentID) <> True Then
                        '        SaveUserContentStatus(UserID, ProductID, contentID)
                        '        Session("SavedContentFor_" & ProductID & "_" & contentID) = True
                        '        Response.Redirect("ViewActivityContent.aspx?ProductID=" & ProductID & "&Pagecode=TC")
                        '    End If
                        'End If

                        Select Case attemptstatustring.ToLower()
                            Case ""
                                attemptstatustring = "Not Started"
                            Case "active"
                                attemptstatustring = "Incomplete"
                            Case "completed"
                                attemptstatustring = "Completed"
                            Case Else
                                attemptstatustring = "Incomplete"
                        End Select

                        Result = Result & Split(api_result, "^^^^")(0) & contentTitle.ToString.Replace("$SCORM", "") & " (" & attemptstatustring & ")</a></p>"


                    Else
                        'Content Type code - Checking if we have a contenttypeID for this activity so that we can check if it is SCT type.
                        Dim ActivityContentTypeID As String = ""
                        If Not IsDBNull(dv(RowCounter).Item("ContentTypeID")) Then
                            ActivityContentTypeID = dv(RowCounter).Item("ContentTypeID")
                        End If
                        Dim CT As New NetScore.BAL.CEActivitiesObjects.CEActivityContentType(ActivityContentTypeID)
                        'Content type code ends here

                        'Dim ContentURL As String = Utility.SafeString(dv(RowCounter).Item("ContentURL"))
                        If dv(RowCounter).Item("ContentPlayerREL") = "windowForIBHREContent" Or CT.ContentType.ToLower() = "ibhre" Then
                            contentURL = "/Users/LinkToIBHREContent.aspx?ContentLink=" & Server.UrlEncode(contentURL)
                        End If
                        Dim AdminContentURL = contentURL
                        If contentURL.IndexOf("?") > 0 Then
                            AdminContentURL = AdminContentURL & "&ProductId=" & ProductID & "&IsAdmin=Y"
                        Else
                            AdminContentURL = AdminContentURL & "?ProductId=" & ProductID & "&IsAdmin=Y"
                        End If
                        AdminContentURL &= "&adminID=" & Session("adminUserID")
                        If CurrentPageCode = "MAC" Then
                            Result = Result & "<a " & webnar_icon & " href=""" & contentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & contentTitle & "</a></p>"

                        ElseIf (Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) = "windowVimeoPlayer") Then
                            '  Dim sAbsoluteURL = ConfigurationManager.AppSettings("AbsoluteURL")
                            Dim sPlayerURL As String = "/admin/vimeoplayer.aspx?playerurl=" & AdminContentURL

                            Result = Result & "<a " & webnar_icon & " href=""" & sPlayerURL & """   class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & contentTitle & "</a></p>"


                        ElseIf (customActivityType.CustomActivityTypeCode.ToUpper() = "CSAT") Then
                            'Dim csaturl As String

                            'Dim ClientProductid As String = ceactivity_obj.MappingProductID
                            'ClientProductid = String.Join(Nothing, (System.Text.RegularExpressions.Regex.Split(ClientProductid, "[^\d]")))
                            'csaturl = GetCsatURL(ClientProductid)
                            'Result = Result & "<a onClick=""" & "SaveUserContentStatus('" & ProductID & "','" & contentID & "');"" " & webnar_icon & " href=""" & csaturl & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & dv(RowCounter).Item("ContentTitle") & "</a>"
                        ElseIf (Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) = "windowArticulatePlayer") Then
                            AdminContentURL = contentURL
                            Result = Result & "<a " & webnar_icon & " href=""" & AdminContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & contentTitle & "</a></p>"
                        Else
                            Result = Result & "<a " & webnar_icon & " href=""" & AdminContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & contentTitle & "</a></p>"
                        End If
                    End If

                    If Utility.SafeString(dv(RowCounter).Item("ContentURL2")) <> String.Empty Then
                        Dim sContent2String As String
                        ' set up for handling different cases based on REL content - for now just PDF
                        If Utility.SafeString(dv(RowCounter).Item("ContentPlayer2REL")) = "windowPDFViewer" Then
                            sContent2String = "PDF version"
                        End If
                        ' <p><a href="#" id="pdflink">Pdf link</a></p>
                        Result = Result & "<p><a id=""pdflink"" href=""../Users/" & Utility.SafeString(dv(RowCounter).Item("ContentURL2")) & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayer2REL")) & """ >" & sContent2String & "</a></p>"
                    End If

                    ''If Not IsDBNull(dv(RowCounter).Item("SpeakerID")) Then
                    ''    Result = Result & "<p class=""spk"">Speaker(s):</p>"
                    ''    Result = Result & "<ul class=""spklist"">"
                    ''    Result = Result & "<li><a href=""speaker.aspx?SpeakerID=" & dv(RowCounter).Item("SpeakerID") & """  class=""popupwindow"" rel=""windowBio"">" & SpeakerName & "</a></li>"


                    ''    Result = Result & "</ul>"
                    ''End If

                    Result &= speakerDiv

                    Result = Result & "</div> <!-- end preslink -->"


                    Result = Result & "</div> <!-- end pressitem -->"
                Next
                ' End Torsten 11-21-2009
            Else
                Dim contentEditURL As String = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID="
                Result = Result & "<div style='height: 20px;'>Please add a content before adding content description.</div> <button onclick=""window.open('" & contentEditURL & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add New Content</button>"

                Result = Result & "<div class=""presitem"">"
                Result = Result & "<div class=""preslink"">"
                Result = Result & "<p>No Presentations available for this activity.</p>"
                Result = Result & "</div <!-- end preslink-->"
                Result = Result & "</div <!-- end presitem-->"
            End If


            l.Text = preHTML & Result & postHTML

        Catch ex As Exception
            Master.ErrorLabel = "Error displaying Presentations. " & ex.Message
        End Try

    End Sub

    Private Sub GetPresentationContentByPageCode(ByVal l As Label, ByVal PageCode As String, ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager
        Try
            CM = CreateCourseManager()

            Dim ds As New DataSet
            ds = CM.GetCourseContent(ProductID, UserID, PageCode)

            Dim Result As String = String.Empty

            Dim RowCount As Integer = ds.Tables(0).Rows.Count
            Dim RowCounter As Integer = 0
            Dim displayTestLink As Boolean
            Dim PresentationComplete As Boolean
            Dim SpeakerName As String = ""

            Dim hasTest As Boolean = False ' ASM currently does not have tests but will in the future - we may want to put those on a separate tab.

            If RowCount > 0 Then

                ' Torsten 11-21-2009: apply ContentOrder
                Dim dv As DataView = ds.Tables(0).DefaultView
                dv.Sort = "ContentOrder ASC"
                Dim ActivityContentDescription As String = ""
                If Not IsDBNull(ds.Tables(0).Rows(0)("ActivityContentDescription")) Then
                    ActivityContentDescription = ds.Tables(0).Rows(0)("ActivityContentDescription")
                End If

                Dim sPopUpBlockerWarning As String = ""

                Dim contentEditURL As String = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&PageCode=RESCLIB&ContentID="
                Result = Result & "<div class=""content-desc"">" & ActivityContentDescription & "</div>" & sPopUpBlockerWarning & " <button onclick=""window.open('" & contentEditURL & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add New Content</button>"


                'Result = Result & sPopUpBlockerWarning

                For RowCounter = 0 To RowCount - 1

                    Dim contentID As Integer = dv(RowCounter).Item("ID")

                    Dim edit_url As String = String.Empty
                    Dim delete_url As String = String.Empty
                    Dim moveUp_url As String = String.Empty
                    Dim moveDown_url As String = String.Empty

                    edit_url = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID=" & contentID
                    edit_url = "<div class=""editlink"">&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Edit</button></div>"

                    delete_url = getDeleteUrl(contentID, "Delete Content?")
                    delete_url = "<div class=""deletelink"">&nbsp;&nbsp;<button onclick=""" & delete_url & """ class=""submit"">Delete</button></div>"

                    If RowCounter > 0 Then
                        moveUp_url = getMoveUp(contentID)
                        moveUp_url = "<div class=""moveuplink"">&nbsp;&nbsp;<button onclick=""" & moveUp_url & """ class=""submit"">Up</button></div>"
                    End If

                    If RowCounter < RowCount - 1 Then
                        moveDown_url = getMoveDown(contentID)
                        moveDown_url = "<div class=""movedownlink"">&nbsp;&nbsp;<button onclick=""" & moveDown_url & """ class=""submit"">Down</button></div>"
                    End If


                    Result = Result & "<div class=""presitem"">"
                    Result = Result & "<div class=""preslink"">"
                    Result = Result & edit_url & delete_url & moveUp_url & moveDown_url
                    displayTestLink = False
                    PresentationComplete = False

                    ''If Not IsDBNull(dv(RowCounter).Item("SpeakerID")) Then
                    ''    SpeakerName = dv(RowCounter).Item("FirstName")
                    ''    If Not IsDBNull(dv(RowCounter).Item("MiddleName")) Then
                    ''        If dv(RowCounter).Item("MiddleName").ToString <> "" Then
                    ''            SpeakerName = SpeakerName & " " & dv(RowCounter).Item("MiddleName")
                    ''        End If
                    ''    End If
                    ''    SpeakerName &= " " & dv(RowCounter).Item("LastName")
                    ''    If Not IsDBNull(dv(RowCounter).Item("Suffix")) Then
                    ''        If dv(RowCounter).Item("Suffix").ToString <> "" Then
                    ''            SpeakerName &= ", " & dv(RowCounter).Item("Suffix")
                    ''        End If
                    ''    End If
                    ''End If

                    Dim speakerRoles As List(Of NetScore.Entities.ContentRole) = NetScore.BAL.CourseManager.GetContentRolesWithSpeakers(contentID)
                    Dim speakerDiv = String.Empty
                    For Each contentRole As NetScore.Entities.ContentRole In speakerRoles
                        speakerDiv &= "<p class=""spk"">" & contentRole.RoleName & ":"
                        speakerDiv &= "<ul class=""spklist"">"
                        Dim count As Integer = 0
                        For Each speaker As NetScore.Entities.Speaker In contentRole.Speakers
                            count = count + 1
                            speakerDiv &= "<li><a href=""speaker.aspx?SpeakerID=" & speaker.SpeakerID & """  class=""popupwindow"" rel=""windowBio"">" & speaker.FullName & "</a>" & IIf(contentRole.Speakers.Count > count, "; ", String.Empty) & "</li>"
                        Next
                        speakerDiv &= "</p>"
                    Next
                    speakerDiv &= "</ul>"



                    Dim webnar_icon As String = ""
                    'Dim customActivityType As NetScore.Entities.CustomActivityType = GetCustomActivityTypeByProductID(ProductID)
                    If customActivityType.FunctionalActivityType.ActivityCode = "WEBINAR" Then
                        ' TODO: can we paramterize this id somehow?
                        webnar_icon = "id=""webinarlink"""
                    End If
                    '<p><a href="#">Pres title</a></p>
                    Result = Result & "<p>"
                    If (dv(RowCounter).Item("ContentTitle") & "").ToString.EndsWith("$SCORM") Then
                        Result = Result & dv(RowCounter).Item("ContentTitle") & "</p>"

                    Else
                        Dim ContentURL As String = Utility.SafeString(dv(RowCounter).Item("ContentURL"))
                        Dim AdminContentURL = ContentURL
                        If ContentURL.IndexOf("?") > 0 Then
                            AdminContentURL = AdminContentURL & "&ProductId=" & ProductID & "&IsAdmin=Y"""
                        Else
                            AdminContentURL = AdminContentURL & "?ProductId=" & ProductID & "&IsAdmin=Y"""
                        End If

                        If CurrentPageCode = "MAC" Then
                            Result = Result & "<a " & webnar_icon & " href=""" & ContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & dv(RowCounter).Item("ContentTitle") & "</a></p>"

                        ElseIf (Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) = "windowVimeoPlayer") Then
                            Dim sAbsoluteURL = ConfigurationManager.AppSettings("AbsoluteURL")
                            Dim sPlayerURL As String = sAbsoluteURL.Replace("https", "http") & "/admin/vimeoplayer.aspx?playerurl=" & AdminContentURL
                            Result = Result & "<a " & webnar_icon & " href=""" & sPlayerURL & """   class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & dv(RowCounter).Item("ContentTitle") & "</a></p>"


                        ElseIf (customActivityType.CustomActivityTypeCode.ToUpper() = "CSAT") Then
                            Dim csaturl As String

                            Dim ClientProductid As String = ceactivity_obj.MappingProductID
                            ClientProductid = String.Join(Nothing, (System.Text.RegularExpressions.Regex.Split(ClientProductid, "[^\d]")))
                            csaturl = GetCsatURL(ClientProductid)
                            Result = Result & "<a onClick=""" & "SaveUserContentStatus('" & ProductID & "','" & contentID & "');"" " & webnar_icon & " href=""" & csaturl & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & dv(RowCounter).Item("ContentTitle") & "</a>"
                        ElseIf (Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) = "windowArticulatePlayer") Then
                            AdminContentURL = ContentURL
                            Result = Result & "<a " & webnar_icon & " href=""" & AdminContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & dv(RowCounter).Item("ContentTitle") & "</a></p>"
                        Else
                            Result = Result & "<a " & webnar_icon & " href=""" & AdminContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & dv(RowCounter).Item("ContentTitle") & "</a></p>"
                        End If
                    End If

                    If Utility.SafeString(dv(RowCounter).Item("ContentURL2")) <> String.Empty Then
                        Dim sContent2String As String
                        ' set up for handling different cases based on REL content - for now just PDF
                        If Utility.SafeString(dv(RowCounter).Item("ContentPlayer2REL")) = "windowPDFViewer" Then
                            sContent2String = "PDF version"
                        End If
                        ' <p><a href="#" id="pdflink">Pdf link</a></p>
                        Result = Result & "<p><a id=""pdflink"" href=""../Users/" & Utility.SafeString(dv(RowCounter).Item("ContentURL2")) & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayer2REL")) & """ >" & sContent2String & "</a></p>"
                    End If

                    ''If Not IsDBNull(dv(RowCounter).Item("SpeakerID")) Then
                    ''    Result = Result & "<p class=""spk"">Speaker(s):</p>"
                    ''    Result = Result & "<ul class=""spklist"">"
                    ''    Result = Result & "<li><a href=""speaker.aspx?SpeakerID=" & dv(RowCounter).Item("SpeakerID") & """  class=""popupwindow"" rel=""windowBio"">" & SpeakerName & "</a></li>"


                    ''    Result = Result & "</ul>"
                    ''End If

                    Result &= speakerDiv

                    Result = Result & "</div> <!-- end preslink -->"


                    Result = Result & "</div> <!-- end pressitem -->"
                Next
                ' End Torsten 11-21-2009
            Else
                Dim contentEditURL As String = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&PageCode=" & PageCode & "&ContentID="
                Result = Result & "<div style='height: 20px;'>Please add a content before adding content description.</div> <button onclick=""window.open('" & contentEditURL & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add New Content</button>"
                Result = Result & "<div class=""presitem"">"
                Result = Result & "<div class=""preslink"">"
                Result = Result & "<p>No Presentations available for this activity.</p>"
                Result = Result & "</div <!-- end preslink-->"
                Result = Result & "</div <!-- end presitem-->"
            End If


            l.Text = preHTML & Result & postHTML

        Catch ex As Exception
            Master.ErrorLabel = "Error displaying Presentations. " & ex.Message
        End Try

    End Sub

    Private Sub GetMeetingContent(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager
        Try
            CM = CreateCourseManager()

            Dim ds As New DataSet
            ds = CM.GetCourseContent(ProductID, UserID)

            Dim Result As String = String.Empty

            Dim RowCount As Integer = ds.Tables(0).Rows.Count
            Dim RowCounter As Integer = 0
            Dim displayTestLink As Boolean
            Dim PresentationComplete As Boolean
            Dim SpeakerName As String = ""

            Dim hasTest As Boolean = False ' ASM currently does not have tests but will in the future - we may want to put those on a separate tab.

            If RowCount > 0 Then

                ' Torsten 11-21-2009: apply ContentOrder
                Dim dv As DataView = ds.Tables(0).DefaultView
                dv.Sort = "ContentOrder ASC"
                Dim ActivityContentDescription As String = ""
                If Not IsDBNull(ds.Tables(0).Rows(0)("ActivityContentDescription")) Then
                    ActivityContentDescription = ds.Tables(0).Rows(0)("ActivityContentDescription")
                End If

                Dim sPopUpBlockerWarning As String = ""

                Dim contentEditURL As String = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID="
                Result = Result & "<div class=""content-desc"">" & ActivityContentDescription & "</div>" & sPopUpBlockerWarning & " <button onclick=""window.open('" & contentEditURL & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add New Content</button>"

                For RowCounter = 0 To RowCount - 1

                    Dim contentID As Integer = dv(RowCounter).Item("ID")

                    Dim edit_url As String = String.Empty
                    Dim delete_url As String = String.Empty
                    Dim moveUp_url As String = String.Empty
                    Dim moveDown_url As String = String.Empty

                    edit_url = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID=" & contentID
                    edit_url = "<div class=""editlink"">&nbsp;&nbsp;<button onclick=""window.open('" & edit_url & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Edit</button></div>"

                    delete_url = getDeleteUrl(contentID, "Delete Content?")
                    delete_url = "<div class=""deletelink"">&nbsp;&nbsp;<button onclick=""" & delete_url & """ class=""submit"">Delete</button></div>"

                    If RowCounter > 0 Then
                        moveUp_url = getMoveUp(contentID)
                        moveUp_url = "<div class=""moveuplink"">&nbsp;&nbsp;<button onclick=""" & moveUp_url & """ class=""submit"">Up</button></div>"
                    End If

                    If RowCounter < RowCount - 1 Then
                        moveDown_url = getMoveDown(contentID)
                        moveDown_url = "<div class=""movedownlink"">&nbsp;&nbsp;<button onclick=""" & moveDown_url & """ class=""submit"">Down</button></div>"
                    End If


                    Result = Result & "<div class=""presitem"">"
                    Result = Result & "<div class=""preslink"">"
                    Result = Result & edit_url & delete_url & moveUp_url & moveDown_url
                    displayTestLink = False
                    PresentationComplete = False

                    ''If Not IsDBNull(dv(RowCounter).Item("SpeakerID")) Then
                    ''    SpeakerName = dv(RowCounter).Item("FirstName")
                    ''    If Not IsDBNull(dv(RowCounter).Item("MiddleName")) Then
                    ''        If dv(RowCounter).Item("MiddleName").ToString <> "" Then
                    ''            SpeakerName = SpeakerName & " " & dv(RowCounter).Item("MiddleName")
                    ''        End If
                    ''    End If
                    ''    SpeakerName &= " " & dv(RowCounter).Item("LastName")
                    ''    If Not IsDBNull(dv(RowCounter).Item("Suffix")) Then
                    ''        If dv(RowCounter).Item("Suffix").ToString <> "" Then
                    ''            SpeakerName &= ", " & dv(RowCounter).Item("Suffix")
                    ''        End If
                    ''    End If
                    ''End If

                    Dim speakerRoles As List(Of NetScore.Entities.ContentRole) = NetScore.BAL.CourseManager.GetContentRolesWithSpeakers(contentID)
                    Dim speakerDiv = String.Empty
                    For Each contentRole As NetScore.Entities.ContentRole In speakerRoles
                        speakerDiv &= "<p class=""spk"">" & contentRole.RoleName & ":"
                        speakerDiv &= "<ul class=""spklist"">"
                        Dim count As Integer = 0
                        For Each speaker As NetScore.Entities.Speaker In contentRole.Speakers
                            count = count + 1
                            speakerDiv &= "<li><a href=""speaker.aspx?SpeakerID=" & speaker.SpeakerID & """  class=""popupwindow"" rel=""windowBio"">" & speaker.FullName & "</a>" & IIf(contentRole.Speakers.Count > count, "; ", String.Empty) & "</li>"
                        Next
                        speakerDiv &= "</p>"
                    Next
                    speakerDiv &= "</ul>"



                    Dim webnar_icon As String = ""
                    'Dim customActivityType As NetScore.Entities.CustomActivityType = GetCustomActivityTypeByProductID(ProductID)
                    If customActivityType.FunctionalActivityType.ActivityCode = "WEBINAR" Then
                        ' TODO: can we paramterize this id somehow?
                        webnar_icon = "id=""webinarlink"""
                    End If
                    '<p><a href="#">Pres title</a></p>
                    Result = Result & "<p>"
                    If (dv(RowCounter).Item("ContentTitle") & "").ToString.EndsWith("$SCORM") Then
                        Result = Result & dv(RowCounter).Item("ContentTitle") & "</p>"

                    Else
                        Dim ContentURL As String = Utility.SafeString(dv(RowCounter).Item("ContentURL"))
                        Dim AdminContentURL = ContentURL
                        If ContentURL.IndexOf("?") > 0 Then
                            AdminContentURL = AdminContentURL & "&ProductId=" & ProductID & "&IsAdmin=Y"""
                        Else
                            AdminContentURL = AdminContentURL & "?ProductId=" & ProductID & "&IsAdmin=Y"""
                        End If

                        If CurrentPageCode = "MAC" Then
                            Result = Result & "<a " & webnar_icon & " href=""" & ContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & dv(RowCounter).Item("ContentTitle") & "</a></p>"
                        Else
                            Result = Result & "<a " & webnar_icon & " href=""" & AdminContentURL & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayerREL")) & """ >" & dv(RowCounter).Item("ContentTitle") & "</a></p>"
                        End If
                    End If

                    If Utility.SafeString(dv(RowCounter).Item("ContentURL2")) <> String.Empty Then
                        Dim sContent2String As String
                        ' set up for handling different cases based on REL content - for now just PDF
                        If Utility.SafeString(dv(RowCounter).Item("ContentPlayer2REL")) = "windowPDFViewer" Then
                            sContent2String = "PDF version"
                        End If
                        ' <p><a href="#" id="pdflink">Pdf link</a></p>
                        Result = Result & "<p><a id=""pdflink"" href=""../Users/" & Utility.SafeString(dv(RowCounter).Item("ContentURL2")) & """ class=""popupwindow"" rel=""" & Utility.SafeString(dv(RowCounter).Item("ContentPlayer2REL")) & """ >" & sContent2String & "</a></p>"
                    End If

                    ''If Not IsDBNull(dv(RowCounter).Item("SpeakerID")) Then
                    ''    Result = Result & "<p class=""spk"">Speaker(s):</p>"
                    ''    Result = Result & "<ul class=""spklist"">"
                    ''    Result = Result & "<li><a href=""speaker.aspx?SpeakerID=" & dv(RowCounter).Item("SpeakerID") & """  class=""popupwindow"" rel=""windowBio"">" & SpeakerName & "</a></li>"


                    ''    Result = Result & "</ul>"
                    ''End If

                    Result &= speakerDiv

                    Result = Result & "</div> <!-- end preslink -->"


                    Result = Result & "</div> <!-- end pressitem -->"
                Next
                ' End Torsten 11-21-2009
            Else
                Dim contentEditURL As String = "CEActivityContent.aspx?preview=Y&ActivityID=" & ProductID & "&ContentID="
                Result = Result & "<div style='height: 20px;'>Please add a content before adding content description.</div> <button onclick=""window.open('" & contentEditURL & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add New Content</button>"
                Result = Result & "<div class=""presitem"">"
                Result = Result & "<div class=""preslink"">"
                Result = Result & "<p>No Presentations available for this activity.</p>"
                Result = Result & "</div <!-- end preslink-->"
                Result = Result & "</div <!-- end presitem-->"
            End If

            l.Text = preHTML & Result & postHTML

        Catch ex As Exception
            Master.ErrorLabel = "Error displaying Presentations. " & ex.Message
        End Try

    End Sub

    Private Sub GetUserSessionInfo(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager
        Try
            CM = CreateCourseManager()

            Dim ds As New DataSet
            ds = CM.GetCourseContent(ProductID, UserID)

            Dim Result As String = preHTML
            Dim RowCount As Integer = ds.Tables(0).Rows.Count
            Dim RowCounter As Integer = 0
            Dim displayTestLink As Boolean
            Dim PresentationComplete As Boolean
            Dim SpeakerName As String = ""

            Result = "<table class='presentations'>"

            If RowCount > 0 Then
                Result = Result & "<tr>"
                Result = Result & "<th>**Presentations</th>"
                Result = Result & "<th>Results</th>"
                Result = Result & "<th>Completed</th>"
                Result = Result & "</tr>"

                For RowCounter = 0 To RowCount - 1
                    displayTestLink = False
                    PresentationComplete = False

                    SpeakerName = ds.Tables(0).Rows(RowCounter).Item("FirstName")
                    If Not IsDBNull(ds.Tables(0).Rows(RowCounter).Item("MiddleName")) Then
                        If ds.Tables(0).Rows(RowCounter).Item("MiddleName").ToString <> "" Then
                            SpeakerName = SpeakerName & " " & ds.Tables(0).Rows(RowCounter).Item("MiddleName")
                        End If
                    End If
                    SpeakerName &= " " & ds.Tables(0).Rows(RowCounter).Item("LastName")
                    If Not IsDBNull(ds.Tables(0).Rows(RowCounter).Item("Suffix")) Then
                        If ds.Tables(0).Rows(RowCounter).Item("Suffix").ToString <> "" Then
                            SpeakerName &= ", " & ds.Tables(0).Rows(RowCounter).Item("Suffix")
                        End If
                    End If


                    Result = Result & "<tr>"
                    Result = Result & "	<td><a target=""_blank"" href=""Presentations.aspx?Activityid=" & ds.Tables(0).Rows(RowCounter).Item("CEActivityID") & "&ContentID=" & ds.Tables(0).Rows(RowCounter).Item("ID") & "&ContentURL=" & ds.Tables(0).Rows(RowCounter).Item("ContentURL") & ds.Tables(0).Rows(RowCounter).Item("ContentURL2") & """ class=""presentationlink"">" & ds.Tables(0).Rows(RowCounter).Item("ContentTitle") & "</a></strong>"

                    Result = Result & "<br><br><i>Speaker: </i>" & SpeakerName
                    If Not IsDBNull(ds.Tables(0).Rows(RowCounter).Item("Institution")) Then
                        If ds.Tables(0).Rows(RowCounter).Item("Institution") <> "" Then
                            Result = Result & "<br><i>Institution: </i>" & ds.Tables(0).Rows(RowCounter).Item("Institution")
                        End If
                    End If

                    Result = Result & "</td>"
                    If ds.Tables(0).Rows(RowCounter).Item("presentationStatus") = 1 Then
                        displayTestLink = True
                    End If

                    Result = Result & "<td>"
                    If displayTestLink Then
                        If Not IsDBNull(ds.Tables(0).Rows(RowCounter).Item("TestEvalID")) Then
                            If ds.Tables(0).Rows(RowCounter).Item("TestEvalID") <> 0 Then
                                Select Case ds.Tables(0).Rows(RowCounter).Item("TestStatus")
                                    Case 0 'did not take the test
                                        Result = Result & "<a href=""activitypreview.aspx?pagecode=PPAA&ProductID=" & ProductID & "&EventID=" & ProductID & "&mode=new&TestID=" & ds.Tables(0).Rows(RowCounter).Item("TestEvalID") & """>Take Test</a></td>"
                                    Case 1 'complete
                                        PresentationComplete = True
                                        Result = Result & "<a href=""activitypreview.aspx?pagecode=PPAA&page=review&ProductID=" & ProductID & "&EventID=" & ProductID & "&mode=review&TestID=" & ds.Tables(0).Rows(RowCounter).Item("TestEvalID") & """>View</a></td>"
                                End Select
                            Else
                                'result = result & "No Test"
                                PresentationComplete = True
                            End If
                        End If
                    Else
                        Result = Result & "&nbsp;"
                    End If
                    Result = Result & "</td>"

                    If PresentationComplete Then
                        Result = Result & "<td class='complete'>&nbsp;</td>"
                    Else
                        Result = Result & "<td>&nbsp;</td>"
                    End If
                    Result = Result & "</tr>"
                Next
            Else
                Result = Result & "<tr><td>No Presentations available for this activity.</td></tr>"
            End If

            Result = Result & "</table>"
            Result = Result & "<div class='smallinstructions'>**Please make sure to disable the pop up blocker to view the presentations.</div>"
            l.Text = Result & postHTML

        Catch ex As Exception
            Master.ErrorLabel = "Error displaying Presentations. " & ex.Message
        End Try

    End Sub

    Private Sub GetUserActivityRequiredInfo(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        Dim Result As String = preHTML
        If Session("Login") <> True Then
            Response.Redirect("login.aspx")
        End If

        If Not customActivityType Is Nothing Then
            Dim _userDetailsByCustomType As List(Of UserDetailsForCustomType) = UserDetailsForCustomType.GetRequiredUserFieldsByCustomType(customActivityType.CustomActivityTypeID)
            If Not _userDetailsByCustomType Is Nothing Then
                For Each _userDetails In _userDetailsByCustomType
                    Result &= "<p>" & _userDetails.UserFieldText & "&nbsp;&nbsp;<input type=""" & _userDetails.UserFieldType & """ value=""" & _userDetails.UserFieldLabel & """ name=""" & _userDetails.UserField & """ disabled /></p>"
                Next
            End If
        Else
            Throw New Exception("Custom type not defined")
        End If
        l.Text = Result & postHTML
    End Sub
    Private Sub GetCertificateLinks(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        Dim CM As ICourseManager
        Dim Result As String = preHTML
        Result &= "<p class='linkcert'><a href=# onclick=""javascript:return false;""><span>Your Certificate</span></a></p>"
        l.Text = Result & postHTML
    End Sub

    Private Sub GetForumContent(ByVal l As Label, ByVal preHTML As String, ByVal postHTML As String)
        If bHasForum Then
            Dim _balCM As New NetScore.BAL.CourseManager()

            Dim parentActivityID As Integer = ProductID

            Dim upper As String = customActivityType.CustomActivityTypeCode.ToUpper()
            If upper = "ONLINE CHILD" Or upper = "LIVE WEBINAR CHILD" Then
                'Check the forum at the parent level
                ceactivity_obj = New CEActivitiesObjects.CEActivity(ProductID)
                If ceactivity_obj.ParentID > 0 Then
                    parentActivityID = ceactivity_obj.ParentID
                End If
            End If

            Dim hasActivityForumGroup As Boolean = ForumManager.HasActivityForumGroup(parentActivityID)
            Dim hasPublishedActivityForumGroup As Boolean = ForumManager.HasPublishedActivityForumGroup(parentActivityID)

            Dim Result As String = preHTML
            If hasActivityForumGroup = True Then
                Result &= "<p></p><p><a href=""/ForumLogin.aspx?ActivityID=" & parentActivityID & """ target=""_blank"">Click here</a> to view Discussion Board</p>"
                If hasPublishedActivityForumGroup = False Then
                    Result &= "<p><strong>Note: The ""Discussion Board"" is not published yet. The users will not be able to view the discussion board until it is published.</strong></p>"
                End If
            Else
                Result &= "<p></p><p><strong>No ""Discussion Board"" available for this activity.</strong></p>"
            End If
            l.Text = Result & postHTML
        Else
            l.Text = preHTML & "<p></p><p><strong>Forum is not enabled for this client.</strong></p>" & postHTML
        End If


    End Sub


    Private Sub GetEvalQuestionnaire(Optional ByVal testTypeID As Integer = 3)

        Dim TM As ITestManager
        Dim xmlDoc As New XmlDocument
        Dim xslDoc As New XslTransform
        Try
            TM = CreateTestManager()

            Dim sTestXML As String

            'Sen. 09/26/08
            ' ''If testTypeID = 3 Then
            ' ''    sTestXML = TM.GetEvalQuestionnaire(ProductID, UserID, Mode)
            ' ''Else
            ' ''    sTestXML = TM.GetEvalQuestionnaire(ProductID, UserID, Mode, testTypeID)
            ' ''End If
            If testTypeID = 3 Then
                sTestXML = TM.GetEvalQuestionnaire(ProductID, UserID, Mode)
            ElseIf testTypeID = 1 Then
                sTestXML = TM.GetPreTestQuestionnaire(ProductID, UserID, Mode)
            ElseIf testTypeID = 2 Then
                sTestXML = TM.GetPostTestQuestionnaire(ProductID, UserID, Mode)
            ElseIf testTypeID = 4 Then
                sTestXML = TM.GetPreSurveyQuestionnaire(ProductID, UserID, Mode)
            ElseIf testTypeID = 5 Then
                sTestXML = TM.GetPostSurveyQuestionnaire(ProductID, UserID, Mode)
            End If


            xmlDoc.LoadXml(sTestXML)

            'xmlDoc.Save("c:\development\asm\" & ProductID & "_Eval.xml")

            Mode = xmlDoc.DocumentElement.GetAttribute("testMode")

            Dim xslfile As String
            If testTypeID = 3 Then
                xslfile = "XSL/netscore_Take_Evaluation.xsl"
            ElseIf testTypeID = 4 Then
                xslfile = "XSL/netscore_Take_Presurvey.xsl"
            ElseIf testTypeID = 5 Then
                xslfile = "XSL/netscore_Take_Postsurvey.xsl"
            End If

            If Mode = "new" Then
                xslDoc.Load(Server.MapPath(xslfile))
                ' Torsten 2-11-2008
                lblNextLink.Visible = False ' user should click submit button and not Next
            Else
                xslDoc.Load(Server.MapPath("XSL/netscore_Review_Evaluation.xsl"))
                ' Torsten 2-11-2008
                lblNextLink.Visible = True ' user does not have a submit button and shoudl click Next
            End If

            Me.lblTestXML.Document = xmlDoc
            Me.lblTestXML.Transform = xslDoc

            Me.l_eidtEvalLink.Text = getEvalEditLink(testTypeID)
            Me.l_eidtEvalLink.Visible = True
        Catch ex As Exception
            Me.l_eidtEvalLink.Text = getEvalEditLink(testTypeID)
            Me.l_eidtEvalLink.Visible = True
            'Master.ErrorLabel = ex.Message()
        Finally
            TM = Nothing
            xmlDoc = Nothing
            xslDoc = Nothing
        End Try

    End Sub

    Private Sub GetPostTest()
        Dim TM As ITestManager
        Dim xmlDoc As New XmlDocument
        Dim xslDoc As New XslTransform
        Try
            TM = CreateTestManager()

            '''Sen. 09/26/08
            '''Dim sTestXML As String = TM.GetEvalQuestionnaire(ProductID, UserID, Mode, 2)
            Dim sTestXML As String = TM.GetPostTestQuestionnaire(ProductID, UserID, Mode)

            xmlDoc.LoadXml(sTestXML)

            'xmlDoc.Save("c:\development\asm\" & ProductID & "_Eval.xml")

            Mode = xmlDoc.DocumentElement.GetAttribute("testMode")

            If Mode = "new" Then
                xslDoc.Load(Server.MapPath("XSL/netscore_Take_test.xsl"))
                ' Torsten 2-11-2008
                lblNextLink.Visible = False ' user should click submit button and not Next
            Else
                xslDoc.Load(Server.MapPath("XSL/netscore_Review_test.xsl"))
                ' Torsten 2-11-2008
                lblNextLink.Visible = True ' user does not have a submit button and shoudl click Next
            End If

            Me.lblTestXML.Document = xmlDoc
            Me.lblTestXML.Transform = xslDoc

            Me.l_eidtEvalLink.Text = getEvalEditLink(2)
            Me.l_eidtEvalLink.Visible = True
        Catch ex As Exception
            Me.l_eidtEvalLink.Text = getEvalEditLink(2)
            Me.l_eidtEvalLink.Visible = True
            'Master.ErrorLabel = ex.Message()
        Finally
            TM = Nothing
            xmlDoc = Nothing
            xslDoc = Nothing
        End Try
    End Sub
    Private Sub GetPostTestForVerify()
        Dim TM As ITestManager
        Dim xmlDoc As New XmlDocument
        Dim xslDoc As New XslTransform
        Try
            TM = CreateTestManager()

            '''Sen. 09/26/08
            '''Dim sTestXML As String = TM.GetEvalQuestionnaire(ProductID, UserID, Mode, 2)
            Dim sTestXML As String = TM.GetPostTestQuestionnaire(ProductID, UserID, Mode)

            xmlDoc.LoadXml(sTestXML)

            'xmlDoc.Save("c:\development\asm\" & ProductID & "_Eval.xml")

            Mode = xmlDoc.DocumentElement.GetAttribute("testMode")

            If Mode = "new" Then
                xslDoc.Load(Server.MapPath("XSL/netscore_Take_test.xsl"))
                ' Torsten 2-11-2008
                lblNextLink.Visible = False ' user should click submit button and not Next
            Else
                xslDoc.Load(Server.MapPath("XSL/netscore_Review_test.xsl"))
                ' Torsten 2-11-2008
                lblNextLink.Visible = True ' user does not have a submit button and shoudl click Next
            End If

            Me.lblTestXML.Document = xmlDoc
            Me.lblTestXML.Transform = xslDoc

            Me.l_eidtEvalLink.Text = getEvalEditLink(15)
            Me.l_eidtEvalLink.Visible = True
        Catch ex As Exception
            Me.l_eidtEvalLink.Text = getEvalEditLink(15)
            Me.l_eidtEvalLink.Visible = True
            'Master.ErrorLabel = ex.Message()
        Finally
            TM = Nothing
            xmlDoc = Nothing
            xslDoc = Nothing
        End Try
    End Sub
    Private Sub GetPretTest()
        Dim TM As ITestManager
        Dim xmlDoc As New XmlDocument
        Dim xslDoc As New XslTransform
        Try
            TM = CreateTestManager()

            '''Sen. 09/26/08
            '''Dim sTestXML As String = TM.GetEvalQuestionnaire(ProductID, UserID, Mode, 1)
            Dim sTestXML As String = TM.GetPreTestQuestionnaire(ProductID, UserID, Mode)

            xmlDoc.LoadXml(sTestXML)

            'xmlDoc.Save("c:\development\asm\" & ProductID & "_Eval.xml")

            Mode = xmlDoc.DocumentElement.GetAttribute("testMode")

            If Mode = "new" Then
                xslDoc.Load(Server.MapPath("XSL/netscore_Take_pretest.xsl"))
                ' Torsten 2-11-2008
                lblNextLink.Visible = False ' user should click submit button and not Next
            Else
                xslDoc.Load(Server.MapPath("XSL/netscore_Review_pretest.xsl"))
                ' Torsten 2-11-2008
                lblNextLink.Visible = True ' user does not have a submit button and shoudl click Next
            End If

            Me.lblTestXML.Document = xmlDoc
            Me.lblTestXML.Transform = xslDoc

            Me.l_eidtEvalLink.Text = getEvalEditLink(1)
            Me.l_eidtEvalLink.Visible = True
        Catch ex As Exception
            Me.l_eidtEvalLink.Text = getEvalEditLink(1)
            Me.l_eidtEvalLink.Visible = True
            'Master.ErrorLabel = ex.Message()
        Finally
            TM = Nothing
            xmlDoc = Nothing
            xslDoc = Nothing
        End Try
    End Sub

    Private Sub SaveEvalQuestionnaire()
        'Dim req As HttpRequest = System.Web.HttpContext.Current.Request

        'Dim coll As New Collection
        'Dim item
        'Dim key As String
        'Dim value As String
        'For Each item In req.Form
        '    key = item
        '    value = req.Form.Get(item)
        '    coll.Add(key & "#" & value, key)
        'Next

        'Dim TM As ITestManager
        'Try
        '    TM = CreateTestManager()

        '    TM.SaveEvalQuestionnaire(ProductID, coll)
        '    Me.CurrentPageCode = "CERT"
        '    Response.Redirect("activitypreview.aspx?pagecode=CERT&ProductID=" & ProductID)
        'Catch ex As Exception
        '    Master.ErrorLabel = ex.Message()
        'Finally
        '    TM = Nothing
        'End Try

    End Sub

    Private Sub GetCourseDetails()
        Dim CM As ICourseManager
        CM = CreateCourseManager()

        Dim ds As New DataSet
        ds = CM.GetCourseDetails(ProductID, CourseType, UserID)

        'Dim customActivityType As NetScore.Entities.CustomActivityType = GetCustomActivityTypeByProductID(ProductID)

        Dim Result As String = ""
        Dim RowCount As Integer = ds.Tables(0).Rows.Count
        Dim RowCounter As Integer = 0

        If RowCount > 0 Then
            For RowCounter = 0 To RowCount - 1
                Dim CourseTitle As String = ds.Tables(0).Rows(RowCounter).Item("Description")
                Dim CourseCredits As String '= ds.Tables(0).Rows(RowCounter).Item("MaxCredits")


                Try
                    CourseCredits = ds.Tables(0).Rows(RowCounter).Item("MaxCredits")
                    Dim dCourseCredits As Double = Convert.ToDouble(CourseCredits)
                    If dCourseCredits >= 10 Then
                        CourseCredits = dCourseCredits.ToString("00.00")
                    Else
                        CourseCredits = dCourseCredits.ToString("0.00")
                    End If
                Catch ex As Exception
                    CourseCredits = "TBD"
                End Try

                Dim StartDate As String = ds.Tables(0).Rows(RowCounter).Item("StartDate")
                Dim EndDate As String = ds.Tables(0).Rows(RowCounter).Item("EndDate")
                Dim Type As String = ds.Tables(0).Rows(RowCounter).Item("Type")

                Dim edit_url As String
                Dim cea As CEActivitiesObjects.CEActivity
                cea = New CEActivitiesObjects.CEActivity(ProductID)
                Dim typedesc As String = cea.CEOfferingTypeDescription
                '                    If typedesc = "liveevent" Then
                '                        edit_url = "AttendanceEdit.aspx?preview=Y&ActivityID=" & ProductID
                '                    ElseIf typeDesc = "WEBINAR" Then
                '                        edit_url = "WebinarEdit.aspx?preview=Y&ActivityID=" & ProductID
                '                    ElseIf typedesc = "symposia" Then
                '                        edit_url = "WebinarEdit.aspx?preview=Y&symposia=Y&ActivityID=" & ProductID
                '                    ElseIf typeDesc = "TELECONFERENCE" Then
                '                        edit_url = "TeleconferenceEdit.aspx?preview=Y&ActivityID=" & ProductID
                '                    End If

                edit_url = "CustomizedEdit.aspx?preview=Y&ActivityID=" & ProductID
                Dim edit_supporterURL As String = "AddActivitySupporters.aspx?ProductID=" & ProductID
                Dim edit_ABIMDetails As String = "AddUpdateABIMInfo.aspx?ProductID=" & ProductID
                Dim view_ActivityLog As String = "ViewAdminActivityLog.aspx?ProductID=" & ProductID
                Me.lblcoursetitle.Text = CourseTitle & "&nbsp;&nbsp;" & "<input type='button' onclick=""window.open('" & edit_url & "','','width=1000, height=800, scrollbars=1, status=1, resizable=1');"" class=""submit"" value='Edit' />"
                Me.lblcoursetitle.Text &= "&nbsp;&nbsp;" & "<input type='button' onclick=""window.open('" & edit_supporterURL & "','','width=1000, height=800, scrollbars=1, status=1, resizable=1');"" class=""submit"" value='Add/Edit Supporters' />"
                Me.lblcoursetitle.Text &= "&nbsp;&nbsp;" & "<input type='button' onclick=""window.open('" & edit_ABIMDetails & "','','width=500, height=400, scrollbars=0, status=1, resizable=0');"" class=""submit"" value='Add/Edit ABIM Info' />"
                Me.lblcoursetitle.Text &= "&nbsp;&nbsp;" & "<input type='button' onclick=""$('#divPars').show(100);$('#ctl00_ContentPlaceHolder1_divLoader').show(1);"" class='submit' value='Add/Edit PARS Info' />"
                Me.lblcoursetitle.Text &= "&nbsp;&nbsp;" & "<input type='button' onclick=""window.open('" & view_ActivityLog & "','','width=800, height=600, scrollbars=1, status=1, resizable=0');"" class=""submit"" value='View Admin Activity Log' />"

                If bShowParentLink = True Then
                    Me.lblcoursetitle.Text &= "&nbsp;&nbsp;&nbsp;<a href=""ActivityPreview.aspx?ProductID=" & ceactivity_obj.ParentID & "&PageCode=AL"" class=""submit"">View/Edit Parent</a>"
                End If
                Me.divproducticon.Attributes("class") = customActivityType.CssClassLarge
                lblPubStat.Text = cea.ActivityStatus.ToString()

                iframePars.Attributes.Add("src", "AddUpdatePARSInfo.aspx?ProductID=" & ProductID)

                ' iconwebinar
                ' iconteleconf
                ' iconliveevent

                'Me.lblcoursetype.Text = customActivityType.CustomActivityTypeLabel


                ''Select Case GetActivityTypeDescription(ProductID)
                ''    Case "liveevent"
                ''        Me.lblcoursetype.Text = "Live Event"
                ''    Case "teleconf"
                ''        Me.lblcoursetype.Text = "Teleconference"
                ''        ' Torsten 2-18-2008 - interim solution for refreshing Teleconference page
                ''        ' set meta refresh tag if event is today
                ''        'If StartDate <> "N/A" Then
                ''        '    If DateTime.Parse(StartDate).Date = DateTime.Now.Date Then
                ''        '        'Master.SetPageRefreshTime(CType(Config.TeleconferencePageRefreshRateInSeconds, Integer))
                ''        '        'Sen. 02/29/2008
                ''        '        Dim cur_phaseid As String = GetPhaseActivityID(ProductID)
                ''        '        Dim cscript As String = "<script language='javascript'>"
                ''        '        cscript &= "window.setTimeout(""recur_callback(cnt,'" & ProductID & "','" & cur_phaseid & "');""," & CStr(CType(Config.TeleconferencePageRefreshRateInSeconds, Integer) * 1000) & ");"
                ''        '        cscript &= "</script>"
                ''        '        If cur_phaseid = "1" OrElse cur_phaseid = "2" Then
                ''        '            If Request.Form("go_recur") <> "N" And IsPurchased(UserID, ProductID) Then
                ''        '                Page.RegisterStartupScript("recur_refresh", cscript)
                ''        '            End If

                ''        '        End If

                ''        '    End If
                ''        'End If
                ''    Case "webinar"
                ''        Me.lblcoursetype.Text = "Webinar"
                ''    Case "symposia"
                ''        Me.lblcoursetype.Text = "Symposia"
                ''    Case "online"
                ''        Me.lblcoursetype.Text = "Online"
                ''End Select


                'Me.credits.Text = CourseCredits
                'Torsten 2-10-2008: per Katelyn's comment print release and expiration dates
                'Me.releasedate.Text = DateTime.Parse(StartDate).ToString("M/d/yyyy")
                ' Me.expirationdate.Text = DateTime.Parse(EndDate).ToString("M/d/yyyy")
                'If customActivityType.FunctionalActivityType.ActivityCode = "TELECONFERENCE" Then
                '    Me.span_exp_date.InnerHtml = "Claim Credit By: <strong>" & DateTime.Parse(EndDate).ToString("M/d/yyyy") & "</strong>"
                'End If
                'Me.coursedate.Text = StartDate & " to " & EndDate
            Next
        End If

        Dim sql As String
        sql = "select t.Displaylabel from ns_certificateTypes t, ns_ceactivity_certificates c where c.certificateTypeid = t.certificateTypeid "
        sql &= " and c.courseid = '" & ProductID & "' order by t.description"
        Dim dm As IDataManager
        dm = New DataManager

        Dim dr As IDataReader
        dr = dm.GetDataReader(sql)
        Dim cerT_str As String = ""
        If Not dr Is Nothing Then
            Do While dr.Read
                cerT_str &= dr(0) & ", "
            Loop

            dr.Close()
        End If
        If Right(cerT_str, 2) = ", " Then
            cerT_str = Left(cerT_str, Len(cerT_str) - 2)
        End If
        Me.lbl_certType.Text = cerT_str & "<br><br><button onclick=""window.open('CEActivityCert.aspx?preview=Y&ActivityID=" & ProductID & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Edit Certificates</button>"

        dm = Nothing
    End Sub

    'Private Function GetPurchaseType() As String
    '    Dim CM As ICourseManager
    '        CM = CreateCourseManager()

    '        Dim PurchaseType As String = ""
    '        PurchaseType = CM.GetCoursePurchaseType(ProductID, UserID)
    '        Return PurchaseType
    'End Function

    Protected Function GetClaimedCredits(ByVal sessioncode As String) As String
        Dim result As String = "0"

        'hard code as live. use NS_CEOfferings_User_Attendance table.
        Dim sql As String
        Dim idata As IDataManager
        idata = New DataManager

        sql = "select ceactivityid from ns_ceactivities where customid = '" & confID & "_" & sessioncode & "'"
        session_ceactivityid = idata.GetSimpleValue(sql)

        sql = "select sum(HrsClaimed) from NS_CEOfferings_User_Attendance where UserID='" & UserID & "' and CEActivityID='" & session_ceactivityid & "' and SessionCode='" & sessioncode & "' group by UserID"

        Dim dr As IDataReader
        dr = idata.GetDataReader(sql)
        If Not dr Is Nothing Then
            If dr.Read Then
                result = dr(0) & ""
            End If
            dr.Close()
        End If
        idata = Nothing

        Return result
    End Function

    Protected Function GetStepCode() As String
        Dim sql As String
        Dim idata As IDataManager
        idata = New DataManager

        sql = "select stepcode from NS_User_State_Tracker where ceactivityid='" & ProductID & "' and userid='" & UserID & "' and id=(select max(id) from NS_User_State_Tracker where userid='" & UserID & "')"
        Dim stepcode As String
        stepcode = idata.GetSimpleValue(sql)

        Return stepcode
    End Function

    Protected Sub sessions_grid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles sessions_grid.ItemDataBound
        If show_total Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                CalcTotal(CType(e.Item.Cells(4).Controls(0), DataBoundLiteralControl).Text)
            ElseIf e.Item.ItemType = ListItemType.Footer Then
                e.Item.Cells(3).Text = "<b>Total Claimed:</b>"
                e.Item.Cells(4).Text = "<span class=""totalclaim"">" & tt.ToString() & "</span>"
            End If
        End If
    End Sub
    Dim tt As Decimal = 0
    Private Sub CalcTotal(ByVal value_str As String)
        tt = tt + Decimal.Parse(value_str)
    End Sub

    Function GetSessionDate(ByVal sdate As Date) As String
        Dim d As String = ""
        d = sdate.ToString("MM/dd/yyyy")
        Return d
    End Function

    Function getRemoveLink(ByVal ssioncode As String) As String
        Dim result As String = ""

        'Dim sql As String
        'Dim idata As IDataManager
        'idata = New DataManager

        'sql = "select ceactivityid from ns_ceactivities where customid = '" & confID & "_" & ssioncode & "'"
        'session_ceactivityid = idata.GetSimpleValue(sql)

        'Dim userstate As String
        'sql = "select UserState from NS_CEOfferings_User_Attendance where userid = '" & UserID & "' and ceactivityid = '" & ProductID & "'"
        'userstate = idata.GetSimpleValue(sql)

        'If show_total AndAlso userstate <> "CERT" Then
        '    result &= "<a href=""#"" class=""remove"" onclick=""if(confirm('Are you sure you want to remove the session credit for session " & ssioncode & "?')){document.getElementById('remove_sessioncode').value='" & ssioncode & "';document.getElementById('session_ceactivityid').value='" & session_ceactivityid & "';document.forms[0].submit();}else{return false;}"">Remove?</a>"
        'End If

        Return result
    End Function

    Public Function getSessionLink(ByVal sessioncode As String, ByVal confID As String, ByVal sessionname As String) As String
        Dim result As String = ""

        Dim sessionstatus As String
        Dim sql As String
        Dim dm As IDataManager = New DataManager
        sql = "select sessionstatus from custom_sessions where conferenceID = '" & confID & "' and sessioncode = '" & sessioncode & "'"
        sessionstatus = dm.GetSimpleValue(sql)

        If False And sessionstatus = "I" Then 'Inactive
            result = sessionname
        Else

            Dim ceactivity_obj As CEActivitiesObjects.CEActivity
            ceactivity_obj = New CEActivitiesObjects.CEActivity(ProductID)

            If ceactivity_obj.IsMainEntryForSymposia Then
                result &= "<a href=# onclick=""window.open('symposia_module_forpreview.aspx?productid=" & ProductID & "&conferenceid=" & confID & "&sessioncode=" & sessioncode & "','','width=1000, height=1000, scrollbars=1, status=1, resizable=1');return false;"">"
                result &= sessionname
                result &= "</a>"
            Else
                'result &= "<a href=# onclick=""window.open('credits_select_forpreview.aspx?productid=" & ProductID & "&conferenceid=" & confID & "&sessioncode=" & sessioncode & "','','width=1000, height=1000, scrollbars=1, status=1, resizable=1');return false;"">"
                result &= sessionname
            End If


            result &= getRemoveLink(sessioncode)
        End If

        Return result
    End Function

    Public Function GetActivityTypesCredits(ByVal sessioncode As String, ByVal confID As String) As String
        Dim result As String = ""

        Dim sessionactivityid As String
        Dim sql As String
        Dim dm As IDataManager = New DataManager
        sql = "select ceactivityid from ns_ceactivities where CustomID = '" & Replace(confID & "_" & sessioncode, "'", "''") & "'"
        sessionactivityid = dm.GetSimpleValue(sql)

        Dim session_activity As CEActivitiesObjects.CEActivity = New CEActivitiesObjects.CEActivity(sessionactivityid)
        Dim ht As Hashtable = session_activity.getCertificateTypeIDCredits

        For Each de As DictionaryEntry In ht
            Select Case de.Key
                Case "1"
                    result &= "<img src=""/users/images/legend_cme.gif"">: " & de.Value & Chr(1)
                Case "2"
                    result &= "<img src=""/users/images/legend_part.gif"">: " & de.Value & Chr(1)
                Case "3"
                    result &= "<img src=""/users/images/legend_ca.gif"">: " & de.Value & Chr(1)
                Case "4"
                    result &= "<img src=""/users/images/legend_fl.gif"">: " & de.Value & Chr(1)
                Case "5"
                    result &= "<img src=""/users/images/legend_acpe.gif"">: " & de.Value & Chr(1) & session_activity.getParameter("ACPENumber") & Chr(1)
                Case "6"
                    result &= "<img src=""/users/images/legend_sam.gif"">: " & de.Value & Chr(1)
                Case "8"
                    result &= "<img src=""/users/images/legend_exam.gif"">: " & de.Value & Chr(1)
                Case "9"
                    result &= "<img src=""/users/images/legend_ceu.gif"">: " & de.Value & Chr(1)
                Case "10"
                    result &= "<img src=""/users/images/legend_ce.gif"">: " & de.Value & Chr(1)
            End Select
        Next

        If result.EndsWith(Chr(1)) Then
            result = Left(result, result.Length - 1)
        End If

        result = Replace(result, Chr(1), "<br>")

        Return result
    End Function

    Public Function GetActivityTypesCreditsByCEActivityID(ByVal activityid As String) As String
        Dim result As String = ""

        Dim sessionactivityid As String
        Dim sql As String
        Dim dm As IDataManager = New DataManager
        sql = "select ceactivityid from ns_ceactivities where ceactivityid = '" & activityid & "'"
        sessionactivityid = dm.GetSimpleValue(sql)

        Dim session_activity As CEActivitiesObjects.CEActivity = New CEActivitiesObjects.CEActivity(sessionactivityid)
        Dim ht As Hashtable = session_activity.getCertificateTypeIDCredits

        For Each de As DictionaryEntry In ht
            Select Case de.Key
                Case "1"
                    result &= "<img src=""/users/images/legend_cme.gif"">: " & de.Value & Chr(1)
                Case "2"
                    result &= "<img src=""/users/images/legend_part.gif"">: " & de.Value & Chr(1)
                Case "3"
                    result &= "<img src=""/users/images/legend_ca.gif"">: " & de.Value & Chr(1)
                Case "4"
                    result &= "<img src=""/users/images/legend_fl.gif"">: " & de.Value & Chr(1)
                Case "5"
                    result &= "<img src=""/users/images/legend_acpe.gif"">: " & de.Value & Chr(1) & session_activity.getParameter("ACPENumber") & Chr(1)
                Case "6"
                    result &= "<img src=""/users/images/legend_sam.gif"">: " & de.Value & Chr(1)
                Case "8"
                    result &= "<img src=""/users/images/legend_exam.gif"">: " & de.Value & Chr(1)
                Case "9"
                    result &= "<img src=""/users/images/legend_ceu.gif"">: " & de.Value & Chr(1)
                Case "10"
                    result &= "<img src=""/users/images/legend_ce.gif"">: " & de.Value & Chr(1)
            End Select
        Next

        If result.EndsWith(Chr(1)) Then
            result = Left(result, result.Length - 1)
        End If

        result = Replace(result, Chr(1), "<br>")

        Return result
    End Function

    Public Function GetSessionStatus(ByVal s_status As String) As String
        Dim result As String = ""

        If s_status = "I" Then
            result = "Inactive"
        Else
            result = "Active"
        End If

        Return result
    End Function

    Public Function getEvalEditLink(Optional ByVal typeid As Integer = 3) As String
        Dim result As String
        Dim TestEvalID As String

        Dim ac As CEActivitiesObjects.CEActivity
        ac = New CEActivitiesObjects.CEActivity(ProductID)

        If typeid = 3 Then
            result = ac.getTestEvalID
        Else
            result = ac.getTestEvalID(typeid)
        End If

        TestEvalID = result

        If typeid = 3 Then
            If result = "" Then
                result = "<button onclick=""window.open('edit_test.aspx?type=eval&preview=Y&evalTestID=new&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Add/Edit Evaluation</button>"
            Else
                result = "<button onclick=""window.open('edit_test.aspx?type=eval&preview=Y&evalTestID=" & result & "&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Edit Evaluation</button>"
            End If
        ElseIf typeid = 2 Then
            Dim ActivityContentDescription As String = ceactivity_obj.getDescriptionBySecionTitle("PPAA").SectionContent
            Dim html As String = ""
            Dim contentEditURL As String = "CEActivityDescription.aspx?PageCode=PPAA&Preview=Y&ActivityID=" & ProductID
            html = "<button onclick=""window.open('" & contentEditURL & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add/Edit Content Description</button>"
            html = html & "<div class=""content-desc"">" & ActivityContentDescription & "</div>"

            If result = "" Then
                result = html & "<button onclick=""window.open('edit_test.aspx?type=posttest&preview=Y&evalTestID=new&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Add/Edit Post Test</button>"

                Dim IsPreTestValid As Boolean
                Dim SqlQuery As String = "Exec CheckPreTestExist " + Convert.ToString(ProductID)
                Dim dr As IDataReader
                Dim dmanager As IDataManager = New DataManager
                dr = dmanager.GetDataReader(SqlQuery)

                If dr IsNot Nothing Then
                    While (dr.Read)
                        IsPreTestValid = dr("Result")
                    End While
                    dr.Close()
                End If

            End If


        ElseIf typeid = 15 Then
            Dim ActivityContentDescription1 As String = ceactivity_obj.getDescriptionBySecionTitle("Verify").SectionContent
            Dim html1 As String = ""
            Dim contentEditURL1 As String = "CEActivityDescription.aspx?PageCode=Verify&Preview=Y&ActivityID=" & ProductID
            html1 = "<button onclick=""window.open('" & contentEditURL1 & "','','width=800, height=600, scrollbars=1, status=1, resizable=1');"" class=""submit"">Add/Edit Verify Description</button>"
            html1 = html1 & "<div class=""content-desc"">" & ActivityContentDescription1 & "</div>"

            If result = "" Then
                result = html1
                '& "<button onclick=""window.open('edit_test.aspx?type=posttest&preview=Y&evalTestID=new&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Add/Edit Post Test</button>"

                Dim IsPreTestValid As Boolean
                Dim SqlQuery As String = "Exec CheckPreTestExist " + Convert.ToString(ProductID)
                Dim dr As IDataReader
                Dim dmanager As IDataManager = New DataManager
                dr = dmanager.GetDataReader(SqlQuery)

                If dr IsNot Nothing Then
                    While (dr.Read)
                        IsPreTestValid = dr("Result")
                    End While
                    dr.Close()
                End If

            ElseIf typeid = 1 Then
                If result = "" Then
                    result = "<button onclick=""window.open('edit_test.aspx?type=pretest&preview=Y&evalTestID=new&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Add/Edit Pre Test</button>"
                Else
                    result = "<button onclick=""window.open('edit_test.aspx?type=pretest&preview=Y&evalTestID=" & result & "&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Edit Pre Test</button>"
                End If
            ElseIf typeid = 4 Then
                If result = "" Then
                    result = "<button onclick=""window.open('edit_test.aspx?type=presurvey&preview=Y&evalTestID=new&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Add/Edit Pre Survey</button>"
                Else
                    result = "<button onclick=""window.open('edit_test.aspx?type=presurvey&preview=Y&evalTestID=" & result & "&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Edit Pre Survey</button>"
                End If
            ElseIf typeid = 5 Then
                If result = "" Then
                    result = "<button onclick=""window.open('edit_test.aspx?type=postsurvey&preview=Y&evalTestID=new&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Add/Edit Post Survey</button>"
                Else
                    result = "<button onclick=""window.open('edit_test.aspx?type=postsurvey&preview=Y&evalTestID=" & result & "&CEActivityID=" & ProductID & "','','width=1200, height=750, scrollbars=1, status=1, resizable=1')"" class=""submit"">Edit Post Survey</button>"
                End If
            End If
        End If
        Return result
    End Function

    Protected Sub sessions_grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles sessions_grid.PageIndexChanged
        sessions_grid.CurrentPageIndex = e.NewPageIndex
        BuildSessionGrid()

    End Sub
    Protected Sub activity_grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles sessions_grid.PageIndexChanged
        'activity_grid.CurrentPageIndex = e.NewPageIndex
        BuildActivityGrid()

    End Sub

    Protected Sub imgSubmit_search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSubmit_search.Click
        Me.BuildSessionGrid()
    End Sub

    Private Function getMoveUp(ByVal elementID As Integer) As String
        Dim result As String = ""
        '        Dim str_disabled As String = ""
        '       If (Request.Form("__eventtarget") + "").ToString().Contains("RadGrid1") Then
        'ViewState("DisableOrderChange") = "Y"
        'str_disabled = "enabled=""false"""
        'Return ""
        'End If
        result = "javascript:document.getElementById('moveupid').value='" + elementID.ToString() + "';document.forms[0].submit();return false;"""
        Return result
    End Function
    Private Function OverviewMoveUp(ByVal sectionTitle As String) As String
        Dim result As String = ""

        result = "javascript:document.getElementById('overviewmoveupid').value='" + sectionTitle.ToString() + "';document.forms[0].submit();return false;"""
        Return result
    End Function

    Private Function getMoveDown(ByVal elementID As Integer) As String
        Dim result As String = ""
        'Dim str_disabled As String = ""
        'If (Request.Form("__eventtarget") + "").ToString().Contains("RadGrid1") Then
        '    ViewState("DisableOrderChange") = "Y"
        '    str_disabled = "enabled=""false"""
        '    Return ""
        'End If
        result = "javascript:document.getElementById('movedownid').value='" + elementID.ToString() + "';document.forms[0].submit();return false;"
        Return result
    End Function

    Private Function OverviewMoveDown(ByVal sectionTitle As String) As String
        Dim result As String = ""

        result = "javascript:document.getElementById('overviewmovedownid').value='" + sectionTitle.ToString() + "';document.forms[0].submit();return false;"
        Return result
    End Function

    Private Function getDeleteUrl(ByVal elementID As Integer, ByVal alert_msg As String) As String
        Dim result As String = String.Empty
        result = "javascript:if(confirm('" & alert_msg & "')){document.getElementById('deleteid').value='" + elementID.ToString() + "';document.forms[0].submit();}return false;"
        Return result
    End Function

    Public Function getAddEventLink() As String
        Dim result As String = ""
        If customActivityType.CustomActivityTypeCode.ToUpper() = "LIVE WEBINAR GROUP" Then
            Dim childCustomActivityTypeID As Integer = 0
            Dim _customtypes As NetScore.Entities.CustomActivityTypes = Utility.GetCustomActivityTypes

            For Each type As NetScore.Entities.CustomActivityType In _customtypes.Values
                If type.CustomActivityTypeCode.ToUpper() = "LIVE WEBINAR CHILD" Then
                    childCustomActivityTypeID = type.CustomActivityTypeID
                End If
            Next

            result = "TeleconferenceEdit.aspx?preview=Y&ParentActivityID=" & ProductID & "&ActivityID=new&ActivityType=" & childCustomActivityTypeID
        ElseIf customActivityType.CustomActivityTypeCode.ToUpper() = "ONLINE PARENT" Then
            Dim childCustomActivityTypeID As Integer = 0
            Dim _customtypes As NetScore.Entities.CustomActivityTypes = Utility.GetCustomActivityTypes

            For Each type As NetScore.Entities.CustomActivityType In _customtypes.Values
                If type.CustomActivityTypeCode.ToUpper() = "ONLINE CHILD" Then
                    childCustomActivityTypeID = type.CustomActivityTypeID
                End If
            Next
            result = "WebinarEdit.aspx?preview=Y&ParentActivityID=" & ProductID & "&ActivityID=new&ActivityType=" & childCustomActivityTypeID
        Else
            'Other types not implemented
        End If
        Return result
    End Function



    Public Function getEditActivityWorkFlowLink() As String
        Dim result As String = ""
        If customActivityType.CustomActivityTypeCode.ToUpper() = "ONLINE PARENT" Then
            result = "AddUpdateActivityWorkFlow.aspx?preview=Y&ParentActivityID=" & ProductID
        Else
            'Other types not implemented
        End If
        Return result
    End Function



    Private Function GetCsatURL(ByVal ClientProdId As String) As String
        Dim clientcsaturl As String = ""
        'Dim cs As New CSATserver()
        'Dim CsatAuth_key As String = System.Configuration.ConfigurationManager.AppSettings("csatauth_key")
        'Dim CsatDev_key As String = System.Configuration.ConfigurationManager.AppSettings("csatdev_key")

        'clientcsaturl = cs.getCSATautoLogin(CsatAuth_key, CsatDev_key, "Bobby", "666999", "12", "lastName", "example@example.com", "10.2.0.127")
        ''test(ClientUserID, firstname, ClientProductid)
        'Dim usertoken As String = ""
        'Dim returnurlstring As String = ""
        'usertoken = "cc37a163-8283-4f4f-a4be-67b7c05ea249"
        'returnurlstring = "users/userlogin.aspx"
        'clientcsaturl = clientcsaturl & "&userid=" & ClientUserID & "&productid=" & ClientProdId & "&token=" & usertoken & "&returnurl=" & returnurlstring & ""
        '' Response.Redirect(clientcsaturl)
        Return clientcsaturl
    End Function

    Protected Sub btnForum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnForum.Click
        ' Create/Launch Forum
        ForumManager.CreateActivityForumGroup(ProductID)
        OpenForumWindow()
    End Sub

    Protected Sub btnPublishForum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPublishForum.Click
        ' Publish Forum
        ForumManager.PublishActivityForumGroup(ProductID, True)
        OpenForumWindow()
    End Sub

    Protected Sub btnUnpublishForum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnpublishForum.Click
        ' Unpublish Forum
        ForumManager.PublishActivityForumGroup(ProductID, False)
    End Sub

    Private Sub OpenForumWindow()
        Dim url As String = String.Format("/ForumLogin.aspx?ActivityID={0}", ProductID)
        OpenNewWindow(url)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If btnForum.Visible And bHasForum Then
            btnForum.Enabled = Not (ForumManager.HasActivityForumGroup(ProductID))

            btnPublishForum.Visible = True
            btnUnpublishForum.Visible = True

            btnPublishForum.Enabled = Not (ForumManager.HasPublishedActivityForumGroup(ProductID))
            btnUnpublishForum.Enabled = Not (btnPublishForum.Enabled)
        End If
    End Sub


    Private Sub OpenNewWindow(ByVal url As String)
        Dim s As String = String.Format("<script language='javascript'>window.open('{0}');</script>", url)
        Page.RegisterStartupScript("open_script", s)
    End Sub
    Protected Sub btnChildActivitiesAcess_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim idata As IDataManager
        idata = New DataManager
        Dim sqlstring As String
        If (RBLChildActivitiesCompletionFlag.SelectedValue = "Yes") Then
            sqlstring = "Update ns_Ceactivities set ChildActivitesCompletionFlag = 1 where ceactivityid = " & ProductID & ""
        Else
            sqlstring = "Update ns_Ceactivities set ChildActivitesCompletionFlag = 2 where ceactivityid = " & ProductID & ""
        End If
        idata.RunSql(sqlstring)
    End Sub

    Protected Sub activity_grid_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim row As GridViewRow = e.Row
            row.Attributes("id") = (e.Row.RowIndex + 1).ToString()
        End If
    End Sub

    <WebMethod>
    Private Sub CopyPreTestToPostTest()
        Dim testevallist As List(Of NetScore.Entities.TestEval) = New List(Of NetScore.Entities.TestEval)
        Dim new_TestEvalID As Integer
        Dim Sql As String = "Exec CopyTestEval " + EvalId
        Dim dr As IDataReader
        Dim dm As IDataManager = New DataManager
        dr = dm.GetDataReader(Sql)

        If dr IsNot Nothing Then
            While (dr.Read)
                new_TestEvalID = dr.GetValue("TestEvalID")
            End While
            dr.Close()
        End If

    End Sub
    'Private Sub Button1_OnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Response.Write("TEst")
    'End Sub

#Region "AjaxMethods"
    <System.Web.Services.WebMethod()> _
    Public Shared Sub UpdateActivityOrder(activityId As String, currentPosition As String, newPosition As String)
        Dim cm As New NetScore.BAL.CourseManager()
        cm.UpdateActivitySortOrder(Convert.ToInt32(activityId), Convert.ToInt32(currentPosition), Convert.ToInt32(newPosition))
    End Sub

#End Region

End Class
