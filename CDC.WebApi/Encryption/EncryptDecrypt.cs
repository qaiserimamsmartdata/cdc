﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CDC.WebApi.Encryption
{
    public static class EncryptDecrypt
    {
        //Key to be used for encryption
        private static readonly byte[] Key =
        {
            0x12, 0xe3, 0x4a, 0xa1, 0x45, 0xd2, 0x56, 0x7c, 0x54, 0xac, 0x67, 0x9f,
            0x45, 0x6e, 0xaa, 0x56
        };

        // Initialization Vector to be used for encryption
        private static readonly byte[] IV = {0x12, 0xe3, 0x4a, 0xa1, 0x45, 0xd2, 0x56, 0x7c};

        /// <summary>
        ///     Gets the encrypted data.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string GetEncryptedData(string input)
        {
            try
            {
                var tripledes = new TripleDESCryptoServiceProvider();
                var inputByteArray = Encoding.UTF8.GetBytes(input);
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, tripledes.CreateEncryptor(Key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();

                var str = Convert.ToBase64String(ms.ToArray());
                cs.Clear();
                return str;
            }
            catch (Exception)
            {
                return input;
            }
        }

        /// <summary>
        ///     Gets the decrypted data.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string GetDecryptedData(string input)
        {
            try
            {
                //Modified By Sanvir to use Smartcare Decryption Algos on Nov 03,2007
                var tripledes = new TripleDESCryptoServiceProvider();
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, tripledes.CreateDecryptor(Key, IV), CryptoStreamMode.Write);
                var cipherbytes = Convert.FromBase64String(input);
                cs.Write(cipherbytes, 0, cipherbytes.Length);
                cs.FlushFinalBlock();

                //construct the string
                var decryptedArray = ms.ToArray();
                var characters = new char[43693];
                var dec = Encoding.UTF8.GetDecoder();

                var charlen = dec.GetChars(decryptedArray, 0, decryptedArray.Length, characters, 0);
                var decrpytedString = new string(characters, 0, charlen);
                return decrpytedString;
            }
            catch (Exception)
            {
                return input;
            }
        }
    }
}