﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Elmah.Contrib.WebApi;
using System.Configuration;
using System.IO;
using System.Net;
using CDC.WebApi.Helpers;

namespace CDC.WebApi
{
    /// <summary>
    /// The WebApiApplication Global Class
    /// </summary>
    /// <seealso cref="System.Web.HttpApplication" />
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Filters.Add(new ElmahHandleErrorApiAttribute());
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Bootstrapper.Run();
            setEmailConfiguration();
        }
        private void setEmailConfiguration()
        {

            #region [ Email Configuration ]
          AT.Net.Service.Email.Configuration.Display = "Pinwheel Care";
            AT.Net.Service.Email.Configuration.Username = ConfigurationManager.AppSettings["SMTP_DEFAULT_USERNAME"];
            AT.Net.Service.Email.Configuration.From = ConfigurationManager.AppSettings["SMTP_DEFAULT_EMAIL"];
            AT.Net.Service.Email.Configuration.Password = ConfigurationManager.AppSettings["SMTP_DEFAULT_PASSWORD"];
            AT.Net.Service.Email.Configuration.Port = int.Parse(ConfigurationManager.AppSettings["SMTP_DEFAULT_PORT"]);
            AT.Net.Service.Email.Configuration.UseSSL = bool.Parse(ConfigurationManager.AppSettings["SMTP_DEFAULT_USESSL"]);
            AT.Net.Service.Email.Configuration.SMTP = ConfigurationManager.AppSettings["SMTP_DEFAULT_HOST"];
            AT.Net.Service.Email.Interval = int.Parse(ConfigurationManager.AppSettings["INTERVAL"]);
            string _ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            AT.Net.Service.EmailDB.ConnectionString = _ConnectionString;
            string ErrorMessage = string.Empty;
            if (AT.Net.Service.EmailDB.CheckConnection(ref ErrorMessage))
            {
                AT.Net.Service.EmailDB.ConfigureDatabase();
            }
            AT.Net.Service.Email.StartProcess();
            #endregion
        }

        protected void Application_BeginRequest()
        {
            if (Request.Headers.AllKeys.Contains("Origin") && Request.HttpMethod == "OPTIONS")
            {
                Response.Flush();
                Response.End();
                //http://stackoverflow.com/questions/29709477/server-cannot-set-status-after-http-headers-have-been-sent-web-api-cors
            }
        }
    }
}