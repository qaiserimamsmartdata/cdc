import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/core/Login'
import Dashboard from '@/components/Areas/Agency/Organization/Dashboard'
import Participant from '@/components/Areas/Agency/Participants/Participant'
import Classes from '@/components/Areas/Agency/Class/Classes'
import Staffs from '@/components/Areas/Agency/Staff/Staffs'
import DashView from '@/components/Areas/Layouts/DashView'
import Registration from '@/components/core/Registration'
import PricingComponent from '@/components/Areas/Agency/Pricing/PricingComponent'
import AgencyTaskCalender from '@/components/Areas/Agency/TaskScheduler/AgencyTaskCalender'

// import BootstrapVue from 'bootstrap-vue'
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'

// import 'assets/css/simple-line-icons.css'
// import 'assets/fonts/fonts.min.css'
// import 'https://fonts.googleapis.com/icon?family=Material+Icons'
// import 'assets/css/bootstrap.css'
//  import 'assets/css/app.css'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      component: DashView,
      children: [
        {
          path: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'Participant',
          component: Participant
        },
        {
          path: 'Classes',
          component: Classes
        },
        {
          path: 'Staffs',
          component: Staffs
        },
        {
          path: 'Calender',
          component: AgencyTaskCalender
        }
      ]
    },

    {
      path: '/Pricing',
      name: 'PricingComponent',
      component: PricingComponent
    },
    {
      path: '/Registration',
      name: 'Registration',
      component: Registration
    }
  ],
  mode:'history'
})
