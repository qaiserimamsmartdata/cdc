// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import axios from 'axios'
import VueLocalForage from 'vue-localforage'
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
import VueProgressBar from 'vue-progressbar'
import VeeValidate from 'vee-validate';
import Datatable from 'vue2-datatable-component'
import FullCalendar from 'vue-full-calendar'
import VModal from 'vue-js-modal'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



Vue.use(BootstrapVue);
Vue.use(FullCalendar)
Vue.use(VModal, { dialog: true })
// import './assets/css/simple-line-icons.css'
// import './assets/fonts/fonts.min.css'
Vue.use(Datatable);
Vue.use(Toaster, {timeout: 5000,
  position:screenLeft

})
Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px'
})
Vue.use(VeeValidate, {fieldsBagName: 'formFields'})
Vue.config.productionTip = false
axios.defaults.baseURL = 'http://localhost:51824/'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  VueLocalForage,
  template: '<App/>',
  components: { App }
}).$mount('#app')
