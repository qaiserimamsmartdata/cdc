import { store } from '../index';
import { STAFF_GET_ALL_STAFF } from '../mutation-types';

export const staffServiceMutations  = {
    [STAFF_GET_ALL_STAFF](state, payload) {
        debugger;
        state.responseModel.status = payload.status;
        state.responseModel.message = payload.message; 
        if(payload.data && payload.data.Content) {
            state.allStaffList = payload.data.Content;
        }
    }
};