/* eslint-disable */
import {
  ALL_PRICINGPLANS,
  ALL_PRICINGPLANS_SUCCESS,
  ERROR_MSG
} from '../mutation-types'

export const commonMutations = {
  [ALL_PRICINGPLANS](state) {
    // Called when fetching products
    state.showLoader = true
  },
  [ALL_PRICINGPLANS_SUCCESS](state, payload) {
    // Called when products have been fetched
    state.showLoader = false
    // Updates state products
    state.PricingPlans = payload
  },
  [ERROR_MSG](state, payload) {}
}
