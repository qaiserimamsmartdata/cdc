/* eslint-disable */
import {
  REGISTRATION,
  REGISTRATION_SUCCESS,
  LOGIN,
  LOGIN_SUCCESS,
  ERROR_MSG
} from '../mutation-types'


export const authMutations = {
  [REGISTRATION](state) {
    // Called when fetching products
    state.showLoader = true
  },
  [REGISTRATION_SUCCESS](state, payload) {
    // Called when products have been fetched
    state.showLoader = false
    // Updates state products
    // state.PricingPlans = payload
  },
  [LOGIN](state) {
    // Called when fetching products
    state.showLoader = true
  },
  [LOGIN_SUCCESS](state, payload) {
    // Called when products have been fetched
    state.showLoader = false
    // Updates state products
    // state.PricingPlans = payload
  },
  [ERROR_MSG](state, payload) {}
}
