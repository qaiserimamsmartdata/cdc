/* eslint-disable */
import {
  ALL_CLASSES,
  ALL_CLASSES_SUCCESS,
  ADD_CLASSES,
  ADD_CLASSES_SUCCESS,
  UPDATE_CLASSES,
  UPDATE_CLASSES_SUCCESS,
  DELETE_CLASSES,
  DELETE_CLASSES_SUCCESS,
  ERROR_MSG
} from '../mutation-types'

export const classesMutations = {
  [ALL_CLASSES](state) {
    // Called when fetching products
    state.showLoader = true
  },
  [ALL_CLASSES_SUCCESS](state, payload) {
    // Called when products have been fetched
    state.showLoader = false
    // Updates state products
    state.PricingPlans = payload
  },
  [ADD_CLASSES](state) {
    // Called when fetching products
    state.showLoader = true
  },
  [ADD_CLASSES_SUCCESS](state, payload) {
    // Called when products have been fetched
    state.showLoader = false
    // Updates state products
    state.PricingPlans = payload
  },
  [UPDATE_CLASSES](state) {
    // Called when fetching products
    state.showLoader = true
  },
  [UPDATE_CLASSES_SUCCESS](state, payload) {
    // Called when products have been fetched
    state.showLoader = false
    // Updates state products
    state.PricingPlans = payload
  },
  [DELETE_CLASSES](state) {
    // Called when fetching products
    state.showLoader = true
  },
  [DELETE_CLASSES_SUCCESS](state, payload) {
    // Called when products have been fetched
    state.showLoader = false
    // Updates state products
    state.PricingPlans = payload
  },
  [ERROR_MSG](state, payload) {}
}
