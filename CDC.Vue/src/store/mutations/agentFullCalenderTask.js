/* eslint-disable */
import {
    AGENCY_ADDTASK
  } from '../mutation-types'
import {
  AGENCY_GETALLTASK
  } from '../mutation-types'
  import {
    store
  } from '../index'
  
  export const agentFullCalenderTaskMutations = { 
    [AGENCY_ADDTASK](state, payload) {  
      state.responseModel.status = payload.status;
      state.responseModel.message = payload.message; 
    },
    [AGENCY_GETALLTASK](state,payload){            
      state.responseModel.status = payload.status;
      state.responseModel.message = payload.message; 
      if(payload.data != undefined){
        state.agencyAllTask = payload.data
      }
      
    }
  }
  