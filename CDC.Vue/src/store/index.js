import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

// import { productGetters, manufacturerGetters } from './getters'
import { authMutations} from './mutations/auth'
import { commonMutations} from './mutations/common'
import { authActions} from './actions/auth'
import { commonActions} from './actions/common'
import{ agentFullCalenderTaskActions } from './actions/agentFullCalenderTask'
import{ agentFullCalenderTaskMutations } from './mutations/agentFullCalenderTask'
import { classActions} from './actions/classes'
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
Vue.use(Vuex)

Vue.use(Toaster, {timeout: 5000})
const store = new Vuex.Store({
  state: {
    PricingPlans: [],
    responseModel: {
      status: 0,
      message: ""
    }, 
    agencyAllTask : [] ,
    classes:[] 
  },
  // getters: Object.assign({}, ),
  mutations: Object.assign({}, authMutations, commonMutations,agentFullCalenderTaskMutations),
  actions: Object.assign({}, authActions,commonActions,agentFullCalenderTaskActions,classActions)
})

export default store
