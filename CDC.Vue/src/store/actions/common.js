  import axios from 'axios'
  const API_BASE = 'http://localhost:8000/api/v1'

  import {
    ALL_PRICINGPLANS,
    ALL_PRICINGPLANS_SUCCESS,
    ALL_ROOMSDROPDOWN,
    ALL_CATEGORYDROPDOWN
  } from '../mutation-types'

  export const commonActions = {
    LOAD_PRICINGPLANS() {
      return new Promise((resolve, reject) => {
        axios.post('/api/Common/getPricingPlans')
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },
    LOAD_CATEGORY({
      commit
    }, {
      model
    }) {
      return new Promise((resolve, reject) => {
        axios.post('/api/Category/GetCategory', model)
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },
    LOAD_ROOMS({
      commit
    }, {
      model
    }) {
      return new Promise((resolve, reject) => {
        axios.post('/api/Room/GetRoom', model)
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },
    LOAD_AGENCYLOCATION({
      commit
    }, {
      model
    }) {
      return new Promise((resolve, reject) => {

        axios.post('/api/common/getLocations', model)
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },
    LOAD_STATUS({
      commit
    }, {
      model
    }) {
      var statusModel = {
        filter: '',
        limit: '10',
        order: '-Name',
        page: 1,
        status: 0,
        studentclass: 0,
        name: '',
        AgencyId: model.AgencyID
    };
      return new Promise((resolve, reject) => {
     
        axios.post('/api/ClassStatus/GetAllClassStatus', statusModel)
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },
  }
