  import axios from 'axios'

  import {
    REGISTRATION,
    REGISTRATION_SUCCESS,
    LOGIN,
    LOGIN_SUCCESS,
    ERROR_MSG
  } from '../mutation-types'

  export const authActions = {
    REGISTRATION({
      commit
    }, {
      model
    }) {
      return new Promise((resolve, reject) => {
        axios.post('/api/AgencyRegistration/AddAgency', model)
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },
    LOGIN({
      commit
    }, {
      model
    }) {
      return new Promise((resolve, reject) => {
        axios.post('/api/User/GetToken', model)
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },

  }
