  import axios from 'axios'
  const API_BASE = 'http://localhost:8000/api/v1'

  import {
    ALL_CLASSES,
    ALL_CLASSES_SUCCESS,
    ADD_CLASSES,
    ADD_CLASSES_SUCCESS,
    UPDATE_CLASSES,
    UPDATE_CLASSES_SUCCESS,
    DELETE_CLASSES,
    DELETE_CLASSES_SUCCESS,
    ERROR_MSG
  } from '../mutation-types'

  export const classActions = {
    ADD_CLASS({
      commit
    }, {
      clsModel
    }) {
      return new Promise((resolve, reject) => {
        axios.post('/api/Class/Add', clsModel)
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },
    GET_CLASS_LIST({
      commit
    }, {
      model
    }) {
      return new Promise((resolve, reject) => {
        axios.post('/api/Class/All', model)
          .then(response => resolve(response.data))
          .catch(error => dispatch('error', {
            error
          }));
      });
    },
    error(context, {
      error
    }) {
      console.error(error);
    },

   
    // LOAD_CATGORY({commit},{model}) {
    //   return new Promise( ( resolve, reject ) => {
    //     axios.post( '/api/Category/GetCategory',model)
    //       .then( response => resolve( response.data ) )
    //       .catch( error => dispatch( 'error', { error } ) );
    //   } );
    // },
    // error( context, { error } ) {
    //   console.error( error );
    // },
    // LOAD_CATGORY({commit},{model}) {
    //   return new Promise( ( resolve, reject ) => {
    //     axios.post( '/api/Room/GetRoom',model)
    //       .then( response => resolve( response.data ) )
    //       .catch( error => dispatch( 'error', { error } ) );
    //   } );
    // },
    // error( context, { error } ) {
    //   console.error( error );
    // },
    // LOAD_AGENCYLOCATIONS({commit},{model}) {
    //   return new Promise( ( resolve, reject ) => {
    //     axios.post( '/api/common/getLocations',model)
    //       .then( response => resolve( response.data ) )
    //       .catch( error => dispatch( 'error', { error } ) );
    //   } );
    // },
    // error( context, { error } ) {
    //   console.error( error );
    // },
  }
