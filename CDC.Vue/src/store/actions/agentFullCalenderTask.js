import axios from 'axios'
// const API_BASE = 'http://localhost:8000/api/v1'

import {
  AGENCY_ADDTASK
} from '../mutation-types'
import {
  AGENCY_GETALLTASK
} from '../mutation-types'

export const agentFullCalenderTaskActions = {
  AGENCY_ADDTASK({commit},modeldata) {    
    // var data = [];
    // data.push(modeldata);

    // var models = {
    //   models : JSON.stringify(data)
    // }
    
    //   return new Promise( ( resolve, reject ) => {
    //     axios.post('/api/Tasks/AddAgentTasks',modeldata)
    //     context.commit('updateMessage', response.data.message);
    //     resolve();
    //   } );
    // },
    // error( context, { error } ) {
    //   console.error( error );
    return new Promise((resolve) => {
      axios.post('/api/Tasks/AddAgentTasks',modeldata).then((response) => {        
        commit('AGENCY_ADDTASK', response);
        resolve();
      });
    });
     },
     AGENCY_GETALLTASK({commit},modeldata){      
      return new Promise((resolve) => {
        axios.post(`/api/Tasks/GetAll?${modeldata}`).then((response) => {              
          commit('AGENCY_GETALLTASK', response);
          resolve();
        });
      });
     }
}